<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangesNameofcols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('Product_Name','product_name');
            $table->renameColumn('Product_Description','product_description');
            $table->renameColumn('Stock_Quantity','stock_quantity');
            $table->renameColumn('Price','price');
            $table->renameColumn('Product_Images','product_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('product_name','Product_Name');
            $table->renameColumn('product_description','Product_Description');
            $table->renameColumn('stock_quantity','Stock_Quantity');
            $table->renameColumn('price','Price');
            $table->renameColumn('product_images','Product_Images');
        });
    }
}
