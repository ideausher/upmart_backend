<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DefaultStatTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->boolean('vehicle_status')->default(0)->change();
            $table->boolean('drivinglicence_status')->default(0)->change();
            $table->boolean('licence_no_plate_status')->default(0)->change();
            $table->boolean('vehicle_insurence_status')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->boolean('vehicle_status')->default(1)->change();
            $table->boolean('drivinglicence_status')->default(1)->change();
            $table->boolean('licence_no_plate_status')->default(1)->change();
            $table->boolean('vehicle_insurence_status')->default(1)->change();
        });
    }
}
