<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumninDeliveryBookingTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveryboy_main_documents', function (Blueprint $table) {
            $table->dateTime('vehicle_insurence_valid_from')->nullable()->after('policy_number');
            $table->datetime('vehicle_insurence_valid_upto')->nullable()->after('policy_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
