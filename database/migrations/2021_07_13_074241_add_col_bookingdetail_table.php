<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColBookingdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->decimal('amount_paid_to_delivery_boy_on_order_delivered',12,2)->after('bs_amount_to_vendor_before_commi');
            $table->decimal('amount_paid_to_delivery_boy_on_order_pickup',12,2)->after('bs_amount_to_vendor_before_commi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            //
        });
    }
}
