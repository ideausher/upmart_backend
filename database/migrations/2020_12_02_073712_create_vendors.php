<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('Vendor_Name');
            $table->string('Email')->unique();
            $table->string('Profile_Picture')->nullable();
            $table->bigInteger('Phone_number_with_country_code');
            $table->string('Password');
            $table->text('Shop_Name');
            $table->text('Shop_Address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
