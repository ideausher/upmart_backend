<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->decimal('amount_before_discount',12,2)->after('bookingDateTime');
            $table->decimal('discount_amount',12,2)->after('bookingDateTime');
            $table->decimal('amount_after_discount',12,2)->after('bookingDateTime');
            $table->decimal('delivery_charge_for_customer',12,2)->after('bookingDateTime');
            $table->decimal('delivery_charge_to_delivery_boy',12,2)->after('bookingDateTime');
            $table->decimal('platform_charge',12,2)->after('bookingDateTime');
            $table->decimal('commission_charge',12,2)->after('bookingDateTime');
            $table->decimal('after_charge_amount',12,2)->after('bookingDateTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            //
        });
    }
}
