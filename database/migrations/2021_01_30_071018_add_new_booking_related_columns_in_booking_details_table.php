<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewBookingRelatedColumnsInBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->integer('deliveryBoyId')->after('couponId')->nullable();
            $table->tinyInteger('bookingType')->after('deliveryBoyId')->default(1); // 1 means delivery 2 means take away
            $table->dateTime('bookingDateTime')->after('bookingType');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            //
        });
    }
}
