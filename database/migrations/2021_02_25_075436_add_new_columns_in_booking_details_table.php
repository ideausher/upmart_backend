<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsInBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            $table->decimal('bs_amount_to_vendor_before_commi',12,2)->nullable()->after('active');
            $table->decimal('bs_comm_charged_to_vendor',12,2)->nullable()->after('active');
            $table->decimal('bs_amount_to_vendor_after_commi',12,2)->nullable()->after('active');
            $table->decimal('bs_total_paid_to_admin',12,2)->nullable()->after('active');
            $table->decimal('ws_deliverycharge_amount_to_db',12,2)->nullable()->after('active');
            $table->decimal('ws_tip_to_db',12,2)->nullable()->after('active');
            $table->decimal('ws_total_amount_to_db',12,2)->nullable()->after('active');
            $table->decimal('as_total_amount_to_admin',12,2)->nullable()->after('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_details', function (Blueprint $table) {
            //
        });
    }
}
