<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInUserBasedAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_based_location', function (Blueprint $table) {
            $table->decimal('accuracy',10,2)->after('lng');
            $table->decimal('altitude',10,2)->after('accuracy');
            $table->decimal('speed',10,2)->after('altitude');
            $table->decimal('speedAccuracy',10,2)->after('speed');
            $table->decimal('heading',10,2)->after('speedAccuracy');
            $table->decimal('time',10,2)->after('heading');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_based_address', function (Blueprint $table) {
            //
        });
    }
}
