<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameDeliveryboyTemporaryDoeuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->string('registeration_number',255)->nullable()->change();
            $table->string('vehicle_picture',255)->nullable()->change();
            $table->string('model',255)->nullable()->change();
            $table->string('color',255)->nullable()->change();
            // $table->timestamp('valid_upto');
            $table->string('drinvinglicence_images',255)->nullable()->change();
            $table->string('drivinglicence_no',255)->nullable()->change();
            // $table->timestamp('drivinglicence_expiry_date');
            $table->string('licence_no_plate_images',255)->nullable()->change();
            $table->string('no_plate',255)->nullable()->change();
            $table->string('vehicle_insurence_image',255)->nullable()->change();
            $table->string('policy_number',255)->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            //
        });
    }
}
