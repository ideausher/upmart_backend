<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsInHelpandsupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('help_and_supports', function (Blueprint $table) {
            $table->boolean('query_status')->after('feedback_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('help_and_supports', function (Blueprint $table) {
            $table->dropColumn('query_status');
        });
    }
}
