<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColsTempStat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->renameColumn('drinvinglicence_status','drivinglicence_status');
            $table->renameColumn('drinvinglicence_reason','drivinglicence_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            //
        });
    }
}
