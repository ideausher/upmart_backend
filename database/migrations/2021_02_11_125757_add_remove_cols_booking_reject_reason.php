<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRemoveColsBookingRejectReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->dropForeign('booking_reject_reason_user_id_foreign');
            $table->dropColumn('user_id');
            $table->integer('booking_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_reject_reason', function (Blueprint $table) {
            $table->dropColumn('booking_id');
        });
    }
}
