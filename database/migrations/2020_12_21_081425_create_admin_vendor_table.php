<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(static function () {
            Schema::create('admin_vendors', static function (Blueprint $table) {
                $table->increments('id');
                // $table->string('first_name')->nullable();
                // $table->string('last_name')->nullable();
                // $table->string('email');
                // $table->string('password');
                // $table->rememberToken();
                $table->bigInteger('admin_users_id')->references('id')->on('admin_users');
                $table->string('Profile_Picture')->nullable();
                $table->string('phonenumber_with_countrycode');
                $table->string('country_iso_code');
                $table->string('shop_name');
                $table->string('shop_address');
                $table->string('additional_address');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('admin_vendors');
    }
}


   