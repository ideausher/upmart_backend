<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeNullableColumnsInTableBannersLocationWiseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_banners_location_wise', function (Blueprint $table) {
            $table->string("address")->nullable()->change();
            $table->string("latitude")->nullable()->change();
            $table->string("longitude")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_banners_location_wise', function (Blueprint $table) {
            //
        });
    }
}
