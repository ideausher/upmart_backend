<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsAdminVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            $table->decimal('platform_charges',12,2)->after('shop_type');
            $table->decimal('commision_charges',12,2)->after('platform_charges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            $table->dropColumn('platform_charges');
            $table->dropColumn('commision_charges');
        });
    }
}
