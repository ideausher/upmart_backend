<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColsTempDocs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->boolean('vehicle_status')->default(1)->comment('1->Pending, 2->Approved, 3->Rejected')->after('vehicle_picture');
            $table->boolean('drinvinglicence_status')->default(1)->comment('1->Pending, 2->Approved, 3->Rejected')->after('drinvinglicence_images');
            $table->boolean('licence_no_plate_status')->default(1)->comment('1->Pending, 2->Approved, 3->Rejected')->after('licence_no_plate_images');
            $table->boolean('vehicle_insurence_status')->default(1)->comment('1->Pending, 2->Approved, 3->Rejected')->after('vehicle_insurence_image');
            
            $table->string('vehicle_reason')->nullable()->after('vehicle_status');
            $table->string('drinvinglicence_reason')->nullable()->after('drinvinglicence_status');
            $table->string('licence_no_plate_reason')->nullable()->after('licence_no_plate_status');
            $table->string('vehicle_insurence_reason')->nullable()->after('vehicle_insurence_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveryboy_temporary_documents', function (Blueprint $table) {
            $table->dropColumn('vehicle_status');
            $table->dropColumn('drinvinglicence_status');
            $table->dropColumn('licence_no_plate_status');
            $table->dropColumn('vehicle_insurence_status');

            $table->dropColumn('vehicle_reason');
            $table->dropColumn('drinvinglicence_reason');
            $table->dropColumn('licence_no_plate_reason');
            $table->dropColumn('vehicle_insurence_reason');
        });
    }
}
