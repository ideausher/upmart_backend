<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryboyTemporaryDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveryboy_temporary_documents', function (Blueprint $table) {
            
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('registeration_number');
            $table->string('vehicle_picture');
            $table->string('model');
            $table->string('color');
            $table->timestamp('valid_upto');
            $table->string('drinvinglicence_images');
            $table->string('drivinglicence_no');
            $table->timestamp('drivinglicence_expiry_date');
            $table->string('licence_no_plate_images');
            $table->string('no_plate');
            $table->string('vehicle_insurence_image');
            $table->string('policy_number');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveryboy_temporary_documents');
    }
}
