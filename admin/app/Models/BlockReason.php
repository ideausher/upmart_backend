<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\HasMedia;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;



class BlockReason extends Model
{
    protected $table = 'block_reasons_vendor';

    protected $fillable = ['reason', 'user_id'];


    protected $dates = [
        'created_at',
        'updated_at'

    ];
    public $timestamps = false;

    // protected $appends = ['resource_url'];
}
