<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryBoyTemporaryDocuments extends Model
{
    use HasFactory;

    protected $table = 'deliveryboy_temporary_documents';

    protected $fillable = [
        'vehicle_status',
        'drivinglicence_status',
        'licence_no_plate_status',
        'vehicle_insurence_status',
        'vehicle_insurence_reason',
        'licence_no_plate_reason',
        'drivinglicence_reason',
        'vehicle_reason',
        'user_id','registeration_number','vehicle_picture','model','color','valid_upto','drivinglicence_images','drivinglicence_no','drivinglicence_expiry_date','licence_no_plate_images','no_plate','vehicle_insurence_image','policy_number','vehicle_company','vehicle_insurence_valid_upto','vehicle_insurence_valid_from'
    ];
}
