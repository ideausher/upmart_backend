<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariousCharge extends Model
{
    protected $fillable = [
        'chargeName',
        'type',
        'value',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/various-charges/'.$this->getKey());
    }
}
