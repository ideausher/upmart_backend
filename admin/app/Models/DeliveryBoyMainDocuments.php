<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryBoyMainDocuments extends Model
{
    use HasFactory;
    
    protected $table='deliveryboy_main_documents';

    protected $fillable = ['user_id','registeration_number','vehicle_picture','model','color','valid_upto','drivinglicence_images','drivinglicence_no','drivinglicence_expiry_date','licence_no_plate_images','no_plate','vehicle_insurence_image','policy_number','vehicle_company','vehicle_insurence_valid_upto','vehicle_insurence_valid_from'];

    public $timestamps = false;
}
