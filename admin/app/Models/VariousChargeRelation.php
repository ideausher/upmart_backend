<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariousChargeRelation extends Model
{
    use HasFactory;

    protected $table = 'various_charge_relation';

    protected $fillable = ['various_charge_id','startRange','endRange','price'];

    protected $dates = [
        'created_at',
        'updated_at'

    ];
    public $timestamps = false;

}
