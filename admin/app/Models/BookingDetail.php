<?php

namespace App\Models;

use App\Models\User;
use App\Models\Coupon;
use App\Models\AdminVendor;
use App\Models\OrderDetail;
use App\Models\UserAddress;
use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{   
    const LEVELS = ['','Pickup','Delivery'];
    const ORDER_STATUS = ['','Pending','Accepted','Rejected','Order in progress','Delivery boy assigned','Delivery boy started the journey','Delivery boy reached to vendor','Delivery boy picked order','Delivery boy reached to customer','Delivered product','Cancelled by customer','Cancelled by delivery boy','Other','Auto cancel'];

    protected $table = 'booking_details';

    protected $fillable = [
        'userId',
        'shopId',
        'addressId',
        'cardId',
        'couponId',
        'deliveryBoyId',
        'bookingType',
        'shop_status_change_time',
        'status',
        'booking_code'
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public $timestamps = false;
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/booking-details/'.$this->getKey());
    }

    public function users()
    {
        return $this->belongsTo(User::class,'userId','id');
    }

    public function shop()
    {
        return $this->belongsTo(AdminVendor::class,'shopId','id');
    }

    public function deliveryboy()
    {
        return $this->belongsTo(User::class,'deliveryBoyId','id');
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class,'addressId','id');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'couponId','id');
    }

    public function orderdetails()
    {
        return $this->hasMany(OrderDetail::class,'bookingId','id');
    }

    public function review()
    {
        return  $this->hasMany(Review::class,'booking_id','id');
    }

    public function rejectedbookings()
    {
        return $this->hasOne(BookingRejectReason::class,'booking_id','id');
    }
}
