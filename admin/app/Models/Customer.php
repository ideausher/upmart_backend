<?php

namespace App\Models;

use App\Models\UserAddress;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'users';

    public function customerrelation(){
        return $this->hasMany(UserAddress::class,'user_id','id');
    }

    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/customers/'.$this->getKey());
    }
}
