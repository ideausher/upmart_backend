<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StripeToken extends Model
{
    protected $table = 'stripe_token';

    protected $fillable = [
        'stripe_token',
        'vendor_id',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/stripe-tokens/'.$this->getKey());
    }
}
