<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $table = 'review';
    protected $fillable = ['shop_id','delivery_boy_id','customer_id','rating','customer_id_by','delivery_boy_id_by','shop_id_by','feedback','booking_id'];
}
