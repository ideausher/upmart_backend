<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpAndSupport extends Model
{
    protected $fillable = [
        'booking_id',
        'user_id',
        'shop_id',
        'feedback_title',
        'feedback_description',
        'query_status'
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url','change_status'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/help-and-supports/'.$this->getKey());
    }

    public function getChangeStatusAttribute()
    {
        return url('/admin/help-and-supports/'.$this->getKey().'/change-status');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function shop()
    {
        return $this->belongsTo(AdminVendor::class,'shop_id','id');
    }

    public function booking()
    {
        return $this->belongsTo(BookingDetail::class,'booking_id','id');
    }
}
