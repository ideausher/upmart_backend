<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockReasonDeliveryBoy extends Model
{
    use HasFactory;

    protected $table='block_reason_delivery_boy';

    protected $fillable = ['reason', 'user_id'];
    
    
    protected $dates = [
        'created_at',
        'updated_at'
    
    ];
    public $timestamps = false;
    
    protected $appends = ['resource_url'];
}
