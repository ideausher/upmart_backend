<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\PushNotification;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class Notification extends Model
{

    use SoftDeletes;

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'notifications';

    protected $fillable = ['send_by','send_to','notification_type','title','description','is_read'];

    public static function createNotificationNew($userId, $title,$message)
    {
        $notificationData["data"]["noti_title"] = "Welcome";
        $notificationData["message"] = $message;
        $notificationData["title"] = $title;
        $pushNotification = new PushNotification();
        $status = (is_array($userId)) ? $pushNotification->sendchunkNotification($notificationData, $userId) : $pushNotification->sendNotification($notificationData, $userId);

        return $status;
    }
    

    // public static function createNotification($itemId, $itemType, $notiTitle, $notiMessage, $userId = null, $foradmin = null)
    // {
    // $notificationData["message"] = $notiMessage;
    // $notificationData["title"] = $notiTitle;
    // $status = $pushNotification->sendNotification($notificationData, $userId);
    // return $status;
    // $notificationData = array();

    // $user = isset($userId) ? User::find($userId) : Auth::User();

    // $booking = '';
    // if ($itemType == '1') {
    // $booking = Booking::where(['id' => $itemId])->first();
    // }
    // $pushNotification = new PushNotification();
    // $notification = new Notification;
    // $notification->user_id = $user->id ?? 0;

    // $notification->for_admin = $foradmin ? 1 : 0;
    // $notification->type_id = $itemId ? $itemId : 0;
    // $notification->type = $itemType;
    // $notification->title = $notiTitle;
    // $notification->message = $notiMessage;
    // $notification->save();

    // $filename = 'api_datalogger_' . date('d-m-y') . '.log';
    // // \File::append( storage_path('logs' . DIRECTORY_SEPARATOR . $filename), 'Notification got here start here'.$notification.'notification end here' . "\n" . str_repeat("=", 20) . "\n\n");

    // $notificationData["data"]["item_type"] = $notification->item_type;
    // $notificationData["data"]["item_id"] = $itemId ? $itemId : 0;
    // $notificationData["data"]["id"] = $notification->id;
    // $notificationData["data"]["noti_title"] = $notiTitle;
    // if (isset($booking->id)) {
    // $notificationData["data"]["bookingId"] = $booking->id;
    // $notificationData["data"]["bookingDate"] = $booking->booking_date;
    // $notificationData["data"]["userName"] = $booking->full_name;
    // $notificationData["data"]["serviceName"] = $booking->service_name;
    // $notificationData["data"]["bookingTimeSlot"] = $booking->slot_start_from . '-' . $booking->slot_start_to;
    // $notificationData["data"]["userProfileImage"] = isset($booking->user->image) ? $booking->user->image : "";
    // }

    // $notificationData["data"]["updated_at"] = $notification->updated_at;
    // $notificationData["data"]["noti_message"] = $notiMessage;
    // $notificationData["data"]["created_at"] = $notification->updated_at;
    // $notificationData["data"]["user_id"] = $notification->user_id;
    // $notificationData["data"]["created_at"] = $notification->created_at;
    // $notificationData["data"]["notification_type"] = 0;

    // if ($itemType == '4') {
    // $notificationData["data"]["notification_type"] = 1;
    // }
    // $notificationData["message"] = $notiMessage;
    // $notificationData["title"] = $notiTitle;

    // Log::error($user->id);
    // Log::error("** Notification sent to the above user **");
    // $status = $pushNotification->sendNotification($notificationData, $user->id);
    // return $notification;
    // }

    public static function createNotificationFuture($itemId, $itemType, $notiTitle, $notiMessage, $userId = null, $foradmin = null)
    {
        $notificationData = array();

        $user = isset($userId) ? User::find($userId) : Auth::User();

        $booking = '';
        if ($itemType == '1') {
        $booking = BookingDetail::where(['id' => $itemId])->first();
        }
        // $pushNotification = new PushNotification();
        $notification = new Notification;
        $notification->user_id = $user->id ?? 0;

        $notification->for_admin = $foradmin ? 1 : 0;
        $notification->type_id = $itemId ? $itemId : 0;
        $notification->type = $itemType;
        $notification->title = $notiTitle;
        $notification->message = $notiMessage;
        $notification->save();

        // $filename = 'api_datalogger_' . date('d-m-y') . '.log';
        // \File::append( storage_path('logs' . DIRECTORY_SEPARATOR . $filename), 'Notification got here start here'.$notification.'notification end here' . "\n" . str_repeat("=", 20) . "\n\n");

        // $notificationData["data"]["item_type"] = $notification->item_type;
        // $notificationData["data"]["item_id"] = $itemId ? $itemId : 0;
        // $notificationData["data"]["id"] = $notification->id;
        // $notificationData["data"]["noti_title"] = $notiTitle;
        // if (isset($booking->id)) {
        // $notificationData["data"]["bookingId"] = $booking->id;
        // $notificationData["data"]["bookingDate"] = $booking->booking_date;
        // $notificationData["data"]["userName"] = $booking->full_name;
        // $notificationData["data"]["serviceName"] = $booking->service_name;
        // $notificationData["data"]["bookingTimeSlot"] = $booking->slot_start_from . '-' . $booking->slot_start_to;
        // $notificationData["data"]["userProfileImage"] = isset($booking->user->image) ? url('images/avatars/' . $booking->user->image) : "";
        // }

        // $notificationData["data"]["updated_at"] = $notification->updated_at;
        // $notificationData["data"]["noti_message"] = $notiMessage;
        // $notificationData["data"]["created_at"] = $notification->updated_at;
        // $notificationData["data"]["user_id"] = $notification->user_id;
        // $notificationData["data"]["created_at"] = $notification->created_at;
        // $notificationData["data"]["notification_type"] = 0;

        // if ($itemType == '4') {
        // $notificationData["data"]["notification_type"] = 1;
        // }
        // $notificationData["message"] = $notiMessage;
        // $notificationData["title"] = $notiTitle;

        // Log::error($user->id);
        // Log::error("** Notification sent to the above user **");
        // $status = $pushNotification->sendNotification($notificationData, $user->id);
        return $notification;
    }
}