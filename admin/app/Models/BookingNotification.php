<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingNotification extends Model
{
    use HasFactory;

    protected $table='booking_notifications';

    protected $fillable = ['booking_id','deliveryboy_id','notification_message','deliveryboy_accept_status','expiration_time','deliveryboy_rating'];

    public function rejectedbookings()
    {
        return $this->hasOne(BookingRejectReason::class,'id','booking_id');
    }
}
