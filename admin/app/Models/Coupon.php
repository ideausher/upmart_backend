<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;

use Brackets\Media\HasMedia\MediaCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class Coupon extends Model implements HasMedia
{
    use SoftDeletes;

    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $fillable = [
        'coupon_code',
        'coupon_description',
        'coupon_discount',
        'coupon_max_amount',
        'coupon_min_amount',
        'coupon_name',
        'coupon_type',
        'end_date',
        'maximum_per_customer_use',
        'maximum_total_use',
        'start_date',
        'shopId',
        'isPrimary'
    
    ];
    
    
    protected $dates = [
        'created_at',
        'deleted_at',
        'end_date',
        'start_date',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/coupons/'.$this->getKey());
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('coupon_image')->disk('media')->accepts('image/*');
    }
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->autoRegisterThumb200();
        $this->addMediaCollection('coupon_image');
    }
    
    public function allmedia()
    {
        return ($this->hasMany(\Spatie\MediaLibrary\MediaCollections\Models\Media::class,'model_id'));
    }

    public static function getGalleryById($model, $id, $size = 'thumb_800')
    {
        $image = [];
        $frontimages = Media::where('collection_name', 'coupon_image')
        ->where('model_type', 'like', '%' . ucwords($model) . '%')
        ->where('model_id', '=', $id)
        ->where('collection_name', '=', 'coupon_image')
        ->orderByRaw('model_id', 'desc')
        ->first();
        if (!empty($frontimages)) {
            $value = $frontimages;
            $image = URL::to($value->getUrl($size));
         
            return $image;
        }
        return '';
    }

    public static function bootHasMediaCollectionsTrait(): void
    {
        static::saving(static function ($model) {
            $model->processMedia(collect(request()->only($model->getMediaCollections()->map->getName()->toArray())));
        });
    }
    public function processMedia(Collection $inputMedia): void
    {
        //First validate input
        // $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
        //     $this->validate(collect($inputMedia->get($mediaCollection->getName())), $mediaCollection);
        // });

        //Then process each media
        $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
            collect($inputMedia->get($mediaCollection->getName()))->each(function ($inputMedium) use (
                $mediaCollection
            ) {
                $this->processMedium($inputMedium, $mediaCollection);
            });
        });
    }
    public function processMedium(array $inputMedium, MediaCollection $mediaCollection): void
    {
        if (isset($inputMedium['id']) && $inputMedium['id']) {
            if ($medium = app(Media::class)->find($inputMedium['id'])) {
                if (isset($inputMedium['action']) && $inputMedium['action'] === 'delete') {
                    $medium->delete();
                } else {
                    $medium->custom_properties = $inputMedium['meta_data'];
                    $medium->save();
                }
             }
        } elseif (isset($inputMedium['action']) && $inputMedium['action'] === 'add') {
            $mediumFileFullPath = $inputMedium['path'];
            $this->addMediaFromUrl($mediumFileFullPath)
                ->withCustomProperties($inputMedium['meta_data'])
                ->toMediaCollection($mediaCollection->getName(), $mediaCollection->getDisk());}
    }
}
