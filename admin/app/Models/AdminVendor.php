<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\HasMedia;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Spatie\MediaLibrary\Models\Media as MediaModel;
use Illuminate\Support\Facades\Request;
use Brackets\Media\HasMedia\MediaCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;


class AdminVendor extends Model
{
    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;
    use SoftDeletes;

    protected $table = 'admin_vendors';

    protected $fillable = [
        
        'country_code',
        'phonenumber_with_countrycode',
        'shop_name',
        'lat',
        'lng',
        'shop_address',
        'additional_address',
        'description',
        'shop_type',
        'platform_charges',
        'commission_charges',
        'gst',
        'hst',
        'pst',
    ];
    
    
    protected $dates = [
    
    ];
    public $timestamps = false;
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/admin-vendors/'.$this->getKey());
    }

    public function vendor()
    {
        return $this->belongsTo(AdminUsers::class,'user_id','id');
    }

    public function items()
    {
        return $this->hasMany(Item::class,'shop_id','id');
    }
}
