<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerificationPanel extends Model
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable=['is_block'];

    public function documents()
    {
        return $this->hasMany(DeliveryBoyTemporaryDocuments::class,'user_id','id');
    }

    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/verification-panel/'.$this->getKey());
    }
}
