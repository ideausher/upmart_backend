<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\CssSelector\Node\FunctionNode;

class Faq extends Model
{
    protected $table = 'faq';

    protected $fillable = [
        'category_id',
        'question',
        'answer',
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/faqs/'.$this->getKey());
    }

    public function category()
    {
        return $this->belongsTo(FaqCategory::class,'category_id','id');
    }
}
