<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
// use Spatie\MediaLibrary\Models\Media as MediaModel;
use Spatie\MediaLibrary\MediaCollections\Models\Media as MediaModel;
use Illuminate\Support\Facades\Request;
use Brackets\Media\HasMedia\MediaCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class Item extends Model implements HasMedia
{
    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;

    protected $fillable = [
        'product_name',
        'product_description',
        'stock_quantity',
        'price',
        'product_image',
        'category_name',
        'company_name',
        'shop_id',
        'shop_name',
        'category_id',
        'is_disabled',
        'discount',
        'new_price',
        'gst_enable'
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/items/'.$this->getKey());
    }

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function registerMediaCollections(): void {
        $this->addMediaCollection('product_images')
        ->accepts('image/*')->disk('media')->maxNumberOfFiles(10);
    }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->autoRegisterThumb200();
        $this->addMediaCollection('product_images');
    }

    public function mediaData() {
       
        return ($this->hasone(\Spatie\MediaLibrary\MediaCollections\Models\Media::class, 'model_id'));
    }

    public static function getGalleryById($model, $id, $size = 'thumb_800')
    {
        $image = [];
        $frontimages = Media::where('collection_name', 'product_images')
        ->where('model_type', 'like', '%' . ucwords($model) . '%')
        ->where('model_id', '=', $id)
        ->where('collection_name', '=', 'product_images')
        ->orderByRaw('model_id', 'desc')
        ->first();
        if (!empty($frontimages)) {
            $value = $frontimages;
            $image = URL::to($value->getUrl($size));
         
            return $image;
        }
        return '';
    }
    public static function bootHasMediaCollectionsTrait(): void
    {
        static::saving(static function ($model) {
            $model->processMedia(collect(request()->only($model->getMediaCollections()->map->getName()->toArray())));
        });
    }
    public function processMedia(Collection $inputMedia): void
    {
        //First validate input
        // $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
        //     $this->validate(collect($inputMedia->get($mediaCollection->getName())), $mediaCollection);
        // });

        //Then process each media
        $this->getMediaCollections()->each(function ($mediaCollection) use ($inputMedia) {
            collect($inputMedia->get($mediaCollection->getName()))->each(function ($inputMedium) use (
                $mediaCollection
            ) {
                $this->processMedium($inputMedium, $mediaCollection);
            });
        });
    }
    public function processMedium(array $inputMedium, MediaCollection $mediaCollection): void
    {
        if (isset($inputMedium['id']) && $inputMedium['id']) {
            if ($medium = app(MediaModel::class)->find($inputMedium['id'])) {
                if (isset($inputMedium['action']) && $inputMedium['action'] === 'delete') {
                    $medium->delete();
                } else {
                    $medium->custom_properties = $inputMedium['meta_data'];
                    $medium->save();
                }
             }
        } elseif (isset($inputMedium['action']) && $inputMedium['action'] === 'add') {
            $mediumFileFullPath = $inputMedium['path'];
            $this->addMediaFromUrl($mediumFileFullPath)
                ->withCustomProperties($inputMedium['meta_data'])
                ->toMediaCollection($mediaCollection->getName(), $mediaCollection->getDisk());}
    }
}
