<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersBasedLocation extends Model
{
    use HasFactory;

    public $table = 'users_based_location';
}
