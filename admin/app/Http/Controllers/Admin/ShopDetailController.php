<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use DB;
use App\Models\AdminUsers;
use App\Models\AdminVendor;

use App\Http\Requests\Admin\AdminUser\UpdateAdminUser;

class ShopDetailController extends Controller
{
    public $adminUser;

    /**
     * Guard used for admin user
     *
     * @var string
     */
    protected $guard = 'admin';

    public function __construct()
    {
        // TODO add authorization
        $this->guard = config('admin-auth.defaults.guard');
    }

    /**
     * Get logged user before each method
     *
     * @param Request $request
     */
    protected function setUser($request)
    {
        if (empty($request->user($this->guard))) {
            abort(404, 'Admin User not found');
        }

        return ($this->adminUser = $request->user($this->guard));
    }

    /**
     * Show the form for editing logged user profile.                
     *
     * @param Request $request
     * @return Factory|View
     */
    public function editshopdetail(Request $adminUser)
    {
        $data=$this->setUser($adminUser);
        $dataadmin = AdminUsers::join('admin_vendors','admin_vendors.user_id','=','admin_users.id')->where('email', $data->email)->first();

        // print_r($dataadmin);die;
        
        return view('admin.shop-detail.edit-shop-detail', [
            'editshop' => json_encode($dataadmin),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @throws ValidationException
     * @return array|RedirectResponse|Redirector
     */
    public function updateshopdetail(Request $request)
    {

        $this->setUser($request);
        $adminUser = $this->adminUser;

        // Validate the request
        $this->validate($request, [
            'shop_name'=>['sometimes','required'],
            'shop_address'=>['sometimes','required'],
            'description'=>['required']
        ]);

        AdminVendor::where('user_id',$this->adminUser->getKey())->update([
            'shop_name'=>$request->input('shop_name'),
            'shop_address'=>$request->input('shop_address'),
            'description'=>$request->input('description'),
            'lat'=>$request->input('lat'),
            'lng'=>$request->input('lng')
        ]);

        if ($request->ajax()) {
            return ['redirect' => url('admin/shop-detail'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/shop-detail')->with('status',trans('brackets/admin-ui::admin.operation.succeeded'));
    }
}
