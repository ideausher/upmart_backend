<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminVendor;
use App\Models\BookingDetail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EarningsController extends Controller
{
    public function index(Request $request)
    {
        $data = '';
        $total_amount = '';
        $total = '';
        $x_row = [];
        $amount = [];

        $request->from_date = $request->from_date ? $request->from_date : Carbon::now()->addDays(-30)->format('Y-m-d');
        $request->to_date = $request->to_date ? $request->to_date : Carbon::now()->format('Y-m-d');

        $user = Auth::user();

        

        if($user->hasRole('vendor')) {

            $shop = AdminVendor::where('user_id',$user->id)->first();
            $shopid = $shop->id;
            
            $col_name = 'vendor_amount';

            $data = BookingDetail::selectRaw('SUM(bs_amount_to_vendor_after_commi) as vendor_amount, created_at')->where('status',10)->whereDate('created_at','>=',$request->from_date)->where('shopId',$shopid)->whereDate('created_at','<=',$request->to_date)->groupBy(DB::raw('Date(created_at)'))->get()->toArray();
        
            $total_amount = BookingDetail::selectRaw('SUM(bs_amount_to_vendor_after_commi) as total_amount')->where('status',10)->whereDate('created_at','>=',$request->from_date)->where('shopId',$shopid)->whereDate('created_at','<=',$request->to_date)->get();
            $total = $total_amount[0]->total_amount;
        }
        else
        {
            $col_name = 'admin_amount';

            $data = BookingDetail::selectRaw('(SUM(as_total_amount_to_admin)+SUM(stripe_charges)) as admin_amount, created_at')->where('status',10)->whereDate('created_at','>=',$request->from_date)->whereDate('created_at','<=',$request->to_date)->groupBy(DB::raw('Date(created_at)'))->get()->toArray();
        
            $total_amount = BookingDetail::selectRaw('(SUM(as_total_amount_to_admin)+SUM(stripe_charges)) as total_amount')->where('status',10)->whereDate('created_at','>=',$request->from_date)->whereDate('created_at','<=',$request->to_date)->get();
            $total = $total_amount[0]->total_amount;
        }

        if(!$data)
            return view('admin.earnings.index',['data'=>$data,'total'=>$total,'x_row'=>$x_row,'amount'=>$amount,'request'=>$request]);
        
        foreach($data as $d)
        {
            $x_row[] = $d['created_at'];
            $amount[] = $d[$col_name];
        }

        return view('admin.earnings.index',['data'=>$data,'total'=>$total,'x_row'=>$x_row,'amount'=>$amount,'request'=>$request]);
    }
}
