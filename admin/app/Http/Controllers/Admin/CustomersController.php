<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\BulkDestroyUser;
use App\Http\Requests\Admin\User\DestroyUser;
use App\Http\Requests\Admin\User\IndexUser;
use App\Models\Customer;
use App\Models\UserAddress;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use App\Http\Controllers\Admin\Auth;

use App\Http\Requests\Admin\BookingDetail\IndexBookingDetail;
use App\Models\BookingDetail;

class CustomersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexUser $request
     * @return array|Factory|View
     */
    public function index(IndexUser $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Customer::class)->modifyQuery(function ($query) use ($request) {
            $query->where('user_type', 1)->with('customerrelation');
            
            })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title', 'name', 'social_id', 'social_type', 'email', 'email_verified_at', 'profile_picture', 'country_code', 'phone_number', 'country_iso_code', 'verified', 'user_type', 'is_block','referral_code','referred_by_code'],

            // set columns to searchIn
            ['id', 'title', 'name', 'social_id', 'social_type', 'email', 'profile_picture', 'country_code', 'phone_number', 'country_iso_code','referral_code','referred_by_code']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.customers.index', ['data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @throws AuthorizationException
     * @return void
     */
    public function show($user)
    {
        $customerdetail = Customer::with('customerrelation')->where('id',$user)->get();

        // print_r($customer);die;

        return view('admin.customers.detail',['details'=>$customerdetail]);
    }

    public function bookings_vendor(IndexBookingDetail $request)
    {
        $userId = $request->customerId;
        $data = BookingDetail::where('userId', $userId)->with(['users','deliveryboy','shop'])->orderBy('id','desc')->get();
        // create and AdminListing instance for a specific model and

        $s = $data->toArray();
        array_walk_recursive($s, function(&$v,$k){
            if($k=='bookingType')
            {
                $v = BookingDetail::LEVELS[$v];
            }
            if($k=='status')
            {
                $v = BookingDetail::ORDER_STATUS[$v];
            }
        });
        $data = collect($s);

        $corres = 'customers';

        return view('admin.booking-detail.bookings-vendor', ['data' => $data,'corr'=>$corres,'adminId'=>$userId]);
    }

    public function bookings_vendor_view($userid, $bookingid)
    {
        $booking = BookingDetail::where('userId',$userid)->where('id',$bookingid)->first();

        $totalGST = 0;
        if($booking->orderdetails){
            foreach($booking->orderdetails as $order){
                $totalGST += $order->gst_tax;
            }
        }

        $totalamount = $booking->amount_before_discount+$booking->platform_charge+$booking->tip_to_delivery_boy+$booking->delivery_charge_for_customer+$totalGST+$booking->pst+$booking->hst-$booking->discount_amount;
        if($booking)
        {
            $booking->totalamount = $totalamount;
            $booking->totalGST = $totalGST;

        }

        return view('admin.booking-detail.bookings-vendor-view', [
            'booking' => $booking,
            'parent'=>'customers',
            'userid' => $userid
        ]);
    }
}
