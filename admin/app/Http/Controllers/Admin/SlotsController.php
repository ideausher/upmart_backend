<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminUsers;
use App\Models\Slot;
use Illuminate\Http\Request;
use Brackets\AdminListing\Facades\AdminListing;

class SlotsController extends Controller
{
    public function index(Request $request)
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $user = AdminUsers::with('vendorrelationship')->where('id', $user->id)->first();
        $data = Slot::where('shop_id',$user->vendorrelationship->id)->orderBy('slot_from', 'ASC')->get();

        $shop_id = $user->vendorrelationship->id;
// dd($data);
        return view('admin.slots.index', ['data' => $data,'shop_id'=>$shop_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSlot $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        // return response()->json($request->all());
        // Sanitize input
        $sanitized = $request->all();

        // Store the Slot
        $slot = Slot::create($sanitized);
        // return response()->json($slot);

        // if ($request->ajax()) {
        //     return ['redirect' => url('admin/slots'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        // }

        $lastInsertId = $slot->id;
        if ($lastInsertId) {

            $array = array();
            $array['success'] = true;
            $array['slot_id'] = $lastInsertId;
            $array['slot_day'] = $slot->day;
            $array['slot_from'] = date('H:i a', strtotime($slot->slot_from));
            $array['slot_to'] = date('H:i a', strtotime($slot->slot_to));
            $array['singleslot'] = true;
            $array['message'] = "Slots Added successfully";
            return json_encode($array);
            // }

        }
        return redirect('admin/slots');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSlot $request
     * @param Slot $slot
     * @return array|RedirectResponse|Redirector
     */
    public function update(Request $request)
    {
        $slot = Slot::findOrFail($request->id);

        // return response()->json($slot);
        // Sanitize input
        $sanitized = $request->all();

        // Update changed values Slot
        // ;
        $slot->update($sanitized);
        $lastInsertId = $slot->id;
        if ($lastInsertId) {

            $array = array();
            $array['success'] = true;
            $array['slot_id'] = $lastInsertId;
            $array['slot_day'] = $slot->day;
            $array['slot_from'] = date('H:i a', strtotime($slot->slot_from));
            $array['slot_to'] = date('H:i a', strtotime($slot->slot_to));
            $array['singleslot'] = true;
            $array['message'] = "Slots Added successfully";
            return json_encode($array);
            // }

        }
        if ($request->ajax()) {
            return [
                'redirect' => url('admin/slots'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/slots');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySlot $request
     * @param Slot $slot
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(Request $request)
    {
        $id = $request->route('id');
        $er = Slot::findOrFail($id)->delete();
        if ($request->ajax()) {
            return response()->json(['success' => $er]);
        }

        //    return response()->json($slot);

        return redirect()->back();
    }

    public function checkOverlappingSlots(Request $request)
    {
        $data = $request->all();
        // return response()->json($data);

        //case 1 Example: existing slot 5:00 - 6:00, new slot 4:30 - 5:30  //Overlapping from left side of existing slot
        $overlap_slots = Slot::where('day', $data['day'])->where('slot_from', '>', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '>', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 2 Example: existing slot 5:00 - 6:00, new slot 5:30 - 6:30  //Overlapping from right side of existing slot
        ->orWhere('day', $data['day'])->where('slot_from', '<', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '<', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 3 Example: existing slot 5:00 - 6:00, new slot 4:30 - 6:30  //Existing slot within new slot
        ->orWhere('day', $data['day'])->where('slot_from', '>', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '<', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 4 Example: existing slot 5:00 - 6:00, new slot 5:15 - 5:45  //New Slot within existing slot
        ->orWhere('day', $data['day'])->where('slot_from', '<', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '>', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 5 Example: existing slot 5:00 - 6:00, new slot 5:00 - 7:00  //Start Time same, end time of new slot greater than existing slot
        ->orWhere('day', $data['day'])->where('slot_from', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '<', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 6 Example: existing slot 5:00 - 6:00, new slot 5:00 - 5:30  //Start time same, end time of new slot smaller than existing slot
        ->orWhere('day', $data['day'])->where('slot_from', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', '>', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 7 Example: existing slot 5:00 - 6:00, new slot 4:30 - 6:00  //End time same, start time of new slot smaller than existing slot
        ->orWhere('day', $data['day'])->where('slot_from', '>', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', $data['slot_to'])->where('shop_id', $data['shop_id'])
        //case 8 Example: existing slot 5:00 - 6:00, new slot 5:30 - 6:00  //End time same, start time of new slot greater than existing slot
        ->orWhere('day', $data['day'])->where('slot_from', '<', $data['slot_from'])->where('slot_from', '<', $data['slot_to'])->where('slot_to', '>', $data['slot_from'])->where('slot_to', $data['slot_to'])->where('shop_id', $data['shop_id'])
        ->count();

        if ($overlap_slots) {
            $array['overlap_slots_exists'] = true;
        } else {
            $array['overlap_slots_exists'] = false;
        }

        $same_slot = Slot::where('shop_id', $data['shop_id'])->where('day', $data['day'])->where('slot_from', $data['slot_from'])->where('slot_to', $data['slot_to'])->count();
        if ($same_slot) {
            $array['same_slot_exists'] = true;
        } else {
            $array['same_slot_exists'] = false;
        }

        echo json_encode($array);

    }
}
