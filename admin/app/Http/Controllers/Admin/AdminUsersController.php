<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Http\Requests\Admin\AdminUser\DestroyAdminUser;
use App\Http\Requests\Admin\AdminUser\ImpersonalLoginAdminUser;
use App\Http\Requests\Admin\AdminUser\IndexAdminUser;
use App\Http\Requests\Admin\AdminUser\StoreAdminUser;
use App\Http\Requests\Admin\AdminUser\UpdateAdminUser;
use App\Http\Requests\Admin\BookingDetail\IndexBookingDetail;
// use Brackets\AdminAuth\Models\AdminUser;
use Spatie\Permission\Models\Role;
use Brackets\AdminAuth\Activation\Facades\Activation;
use Brackets\AdminAuth\Services\ActivationService;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Config;

use App\Models\AdminVendor;
use App\Models\AdminUsers;
use App\Models\BlockReason;
use Mail;
use App\Mail\sendmailtovendor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail as FacadesMail;

use App\Models\BookingDetail;
use App\Models\Item;
use App\Models\Review;
use App\Mail\Vendor;
use App\Models\Slot;
use Illuminate\Support\Facades\Log;

class AdminUsersController extends Controller
{

    /**
     * Guard used for admin user
     *
     * @var string
     */
    protected $guard = 'admin';

    /**
     * AdminUsersController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->guard = config('admin-auth.defaults.guard');
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexAdminUser $request
     * @return Factory|View
     */
    public function index(IndexAdminUser $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AdminUsers::class)->modifyQuery(function ($query) use ($request) {
                $query = $query->leftJoin('admin_vendors','admin_vendors.user_id','=','admin_users.id')->with(['vendorrelationship','mediaData']);
            })->processRequestAndGet(
                $request,

            // set columns to query
            ['id', 'first_name', 'last_name', 'email','country_code','phonenumber_with_countrycode', 'activated', 'forbidden', 'language', 'last_login_at'],

            // set columns to searchIn
            ['id', 'first_name','email','country_code','phonenumber_with_countrycode','admin_vendors.shop_name','admin_vendors.country_code','admin_vendors.phonenumber_with_countrycode'],

            // function ($query) use ($request) {
            //     $query->with(['vendorrelationship','mediaData']);
            //     // $query = $query->whereHas('vendorrelationship',function($query) use($request){
            //     //     $query = $query->orWhere('shop_name', 'like %' . $request['search'] .'%');
            //     // });
            // }
        );
        // $data = AdminUsers::with(['vendorrelationship','mediaData'])->leftJoin('admin_vendors', function($join) {
        //     $join->on('admin_vendors.user_id', '=', 'admin_users.id');
        //   })->WhereRaw("(
        //     admin_vendors.shop_name like '%" . $request['search']."%'  or
        //     admin_users.first_name like '%" . $request['search']."%' or
        //     admin_users.last_name like '%" . $request['search']."%' or
        //     admin_users.email like '%" . $request['search']."%' or
        //     admin_users.id like '%" . $request['search']."%'
        //     )")
        // ->get();

        foreach($data as $post){
            $mediaData = $post->getMedia('Gallery');
            foreach($mediaData as $md){
                
                $publicUrl = $md->getUrl();
                $post->mediaUrl = $publicUrl;
            }
        }

        if ($request->ajax()) {
            return ['data' => $data, 'activation' => Config::get('admin-auth.activation_enabled')];
        }
         
        return view('admin.admin-user.index', ['data' => $data, 'activation' => Config::get('admin-auth.activation_enabled')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {

        $this->authorize('admin.admin-user.create');
        // dd( json_encode((object)  Role::where('guard_name', $this->guard)->get()));
        return view('admin.admin-user.create', [
            'activation' => Config::get('admin-auth.activation_enabled'),
            'roles' => json_encode((object)  Role::all()),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAdminUser $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAdminUser $request)
    {

        $sanitized = $request->getModifiedData();

        $request['roles'] = array ([
            "id" => 4,
            "name" => "vendor",
            "guard_name" => "admin",
            "created_at" => "2020-12-22 11:09:24",
            "updated_at" =>  "2020-12-22 11:09:24"
        ]);

        if(isset($sanitized['first_name']))
            $sanitized['first_name'] = ucfirst($sanitized['first_name']);

        if(isset($sanitized['last_name']))
            $sanitized['last_name'] = ucfirst($sanitized['last_name']);


        $VendArr = ['first_name'=>$sanitized['first_name'],'last_name'=>$sanitized['last_name'],'email'=>$sanitized['email'],'country_code'=>$sanitized['country_code'],'phonenumber_with_countrycode'=>$sanitized['phonenumber_with_countrycode'],'password'=>$sanitized['password'],'activated'=>1,'country_iso_code' => $sanitized['country_iso_code']];
       
        // echo "<pre>";
        // print_r($VendArr);die;
        

        if(isset($request->shop_name))
            $request->shop_name = ucfirst($request->shop_name);
        
        // Store the AdminUser
        $adminUser = AdminUsers::create($VendArr);

        // $request->platform_charges = 0.00;
        // $request->commission_charges = 0.00;

        $adminUser->vendorrelationship()->create([
        'country_code'=>$request->country_code,
       'phonenumber_with_countrycode'=>$request->phonenumber_with_countrycode,
       'shop_name'=>$request->shop_name,
       'description'=>$request->description,
       'lng'=>$request->lng,
       'lat'=>$request->lat,
       'shop_type'=>$request->shop_type,
       'platform_charges'=>$request->platform_charges,
       'commission_charges'=>$request->commission_charges,
       'gst'=>$request->gst,
       'pst'=>$request->pst,
       'hst'=>$request->hst,
       'shop_address'=>$request->shop_address,'additional_address'=>$request->additional_address]);
        
       $timefrom = ['0:00','1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];

       $timeto = ['1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','0:00'];

        $day = [1,2,3,4,5,6,7];
        
        foreach($day as $k=>$v)
        {
            Slot::create([
                'shop_id'=>$adminUser->vendorrelationship->id,
                'day'=>$v,
                'slot_from'=>"0:00",
                'slot_to'=>"23:59"
            ]);
        }
    
       $data = [
           'name'=>$sanitized['first_name'].' '.$sanitized['last_name'],
           'email'=>$sanitized['email'],
           'password'=>$request->password,
           'vendor'=>1
       ];

        // But we do have a roles, so we need to attach the roles to the adminUser
        $adminUser->roles()->sync(collect($request->input('roles'))->map->id->toArray());
        DB::statement("UPDATE model_has_roles SET model_type = ? where model_id = ?",["Brackets\\AdminAuth\\Models\\AdminUser", $adminUser->id]);

        if($sanitized['email'])
        {
                Mail::to([$sanitized['email']])->send(new Vendor($data));
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/admin-users'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/admin-users');
    }

    /**
     * Display the specified resource.
     *
     * @param AdminUser $adminUser
     * @throws AuthorizationException
     * @return void
     */
    public function show(AdminUsers $adminUser)
    {
        $this->authorize('admin.admin-user.show', $adminUser);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AdminUser $adminUser
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AdminUsers $adminUser)
    {
        // $userShopDetails = json_encode(array_merge(['vendorrelationship' => $adminUser->vendorrelationship],
        //  json_decode($adminUser, true)
        // ));

        $this->authorize('admin.admin-user.edit', $adminUser);

        $adminUser->load('roles');

        $shopid = $adminUser->vendorrelationship->id;

        $rating = Review::selectRaw("AVG(rating) as avg_rating")->where('shop_id',$shopid)->get();


        return view('admin.admin-user.edit', [
            'adminUser' => $this->getData($adminUser),
            'activation' => Config::get('admin-auth.activation_enabled'),
            'roles' => Role::where('guard_name', $this->guard)->get(),
            'rating'=>$rating[0]->avg_rating

        ]);
    }
    private function getData($adminUser)
    {
        
        $adminUser->country_code=$adminUser->vendorrelationship->country_code;
        $adminUser->phonenumber_with_countrycode=$adminUser->vendorrelationship->phonenumber_with_countrycode;
        $adminUser->shop_name=$adminUser->vendorrelationship->shop_name;
        $adminUser->shop_address=$adminUser->vendorrelationship->shop_address;
        $adminUser->additional_address=$adminUser->vendorrelationship->additional_address;
        $adminUser->description=$adminUser->vendorrelationship->description;
        $adminUser->lat=$adminUser->vendorrelationship->lat;
        $adminUser->lng=$adminUser->vendorrelationship->lng;
        $adminUser->shop_type=$adminUser->vendorrelationship->shop_type;
        $adminUser->commission_charges=$adminUser->vendorrelationship->commission_charges;
        $adminUser->platform_charges=$adminUser->vendorrelationship->platform_charges;
        $adminUser->gst=$adminUser->vendorrelationship->gst;
        $adminUser->pst=$adminUser->vendorrelationship->pst;
        $adminUser->hst=$adminUser->vendorrelationship->hst;

        return $adminUser;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAdminUser $request
     * @param AdminUser $adminUser
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAdminUser $request, AdminUsers $adminUser)
    {
      
        // Sanitize input
        $sanitized = $request->getModifiedData();
        $request['roles'] = array ([
            "id" => 4,
            "name" => "vendor",
            "guard_name" => "admin",
            "created_at" => "2020-12-22 11:09:24",
            "updated_at" =>  "2020-12-22 11:09:24"
        ]);

        if(isset($sanitized['first_name']))
            $sanitized['first_name'] = ucfirst($sanitized['first_name']);

        if(isset($sanitized['last_name']))
            $sanitized['last_name'] = ucfirst($sanitized['last_name']);
        
        $VendArr = ['first_name'=>$sanitized['first_name'],'last_name'=>$sanitized['last_name'],'email'=>$sanitized['email'],'country_code'=>$sanitized['country_code'],'phonenumber_with_countrycode'=>$sanitized['phonenumber_with_countrycode'],'password'=>$sanitized['password'],'activated'=>1,'country_iso_code' => $sanitized['country_iso_code']];

        // echo "<pre>";
        // print_r($VendArr);die;
// dd($sanitized);

        if(isset($request->shop_name))
            $request->shop_name = ucfirst($request->shop_name);
            // dd($request->all());
        // Update changed values AdminUser
        $adminUser->update($VendArr);
        
        // $request->platform_charges = 0.00;
        // $request->commission_charges = 0.00;
        
        $adminUser->vendorrelationship()->update([
            'country_code'=>$request->country_code,
            'phonenumber_with_countrycode'=>$request->phonenumber_with_countrycode,
            'shop_name'=>$request->shop_name,
            'shop_address'=>$request->shop_address,
            'description'=>$request->description,
            'lng'=>$request->lng,
            'lat'=>$request->lat,
            'shop_type'=>$request->shop_type,
            'platform_charges'=>$request->platform_charges,
            'commission_charges'=>$request->commission_charges,
            'gst'=>$request->gst,
            'pst'=>$request->pst,
            'hst'=>$request->hst,
            'additional_address'=>$request->additional_address]);

        if(!$request->gst || $request->gst==0.00)
        {
            $allitems = $adminUser->vendorrelationship->items;

            if($allitems)
            {
                foreach($allitems as $item)
                {
                    $product = Item::find($item->id);
                    if($product->gst_enable==1)
                        $product->gst_enable = 0;

                    $product->save();
                }
            }
        }


            $data = [
                'name'=>$sanitized['first_name'].' '.$sanitized['last_name'],
                'email'=>$sanitized['email'],
                'password'=>$request->password,
                'vendor'=>0
            ];
     
            if($sanitized['email'])
            {
                Mail::to([$sanitized['email']])->send(new Vendor($data));
            }


        // But we do have a roles, so we need to attach the roles to the adminUser
        if ($request->input('roles')) {
            // $adminUser->roles()->sync(collect($request->input('roles', []))->map->id->toArray());
            // DB::statement("UPDATE model_has_roles SET model_type = ? where model_id = ?",["Brackets\\AdminAuth\\Models\\AdminUser", $adminUser->id]);
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/admin-users'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/admin-users');
    }

    /** 
     * Remove the specified resource from storage.
     *
     * @param DestroyAdminUser $request
     * @param AdminUser $adminUser
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAdminUser $request, AdminUsers $adminUser)
    {
        // $adminUser->forceDelete();
        $adminUser->delete();

        $adminUser->vendorrelationship()->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Resend activation e-mail
     *
     * @param Request $request
     * @param ActivationService $activationService
     * @param AdminUser $adminUser
     * @return array|RedirectResponse
     */
    public function resendActivationEmail(Request $request, ActivationService $activationService, AdminUsers $adminUser)
    {
        if (Config::get('admin-auth.activation_enabled')) {
            $response = $activationService->handle($adminUser);
            if ($response == Activation::ACTIVATION_LINK_SENT) {
                if ($request->ajax()) {
                    return ['message' => trans('brackets/admin-ui::admin.operation.succeeded')];
                }

                return redirect()->back();
            } else {
                if ($request->ajax()) {
                    abort(409, trans('brackets/admin-ui::admin.operation.failed'));
                }

                return redirect()->back();
            }
        } else {
            if ($request->ajax()) {
                abort(400, trans('brackets/admin-ui::admin.operation.not_allowed'));
            }

            return redirect()->back();
        }
    }

    /**
     * @param ImpersonalLoginAdminUser $request
     * @param AdminUser $adminUser
     * @return RedirectResponse
     * @throws  AuthorizationException
     */
    public function impersonalLogin(ImpersonalLoginAdminUser $request, AdminUsers $adminUser)
    {   
       
        Auth::login($adminUser);
        return redirect()->back();
    }

    public function adminUserBlockEmail(Request $request)
    {

        $user =AdminUsers::find($request->userid);

        $res_done=BlockReason::updateOrCreate(
            ['user_id' => $request->userid],
            ['reason' => $request->reason]
        );

        $b_done = $user->update(['forbidden' => 1]);

        $user->vendorrelationship()->update(['forbidden' => 1]);
        // $data=array(
        //     'reason' =>$request->reason
        // );
        $data=array(
            'vendor' => $user->first_name." ".$user->last_name ,
            'reason' =>$request->reason,
            'block'=>1
        );

        // $data=array(
        //     'reason' =>$request->reason
        // );
        if (!empty($user->email)) {
            Mail::to([$user->email])->send(new sendmailtovendor($data));
        }

        return redirect('admin/admin-users');

    }

    public function adminUserUnBlockEmail(Request $request, AdminUsers $adminUser) {
        $user =AdminUsers::find($request->userid);
        $b_done = $user->update(['forbidden' => 0]);

        $user->vendorrelationship()->update(['forbidden' => 0]);

        $data=array(
            'vendor' => $user->first_name." ".$user->last_name ,
            'reason' =>$request->reason,
            'block'=>0
        );

        // $data=array(
        //     'reason' =>$request->reason
        // );
        if (!empty($user->email)) {
            Mail::to([$user->email])->send(new sendmailtovendor($data));
        }

        if($b_done) {
            return redirect('admin/admin-users');
        }
    }

    public function items(AdminUsers $adminUser)
    {
        $shopId = '';
        if (!empty($adminUser->vendorrelationship)) {
            $shopId = $adminUser->vendorrelationship->id;
            
        }
        $items = Item::with('mediaData')->where('shop_id',$shopId)->get();
        // echo "<pre>";
        // print_r($items);
        // die;


        foreach($items as $post){
            if($post->mediaData){
               $mediaData = $post->mediaData;
               $publicUrl = $mediaData->getUrl();
               $post->mediaUrl = $publicUrl;      
            }
         }

        return view('admin.admin-user.items.index',['items'=>$items,'adminId'=>$adminUser->id]);
    }

    public function itemdetails(AdminUsers $adminUser, Item $item)
    {
        $shopId = $adminUser->vendorrelationship->id;
        $items = Item::with('mediaData')->where('id',$item->id)->where('shop_id',$shopId)->get();

        // dd($items);

        $imgs = [];

        foreach($items as $post){
            $mediaData = $post->getMedia('product_images');
            foreach($mediaData as $md){
                
                $publicUrl = $md->getUrl();
                $imgs[] = $publicUrl;
            }
        }

        return view('admin.admin-user.items.view',['details'=>$items,'imgs'=>$imgs,'adminId'=>$adminUser->id]);
    }

    public function bookings_vendor(IndexBookingDetail $request)
    {
        $vendorId = $request->adminUser;
        $shop_id = AdminVendor::where('user_id', $vendorId)->pluck('id')->first();

        $data = BookingDetail::where('shopId', $shop_id)->with(['users','deliveryboy','shop'])->orderBy('id','desc')->get();
        // create and AdminListing instance for a specific model and
        

        $s = $data->toArray();
        array_walk_recursive($s, function(&$v,$k){
            if($k=='bookingType')
            {
                $v = BookingDetail::LEVELS[$v];
            }
            if($k=='status')
            {
                $v = BookingDetail::ORDER_STATUS[$v];
            }
        });
        $data = collect($s);

        $corres = 'admin-users';

        return view('admin.booking-detail.bookings-vendor', ['data' => $data,'corr'=>$corres,'adminId'=>$vendorId]);
    }

    public function bookings_vendor_view($vendorid, $bookingid)
    {
        $shopid = AdminVendor::select('id')->where('user_id',$vendorid)->first();
        
        $booking = BookingDetail::where('shopId',$shopid->id)->where('id',$bookingid)->first();

        $totalGST = 0;
        if($booking->orderdetails){
            foreach($booking->orderdetails as $order){
                $totalGST += $order->gst_tax;
            }
        }

        $totalamount = $booking->amount_before_discount+$booking->platform_charge+$booking->tip_to_delivery_boy+$booking->delivery_charge_for_customer+$totalGST+$booking->pst+$booking->hst-$booking->discount_amount;
        if($booking)
        {
            $booking->totalamount = $totalamount;
            $booking->totalGST = $totalGST;

        }

        return view('admin.booking-detail.bookings-vendor-view', [
            'booking' => $booking,
            'parent'=>'admin-users',
            'userid' => $vendorid
        ]);
    }
}
