<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\View\View;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Models\TableBannersLocationWise;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\TableBannersLocationWise\IndexTableBannersLocationWise;
use App\Http\Requests\Admin\TableBannersLocationWise\StoreTableBannersLocationWise;
use App\Http\Requests\Admin\TableBannersLocationWise\UpdateTableBannersLocationWise;
use App\Http\Requests\Admin\TableBannersLocationWise\DestroyTableBannersLocationWise;
use App\Http\Requests\Admin\TableBannersLocationWise\BulkDestroyTableBannersLocationWise;

class TableBannersLocationWiseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTableBannersLocationWise $request
     * @return array|Factory|View
     */
    public function index(IndexTableBannersLocationWise $request)
    {
        
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TableBannersLocationWise::class)->modifyQuery(function($query) use ($request){
            $query = $query->with('mediaData');
            
            if(Auth::user()->hasRole('vendor')){
                $query = $query->where('merchant_id', Auth::user()->id); 
            }
            else{
                $query = $query->whereNull('merchant_id'); 
            }
        })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'bannerName', 'link', 'address', 'latitude', 'longitude'],

            // set columns to searchIn
            ['id', 'bannerName', 'link', 'address', 'latitude', 'longitude']
        );

        foreach($data as $post){
            $mediaData = $post->getMedia('banner_image');
            foreach($mediaData as $md){
                
                $publicUrl = $md->getUrl();
                $post->mediaUrl = $publicUrl;
            }
        }

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        

        return view('admin.table-banners-location-wise.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.table-banners-location-wise.create');

        return view('admin.table-banners-location-wise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTableBannersLocationWise $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTableBannersLocationWise $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if(isset($sanitized['bannerName']))
            $sanitized['bannerName'] = ucfirst($sanitized['bannerName']);
        if( Auth::user()->hasRole('vendor')){
            $sanitized['merchant_id'] = Auth::user()->id;
        }
        // Store the TableBannersLocationWise
        $tableBannersLocationWise = TableBannersLocationWise::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/table-banners-location-wises'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/table-banners-location-wises');
    }

    /**
     * Display the specified resource.
     *
     * @param TableBannersLocationWise $tableBannersLocationWise
     * @throws AuthorizationException
     * @return void
     */
    public function show(TableBannersLocationWise $tableBannersLocationWise)
    {
        $this->authorize('admin.table-banners-location-wise.show', $tableBannersLocationWise);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TableBannersLocationWise $tableBannersLocationWise
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TableBannersLocationWise $tableBannersLocationWise)
    {
        $this->authorize('admin.table-banners-location-wise.edit', $tableBannersLocationWise);


        return view('admin.table-banners-location-wise.edit', [
            'tableBannersLocationWise' => $tableBannersLocationWise,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTableBannersLocationWise $request
     * @param TableBannersLocationWise $tableBannersLocationWise
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTableBannersLocationWise $request, TableBannersLocationWise $tableBannersLocationWise)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if(isset($sanitized['bannerName']))
            $sanitized['bannerName'] = ucfirst($sanitized['bannerName']);
            
        if(Auth::user()->hasRole('vendor')){
            $sanitized['merchant_id'] = Auth::user()->id;
        }
        // Update changed values TableBannersLocationWise
        $tableBannersLocationWise->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/table-banners-location-wises'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/table-banners-location-wises');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTableBannersLocationWise $request
     * @param TableBannersLocationWise $tableBannersLocationWise
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTableBannersLocationWise $request, TableBannersLocationWise $tableBannersLocationWise)
    {
        $tableBannersLocationWise->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTableBannersLocationWise $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTableBannersLocationWise $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TableBannersLocationWise::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
