<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StripeToken\BulkDestroyStripeToken;
use App\Http\Requests\Admin\StripeToken\DestroyStripeToken;
use App\Http\Requests\Admin\StripeToken\IndexStripeToken;
use App\Http\Requests\Admin\StripeToken\StoreStripeToken;
use App\Http\Requests\Admin\StripeToken\UpdateStripeToken;
use App\Models\StripeToken;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class StripeTokenController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexStripeToken $request
     * @return array|Factory|View
     */
    public function index(IndexStripeToken $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(StripeToken::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'vendor_id'],

            // set columns to searchIn
            ['id', 'stripe_token']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.stripe-token.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.stripe-token.create');

        return view('admin.stripe-token.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStripeToken $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreStripeToken $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the StripeToken
        $stripeToken = StripeToken::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/stripe-tokens'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/stripe-tokens');
    }

    /**
     * Display the specified resource.
     *
     * @param StripeToken $stripeToken
     * @throws AuthorizationException
     * @return void
     */
    public function show(StripeToken $stripeToken)
    {
        $this->authorize('admin.stripe-token.show', $stripeToken);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param StripeToken $stripeToken
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(StripeToken $stripeToken)
    {
        $this->authorize('admin.stripe-token.edit', $stripeToken);


        return view('admin.stripe-token.edit', [
            'stripeToken' => $stripeToken,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStripeToken $request
     * @param StripeToken $stripeToken
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateStripeToken $request, StripeToken $stripeToken)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values StripeToken
        $stripeToken->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/stripe-tokens'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/stripe-tokens');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyStripeToken $request
     * @param StripeToken $stripeToken
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyStripeToken $request, StripeToken $stripeToken)
    {
        $stripeToken->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyStripeToken $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyStripeToken $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    StripeToken::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
