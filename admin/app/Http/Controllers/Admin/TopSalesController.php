<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminVendor;
use App\Models\Item;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopSalesController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $vendorid = '';
        if($user->hasRole('vendor'))
        {
            $vendorid = $user->id;

            $role = 1;

            $shop = AdminVendor::select('id')->where('user_id',$vendorid)->first();
            $items = Item::where('shop_id',$shop->id)->get();

            if($items)
            {
                $sales = [];
                foreach($items as $item)
                {
                    $sales[] = OrderDetail::selectRaw("id, productId, productName, productDescription,SUM(quantity) as totalquantity, new_price")->where('productId',$item->id)->groupBy('productId')->get()->toArray();
                }
            }
        }
        else
        {
            $role = 0;

            $sales = OrderDetail::selectRaw("id, productId, productName, productDescription,SUM(quantity) as totalquantity, new_price")->groupBy('productId')->get();
        }

        return view('admin.top-sales.index',['sales'=>$sales,'role'=>$role]);
    }
}
