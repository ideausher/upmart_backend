<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Faq\BulkDestroyFaq;
use App\Http\Requests\Admin\Faq\DestroyFaq;
use App\Http\Requests\Admin\Faq\IndexFaq;
use App\Http\Requests\Admin\Faq\StoreFaq;
use App\Http\Requests\Admin\Faq\UpdateFaq;
use App\Models\Faq;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

use App\Models\FaqCategory;

class FaqController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexFaq $request
     * @return array|Factory|View
     */
    public function index(IndexFaq $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Faq::class)->modifyQuery(function($query) use ($request){
            $query->with('category')->groupBy('category_id');
           
        })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'category_id'],

            // set columns to searchIn
            ['id', 'question', 'answer']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.faq.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.faq.create');

        $faqCat = FaqCategory::all();

        return view('admin.faq.create',compact('faqCat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFaq $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreFaq $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        
        $cat = $sanitized['category']['id'];

        $sanitized = array_merge($sanitized,['category_id'=>$cat]);
        // print_r($sanitized);die;

        // Store the Faq
        if(!empty($sanitized['questions']) && !empty($sanitized['answers']))
        {
            $i=0;
            foreach($sanitized['questions'] as $question)
            {
                if(!empty($question) && !empty($sanitized['answers'][$i]))
                {
                    $sanitized['question'] = $question;
                    $sanitized['answer'] = $sanitized['answers'][$i];
                    $i++;
                    // echo "<pre>";print_r($sanitized);
                    $faq = Faq::create($sanitized);
                }
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/faqs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/faqs');
    }

    /**
     * Display the specified resource.
     *
     * @param Faq $faq
     * @throws AuthorizationException
     * @return void
     */
    public function show(Faq $faq)
    {
        $this->authorize('admin.faq.show', $faq);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Faq $faq
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Faq $faq)
    {
        $this->authorize('admin.faq.edit', $faq);

        $faqCat = FaqCategory::all();

        $selcat = Faq::with('category')->where('category_id',$faq->category_id)->get()->toArray();

        $selcat = array_column($selcat,'category');

        $datapre = json_encode(array_merge(['category'=>$selcat],json_decode($faq, true)));

        $ss = json_decode($datapre);

        $qa = Faq::where('category_id',$ss->category_id)->get()->toArray();
        

        if($qa)
        {
            $question = [];
            $answers = [];
            foreach($qa as $aq)
            {
                $question[] = $aq['question'];
                $answers[] = $aq['answer'];
            }
        }

        $datapre = json_encode(array_merge(['questions'=>$question,'answers'=>$answers],json_decode($datapre, true)));
        // echo "<pre>";
        //         print_r($datapre);die;

        return view('admin.faq.edit', [
            'faq' => $faq,
            'faqCat'=>$faqCat,
            'datapre'=>$datapre
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFaq $request
     * @param Faq $faq
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateFaq $request, Faq $faq)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if(!empty($sanitized['category'][0]))
        {
            $cat_id = $sanitized['category'][0]['id'];
        }
        else
        {
            $cat_id = $sanitized['category']['id'];
        }
        
        $sanitized = array_merge($sanitized,['category_id'=>$cat_id]);

        // Update changed values Faq
        if(!empty($sanitized['questions']) && !empty($sanitized['answers']))
        {
            $i=0;
            Faq::where('category_id',$faq->category_id)->delete();
            foreach($sanitized['questions'] as $question)
            {
                if (!empty($question) && !empty($sanitized['answers'][$i]))
                {
                    $sanitized['question'] = $question;
                    $sanitized['answer'] = $sanitized['answers'][$i];
                    // $faq->update($sanitized);

                    Faq::create($sanitized);
                    $i++;
                    // echo "< pre>";print_r($sanitized);
                }
            }
        }
        // die;

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/faqs'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/faqs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyFaq $request
     * @param Faq $faq
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyFaq $request, Faq $faq)
    {
        // $faq->delete();

        Faq::where('category_id',$faq->category_id)->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyFaq $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyFaq $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Faq::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
