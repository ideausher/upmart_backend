<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminPasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Mail\ResetPassword;
use App\Models\AdminUsers;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\Admin\StoreResetPassword;
use App\Http\Requests\Admin\UpdateResetPassword;

class PasswordResetController extends Controller
{
    public function index()
    {
        return view('admin.password-reset.index');
    }

    public function send(StoreResetPassword $request)
    {

        $email = $request->email;

        $vendor = AdminUsers::where('email',$email)->get()->count();
        if($vendor>0){
            $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($chars), 0, 6);
            $token = md5($code);

            $rec = ['email'=>$email,'token'=>$token];
            $exist = AdminPasswordReset::where('email',$email)->get()->count();
            if ($exist==0) {
                $reset = AdminPasswordReset::create($rec);
            }
            else
            {
                $reset = AdminPasswordReset::where('email',$email)->update(['email'=>$email,'token'=>$token,'reset_status'=>0]);
            }

            $link = url('admin/password-reset').'/'.$token.'/reset';
            $first_name = AdminUsers::select('first_name')->where('email',$email)->get();

            $data=array(
                'link' =>$link,
                'vendor' => $email,
                'name' => $first_name[0]->first_name
            );

            Mail::to([$email])->send(new ResetPassword($data));
            
    
            return redirect('admin/password-reset')->with('status',trans('brackets/admin-auth::admin.passwords.sent'));
        }
        else
        {
            return back()->withErrors(['email' => "Entered E-Mail doesn't match with our records."]);
        }
    }

    public function reset($token)
    {
        $oldtoken = AdminPasswordReset::where('token',$token)->get()->count();
        
        if($oldtoken>0)
        {
            $exist = AdminPasswordReset::select('reset_status')->where('token',$token)->get();
            $reset_status = $exist[0]->reset_status;
            if ($reset_status==0) {
                return view('admin.password-reset.reset', ['token'=>$token]);
            }
            else
            {
                return 'Password Reset Link Expired!';
            }
        }
        else
        {
            return 'Invalid Password Reset Link!';
        }
    }

    public function update(UpdateResetPassword $request)
    {
        $token = $request->token;
        $email = AdminPasswordReset::select('email')->where('token',$token)->get();

        $update_status = AdminPasswordReset::where('token',$token)->update(['reset_status'=>1]);
        
        if ($update_status) {
            $user = AdminUsers::where('email', $email[0]->email)->first();
            $user->password = Hash::make($request->password);
            $user->save();
        }

        return redirect('/admin/login')->with('status','Password Reset Successfully!');
    }
}
