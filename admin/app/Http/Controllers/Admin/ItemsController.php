<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Item\BulkDestroyItem;
use App\Http\Requests\Admin\Item\DestroyItem;
use App\Http\Requests\Admin\Item\IndexItem;
use App\Http\Requests\Admin\Item\StoreItem;
use App\Http\Requests\Admin\Item\UpdateItem;
use App\Models\Item;
use App\Models\Category;
use App\Models\AdminVendor;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexItem $request
     * @return array|Factory|View
     */
    public function index(IndexItem $request)
    {
        // create and AdminListing instance for a specific model and
        
        if($request->search)
        {
            $data = AdminListing::create(Item::class)->modifyQuery(function($query) use ($request){
                $query->with('mediaData');
            })->processRequestAndGet(
                // pass the request with params
                $request,
    
                // set columns to query
                ['id', 'product_name', 'company_name', 'category_name', 'product_description', 'stock_quantity', 'price', 'product_image','shop_id','shop_name','category_id','is_disabled','discount','new_price'],
    
                // set columns to searchIn
                ['product_name']
            );
        }
        else
        {
            $data = AdminListing::create(Item::class)->modifyQuery(function($query) use ($request){
                $userId = Auth::user()->id;
                $shop_id = AdminVendor::where('user_id',$userId)->pluck('id')->first();
                $query = $query->where('shop_id',$shop_id);
                $query->with('mediaData');
            })->processRequestAndGet(
                // pass the request with params
                $request,
    
                // set columns to query
                ['id', 'product_name', 'company_name', 'category_name', 'product_description', 'stock_quantity', 'price','shop_id','shop_name','category_id','is_disabled','discount','new_price'],
    
                // set columns to searchIn
                ['product_name']
            );
        }

        $userId = Auth::user()->id;
        $shop_id = AdminVendor::where('user_id',$userId)->pluck('id')->first();
        
        foreach($data as $key => $post){
            
            $mediaData = $post->getMedia('product_images');
            $data[$key]['shopid'] = $shop_id;
            foreach($mediaData as $md){
                $publicUrl = $md->getUrl();
                $post->mediaUrl = $publicUrl;
            }
        }

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            
            if(count($data) > 0)
                $data[0]['searchValue'] = $request->search;
            return ['data' => $data];
        }

        if(count($data) > 0)
            $data[0]['searchValue'] = "";
        
        return view('admin.item.index', ['data' => $data,'shopid'=>$shop_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.item.create');
        
        $user_id = Auth::user()->id;

        $shop_id = AdminVendor::where('user_id',$user_id)->pluck('id')->first();

        $shop_name = AdminVendor::where('id',$shop_id)->pluck('shop_name')->first();
        
        $category=Category::all();

        $company_data=DB::table('companies')->distinct()->pluck('company_name');
        $company_values=[];
        foreach ($company_data as $company_name => $value) {
            $company_values[]=$value;
        }

        $data = ['shop_id'=>$shop_id,'shop_name'=>$shop_name];

        return view('admin.item.create',['category'=>$category,
        'company_values'=>$company_values,'data'=>json_encode($data)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreItem $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreItem $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if(isset($sanitized['product_name']))
            $sanitized['product_name'] = ucfirst($sanitized['product_name']);

        $cat_name = $sanitized['category']['category_name'];

        $cat_id = $sanitized['category']['id'];

        $sanitized = array_merge($sanitized,['category_name'=>$cat_name,'category_id'=>$cat_id]);

        // print_r($sanitized);die;

        $sanitized['gst_enable'] = ($request->gst_enable) ? 1 : 0;

        // Store the Item
        $item = Item::create($sanitized);
        if ($request->ajax()) {
            return ['redirect' => url('admin/items'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/items');
    }

    /**
     * Display the specified resource.
     *
     * @param Item $item
     * @throws AuthorizationException
     * @return void
     */
    public function show(Item $item)
    {
        $this->authorize('admin.item.show', $item);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Item $item
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Item $item)
    {
        $this->authorize('admin.item.edit', $item);

        $category = Category::all();

        $selcat = Item::with('categories')->where('category_id',$item->category_id)->get()->toArray();

        // echo "<pre>";
        // print_r($selcat->toArray());die;

        $selcat = array_column($selcat,'categories');

        $datapre = json_encode(array_merge(['category'=>$selcat],json_decode($item,true)));

        // echo "<pre>";
        // print_r($datapre);die;

        $company_data=DB::table('companies')->distinct()->pluck('company_name');
        $company_values=[];
        foreach ($company_data as $company_name => $value) {
            $company_values[]=$value;
        }

        return view('admin.item.edit', [
            'item' => $item,
            'datapre' => $datapre,
            'category'=>$category,
            'selcat'=>$selcat,
            'company_values'=>$company_values,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateItem $request
     * @param Item $item
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateItem $request, Item $item)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if(isset($sanitized['product_name']))
            $sanitized['product_name'] = ucfirst($sanitized['product_name']);

        // echo "<pre>";
        // print_r($sanitized);die;

        if(!empty($sanitized['category'][0]))
        {
            $cat_name = $sanitized['category'][0]['category_name'];

            $cat_id = $sanitized['category'][0]['id'];
        }
        else
        {
            $cat_name = $sanitized['category']['category_name'];

            $cat_id = $sanitized['category']['id'];
        }
        
        $sanitized = array_merge($sanitized,['category_name'=>$cat_name,'category_id'=>$cat_id]);

        $sanitized['gst_enable'] = ($request->gst_enable) ? 1 : 0;

        // Update changed values Item
        $item->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/items'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyItem $request
     * @param Item $item
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyItem $request, Item $item)
    {
        $item->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyItem $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyItem $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Item::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function disable(Request $request)
    {
        $item = Item::find($request->itemId);

        $disable = $item->update(['is_disabled'=>1]);

        if($disable)
        {
            return redirect('admin/items');
        }
    }

    public function enable(Request $request)
    {
        $item = Item::find($request->itemId);

        $enable = $item->update(['is_disabled'=>0]);

        if($enable)
        {
            return redirect('admin/items');
        }
    }

    public function import(Request $request)
    {
        $item = Item::find($request->item_id)->toArray();
        
        $user_id = Auth::user()->id;

        $shop_id = AdminVendor::where('user_id',$user_id)->pluck('id')->first();

        $shop_name = AdminVendor::where('id',$shop_id)->pluck('shop_name')->first();

        // echo "<pre>";
        // print_r($item);die;

        if($item['shop_name']==$shop_name && $item['shop_id']==$shop_id)
        {
            return redirect('admin/items');
        }

        $items = [];
        foreach($item as $key => $value)
        {
            if($key=='shop_id')
            {
                $value = $shop_id;
            }
            if($key=='shop_name')
            {
                $value = $shop_name;
            }

            $items[$key] = $value;
        }
        // echo "<pre>";
        // print_r($items);die;

        $import = Item::create($items);

        if($import)
        {
            return redirect('admin/items');
        }
    }
}
