<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use DB;
use App\Models\AdminUsers;
use App\Models\AdminVendor;

use App\Http\Requests\Admin\UpdateProfile;
use App\Models\Review;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public $adminUser;

    /**
     * Guard used for admin user
     *
     * @var string
     */
    protected $guard = 'admin';

    public function __construct()
    {
        // TODO add authorization
        $this->guard = config('admin-auth.defaults.guard');
    }

    /**
     * Get logged user before each method
     *
     * @param Request $request
     */
    protected function setUser($request)
    {
        if (empty($request->user($this->guard))) {
            abort(404, 'Admin User not found');
        }

        return ($this->adminUser = $request->user($this->guard));
    }

    /**
     * Show the form for editing logged user profile.                
     *
     * @param Request $request
     * @return Factory|View
     */
    public function editProfile(Request $adminUser)
    {
        $data=$this->setUser($adminUser);
        $dataadmin = AdminUsers::where('email', $data->email)->first();
        
// echo "<pre>";
//         print_r($dataadmin);die;

        $user = Auth::user();

        if($user->hasRole('vendor'))
        {
            $shopid = $dataadmin->vendorrelationship->id;
            $rating = Review::selectRaw("AVG(rating) as avg_rating")->where('shop_id',$shopid)->get();

            $rat = $rating[0]->avg_rating;
        }
        else
        {
            $rat = 0;
        }

        return view('admin.profile.edit-profile', [
            'adminUser' => $dataadmin,
            'rating'=>$rat
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @throws ValidationException
     * @return array|RedirectResponse|Redirector
     */
    public function updateProfile(UpdateProfile $request)
    {
        $this->setUser($request);
        $adminUser = $this->adminUser;

        // Sanitize input
        $sanitized = $request->only([
            'first_name',
            'last_name'
        ]);

        // $sanitized2 = $request->only([
        //     'country_code',
        //     'phonenumber_with_countrycode'
        // ]);

        // Update changed values AdminUser
        $this->adminUser->update($sanitized);

        $dataadmin = AdminUsers::where('email', $request->input('email'))->first();

        if($request->availability)
        {
            $request->availability = 1;
        }
        else
        {
            $request->availability = 0;
        }
        
        $dataadmin->update([
            'first_name'=>$request->input('first_name'),
            'last_name'=>$request->input('last_name'),
            'country_code'=>$request->input('country_code'),
            'phonenumber_with_countrycode'=>$request->input('phonenumber_with_countrycode'),
            'availability'=>$request->availability,
            'country_iso_code'=>$request->country_iso_code
        ]);

        $user = Auth::user();

        if($user->hasRole('vendor'))
        {
            $dataadmin->vendorrelationship()->update([
                'country_code'=>$request->input('country_code'),
                'phonenumber_with_countrycode'=>$request->input('phonenumber_with_countrycode'),
                'availability'=>$request->availability
            ]);
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/profile'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/profile');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function editPassword(Request $request)
    {
        $this->setUser($request);

        return view('admin.profile.edit-password', [
            'adminUser' => $this->adminUser,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @throws ValidationException
     * @return array|RedirectResponse|Redirector
     */
    public function updatePassword(Request $request)
    {
        // Validate the request
      
        $this->setUser($request);
        $adminUser = $this->adminUser;

        $request->validate([
            'password' => ['required', 'min:6', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/', 'string'],
            'password_confirmation' => ['required','required_with:password','same:password']
        ],[
            'password.required' =>'Password is required',
            'password.regex'=>'Password must contain at least one number,one uppercase,one lowercase letters and one special character.',
            'password_confirmation.required'=>'Password confirmation is required',
            'password_confirmation.required_with' => "Password confirmation is required",
            'password_confirmation.same' => "Password confirmation doesn't match"
        ]);

        

        // Sanitize input
        $sanitized = $request->only([
            'password',
            
        ]);
        
        //Modify input, set hashed password
        $sanitized['password'] = Hash::make($sanitized['password']);

        // Update changed values AdminUser
        $this->adminUser->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/password'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/password');
    }
}
