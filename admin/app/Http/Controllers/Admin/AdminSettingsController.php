<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminSetting\BulkDestroyAdminSetting;
use App\Http\Requests\Admin\AdminSetting\DestroyAdminSetting;
use App\Http\Requests\Admin\AdminSetting\IndexAdminSetting;
use App\Http\Requests\Admin\AdminSetting\StoreAdminSetting;
use App\Http\Requests\Admin\AdminSetting\UpdateAdminSetting;
use App\Models\AdminSetting;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AdminSettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAdminSetting $request
     * @return array|Factory|View
     */
    public function index(IndexAdminSetting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AdminSetting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'delivery_boy_searching', 'delivery_boy_search_limit'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.admin-setting.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.admin-setting.create');

        return view('admin.admin-setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAdminSetting $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAdminSetting $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the AdminSetting
        $adminSetting = AdminSetting::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/admin-settings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/admin-settings');
    }

    /**
     * Display the specified resource.
     *
     * @param AdminSetting $adminSetting
     * @throws AuthorizationException
     * @return void
     */
    public function show(AdminSetting $adminSetting)
    {
        $this->authorize('admin.admin-setting.show', $adminSetting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AdminSetting $adminSetting
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AdminSetting $adminSetting)
    {
        $this->authorize('admin.admin-setting.edit', $adminSetting);


        return view('admin.admin-setting.edit', [
            'adminSetting' => $adminSetting,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAdminSetting $request
     * @param AdminSetting $adminSetting
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAdminSetting $request, AdminSetting $adminSetting)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values AdminSetting
        $adminSetting->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/admin-settings'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/admin-settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAdminSetting $request
     * @param AdminSetting $adminSetting
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAdminSetting $request, AdminSetting $adminSetting)
    {
        $adminSetting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAdminSetting $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAdminSetting $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    AdminSetting::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
