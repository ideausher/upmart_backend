<?php

namespace App\Http\Controllers\Admin;

use App\Enum\UserEnum;
use App\Http\Controllers\Controller;
use App\Models\AdminUsers;
use App\Models\AdminVendor;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Booking;
use App\Models\BookingDetail;
use App\Models\DeliveryBoyTemporaryDocuments;
use App\Models\SparePartsShopLocation;
use App\Models\User;
use App\Models\VendorRequestedService;
use Brackets\AdminAuth\Models\AdminUser;
use Illuminate\Support\Carbon;
use Intersoft\Auth\App\Enum\UserEnum as EnumUserEnum;
use App\Enum\BookingStatus;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $user = \Illuminate\Support\Facades\Auth::user();

        if($user->hasRole('vendor'))
        {
            $user = AdminUsers::with('vendorrelationship')->where('id', $user->id)->first();
            $todayBookings = BookingDetail::whereDate('created_at', Carbon::now())->where('shopId', $user->vendorrelationship->id)->get();
            $todayEarning = 0;
            $gst = 0;
            if(count($todayBookings) > 0) {
                foreach($todayBookings as $booking) {
                    if($booking->status == BookingStatus::ORDER_ACCEPTED_BY_VENDOR || $booking->status == BookingStatus::ORDER_IN_PROGRESS || $booking->status == BookingStatus::DELIVERY_BOY_ASSIGNED || $booking->status == BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY || $booking->status == BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR || $booking->status == BookingStatus::DELIVERY_BOY_PICKED_ORDER || $booking->status == BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER || $booking->status == BookingStatus::DELIVERED_PRODUCT){
                        $todayEarning += $booking->bs_amount_to_vendor_after_commi;
                    }
                }
            }
            $totalBooking = BookingDetail::whereIN('status', [BookingStatus::ORDER_ACCEPTED_BY_VENDOR,BookingStatus::ORDER_IN_PROGRESS,BookingStatus::DELIVERY_BOY_ASSIGNED,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR,BookingStatus::DELIVERY_BOY_PICKED_ORDER,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,BookingStatus::DELIVERED_PRODUCT])->where('shopId', $user->vendorrelationship->id)->get();
            $totalEarning = 0;
            if(count($totalBooking) > 0) {
                foreach($totalBooking as $booking) {
                    $totalEarning += $booking->bs_amount_to_vendor_after_commi;
                }
            }

            return view('admin.dashboard.index', [
                'todayBookings' => count($todayBookings),
                'todayEarning' => $todayEarning,
                'totalBookings' => count($totalBooking), 
                // 'pendingVendorRequestedService' => count($vendorRequestedService),
                'totalEarnings' => $totalEarning,
               
    
            ]);
        }


        // dd(Carbon::now());
        $todayBookings = BookingDetail::whereDate('created_at', Carbon::now())->get();
        // dd($todayBookings);
        // dd($todayBookings);
        $todayEarning = 0;
        if(count($todayBookings) > 0) {
            foreach($todayBookings as $booking) {
                if($booking->status == BookingStatus::ORDER_PLACED || $booking->status == BookingStatus::ORDER_ACCEPTED_BY_VENDOR || $booking->status == BookingStatus::ORDER_IN_PROGRESS || $booking->status == BookingStatus::DELIVERY_BOY_ASSIGNED || $booking->status == BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY || $booking->status == BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR || $booking->status == BookingStatus::DELIVERY_BOY_PICKED_ORDER || $booking->status == BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER || $booking->status == BookingStatus::DELIVERED_PRODUCT){
                    $todayEarning += $booking->as_total_amount_to_admin + $booking->stripe_charges;    
                }
            }
        }
        $pendingPartner = DeliveryBoyTemporaryDocuments::all()->count();
        // dd($pendingPartner);
        $pendinguploaddocusers = User::where('user_type',2)->with(['documentsTemp','documents'])->get();
        if($pendinguploaddocusers)
        {
            $pendingPartnerDocs = [];
            foreach($pendinguploaddocusers as $pendingdocs)
            {
                // echo count($pendingdocs->documentsTemp);
                if((count($pendingdocs->documentsTemp)==0 || count($pendingdocs->documentsTemp)==1) && count($pendingdocs->documents)==0)
                {
                    $pendingPartnerDocs[] = $pendingdocs->id;
                }
            }
        }
        
        $totalEarning = 0;
        $totalSales = 0;
        $totalBooking = BookingDetail::whereIN('status', [BookingStatus::ORDER_PLACED,BookingStatus::ORDER_ACCEPTED_BY_VENDOR,BookingStatus::ORDER_IN_PROGRESS,BookingStatus::DELIVERY_BOY_ASSIGNED,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR,BookingStatus::DELIVERY_BOY_PICKED_ORDER,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,BookingStatus::DELIVERED_PRODUCT])->get();
        if(count($totalBooking) > 0) {
            foreach($totalBooking as $booking) {
                $totalEarning += $booking->as_total_amount_to_admin + $booking->stripe_charges;
                $totalSales += $booking->after_charge_amount;
            }
        }
        
        $partners = User::where('user_type', 2)->where('verified', 1)->get()->count();
        $customers = User::where('user_type', 1)->where('verified', 1)->get()->count();
        $totalShops = AdminVendor::all();

        // $vendorRequestedService = VendorRequestedService::all();

        return view('admin.dashboard.index', [
            'todayBookings' => count($todayBookings),
            'todayEarning' => $todayEarning,
            'pendingPartnerVerification' => count($pendingPartnerDocs),
            'totalBookings' => count($totalBooking), 
            'totalPartner' => $partners,
            'totalCustomer' => $customers,
            // 'pendingVendorRequestedService' => count($vendorRequestedService),
            'totalShopLocations' => count($totalShops),
            'totalEarnings' => $totalEarning,
            'totalSales' => $totalSales,

        ]);
    }


}
