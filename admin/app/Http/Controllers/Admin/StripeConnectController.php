<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AdminUsers;
use App\Models\AdminVendor;
use Illuminate\Support\Facades\Auth;


class StripeConnectController extends Controller
{
    public function __construct()
    {
        $stripe_api_key = env('STRIPE_SECRET_KEY');
        if ($stripe_api_key) {
            \Stripe\Stripe::setApiKey($stripe_api_key);
        } else {
            return 'payment key not set';
        }
    }

    public function index()
    {
        $userId = Auth::user()->id;
        $getstripe_id = AdminVendor::select('stripe_user_id')->where('user_id',$userId)->first();
        $stripeId = $getstripe_id->stripe_user_id;

        return view('admin.stripe-connect.index',['data'=>$stripeId]);
    }

    public function connect_stripe(Request $request)
    {
        $stripe_code = $request->code;

        $connectStripe = $this->linkAccount($stripe_code);

        return redirect('/admin/stripe-connect')->with('message',$connectStripe);
    }

    /**
     * connect stripe account.
     *
     * @param Request $request
     * @return json  $response
     */
    public function linkAccount($token_account)
    {
        $user = Auth::user()->id;
        $getstripe_id = AdminVendor::select('stripe_user_id')->where('user_id',$user)->first();
        $stripeId = $getstripe_id->stripe_user_id;

        if ($stripeId) {
            return "Your account linked to the stripe";
        } else {
            $token = $token_account;
            $account = \Stripe\OAuth::token([
                'grant_type' => 'authorization_code',
                'code' => $token,
            ]);
            if ($account) {
                AdminVendor::where('user_id',$user)->update(['stripe_user_id'=>$account->stripe_user_id]);
                AdminUsers::where('id',$user)->update(['stripe_user_id'=>$account->stripe_user_id]);
                return 'Successfully connected to stripe';
            } else {
                return 'Cannot link your account with stripe';
            }
        }
    }

    public function stripe_append(Request $request)
    {
        $stripe_code = $request->code;
    }
}
