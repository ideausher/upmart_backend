<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HelpAndSupport\BulkDestroyHelpAndSupport;
use App\Http\Requests\Admin\HelpAndSupport\DestroyHelpAndSupport;
use App\Http\Requests\Admin\HelpAndSupport\IndexHelpAndSupport;
use App\Http\Requests\Admin\HelpAndSupport\StoreHelpAndSupport;
use App\Http\Requests\Admin\HelpAndSupport\UpdateHelpAndSupport;
use App\Models\HelpAndSupport;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

use App\Models\Notification;

class HelpAndSupportsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexHelpAndSupport $request
     * @return array|Factory|View
     */
    public function index(IndexHelpAndSupport $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(HelpAndSupport::class)->modifyquery(function ($query) use ($request) {
            $query->with(['user','shop','booking'])
                ->orderBy('id','DESC');
        })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'booking_id', 'user_id', 'shop_id','feedback_title','query_status'],

            // set columns to searchIn
            ['id', 'feedback_title', 'feedback_description']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.help-and-support.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.help-and-support.create');

        return view('admin.help-and-support.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreHelpAndSupport $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreHelpAndSupport $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the HelpAndSupport
        $helpAndSupport = HelpAndSupport::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/help-and-supports'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/help-and-supports');
    }

    /**
     * Display the specified resource.
     *
     * @param HelpAndSupport $helpAndSupport
     * @throws AuthorizationException
     * @return void
     */
    public function show(HelpAndSupport $helpAndSupport)
    {
        $this->authorize('admin.help-and-support.show', $helpAndSupport);

        // dd($helpAndSupport);

        return view('admin.help-and-support.show',['helpAndSupport'=>$helpAndSupport]);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param HelpAndSupport $helpAndSupport
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(HelpAndSupport $helpAndSupport)
    {
        $this->authorize('admin.help-and-support.edit', $helpAndSupport);


        return view('admin.help-and-support.edit', [
            'helpAndSupport' => $helpAndSupport,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateHelpAndSupport $request
     * @param HelpAndSupport $helpAndSupport
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateHelpAndSupport $request, HelpAndSupport $helpAndSupport)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values HelpAndSupport
        $helpAndSupport->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/help-and-supports'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/help-and-supports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyHelpAndSupport $request
     * @param HelpAndSupport $helpAndSupport
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyHelpAndSupport $request, HelpAndSupport $helpAndSupport)
    {
        $helpAndSupport->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyHelpAndSupport $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyHelpAndSupport $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    HelpAndSupport::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function change_status(Request $request)
    {
        $hsID = $request->hsid;

        $helpAndSupport = HelpAndSupport::find($hsID);

        $status = $request->query_status ?? false;
        $feedback = $request->myMessage ?? false;

        $helpAndSupport->query_status = $status;
        $helpAndSupport->admin_reply = $feedback;
        // push notification
        if($status==0)
        {
            Notification::createNotificationNew($helpAndSupport->user_id,"Dispute feedback!","We have open the ticket id #".$helpAndSupport->ticket_id.".");
            Notification::create([
                'send_by'=>1,
                'send_to'=>$helpAndSupport->user_id,
                'notification_type'=>17,
                'title'=>"Dispute feedback!",
                'description'=>"We have open the ticket id #".$helpAndSupport->ticket_id.".",
                'is_read'=>0
            ]);
        }
        else
        {
            Notification::createNotificationNew($helpAndSupport->user_id,"Dispute feedback!","We have closed the ticket id #".$helpAndSupport->ticket_id.".");
            Notification::create([
                'send_by'=>1,
                'send_to'=>$helpAndSupport->user_id,
                'notification_type'=>18,
                'title'=>"Dispute feedback!",
                'description'=>"We have closed the ticket id #".$helpAndSupport->ticket_id.".",
                'is_read'=>0
            ]);
        }

        $helpAndSupport->save();
        return redirect('admin/help-and-supports');
        
    }
}
