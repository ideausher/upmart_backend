<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminVendor\BulkDestroyAdminVendor;
use App\Http\Requests\Admin\AdminVendor\DestroyAdminVendor;
use App\Http\Requests\Admin\AdminVendor\IndexAdminVendor;
use App\Http\Requests\Admin\AdminVendor\StoreAdminVendor;
use App\Http\Requests\Admin\AdminVendor\UpdateAdminVendor;
use App\Models\AdminVendor;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AdminVendorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexAdminVendor $request
     * @return array|Factory|View
     */
    public function index(IndexAdminVendor $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AdminVendor::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id','user_id', 'Phone_number_with_country_code', 'Shop_Name','Shop_Address','Additional_address'],


            // set columns to searchIn
            ['id', 'Vendor_Name', 'Email', 'Phone_number_with_country_code', 'Shop_Name','Shop_Address'],
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.admin-vendor.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.admin-vendor.create');

        return view('admin.admin-vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAdminVendor $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreAdminVendor $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the AdminVendor
        $adminVendor = AdminVendor::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/admin-vendors'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/admin-vendors');
    }

    /**
     * Display the specified resource.
     *
     * @param AdminVendor $adminVendor
     * @throws AuthorizationException
     * @return void
     */
    public function show(AdminVendor $adminVendor)
    {
        $this->authorize('admin.admin-vendor.show', $adminVendor);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AdminVendor $adminVendor
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(AdminVendor $adminVendor)
    {
        $this->authorize('admin.admin-vendor.edit', $adminVendor);


        return view('admin.admin-vendor.edit', [
            'adminVendor' => $adminVendor,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAdminVendor $request
     * @param AdminVendor $adminVendor
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateAdminVendor $request, AdminVendor $adminVendor)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values AdminVendor
        $adminVendor->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/admin-vendors'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/admin-vendors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyAdminVendor $request
     * @param AdminVendor $adminVendor
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyAdminVendor $request, AdminVendor $adminVendor)
    {
        $adminVendor->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyAdminVendor $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyAdminVendor $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    AdminVendor::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
