<?php

namespace App\Http\Controllers\Admin;

use Mail;
use Exception;
use App\Models\User;
use App\Mail\DeliveryBoy;
use App\PushNotification;
use Illuminate\View\View;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\VerificationPanel;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Models\DeliveryBoyMainDocuments;

use Illuminate\Database\Eloquent\Builder;

use App\Http\Requests\Admin\User\IndexUser;

use App\Http\Requests\Admin\User\StoreUser;
use App\Http\Requests\Admin\UpdateDocuments;
use App\Http\Requests\Admin\User\UpdateUser;

use App\Http\Requests\Admin\User\DestroyUser;

use App\Models\DeliveryBoyTemporaryDocuments;
use Brackets\AdminListing\Facades\AdminListing;
use App\Http\Requests\Admin\User\BulkDestroyUser;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use IntersoftNotification\App\Services\PushNotificationService;

class VerificationPanelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexUser $request
     * @return array|Factory|View
     */
    public function index(IndexUser $request)
    {
    
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(VerificationPanel::class)->modifyQuery(function ($query) use ($request) {
           $query= $query->where('user_type', 2)->has('documents')->with('documents')->latest();
            
            })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'email', 'profile_picture', 'country_code', 'phone_number', 'is_block','user_type'],

            // set columns to searchIn
            ['name', 'email', 'country_code', 'phone_number']
        );

        /* $data = DeliveryBoyTemporaryDocuments::select('users.id as userid','deliveryboy_temporary_documents.*')->join('users','deliveryboy_temporary_documents.user_id','=','users.id')->where('users.user_type',2)->get();
        echo "<pre>";
        print_r($data->toArray());
        die; */

        if ($request->ajax()) {
           
            return ['data' => $data];
        }
        // dd($data);
        return view('admin.verification-panel.index', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function view(VerificationPanel $user)
    {
        return view('admin.verification-panel.view', [
            'user' => $user
        ]);
    }

    public function update(UpdateDocuments $request, VerificationPanel $user)
    {
        $vehicle_status = $request->vehicle_status;
        $drivinglicence_status = $request->drivinglicence_status;
        $licence_no_plate_status = $request->licence_no_plate_status;
        $vehicle_insurence_status = $request->vehicle_insurence_status;

        $vehicle_reason = $request->vehicle_reason;
        $drivinglicence_reason = $request->drivinglicence_reason;
        $licence_no_plate_reason = $request->licence_no_plate_reason;
        $vehicle_insurence_reason = $request->vehicle_insurence_reason;

        if($vehicle_status==null)
        {
            $vehicle_status = 0;
        }
        if($drivinglicence_status==null)
        {
            $drivinglicence_status = 0;
        }
        if($licence_no_plate_status==null)
        {
            $licence_no_plate_status = 0;
        }
        if($vehicle_insurence_status==null)
        {
            $vehicle_insurence_status = 0;
        }

        $up = DeliveryBoyTemporaryDocuments::where('user_id',$user->id)->update([
                'vehicle_status'=>$vehicle_status,
                'drivinglicence_status'=>$drivinglicence_status,
                'licence_no_plate_status'=>$licence_no_plate_status,
                'vehicle_insurence_status'=>$vehicle_insurence_status,
                'vehicle_reason'=>$vehicle_reason,
                'drivinglicence_reason'=>$drivinglicence_reason,
                'licence_no_plate_reason'=>$licence_no_plate_reason,
                'vehicle_insurence_reason'=>$vehicle_insurence_reason
            ]);


        if($up)
        {
            $temp = DeliveryBoyTemporaryDocuments::where('user_id',$user->id)->first()->toArray();
            
            // echo "<pre>";
            // print_r($temp);die;

            $approve = 0;
            $reject = 0;

            if ($temp['vehicle_status']==2 || $temp['drivinglicence_status']==2 || $temp['licence_no_plate_status']==2 || $temp['vehicle_insurence_status']==2) {
                $approve = 1;
                $stat = 'Approved';
            }
            if ($temp['vehicle_status']==3 || $temp['drivinglicence_status']==3 || $temp['licence_no_plate_status']==3 || $temp['vehicle_insurence_status']==3) {
                $reject = 1;
                $stat = 'Rejected';
            }

            if ($temp['vehicle_status']!=1 || $temp['drivinglicence_status']!=1 || $temp['licence_no_plate_status']!=1 || $temp['vehicle_insurence_status']!=1) {
                if (!empty($user->email)) {
                    $data=array(
                        'user_id' => $temp['user_id'],
                        'name' => $user->name,
                        'vehicle_status'=>$temp['vehicle_status'],
                        'drivinglicence_status'=>$temp['drivinglicence_status'],
                        'licence_no_plate_status'=>$temp['licence_no_plate_status'],
                        'vehicle_insurence_status'=>$temp['vehicle_insurence_status'],
                        'vehicle_reason'=>$temp['vehicle_reason'],
                        'drivinglicence_reason'=>$temp['drivinglicence_reason'],
                        'licence_no_plate_reason'=>$temp['licence_no_plate_reason'],
                        'vehicle_insurence_reason'=>$temp['vehicle_insurence_reason'],
                        'approve' => $approve,
                        'reject' => $reject
                    );
                    // Mail::to([$user->email])->send(new DeliveryBoy($data));
                }
            }

            if ($temp['vehicle_status']==2 && $temp['drivinglicence_status']==2 && $temp['licence_no_plate_status']==2 && $temp['vehicle_insurence_status']==2) {

                $exclude = [$temp['vehicle_status'],$temp['drivinglicence_status'],$temp['licence_no_plate_status'],$temp['vehicle_insurence_status'],$temp['vehicle_reason'],$temp['drivinglicence_reason'],$temp['licence_no_plate_reason'],$temp['vehicle_insurence_reason']];

                $temp = array_diff($temp,$exclude);

                $exist = DeliveryBoyMainDocuments::where('user_id', $user->id)->get()->count();

                if ($exist>0) {
                    DeliveryBoyMainDocuments::where('user_id', $user->id)->update($temp);
                } else {
                    $temp['user_id'] = $user->id;
                    Log::info($temp);
                    DeliveryBoyMainDocuments::create($temp);
                }

                DeliveryBoyTemporaryDocuments::where('user_id', $user->id)->delete();

                VerificationPanel::find($user->id)->update(['is_block'=>0]);
            }

            // push notification
            Notification::createNotificationNew($user->id,"Documents Approval process!","We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!");

            Notification::create([
                'send_by'=>1,
                'send_to'=>$user->id,
                'notification_type'=>15,
                'title'=>"Documents Approval process!",
                'description'=>"We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!",
                'is_read'=>0
            ]);

            if ($request->ajax()) {
                return [
                    'redirect' => url('admin/verification-panel'),
                    'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
                ];
            }

            return redirect('admin/verification-panel');
        }
    }
}
