<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Carbon\Carbon;
use App\Models\Item;
use App\Models\User;
use \Stripe\Transfer;
use App\Mail\Booking;
use App\Models\Review;
use Stripe\StripeClient;
use App\PushNotification;
use Illuminate\View\View;
use App\Models\AdminVendor;
use App\Models\OrderDetail;
use App\Models\AdminSetting;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\BookingDetail;
use Illuminate\Http\Response;
use App\Models\FailedTransfer;

use App\Models\UsersBasedLocation;

use Illuminate\Routing\Redirector;

use Illuminate\Support\Facades\DB;

use App\Models\BookingNotification;
use App\Models\BookingRejectReason;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Stripe_payment_record;
use Illuminate\Http\RedirectResponse;
use App\Models\BookingTrackingHistory;

use Illuminate\Contracts\View\Factory;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Admin\BookingDetail\IndexBookingDetail;
use App\Http\Requests\Admin\BookingDetail\StoreBookingDetail;
use App\Http\Requests\Admin\BookingDetail\UpdateBookingDetail;
use App\Http\Requests\Admin\BookingDetail\DestroyBookingDetail;
use IntersoftNotification\App\Services\PushNotificationService;
use App\Http\Requests\Admin\BookingDetail\BulkDestroyBookingDetail;
use App\Models\Otp;
use AWS;
use Aloha;
use App\Models\AdminUser;
use App\Models\AdminUsers;

class BookingDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexBookingDetail $request
     * @return array|Factory|View
     */
    public function index(IndexBookingDetail $request)
    {
        if(isset($request->search)&&strtolower($request->search)=='delivery')
        {
            $request->search=1;
        }
        if(isset($request->search)&&strtolower($request->search)=='take away')
        {
            $request->search=2;
        }
        
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(BookingDetail::class)->modifyQuery(function($query) use ($request){
            
            $user = Auth::user();
            $user_id = $user->id;
            if ($user->hasRole('vendor')) {
                $shop_id = AdminVendor::where('user_id', $user_id)->pluck('id')->first();
                $query->where('shopId', $shop_id);
            }
            else
            {
                // 
            }
            $query->with(['users','deliveryboy','shop'])->latest();
            
            // dd($query->toSql());
           
        })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'userId', 'shopId', 'addressId', 'cardId', 'couponId', 'deliveryBoyId','bookingType','bookingDateTime', 'after_charge_amount', 'tip_to_delivery_boy', 'commission_charge', 'platform_charge', 'delivery_charge_to_delivery_boy', 'delivery_charge_for_customer', 'amount_after_discount', 'discount_amount', 'amount_before_discount', 'instruction', 'status', 'active','booking_code'],

            // set columns to searchIn
            ['id','booking_code','bookingType','status'],
        );

        $s = $data->toArray();
        array_walk_recursive($s, function(&$v,$k){
            if($k=='bookingType')
            {
                $v = BookingDetail::LEVELS[$v];
            }
            if($k=='status')
            {
                $v = BookingDetail::ORDER_STATUS[$v];
            }
        });
        $data = collect($s);

        $delivery_boy_setting = AdminSetting::all();
        $setting='';
        if(!empty($delivery_boy_setting))
        {
            $setting = $delivery_boy_setting[0]->delivery_boy_searching;
        }
        // echo "<pre>";
        // print_r($data);
        // die;
        if ($request->ajax()) {
            
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.booking-detail.index', ['data' => $data,'setting'=>$setting]);
    }

    public function view($booking)
    {
        $booking = BookingDetail::find($booking);

        $delivery_boy_setting = AdminSetting::all();
        $setting='';
        if($delivery_boy_setting)
        {
            $setting = $delivery_boy_setting[0]->delivery_boy_searching;
        }

        $totalGST = 0;
        if($booking->orderdetails){
            foreach($booking->orderdetails as $order){
                $totalGST += $order->gst_tax;
            }
        }

        $totalamount = $booking->amount_before_discount+$booking->platform_charge+$booking->tip_to_delivery_boy+$booking->delivery_charge_for_customer+$totalGST+$booking->pst+$booking->hst-$booking->discount_amount;
        if($booking)
        {
            $booking->totalamount = $totalamount;
            $booking->totalGST = $totalGST;

        }
        return view('admin.booking-detail.view', [
            'booking' => $booking,
            'setting'=>$setting
        ]);
    }

    public function accept(Request $request)
    {
        $bookingid = $request->booking_id;
        $limit = '';
        $delivery_boy_setting = AdminSetting::all();
        if($delivery_boy_setting)
        {
            $limit = $delivery_boy_setting[0]->delivery_boy_search_limit;
        }

        $booking = BookingDetail::find($bookingid);   
        $v_done = $booking->update(['status'=>2,'shop_status_change_time'=>Carbon::now()]);

        if($v_done)
        {
            $admin_user_id = AdminUsers::where('email', env('SUPER_ADMIN_EMAIL'))->first();
            PushNotification::sendAdminPushNotification($admin_user_id->id, 'Order Accepted', "Order with Booking Code #$booking->booking_code is Accepted", 1);

            BookingTrackingHistory::create([
                'bookingId'=>$bookingid,
                'reason'=>'ORDER_ACCEPTED_BY_VENDOR',
                'value'=>'ORDER_ACCEPTED_BY_VENDOR',
                'status_value'=>2,
                'type'=>1,
                'userId'=>null
            ]);

            if($booking->bookingDateTime!=null)
            {
                $send_notification = 1;
            }
            else
            {
                $send_notification = 0;
            }

            if($request->del_status==1)
            {
                if($delivery_boy_setting)
                {
                    $setting = $delivery_boy_setting[0]->delivery_boy_searching;
                    if($setting==1)
                    {
                        
                        // return view('admin.booking-detail.manual',['bookingid'=>$bookingid]);
                        return redirect('admin/booking-details');
                    }
                }
            }

            $avg_rating = Review::selectRaw("AVG(rating) as avg_rating, delivery_boy_id")->where('delivery_boy_id','!=','')->limit($limit)->groupBy('delivery_boy_id')->get()->toArray();

            $coll = collect($avg_rating);
            
            $five_three = $coll->where('avg_rating','<=',5)->where('avg_rating','>=',3);
            $two_one = $coll->where('avg_rating', '<=', 2)->where('avg_rating', '>=', 1);
            $zero = $coll->where('avg_rating','<=',2)->where('avg_rating','>=',1);

            $rating = [];
            $deliveryBoyId = [];

            if (!empty($five_three)) {
                foreach ($five_three as $ratings) {
                    $rating[] = $ratings['avg_rating'];
                    $deliveryBoyId[] = $ratings['delivery_boy_id'];
                }
            }
            elseif(!empty($two_one))
            {
                foreach ($two_one as $ratings) {
                    $rating[] = $ratings['avg_rating'];
                    $deliveryBoyId[] = $ratings['delivery_boy_id'];
                }
            }
            else
            {
                foreach ($zero as $ratings) {
                    $rating[] = $ratings['avg_rating'];
                    $deliveryBoyId[] = $ratings['delivery_boy_id'];
                }
            }

            $distance = UsersBasedLocation::where('user_id',$booking->userId)->first();

            $location = '';

            if($distance)
            {
                $lat = $distance->lat;
                $lng = $distance->lng;

                $location = UsersBasedLocation::selectRaw(" users_based_location.*,( 6371 * acos( cos( radians(' $lat ') ) * cos( radians( lat ) ) * cos( radians( lng) - radians('$lng') ) + sin( radians(' $lat ') ) * sin( radians( lat ) ) ) ) AS distance ")->leftJoin('users','users.id','=','users_based_location.user_id')->where('users.user_type',2)->havingRaw('distance <= ' . env("CUS_DISTANCE_VALUE", "20"))->get();
            }
            
            if(!empty($deliveryBoyId) && $send_notification==0 && !empty($location))
            {
                $i=0;
                Log::error("**** Delivery Boys In Location: " . json_encode(array_column($location->toArray(),'user_id'))  );
                Log::error("**** Delivery Boys : ");
                foreach($deliveryBoyId as $dId)
                {
                    Log::error($dId);
                    $deliveryboy = User::find($dId);
                    if( ! in_array($dId,array_column($location->toArray(),'user_id') )){
                        // if delivery boy is not in the region then do not send notification
                        continue;
                    }

                    $data=array(
                        'name' => $deliveryboy->name,
                        'orderId' => $bookingid,
                        'status'=>'accept'
                    );

                    /* if (!empty($deliveryboy->email)) {
                        Mail::to([$deliveryboy->email])->send(new Booking($data));
                    } */

                    $ratings = $rating[$i];

                    // push notification for delivery boy
                    Notification::createNotificationNew($dId,"Order Accepted","Order #".$booking->booking_code." has been accepted by vendor.");

                    Notification::create([
                        'send_by'=>1,
                        'send_to'=>$dId,
                        'notification_type'=>$booking->status,
                        'title'=>"Order Accepted",
                        'description'=>"Order #".$booking->booking_code." has been accepted by vendor.",
                        'is_read'=>0
                    ]);

                    BookingNotification::updateOrCreate([
                        'booking_id'=>$bookingid,
                        'deliveryboy_id'=>$dId,
                        'deliveryboy_rating' => $ratings,
                        'notification_message'=>'aaa',
                        'deliveryboy_accept_status'=>0,
                        'expiration_time'=>Carbon::now()->addMinutes(5)
                    ]);
                    $i++;
                }
            }

            $userId = $booking->userId;

            $user = User::find($userId);

            $data=array(
                'name' => $user->name,
                'orderId' => $bookingid,
                'status'=>'accept'
            );

            // push notification for customer
            Notification::createNotificationNew($userId,"Order Accepted!","Your Order #".$booking->booking_code." has been accepted.");

            Notification::create([
                'send_by'=>1,
                'send_to'=>$userId,
                'notification_type'=>$booking->status,
                'title'=>"Order Accepted!",
                'description'=>"Your Order #".$booking->booking_code." has been accepted.",
                'is_read'=>0
            ]);
            
            /* if (!empty($user->email)) {
                Mail::to([$user->email])->send(new Booking($data));
            } */

            // if ($request->ajax()) {
            //     return [
            //         'redirect' => url('admin/booking-details'),
            //         'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            //     ];
            // }

            return redirect()->back();
        }
    }

    public function reject(Request $request)
    {
        $bookingid = $request->bookingId;

        $booking = BookingDetail::find($bookingid);

        $userId = $booking->userId;
        $reject = BookingRejectReason::updateOrCreate([
            'booking_id' => $bookingid,
            'reason' => $request->reason,
        ]);

        $stripe_api_key = env('STRIPE_SECRET_KEY');
        if ($stripe_api_key) {
            \Stripe\Stripe::setApiKey($stripe_api_key);
        } else {
            return 'payment key not set';
        }

        
        // refund payment
        $refund_status = false;
        $stripe = Stripe_payment_record::where('booking_code',$booking->booking_code)->first();
        $refund_amount = $booking->after_charge_amount+$booking->tip_to_delivery_boy;
        $refund_amount = $refund_amount*100;
        $refund = new StripeClient($stripe_api_key);
        $refundStatus = false;
        if($stripe){
            try{
                $refundStatus = $refund->refunds->create(['charge'=>$stripe->charge_id,'amount'=>$refund_amount]);
            }
            catch(Exception $ex){
            }
            
            
        }
        $admin_user_id = AdminUsers::where('email', env('SUPER_ADMIN_EMAIL'))->first();
        PushNotification::sendAdminPushNotification($admin_user_id->id, 'Order Rejected', "Order with Booking Code #$booking->booking_code is Rejected due to $request->reason", 1);

        // push notification for customer
        Notification::createNotificationNew($userId,"Order Rejected!","Your Order #".$booking->booking_code." has been rejected.");
        Notification::create([
            'send_by'=>1,
            'send_to'=>$userId,
            'notification_type'=>$booking->status,
            'title'=>"Order Rejected!",
            'description'=>"Your Order #".$booking->booking_code." has been rejected. Reason of Rejection : ".$request->reason .".",
            'is_read'=>0
        ]);
        if($refund){
            $refund_status = true;
            $stripe->payment_status=2;
            $stripe->save();
            if($refundStatus){
                Notification::createNotificationNew($booking->userId,"Order Refund Initiated!","Dear Customer! Your Order payment refund initiated for the order #".$booking->booking_code.".");
                Notification::create([
                    'send_by'=>1,
                    'send_to'=>$booking->userId,
                    'notification_type'=>16,
                    'title'=>"Order Refund Initiated!",
                    'description'=>"Dear Customer! Your Order payment refund initiated for the order #".$booking->booking_code.".",
                    'is_read'=>0
                ]);
                
            }
            
        }
        
        $v_done = $booking->update(['status'=>3,'shop_status_change_time'=>Carbon::now()]);

        if($v_done)
        {
            BookingTrackingHistory::create([
                'bookingId'=>$bookingid,
                'reason'=>'ORDER_REJECTED_BY_VENDOR',
                'value'=>'ORDER_REJECTED_BY_VENDOR',
                'status_value'=>3,
                'type'=>1,
                'userId'=>null
            ]);

            $user = User::find($userId);

            $data=array(
                'name' => $user->name,
                'orderId' => $bookingid,
                'status'=>'reject',
                'reason'=>$request->reason
            );

            /* if (!empty($user->email)) {
                Mail::to([$user->email])->send(new Booking($data));
            } */
            $orderdetail = OrderDetail::select('productId','quantity')->where('bookingId',$bookingid)->get()->toArray();
            if($orderdetail)
            {
                foreach($orderdetail as $od)
                {
                    $item = Item::where('id',$od['productId'])->get()->toArray();
                    foreach($item as $it)
                    {
                        Item::where('id',$od['productId'])->update(['stock_quantity'=>$it['stock_quantity']+$od['quantity']]);
                    }
                }
            }
            
            
            

            // if ($request->ajax()) {
            //     return [
            //         'redirect' => url('admin/booking-details'),
            //         'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            //     ];
            // }
    
            return redirect()->back();
        }
    }

    public function change_status(Request $request)
    {
        $bookingid = $request->booking_id;

        $booking = BookingDetail::find($bookingid);

        $userId = $booking->userId;

        if($request->stat == 10)
        {
            $user = User::find($userId);
            if($user)
            {
                $phonenumber = $user->phone_number;
                $countrycode = $user->country_code;
                $email = $user->email;
            }

            
            // $otp = rand(100000, 999999);
            $otp = 444444;
            $phone = $countrycode . $phonenumber;

            $sms = AWS::createClient('sns');
            $sms = $sms->publish([
                'Message' => "Your OTP for " . env('APP_NAME') . ' is :- ' . $otp,
                'PhoneNumber' => $phone,
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType'  => [
                        'DataType'    => 'String',
                        'StringValue' => 'Transactional',
                    ]
                ],
            ]);
            if($sms)
            {
                $user_otp = Otp::where('phone_number', $phonenumber)->where('country_code',$countrycode)->first();

                if ($user_otp) {
                    $user_otp->update(['otp' => $otp, 'is_verified' => 0]);
                    $id = $user_otp->id;
                    
                } else {
                    Otp::create([
                        'phone_number'=>$phonenumber,
                        'country_code'=>$countrycode,
                        'email'=>$email,
                        'otp'=>$otp,
                        'is_verified'=>0
                    ]);
                }

                return redirect('admin/booking-details/'.$bookingid.'/confirm-otp');
            }
        }
        
        $update = BookingDetail::where('id',$bookingid)->update([
            'status'=>$request->stat
        ]);

        if($update)
        {
            if($request->stat == 4){

                BookingTrackingHistory::create([
                    'bookingId'=>$bookingid,
                    'reason'=>'ORDER_IN_PROGRESS',
                    'value'=>'ORDER_IN_PROGRESS',
                    'status_value'=>4,
                    'type'=>1,
                    'userId'=>$userId
                ]);
    
                // push notification for customer
                Notification::createNotificationNew($userId,"Order in progress!","Your Order #".$booking->booking_code." is in progress.");
    
                Notification::create([
                    'send_by'=>1,
                    'send_to'=>$userId,
                    'notification_type'=>$request->stat,
                    'title'=>"Order in progress!",
                    'description'=>"Your Order #".$booking->booking_code." is in progress.",
                    'is_read'=>0
                ]);
            }

            return redirect()->back();
        }
    }

    public function confirm_otp(Request $request)
    {
        return view('admin.booking-detail.confirm-booking-otp',['bookingid'=>$request->bookingDetail]);
    }

    public function verify_otp(Request $request)
    {
        
        
        $bookingid = $request->bookingDetail;

        $booking = BookingDetail::find($bookingid);

        $userId = $booking->userId;

        $user = User::find($userId);
        if($user)
        {
            $phonenumber = $user->phone_number;
            $countrycode = $user->country_code;
            $email = $user->email;
        }

        $user_otp = Otp::where('phone_number', $phonenumber)->where('otp',$request->otp)->where('is_verified',0)->first();

        if($user_otp)
        {
            $user_otp->update(['is_verified' => 1]);

            $update = BookingDetail::where('id',$bookingid)->update([
                'status'=>10
            ]);
    
            if($update)
            {
                // $this->transferPaymentToDB($booking);
                $this->transferPaymentToVendor($booking);
                BookingTrackingHistory::create([
                    'bookingId'=>$bookingid,
                    'reason'=>'ORDER_COMPLETED',
                    'value'=>'ORDER_COMPLETED',
                    'status_value'=>10,
                    'type'=>1,
                    'userId'=>$userId
                ]);
                    
                // push notification for customer
                Notification::createNotificationNew($userId,"Order Completed!","Your Order #".$booking->booking_code." Completed.");
                    
                Notification::create([
                    'send_by'=>1,
                    'send_to'=>$userId,
                    'notification_type'=>10,
                    'title'=>"Order Completed!",
                    'description'=>"Your Order #".$booking->booking_code." Completed.",
                    'is_read'=>0
                ]);
    
            }
            if ($request->ajax()) {
                return ['redirect' => url('admin/booking-details'), 'message' => trans('admin.otp.succeeded')];
            }
        }
        else
        {
            throw new Exception("Invalid OTP! Please try again.");
            
        }
    }

    public function transferPaymentToDB($booking){
        $stripe_api_key = env('STRIPE_SECRET_KEY');
        if ($stripe_api_key) {
            \Stripe\Stripe::setApiKey($stripe_api_key);
        } else {
            return 'payment key not set';
        }
        $payment = new StripeClient($stripe_api_key);
        try{
            $transfer = Transfer::create([
				'amount' => $booking->ws_total_amount_to_db * 100,
				'currency' => 	'CAD',
				'destination' => $booking->deliveryboy->stripe_user_id,
			]);
        }
        catch(Exception $ex){
            if(! $payment){
                FailedTransfer::create([
                    'booking_id' => $booking->id,
                    'delivery_boy_id' => $booking->deliveryBoyId,
                    'vendor_id' => null,
                    'status'
                ]);
            }
        }
    }
    public function transferPaymentToVendor($booking){
        $stripe_api_key = env('STRIPE_SECRET_KEY');
        if ($stripe_api_key) {
            \Stripe\Stripe::setApiKey($stripe_api_key);
        } else {
            return 'payment key not set';
        }
        $payment = new StripeClient($stripe_api_key);
        $transfer = false;
        try{
            $transfer = Transfer::create([
				'amount' => $booking->bs_amount_to_vendor_after_commi * 100,
				'currency' => 	'CAD',
				'destination' => $booking->shop->stripe_user_id,
			]);
            
        }
        catch(Exception $ex){
            if(! $transfer){
                FailedTransfer::create([
                    'booking_id' => $booking->id,
                    'delivery_boy_id' => null,
                    'vendor_id' => $booking->shopId,
                    'status'
                ]);
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyBookingDetail $request
     * @param BookingDetail $bookingDetail
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyBookingDetail $request, BookingDetail $bookingDetail)
    {
        $bookingDetail->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyBookingDetail $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyBookingDetail $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    BookingDetail::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
