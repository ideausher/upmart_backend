<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\BulkDestroyUser;
use App\Http\Requests\Admin\User\DestroyUser;
use App\Http\Requests\Admin\User\IndexUser;
use App\Http\Requests\Admin\User\StoreUser;
use App\Http\Requests\Admin\User\UpdateUser;
use App\Models\User;
use App\Models\BlockReasonDeliveryBoy;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use App\Http\Controllers\Admin\Auth;

use Mail;
use App\Mail\BlockDeliveryBoy;
use App\Mail\DeliveryBoy;

use App\Models\DeliveryBoyMainDocuments;
use App\Models\DeliveryBoyTemporaryDocuments;

use App\Http\Requests\Admin\UpdateDocuments;

use App\AdminListingCustomer;

use App\Http\Requests\Admin\BookingDetail\IndexBookingDetail;
use App\Models\BookingDetail;

use App\Models\Notification;
use App\Models\Review;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexUser $request
     * @return array|Factory|View
     */
    public function index(IndexUser $request)
    {
        // AdminListingCustomer::create(
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)->modifyQuery(function ($query) use ($request) {
            $query->where('user_type', 2)->with('documentsTemp')->with('documents')->latest();
            })->processRequestAndGet(
            // pass the request with params
            $request,
            // set columns to query
            ['id','name', 'email', 'profile_picture', 'country_code', 'phone_number', 'is_block'],
            // set columns to searchIn
            ['name', 'email', 'country_code', 'phone_number']
        );
        // echo "<pre>";
        // print_r(count($data));die;

        if ($request->ajax()) {
            $data = AdminListing::create(User::class)->modifyQuery(function ($query) use ($request) {
                    $query->where('user_type', 2)->with('documentsTemp')->with('documents')->latest();
                })->processRequestAndGet(
                // pass the request with params
                $request,
    
                // set columns to query
                ['id','name', 'email', 'profile_picture', 'country_code', 'phone_number', 'is_block'],
    
                // set columns to searchIn
                []
            );
            $data = $data->getCollection()->filter(function ($row) use($request) {
                if($request->search == 1 ){ // approved
                    if(count($row->documentsTemp) != 0 && count($row->documents) == 0 ){
                        return true;
                    }
                }
                elseif($request->search == 2 ){ // approved
                    if(count($row->documentsTemp) == 0 && count($row->documents) != 0 ){
                        return true;
                    }
                }
                elseif($request->search == 3 ){ // rejected
                    if(count($row->documentsTemp) != 0 && count($row->documents) == 0 ){
                        if(isset($row->documentsTemp[0]->vehicle_status)){
                            if($row->documentsTemp[0]->vehicle_status == 3 || $row->documentsTemp[0]->drivinglicence_status == 3 || $row->documentsTemp[0]->licence_no_plate_status == 3 || $row->documentsTemp[0]->vehicle_insurence_status == 3  ){
                                return true;
                            }
                        }
                    }
                }
                else{
                    return true;
                }
            });


            // Recreate because filter removed pagination properties
            $data = new \Illuminate\Pagination\LengthAwarePaginator(
                $data,
                count($data),
                100,
                1, [
                'path' => \Request::url(),
                'query' => [
                    'page' => 1
                ]
                ]
            );
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.user.index', ['data' => $data]);
    }

    public function view(User $user)
    {

        $rating = Review::selectRaw("AVG(rating) as avg_rating")->where('delivery_boy_id',$user->id)->get()->toArray();


        return view('admin.user.view', [
            'user' => $user,
            'avg_rating'=>$rating[0]['avg_rating']
        ]);
    }

    public function UserBlockEmail(Request $request) {
        
        $user =User::find($request->userid);

        $res_done=BlockReasonDeliveryBoy::updateOrCreate(
            ['user_id' => $request->userid],
            ['reason' => $request->reason]
        );

        $b_done = $user->update(['is_block' => 1]);
        // $data=array(
        //     'reason' =>$request->reason
        // );
        $data=array(
            'vendor' => $user->name ,
            'reason' =>$request->reason
        );

        /* if (!empty($user->email)) {
            Mail::to([$user->email])->send(new BlockDeliveryBoy($data));
        } */

        // push notification
        Notification::createNotificationNew($user->id,"Account Inactivated!","We have inactivated your account for ".$request->reason.".");

        Notification::create([
            'send_by'=>1,
            'send_to'=>$user->id,
            'notification_type'=>15,
            'title'=>"Account Inactivated!",
            'description'=>"We have Inactivated your account for ".$request->reason.".",
            'is_read'=>0
        ]);
        return redirect('admin/users');

    }

    public function UserUnBlockEmail(Request $request, User $adminUser) {
        $user =User::find($request->userid);
        $b_done = $user->update(['is_block' => 0]);

        if($b_done) {
            // push notification
            Notification::createNotificationNew($user->id,"Account Activated!","We have activated your account.");

            Notification::create([
                'send_by'=>1,
                'send_to'=>$user->id,
                'notification_type'=>15,
                'title'=>"Account Activated!",
                'description'=>"We have activated your account.",
                'is_read'=>0
            ]);

            return redirect('admin/users');
        }
    }
    
    public function update(UpdateDocuments $request, User $user)
    {
        $vehicle_status = $request->vehicle_status;
        $drivinglicence_status = $request->drivinglicence_status;
        $licence_no_plate_status = $request->licence_no_plate_status;
        $vehicle_insurence_status = $request->vehicle_insurence_status;

        $vehicle_reason = $request->vehicle_reason;
        $drivinglicence_reason = $request->drivinglicence_reason;
        $licence_no_plate_reason = $request->licence_no_plate_reason;
        $vehicle_insurence_reason = $request->vehicle_insurence_reason;

        if($vehicle_status==null)
        {
            $vehicle_status = 2;
        }
        if($drivinglicence_status==null)
        {
            $drivinglicence_status = 2;
        }
        if($licence_no_plate_status==null)
        {
            $licence_no_plate_status = 2;
        }
        if($vehicle_insurence_status==null)
        {
            $vehicle_insurence_status = 2;
        }
        // checking the condition of 1 due the checkbox input type
        if($vehicle_status==1)
        {
            $vehicle_status = 3;
        }
        if($drivinglicence_status==1)
        {
            $drivinglicence_status = 3;
        }
        if($licence_no_plate_status==1)
        {
            $licence_no_plate_status = 3;
        }
        if($vehicle_insurence_status==1)
        {
            $vehicle_insurence_status = 3;
        }


        if($vehicle_status==2&&$drivinglicence_status==2&&$licence_no_plate_status==2&&$vehicle_insurence_status==2)
        {
            if ($request->ajax()) {
                return [
                    'redirect' => url('admin/users')
                ];
            }
            return redirect('admin/users');
        }

        $exist = DeliveryBoyMainDocuments::where('user_id', $user->id)->first()->toArray();

        $exist['vehicle_status'] = $vehicle_status;
        $exist['drivinglicence_status'] = $drivinglicence_status;
        $exist['licence_no_plate_status'] = $licence_no_plate_status;
        $exist['vehicle_insurence_status'] = $vehicle_insurence_status;
        $exist['vehicle_reason'] = $vehicle_reason;
        $exist['drivinglicence_reason'] = $drivinglicence_reason;
        $exist['licence_no_plate_reason'] = $licence_no_plate_reason;
        $exist['vehicle_insurence_reason'] = $vehicle_insurence_reason;

        $create = DeliveryBoyTemporaryDocuments::create($exist);

        if($create)
        {
            User::find($user->id)->update(['is_block'=>1]);

            DeliveryBoyMainDocuments::where('user_id', $user->id)->first()->delete();

            $approve = 0;
            $reject = 0;

            if ($vehicle_status==2 || $drivinglicence_status==2 || $licence_no_plate_status==2 || $vehicle_insurence_status==2) {
                $approve = 1;
            }
            if ($vehicle_status==3 || $drivinglicence_status==3 || $licence_no_plate_status==3 || $vehicle_insurence_status==3) {
                $reject = 1;
            }

            if ($vehicle_status!=1 || $drivinglicence_status!=1 || $licence_no_plate_status!=1 || $vehicle_insurence_status!=1) {
                if (!empty($user->email)) {
                    $data=array(
                    'name' => $user->name,
                    'vehicle_status'=>$vehicle_status,
                    'drivinglicence_status'=>$drivinglicence_status,
                    'licence_no_plate_status'=>$licence_no_plate_status,
                    'vehicle_insurence_status'=>$vehicle_insurence_status,
                    'vehicle_reason'=>$vehicle_reason,
                    'drivinglicence_reason'=>$drivinglicence_reason,
                    'licence_no_plate_reason'=>$licence_no_plate_reason,
                    'vehicle_insurence_reason'=>$vehicle_insurence_reason,
                    'approve' => $approve,
                    'reject' => $reject
                );
                    // Mail::to([$user->email])->send(new DeliveryBoy($data));
                }
            }

            // push notification
            Notification::createNotificationNew($user->id,"Documents Approval process!","Admin have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!");

            Notification::create([
                'send_by'=>1,
                'send_to'=>$user->id,
                'notification_type'=>15,
                'title'=>"Documents Approval process!",
                'description'=>"Admin have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!",
                'is_read'=>0
            ]);

            if ($request->ajax()) {
                return [
                    'redirect' => url('admin/users'),
                    'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
                ];
            }

            return redirect('admin/users');
        }
    }
    
    public function bookings_vendor(IndexBookingDetail $request)
    {
        $userId = $request->user;

        $data = BookingDetail::where('deliveryBoyId', $userId)->with(['users','deliveryboy','shop'])->orderBy('id','desc')->get();
        // create and AdminListing instance for a specific model and

        $s = $data->toArray();
        array_walk_recursive($s, function(&$v,$k){
            if($k=='bookingType')
            {
                $v = BookingDetail::LEVELS[$v];
            }
            if($k=='status')
            {
                $v = BookingDetail::ORDER_STATUS[$v];
            }
        });
        $data = collect($s);

        $corres = 'users';

        return view('admin.booking-detail.bookings-vendor', ['data' => $data,'corr'=>$corres,'adminId'=>$userId]);
    }

    public function bookings_vendor_view($userid, $bookingid)
    {
        $booking = BookingDetail::where('deliveryBoyId',$userid)->where('id',$bookingid)->first();

        $totalGST = 0;
        if($booking->orderdetails){
            foreach($booking->orderdetails as $order){
                $totalGST += $order->gst_tax;
            }
        }

        $totalamount = $booking->amount_before_discount+$booking->platform_charge+$booking->tip_to_delivery_boy+$booking->delivery_charge_for_customer+$totalGST+$booking->pst+$booking->hst-$booking->discount_amount;
        if($booking)
        {
            $booking->totalamount = $totalamount;
            $booking->totalGST = $totalGST;

        }

        return view('admin.booking-detail.bookings-vendor-view', [
            'booking' => $booking,
            'parent'=>'users',
            'userid' => $userid
        ]);
    }
}
