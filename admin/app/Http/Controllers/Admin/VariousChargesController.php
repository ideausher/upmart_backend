<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VariousCharge\BulkDestroyVariousCharge;
use App\Http\Requests\Admin\VariousCharge\DestroyVariousCharge;
use App\Http\Requests\Admin\VariousCharge\IndexVariousCharge;
use App\Http\Requests\Admin\VariousCharge\StoreVariousCharge;
use App\Http\Requests\Admin\VariousCharge\UpdateVariousCharge;
use App\Models\VariousCharge;
use App\Models\VariousChargeRelation;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class VariousChargesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexVariousCharge $request
     * @return array|Factory|View
     */
    public function index(IndexVariousCharge $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(VariousCharge::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'chargeName', 'type', 'value'],

            // set columns to searchIn
            ['id', 'chargeName', 'value']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.various-charge.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.various-charge.create');

        return view('admin.various-charge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreVariousCharge $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreVariousCharge $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        if($sanitized['startRange'][0] != 0) {
            $response = json_decode('{"message":"Please start from 0."}', true);
            return response($response, 400);
        }
        if((empty($sanitized['startRange'][0]) && empty($sanitized['endRange'][0]) && empty($sanitized['price'][0])) && $sanitized['value'] == "") {

            if(empty( $sanitized['value'])){
                $response = json_decode('{"message":"Please add Value."}', true);
                return response($response, 400);
            }
            $response = json_decode('{"message":"Please add minimum one range and price."}', true);
            return response($response, 400);
        }
        $variousCharge = VariousCharge::create($sanitized);
        if(!empty($sanitized['startRange']) && !empty($sanitized['endRange']) && !empty($sanitized['price']) && $sanitized['startRange'][0] != '')
        {
            $variousChargeRelationData = array ();
            $i=0;
            foreach($sanitized['startRange'] as $start)
            {
                if(!empty($sanitized['startRange'][$i+1])){
                    if(($sanitized['endRange'][$i])+1 != $sanitized['startRange'][$i+1]) {

                        $response = json_decode('{"message":"Please add continous range."}', true);
                        return response($response, 400);
                    }
                }
                if(isset($start) && !empty($sanitized['endRange'][$i]) && !empty($sanitized['price'][$i]))
                {
                    $ChargeRelationData = array (
                    "various_charge_id" => $variousCharge->id,
                    "startRange" => $sanitized['startRange'][$i],
                    "endRange" => $sanitized['endRange'][$i],
                    "price" => $sanitized['price'][$i],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                );
                    $i++;
                    array_push($variousChargeRelationData, $ChargeRelationData);
                }
            }
            $variousChargeRelation = VariousChargeRelation::insert($variousChargeRelationData);
        }


        if ($request->ajax()) {
            return ['redirect' => url('admin/various-charges'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/various-charges');
    }

    /**
     * Display the specified resource.
     *
     * @param VariousCharge $variousCharge
     * @throws AuthorizationException
     * @return void
     */
    public function show(VariousCharge $variousCharge)
    {
        $this->authorize('admin.various-charge.show', $variousCharge);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param VariousCharge $variousCharge
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(VariousCharge $variousCharge)
    {
        $this->authorize('admin.various-charge.edit', $variousCharge);

        $variousrel = VariousChargeRelation::where('various_charge_id',$variousCharge->id)->get();

        if($variousrel)
        {
            $startRange = [];
            $endRange = [];
            $price = [];
            foreach($variousrel as $aq)
            {
                $startRange[] = $aq['startRange'];
                $endRange[] = $aq['endRange'];
                $price[] = $aq['price'];
            }
        }

        $dataPre = json_encode(array_merge(['startRange'=>$startRange,'endRange'=>$endRange,'price'=>$price],json_decode($variousCharge, true)));
// dd($dataPre);
        return view('admin.various-charge.edit', [
            'variousCharge' => $variousCharge,
            'dataPre'=>$dataPre
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateVariousCharge $request
     * @param VariousCharge $variousCharge
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateVariousCharge $request, VariousCharge $variousCharge)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        $sanitized = $request->getSanitized();
        if(isset($sanitized['startRange'][0]) && $sanitized['startRange'][0]!=0) {
            $response = json_decode('{"message":"Please start from 0."}', true);
            return response($response, 400);
        }
        if(isset($santized['startRange']) && (($sanitized['startRange'][0] == '') && ($sanitized['end'][0]=='') && ($sanitized['price'][0]=='')) && $sanitized['value'] == "") {

            if(empty( $sanitized['value'])){
                $response = json_decode('{"message":"Please add Value."}', true);
                return response($response, 400);
            }

            $response = json_decode('{"message":"Please add minimum one range and price."}', true);
            return response($response, 400);
        }

        // Update changed values VariousCharge

        if(isset($sanitized['startRange']))
        {
            VariousChargeRelation::where('various_charge_id', $variousCharge->id)->delete();
            $variousChargeRelationData = array ();
            $i=0;
            foreach($sanitized['startRange'] as $start)
            {
                if(!empty($sanitized['startRange'][$i+1])){
                    if(($sanitized['endRange'][$i])+1 != $sanitized['startRange'][$i+1]) {

                        $response = json_decode('{"message":"Please add continous range."}', true);
                        return response($response, 400);
                    }
                    if(($sanitized['price'][$i] == $sanitized['price'][$i+1])){
                        $response = json_decode('{"message":"Price of Continous range cannot be same."}', true);
                        return response($response, 400);
                    }
                    $unique = array_unique($sanitized['price']);
                    $duplicates = array_diff_assoc($sanitized['price'], $unique);
                    if($duplicates){
                        $response = json_decode('{"message":"Please add different Price for all ranges."}', true);
                        return response($response, 400);
                    }
                }

                if(isset($start) && !empty($sanitized['endRange'][$i]) && !empty($sanitized['price'][$i]))
                {
                    $ChargeRelationData = array (
                    "various_charge_id" => $variousCharge->id,
                    "startRange" => $sanitized['startRange'][$i],
                    "endRange" => $sanitized['endRange'][$i],
                    "price" => $sanitized['price'][$i],
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                );
                    $i++;
                    array_push($variousChargeRelationData, $ChargeRelationData);
                }
            }
            $variousChargeRelation = VariousChargeRelation::insert($variousChargeRelationData);
        }

        $variousCharge->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/various-charges'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/various-charges');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyVariousCharge $request
     * @param VariousCharge $variousCharge
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyVariousCharge $request, VariousCharge $variousCharge)
    {
        $variousCharge->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyVariousCharge $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyVariousCharge $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    VariousCharge::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
