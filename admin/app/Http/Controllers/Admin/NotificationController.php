<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminFcmToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function saveFcmToken(Request $request)
    {
        $fcm_token = $request->fcm_token;
        $user_id = $request->userId;
        if (!$fcm_token) {
            return response(['message' => 'token not found'], 404);
        }
        $admin_token = AdminFcmToken::where([['token', '=', $fcm_token], ['userId', '=', $user_id]])->first();
        if ($admin_token)
            return response(['message' => 'token already saved', 'token' => $admin_token]);
        else {
            $admin_token = new AdminFcmToken();
            $admin_token->token = $fcm_token;
            $admin_token->userId = $request->userId;
            $admin_token->merchant_type = $request->merchant_type ?? null;
            $admin_token->save();
            return response(['message' => 'fcm token created']);
        }

    }
}
