<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Coupon\BulkDestroyCoupon;
use App\Http\Requests\Admin\Coupon\DestroyCoupon;
use App\Http\Requests\Admin\Coupon\IndexCoupon;
use App\Http\Requests\Admin\Coupon\StoreCoupon;
use App\Http\Requests\Admin\Coupon\UpdateCoupon;
use App\Models\Coupon;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\Models\AdminVendor;
use App\Models\Notification;
use App\Models\BookingDetail;

class CouponsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCoupon $request
     * @return array|Factory|View
     */
    public function index(IndexCoupon $request)
    {
        
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Coupon::class)->modifyQuery(function($query) use ($request){
            $user = Auth::user();
            $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();
            if($user->hasRole('vendor'))
            {
                $query->where('shopId',$shop_id);
            }
            else
            {
                //
            }
            
           
        })->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['coupon_code', 'coupon_description', 'coupon_discount', 'coupon_max_amount', 'coupon_min_amount', 'coupon_name', 'coupon_type', 'end_date', 'id', 'maximum_per_customer_use', 'maximum_total_use', 'start_date','shopId','isPrimary','user_id'],

            // set columns to searchIn
            ['coupon_code', 'coupon_description', 'coupon_name', 'id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.coupon.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.coupon.create');
        $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($chars), 0, 6);
        $array = ['coupon_code'=>$code];

        $user = Auth::user();

        $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();

        if($user->hasRole('vendor'))
        {
            $primary_exist = Coupon::where('shopId',$shop_id)->where('isPrimary',2)->get()->count();
            $primaryName = Coupon::select('coupon_code')->where('shopId',$shop_id)->where('isPrimary',2)->first();

            if(!empty($primary_exist)&&!empty($primaryName->coupon_code))
            {
                $primaryName = $primaryName->coupon_code;
                $primary_exist = $primary_exist;
            }
            else
            {
                $primary_exist = "";
                $primaryName = "";
            }
            
            $role = 'vendor';
        }
        else
        {
            $primary_exist = "";
            $primaryName = "";

            $role = 'admin';
        }

        $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();

        $array = array_merge($array,['shopId'=>$shop_id]);

        return view('admin.coupon.create',['code'=>json_encode($array),'role'=>$role,'primaryExist' => $primary_exist,'primaryName'=>$primaryName]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCoupon $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCoupon $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        // echo $sanitized['isPrimary'];die;
        if(isset($sanitized['isPrimary'])&&$sanitized['isPrimary']==1)
        {
            $sanitized['isPrimary']=2;
        }
        elseif(isset($sanitized['isPrimary'])&&$sanitized['isPrimary']!=2)
        {
            $sanitized['isPrimary'] = 3;
        }
        else
        {
            $sanitized['isPrimary'] = 3;
        }

        $user = Auth::user();

        if($sanitized['isPrimary']==2)
        {

            $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();

            if($user->hasRole('vendor'))
            {
                $primary = Coupon::where('shopId',$shop_id)->where('isPrimary',2)->first();

                if(!empty($primary))
                {
                    $primary->update(['isPrimary'=>3]);
                }
            }
        }

        if(isset($sanitized['coupon_name']))
            $sanitized['coupon_name'] = ucfirst($sanitized['coupon_name']);

        // Store the Coupon
        $coupon = Coupon::create($sanitized);

        if($coupon && $user->hasRole('vendor'))
        {
            $shopid = $coupon->shopId;

            $favUserIds = DB::table('fav_unfac_shops')->leftJoin('users','users.id','=','fav_unfac_shops.user_id')->where('fav_unfac_shops.shop_id',$shopid)->where('users.is_block',0)->where('fav_unfac_shops.isFavourite',1)->pluck('fav_unfac_shops.user_id')->toArray();
            $bookinguser = BookingDetail::leftJoin('users','users.id','=','booking_details.userId')->where('booking_details.shopId',$shopid)->where('users.is_block',0)->groupBy('booking_details.userId')->pluck('booking_details.userId')->toArray();
            $allusers = array_unique(array_merge($favUserIds,$bookinguser));
            $title = 'New coupon created';
            $message = 'Your fav shop has just created a new coupon for you!';

            if($allusers)
            {
                Notification::createNotificationNew($allusers, $title, $message);

                if($allusers)
                {
                    foreach($allusers as $uId)
                    {
                        $notification = new Notification;
                        $notification->send_by = $shopid ?? 0;
                        $notification->send_to = $uId ?? 0;
                        $notification->notification_type = 0;
                        $notification->title = $title;
                        $notification->description = $message;
                        $notification->is_read = 0;
                        $notification->save();
                    }
                }
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/coupons'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/coupons');
    }

    /**
     * Display the specified resource.
     *
     * @param Coupon $coupon
     * @throws AuthorizationException
     * @return void
     */
    public function show(Coupon $coupon)
    {
        $this->authorize('admin.coupon.show', $coupon);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Coupon $coupon
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Coupon $coupon)
    {
        $this->authorize('admin.coupon.edit', $coupon);

        $user = Auth::user();

        $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();

        if($user->hasRole('vendor'))
        {
            $primary_exist = Coupon::where('id','!=',$coupon->id)->where('shopId',$shop_id)->where('isPrimary',2)->get()->count();
            $primaryName = Coupon::select('coupon_code')->where('id','!=',$coupon->id)->where('shopId',$shop_id)->where('isPrimary',2)->first();

            if(!empty($primary_exist)&&!empty($primaryName->coupon_code))
            {
                $primaryName = $primaryName->coupon_code;
                $primary_exist = $primary_exist;
            }
            else
            {
                $primary_exist = "";
                $primaryName = "";
            }

            $role = 'vendor';
        }
        else
        {
            $primary_exist = "";
            $primaryName = "";

            $role = 'admin';
        }

        if($coupon->isPrimary==3)
        {
            $coupon->isPrimary =  0;
        }
        else
        {
            $coupon->isPrimary =  1;
        }
        return view('admin.coupon.edit', [
            'coupon' => $coupon,
            'role'=>$role,
            'primaryExist' => $primary_exist,
            'primaryName'=>$primaryName
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCoupon $request
     * @param Coupon $coupon
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCoupon $request, Coupon $coupon)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
// echo $sanitized['isPrimary'];die;
        if(isset($sanitized['isPrimary'])&&$sanitized['isPrimary']==1)
        {
            $sanitized['isPrimary']=2;
        }
        elseif(isset($sanitized['isPrimary'])&&$sanitized['isPrimary']!=2)
        {
            $sanitized['isPrimary'] = 3;
        }
        else
        {
            $sanitized['isPrimary'] = 3;
        }

        $user = Auth::user();

        if($sanitized['isPrimary']==2)
        {

            $shop_id = AdminVendor::where('user_id',$user->id)->pluck('id')->first();

            if($user->hasRole('vendor'))
            {
                $primary = Coupon::where('id','!=',$coupon->id)->where('shopId',$shop_id)->where('isPrimary',2)->first();

                if(!empty($primary))
                {
                    $primary->update(['isPrimary'=>3]);
                }
                
            }
        }

        if(isset($sanitized['coupon_name']))
            $sanitized['coupon_name'] = ucfirst($sanitized['coupon_name']);
        
        // Update changed values Coupon
        $coupon->update($sanitized);

        if($coupon && $user->hasRole('vendor'))
        {
            $shopid = $coupon->shopId;

            $favUserIds = DB::table('fav_unfac_shops')->leftJoin('users','users.id','=','fav_unfac_shops.user_id')->where('fav_unfac_shops.shop_id',$shopid)->where('users.is_block',0)->where('fav_unfac_shops.isFavourite',1)->pluck('fav_unfac_shops.user_id')->toArray();
            $bookinguser = BookingDetail::leftJoin('users','users.id','=','booking_details.userId')->where('booking_details.shopId',$shopid)->where('users.is_block',0)->groupBy('booking_details.userId')->pluck('booking_details.userId')->toArray();
            $allusers = array_unique(array_merge($favUserIds,$bookinguser));
            $title = 'New coupon created';
            $message = 'Your fav shop has just created a new coupon for you!';

            if($allusers)
            {
                Notification::createNotificationNew($allusers, $title, $message);

                if($allusers)
                {
                    foreach($allusers as $uId)
                    {
                        $notification = new Notification;
                        $notification->send_by = $shopid ?? 0;
                        $notification->send_to = $uId ?? 0;
                        $notification->notification_type = 0;
                        $notification->title = $title;
                        $notification->description = $message;
                        $notification->is_read = 0;
                        $notification->save();
                    }
                }
            }
        }

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/coupons'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/coupons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCoupon $request
     * @param Coupon $coupon
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCoupon $request, Coupon $coupon)
    {
        $coupon->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCoupon $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCoupon $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('coupons')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function view($id)
    {
        $details = Coupon::find($id);

        if($details)
        {
            $mediaData = $details->getMedia('coupon_image');
            foreach($mediaData as $md){
                $publicUrl = $md->getUrl();
                $details->coupon_image = $publicUrl;
            }
        }

        return view('admin.coupon.view', ['details' => $details]);
    }
}
