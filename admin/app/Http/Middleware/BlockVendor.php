<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\BlockedException;
use App\Models\AdminUsers;
use Illuminate\Support\Facades\Auth;

class BlockVendor
{
    const Blocked = 1;
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((Auth::user()) && (Auth::user()->forbidden == BlockVendor::Blocked))
        {
            return redirect('/admin/logout');
        }

        return $next($request);
    }

}
