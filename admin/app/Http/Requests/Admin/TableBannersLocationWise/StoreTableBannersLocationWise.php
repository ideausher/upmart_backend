<?php

namespace App\Http\Requests\Admin\TableBannersLocationWise;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreTableBannersLocationWise extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.table-banners-location-wise.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'bannerName' => ['required', 'string'],
            'link' => ['nullable', 'string', 'url'],
            'address' => ['nullable', 'string'],
            'latitude' => ['nullable', 'string'],
            'longitude' => ['nullable', 'string'],
            
        ];
    }

    public function messages()
    {
        return [
            'bannerName.required'=>'The Banner name field is required.',
            'link.nullable'=>'The Link field is required.',
            'link.url'=>'The Link field should has URL value.',
            'address.required'=>'The Address field is required.',
            'latitude.required'=>'The Latitude field is required.',
            'longitude.required'=>'The Longitude field is required.'
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
