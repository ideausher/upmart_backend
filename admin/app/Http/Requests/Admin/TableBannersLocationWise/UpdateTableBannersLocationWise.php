<?php

namespace App\Http\Requests\Admin\TableBannersLocationWise;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateTableBannersLocationWise extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.table-banners-location-wise.edit', $this->tableBannersLocationWise);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'bannerName' => ['sometimes', 'string'],
            'link' => ['nullable', 'string', 'url'],
            'address' => ['nullable', 'string'],
            'latitude' => ['nullable', 'string'],
            'longitude' => ['nullable', 'string'],
            
        ];
    }

    public function messages()
    {
        return [
            'bannerName.sometimes'=>'The Banner name field is required.',
            'link.nullable'=>'The Link field is required.',
            'link.url'=>'The Link field should has URL value.',
            'address.sometimes'=>'The Address field is required.',
            'latitude.sometimes'=>'The Latitude field is required.',
            'longitude.sometimes'=>'The Longitude field is required.'
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
