<?php

namespace App\Http\Requests\Admin\AdminVendor;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreAdminVendor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.admin-vendor.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'country_code'=> 'required|nullable|string|regex:/^(\+?\d{1,3}|\d{1,4})$/',
            
            'phonenumber_with_countrycode' => 'required|string|regex:/^([0-9]+)$/|unique:admin_vendors,phonenumber_with_countrycode|min:8|max:14',

            'country_iso_code' => 'required|nullable|string|regex:/^([a-zA-Z]+)$/',
            'shop_name' => 'sometimes|string',
            'shop_address' => 'sometimes|string' ,
            'additional_address' => 'sometimes|string',
                
            
                
        ];

        if (Config::get('admin-auth.activation_enabled')) {
            $rules['activated'] = ['required', 'boolean'];
        }

        return ($rules);
    }

    public function messages()
    {
        return [
            'country_code.required'=>'This CountryCode is required',
            'phonenumber_with_countrycode.required' =>'Please Enter your Phone Number',
            'country_iso_code'=>'Country ISO code is required'
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
