<?php

namespace App\Http\Requests\Admin\AdminVendor;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateAdminVendor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.admin-vendor.edit', $this->adminVendor);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [  
            'country_code'=> 'sometimes|nullable|string|regex:/^(\+?\d{1,3}|\d{1,4})$/',
            'phonenumber_with_countrycode' => 'sometimes |string|regex:/^([0-9]+)$/|unique:admin_vendors,phonenumber_with_countrycode|min:8|max:14',
            'country_iso_code' => 'sometimes|nullable|required_with:phone_number|string|regex:/^([a-zA-Z]+)$/',
            'shop_name' => 'sometimes|string',
            'shop_address' => 'sometimes|string' ,
            'additional_address' => 'sometimes|string',
                
            
                
        ];
        
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
