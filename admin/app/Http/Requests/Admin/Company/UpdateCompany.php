<?php

namespace App\Http\Requests\Admin\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.company.edit', $this->company);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'company_name' => ['sometimes', Rule::unique('companies', 'company_name')->ignore($this->company->getKey(), $this->company->getKeyName()), 'string'],
            'company_description' => ['sometimes', 'string'],
            
        ];
    }

    public function messages()
    {
        return [
            'company_name.sometimes'=>'Company name is required',
            'company_name.unique'=>'The Company name has already been taken.',
            'company_description.sometimes'=>'Company description is required',
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
