<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class StoreResetPassword extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
       
        $rules = [  
            'email' => ['required','email']  
        ];

        return ($rules);
    }

    public function messages()
    {
        
        return [
          
            'email.required' =>'Email is required',
            
        ];
   
        }
    /**
     * Modify input data
     *
     * @return array
     */
    public function getModifiedData(): array
    {
 
        $data = $this->only(collect($this->rules())->keys()->all());
    
        return $data;
    }
}
