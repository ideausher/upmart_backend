<?php

namespace App\Http\Requests\Admin\Coupon;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCoupon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.coupon.edit', $this->coupon);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'coupon_code' => ['required', 'string'],
            'coupon_description' => ['required', 'string'],
            'coupon_discount' => ['required', 'numeric'],
            'coupon_max_amount' => ['required', 'numeric'],
            'coupon_min_amount' => ['required', 'numeric'],
            'coupon_name' => ['required', 'string'],
            'coupon_type' => ['required', 'boolean'],
            'end_date' => ['required', 'date'],
            'maximum_per_customer_use' => ['required', 'integer'],
            'maximum_total_use' => ['required', 'integer'],
            'start_date' => ['required', 'date'],
            'shopId' => ['nullable'],
            'isPrimary' => ['nullable']
        ];
    }

    public function messages()
    {
        return [
            'coupon_code.required' => 'The Coupon code field is required.',
            'coupon_description.required' => 'The Coupon description field is required.',
            'coupon_discount.required' => 'The Coupon discount field is required.',
            'coupon_discount.regex' => 'The Coupon discount should be in integer or decimal value.',
            'coupon_max_amount.regex' => 'The Coupon min amount should be in integer or decimal value.',
            'coupon_min_amount.regex' => 'The Coupon max amount should be in integer or decimal value.',
            'coupon_max_amount.required' => 'The Coupon min amount is required.',
            'coupon_min_amount.required' => 'The Coupon max amount is required.',
            'coupon_name.required' => 'The Coupon name field is required.',
            'coupon_type.required' => 'The Coupon type field is required.',
            'coupon_type.boolean' => 'The Coupon type should be in integer value.',
            'end_date.required' => 'The End date is required.',
            'end_date.date' => 'The End date should be in date format.',
            'maximum_per_customer_use.required' => 'The Maximum per customer use is required.',
            'maximum_total_use.required' => 'The Maximum total use is required.',
            'maximum_per_customer_use.integer' => 'The Maximum per customer use should be in integer value.',
            'maximum_total_use.integer' => 'The Maximum total use should be in integer value.',
            'start_date.date' => 'The Start date should be in date format.',
            'start_date.required' => 'The Start date is required.',
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
