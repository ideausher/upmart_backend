<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateDocuments extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.item.edit', $this->item);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'vehicle_status' => ['nullable'],
            'drivinglicence_status' => ['nullable'],
            'licence_no_plate_status' => ['nullable'],
            'vehicle_insurence_status' => ['nullable'],
            'vehicle_insurence_reason' => ['nullable','string'],
            'licence_no_plate_reason' => ['nullable','string'],
            'drivinglicence_reason' => ['nullable','string'],
            'vehicle_reason' => ['nullable','string']
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
