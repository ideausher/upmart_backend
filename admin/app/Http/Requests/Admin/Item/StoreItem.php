<?php

namespace App\Http\Requests\Admin\Item;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.item.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/"; 
        return [
            'product_name' => ['required', 'string', Rule::unique('items', 'product_name'),],
            'product_description' => ['nullable', 'string'],
            'stock_quantity' => ['required', 'numeric'],
            'price' => ['required', 'regex:'.$regex],
            'product_image' => ['nullable', 'string'],
            'company_name'=>['required'],
            'shop_id'=>['required'],
            'shop_name'=>['required'],
            'category'=>['required'],
            'discount' => ['nullable', 'regex:'.$regex],
            'new_price' => ['nullable', 'regex:'.$regex],
        ];
    }

    public function messages()
    {
        return [
            'product_name.required'=>'The Product name field is required',
            'product_name.unique'=>'The Product name is already taken',
            'company_name.required'=>'The Company name field is required',
            'category.required'=>'The Category field is required',
            'stock_quantity.required'=>'The Stock quantity field is required',
            'price.required'=>'The Price field is required',
            'discount.regex'=>'The Discount field should have integer or decimal value',
            'new_price.regex'=>'The New price field should have integer or decimal value',
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
