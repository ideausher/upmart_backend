<?php

namespace App\Http\Requests\Admin\VariousCharge;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreVariousCharge extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.various-charge.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/"; 
        return [
            'chargeName' => ['required', 'string'],
            'type' => ['required', 'integer'],
            'value' => ['nullable', 'regex:'.$regex],
            'startRange' => ['nullable', 'array'],
            'endRange' => ['nullable', 'array'],
            'price' => ['nullable', 'array'],
        ];
    }

    public function messages()
    {
        return [
            'chargeName.required' => 'The Charge name field is required.',
            'type.required' => 'The Type field is required.',
            'type.integer' => 'The Type field should be in integer value.',
            'value.required' => 'The Value field is required.',
            'value.regex' => 'The Value field should be in integer or decimal value.',
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
