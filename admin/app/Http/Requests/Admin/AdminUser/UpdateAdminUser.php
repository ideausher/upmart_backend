<?php

namespace App\Http\Requests\Admin\AdminUser;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UpdateAdminUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('admin.admin-user.edit', $this->adminUser);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/"; 
        $rules = [
            'first_name' => ['required ', 'string','regex:/^([a-z A-Z]+)$/'],
            'last_name' => ['required', 'string','regex:/^([a-z A-Z]+)$/'],
            'email' => ['sometimes', 'email', Rule::unique('admin_users', 'email')->ignore($this->adminUser->getKey(), $this->adminUser->getKeyName()), 'string'],
            'password' => ['required', 'min:6', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/', 'string'],
            // 'forbidden' => ['sometimes', 'boolean'],
            // 'language' => ['sometimes', 'string'],
            'password_confirmation' => ['required','required_with:password','same:password'] , 
            // 'roles' => ['sometimes', 'array'],
            'country_code'=> 'required|nullable|string',
            'country_iso_code'=> 'required|nullable|string',
            'phonenumber_with_countrycode' => 'required|string|regex:/^([0-9]+)$/|min:10|max:12',
            'shop_name' => 'sometimes|required',
            'shop_address' => 'sometimes|required' ,
            'additional_address' => 'required',
            'description' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'shop_type' => 'required',
            'platform_charges' => ['nullable','regex:'.$regex],
            'commission_charges' => ['nullable','regex:'.$regex],
            'gst' => ['nullable','regex:'.$regex],
            'pst' => ['nullable','regex:'.$regex],
            'hst' => ['nullable','regex:'.$regex],
        ];

        if (Config::get('admin-auth.activation_enabled')) {
            $rules['activated'] = ['required', 'boolean'];
        }

        return $rules;
    }
    public function messages()
    {
      
        return [
            'first_name.required' => 'First name required',
            'last_name.required'=>'Last name required',
            'email.required' =>'Email is required',
            'email.email' =>'Email is in incorrect format',
            'email.unique' =>'Email is already taken',
            'password.required' =>'Password is required',
            'password.regex' => 'Password must contain at least one number,one uppercase,one lowercase letters and one special character.',
            'password_confirmation.required'=>'Password confirmation is required',
            'password_confirmation.required_with' => "Password confirmation is required",
            'password_confirmation.same' => "Password confirmation doesn't match",
            'password_confirmation.regex' => 'Password confirmation must contain at least one number,one uppercase,one lowercase letters and one special character.',
            'country_code.required'=>'This Country code is required',
            'country_iso_code.required'=>'This Country code field is required',
            'phonenumber_with_countrycode.required' =>'Phone number is required',
            'phonenumber_with_countrycode.regex' =>'Please Enter valid Phone number',
            'shop_name.required'=>'Shop name is required',
            'additional_address.required' =>'Additional address required',
            'description.required' =>'Description required',
            'lat.required' =>'Latitude is required',
            'lng.required' =>'Longitude is required',
            'shop_type.required' =>'Shop type is required',
            'platform_charges.regex' => 'The Platform charges field should have integer or decimal value',
            'commission_charges.regex' => 'The Commission charges field should have integer or decimal value',
            'gst.regex' => 'The GST field should have integer or decimal value',
            'pst.regex' => 'The PST field should have integer or decimal value',
            'hst.regex' => 'The HST field should have integer or decimal value'
        ];
   
        }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getModifiedData(): array
    {
        $data = $this->only(collect($this->rules())->keys()->all());
        if (!Config::get('admin-auth.activation_enabled')) {
            $data['activated'] = true;
        }
        if (array_key_exists('password', $data) && empty($data['password'])) {
            unset($data['password']);
        }
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }
        return $data;
    }
}
