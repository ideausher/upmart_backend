<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.profile.edit', $this->adminUser);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'country_code'=>['required','numeric'],
            'country_iso_code'=>['required', 'string'],
            'phonenumber_with_countrycode'=>['required','string','regex:/^([0-9]+)$/','min:10','max:12']
        ];
    }

    public function messages()
    {
      
        return [
            'first_name.required' => 'First name required',
            'last_name.required'=>'Last name required',
            'country_code.required'=>'This Country code is required',
            'phonenumber_with_countrycode.required' =>'Phone number is required',
            'phonenumber_with_countrycode.regex' =>'Please Enter valid Phone number'
   
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
