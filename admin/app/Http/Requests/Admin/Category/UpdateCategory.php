<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.category.edit', $this->category);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_name' => ['sometimes', Rule::unique('categories', 'category_name')->ignore($this->category->getKey(), $this->category->getKeyName()), 'string'],
            'category_description' => ['sometimes', 'string'],
            
        ];
    }

    public function messages()
    {
        return [
            'category_name.sometimes'=>'Category name is required',
            'category_name.unique'=>'The Category name has already been taken.',
            'category_description.sometimes'=>'Category description is required',
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
