<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function ($query) {
            $sqlQuery = [
                $query->sql,
                $query->bindings,
                $query->time
            ];
            \File::append(storage_path('logs' . DIRECTORY_SEPARATOR . "AdminQueryLogger"), "****\n" . json_encode($sqlQuery)."\n");
        });
    }
}
