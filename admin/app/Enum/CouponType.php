<?php

namespace App\Enum;

class CouponType
{
	const GlobalCoupon = 1;
	const BothGlobalAndNonGlobalCoupon = 0;
	const PrimayCoupon = 2;
	const NonPrimaryCoupon = 3;
	const BothPrimaryAndNonPrimaryCoupon = 4;
}
