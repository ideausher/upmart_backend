<?php

namespace App\Traits;

use \NumberFormatter;

trait Currency{
    
    public function convert($amount)
    {
        $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );
        return $fmt->formatCurrency($amount,"CAD");   
    }
}


?>