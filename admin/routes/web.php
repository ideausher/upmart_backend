<?php

use Illuminate\Support\Facades\Route;
use App\Mail;
use Illuminate\Http\Request;
//  use Brackets\AdminAuth\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('upload', function (Request $request) {
    if ($request->hasFile('file')) {
        $file = $request->file('file');
        $filename = time() . str_replace(" ","",$file->getClientOriginalName());
        //store url
        $storagePath = Storage::disk('s3')->put($filename, file_get_contents($file));
        $path = Storage::disk('s3')->url($filename);
        return response()->json(['path' => $path], 200);
    }
    return response()->json(trans('brackets/media::media.file.not_provided'), 422);
})->name('brackets/media::upload');

Route::middleware(['web'])->group(static function () {
    Route::namespace('App\Http\Controllers\Admin')->group(static function () {
        Route::post('/admin/login', 'LoginController@Login');
        Route::get('/admin/logout', 'LoginController@logout');
    });
});

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->get('/', function () {

});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {

            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/block',                'AdminUsersController@adminUserBlockEmail')->name('adminUserBlockEmail');
            Route::get('/unblock',                'AdminUsersController@adminUserUnBlockEmail')->name('adminUserUnBlockEmail');
            Route::get('/{adminUser}/items',                'AdminUsersController@items')->name('items');
            Route::get('/{adminUser}/items/{item}/details',                'AdminUsersController@itemdetails')->name('details');
            Route::get('/{adminUser}/bookings',                'AdminUsersController@bookings_vendor')->name('bookings');
            Route::get('/{adminUser}/bookings/{booking}/view',                'AdminUsersController@bookings_vendor_view')->name('bookings-view');

        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('categories')->name('categories/')->group(static function() {
            Route::get('/',                                             'CategoriesController@index')->name('index');
            Route::get('/create',                                       'CategoriesController@create')->name('create');
            Route::post('/',                                            'CategoriesController@store')->name('store');
            Route::get('/{category}/edit',                              'CategoriesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CategoriesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{category}',                                  'CategoriesController@update')->name('update');
            Route::delete('/{category}',                                'CategoriesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('companies')->name('companies/')->group(static function() {
            Route::get('/',                                             'CompaniesController@index')->name('index');
            Route::get('/create',                                       'CompaniesController@create')->name('create');
            Route::post('/',                                            'CompaniesController@store')->name('store');
            Route::get('/{company}/edit',                               'CompaniesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CompaniesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{company}',                                   'CompaniesController@update')->name('update');
            Route::delete('/{company}',                                 'CompaniesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('items')->name('items/')->group(static function() {
            Route::get('/',                                             'ItemsController@index')->name('index');
            Route::get('/create',                                       'ItemsController@create')->name('create');
            Route::post('/',                                            'ItemsController@store')->name('store');
            Route::get('/{item}/edit',                                  'ItemsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ItemsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{item}',                                      'ItemsController@update')->name('update');
            Route::delete('/{item}',                                    'ItemsController@destroy')->name('destroy');
            Route::get('/disable',                                      'ItemsController@disable')->name('disable');
            Route::get('/enable',                                       'ItemsController@enable')->name('enable');
            Route::get('/import',                                       'ItemsController@import')->name('import');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-vendors')->name('admin-vendors/')->group(static function() {
            Route::get('/',                                             'AdminVendorController@index')->name('index');
            Route::get('/create',                                       'AdminVendorController@create')->name('create');
            Route::post('/',                                            'AdminVendorController@store')->name('store');
            Route::get('/{adminVendor}/edit',                           'AdminVendorController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AdminVendorController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{adminVendor}',                               'AdminVendorController@update')->name('update');
            Route::delete('/{adminVendor}',                             'AdminVendorController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('users')->name('users/')->group(static function() {
            Route::get('/',                                             'UsersController@index')->name('index');
            Route::get('/{user}/view',                                  'UsersController@view')->name('view');
            Route::get('/block',                'UsersController@UserBlockEmail')->name('UserBlockEmail');
            Route::get('/unblock',                'UsersController@UserUnBlockEmail')->name('UserUnBlockEmail');
            Route::post('/{user}/update',                                      'UsersController@update')->name('update');
            Route::get('/{user}/bookings',                'UsersController@bookings_vendor')->name('bookings');
            Route::get('/{user}/bookings/{booking}/view',                'UsersController@bookings_vendor_view')->name('bookings-view');
        });
    });
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('stripe-tokens')->name('stripe-tokens/')->group(static function() {
            Route::get('/',                                             'StripeTokenController@index')->name('index');
            Route::get('/create',                                       'StripeTokenController@create')->name('create');
            Route::post('/',                                            'StripeTokenController@store')->name('store');
            Route::get('/{stripeToken}/edit',                           'StripeTokenController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'StripeTokenController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{stripeToken}',                               'StripeTokenController@update')->name('update');
            Route::delete('/{stripeToken}',                             'StripeTokenController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('stripe-connect')->name('stripe-connect/')->group(static function() {
            Route::get('/',                                             'StripeConnectController@index')->name('index');
            Route::get('/connect-stripe',                                             'StripeConnectController@connect_stripe')->name('connect-stripe');
        });
    });
});




Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
    Route::prefix('stripe-connect')->name('stripe-connect/')->group(static function() {
        Route::get('/connect-stripe-app',                                             'StripeConnectController@stripe_append')->name('connect-stripe-app');
    });
});



/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('coupons')->name('coupons/')->group(static function() {
            Route::get('/',                                             'CouponsController@index')->name('index');
            Route::get('/create',                                       'CouponsController@create')->name('create');
            Route::post('/',                                            'CouponsController@store')->name('store');
            Route::get('/{coupon}/edit',                                'CouponsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CouponsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{coupon}',                                    'CouponsController@update')->name('update');
            Route::delete('/{coupon}',                                  'CouponsController@destroy')->name('destroy');
            Route::get('/{coupon}/view',                                       'CouponsController@view')->name('view');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('customers')->name('customers/')->group(static function() {
            Route::get('/',                                             'CustomersController@index')->name('index');
            Route::get('/{customerId}/detail',                             'CustomersController@show')->name('detail');
            Route::get('/{customerId}/bookings',                'CustomersController@bookings_vendor')->name('bookings');
            Route::get('/{customerId}/bookings/{booking}/view',                'CustomersController@bookings_vendor_view')->name('bookings-view');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/shop-detail',                                      'ShopDetailController@editshopdetail')->name('edit-shop-detail');
        Route::post('/shop-detail',                                     'ShopDetailController@updateshopdetail')->name('update-shop-detail');
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('verification-panel')->name('verification-panel/')->group(static function() {
            Route::get('/',                                             'VerificationPanelController@index')->name('index');
            Route::get('/{user}/view',                                  'VerificationPanelController@view')->name('view');
            Route::post('/{user}/update',                                      'VerificationPanelController@update')->name('update');
        });
    });
});

Route::get('/admin/password-reset','App\Http\Controllers\Admin\PasswordResetController@index');
Route::post('/admin/password-reset/send','App\Http\Controllers\Admin\PasswordResetController@send');
Route::get('/admin/password-reset/{reset}/reset','App\Http\Controllers\Admin\PasswordResetController@reset');
Route::post('/admin/password-reset/update','App\Http\Controllers\Admin\PasswordResetController@update');


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('booking-details')->name('booking-details/')->group(static function() {
            Route::get('/',                                             'BookingDetailsController@index')->name('index');
            Route::post('/bulk-destroy',                                'BookingDetailsController@bulkDestroy')->name('bulk-destroy');
            Route::delete('/{bookingDetail}',                           'BookingDetailsController@destroy')->name('destroy');
            Route::get('/{bookingDetail}/view',                                  'BookingDetailsController@view')->name('view');
            Route::get('/accept',                                      'BookingDetailsController@accept')->name('accept');
            Route::get('/reject',                                      'BookingDetailsController@reject')->name('reject');
            Route::get('/change-status',                                      'BookingDetailsController@change_status')->name('change-status');
            Route::get('/{bookingDetail}/confirm-otp',                           'BookingDetailsController@confirm_otp')->name('confirm-otp');
            Route::post('/{bookingDetail}/verify-otp',                           'BookingDetailsController@verify_otp')->name('verify-otp');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('various-charges')->name('various-charges/')->group(static function() {
            Route::get('/',                                             'VariousChargesController@index')->name('index');
            Route::get('/create',                                       'VariousChargesController@create')->name('create');
            Route::post('/',                                            'VariousChargesController@store')->name('store');
            Route::get('/{variousCharge}/edit',                         'VariousChargesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'VariousChargesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{variousCharge}',                             'VariousChargesController@update')->name('update');
            Route::delete('/{variousCharge}',                           'VariousChargesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('table-banners-location-wises')->name('table-banners-location-wises/')->group(static function() {
            Route::get('/',                                             'TableBannersLocationWiseController@index')->name('index');
            Route::get('/create',                                       'TableBannersLocationWiseController@create')->name('create');
            Route::post('/',                                            'TableBannersLocationWiseController@store')->name('store');
            Route::get('/{tableBannersLocationWise}/edit',              'TableBannersLocationWiseController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TableBannersLocationWiseController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{tableBannersLocationWise}',                  'TableBannersLocationWiseController@update')->name('update');
            Route::delete('/{tableBannersLocationWise}',                'TableBannersLocationWiseController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('help-and-supports')->name('help-and-supports/')->group(static function() {
            Route::get('/',                                             'HelpAndSupportsController@index')->name('index');
            Route::get('/create',                                       'HelpAndSupportsController@create')->name('create');
            Route::post('/',                                            'HelpAndSupportsController@store')->name('store');
            Route::get('/{helpAndSupport}/edit',                        'HelpAndSupportsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'HelpAndSupportsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{helpAndSupport}',                            'HelpAndSupportsController@update')->name('update');
            Route::delete('/{helpAndSupport}',                          'HelpAndSupportsController@destroy')->name('destroy');
            Route::get('/{helpAndSupport}/show',                          'HelpAndSupportsController@show')->name('show');
            Route::get('/change-status',                          'HelpAndSupportsController@change_status')->name('change-status');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('earnings')->name('earnings/')->group(static function() {
            Route::get('/',                                             'EarningsController@index')->name('index');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-settings')->name('admin-settings/')->group(static function() {
            Route::get('/',                                             'AdminSettingsController@index')->name('index');
            Route::get('/create',                                       'AdminSettingsController@create')->name('create');
            Route::post('/',                                            'AdminSettingsController@store')->name('store');
            Route::get('/{adminSetting}/edit',                          'AdminSettingsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AdminSettingsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{adminSetting}',                              'AdminSettingsController@update')->name('update');
            Route::delete('/{adminSetting}',                            'AdminSettingsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('faq-categories')->name('faq-categories/')->group(static function() {
            Route::get('/',                                             'FaqCategoryController@index')->name('index');
            Route::get('/create',                                       'FaqCategoryController@create')->name('create');
            Route::post('/',                                            'FaqCategoryController@store')->name('store');
            Route::get('/{faqCategory}/edit',                           'FaqCategoryController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FaqCategoryController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{faqCategory}',                               'FaqCategoryController@update')->name('update');
            Route::delete('/{faqCategory}',                             'FaqCategoryController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('faqs')->name('faqs/')->group(static function() {
            Route::get('/',                                             'FaqController@index')->name('index');
            Route::get('/create',                                       'FaqController@create')->name('create');
            Route::post('/',                                            'FaqController@store')->name('store');
            Route::get('/{faq}/edit',                                   'FaqController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'FaqController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{faq}',                                       'FaqController@update')->name('update');
            Route::delete('/{faq}',                                     'FaqController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('top-sales')->name('top-sales/')->group(static function() {
            Route::get('/',                 'TopSalesController@index')->name('index');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('app-packages')->name('app-packages/')->group(static function() {
            Route::get('/',                                             'AppPackagesController@index')->name('index');
            Route::get('/create',                                       'AppPackagesController@create')->name('create');
            Route::post('/',                                            'AppPackagesController@store')->name('store');
            Route::get('/{appPackage}/edit',                            'AppPackagesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AppPackagesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{appPackage}',                                'AppPackagesController@update')->name('update');
            Route::delete('/{appPackage}',                              'AppPackagesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('app-versions')->name('app-versions/')->group(static function() {
            Route::get('/',                                             'AppVersionsController@index')->name('index');
            Route::get('/create',                                       'AppVersionsController@create')->name('create');
            Route::post('/',                                            'AppVersionsController@store')->name('store');
            Route::get('/{appVersion}/edit',                            'AppVersionsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'AppVersionsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{appVersion}',                                'AppVersionsController@update')->name('update');
            Route::delete('/{appVersion}',                              'AppVersionsController@destroy')->name('destroy');
        });
    });
});

Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->group(function () {
    Route::post('fcm-token', 'NotificationController@saveFcmToken');
});

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function () {
        Route::prefix('dashboard')->name('dashboard/')->group(static function () {
            Route::get('/','DashboardController@index')->name('index');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin','block'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('slots')->name('slots/')->group(static function() {
            Route::get('/',                                             'SlotsController@index')->name('index');
            Route::get('/create',                                       'SlotsController@create')->name('create');
            Route::post('/store',                                       'SlotsController@store')->name('slot/store');
            Route::post('/checkOverlappingSlots',                       'SlotsController@checkOverlappingSlots');
            Route::post('{id}/delete-slot',                             'SlotsController@destroy');
            Route::get('/{slot}/edit',                                  'SlotsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'SlotsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{slot}',                                      'SlotsController@update')->name('update');
            // Route::delete('/{slot}',                                    'SlotsController@destroy')->name('destroy');
        });
    });
});
