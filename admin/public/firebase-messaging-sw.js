importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'apiKey': "AIzaSyA98bd7GU-oC3F_nFIZroIDRgkHgg2ihXs",
  'authDomain': "webpush-8f0ee.firebaseapp.com",
  'projectId': "webpush-8f0ee",
  'storageBucket': "webpush-8f0ee.appspot.com",
  'messagingSenderId': "758771022388",
  'appId': "1:758771022388:web:e625e52e880f9c8707a861",
  'measurementId': "G-PWEZ5K1W3D"
});
// firebase.initializeApp({
//   'apiKey': env('apiKey') ,
//   'authDomain':  env('authDomain'),
//   'projectId':  env('projectId'),
//   'storageBucket':  env('storageBucket'),
//   'messagingSenderId':  env('messagingSenderId'),
//   'appId':  env('appId'),
//   'measurementId':  env('measurementId')
// });

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://images.theconversation.com/files/93616/original/image-20150902-6700-t2axrz.jpg' //your logo here
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});