<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'last_login_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Category::class, static function (Faker\Generator $faker) {
    return [
        'Category_Name' => $faker->sentence,
        'Category_Description' => $faker->text(),
        'Profile_Picture' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Vendor::class, static function (Faker\Generator $faker) {
    return [
        'Vendor_Name' => $faker->sentence,
        'Email' => $faker->sentence,
        'Profile_Picture' => $faker->sentence,
        'Phone_number_with_country_code' => $faker->sentence,
        'Password' => $faker->sentence,
        'Shop_Name' => $faker->text(),
        'Shop Address' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Company::class, static function (Faker\Generator $faker) {
    return [
        'Company_Name' => $faker->sentence,
        'Company_Description' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Product::class, static function (Faker\Generator $faker) {
    return [
        'Product_Name' => $faker->sentence,
        'Company_Name' => $faker->sentence,
        'Product_Description' => $faker->sentence,
        'Stock_Quantity' => $faker->sentence,
        'Price' => $faker->sentence,
        'Product_Images' => $faker->sentence,
        'Category' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Item::class, static function (Faker\Generator $faker) {
    return [
        'Product_Name' => $faker->sentence,
        'Product_Description' => $faker->sentence,
        'Stock_Quantity' => $faker->sentence,
        'Price' => $faker->sentence,
        'Product_Images' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AdminVendor::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, static function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'name' => $faker->firstName,
        'social_id' => $faker->sentence,
        'social_type' => $faker->sentence,
        'email' => $faker->email,
        'email_verified_at' => $faker->dateTime,
        'profile_picture' => $faker->sentence,
        'password' => bcrypt($faker->password),
        'country_code' => $faker->sentence,
        'phone_number' => $faker->sentence,
        'country_iso_code' => $faker->sentence,
        'verified' => $faker->boolean(),
        'remember_token' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'user_type' => $faker->randomNumber(5),
        'is_block' => $faker->boolean(),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\StripeConnect::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\StripeToken::class, static function (Faker\Generator $faker) {
    return [
        'vendor_id' => $faker->randomNumber(5),
        'stripe_token' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\StripeToken::class, static function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTime,
        'stripe_token' => $faker->text(),
        'updated_at' => $faker->dateTime,
        'vendor_id' => $faker->randomNumber(5),
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Coupon::class, static function (Faker\Generator $faker) {
    return [
        'coupon_code' => $faker->sentence,
        'coupon_description' => $faker->sentence,
        'coupon_discount' => $faker->randomFloat,
        'coupon_max_amount' => $faker->randomFloat,
        'coupon_min_amount' => $faker->randomFloat,
        'coupon_name' => $faker->sentence,
        'coupon_type' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        'end_date' => $faker->dateTime,
        'maximum_per_customer_use' => $faker->randomNumber(5),
        'maximum_total_use' => $faker->randomNumber(5),
        'start_date' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Charge::class, static function (Faker\Generator $faker) {
    return [
        'platform_charges' => $faker->randomFloat,
        'delivery_charges' => $faker->randomFloat,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\BookingDetail::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\VariousCharge::class, static function (Faker\Generator $faker) {
    return [
        'chargeName' => $faker->sentence,
        'type' => $faker->randomNumber(5),
        'value' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TableBannersLocationWise::class, static function (Faker\Generator $faker) {
    return [
        'bannerName' => $faker->sentence,
        'link' => $faker->sentence,
        'address' => $faker->sentence,
        'latitude' => $faker->sentence,
        'longitude' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\HelpAndSupport::class, static function (Faker\Generator $faker) {
    return [
        'booking_id' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'shop_id' => $faker->randomNumber(5),
        'feedback_title' => $faker->text(),
        'feedback_description' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AdminSetting::class, static function (Faker\Generator $faker) {
    return [
        'delivery_boy_searching' => $faker->randomNumber(5),
        'delivery_boy_search_limit' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\FaqCategory::class, static function (Faker\Generator $faker) {
    return [
        'category' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Faq::class, static function (Faker\Generator $faker) {
    return [
        'category_id' => $faker->sentence,
        'question' => $faker->text(),
        'answer' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AppPackage::class, static function (Faker\Generator $faker) {
    return [
        'bundle_id' => $faker->sentence,
        'app_name' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\AppVersion::class, static function (Faker\Generator $faker) {
    return [
        'app_package_id' => $faker->randomNumber(5),
        'force_update' => $faker->boolean(),
        'message' => $faker->sentence,
        'version' => $faker->randomFloat,
        'code' => $faker->sentence,
        'platform' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
