<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColAdminVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            $table->double('gst',12,2)->after('commission_charges')->nullable();
            $table->double('pst',12,2)->after('gst')->nullable();
            $table->double('hst',12,2)->after('pst')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            $table->removeColumn('gst');
            $table->removeColumn('pst');
            $table->removeColumn('hst');
        });
    }
}
