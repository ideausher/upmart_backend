<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryboyMainDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveryboy_main_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('registeration_number',255)->nullable();
            $table->string('vehicle_picture',255)->nullable();
            $table->string('model',255)->nullable();
            $table->string('color',255)->nullable();
            $table->timestamp('valid_upto');
            $table->string('drinvinglicence_images',255)->nullable();
            $table->string('drivinglicence_no',255)->nullable();
            $table->timestamp('drivinglicence_expiry_date');
            $table->string('licence_no_plate_images',255)->nullable();
            $table->string('no_plate',255)->nullable();
            $table->string('vehicle_insurence_image',255)->nullable();
            $table->string('policy_number',255)->nullable();
            $table->string('vehicle_company',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveryboy_main_documents');
    }
}
