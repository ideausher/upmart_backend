<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatLongToAdminVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            $table->decimal('lat', 10, 8)->nullable()->after('country_code');
            $table->decimal('lng', 11, 8)->nullable()->after('country_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_vendors', function (Blueprint $table) {
            
        });
    }
}
