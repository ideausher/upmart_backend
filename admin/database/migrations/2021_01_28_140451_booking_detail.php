<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_details', function (Blueprint $table) {
            $table->id();
            $table->integer('orderId');
            $table->integer('userId');
            $table->integer('shopId');
            $table->integer('addressId');
            $table->integer('cardId');
            $table->decimal('original_amount',12,2); 
            $table->decimal('discounted_amount',12,2);
            $table->decimal('final_amount',12,2);
            $table->tinyInteger('status');
            $table->tinyInteger('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
