<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariousChargeRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('various_charge_relation', function (Blueprint $table) {
            $table->id();
            $table->integer('various_charge_id');
            $table->double('startRange',12,2);
            $table->double('endRange',12,2);
            $table->double('price',12,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('various_charge_relation');
    }
}
