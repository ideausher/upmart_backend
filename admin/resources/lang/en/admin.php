<?php

return [
    'admin-user' => [
        'title' => 'Merchants',

        'actions' => [
            'index' => 'Merchants',
            'create' => 'New Merchant',
            'shopDetails' => 'Shop Details',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            'phonenumber_with_countrycode'=>'Phone number',
            'shop_name'=> 'Shop name',
            'shop_address'=> 'Shop address',
            'additional_address' => 'Additional address',
            'profile_picture' =>'Profile picture',
            'is_blocked'=>'Is blocked',
            'Gallery'=>'Profile picture',
            'country_code'=>'Country code',
            'lat'=>'Latitude',
            'lng'=>'Longitute',
            'shop_type'=>'Shop type',
            'platform_charges'=>'Platform charges',
            'commission_charges'=>'Commission charges',
            'gst' => 'GST',
            'pst' => 'PST',
            'hst' => 'HST',
            //Belongs to many relations
            'rating'=>'Avg. Rating',
            'available' => 'Available',
            'roles' => 'Roles',
            'description' => 'Description',
        ],
    ],

    'category' => [
        'title' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'category_name' => 'Category name',
            'category_description' => 'Category description',
            'category_image'=>'Category image',
            
        ],
    ],

    'company' => [
        'title' => 'Companies',

        'actions' => [
            'index' => 'Companies',
            'create' => 'New Company',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'company_name' => 'Company name',
            'company_description' => 'Company description',
            
        ],
    ],

    'item' => [
        'title' => 'Products',
        'details' => 'Product Details',

        'actions' => [
            'index' => 'Products',
            'create' => 'New Product',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'product_name' => 'Product name',
            'product_description' => 'Product description',
            'stock_quantity' => 'Stock quantity',
            'total_quantity' => 'Total quantity',
            'price' => 'Price',
            'product_images' => 'Product images',
            'category' => 'Category',
            'category_name'=>'Category name',
            'company_name' => 'Company name',
            'new_price' => 'New price',
            'discount' => 'Discount (in percentage)',
            'is_disabled'=>'Disable item',
            'product_by'=>'Product by',
            'gst_enable'=>'GST enable',
            'gst'=>'GST',
            'pst'=>'PST',
            'hst'=>'HST',
            'after_gst_price'=>'Price after GST'
        ],
    ],

    'admin-vendor' => [
        'title' => 'Admin Merchant',

        'actions' => [
            'index' => 'Admin Merchant',
            'create' => 'New Admin Merchant',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'user' => [
        'title' => 'Driver Listing',
        'details' => 'Delivery Boy Details',

        'actions' => [
            'index' => 'Delivery Boy',
            'create' => 'New Delivery Boy',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'name' => 'Name',
            'social_id' => 'Social',
            'social_type' => 'Social type',
            'email' => 'Email',
            'email_verified_at' => 'Email verified at',
            'profile_picture' => 'Profile picture',
            'password' => 'Password',
            'country_code' => 'Country code',
            'phone_number' => 'Phone number',
            'country_iso_code' => 'Country iso code',
            'verified' => 'Verified',
            'user_type' => 'User type',
            'is_block' => 'Is block',
            'status' => 'Status',
            'rating'=>'Avg. Rating',
            'date_of_birth' => 'Date of Birth',
            'referral_code' => 'Referral Code',
            'referred_by_code' => 'Refferer Code'
        ],
    ],

    'customer' => [
        'title' => 'Customers',
        'details' => 'Customer Details',
        'userdetails' => 'User Details',

        'actions' => [
            'index' => 'Customer',
            'create' => 'New Customer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'name' => 'Name',
            'social_id' => 'Social',
            'social_type' => 'Social type',
            'email' => 'Email',
            'email_verified_at' => 'Email verified at',
            'profile_picture' => 'Profile picture',
            'password' => 'Password',
            'country_code' => 'Country code',
            'phone_number' => 'Phone number',
            'country_iso_code' => 'Country iso code',
            'verified' => 'Verified',
            'user_type' => 'User type',
            'is_block' => 'Is block',
            'status' => 'Status',
            'address' => 'Address',
            'date_of_birth' => 'Date of Birth',
            'referral_code' => 'Referral Code',
            'referred_by_code' => 'Refferer Code'
        ],
    ],

    'verification-panel' => [
        'title' => 'Verification Panel',
        'details' => 'Verification Panel Details',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'date_of_birth' => 'Date of Birth'
        ],
    ],

    'stripe-connect' => [
        'title' => 'Stripe Connect',

        'actions' => [
            'index' => 'Stripe Connect',
            'create' => 'New Stripe Connect',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'stripe-token' => [
        'title' => 'Stripe Token',

        'actions' => [
            'index' => 'Stripe Token',
            'create' => 'New Stripe Token',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'vendor_id' => 'Merchant',
            'stripe_token' => 'Stripe token',
            
        ],
    ],

    'stripe-connect' => [
        'title' => 'Stripe Connect',

        'actions' => [
            'index' => 'Stripe Connect',
            'create' => 'New Stripe Connect',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'coupon' => [
        'title' => 'Coupons',
        'details' => 'Coupon Details',

        'actions' => [
            'index' => 'Coupons',
            'create' => 'New Coupon',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'coupon_code' => 'Coupon code',
            'coupon_description' => 'Coupon description',
            'coupon_discount' => 'Coupon discount',
            'coupon_max_amount' => 'Coupon max amount',
            'coupon_min_amount' => 'Coupon min amount',
            'coupon_name' => 'Coupon name',
            'coupon_type' => 'Coupon type',
            'end_date' => 'End date',
            'maximum_per_customer_use' => 'Maximum per customer use',
            'maximum_total_use' => 'Maximum total use',
            'start_date' => 'Start date',
            'shopId' => 'Shop Id',
            'isPrimary' => 'Is Primary',
            'coupon_image' => 'Coupon Image'
        ],
    ],

    'booking-detail' => [
        'title' => 'Bookings',
        'bookingDetails' => 'Booking Details',

        'actions' => [
            'index' => 'Bookings',
            'create' => 'New Booking',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'userId' => 'User name',
            'booking_code' => 'Booking code',
            'shopId' => 'Shop name',
            'vendor' => 'Merchant name',
            'addressId' => 'Address',
            'cardId' => 'Card id',
            'couponId' => 'Coupon code',
            'deliveryBoyId' => 'Delivery boy',
            'bookingType' => 'Booking type',
            'bookingDateTime' => 'Scheduled date time',
            'after_charge_amount' => 'After charge amount',
            'couponType' => 'Coupon type',
            'tip_to_delivery_boy' => 'Tip to delivery boy',
            'couponBy' => 'Coupon by',
            'commission_charge' => 'Commission charge',
            'platform_charge' => 'Platform charge',
            'delivery_charge_to_delivery_boy'=>'Delivery charge to delivery boy',
            'delivery_charge_for_customer'=>'Delivery charge for customer',
            'amount_after_discount'=>'Amount after discount',
            'discount_amount'=>'Discount amount',
            'amount_before_discount'=>'Amount before discount',
            'stripe_charges' => 'Stripe charges',
            'amount_after_stripe_charges' => 'Amount after stripe charges',
            'instruction' => 'Instruction',
            'status' => 'Order status',
            'orderProducts' => 'Ordered Products',
            'active' => 'Active',
            'created_at' => 'Boooking created at',
            'cart_value'=>'Cart Value',
            'total'=>'Total',
            'as_total_amount_to_admin'=>'Admin received',
            'received_by_vendor'=>'Received by Merchant',
            'received_by_delivery_boy'=>'Received by delivery boy charge',
            'admin_balance'=>'Admin balance',
            'earned_by_me'=>'Earned by me',
            'review_for_shop'=>'Review for shop',
            'rating_for_shop'=>'Rating for shop',
            'review_for_delivery_boy'=>'Review for delivery boy',
            'rating_for_delivery_boy'=>'Rating for delivery boy',
            'otp'=>'OTP',
            'gst'=>'GST',
            'pst'=>'PST',
            'hst'=>'HST',
            'after_gst_price'=>'Price after GST',
            'booking_reject_reason'=>'Reject Reason'
        ],
    ],

    'various-charge' => [
        'title' => 'Various Charges',

        'actions' => [
            'index' => 'Various Charges',
            'create' => 'New Various Charge',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'chargeName' => 'Charge name',
            'type' => 'Type',
            'value' => 'Value',
            'startRange' => 'Start Range',
            'endRange' => 'End Range',
            'price' => 'Price'
        ],
    ],

    'table-banners-location-wise' => [
        'title' => 'Banners',

        'actions' => [
            'index' => 'Banners',
            'create' => 'New Banners',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'bannerName' => 'Banner name',
            'link' => 'Link',
            'address' => 'Address',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            
        ],
    ],

    'help-and-support' => [
        'title' => 'Help And Supports',

        'actions' => [
            'index' => 'Help And Supports',
            'show' => 'Help And Supports Details',
            'create' => 'New Help And Support',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'booking_id' => 'Booking',
            'user_id' => 'Username',
            'shop_id' => 'Shop name',
            'feedback_title' => 'Feedback title',
            'feedback_description' => 'Feedback description',
            'query_status' => 'Is query resolved?',
            'image' => 'Images',
            'ticket_id' => 'Ticket Id',
            'created_at' => 'Created at',
            'admin_reply' => 'Admin Message'
        ],
    ],

    'earnings'=>[
        'title' => 'Earnings'
    ],

    'top-sales'=>[
        'title' => 'Top Sales'
    ],

    'admin-setting' => [
        'title' => 'Admin Settings',

        'actions' => [
            'index' => 'Admin Settings',
            'create' => 'New Admin Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'delivery_boy_searching' => 'Delivery boy searching',
            'delivery_boy_search_limit' => 'Delivery boy search limit',
            
        ],
    ],

    'faq-category' => [
        'title' => 'Faq Category',

        'actions' => [
            'index' => 'Faq Category',
            'create' => 'New Faq Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'category' => 'Category',
            
        ],
    ],

    'faq' => [
        'title' => 'Faq',

        'actions' => [
            'index' => 'Faq',
            'create' => 'New Faq',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'category_id' => 'Category',
            'question' => 'Question',
            'answer' => 'Answer',
            
        ],
    ],
    'otp' => [
        'succeeded' => 'OTP Verified Successfully!',
    ],

    'app-package' => [
        'title' => 'App Packages',

        'actions' => [
            'index' => 'App Packages',
            'create' => 'New App Package',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'bundle_id' => 'Bundle',
            'app_name' => 'App name',
            
        ],
    ],

    'app-version' => [
        'title' => 'App Versions',

        'actions' => [
            'index' => 'App Versions',
            'create' => 'New App Version',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'app_package_id' => 'App package',
            'app_bundle_id' => 'App Bundle',
            'force_update' => 'Force update',
            'message' => 'Message',
            'version' => 'Version',
            'code' => 'Code',
            'platform' => 'Platform',
            'category' => 'Service category'

        ],
    ],

    'slots' => [
        'title' => 'Slots',

        'actions' => [
            'add' => 'Add Slot',
            'delete' => 'Delete Slot'
        ]
    ]

    // Do not delete me :) I'm used for auto-generation
];