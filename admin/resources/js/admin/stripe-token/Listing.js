import AppListing from '../app-components/Listing/AppListing';

Vue.component('stripe-token-listing', {
    mixins: [AppListing]
});