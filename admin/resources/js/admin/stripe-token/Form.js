import AppForm from '../app-components/Form/AppForm';

Vue.component('stripe-token-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                stripe_token:  '' ,
                vendor_id:  '' ,
                
            }
        }
    }

});