import AppForm from '../app-components/Form/AppForm';

Vue.filter('toCurrency', function (value) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'CAD',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.component('item-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                product_name:  '' ,
                product_description:  '' ,
                stock_quantity:  '' ,
                price:  '' ,
                product_image:  '' ,
                company_name:'',
                shop_id:'',
                shop_name:'',
                category:'',
                discount:'',
                new_price:'',
                gst_enable:''
            },
            mediaCollections: ['product_images']
        }
    },
    beforeUpdate: function(){
        this.form.new_price = $("#new_price").val();
    },
    updated: function () {
        this.form.new_price = $("#new_price").val();
        console.log("sdf");
    },
    methods: {
        submitForm(id) {
            var dis = $("#discount").val();
            var price = $("#price").val();

            var new_price = $("#new_price").val();
            
            if(dis>100)
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Discount Cannot be greater than 100!",
                    duration: 5000,
                });
            }
            else if(!/^\d{0,6}(?:\.\d{0,2})?$/.test(dis))
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Discount Should be in number or decimal format!",
                    duration: 5000,
                });
            }
            else {
                var disprice = price * dis / 100;
                disprice = disprice.toFixed(2);
                var newprice = price-disprice;
                newprice = newprice.toFixed(2);
                $("#new_price").val(newprice);
                
                this.onSubmit();
            }

        }
    }

});