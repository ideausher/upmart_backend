import AppForm from '../app-components/Form/AppForm';

Vue.component('help-and-support-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                booking_id:  '' ,
                user_id:  '' ,
                shop_id:  '' ,
                feedback_title:  '' ,
                feedback_description:  '' ,
                
            }
        }
    }

});