import AppListing from '../app-components/Listing/AppListing';

Vue.component('help-and-support-listing', {
    mixins: [AppListing]
});