import AppForm from '../app-components/Form/AppForm';

Vue.filter('toCurrency', function (value) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'CAD',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.component('various-charge-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                chargeName:  '' ,
                type:  '' ,
                value: '',
                startRange:[""],
                endRange:[""],
                price:  [""],
                
            }
        }
    },
    methods: {
        add(index) {
            this.form.startRange.push('');
            this.form.endRange.push('');
            this.form.price.push('');
        },
        remove(index) {
            if (this.form.startRange.length > 1) {
                this.form.startRange.splice(index, 1);
            }
            if (this.form.endRange.length > 1) {
                this.form.endRange.splice(index, 1);
            }
            if (this.form.price.length > 1) {
                this.form.price.splice(index, 1);
            }

        },
        updated : function () {
            var price = $("#price").val();
            var startRange = $("#startRange").val();
            var endRange = $("#endRange").val();

            if(price == '' || (startRange=='' && endRange==''))
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Value or Start and End Range cannot be empty",
                    duration: 5000,
                });
            }
            else
            {
                this.onSubmit();
            }
        },
        submitForm(id)
        {
            var startRange = $("#startRange").val();
            var endRange = $("#endRange").val();
            var price = $("#price").val();

            if(price=='' || (startRange=='' && endRange==''))
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Value or Start and End Range cannot be empty!",
                    duration: 5000,
                });
            }
            else
            {
                this.onSubmit();
            }
        }
    },

});