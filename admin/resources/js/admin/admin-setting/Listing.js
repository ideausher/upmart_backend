import AppListing from '../app-components/Listing/AppListing';

Vue.component('admin-setting-listing', {
    mixins: [AppListing]
});