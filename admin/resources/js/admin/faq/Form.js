import AppForm from '../app-components/Form/AppForm';

Vue.component('faq-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                category_id:  '' ,
                questions:  [""] ,
                answers:  [""] 
            }
        }
    },
    methods: {
        add(index) {
            this.form.questions.push('');
            this.form.answers.push('');
        },
        remove(index) {
            if (this.form.questions.length > 1) {
                this.form.questions.splice(index, 1);
            }
            if (this.form.answers.length > 1) {
                this.form.answers.splice(index, 1);
            }

        },
        submitForm(id)
        {
            var questions = $("#questions").val();
            var answers = $("#answers").val();

            if(questions=='' || answers=='')
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Question and answer cant be empty!",
                    duration: 5000,
                });
            }
            else
            {
                this.onSubmit();
            }
        }
    },

});