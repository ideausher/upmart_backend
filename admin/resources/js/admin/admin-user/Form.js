import AppForm from '../app-components/Form/AppForm';

Vue.component('admin-user-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                first_name:  '' ,
                last_name:  '' ,
                email:  '' ,
                password:  '' ,
                activated:  false ,
                forbidden:  false ,
                language:  '' ,
                // roles:'',
                shop_name:'',
                shop_address:'',
                additional_address:  '' ,
                phonenumber_with_countrycode:  '' ,
                description:  '' ,
                lat: '',
                lng: '',
                shop_type: '',
                platform_charges: '',
                commission_charges: '',
                gst: '',
                pst: '',
                hst: '',
            },
            mediaCollections: ['shop_image','Gallery']
        }
    },
    beforeUpdate: function(){
        this.form.shop_address = $("#hiidenaddress").val();
        this.form.lat = $("#hiidenlat").val();
        this.form.lng = $("#hiidenlng").val();
        this.form.platform_charges = $("#platform_charges").val();
        this.form.commission_charges = $("#commission_charges").val();
        this.form.gst = $("#gst").val();
        this.form.pst = $("#pst").val();
        this.form.hst = $("#hst").val();
    },
    updated: function () {
       
        this.form.shop_address = $("#hiidenaddress").val();
        this.form.lat = $("#hiidenlat").val();
        this.form.lng = $("#hiidenlng").val();
        this.form.platform_charges = $("#platform_charges").val();
        this.form.commission_charges = $("#commission_charges").val();
        this.form.gst = $("#gst").val();
        this.form.pst = $("#pst").val();
        this.form.hst = $("#hst").val();
        var country_iso_code = $("#country_iso_code").val();
        this.form.country_code = $("#"+country_iso_code).val();
        console.log("sdf");
        // }
    },
    methods: {
        modifyData: function () {
            console.log($("#hiidenaddress").val()); 
            console.log(this.form.shop_address);
            this.form.shop_address = $("#hiidenaddress").val();
            this.form.lat = $("#hiidenlat").val();
            this.form.lng = $("#hiidenlng").val();
        }
      }
});