import AppForm from '../app-components/Form/AppForm';

Vue.component('table-banners-location-wise-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                bannerName:  '' ,
                link:  '' ,
                address:  '' ,
                latitude:  '' ,
                longitude:  '' ,
                
            },
            mediaCollections: ['banner_image']
        }
    },
    beforeUpdate: function(){
        this.form.address = $("#hiidenaddress").val();
        this.form.latitude = $("#hiidenlat").val();
        this.form.longitude = $("#hiidenlng").val();
    },
    updated: function () {
       
            this.form.address = $("#hiidenaddress").val();
            this.form.latitude = $("#hiidenlat").val();
            this.form.longitude = $("#hiidenlng").val();
            console.log("sdf");
        // }
    },
    methods: {
        modifyData: function () {
            console.log($("#hiidenaddress").val()); 
            console.log(this.form.address);
            this.form.address = $("#hiidenaddress").val();
            this.form.latitude = $("#hiidenlat").val();
            this.form.longitude = $("#hiidenlng").val();
        }
      }

});