import AppForm from '../app-components/Form/AppForm';

Vue.component('verification-panel-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                vehicle_status:  '' ,
                drivinglicence_status: '',
                licence_no_plate_status: '',
                vehicle_insurence_status: '',
                vehicle_insurence_reason: '',
                licence_no_plate_reason: '',
                drivinglicence_reason: '',
                vehicle_reason: ''
            }
        }
    },
    methods: {
        submitForm(id) {
            var vehicle_status = $('input[name="vehicle_status"]:checked').val();
            var drivinglicence_status = $('input[name="drivinglicence_status"]:checked').val();
            var licence_no_plate_status = $('input[name="licence_no_plate_status"]:checked').val();
            var vehicle_insurence_status = $('input[name="vehicle_insurence_status"]:checked').val();

            var vehicle_insurence_reason = $("#vehicle_insurence_reason").val();
            var licence_no_plate_reason = $("#licence_no_plate_reason").val();
            var drivinglicence_reason = $("#drivinglicence_reason").val();
            var vehicle_reason = $("#vehicle_reason").val();
            
            if (vehicle_status==3&&vehicle_reason=='') {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Vehicle Reject Reason can't be empty!",
                    duration: 5000,
                });
            }
            else if (drivinglicence_status==3&&drivinglicence_reason=='') {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Driving Licence Reject Reason can't be empty!",
                    duration: 5000,
                });
            }
            else if (licence_no_plate_status==3&&licence_no_plate_reason=='') {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Licence No Reject Reason can't be empty!",
                    duration: 5000,
                });
            }
            else if (vehicle_insurence_status==3&&vehicle_insurence_reason=='') {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Vehicle Insurance Reject Reason can't be empty!",
                    duration: 5000,
                });
            }
            else {
                this.onSubmit();
            }

        }
    }
});