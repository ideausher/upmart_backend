import AppForm from '../app-components/Form/AppForm';

Vue.component('shop-detail-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                shop_name:  '' ,
                shop_address: '',
                description: '',
                lat: '',
                lng: ''
            }
        }
    },
    beforeUpdate: function(){
        this.form.shop_address = $("#hiidenaddress").val();
        this.form.lat = $("#hiidenlat").val();
        this.form.lng = $("#hiidenlng").val();
    },
    updated: function () {
       
            this.form.shop_address = $("#hiidenaddress").val();
            this.form.lat = $("#hiidenlat").val();
            this.form.lng = $("#hiidenlng").val();
            console.log("sdf");
        // }
    },
    methods: {
        modifyData: function () {
            console.log($("#hiidenaddress").val()); 
            console.log(this.form.shop_address);
            this.form.shop_address = $("#hiidenaddress").val();
            this.form.lat = $("#hiidenlat").val();
            this.form.lng = $("#hiidenlng").val();
        }
      }
});