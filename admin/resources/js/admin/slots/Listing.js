import AppListing from '../app-components/Listing/AppListing';

Vue.component('slot-listing', {
    mixins: [AppListing]
});