import AppForm from '../app-components/Form/AppForm';

Vue.component('slot-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                shop_id:  '' ,
                day:  '' ,
                slot_from:  '' ,
                slot_to:  '' ,
                
            }
        }
    }

});