import { sum } from 'lodash';
import AppForm from '../app-components/Form/AppForm';


Vue.filter('toCurrency', function (value) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'CAD',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.component('booking-detail-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                id: '',
                userId: '',
                shopId: '',
                addressId: '',
                booking_code: '',
                cardId: '',
                couponId: '',
                deliveryBoyId: '',
                bookingType: '',
                bookingDateTime: '',
                after_charge_amount: '',
                tip_to_delivery_boy: '',
                commission_charge: '',
                platform_charge: '',
                delivery_charge_to_delivery_boy: '',
                delivery_charge_for_customer: '',
                amount_after_discount: '',
                discount_amount: '',
                amount_before_discount: '',
                stripe_charges: '',
                amount_after_stripe_charges: '',
                instruction: '',
                status: '',
                active: ''
            }
        }
    },
    methods:{
        total(val){
            var sum = 0;
            // return Vue.filter('toCurrency')(11);
            val.map(function(value, key) {
                if(key!=4)
                {
                    sum += parseFloat(value);
                }
                else
                {
                    sum -= parseFloat(value);
                }
            });
            if(sum==0 || sum==NaN)
                sum = 0.00;

            return Vue.filter('toCurrency')(sum);
        }
    }

});