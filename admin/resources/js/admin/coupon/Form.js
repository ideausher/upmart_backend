import AppForm from '../app-components/Form/AppForm';
import moment from 'moment';

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment.utc(String(value)).format('YYYY-MM-DD H:mm:ss')
  }
});

Vue.filter('toCurrency', function (value) {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'CAD',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.component('coupon-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                coupon_code:  '' ,
                coupon_description:  '' ,
                coupon_discount:  '' ,
                coupon_max_amount:  '' ,
                coupon_min_amount:  '' ,
                coupon_name:  '' ,
                coupon_type:  false ,
                end_date:  '' ,
                maximum_per_customer_use:  '' ,
                maximum_total_use:  '' ,
                start_date:  '' ,
                shopId: '',
                isPrimary: '',
                is_primary: ''
                
            },
            mediaCollections: ['coupon_image']
        }
    },
    mounted: function(){
        this.form.start_date = moment.utc(String(this.form.start_date)).format('YYYY-MM-DD H:mm:ss');
        this.form.end_date = moment.utc(String(this.form.end_date)).format('YYYY-MM-DD H:mm:ss');
    },
    methods: {
        submitForm(id,typeForm) {
            var coupon_max_amount = $("#coupon_max_amount").val();
            var coupon_min_amount = $("#coupon_min_amount").val();
            
            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();

            var currentDate = moment(new Date()).format('YYYY-MM-DD H:mm:ss');
  
            // alert(currentDate);
            // alert(start_date);
            // alert(end_date);
            
            if (parseFloat(coupon_max_amount) < parseFloat(coupon_min_amount) ) {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Coupon minimum amount should't be greater than or equal to max amount!",
                    duration: 5000,
                });
            }
            else if(start_date!=''&&end_date!=''&&start_date>end_date)
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Start date can't be greater than End date!",
                    duration: 5000,
                });
            }
            else if(start_date!=''&&typeForm=='add'&&start_date<currentDate)
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Start date can't be less than Current date!",
                    duration: 5000,
                });
            }
            else if(end_date!=''&&end_date<currentDate)
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "End date can't be less than Current date!",
                    duration: 5000,
                });
            }
            else if(start_date!=''&&end_date!=''&&start_date==end_date)
            {
                Vue.notify({
                    type: 'error',
                    title: 'Invalid Data!!',
                    text: "Start date time and End date time can't be same!",
                    duration: 5000,
                });
            }
            else {
                
                this.onSubmit();
            }

        }
    }

});