import AppListing from '../app-components/Listing/AppListing';

Vue.component('admin-vendor-listing', {
    mixins: [AppListing]
});