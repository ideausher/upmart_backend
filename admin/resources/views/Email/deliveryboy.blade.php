<b>Hi, {{$data['name']}}</b>

@if($data['approve']==1)

<p>Your @if($data['vehicle_status']==2) {{'Vehicle Document, '}} @endif @if($data['drivinglicence_status']==2) {{'Driving Licence Document, '}} @endif @if($data['licence_no_plate_status']==2) {{'Licence No Document, '}} @endif @if($data['vehicle_insurence_status']==2) {{'Vehicle Insurance Document'}} @endif has been approved by admin.</p>

@endif

@if($data['reject']==1)
<p>Your @if($data['vehicle_status']==3) {{'Vehicle Document has been rejected by admin Due to this reason: '.$data['vehicle_reason'].', '}} @endif @if($data['drivinglicence_status']==3) {{'Driving Licence Document has been rejected by admin Due to this reason: '.$data['drivinglicence_reason'].', '}} @endif @if($data['licence_no_plate_status']==3) {{'Licence No Document has been rejected by admin Due to this reason: '.$data['licence_no_plate_reason'].', '}} @endif @if($data['vehicle_insurence_status']==3) {{'Vehicle Insurance Document has been rejected by admin Due to this reason: '.$data['vehicle_insurence_reason']}} @endif.</p>

@endif

<b>Thank you</b>