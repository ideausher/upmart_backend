<div>
    <p>Hi, {{$data['name']}}</p>

    <p>Please find your account @if($data['vendor']==0){{'updated'}}@endif credentials below:</p>
    <p>Login link: http://iphoneapps.co.in:9074/upmart/upmart/admin/public/admin/</p>
    <p>Email: {{$data['email']}}</p>
    <p>Password: {{$data['password']}}</p>

    <p>Please dont share your credentials with anyone</p>

    <b>Thank you</b>
</div>