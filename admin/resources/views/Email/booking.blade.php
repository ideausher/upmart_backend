<b>Hi, {{$data['name']}}</b>

@if($data['status']=='accept')

<p>Your Order id: {{$data['orderId']}} has been accepted by the vendor.</p>

@endif

@if($data['status']=='reject')
<p>Your Order id: {{$data['orderId']}} has been rejected due to {{$data['reason']}} by the vendor.</p>

@endif

<b>Thank you</b>