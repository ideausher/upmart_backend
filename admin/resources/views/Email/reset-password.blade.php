<div>
    <p>Hi, {{$data['name']}}</p>

    <p>Please use this link to reset your password: <a href="{{$data['link']}}">{{$data['link']}}</a></p>

    <b>Thank you</b>
</div>