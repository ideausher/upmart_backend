<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>


    <!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-functions.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-messaging.js"></script>
<!-- firebase integration started -->

<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase.js"></script>


<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyA98bd7GU-oC3F_nFIZroIDRgkHgg2ihXs",
        authDomain: "webpush-8f0ee.firebaseapp.com",
        projectId: "webpush-8f0ee",
        storageBucket: "webpush-8f0ee.appspot.com",
        messagingSenderId: "758771022388",
        appId: "1:758771022388:web:e625e52e880f9c8707a861",
        measurementId: "G-PWEZ5K1W3D"
    };
    
    // var firebaseConfig = {
    //     apiKey: {{ env('apiKey') }},
    //     authDomain: {{ env('authDomain') }},
    //     projectId: {{ env('projectId') }},
    //     storageBucket: {{ env('storageBucket') }},
    //     messagingSenderId: {{ env('messagingSenderId') }},
    //     appId: {{ env('appId') }},
    //     measurementId: {{ env('measurementId') }}
    // };
    
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
    messaging
    .requestPermission()
    .then(function () {
    //MsgElem.innerHTML = "Notification permission granted." 
        console.log("Notification permission granted.");
        // get the token in the form of promise
        return messaging.getToken()
    })
    .then(function(token) {
        // print the token on the HTML page     
        console.log(token);
    })
    .catch(function (err) {
        console.log("Unable to get permission to notify.", err);
    });
    
    messaging.onMessage(function(payload) {
        console.log(payload);
        var notify;
        notify = new Notification(payload.notification.title,{
            body: payload.notification.body,
            icon: payload.notification.icon,
            tag: "Dummy"
        });
        console.log(payload.notification);
    });

    //firebase.initializeApp(config);
    var database = firebase.database().ref().child("/users/");
    database.on('value', function(snapshot) {
        renderUI(snapshot.val());
    });

    // On child added to db
    database.on('child_added', function(data) {
        console.log("Comming");
        if(Notification.permission!=='default'){
            var notify;
            notify= new Notification('CodeWife - '+data.val().username,{
                'body': data.val().message,
                'icon': 'bell.png',
                'tag': data.getKey()
            });
            notify.onclick = function(){
                alert(this.tag);
            }
        }else{
            alert('Please allow the notification first');
        }
    });
    self.addEventListener('notificationclick', function(event) {       
        event.notification.close();
    });
    
</script>