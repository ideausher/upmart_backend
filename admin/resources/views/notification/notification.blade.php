@section('bottom-scripts')
<script>
var audio_path = "{{url('alarm.mp3')}}";
var loggedInUser = "{{ Auth::user()->id }}";
var merchant_type = "{{ Auth::user()->merchant_type }}";
var token_store_api = "{{url('/admin/fcm-token')}}";
console.log(token_store_api);
var fcm_api_key = "{{env('FCM_API_KEY','')}}"
var fcm_auth_domain = "{{env('FCM_AUTH_DOMAIN','')}}"
var fcm_project_id = "{{env('FCM_PROJECT_ID','')}}"
var fcm_storage_bucket = "{{env('FCM_STORAGE_BUCKET','')}}"
var fcm_messaging_sender = "{{env('FCM_MESSAGING_SENDER_ID','')}}"
var fcm_app_id = "{{env('FCM_APP_ID','')}}"
var fcm_measurement_id = "{{env('FCM_MEASUREMENT_ID','')}}"
var fcm_vapid_key = "{{env('FCM_VAPID_KEY','')}}"
var booking_package_screen = "{{ (Request::is('admin/booking*') or Request::is('admin/package/*') or Request::is('admin/package'))? true:false}}";
</script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-firestore.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
<script src="{{url('js/notification.js')}}"></script>
@endsection