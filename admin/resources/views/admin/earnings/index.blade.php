@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.earnings.title'))

@section('body')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="row">
    <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ 'Earned Amount: '.$total }}
                </div>
            </div>
            <div class="choose-date-time" style="float: left;
width: 30%;background-color: white;
padding: 10px;
border-bottom: 1px solid rgba(207, 216, 220, 0.35);
border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;">
            
                <form method="get" action="{{ url('admin/earnings') }}" style="display: grid;">
                <div class="from-date" style="display: inline-grid;">
                <label for="from_date">From Date</label>
                <input type="text" name="from_date" id="from_date" value="{{$request->from_date}}" readonly>
                </div>
                <div class="to-date" style="display: inline-grid;margin-bottom: 10px;">
                <label for="to_date">To Date</label>
                <input type="text" name="to_date" id="to_date" value="{{$request->to_date}}" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
            <div class="earnings-graph" style="float: right;
width: 60%;">
                <canvas id="myChart"></canvas>
            </div>
    </div>        
</div>

@endsection

<?php

$x_row1 = [];
foreach($x_row as $x)
{
    $x_row1[] = date("d M Y",strtotime($x));
}

?>

@section('bottom-scripts')

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: <?php echo json_encode($x_row1); ?>,
        datasets: [{
            label: 'Earnings',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: <?php echo json_encode($amount); ?>
        }]
    },

    // Configuration options go here
    options: {}
});

</script>

<script>
  $( function() {
    $( "#from_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    $( "#to_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  </script>


@endsection