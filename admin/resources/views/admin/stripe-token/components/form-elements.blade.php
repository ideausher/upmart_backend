<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe_token'), 'has-success': fields.stripe_token && fields.stripe_token.valid }">
    <label for="stripe_token" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.stripe-token.columns.stripe_token') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.stripe_token" v-validate="'required'" id="stripe_token" name="stripe_token"></textarea>
        </div>
        <div v-if="errors.has('stripe_token')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('stripe_token') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('vendor_id'), 'has-success': fields.vendor_id && fields.vendor_id.valid }">
    <label for="vendor_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.stripe-token.columns.vendor_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.vendor_id" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vendor_id'), 'form-control-success': fields.vendor_id && fields.vendor_id.valid}" id="vendor_id" name="vendor_id" placeholder="{{ trans('admin.stripe-token.columns.vendor_id') }}">
        <div v-if="errors.has('vendor_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vendor_id') }}</div>
    </div>
</div>


