@extends('brackets/admin-ui::admin.layout.default')

@section('title', 'Customers')

@section('body')

    <div class="container-xl">

            <div class="card">
        
        <user-form
            :action="'{{ url('admin/customers') }}'"
            :data = "{{$details->toJson()}}"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-eye"></i> Detail
                </div>

                <div class="card-body">
                    @if(!empty($details[0]->name))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
                        <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.name') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                                {{$details[0]->name}}
                            
                            <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($details[0]->date_of_birth))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
                        <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.date_of_birth') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                                {{$details[0]->date_of_birth }}
                            
                            <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($details[0]->email))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('email'), 'has-success': fields.email && fields.email.valid }">
                        <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.email') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                            {{$details[0]->email}}
                            
                            <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}</div>
                        </div>
                    </div>
                    @endif


                    @if(!empty($details[0]->phone_number) || !empty($details[0]->country_code))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('phone_number'), 'has-success': fields.phone_number && fields.phone_number.valid }">
                        <label for="phone_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.phone_number') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                            {{$details[0]->country_code}}{{$details[0]->phone_number}}
                            
                            <div v-if="errors.has('phone_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('phone_number') }}</div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($details[0]->profile_picture))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('profile_picture'), 'has-success': fields.profile_picture && fields.profile_picture.valid }">
                        <label for="profile_picture" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.profile_picture') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                            <img src="{{$details[0]->profile_picture}}" width="150" height="150">
                            
                            <div v-if="errors.has('profile_picture')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('profile_picture') }}</div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($details[0]->customerrelation))
                    @foreach($details[0]->customerrelation as $address)


                    @if(!empty($address->formatted_address))
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('address'), 'has-success': fields.address && fields.address.valid }">
                        <label for="address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.address') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                            {{$address->formatted_address}}
                            
                            <div v-if="errors.has('address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('address') }}</div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
                                
                <div class="card-footer">
                <a class="btn btn-danger" href="{{ url('admin/customers') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Back</a>
                </div>
                
            
            </form>
        </user-form>

        </div>

        </div>

    
@endsection