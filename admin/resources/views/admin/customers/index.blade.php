@extends('brackets/admin-ui::admin.layout.default')

@section('title', 'Customers')

@section('body')

    <user-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/customers') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ 'Customers' }}   
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        

                                        <th is='sortable' :column="'id'">{{ trans('admin.customer.columns.id') }}</th>
                                        
                                        <th is='sortable' :column="'name'">{{ trans('admin.customer.columns.name') }}</th>
                                        
                                        <th :column="'email'">{{ trans('admin.customer.columns.email') }}</th>

                                        <th :column="'address'">{{ trans('admin.customer.columns.address') }}</th>
                                        
                                        <th :column="'profile_picture'">{{ trans('admin.customer.columns.profile_picture') }}</th>
                                        <th :column="'phone_number'">{{ trans('admin.customer.columns.phone_number') }}</th>
                                        <th :column="'referral_code'">{{ trans('admin.customer.columns.referral_code') }}</th>
                                        <th :column="'referred_by_code'">{{ trans('admin.customer.columns.referred_by_code') }}</th>
                                        

                                        <th></th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                       

                                    <td>@{{ item.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        
                                        <td>@{{ item.email }}</td>
                                      
                                        <td v-if="item.customerrelation[0]">
                                         @{{item.customerrelation[0].formatted_address}}
                                        </td>
                                        <td v-else>{{''}}</td>
                                        
                                        <td><img :src="item.profile_picture" width="60" height="60"></td>
                                        <td>@{{ item.country_code }}@{{ item.phone_number }}</td>
                                        <td>@{{ item.referral_code }}</td>
                                        <td>@{{ item.referred_by_code }}</td>

                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/detail'" title="detail" role="button"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/bookings'" title="Booking" role="button"><i class="fa fa-ticket"></i></a>
                                                </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </user-listing>

@endsection