

<?php

use Akaunting\Money\Money;

?>

@extends('brackets/admin-ui::admin.layout.default')

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com//ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('title', trans('admin.top-sales.title'))

@section('body')
 

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ trans('admin.top-sales.title') }}</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <table id="tableId" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Product Id</th>
                      <th>Product Name</th>
                      <th>Product Price</th>
                    </tr>
                  </thead>
                  <tbody>
          
                    @if($role==0)
                        @foreach($sales as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->productId}}</td>
                                <td>{{$item->productName}}</td>
                                <td>{{Money::CAD($item->new_price, true)}}</td>
                            </tr>
                        @endforeach
                    @else
                        @foreach($sales as $item)
                            @if(!empty($item))
                            <tr>
                                <td>{{$item[0]['id']}}</td>
                                <td>{{$item[0]['productId']}}</td>
                                <td>{{$item[0]['productName']}}</td>
                                <td>{{Money::CAD($item[0]['new_price'], true)}}</td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(function () {
      $.noConflict();
        $('#tableId').DataTable({
          "ordering": true,
          columnDefs: [{
            orderable: false,
            targets: "no-sort"
          }],
          "order": [[ 0, "asc" ]]
        });
    });
</script>
