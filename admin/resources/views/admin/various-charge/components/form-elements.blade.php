<div class="form-group row align-items-center" :class="{'has-danger': errors.has('chargeName'), 'has-success': fields.chargeName && fields.chargeName.valid }">
    <label for="chargeName" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.chargeName') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.chargeName" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('chargeName'), 'form-control-success': fields.chargeName && fields.chargeName.valid}" id="chargeName" name="chargeName" placeholder="{{ trans('admin.various-charge.columns.chargeName') }}" disabled>
        <div v-if="errors.has('chargeName')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('chargeName') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('type'), 'has-success': fields.type && fields.type.valid }">
  <label for="type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.type') }}</label>
  <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
    <select v-model="form.type" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('type'), 'form-control-success': fields.type && fields.type.valid}" id="type" name="type" disabled>
      <option value="0">Percentage</option>
      <option value="1">Fixed</option>
    </select>
    <div v-if="errors.has('type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type') }}</div>
  </div>
</div>


<div class="" v-for="(input,k) in form.startRange" :key="k" v-if="form.id==2">

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('startRange'), 'has-success': fields.startRange && fields.startRange.valid }">
    <label for="startRange" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.startRange') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.startRange[k]" id="startRange" name="startRange"></textarea>
        </div>
        

        <div v-if="errors.has('startRange')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('startRange') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('endRange'), 'has-success': fields.endRange && fields.endRange.valid }">
    <label for="endRange" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.endRange') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.endRange[k]" id="endRange" name="endRange"></textarea>
        </div>

        <div v-if="errors.has('endRange')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('endRange') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('price'), 'has-success': fields.price && fields.price.valid }">
    <label for="price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.price') }}</label>
      <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
          <input type="text" v-model="form.price[k]" class="form-control" id="price" name="price" placeholder="{{ trans('admin.various-charge.columns.price') }}">
        </div>
        <div v-if="errors.has('price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('price') }}</div>
    </div>
</div>


<div class="form-group row align-items-right">
<span>
            <i class="fa fa-minus-circle" @click="remove(k)" v-show="form.startRange.length > 1"></i>
            <i class="fa fa-plus-circle" @click="add(k)" v-show="k+1 == form.startRange.length"></i>
        </span>
</div>
       
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('value'), 'has-success': fields.value && fields.value.valid }" v-if="form.id!=2">
    <label for="value" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.various-charge.columns.value') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.value" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('value'), 'form-control-success': fields.value && fields.value.valid}" id="value" name="value" placeholder="{{ trans('admin.various-charge.columns.value') }}">
        <div v-if="errors.has('value')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('value') }}</div>
    </div>
</div>

