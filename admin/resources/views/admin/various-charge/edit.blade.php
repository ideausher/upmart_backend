@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.various-charge.actions.edit', ['name' => $variousCharge->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <various-charge-form
                :action="'{{ $variousCharge->resource_url }}'"
                :data="{{ $dataPre }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="submitForm('variouscharge')" :action="action" id="variouscharge" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.various-charge.actions.edit', ['name' => $variousCharge->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.various-charge.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </various-charge-form>

        </div>
    
</div>

@endsection