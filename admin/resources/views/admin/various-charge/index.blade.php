@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.various-charge.actions.index'))

@section('body')

    <various-charge-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/various-charges') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.various-charge.actions.index') }}
                        
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>

                                        <th is='sortable' :column="'chargeName'">{{ trans('admin.various-charge.columns.chargeName') }}</th>
                                        <th is='sortable' :column="'type'">{{ trans('admin.various-charge.columns.type') }}</th>
                                        <th is='sortable' :column="'value'">{{ trans('admin.various-charge.columns.value') }}</th>

                                        <th></th>
                                    </tr>
                                  
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                      
                                        <td>@{{ item.chargeName }}</td>
                                        <template v-if="item.type==1">
                                            <td>{{ 'Fixed' }}</td>
                                        </template>
                                        <template v-else>
                                         <td v-if="item.type==0">{{'Percentage'}}</td>
                                        </template>
                                        <template v-if="item.id!=2">
                                        <td v-if="item.type==1">@{{ item.value | toCurrency }}</td>
                                        <td v-else>@{{ item.value}}</td>
                                        </template>
                                        <template v-else>
                                        <td></td>
                                        </template>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </various-charge-listing>

@endsection