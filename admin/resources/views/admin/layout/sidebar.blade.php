<?php

$user = Illuminate\Support\Facades\Auth::user();

if($user->hasRole('vendor'))
{
    $role = 'vendor';
}
else
{
    $role = 'admin';
}

?>

<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">{{ 'Menu' }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/dashboard') }}"><i class="nav-icon fa fa-tachometer"></i> {{ trans('Dashboard') }}</a></li>
            @if($role=='vendor')
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/booking-details') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.booking-detail.title') }}</a></li>
            @endif
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/top-sales') }}"><i class="nav-icon icon-graduation"></i> {{ trans('admin.top-sales.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/earnings') }}"><i class="nav-icon icon-graduation"></i> {{ trans('admin.earnings.title') }}</a></li>
           @if($role=='admin')
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/booking-details') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.booking-detail.title') }}</a></li>
            @endif
            
            @if($role=='vendor')
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/items') }}"><i class="nav-icon icon-drop"></i> {{ trans('admin.item.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/coupons') }}"><i class="nav-icon icon-star"></i> {{ trans('admin.coupon.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/table-banners-location-wises') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.table-banners-location-wise.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/categories') }}"><i class="nav-icon icon-magnet"></i> {{ trans('admin.category.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/companies') }}"><i class="nav-icon icon-star"></i> {{ trans('admin.company.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/slots') }}"><i class="nav-icon icon-graduation"></i> {{ trans('admin.slots.title') }}</a></li>
           @endif
           
           @if($role=='admin')
           <li class="nav-title">{{ 'VEHICLES' }}</li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/users') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.user.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/verification-panel') }}"><i class="nav-icon icon-star"></i> {{ __('Docs Verification') }}</a></li>
           

           <li class="nav-title">{{ 'SPECIALS' }}</li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/coupons') }}"><i class="nav-icon icon-star"></i> {{ trans('admin.coupon.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/table-banners-location-wises') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.table-banners-location-wise.title') }}</a></li>

           <li class="nav-title">{{ 'PLATFORM' }}</li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/categories') }}"><i class="nav-icon icon-magnet"></i> {{ trans('admin.category.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/companies') }}"><i class="nav-icon icon-star"></i> {{ trans('admin.company.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/customers') }}"><i class="nav-icon icon-star"></i> {{ __('Customers') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i class="nav-icon icon-globe"></i> {{ __('Merchants') }}</a></li>


           <li class="nav-title">{{ 'SETTINGS' }}</li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/faq-categories') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.faq-category.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/faqs') }}"><i class="nav-icon icon-puzzle"></i> {{ trans('admin.faq.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/various-charges') }}"><i class="nav-icon icon-magnet"></i> {{ trans('admin.various-charge.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/help-and-supports') }}"><i class="nav-icon icon-drop"></i> {{ trans('admin.help-and-support.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-settings') }}"><i class="nav-icon icon-compass"></i> {{ trans('admin.admin-setting.title') }}</a></li>
           
           <li class="nav-title">{{ 'STORE' }}</li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/app-packages') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.app-package.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/app-versions') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.app-version.title') }}</a></li>
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}
            <!-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li> -->
            @endif

            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            {{--<li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li>--}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
@include('notification.notification')