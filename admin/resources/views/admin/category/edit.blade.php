@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.category.actions.edit', ['name' => $category->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <category-form
                :action="'{{ $category->resource_url }}'"
                :data="{{ $category->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.category.actions.edit', ['name' => $category->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.category.components.form-elements')
                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('Category_Image'), 'has-success': fields.Category_Image && fields.Category_Image.valid }">
    <label for="Category_Image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.category.columns.category_image') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                        'mediaCollection' => app(App\Models\Category::class)->getMediaCollection('Category_Image'),
                        'media' => $category->getThumbs200ForCollection('Category_Image'),
                        'label' => 'Category image'
                    ])
                    <div v-if="errors.has('Category_Image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Category_Image') }}</div>
    </div>
</div>
                    </div>
                    
                    <table>                
                <th class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </th>
                <th class="card-footer">
                
                <a class="btn btn-danger" href="{{ url('admin/categories') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-close'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.cancel') }}</a>
               
            
                </th>
                </table>

                    
                </form>

        </category-form>

        </div>
    
</div>

@endsection