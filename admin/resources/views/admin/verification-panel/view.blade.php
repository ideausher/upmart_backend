@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.verification-panel.details'))

@section('body')

    <div class="container-xl">
        <div class="card">

            <verification-panel-form
                :action="'{{ $user->resource_url.'/update' }}'"
                :data="{{ $user->documents[0]->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="submitForm('verificationForm')" id="verificationForm" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-eye"></i> {{ trans('admin.verification-panel.details') }}
                    </div>

                    <div class="card-body">

                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
                    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.name') }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        {{$user->name}}
                        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
                    </div>
                </div>
                @if(!empty($user->date_of_birth))
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
                    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.date_of_birth') }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        {{$user->date_of_birth}}
                        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
                    </div>
                </div>
                @endif

                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('email'), 'has-success': fields.email && fields.email.valid }">
                    <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.email') }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        {{$user->email}}
                        <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}</div>
                    </div>
                </div>

                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('profile_picture'), 'has-success': fields.profile_picture && fields.profile_picture.valid }">
                    <label for="profile_picture" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.profile_picture') }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        <img src="{{$user->email}}" width="100" height="100">
                        <div v-if="errors.has('profile_picture')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('profile_picture') }}</div>
                    </div>
                </div>

                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('phone_number'), 'has-success': fields.phone_number && fields.phone_number.valid }">
                    <label for="phone_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.phone_number') }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        {{$user->country_code.$user->phone_number}}
                        <div v-if="errors.has('phone_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('phone_number') }}</div>
                    </div>
                </div>

                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.stripe && fields.stripe.valid }">
                    <label for="stripe" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Stripe' }}</label>
                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        Connected
                        <div v-if="errors.has('stripe')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('stripe') }}</div>
                    </div>
                </div>
                
                  
                    @if(!empty($user->documents[0]))

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.stripe && fields.stripe.valid }">
                    <h2 class="col-form-label col-sm-12 document-heading">Documents</h2>
                </div>

@if(!empty($user->documents[0]->registeration_number))

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.registeration_number && fields.registeration_number.valid }">
    <label for="registeration_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Registeration Number' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->registeration_number}}
        <div v-if="errors.has('registeration_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('registeration_number') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.vehicle_picture && fields.vehicle_picture.valid }">
    <label for="vehicle_picture" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Picture' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <img src="{{$user->documents[0]->vehicle_picture}}" width="100" height="100">
        <div v-if="errors.has('vehicle_picture')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_picture') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.vehicle_company && fields.vehicle_company.valid }">
    <label for="vehicle_company" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Company' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->vehicle_company}}
        <div v-if="errors.has('vehicle_company')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_company') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('vehicle_status'), 'has-success': fields.vehicle_status && fields.vehicle_status.valid }">
    <label for="vehicle_status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Status' }}</label>
    <div :class="isFormLocalized ? 'col-md-4 status-div' : 'col-md-9 col-xl-8 status-div'">
        <label>
            <input type="radio" v-model="form.vehicle_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_status'), 'form-control-success': fields.vehicle_status && fields.vehicle_status.valid}" id="vehicle_status" name="vehicle_status" value="2">
            Approved
        </label>
        <label>
            <input type="radio" v-model="form.vehicle_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_status'), 'form-control-success': fields.vehicle_status && fields.vehicle_status.valid}" id="vehicle_status" name="vehicle_status" value="3">
            Reject
        </label>
        <div v-if="errors.has('vehicle_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_status') }}</div>
    </div>
</div>

<div class="form-group row align-items-cente status-div-inner margin-static" :class="{'has-danger': errors.has('vehicle_reason'), 'has-success': fields.vehicle_reason && fields.vehicle_reason.valid }" id="main_vehicle_reason" style="display:none">
<label for="vehicle_reason" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Reject Reason' }}</label>
<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
<input type="text" v-model="form.vehicle_reason" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_reason'), 'form-control-success': fields.vehicle_reason && fields.vehicle_reason.valid}" id="vehicle_reason" name="vehicle_reason" placeholder="{{ 'Reject Reason' }}">
<div v-if="errors.has('vehicle_reason')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_reason') }}</div>
</div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.model && fields.model.valid }">
    <label for="model" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Model' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->model}}
        <div v-if="errors.has('model')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('model') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.color && fields.color.valid }">
    <label for="color" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Color' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->color}}
        <div v-if="errors.has('color')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('color') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static margin-last" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.valid_upto && fields.valid_upto.valid }">
    <label for="valid_upto" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Valid Upto' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->valid_upto}}
        <div v-if="errors.has('valid_upto')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('valid_upto') }}</div>
    </div>
</div>

@endif


@if(!empty($user->documents[0]->drivinglicence_no))
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.drivinglicence_images && fields.drivinglicence_images.valid }">
    <label for="drivinglicence_images" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Driving Licence Images' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <img src="{{$user->documents[0]->drivinglicence_images}}" width="100" height="100">
        <div v-if="errors.has('drivinglicence_images')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('drivinglicence_images') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('drivinglicence_status'), 'has-success': fields.drivinglicence_status && fields.drivinglicence_status.valid }">
<label for="drivinglicence_status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Driving Licence Status' }}</label>
<div :class="isFormLocalized ? 'col-md-4 status-div' : 'col-md-9 col-xl-8 status-div' ">
<label>
    <input type="radio" v-model="form.drivinglicence_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('drivinglicence_status'), 'form-control-success': fields.drivinglicence_status && fields.drivinglicence_status.valid}" id="drivinglicence_status" name="drivinglicence_status" value="2">
    Approved
</label>
<label>
    <input type="radio" v-model="form.drivinglicence_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('drivinglicence_status'), 'form-control-success': fields.drivinglicence_status && fields.drivinglicence_status.valid}" id="drivinglicence_status" name="drivinglicence_status" value="3">
    Reject
</label>
<div v-if="errors.has('drivinglicence_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('drivinglicence_status') }}</div>
</div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('drivinglicence_reason'), 'has-success': fields.drivinglicence_reason && fields.drivinglicence_reason.valid }" id="main_drivinglicence_reason" style="display:none">
<label for="drivinglicence_reason" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Reject Reason' }}</label>
<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
<input type="text" v-model="form.drivinglicence_reason" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('drivinglicence_reason'), 'form-control-success': fields.drivinglicence_reason && fields.drivinglicence_reason.valid}" id="drivinglicence_reason" name="drivinglicence_reason" placeholder="{{ 'Reject Reason' }}">
<div v-if="errors.has('drivinglicence_reason')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('drivinglicence_reason') }}</div>
</div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.drivinglicence_no && fields.drivinglicence_no.valid }">
    <label for="drivinglicence_no" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Driving Licence No.' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->drivinglicence_no}}
        <div v-if="errors.has('drivinglicence_no')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('drivinglicence_no') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.drivinglicence_expiry_date && fields.drivinglicence_expiry_date.valid }">
    <label for="drivinglicence_expiry_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Driving Licence Expiry Date' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->drivinglicence_expiry_date}}
        <div v-if="errors.has('drivinglicence_expiry_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('drivinglicence_expiry_date') }}</div>
    </div>
</div>

@endif

@if(!empty($user->documents[0]->no_plate))

<div class="form-group row align-items-center status-div-inner margin-static " :class="{'has-danger': errors.has('stripe'), 'has-success': fields.licence_no_plate_images && fields.licence_no_plate_images.valid }">
    <label for="licence_no_plate_images" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Licence No. Plate Images' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <img src="{{$user->documents[0]->licence_no_plate_images}}" width="100" height="100">
        <div v-if="errors.has('licence_no_plate_images')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('licence_no_plate_images') }}</div>
    </div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('licence_no_plate_status'), 'has-success': fields.licence_no_plate_status && fields.licence_no_plate_status.valid }">
<label for="licence_no_plate_status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Licence No Plate Status' }}</label>
<div :class="isFormLocalized ? 'col-md-4 status-div' : 'col-md-9 col-xl-8 status-div'">
<label>
    <input type="radio" v-model="form.licence_no_plate_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('licence_no_plate_status'), 'form-control-success': fields.licence_no_plate_status && fields.licence_no_plate_status.valid}" id="licence_no_plate_status" name="licence_no_plate_status" value="2">
    Approved
</label>
<label>
    <input type="radio" v-model="form.licence_no_plate_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('licence_no_plate_status'), 'form-control-success': fields.licence_no_plate_status && fields.licence_no_plate_status.valid}" id="licence_no_plate_status" name="licence_no_plate_status" value="3">
    Reject
</label>
<div v-if="errors.has('licence_no_plate_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('licence_no_plate_status') }}</div>
</div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static" :class="{'has-danger': errors.has('licence_no_plate_reason'), 'has-success': fields.licence_no_plate_reason && fields.licence_no_plate_reason.valid }" id="main_licence_no_plate_reason" style="display:none">
<label for="licence_no_plate_reason" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Reject Reason' }}</label>
<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
<input type="text" v-model="form.licence_no_plate_reason" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('licence_no_plate_reason'), 'form-control-success': fields.licence_no_plate_reason && fields.licence_no_plate_reason.valid}" id="licence_no_plate_reason" name="licence_no_plate_reason" placeholder="{{ 'Reject Reason' }}">
<div v-if="errors.has('licence_no_plate_reason')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('licence_no_plate_reason') }}</div>
</div>
</div>

<div class="form-group row align-items-center status-div-inner margin-static margin-last" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.no_plate && fields.no_plate.valid }">
    <label for="no_plate" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'No. Plate' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->no_plate}}
        <div v-if="errors.has('no_plate')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('no_plate') }}</div>
    </div>
</div>

@endif


@if(!empty($user->documents[0]->policy_number))

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.vehicle_insurence_image && fields.vehicle_insurence_image.valid }">
    <label for="vehicle_insurence_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Insurance Image' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <img src="{{$user->documents[0]->vehicle_insurence_image}}" width="100" height="100">
        <div v-if="errors.has('vehicle_insurence_image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_insurence_image') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.vehicle_insurence_valid_upto && fields.vehicle_insurence_valid_upto.valid }">
    <label for="vehicle_insurence_valid_upto" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Insurance Valid Upto' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->vehicle_insurence_valid_upto}}
        <div v-if="errors.has('vehicle_insurence_valid_upto')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_insurence_valid_upto') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.vehicle_insurence_valid_from && fields.vehicle_insurence_valid_from.valid }">
    <label for="vehicle_insurence_valid_from" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Insurance Valid From' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->vehicle_insurence_valid_from}}
        <div v-if="errors.has('vehicle_insurence_valid_from')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_insurence_valid_from') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('vehicle_insurence_status'), 'has-success': fields.vehicle_insurence_status && fields.vehicle_insurence_status.valid }">
<label for="vehicle_insurence_status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Vehicle Insurance Status' }}</label>
<div :class="isFormLocalized ? 'col-md-4 status-div' : 'col-md-9 col-xl-8 status-div'">
<label>
    <input type="radio" v-model="form.vehicle_insurence_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_insurence_status'), 'form-control-success': fields.vehicle_insurence_status && fields.vehicle_insurence_status.valid}" id="vehicle_insurence_status" name="vehicle_insurence_status" value="2">
    Approved
</label>
<label>
    <input type="radio" v-model="form.vehicle_insurence_status" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_insurence_status'), 'form-control-success': fields.vehicle_insurence_status && fields.vehicle_insurence_status.valid}" id="vehicle_insurence_status" name="vehicle_insurence_status" value="3">
    Reject
</label>
<div v-if="errors.has('vehicle_insurence_status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_insurence_status') }}</div>
</div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('vehicle_insurence_reason'), 'has-success': fields.vehicle_insurence_reason && fields.vehicle_insurence_reason.valid }" id="main_vehicle_insurence_reason" style="display:none">
<label for="vehicle_insurence_reason" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Reject Reason' }}</label>
<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
<input type="text" v-model="form.vehicle_insurence_reason" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('vehicle_insurence_reason'), 'form-control-success': fields.vehicle_insurence_reason && fields.vehicle_insurence_reason.valid}" id="vehicle_insurence_reason" name="vehicle_insurence_reason" placeholder="{{ 'Reject Reason' }}">
<div v-if="errors.has('vehicle_insurence_reason')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('vehicle_insurence_reason') }}</div>
</div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe'), 'has-success': fields.policy_number && fields.policy_number.valid }">
    <label for="policy_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Policy Number' }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        {{$user->documents[0]->policy_number}}
        <div v-if="errors.has('policy_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('policy_number') }}</div>
    </div>
</div>

@endif


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('verified'), 'has-success': fields.verified && fields.verified.valid }">
<label for="verified" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Verified' }}</label>
<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
@if($user->documents[0]->vehicle_status==3 && $user->documents[0]->drivinglicence_status==3 && $user->documents[0]->licence_no_plate_status==3 && $user->documents[0]->vehicle_insurence_status==3){{'Rejected'}}@elseif(($user->documents[0]->vehicle_status==1 || $user->documents[0]->vehicle_status==2 || $user->documents[0]->vehicle_status==3) && ($user->documents[0]->drivinglicence_status==1 || $user->documents[0]->drivinglicence_status==2 || $user->documents[0]->drivinglicence_status==3) && ($user->documents[0]->licence_no_plate_status==1 || $user->documents[0]->licence_no_plate_status==2 || $user->documents[0]->licence_no_plate_status==3) && ($user->documents[0]->vehicle_insurence_status==1 || $user->documents[0]->vehicle_insurence_status==2 || $user->documents[0]->vehicle_insurence_status==3)){{'Pending'}}@elseif($user->documents[0]->vehicle_status==0 && $user->documents[0]->drivinglicence_status==0 && $user->documents[0]->licence_no_plate_status==0 && $user->documents[0]->vehicle_insurence_status==0){{'Pending'}}@endif
    <div v-if="errors.has('verified')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('verified') }}</div>
</div>
</div>
@endif
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                        
                <a class="btn btn-danger" href="{{ url('admin/verification-panel') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Cancel</a>
                    </div>
                    
                </form>

        </verification-panel-form>

        </div>
    
</div>

@endsection

@section('bottom-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <script>
        var vehicle_Status = $('input[name="vehicle_status"]:checked').val();
                if(vehicle_Status==3)
                {
                    $("#main_vehicle_reason").show();
                }
                else
                {
                    $("#main_vehicle_reason").hide();
                }
            $('input[name="vehicle_status"]').on('change', function() {
                var vehicleStatus = $('input[name="vehicle_status"]:checked').val();
                if(vehicleStatus==3)
                {
                    $("#main_vehicle_reason").show();
                }
                else
                {
                    $("#main_vehicle_reason").hide();
                }
            });

            var driving_Status = $('input[name="drivinglicence_status"]:checked').val();
                if(driving_Status==3)
                {
                    $("#main_drivinglicence_reason").show();
                }
                else
                {
                    $("#main_drivinglicence_reason").hide();
                }
            
            $('input[name="drivinglicence_status"]').on('change', function() {
                var drivinglicence_status = $('input[name="drivinglicence_status"]:checked').val();
                if(drivinglicence_status==3)
                {
                    $("#main_drivinglicence_reason").show();
                }
                else
                {
                    $("#main_drivinglicence_reason").hide();
                }
            });

            var licence_Status = $('input[name="licence_no_plate_status"]:checked').val();
                if(licence_Status==3)
                {
                    $("#main_licence_no_plate_reason").show();
                }
                else
                {
                    $("#main_licence_no_plate_reason").hide();
                }

            $('input[name="licence_no_plate_status"]').on('change', function() {
                var licence_no_plate_status = $('input[name="licence_no_plate_status"]:checked').val();
                if(licence_no_plate_status==3)
                {
                    $("#main_licence_no_plate_reason").show();
                }
                else
                {
                    $("#main_licence_no_plate_reason").hide();
                }
            });

            var vehicle_insurance = $('input[name="vehicle_insurence_status"]:checked').val();
                if(vehicle_insurance==3)
                {
                    $("#main_vehicle_insurence_reason").show();
                }
                else
                {
                    $("#main_vehicle_insurence_reason").hide();
                }

            $('input[name="vehicle_insurence_status"]').on('change', function() {
                var vehicle_insurence_status = $('input[name="vehicle_insurence_status"]:checked').val();
                if(vehicle_insurence_status==3)
                {
                    $("#main_vehicle_insurence_reason").show();
                }
                else
                {
                    $("#main_vehicle_insurence_reason").hide();
                }
            });
        </script>
@endsection