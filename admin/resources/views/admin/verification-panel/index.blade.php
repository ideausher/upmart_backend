@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.verification-panel.title'))

@section('body')

    <user-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/verification-panel') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.verification-panel.details') }}
                        
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>                                        
                                        <th is='sortable' :column="'name'">{{ trans('admin.user.columns.name') }}</th>
                                        
                                        <th is='sortable' :column="'email'">{{ trans('admin.user.columns.email') }}</th>
                                       
                                        <th is='sortable' :column="'phone_number'">{{ trans('admin.user.columns.phone_number') }}</th>
                                       
                                        <th is='sortable' :column="'verified'">{{ trans('admin.user.columns.verified') }}</th>

                                        <th></th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        
                                        <td>@{{ item.name }}</td>
                                    
                                        <td>@{{ item.email }}</td>
                                    
                                        <td>@{{ item.country_code }}@{{ item.phone_number }}</td>

                                        <td v-for="(cc, aa) in item.documents">
                                        
                                            <span v-if="cc.vehicle_status==3 && cc.drivinglicence_status==3 && cc.licence_no_plate_status==3 && cc.vehicle_insurence_status==3">{{'Rejected'}}</span>

                                            <span v-else-if="cc.vehicle_status==0 && cc.drivinglicence_status==0 && cc.licence_no_plate_status==0 && cc.vehicle_insurence_status==0">{{'Pending'}}</span>

                                            <span v-else-if="(cc.vehicle_status==1 || cc.vehicle_status==2 || cc.vehicle_status==3) && (cc.drivinglicence_status==1 || cc.drivinglicence_status==2 || cc.drivinglicence_status==3) && (cc.licence_no_plate_status==1 || cc.licence_no_plate_status==2 || cc.licence_no_plate_status==3) && (cc.vehicle_insurence_status==1 || cc.vehicle_insurence_status==2 || cc.vehicle_insurence_status==3)">{{'Pending'}}</span>

                                            
                                        </td>

                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/view'" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </user-listing>


@endsection