@extends('brackets/admin-ui::admin.layout.default')

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com//ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('title', trans('Verification Panel'))

@section('body')
 

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Verification Panel</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <table id="tableId" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone Number</th>
                      <th>Verified</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
        
 
                    @foreach($data as $req)
                    <tr>
                    <td>{{$req->name}}</td>
                    <td>{{$req->email}}</td>
                    <td>{{$req->country_code.$req->phone_number}}</td>
                    <td>@if($req->documents[0]->vehicle_status==3 && $req->documents[0]->drivinglicence_status==3 && $req->documents[0]->licence_no_plate_status==3 && $req->documents[0]->vehicle_insurence_status==3){{'Rejected'}}@elseif(($req->documents[0]->vehicle_status==1 || $req->documents[0]->vehicle_status==2 || $req->documents[0]->vehicle_status==3) && ($req->documents[0]->drivinglicence_status==1 || $req->documents[0]->drivinglicence_status==2 || $req->documents[0]->drivinglicence_status==3) && ($req->documents[0]->licence_no_plate_status==1 || $req->documents[0]->licence_no_plate_status==2 || $req->documents[0]->licence_no_plate_status==3) && ($req->documents[0]->vehicle_insurence_status==1 || $req->documents[0]->vehicle_insurence_status==2 || $req->documents[0]->vehicle_insurence_status==3)){{'Pending'}}@endif</td>
                    <td><a class="btn btn-sm btn-spinner btn-info" href="{{url('/admin/verification-panel/'.$req->id.'/view')}}" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(function () {
      $.noConflict();
        $('#tableId').DataTable({
          "ordering": false,
          columnDefs: [{
            orderable: false,
            targets: "no-sort"
          }],
          "order": [[ 1, "desc" ]]
        });
    });
</script>
