@extends('brackets/admin-ui::admin.layout.default')

@section('title', 'Edit Shop Detail')

@section('body')

    <div class="container-xl">

        <div class="card">

            <shop-detail-form
                :action="'{{ url('admin/shop-detail') }}'"
                :data="{{ $editshop }}"
                v-cloak 
                inline-template>

                <form class="form-horizontal form-edit" method="post" @submit="modifyData()" @submit.prevent="onSubmit" :action="action">
                

                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ 'Edit Shop Detail' }}
                    </div>

                    <div class="card-body">   
                        
                        <div class="row">
                            
                            <div class="col-md-8">

                            <input type="hidden" id="hiidenlat" v-model="form.lat"/>
                            <input type="hidden" id="hiidenlng" v-model="form.lng"/>

                            <input type="hidden" id="latitude" name="lat" @input="validate($event)" v-model="form.lat"/>
                            <input type="hidden" id="longitude" name="lng" @input="validate($event)" v-model="form.lng"/>
                                
							<div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_name'), 'has-success': fields.Shop_Name && fields.shop_name.valid }">
								<label for="shop_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.shop_name') }}</label>
								<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
									<div>
										<textarea class="form-control" v-model="form.shop_name" v-validate="'required'" id="shop_name" name="shop_name" placeholder="{{ trans('admin.admin-user.columns.shop_name') }}"></textarea>
									</div>
									<div v-if="errors.has('shop_name')" class="form-control-feedback form-text" v-cloak>Shop name is required</div>
								</div>
							</div> 

							<div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_address'), 'has-success': fields.shop_address && fields.shop_address.valid }">
    <label for="shop_address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.shop_address') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
        <div>
            <input class="form-control" id="hiidenaddress" @change="modifyData(1)" v-model="form.shop_address" v-validate="'required'" id="shop_address" name="shop_address"></input>
        </div>
        <div v-if="errors.has('shop_address')" class="form-control-feedback form-text" v-cloak>Shop address is required</div>
    </div>
</div>
							<div class="form-group row align-items-center" :class="{'has-danger': errors.has('description'), 'has-success': fields.description && fields.description.valid }">
								<label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ 'Description' }}</label>
								<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
									<div>
										<textarea class="form-control" id="description" v-model="form.description" v-validate="'required'" id="description" name="description" placeholder="Description"></textarea>
									</div>
									<div v-if="errors.has('description')" class="form-control-feedback form-text" v-cloak>Description is required</div>
								</div>
							</div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </shop-detail-form>

        </div>

    </div>

@endsection


@section('bottom-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap" async defer></script>
<script>
    function initMap() {
        const input = document.getElementById("hiidenaddress");
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.setFields(["place_id", "geometry", "name", "formatted_address"]);
        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            var lat = place.geometry.location.lat()
            var lng = place.geometry.location.lng()
            $("#hiidenlat").val(lat);
            $("#hiidenlng").val(lng);
            
            $("#latitude").val(lat);
            $("#longitude").val(lng);
        });
        
    }
</script>
@endsection