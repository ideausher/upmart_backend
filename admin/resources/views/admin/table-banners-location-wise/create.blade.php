<?php

$user = Illuminate\Support\Facades\Auth::user();
    if($user->hasRole('vendor'))
    {
        $role = 'vendor';
    }
    else
    {
        $role = 'admin';
    }
    if($role=='vendor' || $role == 'distributor')
    {
        $heading = "Banners";
    }
    if($role=='admin'){
    $heading = "Banners Location Wise";
    }
?>
@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.table-banners-location-wise.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <table-banners-location-wise-form
            :action="'{{ url('admin/table-banners-location-wises') }}'"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit="modifyData()" @submit.prevent="onSubmit" :action="action" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.table-banners-location-wise.actions.create') }}
                </div>

                <div class="card-body">
                <input type="hidden" id="hiidenlat" />
                            <input type="hidden" id="hiidenlng" />
                    @include('admin.table-banners-location-wise.components.form-elements')

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('banner_image'), 'has-success': fields.banner_image && fields.banner_image.valid }">
    <label for="banner_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Banner Image' }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
    @include('brackets/admin-ui::admin.includes.media-uploader', [
    'mediaCollection' => app(App\Models\TableBannersLocationWise::class)->getMediaCollection('banner_image'),
    'label' => 'Banner Image'
])
    </div>
</div>
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </table-banners-location-wise-form>

        </div>

        </div>

    
@endsection


@section('bottom-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap" async defer></script>
<script>
    function initMap() {
        const input = document.getElementById("hiidenaddress");
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.setFields(["place_id", "geometry", "name", "formatted_address"]);
        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#hiidenlat").val(lat);
            $("#hiidenlng").val(lng);
            $("#hiidenaddress").val(place.formatted_address);
            $("#latitude").val(lat);
            $("#longitude").val(lng);
        });
    }
</script>
@endsection