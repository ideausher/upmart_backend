<div class="form-group row align-items-center" :class="{'has-danger': errors.has('category'), 'has-success': fields.category && fields.category.valid }">
    <label for="category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.faq.columns.category_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <!-- <input type="text" v-model="form.category_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('category_id'), 'form-control-success': fields.category_id && fields.category_id.valid}" id="category_id" name="category_id" placeholder="{{ trans('admin.faq.columns.category_id') }}"> -->

        <multiselect v-model="form.category" :name="category" :options="{{ $faqCat->toJson() }}" label="category" track-by="id"
            id="category" name="category" :multiple="false">
        </multiselect>

        <div v-if="errors.has('category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('category') }}</div>
    </div>
</div>
<div class="" v-for="(input,k) in form.questions" :key="k">

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('questions'), 'has-success': fields.questions && fields.questions.valid }">
    <label for="questions" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.faq.columns.question') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.questions[k]" id="questions" name="questions"></textarea>
        </div>
        

        <div v-if="errors.has('questions')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('questions') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('answers'), 'has-success': fields.answers && fields.answers.valid }">
    <label for="answers" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.faq.columns.answer') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.answers[k]" id="answers" name="answers"></textarea>
        </div>

        <div v-if="errors.has('answers')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('answers') }}</div>
    </div>
</div>
<div class="form-group row align-items-right">
<span>
            <i class="fa fa-minus-circle" @click="remove(k)" v-show="form.questions.length > 1"></i>
            <i class="fa fa-plus-circle" @click="add(k)" v-show="k+1 == form.questions.length"></i>
        </span>
</div>
       
</div>