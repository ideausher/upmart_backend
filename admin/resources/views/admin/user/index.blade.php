@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user.actions.index'))

@section('body')

    <user-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/users') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.user.actions.index') }}
                        
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" id='search-input' v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <select class="form-control" name="filter_verified" id="filter-verified" v-model="search" onchange="verify();" />
                                                <option value="" selected>Select Verified</option>
                                                <option value="0">All</option>
                                                <option value="1">Pending</option>
                                                <option value="2">Approved</option>
                                                <option value="3">Rejected</option>
                                            </select>
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>                                        
                                        <th is='sortable' :column="'name'">{{ trans('admin.user.columns.name') }}</th>
                                        
                                        <th is='sortable' :column="'email'">{{ trans('admin.user.columns.email') }}</th>
                                       
                                        <th is='sortable' :column="'phone_number'">{{ trans('admin.user.columns.phone_number') }}</th>
                                       
                                        <th is='sortable' :column="'verified'">{{ trans('admin.user.columns.verified') }}</th>
                                       
                                        <th is='sortable' :column="'status'">{{ trans('admin.user.columns.status') }}</th>

                                        <th></th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                    
                                        <td>@{{ item.name }}</td>
                                    
                                        <td>@{{ item.email }}</td>
                                    
                                        <td>@{{ item.country_code }}@{{ item.phone_number }}</td>

                                        <td v-for="(cc, aa) in item.documents_temp" v-if="item.documents_temp[0]">
                                        
                                        <span v-if="cc.vehicle_status==3 && cc.drivinglicence_status==3 && cc.licence_no_plate_status==3 && cc.vehicle_insurence_status==3">{{'Rejected'}}</span>

                                        <span v-else-if="cc.vehicle_status==0 || cc.drivinglicence_status==0 || cc.licence_no_plate_status==0 || cc.vehicle_insurence_status==0">{{'Pending'}}</span>

                                            <span v-else-if="(cc.vehicle_status==1 || cc.vehicle_status==2 || cc.vehicle_status==3) && (cc.drivinglicence_status==1 || cc.drivinglicence_status==2 || cc.drivinglicence_status==3) && (cc.licence_no_plate_status==1 || cc.licence_no_plate_status==2 || cc.licence_no_plate_status==3) && (cc.vehicle_insurence_status==1 || cc.vehicle_insurence_status==2 || cc.vehicle_insurence_status==3)">{{'Pending'}}</span>

                                            
                                        </td>

                                        <td v-if="!item.documents_temp[0] && item.documents[0]">{{'Approved'}}</td>

                                        <td v-if="!item.documents_temp[0] && !item.documents[0]">{{'Pending '}}</td>

                                        <template v-if="!item.is_block">
                                        <td >

                                            <!-- <label class="switch switch-3d switch-success" :for="item.id" :forN="item.name" onclick="myFunction(this)" data-toggle="modal" data-target="#exampleModal">

                                                <input type="checkbox" class="switch-input"  v-model="collection[index].is_block"

                                                @change="toggleSwitch(item.resource_url, 'is_block', collection[index])"/>
                                                <span class="switch-slider"></span>
                                            </label> -->

                                            <a class="btn btn-sm btn-success" title="{{ 'Inactive' }}" role="button" :for="item.id" :forN="item.name" onclick="myFunction(this)" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-toggle-on"></i></a>
                                                </div>
                                        </td>
                                      </template>

                                    <template v-else>

                                        <td v-if="item.is_block">
                                            <!-- <label class="switch switch-3d switch-danger" :for="item.id" onclick="myFunction1(this)" data-toggle="modal" data-target="#exampleModal1"/>
                                                <input type="checkbox" class="switch-input"   v-model="collection[index].is_block"
                                                     data-target="#exampleModal" data-toggle="modal"
                                                    @change="toggleSwitch(item.resource_url, 'is_block', collection[index])"/>
                                                <span class="switch-slider"></span>
                                            </label> -->

                                            <a class="btn btn-sm btn-danger" title="{{ 'Active' }}" role="button" :for="item.id" onclick="myFunction1(this)" data-toggle="modal" data-target="#exampleModal1"><i class="fa fa-toggle-off"></i></a>

                                        </td>
                                    </template>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/view'" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a>
                                                </div>
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/bookings'" title="Booking" role="button"><i class="fa fa-ticket"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>
                            <!-- <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                               
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </user-listing>


    <!-- Modal -->
    <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to make Inactive this delivery boy?</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm()">

                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>
                        <form method="get" name="reason_form"  id="form" onsubmit="return validateReasonForm()"
                            action="{{ url('admin/users/block') }}">
                            <div class="modal-iiner-content">
                                <input type="hidden" name="userid" id="userId">
                                <input type="hidden" name="username" id="userName">
                                {{ csrf_field() }}
                                </span>Please Enter Reason For Inactive</span>
                                <input type="text" name="reason" id="myMessage">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="resetForm()">Cancel</button>
                                <input type="submit" class="btn btn-primary" value="Yes, Inactive" id="block">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade modal-custom" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to make Active this delivery boy?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>
                        <form method="get" name="reason_form"  id="form"
                            action="{{ url('admin/users/unblock') }}">
                            <div class="modal-iiner-content">
                                <input type="hidden" name="userid" id="userIduser">
                                {{ csrf_field() }}
                                {{-- </span>Please Enter Reason For Block</span>
                                <input type="text" name="reason" id="myMessage"> --}}
                            </div>  
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <input type="submit" class="btn btn-primary" value="Yes, Active" id="block">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


<!-- unblock vendor -->

@endsection

@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ url('js/popup.js') }}" type="text/javascript"></script>

    <script>
        function verify()
        {
            var vId = $('#filter-verified option:selected').val();
            var sval = $('#search-input').val();

            if(vId==sval)
            {
                $('#search-input').val('');
            }
           
        }
    </script>

@endsection