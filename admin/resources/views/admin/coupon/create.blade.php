@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.coupon.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <coupon-form
            :action="'{{ url('admin/coupons') }}'"
            :data = "{{$code}}"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="submitForm('couponForm','add')" :action="action" id="couponForm" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.coupon.actions.create') }}
                </div>

                <div class="card-body">
                
                    @include('admin.coupon.components.form-elements')
                    @if($role=='vendor')
                        <input type="hidden" name="shopId" v-model="form.shopId" id="shopId">
                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('isPrimary'), 'has-success': fields.isPrimary && fields.isPrimary.valid }">
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                <input class="form-check-input" id="isPrimary" type="checkbox" v-model="form.isPrimary" data-vv-name="isPrimary"  name="isPrimary_fake_element">
                                    <label class="form-check-label" for="isPrimary">
                                        {{ 'Is Primary' }}
                                    </label>
                                <div v-if="errors.has('isPrimary')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('isPrimary') }}</div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-lg new-modal" data-toggle="modal" data-target="#myModal"></button>
                    @endif
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_image'), 'has-success': fields.coupon_image && fields.coupon_image.valid }">
    <label for="coupon_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_image') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        @include('brackets/admin-ui::admin.includes.media-uploader',[
            'mediaCollection' => app(App\Models\Coupon::class)->getMediaCollection('coupon_image'),
            'label' => 'Coupon Image'
        ])
        <div v-if="errors.has('coupon_image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_image') }}</div>
    </div>
</div>
                </div>
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </coupon-form>

        </div>

        </div>

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
          <p>You have already selected '{{$primaryName}}' as primary coupon!<br> Do you want to change it?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="no">No</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="yes">Yes</button>
        </div>
      </div>
      
    </div>
  </div>


@endsection

@section('bottom-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script>
    var is_primary = "<?php echo $primaryExist; ?>";

    $('#isPrimary').on('change', function() {
        var isPrimary = $('#isPrimary:checked').val();
       if(is_primary==1&&(isPrimary=="on" || isPrimary==""))
       {
           $(".new-modal").trigger("click");
       }
    });
    $("#yes").click(function(){
        $("#isPrimary").prop("checked", true);
        $('#isPrimary:checked').val("on");
    });
    $("#no").click(function(){
        $( "#isPrimary" ).trigger( "click" );
        $("#isPrimary").prop("checked", false);
        $('#isPrimary:checked').val("");
    });

</script>

@endsection