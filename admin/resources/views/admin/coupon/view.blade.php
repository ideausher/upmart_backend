@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.coupon.details'))

@section('body')

    <div class="container-xl">
        <div class="card">

            <coupon-form
                :data="{{ $details->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                        <i class="fa fa-eye"></i> {{ trans('admin.coupon.details') }}
                    </div>

                    <div class="card-body">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shopId'), 'has-success': fields.shopId && fields.shopId.valid }">
                        <label for="shopId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.shopId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->shopId}}
                            <div v-if="errors.has('shopId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shopId') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_name'), 'has-success': fields.coupon_name && fields.coupon_name.valid }">
                        <label for="coupon_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_name') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->coupon_name}}
                            <div v-if="errors.has('coupon_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_name') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_code'), 'has-success': fields.coupon_code && fields.coupon_code.valid }">
                        <label for="coupon_code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_code') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->coupon_code}}
                            <div v-if="errors.has('coupon_code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_code') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_description'), 'has-success': fields.coupon_description && fields.coupon_description.valid }">
                        <label for="coupon_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_description') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->coupon_description}}
                            <div v-if="errors.has('coupon_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_description') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('isPrimary'), 'has-success': fields.isPrimary && fields.isPrimary.valid }">
                        <label for="isPrimary" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.isPrimary') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($details->isPrimary==2){{'Yes'}}@elseif($details->isPrimary==3){{'No'}}@endif
                            <div v-if="errors.has('isPrimary')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('isPrimary') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_type'), 'has-success': fields.coupon_type && fields.coupon_type.valid }">
                        <label for="coupon_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_type') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($details->coupon_type==0){{'Percentage'}}@elseif($details->coupon_type==1){{'Fixed'}}@endif
                            <div v-if="errors.has('coupon_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_type') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_discount'), 'has-success': fields.coupon_discount && fields.coupon_discount.valid }">
                        <label for="coupon_discount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_discount') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($details->coupon_type==1)@{{form.coupon_discount | toCurrency}}@else@{{form.coupon_discount}}@endif
                            <div v-if="errors.has('coupon_discount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_discount') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_min_amount'), 'has-success': fields.coupon_min_amount && fields.coupon_min_amount.valid }">
                        <label for="coupon_min_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_min_amount') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @{{form.coupon_min_amount | toCurrency}}
                            <div v-if="errors.has('coupon_min_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_min_amount') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_max_amount'), 'has-success': fields.coupon_max_amount && fields.coupon_max_amount.valid }">
                        <label for="coupon_max_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_max_amount') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @{{form.coupon_max_amount | toCurrency}}
                            <div v-if="errors.has('coupon_max_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_max_amount') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('maximum_total_use'), 'has-success': fields.maximum_total_use && fields.maximum_total_use.valid }">
                        <label for="maximum_total_use" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.maximum_total_use') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->maximum_total_use}}
                            <div v-if="errors.has('maximum_total_use')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('maximum_total_use') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('maximum_per_customer_use'), 'has-success': fields.maximum_per_customer_use && fields.maximum_per_customer_use.valid }">
                        <label for="maximum_per_customer_use" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.maximum_per_customer_use') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->maximum_per_customer_use}}
                            <div v-if="errors.has('maximum_per_customer_use')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('maximum_per_customer_use') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('start_date'), 'has-success': fields.start_date && fields.start_date.valid }">
                        <label for="start_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.start_date') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->start_date}}
                            <div v-if="errors.has('start_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('start_date') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('end_date'), 'has-success': fields.end_date && fields.end_date.valid }">
                        <label for="end_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.end_date') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details->end_date}}
                            <div v-if="errors.has('end_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('end_date') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('coupon_image'), 'has-success': fields.coupon_image && fields.coupon_image.valid }">
                        <label for="coupon_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.coupon.columns.coupon_image') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            <img src="{{$details->coupon_image}}" width="140" height="142">
                            <div v-if="errors.has('coupon_image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('coupon_image') }}</div>
                        </div>
                    </div>

                    </div>

                    <div class="card-footer">
                <a class="btn btn-danger" href="{{ url('admin/coupons') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Back</a>
                </div>
                    

                </div>

        </coupon-form>

        </div>
    
</div>


@endsection
