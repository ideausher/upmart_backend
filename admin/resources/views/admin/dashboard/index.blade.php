@extends('brackets/admin-ui::admin.layout.default')


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

<style type="text/css">
  .card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
</style>


@section('body')
<?php
 $user = \Illuminate\Support\Facades\Auth::user();



?>
<div class="container">
@if($user->hasRole('vendor'))
<div class="row">
    <div class="col-md-6">
      <div class="card-counter primary">
        <i class="fa fa-ticket"></i>
        <span class="count-numbers">{{ $todayBookings }}</span>
        <span class="count-name">Today's Bookings</span>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card-counter primary">
        <i class="fa fa-dollar"></i>
        <span class="count-numbers">{{ $todayEarning }}</span>
        <span class="count-name">Today's Earnings</span>
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="card-counter primary">
        <i class="fa fa-shopping-cart"></i>
        <span class="count-numbers">{{ $totalBookings }}</span>
        <span class="count-name">Total Bookings</span>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card-counter primary">
        <i class="fa fa-clock-o"></i>
        <span class="count-numbers">{{ $totalEarnings }}</span>
        <span class="count-name">Total Earnings</span>
      </div>
    </div>
  </div>
@else
<div class="row">
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-ticket"></i>
        <span class="count-numbers">{{ $todayBookings }}</span>
        <span class="count-name">Today's Bookings</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-dollar"></i>
        <span class="count-numbers">{{ $todayEarning }}</span>
        <span class="count-name">Today's Earnings</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-clock-o"></i>
        <span class="count-numbers">{{ $pendingPartnerVerification }}</span>
        <span class="count-name">Pending Partner Verifications</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-shopping-cart"></i>
        <span class="count-numbers">{{ $totalBookings }}</span>
        <span class="count-name">Total Bookings</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-handshake-o"></i>
        <span class="count-numbers">{{ $totalPartner }}</span>
        <span class="count-name">Total Partners</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-users"></i>
        <span class="count-numbers">{{ $totalCustomer }}</span>
        <span class="count-name">Total Customers</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-map-marker"></i>
        <span class="count-numbers">{{ $totalShopLocations }}</span>
        <span class="count-name">Total Shops</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-dollar"></i>
        <span class="count-numbers">{{ $totalEarnings }}</span>
        <span class="count-name">Total Earnings</span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-counter primary">
        <i class="fa fa-clock-o"></i>
        <span class="count-numbers">{{ $totalSales }}</span>
        <span class="count-name">Total Sales</span>
      </div>
    </div>
  </div>
</div>
@endif


@endsection
