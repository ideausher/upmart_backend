

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('Company_Name'), 'has-success': fields.Company_Name && fields.Company_Name.valid }">
    <label for="Company_Name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Company_Name</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select class="form-control" id="sel1" v-model="form.Company_Name" name="Company_Name" placeholder="Select the Company">
        @foreach($company_values as $company)
               <option value="{{$company}}" name="Company_Name">{{$company}}</option>
                @endforeach
        </select>
        <div v-if="errors.has('Company_Name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('Company_Name') }}</div>
    </div>
</div>
