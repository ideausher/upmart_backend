<?php

use App\Models\AdminUsers;

$user = Illuminate\Support\Facades\Auth::user();
$shop = AdminUsers::find($user->id);

?>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_name'), 'has-success': fields.product_name && fields.product_name.valid }">
    <label for="product_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.product_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.product_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('product_name'), 'form-control-success': fields.product_name && fields.product_name.valid}" id="product_name" name="product_name" placeholder="{{ trans('admin.item.columns.product_name') }}">
        <div v-if="errors.has('product_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('company_name'), 'has-success': fields.company_name && fields.company_name.valid }">
    <label for="company_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.company_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select class="form-control" id="sel1" v-model="form.company_name" name="company_name" placeholder="Select the Company">
        @foreach($company_values as $company)
               <option value="{{$company}}" name="company_name">{{$company}}</option>
                @endforeach
        </select>
        <div v-if="errors.has('company_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_description'), 'has-success': fields.product_description && fields.product_description.valid }">
    <label for="product_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.product_description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <textarea v-model="form.product_description" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('product_description'), 'form-control-success': fields.product_description && fields.product_description.valid}" id="product_description" name="product_description" placeholder="{{ trans('admin.item.columns.product_description') }}">
        </textarea>
        <div v-if="errors.has('product_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_description') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('stock_quantity'), 'has-success': fields.stock_quantity && fields.stock_quantity.valid }">
    <label for="stock_quantity" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.stock_quantity') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.stock_quantity" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('stock_quantity'), 'form-control-success': fields.stock_quantity && fields.stock_quantity.valid}" id="stock_quantity" name="stock_quantity" placeholder="{{ trans('admin.item.columns.stock_quantity') }}">
        <div v-if="errors.has('stock_quantity')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('stock_quantity') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('price'), 'has-success': fields.price && fields.price.valid }">
    <label for="price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.price') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.price" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('price'), 'form-control-success': fields.price && fields.price.valid}" id="price" name="price" placeholder="{{ trans('admin.item.columns.price') }}">
        <div v-if="errors.has('price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('price') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('discount'), 'has-success': fields.discount && fields.discount.valid }">
    <label for="discount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.discount') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.discount" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('discount'), 'form-control-success': fields.discount && fields.discount.valid}" id="discount" name="discount" placeholder="{{ trans('admin.item.columns.discount') }}">
        <div v-if="errors.has('discount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('discount') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('new_price'), 'has-success': fields.new_price && fields.new_price.valid }">
    <label for="new_price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.new_price') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.new_price" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('new_price'), 'form-control-success': fields.new_price && fields.new_price.valid}" id="new_price" name="new_price" placeholder="{{ trans('admin.item.columns.new_price') }}" readonly>
        <div v-if="errors.has('new_price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('new_price') }}</div>
    </div>
</div>
@if($shop->vendorrelationship->gst)
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('gst_enable'), 'has-success': fields.gst_enable && fields.gst_enable.valid }">
    <label for="gst_enable" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.gst_enable') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
		<template v-if="!form.gst_enable">
			

					<label class="switch switch-3d switch-success" data-toggle="modal" data-target="#exampleModal">

						<input type="checkbox" class="switch-input" v-model="form.gst_enable" />
						<span class="switch-slider"></span>
					</label>

				</template>

			<template v-else>

					<label class="switch switch-3d switch-success" data-toggle="modal" data-target="#exampleModal1" />
						<input type="checkbox" class="switch-input" v-model="form.gst_enable"
								data-target="#exampleModal" data-toggle="modal">
						<span class="switch-slider"></span>
					</label>
			</template>
		</div>
</div>
@endif