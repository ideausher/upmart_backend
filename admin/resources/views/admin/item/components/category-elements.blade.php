<div class="form-group row align-items-center"
    :class="{'has-danger': errors.has('category'), 'has-success': fields.category && fields.category.valid }">
    <label for="category" class="col-form-label text-md-right"
        :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.category_name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.category" :name="category" :options="{{ $category->toJson() }}" label="category_name" track-by="id"
            id="category" name="category" :multiple="false">
        </multiselect>
        <div v-if="errors.has('category')" class="form-control-feedback form-text" v-cloak>
            @{{ errors . first('category') }}</div>
    </div>
</div>