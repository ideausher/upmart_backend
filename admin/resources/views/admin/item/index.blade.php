@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.item.actions.index'))

@section('body')

    <item-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/items') }}'"
        inline-template>


        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.item.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/items/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.item.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                        
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                           
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        
                                        <th v-if="!(collection.length > 0  && collection[0] && collection[0].searchValue)" class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'">{{ trans('admin.item.columns.id')}}</th>
                                        <th is='sortable' :column="'product_name'">{{ trans('admin.item.columns.product_name') }}</th>
                                        <th is='sortable' :column="'company_name'">{{ trans('admin.item.columns.company_name') }}</th>
                                        <th is='sortable' :product_description="'product_description'">{{ trans('admin.item.columns.product_description') }}</th>
                                        <th is='sortable' :column="'stock_quantity'">{{ trans('admin.item.columns.stock_quantity') }}</th>
                                        <th is='sortable' :column="'price'">{{ trans('admin.item.columns.price') }}</th>
                                        <th is='sortable' :column="'product_images'">{{ trans('admin.item.columns.product_images') }}</th>
                                        <th is='sortable' :column="'category_name'">{{ trans('admin.item.columns.category_name') }}</th>
                                        <th v-if="!(collection.length > 0  && collection[0] && collection[0].searchValue)" is='sortable' :column="'is_disabled'">{{ trans('admin.item.columns.is_disabled') }}</th>
                                        <template v-for="(xx, yy) in collection">
                                        <th v-if="xx.shop_id === xx.shopid && yy == 0" is='sortable' :column="'product_by'">{{ trans('admin.item.columns.product_by') }}</th>
                                        </template>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="8">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/items')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/items/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td v-if="!collection[0].searchValue" class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>
                                        
                                    <td>@{{ item.id }}</td>
                                        <td>@{{ item.product_name }}</td>
                                        <td>@{{ item.company_name }}</td>
                                        <td>@{{ item.product_description }}</td>
                                        <td>@{{ item.stock_quantity }}</td>
                                        <td>@{{ item.new_price | toCurrency }}</td>
                                        <td><img :src="item.mediaUrl" height="50" width="50"></td>
                                        <td>@{{ item.category_name }}</td>
                                        

                                        <template v-if="!item.is_disabled && !collection[0].searchValue">
                                            <td >

                                                <label class="switch switch-3d switch-success" :for="item.id" onclick="mydisable(this)" data-toggle="modal" data-target="#exampleModal">

                                                    <input type="checkbox" class="switch-input"  v-model="collection[index].is_disabled"

                                                    @change="toggleSwitch(item.resource_url, 'is_disabled', collection[index])"/>
                                                    <span class="switch-slider"></span>
                                                </label>

                                            </td>
                                        </template>

                                        <template v-else>

                                            <td v-if="item.is_disabled && !collection[0].searchValue">
                                                <label class="switch switch-3d switch-danger" :for="item.id" onclick="myenable(this)" data-toggle="modal" data-target="#exampleModal1"/>
                                                    <input type="checkbox" class="switch-input"   v-model="collection[index].is_disabled"
                                                        data-target="#exampleModal" data-toggle="modal"
                                                        @change="toggleSwitch(item.resource_url, 'is_disabled', collection[index])"/>
                                                    <span class="switch-slider"></span>
                                                </label>
                                            </td>
                                        </template>

                                        <td v-if="item.shop_id === item.shopid">{{ 'Me' }}</td>
                                        <td v-if="item.shop_id !== item.shopid">{{ 'Not me' }}</td>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                            <template v-if="item.shop_id === item.shopid">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                            </template>
                                                <template v-if="item.shop_id === item.shopid">
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                                </template>
                                                <template v-if="item.shop_id !== item.shopid">
                                                
                                                <form v-if="collection[0].searchValue" class="col" action="{{url('admin/items/import')}}">
                                                    <input type="hidden" name="item_id" id="item_id" :value="item.id">
                                                    <button type="submit" class="btn btn-sm btn-primary" title="{{ 'Import' }}"><i class="fa fa-copy"></i></button>
                                                </form>
                                                </template>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/items/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.item.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </item-listing>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to disable this item?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div>

                    <form method="get" name="reason_form" id="form" action="{{ url('admin/items/disable') }}">
                        <input type="hidden" name="itemId" id="itemId">
                        {{ csrf_field() }}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Disable" id="disable">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to enable this item?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div>

                    <form method="get" name="reason_form"  id="form"
                        action="{{ url('admin/items/enable') }}">
                        <input type="hidden" name="itemId" id="itemIditem">
                        {{ csrf_field() }}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Enable" id="enable">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


<!-- unblock item -->

@endsection



@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>


    <script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
@endsection
