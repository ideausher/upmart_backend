@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.item.actions.create'))

@section('body')

    <div class="container-xl">

                <div class="card">
        
        <item-form
            :action="'{{ url('admin/items') }}'"
            :data="{{$data}}"
            v-cloak
            inline-template>

            <form class="form-horizontal form-create" method="post" @submit.prevent="submitForm('itemForm')" :action="action" id="itemForm" novalidate>
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.item.actions.create') }}
                </div>

                <div class="card-body">
                    @include('admin.item.components.form-elements')
                    

                        <input type="hidden" name="shop_id" v-model="form.shop_id" id="shop_id">
                        <input type="hidden"  name="shop_name" v-model="form.shop_name" id="shop_name">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_images'), 'has-success': fields.product_images && fields.product_images.valid }">
                    <label for="product_images" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.product_images') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
    @include('brackets/admin-ui::admin.includes.media-uploader', [
    'mediaCollection' => app(App\Models\Item::class)->getMediaCollection('product_images'),
    'label' => 'Product images'
])
    </div>
</div>

                    @include('admin.item.components.category-elements')

                </div>
                
                                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
                
            </form>

        </item-form>

        </div>

        </div>

    
@endsection
