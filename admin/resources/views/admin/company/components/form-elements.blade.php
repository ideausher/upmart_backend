<div class="form-group row align-items-center" :class="{'has-danger': errors.has('company_name'), 'has-success': fields.company_name && fields.company_name.valid }">
    <label for="company_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.company_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.company_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('company_name'), 'form-control-success': fields.company_name && fields.company_name.valid}" id="company_name" name="company_name" placeholder="{{ trans('admin.company.columns.company_name') }}">
        <div v-if="errors.has('company_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('company_description'), 'has-success': fields.company_description && fields.company_description.valid }">
    <label for="company_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.company_description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.company_description" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('company_description'), 'form-control-success': fields.company_description && fields.company_description.valid}" id="company_description" name="company_description" placeholder="{{ trans('admin.company.columns.company_description') }}">
        <div v-if="errors.has('company_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company_description') }}</div>
    </div>
</div>


