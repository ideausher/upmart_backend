@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.slots.title'))

@section('body')
<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" />
{{-- <link href="{{ URL::asset('clockpicker/assets/css/bootstrap.min.css') }}" rel="stylesheet" /> --}}
<link href="{{ URL::asset('clockpicker/dist/bootstrap-clockpicker.css') }}" rel="stylesheet" />

<slot-listing :data="{{ $data->toJson() }}" :url="'{{ url('admin/slots') }}'" inline-template>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.slots.title') }}
                    {{-- <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/slots/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.slot.actions.create') }}</a> --}}
                </div>
                <div class="card-body" v-cloak>
                    <div class="card-block">
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        {{-- <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" /> --}}
                                        {{-- <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span> --}}
                                    </div>
                                </div>
                                <div class="col-sm-auto form-group ">
                                    {{-- <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>  --}}
                                </div>
                            </div>
                        </form>

                        <div class="slottable" id="slottable">

                            @include('admin.slots.components.slot-element')

                        </div>

                        {{-- Model start  --}}

                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Add Slot</h5>
                                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group clockpicker">
                                                    <input type="text" class="form-control" id="slot_from" name="slot_from" placeholder="Start Time" value="">
                                                    <span class="input-group-addon fa fa-clock-o">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group clockpicker">
                                                    <input type="text" class="form-control" id="slot_to" name="slot_to" placeholder="End Time" value="">
                                                    <span class="input-group-addon fa fa-clock-o">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="edit_slot" class="form-control">
                                        <input type="hidden" value={{$shop_id}} id="user_id">
                                        <input type="hidden" id="day" class="form-control">
                                    </div>


                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-primary" id="add_update_slot" data-dismiss="modal">Confirm</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <h5 class="modal-heading" id="myModal">Warning!</h5>
                                    <p>Do you really want to delete this item?</p>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" id="deleteSlot" class="btn btn-sm btn-danger pr-3 pl-3 float-right">Delete</button>
                                    <button type="button" id="close" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>

                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-sm-auto">
                        <pagination></pagination>
                    </div> --}}
                </div>

                {{-- <div class="no-items-found" v-if="!collection.length > 0">
                    <i class="icon-magnifier"></i>
                    <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                    <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                    <a class="btn btn-primary btn-spinner" href="{{ url('admin/slots/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.slot.actions.create') }}</a> --}}
                {{-- </div> --}}
            </div>
        </div>
    </div>
    </div>
    </div>
</slot-listing>

@endsection
@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ URL::asset('clockpicker/assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('clockpicker/dist/bootstrap-clockpicker.js') }}"></script>
<script src="{{ url('js/slot.js') }}" type="text/javascript"></script>
@endsection
