<div class="row">
    <div class="col-lg-12">
        @php
        $days_array = ['1'=>'monday','2'=>'tuesday','3'=>'wednesday','4'=>'thursday','5'=>'friday','6'=>'saturday','7'=>'sunday'];
        $days_slot_box_array = array();
        foreach ($data as $slot) {
        $slot_from = date('H:i a', strtotime($slot->slot_from));
        $slot_to = date('H:i a', strtotime($slot->slot_to));
        $slot_id = $slot->id;
        $slot_box = '<span class="span-tab" data-id="' . $slot_id . '" data-toggle="modal" data-target="#exampleModalCenter"><span id="slot_capacity" hidden>' . $slot->capacity . '</span><span id="start_time">' . $slot_from . '</span>-<span id="end_time">' . $slot_to . '</span></span><a style="display:inline-block; width:20px;" class="delete_this_slote" data-toggle="modal" data-target="#myModal" data-delid="' . $slot->id . '"><i class=" fa fa-close"></i></a>';
        if(array_key_exists($slot->day,$days_slot_box_array)){
        $days_slot_box_array[$slot->day] .= $slot_box;
        }else{
        $days_slot_box_array[$slot->day] = $slot_box;
        }

        }
        @endphp
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            @for ($day=1; $day<=7;$day++) <li class="nav-item">
                <a class="nav-link " id="home-tab" data-toggle="tab" href="#{!! $days_array[$day] !!}" role="tab" aria-controls="{!! $days_array[$day] !!}" aria-selected="true"><b>{!! ucfirst($days_array[$day]) !!}</b></a>
                @if(array_key_exists($day,$days_slot_box_array))
                {!!$days_slot_box_array[$day] !!}
                @endif
                <button type="button" title="Add new Slots" class="btn btn-primary modal-window" data-idval="{!! $day !!}" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus"></i></button>
                </li>
                @endfor

        </ul>
    </div>
</div>
