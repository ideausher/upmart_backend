@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.help-and-support.actions.show'))

@section('body')

    <div class="container-xl">
        <div class="card">

            <booking-detail-form
                :data="{{ $helpAndSupport->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                    
                        <i class="fa fa-eye"></i> {{ trans('admin.help-and-support.actions.show') }}
                    </div>

                    <div class="card-body">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('feedback_title'), 'has-success': fields.feedback_title && fields.feedback_title.valid }">
                        <label for="feedback_title" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.feedback_title') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->feedback_title}}
                            <div v-if="errors.has('feedback_title')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('feedback_title') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('feedback_description'), 'has-success': fields.feedback_description && fields.feedback_description.valid }">
                        <label for="feedback_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.feedback_description') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->feedback_description}}
                            <div v-if="errors.has('feedback_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('feedback_description') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('ticket_id'), 'has-success': fields.ticket_id && fields.ticket_id.valid }">
                        <label for="ticket_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.ticket_id') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->ticket_id}}
                            <div v-if="errors.has('ticket_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('ticket_id') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('image'), 'has-success': fields.image && fields.image.valid }">
                        <label for="image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.image') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($helpAndSupport->image)
                                @foreach(json_decode($helpAndSupport->image) as $images)
                                    <img src="{{$images}}" width="120" height="120">
                                @endforeach
                            @endif
                            <div v-if="errors.has('image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                        <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.created_at') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->created_at}}
                            <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                        <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.admin_reply') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->admin_reply}}
                            <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                        <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.query_status') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            
                            <span class="btn btn-success btn-sm" v-if="form.query_status == 1">Yes</span>
                            <span class="btn btn-danger btn-sm" v-if="form.query_status == 0">No</span>
                        </div>
                    </div>

                    </div>

                </div>

        </booking-detail-form>

        </div>
    
</div>


<!-- Booking details -->


<div class="container-xl">
        <div class="card">

            <booking-detail-form
                :data="{{ $helpAndSupport->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                    
                        <i class="fa fa-eye"></i> {{ trans('admin.booking-detail.bookingDetails') }}
                    </div>

                    <div class="card-body">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('id'), 'has-success': fields.id && fields.id.valid }">
                        <label for="id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{'Booking '}} {{ trans('admin.booking-detail.columns.id') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->booking->id}}
                            <div v-if="errors.has('id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('id') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('booking_code'), 'has-success': fields.booking_code && fields.booking_code.valid }">
                        <label for="booking_code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.booking_code') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->booking->booking_code}}
                            <div v-if="errors.has('booking_code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('booking_code') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('addressId'), 'has-success': fields.addressId && fields.addressId.valid }">
                        <label for="addressId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.addressId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->booking->address->formatted_address}}
                            <div v-if="errors.has('addressId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('addressId') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('bookingType'), 'has-success': fields.bookingType && fields.bookingType.valid }">
                        <label for="bookingType" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.bookingType') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($helpAndSupport->booking->bookingType==1){{'Delivery'}}@elseif($helpAndSupport->booking->bookingType==2){{'Pickup'}}@endif
                            <div v-if="errors.has('bookingType')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bookingType') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('bookingDateTime'), 'has-success': fields.bookingDateTime && fields.bookingDateTime.valid }">
                        <label for="bookingDateTime" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.bookingDateTime') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->booking->bookingDateTime}}
                            <div v-if="errors.has('bookingDateTime')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bookingDateTime') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
                        <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.status') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($helpAndSupport->booking->status==1){{'Pending'}}@elseif($helpAndSupport->booking->status==2){{'Accepted'}}
                            @elseif($helpAndSupport->booking->status==3){{'Rejected'}}@elseif($helpAndSupport->booking->status==4){{'Order in progress'}}@elseif($helpAndSupport->booking->status==5){{'Delivery boy assigned'}}@elseif($helpAndSupport->booking->status==6){{'Delivery boy started the journey'}}@elseif($helpAndSupport->booking->status==7){{'Delivery boy reached to vendor'}}@elseif($helpAndSupport->booking->status==8){{'Delivery boy picked order'}}@elseif($helpAndSupport->booking->status==9){{'Delivery boy reached to customer'}}@elseif($helpAndSupport->booking->status==10){{'Delivered product'}}@elseif($helpAndSupport->booking->status==11){{'Cancelled by customer'}}@elseif($helpAndSupport->booking->status==12){{'Cancelled by delivery boy'}}@elseif($helpAndSupport->booking->status==13){{'Other'}}@endif
                            <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                        <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.created_at') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->booking->created_at}}
                            <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                        </div>
                    </div>

                    </div>

                </div>

        </booking-detail-form>

        </div>
    
</div>


<!-- Shop details -->

<div class="container-xl">
        <div class="card">

            <booking-detail-form
                :data="{{ $helpAndSupport->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                    
                        <i class="fa fa-eye"></i> {{ trans('admin.admin-user.actions.shopDetails') }}
                    </div>

                    <div class="card-body">

                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_name'), 'has-success': fields.shop_name && fields.shop_name.valid }">
                            <label for="shop_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.shop_name') }}</label>
                                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                {{$helpAndSupport->shop->shop_name}}
                                <div v-if="errors.has('shop_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shop_name') }}</div>
                            </div>
                        </div>

                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_address'), 'has-success': fields.shop_address && fields.shop_address.valid }">
                            <label for="shop_address" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.shop_address') }}</label>
                                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                {{$helpAndSupport->shop->shop_address}}
                                <div v-if="errors.has('shop_address')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shop_address') }}</div>
                            </div>
                        </div>

                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_type'), 'has-success': fields.shop_type && fields.shop_type.valid }">
                            <label for="shop_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.shop_type') }}</label>
                                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                               @if($helpAndSupport->shop->shop_type==2) {{'Delivery'}}
                               @elseif($helpAndSupport->shop->shop_type==1) {{'Pickup'}}
                               @else{{'Both'}}@endif
                                <div v-if="errors.has('shop_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shop_type') }}</div>
                            </div>
                        </div>

                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('description'), 'has-success': fields.description && fields.description.valid }">
                            <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.description') }}</label>
                                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                {{$helpAndSupport->shop->description}}
                                <div v-if="errors.has('description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('description') }}</div>
                            </div>
                        </div>

                    </div>

                </div>

        </booking-detail-form>

        </div>
    
</div>


<!-- User Details -->

<div class="container-xl">
        <div class="card">

            <booking-detail-form
                :data="{{ $helpAndSupport->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                    
                        <i class="fa fa-eye"></i> {{ trans('admin.customer.userdetails') }}
                    </div>

                    <div class="card-body">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('user_id'), 'has-success': fields.user_id && fields.user_id.valid }">
                        <label for="user_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.user_id') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->user->name}}
                            <div v-if="errors.has('user_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('user_id') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('email'), 'has-success': fields.email && fields.email.valid }">
                        <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.email') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->user->email}}
                            <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('phone_number'), 'has-success': fields.phone_number && fields.phone_number.valid }">
                        <label for="phone_number" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.customer.columns.phone_number') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$helpAndSupport->user->country_code.$helpAndSupport->user->phone_number}}
                            <div v-if="errors.has('phone_number')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('phone_number') }}</div>
                        </div>
                    </div>

                    <div class="card-footer">
                <a class="btn btn-primary" href="{{ url('admin/help-and-supports') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Back</a>
                </div>

                    </div>

                </div>

        </booking-detail-form>

        </div>
    
</div>

@endsection

@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>


    <script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
@endsection
