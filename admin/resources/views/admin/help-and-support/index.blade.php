@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.help-and-support.actions.index'))

@section('body')

    <help-and-support-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/help-and-supports') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.help-and-support.actions.index') }}
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>

                                        <th is='sortable' :column="'id'">{{ trans('admin.help-and-support.columns.id') }}</th>
                                        <th is='sortable' :column="'booking_code'">{{ trans('admin.booking-detail.columns.booking_code') }}</th>
                                        <th is='sortable' :column="'user_id'">{{ trans('admin.help-and-support.columns.user_id') }}</th>
                                        <th is='sortable' :column="'shop_id'">{{ trans('admin.help-and-support.columns.shop_id') }}</th>
                                        <th is='sortable' :column="'feedback_title'">{{ trans('admin.help-and-support.columns.feedback_title') }}</th>
                                        <th is='sortable' :column="'query_status'">{{ trans('admin.help-and-support.columns.query_status') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">

                                        <td>@{{ item.id }}</td>
                                        <td v-if="item.booking">@{{ item.booking.booking_code }}</td>
                                        <td v-if="item.user">@{{ item.user.name }}</td>
                                        <td v-if="item.shop">@{{ item.shop.shop_name }}</td>
                                        <td v-if="item.feedback_title">@{{ item.feedback_title }}</td>
                                        <td v-else>{{ '' }}</td>

                                        <td>
                                            <a :for="item.id" status="0" onclick="mychangestatus(this)" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-sm" v-if="item.query_status==1">Yes</a>
                                            <a :for="item.id" status="1" onclick="mychangestatus(this)" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger btn-sm" v-else>No</a>
                                        </td>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/show'" title="{{ 'Show' }}" role="button"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                 

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </help-and-support-listing>

    <!-- Modal -->
    <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Change Status</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm()">

                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>

                    <form method="get" name="reason_form" id="form" onsubmit="return validateReasonForm1()"
                        action="{{ url('admin/help-and-supports/change-status') }}">
                        <div class="modal-iiner-content">
                            <input type="hidden" name="hsid" id="hsId">
                            <input type="hidden" name="query_status" id="query_status">
                            {{ csrf_field() }}
                            </span>Enter message for this user</span>
                            <textarea name="myMessage" id="myMessage"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="resetForm()">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Change Status" id="change-status">
                        </div>
                    </form>
                </div>
                </div>
                
            </div>
        </div>
    </div>

@endsection

@section('bottom-scripts')
<script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
@endsection