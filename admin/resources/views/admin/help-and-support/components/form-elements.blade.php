<div class="form-group row align-items-center" :class="{'has-danger': errors.has('booking_id'), 'has-success': fields.booking_id && fields.booking_id.valid }">
    <label for="booking_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.booking_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.booking_id" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('booking_id'), 'form-control-success': fields.booking_id && fields.booking_id.valid}" id="booking_id" name="booking_id" placeholder="{{ trans('admin.help-and-support.columns.booking_id') }}">
        <div v-if="errors.has('booking_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('booking_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('user_id'), 'has-success': fields.user_id && fields.user_id.valid }">
    <label for="user_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.user_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.user_id" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('user_id'), 'form-control-success': fields.user_id && fields.user_id.valid}" id="user_id" name="user_id" placeholder="{{ trans('admin.help-and-support.columns.user_id') }}">
        <div v-if="errors.has('user_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('user_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_id'), 'has-success': fields.shop_id && fields.shop_id.valid }">
    <label for="shop_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.shop_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.shop_id" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('shop_id'), 'form-control-success': fields.shop_id && fields.shop_id.valid}" id="shop_id" name="shop_id" placeholder="{{ trans('admin.help-and-support.columns.shop_id') }}">
        <div v-if="errors.has('shop_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shop_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('feedback_title'), 'has-success': fields.feedback_title && fields.feedback_title.valid }">
    <label for="feedback_title" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.feedback_title') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.feedback_title" v-validate="'required'" id="feedback_title" name="feedback_title"></textarea>
        </div>
        <div v-if="errors.has('feedback_title')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('feedback_title') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('feedback_description'), 'has-success': fields.feedback_description && fields.feedback_description.valid }">
    <label for="feedback_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-and-support.columns.feedback_description') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.feedback_description" v-validate="'required'" id="feedback_description" name="feedback_description"></textarea>
        </div>
        <div v-if="errors.has('feedback_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('feedback_description') }}</div>
    </div>
</div>


