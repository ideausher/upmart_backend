<?php

use Akaunting\Money\Money;

use Illuminate\Support\Facades\Auth;

$user = Auth::user();

if($user->hasRole('vendor'))
{
    $role = 'vendor';
}
else
{
    $role = 'admin';
}

?>

@extends('brackets/admin-ui::admin.layout.default')

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com//ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('title', trans('admin.booking-detail.actions.index'))

@section('body')

<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ trans('admin.booking-detail.actions.index') }}</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <table id="tableId" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                    <th>{{ trans('admin.booking-detail.columns.id') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.userId') }}</th>
                    @if($role=='admin')
                    <th>{{ trans('admin.booking-detail.columns.shopId') }}</th>
                    <!-- <th is='sortable' :column="'vendor'">{{ trans('admin.booking-detail.columns.vendor') }}</th> -->
                    @endif
                    <th>{{ trans('admin.booking-detail.columns.booking_code') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.deliveryBoyId') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.bookingType') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.bookingDateTime') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.delivery_charge_to_delivery_boy') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.delivery_charge_for_customer') }}</th>
                    <th>{{ trans('admin.booking-detail.columns.status') }}</th>
                    <th>{{ 'Actions' }}</th>
                    </tr>
                  </thead>
                  <tbody>

                  @foreach($data as $item)
                    <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['users']['name']}}</td>
                    @if($role=='admin')
                      <td>{{$item['shop']['shop_name']}}</td>
                    @endif
                    <td>{{$item['booking_code']}}</td>
                    <td>@if(!empty($item['deliveryBoyId'])){{$item['deliveryboy']['name']}}@else{{''}}@endif</td>
                    <td>{{$item['bookingType']}}</td>
                    <td>{{$item['bookingDateTime']}}</td>
                    <td>{{Money::CAD($item['delivery_charge_to_delivery_boy'], true)}}</td>
                    <td>{{Money::CAD($item['delivery_charge_for_customer'], true)}}</td>
                    <td>{{$item['status']}}</td>
                    <td><a class="btn btn-sm btn-spinner btn-info" href="{{url('/admin/'.$corr.'/'.$adminId.'/bookings/'.$item['id'].'/view')}}" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a></td>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(function () {
      $.noConflict();
        $('#tableId').DataTable({
          "ordering": true,
          columnDefs: [{
            orderable: false,
            targets: "no-sort"
          }],
          "order": [[ 0, "asc" ]]
        });
    });
</script>
