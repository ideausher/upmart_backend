@extends('brackets/admin-ui::admin.layout.default')

@section('title', 'Confirm OTP')

@section('body')

    <div class="container-xl">

        <div class="card">

            <shop-detail-form
                :action="'{{ url('admin/booking-details/'.$bookingid.'/verify-otp') }}'"
                v-cloak 
                inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action">
                
                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ 'Confirm OTP' }}
                    </div>

                    <div class="card-body">   
                        
                        <div class="row">
                            
                            <div class="col-md-8">
                                
							<div class="form-group row align-items-center" :class="{'has-danger': errors.has('otp'), 'has-success': fields.otp && fields.otp.valid }">
								<label for="otp" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.booking-detail.columns.otp') }}</label>
								<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
									<div>
										<textarea class="form-control" v-model="form.otp" v-validate="'required'" id="otp" name="otp" placeholder="{{ trans('admin.booking-detail.columns.otp') }}"></textarea>
									</div>
									<div v-if="errors.has('otp')" class="form-control-feedback form-text" v-cloak>OTP is required</div>
                                    @if(session('error'))
                                    <div class="form-control-feedback form-text" style="color:red;">{{session('error')}}</div>
                                    @endif
								</div>
							</div> 

						
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ 'Confirm' }}
                        </button>
                    </div>

                </form>

            </shop-detail-form>

        </div>

    </div>

@endsection