@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.booking-detail.actions.index'))

@section('body')

    <div class="container-xl">

                <div class="card">
            <div class="form-horizontal form-create">
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.booking-detail.actions.index') }}
                </div>
                
                  
                <div class="card-footer">
                <form method="get" action="{{ url('admin/booking-details/accept') }}">
                    {{ csrf_field() }}
                        <input type="hidden" name="booking_id" id="booking_id" value="{{$bookingid}}">
                        <input type="hidden" name="del_status" id="del_status" value="0">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i>
                            {{ 'Yes, Do manual' }}
                        </button>
                    </form>
                </div>
                
            </div>


        </div>

        </div>

    
@endsection
