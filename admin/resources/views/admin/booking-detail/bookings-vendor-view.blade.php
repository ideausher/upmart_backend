<?php

use Illuminate\Support\Facades\Auth;

use Akaunting\Money\Money;

$user = Auth::user();

if($user->hasRole('vendor'))
{
    $role = 'vendor';
}
else
{
    $role = 'admin';
}

?>


@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.booking-detail.bookingDetails'))

@section('body')

    <div class="container-xl">
        <div class="card">
            
        <booking-detail-form
                :data="{{ $booking->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" novalidate>


                    <div class="card-header">
                    
                        <i class="fa fa-eye"></i> {{ trans('admin.booking-detail.bookingDetails') }}
                    </div>

                    <div class="card-body">

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('booking_code'), 'has-success': fields.booking_code && fields.booking_code.valid }">
                        <label for="booking_code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.booking_code') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @{{form.booking_code}}
                            <div v-if="errors.has('booking_code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('booking_code') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('userId'), 'has-success': fields.userId && fields.userId.valid }">
                        <label for="userId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.userId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->users->name)){{$booking->users->name}}@endif
                            <div v-if="errors.has('userId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('userId') }}</div>
                        </div>
                    </div>

                    @if($role=='admin')
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shopId'), 'has-success': fields.shopId && fields.shopId.valid }">
                        <label for="shopId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.shopId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->shop->shop_name)){{$booking->shop->shop_name}}@endif
                            <div v-if="errors.has('shopId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shopId') }}</div>
                        </div>
                    </div>
                    @endif

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('addressId'), 'has-success': fields.addressId && fields.addressId.valid }">
                        <label for="addressId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.addressId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->address->formatted_address)){{$booking->address->formatted_address}}@endif
                            <div v-if="errors.has('addressId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('addressId') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('couponId'), 'has-success': fields.couponId && fields.couponId.valid }">
                        <label for="couponId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.couponId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->coupon->coupon_code)){{$booking->coupon->coupon_code}}@else{{'N/A'}}@endif
                            <div v-if="errors.has('couponId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('couponId') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('couponBy'), 'has-success': fields.couponBy && fields.couponBy.valid }">
                        <label for="couponBy" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.couponBy') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($booking->coupon)
                            @if($booking->coupon->shopId==null){{'Admin'}}@else{{'Vendor'}}@endif
                            @else{{'N/A'}}
                            @endif
                            <div v-if="errors.has('couponBy')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('couponBy') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('deliveryBoyId'), 'has-success': fields.deliveryBoyId && fields.deliveryBoyId.valid }">
                        <label for="deliveryBoyId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.deliveryBoyId') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->deliveryboy->name)){{$booking->deliveryboy->name}}@else{{'N/A'}}@endif
                            <div v-if="errors.has('deliveryBoyId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('deliveryBoyId') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('bookingType'), 'has-success': fields.bookingType && fields.bookingType.valid }">
                        <label for="bookingType" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.bookingType') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($booking->bookingType==2){{'Delivery'}}@elseif($booking->bookingType==1){{'Pickup'}}@endif
                            <div v-if="errors.has('bookingType')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bookingType') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('bookingDateTime'), 'has-success': fields.bookingDateTime && fields.bookingDateTime.valid }">
                        <label for="bookingDateTime" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.bookingDateTime') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->bookingDateTime)){{$booking->bookingDateTime}}@else{{'N/A'}}@endif
                            <div v-if="errors.has('bookingDateTime')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bookingDateTime') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('couponType'), 'has-success': fields.couponType && fields.couponType.valid }">
                        <label for="couponType" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.couponType') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($booking->coupon)
                            @if($booking->coupon->coupon_type==1){{'Fixed'}}@else($booking->coupon->coupon_type==0){{'Percentage'}}@endif
                            @else{{'N/A'}}
                            @endif
                            <div v-if="errors.has('couponType')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('couponType') }}</div>
                        </div>
                    </div>
                    
                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('instruction'), 'has-success': fields.instruction && fields.instruction.valid }">
                        <label for="instruction" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.instruction') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if(!empty($booking->instruction)){{$booking->instruction}}@else{{'N/A'}}@endif
                            <div v-if="errors.has('instruction')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('instruction') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
                        <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.status') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @if($booking->status==1){{'Pending'}}@elseif($booking->status==2){{'Accepted'}}
                            @elseif($booking->status==3){{'Rejected'}}@elseif($booking->status==4){{'Order in progress'}}@elseif($booking->status==5){{'Delivery boy assigned'}}@elseif($booking->status==6){{'Delivery boy started the journey'}}@elseif($booking->status==7){{'Delivery boy reached to vendor'}}@elseif($booking->status==8){{'Delivery boy picked order'}}@elseif($booking->status==9){{'Delivery boy reached to customer'}}@elseif($booking->status==10){{'Delivered product'}}@elseif($booking->status==11){{'Cancelled by customer'}}@elseif($booking->status==12){{'Cancelled by delivery boy'}}@elseif($booking->status==13){{'Other'}}@elseif($booking->status==14){{'Auto cancel'}}@endif
                            <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
                        </div>
                    </div>

                    @if($booking->rejectedbookings)

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
                        <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.booking_reject_reason') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$booking->rejectedbookings->reason}}
                            <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
                        </div>
                    </div>

                    @endif

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                        <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.created_at') }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$booking->created_at}}
                            <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                        </div>
                    </div>
                    @if($role=='admin' && !empty($booking->review))
                    
                        @foreach($booking->review as $rev)

                            @if(!empty($rev->shop_id))

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                                    <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.review_for_shop') }}</label>
                                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                        {{$rev->feedback}}
                                        <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                                    </div>
                                </div>

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                                    <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.rating_for_shop') }}</label>
                                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                        @php
                                        for( $x = 0; $x < 5; $x++ )
                                        {
                                            if( floor( $rev->rating )-$x >= 1 )
                                            { echo '<i class="fa fa-star"></i>'; }
                                            elseif( $rev->rating-$x > 0 )
                                            { echo '<i class="fa fa-star-half-o"></i>'; }
                                            else
                                            { echo '<i class="fa fa-star-o"></i>'; }
                                        }
                                        @endphp
                                        <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                                    </div>
                                </div>

                            @endif

                            @if(!empty($rev->delivery_boy_id))

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                                    <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.review_for_delivery_boy') }}</label>
                                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                        {{$rev->feedback}}
                                        <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                                    </div>
                                </div>

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('created_at'), 'has-success': fields.created_at && fields.created_at.valid }">
                                    <label for="created_at" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.booking-detail.columns.rating_for_delivery_boy') }}</label>
                                        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                                        @php
                                        for( $x = 0; $x < 5; $x++ )
                                        {
                                            if( floor( $rev->rating )-$x >= 1 )
                                            { echo '<i class="fa fa-star"></i>'; }
                                            elseif( $rev->rating-$x > 0 )
                                            { echo '<i class="fa fa-star-half-o"></i>'; }
                                            else
                                            { echo '<i class="fa fa-star-o"></i>'; }
                                        }
                                        @endphp
                                        <div v-if="errors.has('created_at')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('created_at') }}</div>
                                    </div>
                                </div>

                            @endif

                        @endforeach

                    @endif
                    

                    </div>

                    <div class="amount-section">
                        <div class="balance-parent-div">
                   
                    @if($booking->status!=3&&$booking->status!=11&&$booking->status!=12&&$booking->status!=14)

                    @if($role=='vendor')
                    
                            <label class="balance-heading">Balance</label>
                            <div class="form-group row align-items-center">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'"></label>
                                <label for="after_charge_amount" class="col-form-label col-md-3 balance-amount">Amount</label>
                            </div>

                    @endif

                    @endif

                    @if($role=='admin')
                    @if($booking->bookingType==2)
                    <label class="balance-heading">Flat Charges To Delivery Boy</label>
                            <div class="form-group row align-items-center">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'"></label>
                                <label for="after_charge_amount" class="col-form-label col-md-3 balance-amount">Amount</label>
                            </div>
                            
                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('after_charge_amount'), 'has-success': fields.after_charge_amount && fields.after_charge_amount.valid }">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ 'On Order Pickup' }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-3 col-xl-3'">
                                     @{{form.amount_paid_to_delivery_boy_on_order_pickup | toCurrency}}
                                    <div v-if="errors.has('after_charge_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('after_charge_amount') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('after_charge_amount'), 'has-success': fields.after_charge_amount && fields.after_charge_amount.valid }">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ 'On Order Delivered' }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-3 col-xl-3'">
                                     @{{form.amount_paid_to_delivery_boy_on_order_delivered | toCurrency}}
                                    <div v-if="errors.has('after_charge_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('after_charge_amount') }}</div>
                                </div>
                            </div>
                                    @endif

                            <label class="balance-heading">Balance</label>
                            <div class="form-group row align-items-center">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'"></label>
                                <label for="after_charge_amount" class="col-form-label col-md-3 balance-amount">Amount</label>
                            </div>
                            
                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('after_charge_amount'), 'has-success': fields.after_charge_amount && fields.after_charge_amount.valid }">
                                <label for="after_charge_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.cart_value') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-3 col-xl-3'">
                                    + @{{form.amount_before_discount | toCurrency}}
                                    <div v-if="errors.has('after_charge_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('after_charge_amount') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('platform_charge'), 'has-success': fields.platform_charge && fields.platform_charge.valid }">
                                <label for="platform_charge" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.platform_charge') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.platform_charge | toCurrency}}
                                    <div v-if="errors.has('platform_charge')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('platform_charge') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('tip_to_delivery_boy'), 'has-success': fields.tip_to_delivery_boy && fields.tip_to_delivery_boy.valid }">
                                <label for="tip_to_delivery_boy" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.tip_to_delivery_boy') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.tip_to_delivery_boy | toCurrency}}
                                    <div v-if="errors.has('tip_to_delivery_boy')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('tip_to_delivery_boy') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('delivery_charge_for_customer'), 'has-success': fields.delivery_charge_for_customer && fields.delivery_charge_for_customer.valid }">
                                <label for="delivery_charge_for_customer" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.delivery_charge_for_customer') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.delivery_charge_for_customer | toCurrency}}
                                    <div v-if="errors.has('delivery_charge_for_customer')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('delivery_charge_for_customer') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('discount_amount'), 'has-success': fields.discount_amount && fields.discount_amount.valid }">
                                <label for="discount_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.discount_amount') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    - @{{form.discount_amount | toCurrency}}
                                    <div v-if="errors.has('discount_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('discount_amount') }}</div>
                                </div>
                            </div>
                        
                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('gst'), 'has-success': fields.gst && fields.gst.valid }">
                                <label for="gst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.gst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + {{Money::CAD($booking->totalGST, true)}}
                                    <div v-if="errors.has('gst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('gst') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('pst'), 'has-success': fields.pst && fields.pst.valid }">
                                <label for="pst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.pst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.pst | toCurrency}}
                                    <div v-if="errors.has('pst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pst') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('hst'), 'has-success': fields.hst && fields.hst.valid }">
                                <label for="hst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.hst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.hst | toCurrency}}
                                    <div v-if="errors.has('hst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('hst') }}</div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="balance-parent-div">
                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('total'), 'has-success': fields.total && fields.total.valid }">
                                <label for="total" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.total') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    {{Money::CAD($booking->totalamount,true)}}
                                    <div v-if="errors.has('total')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total') }}</div>
                                </div>
                            </div>

                            <!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('stripe_charges'), 'has-success': fields.stripe_charges && fields.stripe_charges.valid }">
                                <label for="stripe_charges" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.stripe_charges') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    - {{$booking->stripe_charges}}
                                    <div v-if="errors.has('stripe_charges')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('stripe_charges') }}</div>
                                </div>
                            </div> -->

                            <!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('as_total_amount_to_admin'), 'has-success': fields.as_total_amount_to_admin && fields.as_total_amount_to_admin.valid }">
                                <label for="as_total_amount_to_admin" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.as_total_amount_to_admin') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + {{$booking->amount_after_stripe_charges}} (inc. stripe charges)
                                    <div v-if="errors.has('as_total_amount_to_admin')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('as_total_amount_to_admin') }}</div>
                                </div>
                            </div> -->
                        </div>


                        @endif


                        @if($booking->status!=3&&$booking->status!=11&&$booking->status!=12&&$booking->status!=14)
                         @if($role=='admin')
                        <hr>
                        <div class="balance-parent-div">
                                <label class="balance-heading"> After Settlement </label>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('received_by_vendor'), 'has-success': fields.received_by_vendor && fields.received_by_vendor.valid }">
                                <label for="received_by_vendor" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.received_by_vendor') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.bs_amount_to_vendor_after_commi | toCurrency}} (inc. all taxes)
                                    <div v-if="errors.has('received_by_vendor')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('received_by_vendor') }}</div>
                                </div>
                            </div>
                            
                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('received_by_delivery_boy'), 'has-success': fields.received_by_delivery_boy && fields.received_by_delivery_boy.valid }">
                                <div class="col-md-9 text-md-right balance-double">
                                    <label for="received_by_delivery_boy" class="col-form-label" >{{ trans('admin.booking-detail.columns.received_by_delivery_boy') }}</label>
                                    +
                                    <label for="tip_to_delivery_boy" class="col-form-label tip-boy">{{ trans('admin.booking-detail.columns.tip_to_delivery_boy') }}</label>
                                    +
                                    <label for="tip_to_delivery_boy" class="col-form-label tip-boy">{{ trans('On Order Pickup') }}</label>
                                    +
                                    <label for="tip_to_delivery_boy" class="col-form-label tip-boy">{{ trans('On Order Delivered') }}</label>
                                </div>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-8 col-xl-3'">
                                    +
                                    @{{total([form.delivery_charge_to_delivery_boy,form.tip_to_delivery_boy,form.amount_paid_to_delivery_boy_on_order_pickup,form.amount_paid_to_delivery_boy_on_order_delivered])}}
                                    <div v-if="errors.has('received_by_delivery_boy')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('received_by_delivery_boy') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('admin_balance'), 'has-success': fields.admin_balance && fields.admin_balance.valid }">
                                <label for="admin_balance" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.admin_balance') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    @{{total([form.as_total_amount_to_admin,form.stripe_charges])}} (inc. stripe charges)
                                    <div v-if="errors.has('admin_balance')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('admin_balance') }}</div>
                                </div>
                            </div>

                            
                            @else
                            
                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('gst'), 'has-success': fields.gst && fields.gst.valid }">
                                <label for="gst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.gst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + {{Money::CAD($booking->totalGST, true)}}
                                    <div v-if="errors.has('gst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('gst') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('pst'), 'has-success': fields.pst && fields.pst.valid }">
                                <label for="pst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.pst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.pst | toCurrency}}
                                    <div v-if="errors.has('pst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('pst') }}</div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('hst'), 'has-success': fields.hst && fields.hst.valid }">
                                <label for="hst" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.hst') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    + @{{form.hst | toCurrency}}
                                    <div v-if="errors.has('hst')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('hst') }}</div>
                                </div>
                            </div>
                   

                            <div class="form-group row align-items-center" :class="{'has-danger': errors.has('received_by_vendor'), 'has-success': fields.received_by_vendor && fields.received_by_vendor.valid }">
                                <label for="received_by_vendor" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-9'">{{ trans('admin.booking-detail.columns.earned_by_me') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-3'">
                                    @{{form.bs_amount_to_vendor_after_commi}} (inc. stripe charges and above mentioned taxes)
                                    <div v-if="errors.has('received_by_vendor')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('received_by_vendor') }}</div>
                                </div>
                            </div>

                            @endif
                            </div>
                        @endif
                        
                        

                        </form>
                        </booking-detail-form>

        </div>
    
</div>


<!-- Modal -->

<div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to reject this booking?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>

                    <form method="get" name="reason_form"  id="form" onsubmit="return validateReasonForm()"
                        action="{{ url('admin/booking-details/reject') }}">
                        <div class="modal-iiner-content">
                            <input type="hidden" name="bookingId" id="bookingId">
                            {{ csrf_field() }}
                            </span>Please Enter Reason For Reject</span>
                            <input type="text" name="reason" id="myMessage">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Reject" id="reject">
                        </div>
                    </form>
                </div>
                </div>
                

            </div>
        </div>
    </div>

    <booking-detail-listing
        :url="'{{ url('admin/booking-details') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.booking-detail.columns.orderProducts') }}
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                           

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>{{ trans('admin.item.columns.id') }}</th>
                                        <th>{{ trans('admin.item.columns.product_name') }}</th>
                                        <th>{{ trans('admin.item.columns.product_description') }}</th>
                                        <th>{{ trans('admin.item.columns.total_quantity') }}</th>
                                        <th>{{'Product '}}{{ trans('admin.item.columns.price') }}</th>
                                        <th>{{ trans('admin.item.columns.gst') }}</th>
                                        <th>{{ trans('admin.item.columns.after_gst_price') }}</th>
                                    </tr>
                                 
                                </thead>
                                <tbody>
                                @foreach($booking->orderdetails as $od)
                                    <tr>
                                        <td>{{$od->productId}}</td>
                                        <td>{{$od->productName}}</td>
                                        <td>{{$od->productDescription}}</td>
                                        <td>{{$od->quantity}}</td>
                                        <td>{{Money::CAD($od->new_price, true)}}</td>
                                        <td>{{Money::CAD($od->gst_tax, true)}}</td>
                                        <td>{{Money::CAD($od->total_price, true)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>


                        </div>
                        @if($booking->status==1&&$role=='vendor')
                    <div class="card-footer" style="display: inline-flex;">
                    
                    <form method="get" action="{{ url('admin/booking-details/accept') }}">
                    {{ csrf_field() }}
                        <input type="hidden" name="booking_id" id="booking_id">
                        <button type="submit" for="{{$booking->id}}" onclick="myaccept(this)" class="btn btn-success">
                            <i class="fa fa-check"></i>
                            {{ 'Accept' }}
                        </button>
                    </form>
                
                    <a onclick="myreject(this)" class="btn btn-danger" for="{{$booking->id}}" data-toggle="modal" data-target="#exampleModal" style="margin-left: 10px;">
                            <i class="fa fa-close"></i>
                            {{ 'Reject' }}
                        </a>
                </div>
                    @endif
                    <a class="btn btn-primary" href="{{ url('admin/'.$parent.'/'.$userid.'/bookings/') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Back</a>
                </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </booking-detail-listing>

@endsection

@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>


    <script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
@endsection
