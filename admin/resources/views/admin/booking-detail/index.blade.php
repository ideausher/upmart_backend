<?php

use Illuminate\Support\Facades\Auth;

$user = Auth::user();

if($user->hasRole('vendor'))
{
    $role = 'vendor';
}
else
{
    $role = 'admin';
}

?>

@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.booking-detail.actions.index'))

@section('body')

    <booking-detail-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/booking-details') }}'"
        inline-template>
        
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.booking-detail.actions.index') }}
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.booking-detail.columns.id') }}</th>
                                        <th is='sortable' :column="'userId'">{{ trans('admin.booking-detail.columns.userId') }}</th>
                                        @if($role=='admin')
                                        <th is='sortable' :column="'shopId'">{{ trans('admin.booking-detail.columns.shopId') }}</th>
                                        <!-- <th is='sortable' :column="'vendor'">{{ trans('admin.booking-detail.columns.vendor') }}</th> -->
                                        @endif
                                        <th is='sortable' :column="'booking_code'">{{ trans('admin.booking-detail.columns.booking_code') }}</th>
                                        <th is='sortable' :column="'deliveryBoyId'">{{ trans('admin.booking-detail.columns.deliveryBoyId') }}</th>
                                        <th is='sortable' :column="'bookingType'">{{ trans('admin.booking-detail.columns.bookingType') }}</th>
                                        <th is='sortable' :column="'bookingDateTime'">{{ trans('admin.booking-detail.columns.bookingDateTime') }}</th>
                                        @if($role=='admin')
                                        <th is='sortable' :column="'delivery_charge_to_delivery_boy'">{{ trans('admin.booking-detail.columns.delivery_charge_to_delivery_boy') }}</th>
                                        <th is='sortable' :column="'delivery_charge_for_customer'">{{ trans('admin.booking-detail.columns.delivery_charge_for_customer') }}</th>
                                        @endif
                                        <th is='sortable' :column="'status'">{{ trans('admin.booking-detail.columns.status') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                    
                                        <td>@{{item.id}}</td>
                                        <td v-if="item.users">@{{item.users.name}}</td>
                                        @if($role=='admin')
                                        <td>@{{item.shop.shop_name}}</td>
                                        @endif
                                        <td>@{{item.booking_code}}</td>
                                        <td v-if="item.deliveryBoyId">@{{item.deliveryboy.name}}</td>
                                        <td v-if="!item.deliveryBoyId">{{''}}</td>
                                        <td>@{{item.bookingType}}</td>
                                        <td>@{{item.bookingDateTime}}</td>
                                        @if($role=='admin')
                                        <td>@{{item.delivery_charge_to_delivery_boy | toCurrency}}</td>
                                        <td>@{{item.delivery_charge_for_customer | toCurrency}}</td>
                                        @endif
                                        <td>@{{item.status}}</td>
                                        <!-- <td v-if="item.status==1">{{'Pending'}}</td>
                                        <td v-else-if="item.status==2">{{'Accepted'}}</td>
                                        <td v-else-if="item.status==3">{{'Rejected'}}</td>
                                        <td v-else-if="item.status==4">{{'Order in progress'}}</td>
                                        <td v-else-if="item.status==5">{{'Delivery boy assigned'}}</td>
                                        <td v-else-if="item.status==6">{{'Delivery boy started the journey'}}</td>
                                        <td v-else-if="item.status==7">{{'Delivery boy reached to vendor'}}</td>
                                        <td v-else-if="item.status==8">{{'Delivery boy picked order'}}</td>
                                        <td v-else-if="item.status==9">{{'Delivery boy reached to customer'}}</td>
                                        <td v-else-if="item.status==10">{{'Delivered product'}}</td>
                                        <td v-else-if="item.status==11">{{'Cancelled by customer'}}</td>
                                        <td v-else-if="item.status==12">{{'Cancelled by delivery boy'}}</td>
                                        <td v-else-if="item.status==13">{{'Other'}}</td>
                                        <td v-else-if="item.status==14">{{'Auto cancel'}}</td> -->
                                    
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/view'" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a>
                                                </div>
                                                @if($setting==1)
                                                <form class="col" method="get" action="{{ url('admin/booking-details/accept') }}" v-if="item.status==2">
                                                <input type="hidden" name="booking_id" id="booking_id" :value="item.id">
                                                <input type="hidden" name="del_status" id="del_status" value="0">
                                                    <button type="submit" class="btn btn-sm btn-success" title="Manual Search For Delivery Boy"><i class="fa fa-search"></i></button>
                                                </form>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </booking-detail-listing>

@endsection