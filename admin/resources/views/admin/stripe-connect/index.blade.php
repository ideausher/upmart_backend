@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.stripe-connect.actions.index'))

@section('body')

    <div class="container-xl">

                <div class="card">
            <div class="form-horizontal form-create">
                
                <div class="card-header">
                    <i class="fa fa-plus"></i> {{ trans('admin.stripe-connect.actions.index') }}
                </div>

                @if(Session::has('message'))
                    <div class="alert alert-block alert-success">
                        <i class=" fa fa-check cool-green "></i>
                        {{ nl2br(Session::get('message')) }}
                    </div>
                @endif
                                
                <div class="card-footer">
                    @if(!empty($data))
                    <button type="button" class="btn btn-primary">
                        <i class="fa fa-check"></i>
                        {{ 'Stripe connected' }}
                    </button>
                    @else
                    <button type="button" class="btn btn-primary connect-stripe">
                        <i class="fa fa-circle"></i>
                        {{ 'Click to connect to stripe' }}
                    </button>
                    @endif
                </div>
                
            </div>


        </div>

        </div>

    
@endsection

@section('bottom-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



<script>

$(".connect-stripe").click(function(){
    
    var url = "https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id={{env('STRIPE_CLIENT_ID')}}";
    window.location.href = url;
});

</script>


@endsection