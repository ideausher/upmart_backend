<!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('delivery_boy_searching'), 'has-success': fields.delivery_boy_searching && fields.delivery_boy_searching.valid }">
    <label for="delivery_boy_searching" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-setting.columns.delivery_boy_searching') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select class="form-control" id="delivery_boy_searching" v-model="form.delivery_boy_searching" name="delivery_boy_searching" placeholder="Select the Company">
            <option value="" selected>Select option</option>
            <option value="0">Automatic</option>
            <option value="1">Manual</option>
        </select>
        <div v-if="errors.has('delivery_boy_searching')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('delivery_boy_searching') }}</div>
    </div>
</div> -->

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('delivery_boy_search_limit'), 'has-success': fields.delivery_boy_search_limit && fields.delivery_boy_search_limit.valid }">
    <label for="delivery_boy_search_limit" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-setting.columns.delivery_boy_search_limit') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.delivery_boy_search_limit" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('delivery_boy_search_limit'), 'form-control-success': fields.delivery_boy_search_limit && fields.delivery_boy_search_limit.valid}" id="delivery_boy_search_limit" name="delivery_boy_search_limit" placeholder="{{ trans('admin.admin-setting.columns.delivery_boy_search_limit') }}">
        <div v-if="errors.has('delivery_boy_search_limit')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('delivery_boy_search_limit') }}</div>
    </div>
</div>


