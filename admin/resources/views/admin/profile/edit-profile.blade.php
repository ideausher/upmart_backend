
<?php

use Illuminate\Support\Facades\Auth;

$user = Auth::user();

if($user->hasRole('vendor'))
{
    $role = 'vendor';
}
else
{
    $role = 'admin';
}

?>

@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.edit_profile'))

@section('body')

    <div class="container-xl">

        <div class="card">

            <profile-edit-profile-form
                :action="'{{ url('admin/profile') }}'"
                :data="{{ $adminUser->toJson() }}"

                inline-template>

                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action">


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.admin-user.actions.edit_profile') }}
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('first_name'), 'has-success': fields.first_name && fields.first_name.valid }">
                                    <label for="first_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.first_name') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                                        <input type="text" v-model="form.first_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('first_name'), 'form-control-success': fields.first_name && fields.first_name.valid}" id="first_name" name="first_name" placeholder="{{ trans('admin.admin-user.columns.first_name') }}">
                                        <div v-if="errors.has('first_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('first_name') }}</div>
                                    </div>
                                </div>

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('last_name'), 'has-success': fields.last_name && fields.last_name.valid }">
                                    <label for="last_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.last_name') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                                        <input type="text" v-model="form.last_name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('last_name'), 'form-control-success': fields.last_name && fields.last_name.valid}" id="last_name" name="last_name" placeholder="{{ trans('admin.admin-user.columns.last_name') }}">
                                        <div v-if="errors.has('last_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('last_name') }}</div>
                                    </div>
                                </div>

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('email'), 'has-success': fields.email && fields.email.valid }">
                                    <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.email') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                                        <input type="text" disabled v-model="form.email" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('email'), 'form-control-success': fields.email && fields.email.valid}" id="email" name="email" placeholder="{{ trans('admin.admin-user.columns.email') }}">
                                        <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}</div>
                                    </div>
                                </div>

                                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('country_iso_code'), 'has-success': fields.country_iso_code && fields.country_iso_code.valid }">
    <label for="country_iso_code" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.country_code') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
			<select v-model="form.country_iso_code" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('country_iso_code'), 'form-control-success': fields.country_iso_code && fields.country_iso_code.valid}" id="country_iso_code" name="country_iso_code" placeholder="{{ trans('admin.admin-user.columns.country_iso_code') }}">
				<option value="DZ" data-countryCode="+213">Algeria (+213)</option>
				<option value="AD" data-countryCode="+376">Andorra (+376)</option>
				<option value="AO" data-countryCode="+244">Angola (+244)</option>
				<option value="AI" data-countryCode="+1264">Anguilla (+1264)</option>
				<option value="AG" data-countryCode="+1268">Antigua &amp; Barbuda (+1268)</option>
				<option value="AR" data-countryCode="+54">Argentina (+54)</option>
				<option value="AM" data-countryCode="+374">Armenia (+374)</option>
				<option value="AW" data-countryCode="+297">Aruba (+297)</option>
				<option value="AU" data-countryCode="+61">Australia (+61)</option>
				<option value="AT" data-countryCode="+43">Austria (+43)</option>
				<option value="AZ" data-countryCode="+994">Azerbaijan (+994)</option>
				<option value="BS" data-countryCode="+1242">Bahamas (+1242)</option>
				<option value="BH" data-countryCode="+973">Bahrain (+973)</option>
				<option value="BD" data-countryCode="+880">Bangladesh (+880)</option>
				<option value="BB" data-countryCode="+1246">Barbados (+1246)</option>
				<option value="BY" data-countryCode="+375">Belarus (+375)</option>
				<option value="BE" data-countryCode="+32">Belgium (+32)</option>
				<option value="BZ" data-countryCode="+501">Belize (+501)</option>
				<option value="BJ" data-countryCode="+229">Benin (+229)</option>
				<option value="BM" data-countryCode="+1441">Bermuda (+1441)</option>
				<option value="BT" data-countryCode="+975">Bhutan (+975)</option>
				<option value="BO" data-countryCode="+591">Bolivia (+591)</option>
				<option value="BA" data-countryCode="+387">Bosnia Herzegovina (+387)</option>
				<option value="BW" data-countryCode="+267">Botswana (+267)</option>
				<option value="BR" data-countryCode="+55">Brazil (+55)</option>
				<option value="BN" data-countryCode="+673">Brunei (+673)</option>
				<option value="BG" data-countryCode="+359">Bulgaria (+359)</option>
				<option value="BF" data-countryCode="+226">Burkina Faso (+226)</option>
				<option value="BI" data-countryCode="+257">Burundi (+257)</option>
				<option value="KH" data-countryCode="+855">Cambodia (+855)</option>
				<option value="CM" data-countryCode="+237">Cameroon (+237)</option>
				<option value="CA" data-countryCode="+1">Canada (+1)</option>
				<option value="CV" data-countryCode="+238">Cape Verde Islands (+238)</option>
				<option value="KY" data-countryCode="+1345">Cayman Islands (+1345)</option>
				<option value="CF" data-countryCode="+236">Central African Republic (+236)</option>
				<option value="CL" data-countryCode="+56">Chile (+56)</option>
				<option value="CN" data-countryCode="+86">China (+86)</option>
				<option value="CO" data-countryCode="+57">Colombia (+57)</option>
				<option value="KM" data-countryCode="+269">Comoros (+269)</option>
				<option value="CG" data-countryCode="+242">Congo (+242)</option>
				<option value="CK" data-countryCode="+682">Cook Islands (+682)</option>
				<option value="CR" data-countryCode="+506">Costa Rica (+506)</option>
				<option value="HR" data-countryCode="+385">Croatia (+385)</option>
				<option value="CU" data-countryCode="+53">Cuba (+53)</option>
				<option value="CY" data-countryCode="+90392">Cyprus North (+90392)</option>
				<option value="CY" data-countryCode="+357">Cyprus South (+357)</option>
				<option value="CZ" data-countryCode="+42">Czech Republic (+42)</option>
				<option value="DK" data-countryCode="+45">Denmark (+45)</option>
				<option value="DJ" data-countryCode="+253">Djibouti (+253)</option>
				<option value="DM" data-countryCode="+1809">Dominica (+1809)</option>
				<option value="DO" data-countryCode="+1809">Dominican Republic (+1809)</option>
				<option value="EC" data-countryCode="+593">Ecuador (+593)</option>
				<option value="EG" data-countryCode="+20">Egypt (+20)</option>
				<option value="SV" data-countryCode="+503">El Salvador (+503)</option>
				<option value="GQ" data-countryCode="+240">Equatorial Guinea (+240)</option>
				<option value="ER" data-countryCode="+291">Eritrea (+291)</option>
				<option value="EE" data-countryCode="+372">Estonia (+372)</option>
				<option value="ET" data-countryCode="+251">Ethiopia (+251)</option>
				<option value="FK" data-countryCode="+500">Falkland Islands (+500)</option>
				<option value="FO" data-countryCode="+298">Faroe Islands (+298)</option>
				<option value="FJ" data-countryCode="+679">Fiji (+679)</option>
				<option value="FI" data-countryCode="+358">Finland (+358)</option>
				<option value="FR" data-countryCode="+33">France (+33)</option>
				<option value="GF" data-countryCode="+594">French Guiana (+594)</option>
				<option value="PF" data-countryCode="+689">French Polynesia (+689)</option>
				<option value="GA" data-countryCode="+241">Gabon (+241)</option>
				<option value="GM" data-countryCode="+220">Gambia (+220)</option>
				<option value="GE" data-countryCode="+7880">Georgia (+7880)</option>
				<option value="DE" data-countryCode="+49">Germany (+49)</option>
				<option value="GH" data-countryCode="+233">Ghana (+233)</option>
				<option value="GI" data-countryCode="+350">Gibraltar (+350)</option>
				<option value="GR" data-countryCode="+30">Greece (+30)</option>
				<option value="GL" data-countryCode="+299">Greenland (+299)</option>
				<option value="GD" data-countryCode="+1473">Grenada (+1473)</option>
				<option value="GP" data-countryCode="+590">Guadeloupe (+590)</option>
				<option value="GU" data-countryCode="+671">Guam (+671)</option>
				<option value="GT" data-countryCode="+502">Guatemala (+502)</option>
				<option value="GN" data-countryCode="+224">Guinea (+224)</option>
				<option value="GW" data-countryCode="+245">Guinea - Bissau (+245)</option>
				<option value="GY" data-countryCode="+592">Guyana (+592)</option>
				<option value="HT" data-countryCode="+509">Haiti (+509)</option>
				<option value="HN" data-countryCode="+504">Honduras (+504)</option>
				<option value="HK" data-countryCode="+852">Hong Kong (+852)</option>
				<option value="HU" data-countryCode="+36">Hungary (+36)</option>
				<option value="IS" data-countryCode="+354">Iceland (+354)</option>
				<option value="IN" data-countryCode="+91">India (+91)</option>
				<option value="ID" data-countryCode="+62">Indonesia (+62)</option>
				<option value="IR" data-countryCode="+98">Iran (+98)</option>
				<option value="IQ" data-countryCode="+964">Iraq (+964)</option>
				<option value="IE" data-countryCode="+353">Ireland (+353)</option>
				<option value="IL" data-countryCode="+972">Israel (+972)</option>
				<option value="IT" data-countryCode="+39">Italy (+39)</option>
				<option value="JM" data-countryCode="+1876">Jamaica (+1876)</option>
				<option value="JP" data-countryCode="+81">Japan (+81)</option>
				<option value="JO" data-countryCode="+962">Jordan (+962)</option>
				<option value="KZ" data-countryCode="+7">Kazakhstan (+7)</option>
				<option value="KE" data-countryCode="+254">Kenya (+254)</option>
				<option value="KI" data-countryCode="+686">Kiribati (+686)</option>
				<option value="KP" data-countryCode="+850">Korea North (+850)</option>
				<option value="KR" data-countryCode="+82">Korea South (+82)</option>
				<option value="KW" data-countryCode="+965">Kuwait (+965)</option>
				<option value="KG" data-countryCode="+996">Kyrgyzstan (+996)</option>
				<option value="LA" data-countryCode="+856">Laos (+856)</option>
				<option value="LV" data-countryCode="+371">Latvia (+371)</option>
				<option value="LB" data-countryCode="+961">Lebanon (+961)</option>
				<option value="LS" data-countryCode="+266">Lesotho (+266)</option>
				<option value="LR" data-countryCode="+231">Liberia (+231)</option>
				<option value="LY" data-countryCode="+218">Libya (+218)</option>
				<option value="LI" data-countryCode="+417">Liechtenstein (+417)</option>
				<option value="LT" data-countryCode="+370">Lithuania (+370)</option>
				<option value="LU" data-countryCode="+352">Luxembourg (+352)</option>
				<option value="MO" data-countryCode="+853">Macao (+853)</option>
				<option value="MK" data-countryCode="+389">Macedonia (+389)</option>
				<option value="MG" data-countryCode="+261">Madagascar (+261)</option>
				<option value="MW" data-countryCode="+265">Malawi (+265)</option>
				<option value="MY" data-countryCode="+60">Malaysia (+60)</option>
				<option value="MV" data-countryCode="+960">Maldives (+960)</option>
				<option value="ML" data-countryCode="+223">Mali (+223)</option>
				<option value="MT" data-countryCode="+356">Malta (+356)</option>
				<option value="MH" data-countryCode="+692">Marshall Islands (+692)</option>
				<option value="MQ" data-countryCode="+596">Martinique (+596)</option>
				<option value="MR" data-countryCode="+222">Mauritania (+222)</option>
				<option value="YT" data-countryCode="+269">Mayotte (+269)</option>
				<option value="MX" data-countryCode="+52">Mexico (+52)</option>
				<option value="FM" data-countryCode="+691">Micronesia (+691)</option>
				<option value="MD" data-countryCode="+373">Moldova (+373)</option>
				<option value="MC" data-countryCode="+377">Monaco (+377)</option>
				<option value="MN" data-countryCode="+976">Mongolia (+976)</option>
				<option value="MS" data-countryCode="+1664">Montserrat (+1664)</option>
				<option value="MA" data-countryCode="+212">Morocco (+212)</option>
				<option value="MZ" data-countryCode="+258">Mozambique (+258)</option>
				<option value="MN" data-countryCode="+95">Myanmar (+95)</option>
				<option value="NA" data-countryCode="+264">Namibia (+264)</option>
				<option value="NR" data-countryCode="+674">Nauru (+674)</option>
				<option value="NP" data-countryCode="+977">Nepal (+977)</option>
				<option value="NL" data-countryCode="+31">Netherlands (+31)</option>
				<option value="NC" data-countryCode="+687">New Caledonia (+687)</option>
				<option value="NZ" data-countryCode="+64">New Zealand (+64)</option>
				<option value="NI" data-countryCode="+505">Nicaragua (+505)</option>
				<option value="NE" data-countryCode="+227">Niger (+227)</option>
				<option value="NG" data-countryCode="+234">Nigeria (+234)</option>
				<option value="NU" data-countryCode="+683">Niue (+683)</option>
				<option value="NF" data-countryCode="+672">Norfolk Islands (+672)</option>
				<option value="NP" data-countryCode="+670">Northern Marianas (+670)</option>
				<option value="NO" data-countryCode="+47">Norway (+47)</option>
				<option value="OM" data-countryCode="+968">Oman (+968)</option>
				<option value="PW" data-countryCode="+680">Palau (+680)</option>
				<option value="PA" data-countryCode="+507">Panama (+507)</option>
				<option value="PG" data-countryCode="+675">Papua New Guinea (+675)</option>
				<option value="PY" data-countryCode="+595">Paraguay (+595)</option>
				<option value="PE" data-countryCode="+51">Peru (+51)</option>
				<option value="PH" data-countryCode="+63">Philippines (+63)</option>
				<option value="PL" data-countryCode="+48">Poland (+48)</option>
				<option value="PT" data-countryCode="+351">Portugal (+351)</option>
				<option value="PR" data-countryCode="+1787">Puerto Rico (+1787)</option>
				<option value="QA" data-countryCode="+974">Qatar (+974)</option>
				<option value="RE" data-countryCode="+262">Reunion (+262)</option>
				<option value="RO" data-countryCode="+40">Romania (+40)</option>
				<option value="RU" data-countryCode="+7">Russia (+7)</option>
				<option value="RW" data-countryCode="+250">Rwanda (+250)</option>
				<option value="SM" data-countryCode="+378">San Marino (+378)</option>
				<option value="ST" data-countryCode="+239">Sao Tome &amp; Principe (+239)</option>
				<option value="SA" data-countryCode="+966">Saudi Arabia (+966)</option>
				<option value="SN" data-countryCode="+221">Senegal (+221)</option>
				<option value="CS" data-countryCode="+381">Serbia (+381)</option>
				<option value="SC" data-countryCode="+248">Seychelles (+248)</option>
				<option value="SL" data-countryCode="+232">Sierra Leone (+232)</option>
				<option value="SG" data-countryCode="+65">Singapore (+65)</option>
				<option value="SK" data-countryCode="+421">Slovak Republic (+421)</option>
				<option value="SI" data-countryCode="+386">Slovenia (+386)</option>
				<option value="SB" data-countryCode="+677">Solomon Islands (+677)</option>
				<option value="SO" data-countryCode="+252">Somalia (+252)</option>
				<option value="ZA" data-countryCode="+27">South Africa (+27)</option>
				<option value="ES" data-countryCode="+34">Spain (+34)</option>
				<option value="LK" data-countryCode="+94">Sri Lanka (+94)</option>
				<option value="SH" data-countryCode="+290">St. Helena (+290)</option>
				<option value="KN" data-countryCode="+1869">St. Kitts (+1869)</option>
				<option value="SC" data-countryCode="+1758">St. Lucia (+1758)</option>
				<option value="SD" data-countryCode="+249">Sudan (+249)</option>
				<option value="SR" data-countryCode="+597">Suriname (+597)</option>
				<option value="SZ" data-countryCode="+268">Swaziland (+268)</option>
				<option value="SE" data-countryCode="+46">Sweden (+46)</option>
				<option value="CH" data-countryCode="+41">Switzerland (+41)</option>
				<option value="SI" data-countryCode="+963">Syria (+963)</option>
				<option value="TW" data-countryCode="+886">Taiwan (+886)</option>
				<option value="TJ" data-countryCode="+7">Tajikstan (+7)</option>
				<option value="TH" data-countryCode="+66">Thailand (+66)</option>
				<option value="TG" data-countryCode="+228">Togo (+228)</option>
				<option value="TO" data-countryCode="+676">Tonga (+676)</option>
				<option value="TT" data-countryCode="+1868">Trinidad &amp; Tobago (+1868)</option>
				<option value="TN" data-countryCode="+216">Tunisia (+216)</option>
				<option value="TR" data-countryCode="+90">Turkey (+90)</option>
				<option value="TM" data-countryCode="+7">Turkmenistan (+7)</option>
				<option value="TM" data-countryCode="+993">Turkmenistan (+993)</option>
				<option value="TC" data-countryCode="+1649">Turks &amp; Caicos Islands (+1649)</option>
				<option value="TV" data-countryCode="+688">Tuvalu (+688)</option>
				<option value="UG" data-countryCode="+256">Uganda (+256)</option>
				<!-- <option value="GB" data-countryCode="44">UK (+44)</option> -->
				<option value="UA" data-countryCode="+380">Ukraine (+380)</option>
				<option value="AE" data-countryCode="+971">United Arab Emirates (+971)</option>
				<option value="UY" data-countryCode="+598">Uruguay (+598)</option>
				<option value="US" data-countryCode="+1">USA (+1)</option>
				<option value="UZ" data-countryCode="+7">Uzbekistan (+7)</option>
				<option value="VU" data-countryCode="+678">Vanuatu (+678)</option>
				<option value="VA" data-countryCode="+379">Vatican City (+379)</option>
				<option value="VE" data-countryCode="+58">Venezuela (+58)</option>
				<option value="VN" data-countryCode="+84">Vietnam (+84)</option>
				<option value="VG" data-countryCode="+84">Virgin Islands - British (+1284)</option>
				<option value="VI" data-countryCode="+84">Virgin Islands - US (+1340)</option>
				<option value="WF" data-countryCode="+681">Wallis &amp; Futuna (+681)</option>
				<option value="YE" data-countryCode="+969">Yemen (North)(+969)</option>
				<option value="YE" data-countryCode="+967">Yemen (South)(+967)</option>
				<option value="ZM" data-countryCode="+260">Zambia (+260)</option>
				<option value="ZW" data-countryCode="+263">Zimbabwe (+263)</option>

			</select>
			<div v-if="errors.has('country_iso_code')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('country_iso_code') }}</div>
   		 </div>
	</div>

	<input type="hidden" id="DZ" value="+213">
<input type="hidden" id="AD" value="+376">
<input type="hidden" id="AO" value="+244">
<input type="hidden" id="AI" value="+1264">
<input type="hidden" id="AG" value="+1268">
<input type="hidden" id="AR" value="+54">
<input type="hidden" id="AM" value="+374">
<input type="hidden" id="AW" value="+297">
<input type="hidden" id="AU" value="+61">
<input type="hidden" id="AT" value="+43">
<input type="hidden" id="AZ" value="+994">
<input type="hidden" id="BS" value="+1242">
<input type="hidden" id="BH" value="+973">
<input type="hidden" id="BD" value="+880">
<input type="hidden" id="BB" value="+1246">
<input type="hidden" id="BY" value="+375">
<input type="hidden" id="BE" value="+32">
<input type="hidden" id="BZ" value="+501">
<input type="hidden" id="BJ" value="+229">
<input type="hidden" id="BM" value="+1441">
<input type="hidden" id="BT" value="+975">
<input type="hidden" id="BO" value="+591">
<input type="hidden" id="BA" value="+387">
<input type="hidden" id="BW" value="+267">
<input type="hidden" id="BR" value="+55">
<input type="hidden" id="BN" value="+673">
<input type="hidden" id="BG" value="+359">
<input type="hidden" id="BF" value="+226">
<input type="hidden" id="BI" value="+257">
<input type="hidden" id="KH" value="+855">
<input type="hidden" id="CM" value="+237">
<input type="hidden" id="CA" value="+1">
<input type="hidden" id="CV" value="+238">
<input type="hidden" id="KY" value="+1345">
<input type="hidden" id="CF" value="+236">
<input type="hidden" id="CL" value="+56">
<input type="hidden" id="CN" value="+86">
<input type="hidden" id="CO" value="+57">
<input type="hidden" id="KM" value="+269">
<input type="hidden" id="CG" value="+242">
<input type="hidden" id="CK" value="+682">
<input type="hidden" id="CR" value="+506">
<input type="hidden" id="HR" value="+385">
<input type="hidden" id="CU" value="+53">
<input type="hidden" id="CY" value="+90392">
<input type="hidden" id="CY" value="+357">
<input type="hidden" id="CZ" value="+42">
<input type="hidden" id="DK" value="+45">
<input type="hidden" id="DJ" value="+253">
<input type="hidden" id="DM" value="+1809">
<input type="hidden" id="DO" value="+1809">
<input type="hidden" id="EC" value="+593">
<input type="hidden" id="EG" value="+20">
<input type="hidden" id="SV" value="+503">
<input type="hidden" id="GQ" value="+240">
<input type="hidden" id="ER" value="+291">
<input type="hidden" id="EE" value="+372">
<input type="hidden" id="ET" value="+251">
<input type="hidden" id="FK" value="+500">
<input type="hidden" id="FO" value="+298">
<input type="hidden" id="FJ" value="+679">
<input type="hidden" id="FI" value="+358">
<input type="hidden" id="FR" value="+33">
<input type="hidden" id="GF" value="+594">
<input type="hidden" id="PF" value="+689">
<input type="hidden" id="GA" value="+241">
<input type="hidden" id="GM" value="+220">
<input type="hidden" id="GE" value="+7880">
<input type="hidden" id="DE" value="+49">
<input type="hidden" id="GH" value="+233">
<input type="hidden" id="GI" value="+350">
<input type="hidden" id="GR" value="+30">
<input type="hidden" id="GL" value="+299">
<input type="hidden" id="GD" value="+1473">
<input type="hidden" id="GP" value="+590">
<input type="hidden" id="GU" value="+671">
<input type="hidden" id="GT" value="+502">
<input type="hidden" id="GN" value="+224">
<input type="hidden" id="GW" value="+245">
<input type="hidden" id="GY" value="+592">
<input type="hidden" id="HT" value="+509">
<input type="hidden" id="HN" value="+504">
<input type="hidden" id="HK" value="+852">
<input type="hidden" id="HU" value="+36">
<input type="hidden" id="IS" value="+354">
<input type="hidden" id="IN" value="+91">
<input type="hidden" id="ID" value="+62">
<input type="hidden" id="IR" value="+98">
<input type="hidden" id="IQ" value="+964">
<input type="hidden" id="IE" value="+353">
<input type="hidden" id="IL" value="+972">
<input type="hidden" id="IT" value="+39">
<input type="hidden" id="JM" value="+1876">
<input type="hidden" id="JP" value="+81">
<input type="hidden" id="JO" value="+962">
<input type="hidden" id="KZ" value="+7">
<input type="hidden" id="KE" value="+254">
<input type="hidden" id="KI" value="+686">
<input type="hidden" id="KP" value="+850">
<input type="hidden" id="KR" value="+82">
<input type="hidden" id="KW" value="+965">
<input type="hidden" id="KG" value="+996">
<input type="hidden" id="LA" value="+856">
<input type="hidden" id="LV" value="+371">
<input type="hidden" id="LB" value="+961">
<input type="hidden" id="LS" value="+266">
<input type="hidden" id="LR" value="+231">
<input type="hidden" id="LY" value="+218">
<input type="hidden" id="LI" value="+417">
<input type="hidden" id="LT" value="+370">
<input type="hidden" id="LU" value="+352">
<input type="hidden" id="MO" value="+853">
<input type="hidden" id="MK" value="+389">
<input type="hidden" id="MG" value="+261">
<input type="hidden" id="MW" value="+265">
<input type="hidden" id="MY" value="+60">
<input type="hidden" id="MV" value="+960">
<input type="hidden" id="ML" value="+223">
<input type="hidden" id="MT" value="+356">
<input type="hidden" id="MH" value="+692">
<input type="hidden" id="MQ" value="+596">
<input type="hidden" id="MR" value="+222">
<input type="hidden" id="YT" value="+269">
<input type="hidden" id="MX" value="+52">
<input type="hidden" id="FM" value="+691">
<input type="hidden" id="MD" value="+373">
<input type="hidden" id="MC" value="+377">
<input type="hidden" id="MN" value="+976">
<input type="hidden" id="MS" value="+1664">
<input type="hidden" id="MA" value="+212">
<input type="hidden" id="MZ" value="+258">
<input type="hidden" id="MN" value="+95">
<input type="hidden" id="NA" value="+264">
<input type="hidden" id="NR" value="+674">
<input type="hidden" id="NP" value="+977">
<input type="hidden" id="NL" value="+31">
<input type="hidden" id="NC" value="+687">
<input type="hidden" id="NZ" value="+64">
<input type="hidden" id="NI" value="+505">
<input type="hidden" id="NE" value="+227">
<input type="hidden" id="NG" value="+234">
<input type="hidden" id="NU" value="+683">
<input type="hidden" id="NF" value="+672">
<input type="hidden" id="NP" value="+670">
<input type="hidden" id="NO" value="+47">
<input type="hidden" id="OM" value="+968">
<input type="hidden" id="PW" value="+680">
<input type="hidden" id="PA" value="+507">
<input type="hidden" id="PG" value="+675">
<input type="hidden" id="PY" value="+595">
<input type="hidden" id="PE" value="+51">
<input type="hidden" id="PH" value="+63">
<input type="hidden" id="PL" value="+48">
<input type="hidden" id="PT" value="+351">
<input type="hidden" id="PR" value="+1787">
<input type="hidden" id="QA" value="+974">
<input type="hidden" id="RE" value="+262">
<input type="hidden" id="RO" value="+40">
<input type="hidden" id="RU" value="+7">
<input type="hidden" id="RW" value="+250">
<input type="hidden" id="SM" value="+378">
<input type="hidden" id="ST" value="+239">
<input type="hidden" id="SA" value="+966">
<input type="hidden" id="SN" value="+221">
<input type="hidden" id="CS" value="+381">
<input type="hidden" id="SC" value="+248">
<input type="hidden" id="SL" value="+232">
<input type="hidden" id="SG" value="+65">
<input type="hidden" id="SK" value="+421">
<input type="hidden" id="SI" value="+386">
<input type="hidden" id="SB" value="+677">
<input type="hidden" id="SO" value="+252">
<input type="hidden" id="ZA" value="+27">
<input type="hidden" id="ES" value="+34">
<input type="hidden" id="LK" value="+94">
<input type="hidden" id="SH" value="+290">
<input type="hidden" id="KN" value="+1869">
<input type="hidden" id="SC" value="+1758">
<input type="hidden" id="SD" value="+249">
<input type="hidden" id="SR" value="+597">
<input type="hidden" id="SZ" value="+268">
<input type="hidden" id="SE" value="+46">
<input type="hidden" id="CH" value="+41">
<input type="hidden" id="SI" value="+963">
<input type="hidden" id="TW" value="+886">
<input type="hidden" id="TJ" value="+7">
<input type="hidden" id="TH" value="+66">
<input type="hidden" id="TG" value="+228">
<input type="hidden" id="TO" value="+676">
<input type="hidden" id="TT" value="+1868">
<input type="hidden" id="TN" value="+216">
<input type="hidden" id="TR" value="+90">
<input type="hidden" id="TM" value="+7">
<input type="hidden" id="TM" value="+993">
<input type="hidden" id="TC" value="+1649">
<input type="hidden" id="TV" value="+688">
<input type="hidden" id="UG" value="+256">
<!-- <input type="hidden" id="GB" value="44">UK (+44) -->
<input type="hidden" id="UA" value="+380">
<input type="hidden" id="AE" value="+971">
<input type="hidden" id="UY" value="+598">
<input type="hidden" id="US" value="+1">
<input type="hidden" id="UZ" value="+7">
<input type="hidden" id="VU" value="+678">
<input type="hidden" id="VA" value="+379">
<input type="hidden" id="VE" value="+58">
<input type="hidden" id="VN" value="+84">
<input type="hidden" id="VG" value="+84">
<input type="hidden" id="VI" value="+84">
<input type="hidden" id="WF" value="+681">
<input type="hidden" id="YE" value="+969">
<input type="hidden" id="YE" value="+967">
<input type="hidden" id="ZM" value="+260">
<input type="hidden" id="ZW" value="+263">

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('phonenumber_with_countrycode'), 'has-success': fields.phonenumber_with_countrycode && fields.phonenumber_with_countrycode.valid }">
    <label for="phonenumber_with_countrycode" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.phonenumber_with_countrycode') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
        <input type="text" v-model="form.phonenumber_with_countrycode" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('phonenumber_with_countrycode'), 'form-control-success': fields.phonenumber_with_countrycode && fields.phonenumber_with_countrycode.valid}" id="phonenumber_with_countrycode" name="phonenumber_with_countrycode" placeholder="{{ trans('admin.admin-user.columns.phonenumber_with_countrycode') }}">
        <div v-if="errors.has('phonenumber_with_countrycode')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('phonenumber_with_countrycode') }}</div>
    </div>
</div>

@if($role=='vendor')
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('available'), 'has-success': fields.available && fields.available.valid }">
    <label for="available" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.available') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
		<template v-if="!form.availability">
			

					<label class="switch switch-3d switch-success" data-toggle="modal" data-target="#exampleModal">

						<input type="checkbox" class="switch-input" v-model="form.availability" />
						<span class="switch-slider"></span>
					</label>

				</template>

			<template v-else>

					<label class="switch switch-3d switch-success" data-toggle="modal" data-target="#exampleModal1" />
						<input type="checkbox" class="switch-input" v-model="form.availability"
								data-target="#exampleModal" data-toggle="modal">
						<span class="switch-slider"></span>
					</label>
			</template>
		</div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('rating'), 'has-success': fields.rating && fields.rating.valid }">
    <label for="rating" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.rating') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
        @php

			for( $x = 0; $x < 5; $x++ )
			{
				if( floor( $rating )-$x >= 1 )
				{ echo '<i class="fa fa-star"></i>'; }
				elseif( $rating-$x > 0 )
				{ echo '<i class="fa fa-star-half-o"></i>'; }
				else
				{ echo '<i class="fa fa-star-o"></i>'; }
			}

		@endphp
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_image'), 'has-success': fields.shop_image && fields.shop_image.valid }">
                            <label for="shop_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ 'Shop Image' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                            
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
    'mediaCollection' => app(App\Models\AdminUsers::class)->getMediaCollection('shop_image'),
    'media' => $adminUser->getThumbs200ForCollection('shop_image'),
    'label' => 'Shop image'
])
                            </div>
                        </div>

@endif





                                <!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('profile_picture'), 'has-success': fields.profile_picture && fields.profile_picture.valid }">
                                    <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.profile_picture') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                                        <input type="text" v-model="form.profile_picture" v-validate="'required|profile_picture'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('profile_picture'), 'form-control-success': fields.profile_picture && fields.profile_picture.valid}" id="profile_picture" name="profile_picture" placeholder="{{ trans('admin.admin-user.columns.profile_picture') }}">
                                        <div v-if="errors.has('profile_picture')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('profile_picture') }}</div>
                                    </div>
                                </div> -->

                                <!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('language'), 'has-success': fields.language && fields.language.valid }">
                                    <label for="language" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.language') }}</label>
                                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
                                        <multiselect v-model="form.language" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_an_option') }}" :options="{{ $locales->toJson() }}" open-direction="bottom"></multiselect>
                                        <div v-if="errors.has('language')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('language') }}</div>
                                    </div>
                                </div> -->


                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
						</button>
						<a class="btn btn-primary" href="{{ url('admin/shop-detail') }}">
							<i class="nav-icon icon-star"></i> {{ __('Shop Detail') }}
						</a>
						<a href="{{ url('admin/stripe-connect') }}" class="btn btn-primary">
                            <i class="nav-icon icon-graduation"></i>
                            {{ trans('admin.stripe-connect.title') }}
						</a>
                    </div>

                </form>

            </profile-edit-profile-form>

        </div>

    </div>

@endsection
