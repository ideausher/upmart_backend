@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.create'))

@section('body')

    <div class="container-xl">
       
        <div class="card">

            <admin-user-form
                :action="'{{ url('admin/admin-users') }}'"
                :activation="!!'{{ $activation }}'"
                inline-template>

                <form class="form-horizontal form-create" method="post" @submit="modifyData()" @submit.prevent="onSubmit" :action="action">

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('admin.admin-user.actions.create') }}
                    </div>

                    <div class="card-body">

                        @include('admin.admin-user.components.form-elements')
                            <input type="hidden" id="hiidenlat" />
                            <input type="hidden" id="hiidenlng" />
                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('Gallery'), 'has-success': fields.Gallery && fields.Gallery.valid }">
    <label for="Profile_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ trans('admin.admin-user.columns.Gallery') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
        @include('brackets/admin-ui::admin.includes.media-uploader', [
            'mediaCollection' => app(App\Models\AdminUsers::class)->getMediaCollection('Gallery'),
            'label' => 'Profile picture'
        ])


                    </div>
                        </div>

                        <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_image'), 'has-success': fields.shop_image && fields.shop_image.valid }">
    <label for="shop_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-3'">{{ 'Shop Image' }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-7'">
    @include('brackets/admin-ui::admin.includes.media-uploader', [
    'mediaCollection' => app(App\Models\AdminUsers::class)->getMediaCollection('shop_image'),
    'label' => 'Shop image'
])
    </div>
</div>

                    <table>                
                <th class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </th>
                <th class="card-footer">
                
                <a class="btn btn-danger" href="{{ url('admin/admin-users') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-close'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.cancel') }}</a>
               
            
                </th>
                </table>

                </form>

            </admin-user-form>

        </div>

    </div>

@endsection

@section('bottom-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=places&callback=initMap" async defer></script>
<script>
    function initMap() {
        const input = document.getElementById("hiidenaddress");
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.setFields(["place_id", "geometry", "name", "formatted_address"]);
        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
            $("#hiidenlat").val(lat);
            $("#hiidenlng").val(lng);
            $("#hiidenaddress").val(place.formatted_address);
            $("#latitude").val(lat);
            $("#longitude").val(lng);
        });
    }
</script>
@endsection