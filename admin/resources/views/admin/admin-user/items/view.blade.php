@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.item.details'))

@section('body')

    <div class="container-xl">
        <div class="card">

            <item-form
                :data="{{ $details[0]->toJson() }}"
                v-cloak
                inline-template>
            
                <div class="form-horizontal form-edit" >


                    <div class="card-header">
                        <i class="fa fa-eye"></i> {{ trans('admin.item.details') }}
                    </div>

                    <div class="card-body">
                    <!-- <div class="form-group row align-items-center" :class="{'has-danger': errors.has('id'), 'has-success': fields.id && fields.id.valid }">
                        <label for="id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Id' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->id}}
                            <div v-if="errors.has('id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('id') }}</div>
                        </div>
                    </div> -->

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_name'), 'has-success': fields.product_name && fields.product_name.valid }">
                        <label for="product_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Product Name' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->product_name}}
                            <div v-if="errors.has('product_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_name') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_description'), 'has-success': fields.product_description && fields.product_description.valid }">
                        <label for="product_description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Product Description' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->product_description}}
                            <div v-if="errors.has('product_description')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_description') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('stock_quantity'), 'has-success': fields.stock_quantity && fields.stock_quantity.valid }">
                        <label for="stock_quantity" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Stock Quantity' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->stock_quantity}}
                            <div v-if="errors.has('stock_quantity')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('stock_quantity') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('price'), 'has-success': fields.price && fields.price.valid }">
                        <label for="price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Price' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @{{form.price | toCurrency }}
                            <div v-if="errors.has('price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('price') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('product_image'), 'has-success': fields.product_image && fields.product_image.valid }">
                        <label for="product_image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Product Images' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @foreach($imgs as $url)
                            <img src="{{$url}}" width="150" height="150">
                            @endforeach
                            <div v-if="errors.has('product_image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('product_image') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('category_name'), 'has-success': fields.category_name && fields.category_name.valid }">
                        <label for="category_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Category Name' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->category_name}}
                            <div v-if="errors.has('category_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('category_name') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('company_name'), 'has-success': fields.company_name && fields.company_name.valid }">
                        <label for="company_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Company Name' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->company_name}}
                            <div v-if="errors.has('company_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('company_name') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('shop_name'), 'has-success': fields.shop_name && fields.shop_name.valid }">
                        <label for="shop_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Shop Name' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->shop_name}}
                            <div v-if="errors.has('shop_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shop_name') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('discount'), 'has-success': fields.discount && fields.discount.valid }">
                        <label for="discount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'Discount' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            {{$details[0]->discount}}
                            <div v-if="errors.has('discount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('discount') }}</div>
                        </div>
                    </div>

                    <div class="form-group row align-items-center" :class="{'has-danger': errors.has('new_price'), 'has-success': fields.new_price && fields.new_price.valid }">
                        <label for="new_price" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ 'New Price' }}</label>
                            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                            @{{form.new_price | toCurrency }}
                            <div v-if="errors.has('new_price')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('new_price') }}</div>
                        </div>
                    </div>

                    </div>
                    <div class="card-footer">
                <a class="btn btn-danger" href="{{ url('admin/admin-users/'.$adminId.'/items') }}"> <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-arrow-left'"></i>
                        Back</a>
                </div>

                </div>

        </item-form>

        </div>
    
</div>


@endsection
