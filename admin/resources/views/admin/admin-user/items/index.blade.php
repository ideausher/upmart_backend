
<?php

use Akaunting\Money\Money;

?>

@extends('brackets/admin-ui::admin.layout.default')

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com//ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('title', trans('admin.item.actions.index'))

@section('body')
 

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">{{ trans('admin.item.actions.index') }}</h3>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <table id="tableId" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Product Name</th>
                      <th>Company Name</th>
                      <th>Product Description</th>
                      <th>Stock Quantity</th>
                      <th>Price</th>
                      <th>Product Images</th>
                      <th>Category</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
          
        
 
                    @foreach($items as $item)
                    <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->product_name}}</td>
                    <td>{{$item->company_name}}</td>
                    <td>{{$item->product_description}}</td>
                    <td>{{$item->stock_quantity}}</td>
                    <td>{{Money::CAD($item->new_price, true)}}</td>
                    <td><img src="{{$item->mediaUrl}}" width="60" height="60"></td>
                    <td>{{$item->category_name}}</td>
                    <td><a class="btn btn-sm btn-spinner btn-info" href="{{url('/admin/admin-users/'.$adminId.'/items/'.$item->id.'/details')}}" title="{{ 'view' }}" role="button"><i class="fa fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(function () {
      $.noConflict();
        $('#tableId').DataTable({
          "ordering": true,
          columnDefs: [{
            orderable: false,
            targets: "no-sort"
          }],
          "order": [[ 0, "asc" ]]
        });
    });
</script>
