@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.index'))

@section('body')
<script>//Camel Case of  First NAme

String.prototype.toTitle = function() {
  return this.replace(/(^|\s)\S/g, function(t) { return t.toUpperCase() });
}</script>

    <admin-user-listing
        :data="{{ $data->toJson() }}"
        :activation="!!'{{ $activation }}'"
        :url="'{{ url('admin/admin-users') }}'"
        inline-template>
        
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.admin-user.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/admin-users/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.admin-user.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>

                        <table class="table table-hover table-listing">
                            <thead>
                                <tr>
                                    <!-- <th is='sortable' :column="'id'">{{ trans('admin.admin-user.columns.id') }}</th> -->
                                    <th is='sortable' :column="'first_name'">{{ trans('admin.admin-user.columns.first_name') }}</th>


                                    <!-- <th is='sortable' :column="'last_name'">{{ trans('admin.admin-user.columns.last_name') }}</th> -->
                                    <th is='sortable' :column="'email'">{{ trans('admin.admin-user.columns.email') }}</th>
                                    <th is='sortable' :column="'shop_name'">{{ trans('admin.admin-user.columns.phonenumber_with_countrycode') }}</th>
                                    <th is='sortable' :column="'profile_picture'">{{ trans('admin.admin-user.columns.profile_picture') }}</th>


                                    <th is='sortable' :column="'shop_name'">{{ trans('admin.admin-user.columns.shop_name') }}</th>
                                    <th is='sortable' :column="'shop_address'">{{ trans('admin.admin-user.columns.shop_address') }}</th>
                                    {{-- <!-- <th is='sortable' :column="'activated'" v-if="activation">{{ trans('admin.admin-user.columns.activated') }}</th> --}}
                                    <th is='sortable' :column="'forbidden'">{{ trans('admin.admin-user.columns.forbidden') }}</th>
                                    {{-- <th is='sortable' :column="'is_blocked'"> --}}
                                        {{-- {{trans('admin.admin-user.columns.is_blocked')}} --}}
                                        {{-- </th> --}}

                                    <!-- <th is='sortable' :column="'language'">{{ trans('admin.admin-user.columns.language') }}</th> -->
                                    <!-- <th is='sortable' :column="'last_login_at'">{{ trans('admin.admin-user.columns.last_login_at') }}</th> -->

                                    <th></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <tr v-for="(item, index) in collection">
                                    <!-- <td >@{{ item.id }}</td> -->
                                    <td >@{{ item.first_name}}</td>
                                    <!-- <td >@{{ item.last_name }}</td> -->
                                    <td >@{{ item.email }}</td>
                                    <td>
                                        <span v-if="item.vendorrelationship != null"> @{{item.vendorrelationship.phonenumber_with_countrycode}}</span></td>
                                        <td ><img :src="item.mediaUrl" height="50" width="50"  ></td>
                                    <td> <span v-if="item.vendorrelationship != null"> @{{item.vendorrelationship.shop_name}}</span></td>
                                    <td><span v-if="item.vendorrelationship != null"> @{{item.vendorrelationship.shop_address}}</span></td>


                                    <!-- unblock a vendor
                                    <td v-if="activation">
                                        <label class="switch switch-3d switch-success"  :for="item.id"
                                                onclick="myFunction(this)" data-toggle="modal" data-target="#exampleModalunblock">
                                            <input type="checkbox" class="switch-input" v-model="collection[index].activated" @change="toggleSwitch(item.resource_url, 'activated', collection[index])">
                                            <span class="switch-slider"></span>
                                        </label>
                                    </td>
                                    <td > -->
                                    <!--block a vendor -->
                                      <!--  <label class="switch switch-3d switch-danger" :for="item.id"
                                                onclick="myFunction(this)" data-toggle="modal" data-target="#exampleModal">
                                            <input type="checkbox" class="switch-input" v-model="collection[index].forbidden" @change="toggleSwitch(item.resource_url, 'forbidden', collection[index])">
                                            <span class="switch-slider"></span>
                                        </label>
                                    </td> -->
                                       <!-- <td >@{{ item.language }}</td> -->
                                    <!-- <td >@{{ item.last_login_at }}</td>  -->
                                    <template v-if="!item.forbidden">
                                        <td >

                                            <label class="switch switch-3d switch-success" :for="item.id" :forN="item.first_name" onclick="myFunction(this)" data-toggle="modal" data-target="#exampleModal">

                                                <input type="checkbox" class="switch-input"  v-model="collection[index].forbidden"

                                                @change="toggleSwitch(item.resource_url, 'forbidden', collection[index])"/>
                                                <span class="switch-slider"></span>
                                            </label>

                                        </td>
                                      </template>

                                    <template v-else>

                                        <td v-if="item.forbidden">
                                            <label class="switch switch-3d switch-danger" :for="item.id" onclick="myFunction1(this)" data-toggle="modal" data-target="#exampleModal1"/>
                                                <input type="checkbox" class="switch-input"   v-model="collection[index].forbidden"
                                                     data-target="#exampleModal" data-toggle="modal"
                                                    @change="toggleSwitch(item.resource_url, 'forbidden', collection[index])"/>
                                                <span class="switch-slider"></span>
                                            </label>
                                        </td>
                                    </template>




                                    <td>
                                        <div class="row no-gutters">
                                            @can('admin.admin-user.impersonal-login')
                                            <div class="col-auto">
                                                <button class="btn btn-sm btn-success" v-show="item.activated" @click="impersonalLogin(item.resource_url + '/impersonal-login', item)" title="Impersonal login" role="button"><i class="fa fa-user-o"></i></button>
                                            </div>
                                            @endcan
                                            <div class="col-auto">
                                                <button class="btn btn-sm btn-warning" v-show="!item.activated" @click="resendActivation(item.resource_url + '/resend-activation')" title="Resend activation" role="button"><i class="fa fa-envelope-o"></i></button>
                                            </div>
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            </div>
                                            <div class="col-auto">
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/items'" title="{{ 'Items' }}" role="button"><i class="fa fa-list-alt"></i></a>
                                            </div>
                                            <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                            </form>
                                            <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/bookings'" title="Booking" role="button"><i class="fa fa-ticket"></i></a>
                                                </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row" v-if="pagination.state.total > 0">
                            <div class="col-sm">
                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                            </div>
                            <div class="col-sm-auto">
                                <pagination></pagination>
                            </div>
                        </div>

	                    <div class="no-items-found" v-if="!collection.length > 0">
		                    <i class="icon-magnifier"></i>
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            <a class="btn btn-primary btn-spinner" href="{{ url('admin/admin-users/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.new') }} AdminUser</a>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </admin-user-listing>

        <!-- Modal -->
    <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to block this vendor?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm()">

                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div>

                    <form method="get" name="reason_form"  id="form" onsubmit="return validateReasonForm()"
                        action="{{ url('admin/admin-users/block') }}">
                        <div class="modal-iiner-content">
                            <input type="hidden" name="userid" id="userId">
                            <input type="hidden" name="username" id="userName">
                            {{ csrf_field() }}
                            </span>Please Enter Reason For Block</span>
                            <input type="text" name="reason" id="myMessage">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="resetForm()">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Block" id="block">
                        </div>
                    </form>
                </div>
                </div>
                

            </div>
        </div>
    </div>

    <div class="modal fade modal-custom" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="confirm_text">Are you sure you want to unblock this vendor?</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div>

                    <form method="get" name="reason_form"  id="form"
                        action="{{ url('admin/admin-users/unblock') }}">
                        <input type="hidden" name="userid" id="userIduser">
                        {{ csrf_field() }}
                        {{-- </span>Please Enter Reason For Block</span>
                        <input type="text" name="reason" id="myMessage"> --}}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary" value="Yes, Unblock" id="block">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


<!-- unblock vendor -->







@endsection

@section('bottom-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>


    <script src="{{ url('js/popup.js') }}" type="text/javascript"></script>
@endsection
