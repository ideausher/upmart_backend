-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 20, 2021 at 01:03 PM
-- Server version: 5.7.35-0ubuntu0.18.04.1
-- PHP Version: 7.3.29-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_activations`
--

CREATE TABLE `admin_activations` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_fcm_tokens`
--

CREATE TABLE `admin_fcm_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `merchant_type` int(11) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `token` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_fcm_tokens`
--

INSERT INTO `admin_fcm_tokens` (`id`, `merchant_type`, `userId`, `token`, `created_at`, `updated_at`) VALUES
(1, NULL, 268, 'fq3tHBLlbsWGovC2l7dFUs:APA91bH-GvMtl_z-hHj4xBoouCPIydAeaU45_w2-EMTc3Ti2Y5Pak4ZOCH7JhbJUfDO2dL9GSJLhn3rKKh4cBZ0kTu-QwC4_eQLrtRDWUpyB93XRheJmYtOx_p4moJ7Hb1JggnfniBmY', '2021-08-03 02:47:10', '2021-08-03 02:47:10'),
(2, NULL, 1, 'ffguUv9QyaDcF-e6e1Vhxv:APA91bHpJKr7QFC1-nAwiLsw_aY2xFO_2yYP6kQ05cHeHA8nBnDLFu0eMiRK0M_6SV7x4gZVh2kms2WPs7WFP9uj5BD_dGdoZCzuZGZ9yqzJynxnwy3G9QitzCYkqb7SLax7El-U_LSm', '2021-08-03 02:50:51', '2021-08-03 02:50:51'),
(3, NULL, 1, 'fq3tHBLlbsWGovC2l7dFUs:APA91bH-GvMtl_z-hHj4xBoouCPIydAeaU45_w2-EMTc3Ti2Y5Pak4ZOCH7JhbJUfDO2dL9GSJLhn3rKKh4cBZ0kTu-QwC4_eQLrtRDWUpyB93XRheJmYtOx_p4moJ7Hb1JggnfniBmY', '2021-08-04 04:41:00', '2021-08-04 04:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_settings`
--

CREATE TABLE `admin_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `delivery_boy_searching` int(11) NOT NULL COMMENT '''0->Automatic,1->Manual''',
  `delivery_boy_search_limit` bigint(20) NOT NULL,
  `charge_payment_from_customer_booking_accept` int(11) NOT NULL COMMENT '1->after,2->before',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `max_accepting_limit` int(11) NOT NULL DEFAULT '1',
  `send_db_noti_before_schedule_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `delivery_boy_searching`, `delivery_boy_search_limit`, `charge_payment_from_customer_booking_accept`, `created_at`, `updated_at`, `max_accepting_limit`, `send_db_noti_before_schedule_time`) VALUES
(1, 0, 20, 2, '2021-02-24 04:29:28', '2021-05-11 10:24:00', 1, '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber_with_countrycode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `availability` int(11) NOT NULL DEFAULT '0',
  `country_iso_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `first_name`, `last_name`, `email`, `country_code`, `phonenumber_with_countrycode`, `password`, `remember_token`, `activated`, `forbidden`, `stripe_user_id`, `language`, `deleted_at`, `created_at`, `updated_at`, `last_login_at`, `availability`, `country_iso_code`) VALUES
(1, 'Nagarjun', 'Gaddam', 'administrator@brackets.sk', '+1', '5023862472', '$2y$10$.00PCR//PEbTyIjtsRHwQewwmR/K.YrU7TwN6nnK0RgY.gx6D0FGO', 'yMdnxKmjAHb9Y2VhMl2PPv42A2j6VFmeDSjZNqOgFRWHSYE0ZKzROP6BpoXX', 1, 0, 'acct_1JHNNEGJHAWQLZvJ', 'en', NULL, '2020-10-15 23:47:35', '2021-08-20 01:42:04', '2021-08-20 01:42:04', 0, 'US'),
(267, 'Test', 'Vendor', 'Testvendor@mailinator.com', '+1', '4747474747', '$2y$10$7lMIY5G/AIdvEWYwn06VVeHNInXsxqQjYEQZolX6EfvmIiJaqc/7S', 'WMS8oum1T4xUbh44ug3t1REBHy8t1Xe3Nx1RHLnuaIlxAZ5Qt2aYuWQDBRp5', 1, 0, 'acct_1JJwUPQ1L9hdFDFV', 'en', NULL, '2021-07-22 06:20:24', '2021-08-18 05:37:23', '2021-08-18 05:37:23', 1, 'US'),
(268, 'Shalini', 'Mishra', 'shalini@yopmail.com', '+91', '8799008978', '$2y$10$XF0tW.ZriwqhHu5Y/8QqpOiPOSZp6teAd5PH7l6Oa3lkOOZtLbIdy', 'bLJMNxs3099br4KjCZjxytcWc6viD0aHndqGncSZBlmqoEAWcvTTlu6cSQL6', 1, 0, NULL, 'en', NULL, '2021-07-23 00:27:30', '2021-08-18 05:33:54', '2021-08-18 05:33:54', 1, 'IN'),
(269, 'Test', 'Dev', 'adminupmart@yopmail.com', '+91', '9632587411', '$2y$10$cc.Tozr85z4R2LA4KaydrePkCJ6gnI4pMtjgmM7TYU5BTttr0y6Na', 'dIqF7G9PhOjNbZOuVOw4596zt8HptvPbpuPs369RfDjRWyHjQImAW9GLd46L', 1, 0, 'acct_1JHNNEGJHAWQLZvJ', 'en', NULL, '2021-07-23 00:49:25', '2021-08-11 03:33:36', '2021-08-11 03:32:22', 0, 'IN'),
(270, 'India', 'Mart', 'nagarjun.gaddam@gmail.com', '+1', '3065266841', '$2y$10$ooapn5AZuEb9w3uIvaiSmORm4EsbUd8OZMbre9r6X9Xvd8eNTmPQ.', NULL, 1, 0, NULL, 'en', '2021-08-15 11:51:30', '2021-08-15 11:48:59', '2021-08-15 11:51:30', NULL, 0, 'CA'),
(271, 'Spice', 'Bazar', 'ghj.yu6@gmail.com', '+1', '3065266841', '$2y$10$H5j1u2fBzBWnXPxHwli.rur/6GHAfxtcu/cYO2xuk/7sh4gu8reuu', NULL, 1, 0, NULL, 'en', NULL, '2021-08-15 11:49:31', '2021-08-15 11:49:31', NULL, 0, 'CA'),
(272, 'India', 'Mart', 'testxyz@gmail.com', '+1', '3065266841', '$2y$10$8CjtzqkTOSKJ7Y.LOFaSwOt4wCYMGu6qzfdR1RQ4AxWyUyNYqRxGG', NULL, 1, 1, NULL, 'en', NULL, '2021-08-15 11:49:58', '2021-08-19 09:19:46', NULL, 0, 'CA'),
(273, 'Spice', 'Bazar', 'ghh.yhi@gmail.com', '+1', '3065266841', '$2y$10$NbIlgPe62qHZ/mgKIfqMYeIYLXV8VmiUn0IvPPjvigwShgbgGLeAm', NULL, 1, 1, NULL, 'en', NULL, '2021-08-15 11:50:34', '2021-08-15 11:53:00', NULL, 0, 'CA'),
(274, 'India', 'Mart', 'swethareddypasham@gmail.com', '+1', '3065266841', '$2y$10$MqhmNOdte5/5lVcI8PdsjeQ.yz0EIwphQg97mgIPwS.7DG99MTGPm', NULL, 1, 0, NULL, 'en', '2021-08-15 11:51:25', '2021-08-15 11:50:49', '2021-08-15 11:51:25', NULL, 0, 'CA');

-- --------------------------------------------------------

--
-- Table structure for table `admin_vendors`
--

CREATE TABLE `admin_vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `phonenumber_with_countrycode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `additional_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `shop_rating` int(11) NOT NULL DEFAULT '0',
  `shop_type` int(11) NOT NULL,
  `platform_charges` decimal(12,2) DEFAULT '0.00',
  `commission_charges` decimal(12,2) DEFAULT '0.00',
  `gst` decimal(12,2) DEFAULT '0.00',
  `pst` decimal(12,2) DEFAULT '0.00',
  `hst` decimal(12,2) DEFAULT '0.00',
  `stripe_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  `availability` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_vendors`
--

INSERT INTO `admin_vendors` (`id`, `user_id`, `phonenumber_with_countrycode`, `shop_name`, `shop_address`, `additional_address`, `description`, `country_code`, `lat`, `lng`, `shop_rating`, `shop_type`, `platform_charges`, `commission_charges`, `gst`, `pst`, `hst`, `stripe_user_id`, `forbidden`, `availability`, `deleted_at`) VALUES
(1, 267, '4747474747', 'Test vendor y', 'Daraka Muhala, Mansa, Punjab 151505, India', 'dsaf', 'asdf', '+1', '29.98865540', '75.39311990', 0, 3, '10.00', '10.00', '10.00', '10.00', '10.00', 'acct_1JJwUPQ1L9hdFDFV', 0, 1, NULL),
(2, 268, '8799008978', 'Reliance Shop', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', '+91', '30.70984170', '76.68945210', 0, 3, '10.00', '5.00', '5.00', '10.00', '20.00', 'bnmbm', 0, 1, NULL),
(3, 269, '9632587411', 'Dev. Test Shop name', 'Ashriya Infotech Pvt Ltd, Phase 8, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab, India', 'dhanbad', 'developer test shop', '+91', '30.70984170', '76.68945210', 0, 1, '10.00', '10.00', '5.00', '10.00', '15.00', 'acct_1JHNxxILDLJdxGNX', 0, 0, NULL),
(4, 270, '3065266841', 'India Mart', '2131 Broad St, Regina, SK S4P 3W4, Canada', 'Saskatchewan', 'Indin Grocery Store', '+1', '50.44492500', '-104.60614770', 0, 3, '2.99', NULL, '5.00', '6.00', NULL, NULL, 0, 0, '2021-08-15 11:51:30'),
(5, 271, '3065266841', 'Spice Bazar', '2131 Rose St, Regina, SK S4P 2A4, Canada', '2131 Broad Street\nApt - 704', 'Spice Shop', '+1', '50.44491160', '-104.60756530', 0, 3, '2.99', '6.00', '5.00', '6.00', '0.00', NULL, 0, 0, NULL),
(6, 272, '3065266841', 'India Mart', '2131 Broad St, Regina, SK S4P 3W4, Canada', 'Saskatchewan', 'India Grocery Store', '+1', '50.44492500', '-104.60614770', 0, 3, '2.99', NULL, '5.00', '6.00', NULL, NULL, 1, 0, NULL),
(7, 273, '3065266841', 'Spice Bazar', '2131 Rose St, Regina, SK S4P 2A4, Canada', '2131 Broad Street\nApt - 704', 'Spice Shop', '+1', '50.44491160', '-104.60756530', 0, 3, '2.99', '6.00', '5.00', '6.00', '0.00', NULL, 1, 0, NULL),
(8, 274, '3065266841', 'India Mart', '2131 Broad St, Regina, SK S4P 3W4, Canada', 'Saskatchewan', 'India Grocery Store', '+1', '50.44492500', '-104.60614770', 0, 3, '2.99', NULL, '5.00', '6.00', NULL, NULL, 0, 0, '2021-08-15 11:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `app_packages`
--

CREATE TABLE `app_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bundle_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `app_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `app_packages`
--

INSERT INTO `app_packages` (`id`, `bundle_id`, `app_name`, `created_at`, `updated_at`) VALUES
(8, 'com.upmart.customer', 'Upmart Customer App', '2021-04-29 01:20:34', '2021-05-26 05:21:50'),
(9, 'com.upmart.driver', 'Driver app', '2021-06-07 00:02:32', '2021-06-07 00:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `app_versions`
--

CREATE TABLE `app_versions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `app_package_id` int(11) NOT NULL,
  `force_update` tinyint(1) NOT NULL,
  `message` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `version` double(12,2) NOT NULL,
  `code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `platform` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `block_reasons_vendor`
--

CREATE TABLE `block_reasons_vendor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_reasons_vendor`
--

INSERT INTO `block_reasons_vendor` (`id`, `user_id`, `reason`, `created_at`, `updated_at`) VALUES
(1, 269, 'delivery boy is not available yet', NULL, NULL),
(2, 268, 'NA', NULL, NULL),
(3, 273, 'NA', NULL, NULL),
(4, 272, '123', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `block_reason_delivery_boy`
--

CREATE TABLE `block_reason_delivery_boy` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_reason_delivery_boy`
--

INSERT INTO `block_reason_delivery_boy` (`id`, `user_id`, `reason`, `created_at`, `updated_at`) VALUES
(1, 6, 'ghgthgjjg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_details`
--

CREATE TABLE `booking_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_code` text COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `shopId` int(11) DEFAULT NULL,
  `addressId` int(11) DEFAULT NULL,
  `cardId` int(11) DEFAULT NULL,
  `couponId` int(11) DEFAULT NULL,
  `deliveryBoyId` int(11) DEFAULT NULL,
  `bookingType` tinyint(4) NOT NULL DEFAULT '1',
  `bookingDateTime` datetime DEFAULT NULL,
  `after_charge_amount` decimal(12,2) NOT NULL,
  `tip_to_delivery_boy` decimal(12,2) DEFAULT '0.00',
  `commission_charge` decimal(12,2) NOT NULL,
  `platform_charge` decimal(12,2) NOT NULL,
  `delivery_charge_to_delivery_boy` decimal(12,2) NOT NULL,
  `delivery_charge_for_customer` decimal(12,2) NOT NULL,
  `amount_after_discount` decimal(12,2) NOT NULL,
  `discount_amount` decimal(12,2) NOT NULL,
  `amount_before_discount` decimal(12,2) NOT NULL,
  `stripe_charges` decimal(12,2) NOT NULL,
  `amount_after_stripe_charges` decimal(12,2) NOT NULL,
  `instruction` text COLLATE utf8_unicode_ci,
  `deliveryboy_accepted_time` datetime DEFAULT NULL,
  `shop_status_change_time` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `as_total_amount_to_admin` decimal(12,2) DEFAULT NULL,
  `ws_total_amount_to_db` decimal(12,2) DEFAULT NULL,
  `ws_tip_to_db` decimal(12,2) DEFAULT NULL,
  `ws_deliverycharge_amount_to_db` decimal(12,2) DEFAULT NULL,
  `bs_total_paid_to_admin` decimal(12,2) DEFAULT NULL,
  `bs_amount_to_vendor_after_commi` decimal(12,2) DEFAULT NULL,
  `bs_comm_charged_to_vendor` decimal(12,2) DEFAULT NULL,
  `bs_amount_to_vendor_before_commi` decimal(12,2) DEFAULT NULL,
  `amount_paid_to_delivery_boy_on_order_pickup` decimal(12,2) NOT NULL,
  `hst` decimal(12,2) NOT NULL,
  `pst` decimal(12,2) NOT NULL,
  `amount_paid_to_delivery_boy_on_order_delivered` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `timezone` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_details`
--

INSERT INTO `booking_details` (`id`, `booking_code`, `userId`, `shopId`, `addressId`, `cardId`, `couponId`, `deliveryBoyId`, `bookingType`, `bookingDateTime`, `after_charge_amount`, `tip_to_delivery_boy`, `commission_charge`, `platform_charge`, `delivery_charge_to_delivery_boy`, `delivery_charge_for_customer`, `amount_after_discount`, `discount_amount`, `amount_before_discount`, `stripe_charges`, `amount_after_stripe_charges`, `instruction`, `deliveryboy_accepted_time`, `shop_status_change_time`, `status`, `active`, `as_total_amount_to_admin`, `ws_total_amount_to_db`, `ws_tip_to_db`, `ws_deliverycharge_amount_to_db`, `bs_total_paid_to_admin`, `bs_amount_to_vendor_after_commi`, `bs_comm_charged_to_vendor`, `bs_amount_to_vendor_before_commi`, `amount_paid_to_delivery_boy_on_order_pickup`, `hst`, `pst`, `amount_paid_to_delivery_boy_on_order_delivered`, `created_at`, `updated_at`, `timezone`) VALUES
(1, '0S0S20210722121108', 1, 1, 1, NULL, 1, NULL, 1, '2021-07-23 00:00:00', '78.10', '0.00', '5.30', '10.00', '0.00', '0.00', '53.00', '10.00', '63.00', '2.56', '75.54', NULL, NULL, NULL, 11, 1, '12.74', '0.00', '0.00', '0.00', '75.54', '62.80', '5.30', '53.00', '0.00', '5.30', '5.30', '0.00', '2021-07-22 06:41:10', '2021-07-22 06:41:24', 'Asia/Kolkata'),
(2, 'D0SA20210722121450', 1, 1, 1, NULL, NULL, NULL, 1, '2021-07-23 00:00:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, NULL, 11, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-07-22 06:44:52', '2021-07-22 06:45:26', 'Asia/Kolkata'),
(3, 'DASD20210723115607', 5, 2, 3, NULL, 7, NULL, 2, '2021-07-24 00:00:00', '108.69', '1.00', '3.80', '10.00', '1.99', '5.97', '68.40', '7.60', '76.00', '3.45', '106.24', NULL, NULL, NULL, 14, 1, '-28.27', '37.99', '1.00', '1.99', '106.24', '96.52', '3.80', '76.00', '15.00', '13.68', '6.84', '20.00', '2021-07-23 06:26:09', '2021-07-23 13:05:05', 'Asia/Kolkata'),
(4, 'D00D20210726125931', 12, 1, 5, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '109.77', '0.00', '4.50', '10.00', '13.76', '41.27', '45.00', '0.00', '45.00', '3.48', '106.29', NULL, NULL, '2021-07-26 13:03:31', 3, 1, '3.53', '48.76', '0.00', '13.76', '106.29', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-26 07:29:33', '2021-07-26 07:29:33', 'Asia/Kolkata'),
(5, '0DD020210726130214', 12, 1, 5, NULL, NULL, 2, 2, '2021-07-27 00:00:00', '109.77', '0.00', '4.50', '10.00', '13.76', '41.27', '45.00', '0.00', '45.00', '3.48', '106.29', NULL, NULL, '2021-07-26 13:39:28', 10, 1, '3.53', '48.76', '0.00', '13.76', '106.29', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-26 07:32:17', '2021-07-26 08:13:21', 'Asia/Kolkata'),
(6, '0DA020210727105129', 12, 1, 5, NULL, 9, NULL, 2, '2021-07-28 00:00:00', '123.81', '3.00', '5.67', '10.00', '13.76', '41.27', '56.70', '6.30', '63.00', '3.89', '122.92', NULL, NULL, '2021-07-27 11:07:41', 3, 1, '4.29', '51.76', '3.00', '13.76', '122.92', '66.87', '5.67', '56.70', '15.00', '5.67', '5.67', '20.00', '2021-07-27 05:21:31', '2021-07-27 05:21:31', 'Asia/Kolkata'),
(7, 'DSSD20210727112717', 12, 1, 5, NULL, NULL, NULL, 2, '2021-07-28 00:00:00', '109.77', '1.00', '4.50', '10.00', '13.76', '41.27', '45.00', '0.00', '45.00', '3.48', '107.29', NULL, NULL, '2021-07-27 11:53:09', 11, 1, '3.53', '49.76', '1.00', '13.76', '107.29', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-27 05:57:19', '2021-07-27 06:34:49', 'Asia/Kolkata'),
(8, 'SASS20210727113041', 12, 1, 5, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '109.77', '0.00', '4.50', '10.00', '13.76', '41.27', '45.00', '0.00', '45.00', '3.48', '106.29', NULL, NULL, NULL, 14, 1, '3.53', '48.76', '0.00', '13.76', '106.29', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-27 06:00:43', '2021-07-27 06:01:06', 'Asia/Kolkata'),
(9, 'SSS020210727113105', 12, 1, 5, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '109.77', '0.00', '4.50', '10.00', '13.76', '41.27', '45.00', '0.00', '45.00', '3.48', '106.29', NULL, NULL, NULL, 14, 1, '3.53', '48.76', '0.00', '13.76', '106.29', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-27 06:01:07', '2021-07-27 06:02:06', 'Asia/Kolkata'),
(10, 'ASDS20210727125356', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-27 07:23:58', '2021-07-27 07:24:16', 'Asia/Kolkata'),
(11, 'SDA020210727131332', 13, 1, 7, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '104.35', '0.00', '4.50', '10.00', '11.95', '35.85', '45.00', '0.00', '45.00', '3.33', '101.02', NULL, NULL, NULL, 14, 1, '0.07', '46.95', '0.00', '11.95', '101.02', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-27 07:43:34', '2021-07-27 07:44:12', 'Asia/Kolkata'),
(12, '0S0A20210727131532', 13, 1, 7, NULL, NULL, NULL, 2, '2021-07-27 00:00:00', '104.35', '0.00', '4.50', '10.00', '11.95', '35.85', '45.00', '0.00', '45.00', '3.33', '101.02', NULL, NULL, NULL, 14, 1, '0.07', '46.95', '0.00', '11.95', '101.02', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-27 07:45:33', '2021-07-27 07:46:08', 'Asia/Kolkata'),
(13, 'ADAD20210728054446', 23, 1, 9, NULL, NULL, NULL, 2, '2021-07-28 00:00:00', '103.88', '1.00', '4.50', '10.00', '11.79', '35.38', '45.00', '0.00', '45.00', '3.31', '101.57', NULL, NULL, NULL, 14, 1, '-0.22', '47.79', '1.00', '11.79', '101.57', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-28 00:14:48', '2021-07-28 00:15:06', 'Asia/Kolkata'),
(14, 'ADDA20210728054556', 23, 1, 9, NULL, 9, NULL, 1, '2021-07-28 00:00:00', '63.10', '0.00', '4.05', '10.00', '0.00', '0.00', '40.50', '4.50', '45.00', '2.13', '60.97', NULL, NULL, NULL, 14, 1, '11.92', '0.00', '0.00', '0.00', '60.97', '49.05', '4.05', '40.50', '0.00', '4.05', '4.05', '0.00', '2021-07-28 00:15:58', '2021-07-28 00:16:05', 'Asia/Kolkata'),
(15, 'D0SA20210728054628', 23, 1, 9, NULL, NULL, NULL, 1, '2021-07-31 00:00:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, NULL, 11, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-07-28 00:16:30', '2021-07-28 00:16:50', 'Asia/Kolkata'),
(16, '0AAA20210728054731', 23, 1, 9, NULL, NULL, NULL, 1, '2021-07-29 00:00:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, '2021-07-28 05:48:22', 4, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-07-28 00:17:33', '2021-07-28 00:17:33', 'Asia/Kolkata'),
(17, 'A0AS20210728063514', 24, 2, 10, NULL, NULL, NULL, 2, '2021-08-02 23:59:00', '113.14', '0.00', '3.80', '10.00', '0.18', '0.54', '76.00', '0.00', '76.00', '3.58', '109.56', NULL, NULL, '2021-07-28 06:44:29', 3, 1, '-24.42', '35.18', '0.00', '0.18', '109.56', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 01:05:16', '2021-07-28 01:05:16', 'Asia/Kolkata'),
(18, 'DAA020210728064540', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '215.74', '0.00', '7.60', '10.00', '0.18', '0.54', '152.00', '0.00', '152.00', '6.56', '209.18', NULL, NULL, '2021-07-28 06:46:05', 3, 1, '-23.60', '35.18', '0.00', '0.18', '209.18', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-28 01:15:42', '2021-07-28 01:15:42', 'Asia/Kolkata'),
(19, 'ADAD20210728064622', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '215.74', '0.00', '7.60', '10.00', '0.18', '0.54', '152.00', '0.00', '152.00', '6.56', '209.18', NULL, NULL, '2021-07-28 06:46:52', 3, 1, '-23.60', '35.18', '0.00', '0.18', '209.18', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-28 01:16:23', '2021-07-28 01:16:23', 'Asia/Kolkata'),
(20, 'SDAS20210728064740', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '215.74', '0.00', '7.60', '10.00', '0.18', '0.54', '152.00', '0.00', '152.00', '6.56', '209.18', NULL, NULL, '2021-07-28 06:48:06', 3, 1, '-23.60', '35.18', '0.00', '0.18', '209.18', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-28 01:17:42', '2021-07-28 01:17:42', 'Asia/Kolkata'),
(21, 'SDAD20210728074359', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '113.14', '0.00', '3.80', '10.00', '0.18', '0.54', '76.00', '0.00', '76.00', '3.58', '109.56', NULL, NULL, '2021-07-28 07:45:46', 3, 1, '-24.42', '35.18', '0.00', '0.18', '109.56', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 02:14:01', '2021-07-28 02:14:01', 'Asia/Kolkata'),
(22, 'SA0S20210728074711', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '113.14', '0.00', '3.80', '10.00', '0.18', '0.54', '76.00', '0.00', '76.00', '3.58', '109.56', NULL, NULL, '2021-07-28 07:47:34', 3, 1, '-24.42', '35.18', '0.00', '0.18', '109.56', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 02:17:13', '2021-07-28 02:17:13', 'Asia/Kolkata'),
(23, 'DDD020210728080544', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '113.14', '0.00', '3.80', '10.00', '0.18', '0.54', '76.00', '0.00', '76.00', '3.58', '109.56', NULL, NULL, NULL, 14, 1, '-24.42', '35.18', '0.00', '0.18', '109.56', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 02:35:51', '2021-07-28 13:04:05', 'Asia/Kolkata'),
(24, 'AAAS20210728081629', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '215.74', '0.00', '7.60', '10.00', '0.18', '0.54', '152.00', '0.00', '152.00', '6.56', '209.18', NULL, NULL, '2021-07-28 08:17:11', 3, 1, '-23.60', '35.18', '0.00', '0.18', '209.18', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-28 02:46:31', '2021-07-28 02:46:31', 'Asia/Kolkata'),
(25, 'AS0020210728101928', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-28 00:00:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-28 10:20:19', 3, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 04:49:30', '2021-07-28 04:50:09', 'Asia/Kolkata'),
(26, 'SDA020210728103547', 26, 2, 6, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-28 10:36:46', 3, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 05:05:51', '2021-07-28 05:05:51', 'Asia/Kolkata'),
(27, 'SDDD20210728103713', 26, 2, 6, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-28 10:37:37', 3, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 05:07:15', '2021-07-28 05:07:15', 'Asia/Kolkata'),
(28, '00A020210728134402', 17, 1, 13, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '103.44', '0.00', '4.50', '10.00', '11.65', '34.94', '45.00', '0.00', '45.00', '3.30', '100.14', NULL, NULL, NULL, 14, 1, '-0.51', '46.65', '0.00', '11.65', '100.14', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-28 08:14:04', '2021-07-28 13:04:06', 'Asia/Kolkata'),
(29, 'DSDD20210728135218', 13, 1, 7, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '104.35', '0.00', '4.50', '10.00', '11.95', '35.85', '45.00', '0.00', '45.00', '3.33', '101.02', NULL, NULL, NULL, 14, 1, '0.07', '46.95', '0.00', '11.95', '101.02', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-28 08:22:20', '2021-07-28 13:04:09', 'Asia/Kolkata'),
(30, '0ADA20210728135408', 13, 1, 7, NULL, NULL, NULL, 2, '2021-07-28 23:59:00', '104.35', '0.00', '4.50', '10.00', '11.95', '35.85', '45.00', '0.00', '45.00', '3.33', '101.02', NULL, NULL, NULL, 14, 1, '0.07', '46.95', '0.00', '11.95', '101.02', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-28 08:24:10', '2021-07-28 13:04:11', 'Asia/Kolkata'),
(31, 'SA0A20210729045505', 24, 2, 10, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '113.14', '0.00', '3.80', '10.00', '0.18', '0.54', '76.00', '0.00', '76.00', '3.58', '109.56', NULL, NULL, NULL, 14, 1, '-24.42', '35.18', '0.00', '0.18', '109.56', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-28 23:25:07', '2021-07-29 23:41:36', 'Asia/Kolkata'),
(32, '0DD020210729052337', 13, 1, 7, NULL, NULL, 2, 2, '2021-07-29 23:59:00', '104.35', '1.00', '4.50', '10.00', '11.95', '35.85', '45.00', '0.00', '45.00', '3.33', '102.02', NULL, NULL, '2021-07-29 05:31:52', 10, 1, '0.07', '47.95', '1.00', '11.95', '102.02', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-28 23:53:38', '2021-07-29 00:02:55', 'Asia/Kolkata'),
(33, '0SAD20210729055057', 13, 1, 16, NULL, NULL, 7, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 05:52:26', 10, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 00:20:58', '2021-07-29 00:23:37', 'Asia/Kolkata'),
(34, 'D00D20210729062620', 13, 1, 16, NULL, NULL, 6, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 06:26:48', 10, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 00:56:22', '2021-07-29 01:00:26', 'Asia/Kolkata'),
(35, 'AD0D20210729062646', 13, 1, 16, NULL, NULL, 7, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 06:34:46', 10, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 00:57:04', '2021-07-29 01:06:11', 'Asia/Kolkata'),
(36, '00S020210729062941', 13, 1, 16, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 06:37:55', 14, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 00:59:48', '2021-07-29 23:41:39', 'Asia/Kolkata'),
(37, '0A0D20210729063253', 13, 1, 16, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 07:04:05', 14, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 01:02:55', '2021-07-29 23:41:51', 'Asia/Kolkata'),
(38, 'DS0020210729063749', 13, 1, 16, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '329.63', '0.00', '4.50', '10.00', '142.61', '261.13', '45.00', '0.00', '45.00', '9.86', '319.77', NULL, NULL, '2021-07-29 07:44:21', 14, 1, '88.16', '177.61', '0.00', '142.61', '319.77', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-07-29 01:07:57', '2021-07-29 23:41:58', 'Asia/Kolkata'),
(39, '0AAD20210729074341', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 13:18:40', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 07:45:43', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 02:13:43', '2021-07-29 07:54:06', NULL),
(40, 'S0DA20210729081141', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 13:46:41', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 08:12:01', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 02:41:43', '2021-07-29 08:22:06', NULL),
(41, 'DADS20210729094034', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 15:15:33', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, NULL, 11, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 04:10:35', '2021-07-29 04:16:26', NULL),
(42, 'D00S20210729095033', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 11, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 04:20:35', '2021-07-29 04:20:58', 'Asia/Kolkata'),
(43, '00SD20210729095401', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 15:29:00', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, NULL, 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 04:24:03', '2021-07-29 23:42:00', NULL),
(44, '00SS20210729100736', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-29 15:42:35', '109.22', '0.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '105.75', 'fadsf', NULL, NULL, 11, 1, '-10.68', '43.89', '0.00', '8.89', '105.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 04:37:38', '2021-07-29 04:37:38', NULL),
(45, '0D0020210729102618', 19, 2, 6, NULL, NULL, 40, 2, '2021-07-29 23:59:00', '215.58', '0.00', '7.60', '10.00', '0.13', '0.38', '152.00', '0.00', '152.00', '6.55', '209.03', NULL, NULL, '2021-07-29 10:26:42', 10, 1, '-23.70', '35.13', '0.00', '0.13', '209.03', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-29 04:56:20', '2021-07-29 05:43:55', 'Asia/Kolkata'),
(46, 'DDSA20210729102656', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '215.58', '0.00', '7.60', '10.00', '0.13', '0.38', '152.00', '0.00', '152.00', '6.55', '209.03', NULL, NULL, NULL, 14, 1, '-23.70', '35.13', '0.00', '0.13', '209.03', '197.60', '7.60', '152.00', '15.00', '30.40', '15.20', '20.00', '2021-07-29 04:56:58', '2021-07-29 23:42:02', 'Asia/Kolkata'),
(47, 'AD0020210729104919', 19, 2, 6, NULL, NULL, 40, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-29 10:50:24', 11, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 05:19:21', '2021-07-29 05:57:24', 'Asia/Kolkata'),
(48, 'D0AD20210729110741', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-29 11:08:06', 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 05:37:43', '2021-07-29 23:42:09', 'Asia/Kolkata'),
(49, 'AS0S20210729111134', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-29 11:11:51', 11, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 05:41:36', '2021-07-29 05:59:00', 'Asia/Kolkata'),
(50, 'A0A020210729112927', 19, 2, 6, NULL, NULL, 40, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-29 11:29:44', 10, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 05:59:29', '2021-07-29 06:00:47', 'Asia/Kolkata'),
(51, 'DS0D20210729113114', 19, 2, 6, NULL, NULL, 40, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-29 11:31:58', 11, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 06:01:16', '2021-07-29 06:02:35', 'Asia/Kolkata'),
(52, '0S0D20210729114350', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 17:18:49', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 11:44:10', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 06:13:52', '2021-07-29 23:42:12', NULL),
(53, 'DSDD20210729115746', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 17:32:45', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 11:58:05', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 06:27:48', '2021-07-29 23:42:14', NULL),
(54, '0AAS20210729120020', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 06:30:22', '2021-07-29 23:42:16', 'Asia/Kolkata'),
(55, 'SDAD20210729120557', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-29 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-29 06:36:04', '2021-07-29 23:42:17', 'Asia/Kolkata'),
(56, 'DA0S20210729122825', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 18:03:25', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 12:28:43', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 06:58:28', '2021-07-29 23:42:19', NULL),
(57, 'DD0A20210729130257', 13, 1, 7, NULL, 7, NULL, 2, '2021-07-29 18:37:56', '118.39', '10.00', '6.30', '10.00', '11.95', '35.85', '56.70', '6.30', '63.00', '3.73', '124.66', 'fadsf', NULL, '2021-07-29 13:04:03', 14, 1, '-4.83', '56.95', '10.00', '11.95', '124.66', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 07:32:59', '2021-07-29 23:42:21', NULL),
(58, 'S0DS20210729134903', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-29 19:24:01', '109.22', '0.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '105.75', 'fadsf', NULL, NULL, 11, 1, '-10.68', '43.89', '0.00', '8.89', '105.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 08:19:04', '2021-07-29 08:19:06', NULL),
(59, 'D0SS20210730043244', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 10:07:43', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 04:33:22', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 23:02:57', '2021-07-30 00:44:20', NULL),
(60, '0SSS20210730045923', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 10:34:22', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 05:07:14', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 23:29:35', '2021-07-30 00:45:29', NULL),
(61, '00DS20210730052626', 1, 1, 1, NULL, 7, 7, 2, '2021-07-30 11:01:25', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 05:28:11', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-29 23:56:28', '2021-07-30 00:02:01', NULL),
(62, 'ADD020210730053400', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 11:08:59', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 05:34:35', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 00:04:02', '2021-07-30 00:12:05', NULL),
(63, 'A0AA20210730054256', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 11:17:55', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 05:43:25', 3, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 00:12:58', '2021-07-30 00:12:58', NULL),
(64, 'SDSS20210730054345', 1, 1, 1, NULL, 7, 7, 2, '2021-07-30 11:18:44', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 05:44:03', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 00:13:52', '2021-07-30 00:42:35', NULL),
(65, 'DA0D20210730061346', 1, 1, 1, NULL, 7, 7, 2, '2021-07-30 11:48:44', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 06:15:56', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 00:43:50', '2021-07-30 00:47:54', NULL),
(66, 'SDSS20210730070708', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-30 07:07:42', 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-30 01:37:10', '2021-07-30 13:03:05', 'Asia/Kolkata'),
(67, 'SAAS20210730071423', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-07-30 07:14:41', 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-30 01:44:27', '2021-07-30 13:03:07', 'Asia/Kolkata'),
(68, 'SASD20210730071935', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-30 01:49:37', '2021-07-30 13:03:08', 'Asia/Kolkata'),
(69, 'D0SS20210730075536', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, NULL, 14, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-30 02:25:37', '2021-07-30 13:03:09', 'Asia/Kolkata'),
(70, 'S0SS20210730075811', 19, 2, 6, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '318.18', '3.00', '11.40', '10.00', '0.13', '0.38', '228.00', '0.00', '228.00', '9.53', '311.65', NULL, NULL, '2021-07-30 12:54:19', 14, 1, '-22.88', '38.13', '3.00', '0.13', '311.65', '296.40', '11.40', '228.00', '15.00', '45.60', '22.80', '20.00', '2021-07-30 02:28:13', '2021-07-30 13:03:11', 'Asia/Kolkata'),
(71, '0AAA20210730080025', 42, 2, 19, NULL, NULL, NULL, 2, '2021-07-30 23:58:00', '113.15', '0.00', '3.80', '10.00', '0.18', '0.55', '76.00', '0.00', '76.00', '3.58', '109.57', NULL, NULL, '2021-07-30 08:00:53', 14, 1, '-24.41', '35.18', '0.00', '0.18', '109.57', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-30 02:30:28', '2021-07-30 13:03:13', 'Asia/Kolkata'),
(72, '00AS20210730102056', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 23:59:00', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', NULL, NULL, '2021-07-30 10:22:45', 14, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 04:50:57', '2021-07-30 13:04:11', 'Asia/Kolkata'),
(73, 'DD0020210730102914', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 16:04:13', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 10:30:14', 14, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 04:59:16', '2021-07-30 10:40:08', NULL),
(74, 'A0SA20210730105859', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 16:33:57', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 10:59:13', 14, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 05:29:01', '2021-07-30 11:09:06', NULL),
(75, 'SDSS20210730110155', 1, 1, 1, NULL, 7, NULL, 2, '2021-07-30 16:36:53', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-07-30 11:03:12', 14, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-07-30 05:31:57', '2021-07-30 11:12:06', NULL),
(76, '0AS020210731055713', 42, 2, 19, NULL, NULL, NULL, 2, '2021-08-10 23:59:00', '113.15', '0.00', '3.80', '10.00', '0.18', '0.55', '76.00', '0.00', '76.00', '3.58', '109.57', NULL, NULL, '2021-08-03 08:23:16', 14, 1, '-24.41', '35.18', '0.00', '0.18', '109.57', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-07-31 00:27:14', '2021-08-10 13:04:04', 'Asia/Kolkata'),
(77, 'DADS20210802054402', 1, 1, 1, NULL, NULL, NULL, 2, '2021-08-10 23:59:00', '95.18', '0.00', '4.50', '10.00', '8.89', '26.68', '45.00', '0.00', '45.00', '3.06', '92.12', NULL, NULL, '2021-08-02 05:44:33', 14, 1, '-5.77', '43.89', '0.00', '8.89', '92.12', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-02 00:14:10', '2021-08-03 00:00:08', 'Asia/Kolkata'),
(78, '00AS20210802054721', 1, 1, 1, NULL, 7, 7, 2, '2021-08-10 11:22:20', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-02 06:29:51', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-02 00:17:23', '2021-08-04 01:08:26', NULL),
(79, 'SDSA20210802063229', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-10 12:07:28', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-02 06:32:47', 13, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-02 01:02:31', '2021-08-02 23:54:08', NULL),
(80, '00DS20210802063957', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-10 12:14:55', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-02 06:40:24', 14, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-02 01:09:58', '2021-08-02 23:54:10', NULL),
(81, 'D0DA20210803054532', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 11:20:32', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 05:46:09', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 00:15:35', '2021-08-03 01:40:26', NULL),
(82, '0SSA20210803055554', 1, 1, 1, NULL, 7, 2, 2, '2021-08-03 11:30:52', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 05:56:10', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 00:25:56', '2021-08-03 00:49:53', NULL),
(83, 'DSSS20210803063007', 1, 1, 1, NULL, NULL, NULL, 1, '2021-08-04 23:59:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, '2021-08-03 06:30:21', 10, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-08-03 01:00:09', '2021-08-03 01:00:09', 'Asia/Kolkata'),
(84, 'DA0020210803070656', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 12:41:56', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 07:07:39', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 01:36:57', '2021-08-03 01:40:39', NULL),
(85, 'DDAD20210803071518', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 12:50:18', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 07:15:40', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 01:45:20', '2021-08-03 07:20:21', NULL),
(86, 'ADDA20210803071930', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 12:54:30', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 07:20:05', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 01:49:32', '2021-08-03 07:21:17', NULL),
(87, 'SSA020210803072509', 1, 1, 1, NULL, 7, 7, 2, '2021-08-03 13:00:09', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 07:25:23', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 01:55:11', '2021-08-03 01:58:19', NULL),
(88, 'ADAD20210803072956', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 13:04:56', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, NULL, 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 01:59:58', '2021-08-03 07:21:47', NULL),
(89, '0SSS20210803121132', 1, 1, 1, NULL, NULL, NULL, 1, '2021-08-04 23:59:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, NULL, 11, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-08-03 06:41:34', '2021-08-03 07:21:58', 'Asia/Kolkata'),
(90, 'SDD020210803121645', 1, 1, 1, NULL, NULL, NULL, 1, '2021-08-04 23:59:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, NULL, 11, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-08-03 06:46:47', '2021-08-03 07:22:26', 'Asia/Kolkata'),
(91, 'SS0D20210803122034', 1, 1, 1, NULL, NULL, NULL, 1, '2021-08-04 23:59:00', '68.50', '0.00', '4.50', '10.00', '0.00', '0.00', '45.00', '0.00', '45.00', '2.29', '66.21', NULL, NULL, NULL, 11, 1, '12.21', '0.00', '0.00', '0.00', '66.21', '54.00', '4.50', '45.00', '0.00', '4.50', '4.50', '0.00', '2021-08-03 06:50:35', '2021-08-03 07:22:34', 'Asia/Kolkata'),
(92, '0DA020210803122439', 1, 1, 1, NULL, 7, 7, 2, '2021-08-03 17:59:38', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 12:33:58', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 06:54:41', '2021-08-03 07:22:54', NULL),
(93, 'DA0D20210803123901', 1, 1, 1, NULL, 7, 7, 2, '2021-08-03 18:14:01', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 12:39:34', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 07:09:03', '2021-08-03 07:23:26', NULL),
(94, 'ADD020210803124655', 1, 1, 1, NULL, 7, 7, 2, '2021-08-03 18:21:55', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 12:47:07', 11, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 07:16:57', '2021-08-03 07:25:08', NULL),
(95, 'DDDD20210803125717', 1, 1, 1, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '95.18', '0.00', '4.50', '10.00', '8.89', '26.68', '45.00', '0.00', '45.00', '3.06', '92.12', NULL, NULL, '2021-08-03 12:57:52', 11, 1, '-5.77', '43.89', '0.00', '8.89', '92.12', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-03 07:27:19', '2021-08-03 07:37:34', 'Asia/Kolkata'),
(96, 'DAAA20210803130008', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-03 18:35:08', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-03 13:01:19', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-03 07:30:10', '2021-08-03 07:31:40', NULL),
(97, 'S0DS20210803130709', 1, 1, 1, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '95.18', '0.00', '4.50', '10.00', '8.89', '26.68', '45.00', '0.00', '45.00', '3.06', '92.12', NULL, NULL, '2021-08-03 13:12:58', 11, 1, '-5.77', '43.89', '0.00', '8.89', '92.12', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-03 07:37:11', '2021-08-04 05:03:25', 'Asia/Kolkata'),
(98, 'SAAD20210804053617', 19, 2, 6, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-08-04 05:36:33', 2, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-08-04 00:06:19', '2021-08-04 00:07:21', 'Asia/Kolkata'),
(99, 'AD0020210804063856', 19, 2, 6, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-08-04 06:39:10', 10, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-08-04 01:08:58', '2021-08-04 01:10:40', 'Asia/Kolkata'),
(100, 'DA0S20210804073912', 1, 1, 1, NULL, 7, 7, 2, '2021-08-04 13:14:12', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-04 07:39:54', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-04 02:09:20', '2021-08-04 02:13:36', NULL),
(101, 'D0AD20210804081322', 19, 2, 6, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '112.98', '0.00', '3.80', '10.00', '0.13', '0.38', '76.00', '0.00', '76.00', '3.58', '109.40', NULL, NULL, '2021-08-04 08:13:42', 10, 1, '-24.53', '35.13', '0.00', '0.13', '109.40', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-08-04 02:43:24', '2021-08-04 02:47:16', 'Asia/Kolkata'),
(102, 'ASDD20210804102201', 1, 1, 1, NULL, 7, 7, 2, '2021-08-04 23:59:00', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', NULL, NULL, '2021-08-04 10:23:12', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-04 04:52:03', '2021-08-04 05:07:10', 'Asia/Kolkata'),
(103, 'A0DS20210804105109', 1, 1, 1, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '105.98', '5.00', '5.40', '10.00', '8.89', '26.68', '54.00', '0.00', '54.00', '3.37', '107.61', NULL, NULL, '2021-08-04 10:55:46', 10, 1, '-5.18', '48.89', '5.00', '8.89', '107.61', '63.90', '5.40', '54.00', '15.00', '5.40', '5.40', '20.00', '2021-08-04 05:21:11', '2021-08-04 05:30:20', 'Asia/Kolkata'),
(104, 'DADD20210804113921', 1, 1, 1, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '95.18', '2.00', '4.50', '10.00', '8.89', '26.68', '45.00', '0.00', '45.00', '0.00', '97.18', NULL, NULL, '2021-08-04 11:39:58', 10, 1, '-2.71', '45.89', '2.00', '8.89', '97.18', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-04 06:09:29', '2021-08-04 06:17:22', 'Asia/Kolkata'),
(105, 'D0AA20210804114034', 1, 1, 1, NULL, 7, 7, 2, '2021-08-04 17:15:34', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '0.00', '119.22', 'fadsf', NULL, '2021-08-04 11:58:51', 10, 1, '-7.21', '53.89', '10.00', '8.89', '119.22', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-04 06:10:41', '2021-08-04 06:30:25', NULL),
(106, '0DAS20210804121146', 1, 1, 1, NULL, NULL, 7, 2, '2021-08-04 23:59:00', '47.48', '0.00', '0.90', '10.00', '8.89', '26.68', '9.00', '0.00', '9.00', '0.00', '47.48', NULL, NULL, '2021-08-04 12:12:08', 10, 1, '-6.31', '43.89', '0.00', '8.89', '47.48', '9.90', '0.90', '9.00', '15.00', '0.90', '0.90', '20.00', '2021-08-04 06:41:47', '2021-08-04 06:45:03', 'Asia/Kolkata'),
(107, 'DA0S20210804134223', 1, 1, 1, NULL, 7, NULL, 2, '2021-08-04 19:17:23', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '0.00', '119.22', 'fadsf', NULL, NULL, 14, 1, '-7.21', '53.89', '10.00', '8.89', '119.22', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-04 08:12:25', '2021-08-04 13:53:06', NULL),
(108, 'SSD020210805082305', 1, 1, 1, NULL, 7, 7, 2, '2021-08-05 13:58:06', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '0.00', '119.22', 'fadsf', NULL, '2021-08-05 09:37:17', 10, 1, '-7.21', '53.89', '10.00', '8.89', '119.22', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-05 02:53:07', '2021-08-05 04:09:39', NULL),
(109, 'A0AD20210805083241', 1, 1, 1, NULL, 7, 7, 2, '2021-08-05 14:07:41', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-05 09:51:40', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-05 03:02:42', '2021-08-05 04:22:53', NULL),
(110, 'D0DS20210805100047', 1, 1, 1, NULL, 7, 7, 2, '2021-08-05 15:35:48', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-05 10:01:06', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-05 04:30:49', '2021-08-05 04:33:34', NULL),
(111, '0DS020210805101132', 1, 1, 1, NULL, 7, 7, 2, '2021-08-05 15:46:32', '109.22', '10.00', '6.30', '10.00', '8.89', '26.68', '56.70', '6.30', '63.00', '3.47', '115.75', 'fadsf', NULL, '2021-08-05 10:12:15', 10, 1, '-10.68', '53.89', '10.00', '8.89', '115.75', '72.54', '6.30', '63.00', '15.00', '5.67', '5.67', '20.00', '2021-08-05 04:41:34', '2021-08-05 04:48:00', NULL),
(112, 'D0SA20210811092637', 58, 2, 22, NULL, NULL, NULL, 2, '2021-08-11 23:59:00', '113.53', '0.00', '3.80', '10.00', '0.31', '0.93', '76.00', '0.00', '76.00', '3.59', '109.94', NULL, NULL, '2021-08-11 09:28:23', 3, 1, '-24.17', '35.31', '0.00', '0.31', '109.94', '98.80', '3.80', '76.00', '15.00', '15.20', '7.60', '20.00', '2021-08-11 03:56:38', '2021-08-11 03:56:38', 'Asia/Kolkata'),
(113, 'AS0A20210813075345', 1, 1, 1, NULL, NULL, NULL, 2, '2021-08-14 23:59:00', '95.18', '0.00', '4.50', '10.00', '8.89', '26.68', '45.00', '0.00', '45.00', '3.06', '92.12', NULL, NULL, NULL, 11, 1, '-5.77', '43.89', '0.00', '8.89', '92.12', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-13 02:23:47', '2021-08-14 02:07:38', 'Asia/Kolkata'),
(114, 'DDAD20210818105043', 1, 1, 26, NULL, NULL, NULL, 2, '2021-08-18 23:59:00', '131.57', '0.00', '4.50', '10.00', '21.02', '63.07', '45.00', '0.00', '45.00', '4.12', '127.45', NULL, NULL, NULL, 14, 1, '17.43', '56.02', '0.00', '21.02', '127.45', '54.00', '4.50', '45.00', '15.00', '4.50', '4.50', '20.00', '2021-08-18 05:20:45', '2021-08-18 13:04:04', 'Asia/Kolkata');

-- --------------------------------------------------------

--
-- Table structure for table `booking_notifications`
--

CREATE TABLE `booking_notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` bigint(20) UNSIGNED NOT NULL,
  `deliveryboy_id` bigint(20) UNSIGNED NOT NULL,
  `deliveryboy_rating` decimal(4,2) NOT NULL,
  `notification_message` text COLLATE utf8_unicode_ci NOT NULL,
  `deliveryboy_accept_status` int(11) NOT NULL,
  `expiration_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_notifications`
--

INSERT INTO `booking_notifications` (`id`, `booking_id`, `deliveryboy_id`, `deliveryboy_rating`, `notification_message`, `deliveryboy_accept_status`, `expiration_time`, `created_at`, `updated_at`) VALUES
(45, 78, 7, '3.00', 'You got the order request for the booking code #00AS20210802054721 ', 1, '2021-08-04 06:21:06', '2021-08-03 00:41:06', '2021-08-04 01:06:44'),
(46, 81, 2, '3.00', 'You got the order request for the booking code #D0DA20210803054532 ', 0, '2021-08-03 07:10:26', '2021-08-03 00:41:06', '2021-08-03 01:40:26'),
(47, 82, 2, '3.00', 'You got the order request for the booking code #0SSA20210803055554 ', 1, '2021-08-03 06:21:06', '2021-08-03 00:41:06', '2021-08-03 00:49:27'),
(48, 78, 6, '1.00', 'You got the order request for the booking code #00AS20210802054721 ', 0, '2021-08-04 06:36:44', '2021-08-03 00:41:11', '2021-08-04 01:06:44'),
(49, 81, 6, '1.00', 'You got the order request for the booking code #D0DA20210803054532 ', 0, '2021-08-03 07:10:26', '2021-08-03 00:41:11', '2021-08-03 01:40:26'),
(50, 82, 6, '1.00', 'You got the order request for the booking code #0SSA20210803055554 ', 0, '2021-08-03 06:21:12', '2021-08-03 00:41:12', '2021-08-03 00:41:12'),
(51, 78, 3, '0.00', 'You got the order request for the booking code #00AS20210802054721 ', 0, '2021-08-04 06:36:44', '2021-08-03 01:37:02', '2021-08-04 01:06:44'),
(52, 81, 3, '0.00', 'You got the order request for the booking code #D0DA20210803054532 ', 0, '2021-08-03 07:10:26', '2021-08-03 01:37:03', '2021-08-03 01:40:26'),
(53, 84, 3, '0.00', 'You got the order request for the booking code #DA0020210803070656 ', 0, '2021-08-03 07:10:39', '2021-08-03 01:38:03', '2021-08-03 01:40:39'),
(54, 85, 2, '3.00', 'You got the order request for the booking code #DDAD20210803071518 ', 0, '2021-08-03 07:26:02', '2021-08-03 01:46:02', '2021-08-03 01:46:02'),
(55, 85, 7, '5.00', 'You got the order request for the booking code #DDAD20210803071518 ', 0, '2021-08-03 07:26:03', '2021-08-03 01:46:03', '2021-08-03 01:46:03'),
(56, 85, 6, '1.00', 'You got the order request for the booking code #DDAD20210803071518 ', 0, '2021-08-03 07:28:03', '2021-08-03 01:48:03', '2021-08-03 01:48:03'),
(57, 85, 3, '0.00', 'You got the order request for the booking code #DDAD20210803071518 ', 0, '2021-08-03 07:29:03', '2021-08-03 01:49:03', '2021-08-03 01:49:03'),
(58, 86, 6, '1.00', 'You got the order request for the booking code #ADDA20210803071930 ', 0, '2021-08-03 07:31:02', '2021-08-03 01:51:02', '2021-08-03 01:51:02'),
(59, 86, 3, '0.00', 'You got the order request for the booking code #ADDA20210803071930 ', 0, '2021-08-03 07:32:03', '2021-08-03 01:52:03', '2021-08-03 01:52:03'),
(60, 87, 7, '5.00', 'You got the order request for the booking code #SSA020210803072509 ', 1, '2021-08-03 07:36:03', '2021-08-03 01:56:03', '2021-08-03 01:57:29'),
(61, 87, 2, '2.90', 'You got the order request for the booking code #SSA020210803072509 ', 0, '2021-08-03 07:37:03', '2021-08-03 01:57:03', '2021-08-03 01:57:03'),
(62, 76, 40, '0.00', 'You got the order request for the booking code #0AS020210731055713 ', 0, '2021-08-03 08:06:02', '2021-08-03 02:26:02', '2021-08-03 02:26:02'),
(63, 92, 7, '5.00', 'You got the order request for the booking code #0DA020210803122439 ', 1, '2021-08-03 12:44:03', '2021-08-03 07:04:03', '2021-08-03 07:04:16'),
(64, 93, 7, '5.00', 'You got the order request for the booking code #DA0D20210803123901 ', 1, '2021-08-03 12:50:05', '2021-08-03 07:10:05', '2021-08-03 07:10:07'),
(65, 94, 7, '5.00', 'You got the order request for the booking code #ADD020210803124655 ', 1, '2021-08-03 12:58:02', '2021-08-03 07:18:02', '2021-08-03 07:18:07'),
(66, 95, 7, '5.00', 'You got the order request for the booking code #DDDD20210803125717 ', 1, '2021-08-03 13:08:02', '2021-08-03 07:28:02', '2021-08-03 07:28:12'),
(67, 97, 7, '5.00', 'You got the order request for the booking code #S0DS20210803130709 ', 1, '2021-08-04 10:33:32', '2021-08-03 07:43:02', '2021-08-04 05:03:32'),
(68, 98, 7, '5.00', 'You got the order request for the booking code #SAAD20210804053617 ', 1, '2021-08-04 05:47:03', '2021-08-04 00:07:03', '2021-08-04 00:07:21'),
(69, 99, 7, '5.00', 'You got the order request for the booking code #AD0020210804063856 ', 1, '2021-08-04 06:50:03', '2021-08-04 01:10:03', '2021-08-04 01:10:14'),
(70, 100, 7, '5.00', 'You got the order request for the booking code #DA0S20210804073912 ', 1, '2021-08-04 07:51:03', '2021-08-04 02:11:03', '2021-08-04 02:12:19'),
(71, 101, 7, '5.00', 'You got the order request for the booking code #D0AD20210804081322 ', 1, '2021-08-04 08:24:03', '2021-08-04 02:44:03', '2021-08-04 02:44:27'),
(72, 102, 7, '5.00', 'You got the order request for the booking code #ASDD20210804102201 ', 1, '2021-08-04 10:43:03', '2021-08-04 05:03:03', '2021-08-04 05:06:42'),
(73, 103, 7, '5.00', 'You got the order request for the booking code #A0DS20210804105109 ', 1, '2021-08-04 11:06:02', '2021-08-04 05:26:02', '2021-08-04 05:26:11'),
(74, 104, 7, '5.00', 'You got the order request for the booking code #DADD20210804113921 ', 1, '2021-08-04 11:50:05', '2021-08-04 06:10:05', '2021-08-04 06:16:57'),
(75, 105, 7, '5.00', 'You got the order request for the booking code #D0AA20210804114034 ', 1, '2021-08-04 12:09:03', '2021-08-04 06:29:03', '2021-08-04 06:29:09'),
(76, 106, 7, '5.00', 'You got the order request for the booking code #0DAS20210804121146 ', 1, '2021-08-04 12:23:03', '2021-08-04 06:43:03', '2021-08-04 06:44:37'),
(77, 108, 7, '5.00', 'You got the order request for the booking code #SSD020210805082305 ', 1, '2021-08-05 09:48:03', '2021-08-05 04:08:03', '2021-08-05 04:08:44'),
(78, 109, 7, '5.00', 'You got the order request for the booking code #A0AD20210805083241 ', 1, '2021-08-05 10:02:03', '2021-08-05 04:22:03', '2021-08-05 04:22:36'),
(79, 110, 7, '5.00', 'You got the order request for the booking code #D0DS20210805100047 ', 1, '2021-08-05 10:12:03', '2021-08-05 04:32:03', '2021-08-05 04:33:15'),
(80, 111, 7, '5.00', 'You got the order request for the booking code #0DS020210805101132 ', 1, '2021-08-05 10:23:02', '2021-08-05 04:43:02', '2021-08-05 04:47:45');

-- --------------------------------------------------------

--
-- Table structure for table `booking_reject_reason`
--

CREATE TABLE `booking_reject_reason` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_reject_reason`
--

INSERT INTO `booking_reject_reason` (`id`, `booking_id`, `reason`, `created_at`, `updated_at`) VALUES
(1, 4, 'desawq nbbgh mnhbhj nbhgvgf bhgvfc bhgv', '2021-07-26 07:33:29', '2021-07-26 07:33:29'),
(2, 6, 'desawq nbbgh mnhbhj nbhgvgf bhgvfc bhgv', '2021-07-27 05:37:39', '2021-07-27 05:37:39'),
(3, 17, 'no a valid shop', '2021-07-28 01:14:22', '2021-07-28 01:14:22'),
(4, 18, 'no a valid shop', '2021-07-28 01:15:58', '2021-07-28 01:15:58'),
(5, 19, 'no a valid shop', '2021-07-28 01:16:50', '2021-07-28 01:16:50'),
(6, 20, 'no vendor available', '2021-07-28 01:18:04', '2021-07-28 01:18:04'),
(7, 21, 'no vendor available', '2021-07-28 02:15:44', '2021-07-28 02:15:44'),
(8, 22, 'no a valid shop', '2021-07-28 02:17:33', '2021-07-28 02:17:33'),
(9, 24, 'no a valid shop', '2021-07-28 02:47:09', '2021-07-28 02:47:09'),
(10, 25, 'no a valid shop', '2021-07-28 04:50:06', '2021-07-28 04:50:06'),
(11, 26, 'no a valid shop', '2021-07-28 05:06:33', '2021-07-28 05:06:33'),
(12, 27, 'no a valid shop', '2021-07-28 05:07:34', '2021-07-28 05:07:34'),
(13, 42, 'Vsvsvzvxvvxcczczcczczczcczczczccz', '2021-07-29 04:20:58', '2021-07-29 04:20:58'),
(14, 44, 'this booking is not required', '2021-07-29 04:37:38', '2021-07-29 04:37:38'),
(15, 47, 'kgjggnfjfhfccbbcbcbccbbcbcbfcbbxbcvcvcgxhchchchccbbcchvhchchc', '2021-07-29 05:57:24', '2021-07-29 05:57:24'),
(16, 49, 'Fhsjgxhfzfhxhxjfxfjxjfzcn, Vnxgnxgjxgnxjgxgjxgjxgjxgjxhfxjgx', '2021-07-29 05:59:00', '2021-07-29 05:59:00'),
(17, 51, 'Hgrhdhgdgdgxgdgxggxvxgxvdvvxvxvxgxhhdhdhdhgdhdhdhgdgdgdgdh', '2021-07-29 06:02:35', '2021-07-29 06:02:35'),
(18, 58, 'this booking is not required', '2021-07-29 08:19:06', '2021-07-29 08:19:06'),
(19, 61, 'Jebend S S Shs D D D D D D D D', '2021-07-30 00:02:01', '2021-07-30 00:02:01'),
(20, 62, 'Whwhsbbssss ssnss sj sbsbs ss Ns ss', '2021-07-30 00:12:05', '2021-07-30 00:12:05'),
(21, 63, 'fasfgasdfg sdgadfsgag', '2021-07-30 00:13:24', '2021-07-30 00:13:24'),
(22, 59, 'Whwhsbbssss ssnss sj sbsbs ss Ns ss', '2021-07-30 00:44:20', '2021-07-30 00:44:20'),
(23, 60, 'Whwhsbbssss ssnss sj sbsbs ss Ns ss', '2021-07-30 00:45:29', '2021-07-30 00:45:29'),
(24, 65, 'Whwhsbbssss ssnss sj sbsbs ss Ns ss', '2021-07-30 00:47:55', '2021-07-30 00:47:55'),
(25, 81, 'Kdnd  D de dje Xkd D  S  Snshttps://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 01:40:26', '2021-08-03 01:40:26'),
(26, 84, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 01:40:39', '2021-08-03 01:40:39'),
(27, 85, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:20:21', '2021-08-03 07:20:21'),
(28, 86, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:21:17', '2021-08-03 07:21:17'),
(29, 88, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:21:47', '2021-08-03 07:21:47'),
(30, 89, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:21:58', '2021-08-03 07:21:58'),
(31, 90, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:22:26', '2021-08-03 07:22:26'),
(32, 91, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:22:35', '2021-08-03 07:22:35'),
(33, 92, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:22:54', '2021-08-03 07:22:54'),
(34, 93, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:23:26', '2021-08-03 07:23:26'),
(35, 94, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:25:08', '2021-08-03 07:25:08'),
(36, 95, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-03 07:37:34', '2021-08-03 07:37:34'),
(37, 97, 'https://drive.google.com/file/d/1JOJZjg1KZeE-cmnEAktlQgxIOAeuP593/view?usp=sharing', '2021-08-04 05:03:25', '2021-08-04 05:03:25'),
(38, 112, 'mmm', '2021-08-11 03:58:19', '2021-08-11 03:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `booking_tracking_history`
--

CREATE TABLE `booking_tracking_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bookingId` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `status_value` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking_tracking_history`
--

INSERT INTO `booking_tracking_history` (`id`, `bookingId`, `reason`, `value`, `status_value`, `userId`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-22 06:41:10', '2021-07-22 06:41:10'),
(2, 1, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-22 06:41:27', '2021-07-22 06:41:27'),
(3, 2, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-22 06:44:52', '2021-07-22 06:44:52'),
(4, 2, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-22 06:45:28', '2021-07-22 06:45:28'),
(5, 3, 'ORDER_PLACED', 'ORDER_PLACED', 1, 5, 1, '2021-07-23 06:26:09', '2021-07-23 06:26:09'),
(6, 3, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 5, 1, '2021-07-23 13:05:03', '2021-07-23 13:05:03'),
(7, 4, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-26 07:29:33', '2021-07-26 07:29:33'),
(8, 5, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-26 07:32:17', '2021-07-26 07:32:17'),
(9, 4, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-26 07:33:31', '2021-07-26 07:33:31'),
(10, 5, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-26 08:09:28', '2021-07-26 08:09:28'),
(11, 5, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 12, 1, '2021-07-26 08:09:33', '2021-07-26 08:09:33'),
(12, 5, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 2, 1, '2021-07-26 08:10:34', '2021-07-26 08:10:34'),
(13, 5, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 2, 1, '2021-07-26 08:10:56', '2021-07-26 08:10:56'),
(14, 5, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 2, 1, '2021-07-26 08:11:34', '2021-07-26 08:11:34'),
(15, 5, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 2, 1, '2021-07-26 08:11:50', '2021-07-26 08:11:50'),
(16, 5, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 2, 1, '2021-07-26 08:12:44', '2021-07-26 08:12:44'),
(17, 5, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 2, 1, '2021-07-26 08:13:22', '2021-07-26 08:13:22'),
(18, 6, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-27 05:21:31', '2021-07-27 05:21:31'),
(19, 6, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-27 05:37:41', '2021-07-27 05:37:41'),
(20, 7, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-27 05:57:19', '2021-07-27 05:57:19'),
(21, 8, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-27 06:00:43', '2021-07-27 06:00:43'),
(22, 8, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 12, 1, '2021-07-27 06:01:03', '2021-07-27 06:01:03'),
(23, 9, 'ORDER_PLACED', 'ORDER_PLACED', 1, 12, 1, '2021-07-27 06:01:07', '2021-07-27 06:01:07'),
(24, 9, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 12, 1, '2021-07-27 06:02:03', '2021-07-27 06:02:03'),
(25, 7, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-27 06:23:09', '2021-07-27 06:23:09'),
(26, 7, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 12, 1, '2021-07-27 06:34:55', '2021-07-27 06:34:55'),
(27, 10, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-27 07:23:58', '2021-07-27 07:23:58'),
(28, 10, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-27 07:24:03', '2021-07-27 07:24:03'),
(29, 11, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-27 07:43:34', '2021-07-27 07:43:34'),
(30, 11, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-27 07:44:04', '2021-07-27 07:44:04'),
(31, 12, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-27 07:45:33', '2021-07-27 07:45:33'),
(32, 12, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-27 07:46:03', '2021-07-27 07:46:03'),
(33, 13, 'ORDER_PLACED', 'ORDER_PLACED', 1, 23, 1, '2021-07-28 00:14:48', '2021-07-28 00:14:48'),
(34, 13, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 23, 1, '2021-07-28 00:15:04', '2021-07-28 00:15:04'),
(35, 14, 'ORDER_PLACED', 'ORDER_PLACED', 1, 23, 1, '2021-07-28 00:15:58', '2021-07-28 00:15:58'),
(36, 14, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 23, 1, '2021-07-28 00:16:03', '2021-07-28 00:16:03'),
(37, 15, 'ORDER_PLACED', 'ORDER_PLACED', 1, 23, 1, '2021-07-28 00:16:30', '2021-07-28 00:16:30'),
(38, 15, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 23, 1, '2021-07-28 00:16:52', '2021-07-28 00:16:52'),
(39, 16, 'ORDER_PLACED', 'ORDER_PLACED', 1, 23, 1, '2021-07-28 00:17:33', '2021-07-28 00:17:33'),
(40, 16, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-28 00:18:22', '2021-07-28 00:18:22'),
(41, 16, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 23, 1, '2021-07-28 00:18:25', '2021-07-28 00:18:25'),
(42, 17, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 01:05:16', '2021-07-28 01:05:16'),
(43, 17, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 01:14:29', '2021-07-28 01:14:29'),
(44, 18, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 01:15:42', '2021-07-28 01:15:42'),
(45, 18, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 01:16:05', '2021-07-28 01:16:05'),
(46, 19, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 01:16:23', '2021-07-28 01:16:23'),
(47, 19, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 01:16:52', '2021-07-28 01:16:52'),
(48, 20, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 01:17:42', '2021-07-28 01:17:42'),
(49, 20, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 01:18:06', '2021-07-28 01:18:06'),
(50, 21, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 02:14:01', '2021-07-28 02:14:01'),
(51, 21, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 02:15:46', '2021-07-28 02:15:46'),
(52, 22, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 02:17:13', '2021-07-28 02:17:13'),
(53, 22, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 02:17:34', '2021-07-28 02:17:34'),
(54, 23, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 02:35:51', '2021-07-28 02:35:51'),
(55, 24, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 02:46:31', '2021-07-28 02:46:31'),
(56, 24, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 02:47:11', '2021-07-28 02:47:11'),
(57, 25, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-28 04:49:30', '2021-07-28 04:49:30'),
(58, 25, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-28 04:50:07', '2021-07-28 04:50:07'),
(59, 25, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 04:50:19', '2021-07-28 04:50:19'),
(60, 26, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-28 05:05:51', '2021-07-28 05:05:51'),
(61, 26, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 05:06:46', '2021-07-28 05:06:46'),
(62, 26, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 05:06:46', '2021-07-28 05:06:46'),
(63, 27, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-28 05:07:15', '2021-07-28 05:07:15'),
(64, 27, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-28 05:07:37', '2021-07-28 05:07:37'),
(65, 28, 'ORDER_PLACED', 'ORDER_PLACED', 1, 17, 1, '2021-07-28 08:14:04', '2021-07-28 08:14:04'),
(66, 29, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-28 08:22:20', '2021-07-28 08:22:20'),
(67, 30, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-28 08:24:10', '2021-07-28 08:24:10'),
(68, 23, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 24, 1, '2021-07-28 13:04:03', '2021-07-28 13:04:03'),
(69, 28, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 17, 1, '2021-07-28 13:04:05', '2021-07-28 13:04:05'),
(70, 29, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-28 13:04:06', '2021-07-28 13:04:06'),
(71, 30, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-28 13:04:09', '2021-07-28 13:04:09'),
(72, 31, 'ORDER_PLACED', 'ORDER_PLACED', 1, 24, 1, '2021-07-28 23:25:07', '2021-07-28 23:25:07'),
(73, 32, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-28 23:53:38', '2021-07-28 23:53:38'),
(74, 32, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 00:01:52', '2021-07-29 00:01:52'),
(75, 32, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 2, 1, '2021-07-29 00:02:19', '2021-07-29 00:02:19'),
(76, 32, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 2, 1, '2021-07-29 00:02:27', '2021-07-29 00:02:27'),
(77, 32, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 2, 1, '2021-07-29 00:02:31', '2021-07-29 00:02:31'),
(78, 32, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 2, 1, '2021-07-29 00:02:35', '2021-07-29 00:02:35'),
(79, 32, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 2, 1, '2021-07-29 00:02:42', '2021-07-29 00:02:42'),
(80, 32, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 2, 1, '2021-07-29 00:02:57', '2021-07-29 00:02:57'),
(81, 33, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 00:20:58', '2021-07-29 00:20:58'),
(82, 33, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 00:22:26', '2021-07-29 00:22:26'),
(83, 33, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 00:22:47', '2021-07-29 00:22:47'),
(84, 33, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-07-29 00:23:08', '2021-07-29 00:23:08'),
(85, 33, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-07-29 00:23:17', '2021-07-29 00:23:17'),
(86, 33, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-07-29 00:23:19', '2021-07-29 00:23:19'),
(87, 33, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-07-29 00:23:21', '2021-07-29 00:23:21'),
(88, 33, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-07-29 00:23:25', '2021-07-29 00:23:25'),
(89, 33, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-07-29 00:23:39', '2021-07-29 00:23:39'),
(90, 34, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 00:56:22', '2021-07-29 00:56:22'),
(91, 34, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 00:56:49', '2021-07-29 00:56:49'),
(92, 34, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 00:57:00', '2021-07-29 00:57:00'),
(93, 35, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 00:57:04', '2021-07-29 00:57:04'),
(94, 34, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 6, 1, '2021-07-29 00:58:22', '2021-07-29 00:58:22'),
(95, 34, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 6, 1, '2021-07-29 00:59:04', '2021-07-29 00:59:04'),
(96, 34, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 6, 1, '2021-07-29 00:59:06', '2021-07-29 00:59:06'),
(97, 34, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 6, 1, '2021-07-29 00:59:24', '2021-07-29 00:59:24'),
(98, 36, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 00:59:48', '2021-07-29 00:59:48'),
(99, 34, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 6, 1, '2021-07-29 01:00:10', '2021-07-29 01:00:10'),
(100, 34, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 6, 1, '2021-07-29 01:00:55', '2021-07-29 01:00:55'),
(101, 34, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 6, 1, '2021-07-29 01:01:01', '2021-07-29 01:01:01'),
(102, 37, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 01:02:55', '2021-07-29 01:02:55'),
(103, 35, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 01:04:46', '2021-07-29 01:04:46'),
(104, 35, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 01:04:55', '2021-07-29 01:04:55'),
(105, 35, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-07-29 01:05:25', '2021-07-29 01:05:25'),
(106, 35, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-07-29 01:05:34', '2021-07-29 01:05:34'),
(107, 35, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-07-29 01:05:39', '2021-07-29 01:05:39'),
(108, 35, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-07-29 01:05:54', '2021-07-29 01:05:54'),
(109, 35, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-07-29 01:05:58', '2021-07-29 01:05:58'),
(110, 35, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-07-29 01:06:14', '2021-07-29 01:06:14'),
(111, 36, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 01:07:55', '2021-07-29 01:07:55'),
(112, 38, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 01:07:57', '2021-07-29 01:07:57'),
(113, 36, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 01:08:06', '2021-07-29 01:08:06'),
(114, 37, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 01:34:05', '2021-07-29 01:34:05'),
(115, 37, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 01:34:09', '2021-07-29 01:34:09'),
(116, 39, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 02:13:43', '2021-07-29 02:13:43'),
(117, 38, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 02:14:21', '2021-07-29 02:14:21'),
(118, 38, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 02:14:28', '2021-07-29 02:14:28'),
(119, 39, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 02:15:43', '2021-07-29 02:15:43'),
(120, 39, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 02:15:48', '2021-07-29 02:15:48'),
(121, 40, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 02:41:43', '2021-07-29 02:41:43'),
(122, 40, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 02:42:01', '2021-07-29 02:42:01'),
(123, 40, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 02:42:05', '2021-07-29 02:42:05'),
(124, 41, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 04:10:35', '2021-07-29 04:10:35'),
(125, 42, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 04:20:35', '2021-07-29 04:20:35'),
(126, 42, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 19, 1, '2021-07-29 04:21:01', '2021-07-29 04:21:01'),
(127, 43, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 04:24:03', '2021-07-29 04:24:03'),
(128, 44, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-29 04:37:38', '2021-07-29 04:37:38'),
(129, 44, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-29 04:37:42', '2021-07-29 04:37:42'),
(130, 45, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 04:56:20', '2021-07-29 04:56:20'),
(131, 45, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 04:56:43', '2021-07-29 04:56:43'),
(132, 46, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 04:56:58', '2021-07-29 04:56:58'),
(133, 47, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 05:19:21', '2021-07-29 05:19:21'),
(134, 47, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 05:20:24', '2021-07-29 05:20:24'),
(135, 48, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 05:37:43', '2021-07-29 05:37:43'),
(136, 48, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 05:38:06', '2021-07-29 05:38:06'),
(137, 48, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 19, 1, '2021-07-29 05:40:11', '2021-07-29 05:40:11'),
(138, 49, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 05:41:36', '2021-07-29 05:41:36'),
(139, 49, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 05:41:51', '2021-07-29 05:41:51'),
(140, 45, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 40, 1, '2021-07-29 05:42:25', '2021-07-29 05:42:25'),
(141, 45, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 40, 1, '2021-07-29 05:43:12', '2021-07-29 05:43:12'),
(142, 45, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 40, 1, '2021-07-29 05:43:21', '2021-07-29 05:43:21'),
(143, 45, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 40, 1, '2021-07-29 05:43:29', '2021-07-29 05:43:29'),
(144, 45, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 40, 1, '2021-07-29 05:43:38', '2021-07-29 05:43:38'),
(145, 45, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 40, 1, '2021-07-29 05:43:56', '2021-07-29 05:43:56'),
(146, 47, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 40, 1, '2021-07-29 05:44:32', '2021-07-29 05:44:32'),
(147, 47, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 19, 1, '2021-07-29 05:57:32', '2021-07-29 05:57:32'),
(148, 49, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 19, 1, '2021-07-29 05:59:03', '2021-07-29 05:59:03'),
(149, 50, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 05:59:29', '2021-07-29 05:59:29'),
(150, 50, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 05:59:44', '2021-07-29 05:59:44'),
(151, 50, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 40, 1, '2021-07-29 06:00:16', '2021-07-29 06:00:16'),
(152, 50, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 40, 1, '2021-07-29 06:00:23', '2021-07-29 06:00:23'),
(153, 50, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 40, 1, '2021-07-29 06:00:28', '2021-07-29 06:00:28'),
(154, 50, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 40, 1, '2021-07-29 06:00:30', '2021-07-29 06:00:30'),
(155, 50, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 40, 1, '2021-07-29 06:00:36', '2021-07-29 06:00:36'),
(156, 50, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 40, 1, '2021-07-29 06:00:48', '2021-07-29 06:00:48'),
(157, 51, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 06:01:16', '2021-07-29 06:01:16'),
(158, 51, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 06:01:58', '2021-07-29 06:01:58'),
(159, 51, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 40, 1, '2021-07-29 06:02:10', '2021-07-29 06:02:10'),
(160, 51, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 19, 1, '2021-07-29 06:02:37', '2021-07-29 06:02:37'),
(161, 52, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 06:13:52', '2021-07-29 06:13:52'),
(162, 52, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 06:14:10', '2021-07-29 06:14:10'),
(163, 52, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 06:14:21', '2021-07-29 06:14:21'),
(164, 53, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 06:27:48', '2021-07-29 06:27:48'),
(165, 53, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 06:28:05', '2021-07-29 06:28:05'),
(166, 53, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 06:28:09', '2021-07-29 06:28:09'),
(167, 54, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 06:30:22', '2021-07-29 06:30:22'),
(168, 55, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-29 06:36:04', '2021-07-29 06:36:04'),
(169, 56, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 06:58:29', '2021-07-29 06:58:29'),
(170, 56, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 06:58:43', '2021-07-29 06:58:43'),
(171, 56, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 06:58:54', '2021-07-29 06:58:54'),
(172, 57, 'ORDER_PLACED', 'ORDER_PLACED', 1, 13, 1, '2021-07-29 07:32:59', '2021-07-29 07:32:59'),
(173, 57, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 07:34:03', '2021-07-29 07:34:03'),
(174, 57, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 13, 1, '2021-07-29 07:34:08', '2021-07-29 07:34:08'),
(175, 39, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 07:54:04', '2021-07-29 07:54:04'),
(176, 58, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-29 08:19:04', '2021-07-29 08:19:04'),
(177, 58, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-29 08:19:08', '2021-07-29 08:19:08'),
(178, 40, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 08:22:04', '2021-07-29 08:22:04'),
(179, 59, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-29 23:02:57', '2021-07-29 23:02:57'),
(180, 59, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 23:03:22', '2021-07-29 23:03:22'),
(181, 59, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-29 23:05:22', '2021-07-29 23:05:22'),
(182, 60, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-29 23:29:35', '2021-07-29 23:29:35'),
(183, 60, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 23:37:14', '2021-07-29 23:37:14'),
(184, 60, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-29 23:37:17', '2021-07-29 23:37:17'),
(185, 31, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 24, 1, '2021-07-29 23:41:35', '2021-07-29 23:41:35'),
(186, 36, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:41:37', '2021-07-29 23:41:37'),
(187, 37, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:41:39', '2021-07-29 23:41:39'),
(188, 38, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:41:51', '2021-07-29 23:41:51'),
(189, 43, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:41:58', '2021-07-29 23:41:58'),
(190, 46, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:00', '2021-07-29 23:42:00'),
(191, 48, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:02', '2021-07-29 23:42:02'),
(192, 48, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:03', '2021-07-29 23:42:03'),
(193, 52, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:09', '2021-07-29 23:42:09'),
(194, 52, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:09', '2021-07-29 23:42:09'),
(195, 53, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:11', '2021-07-29 23:42:11'),
(196, 53, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:12', '2021-07-29 23:42:12'),
(197, 54, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:13', '2021-07-29 23:42:13'),
(198, 54, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:14', '2021-07-29 23:42:14'),
(199, 55, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:15', '2021-07-29 23:42:15'),
(200, 55, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-29 23:42:16', '2021-07-29 23:42:16'),
(201, 56, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:17', '2021-07-29 23:42:17'),
(202, 56, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:17', '2021-07-29 23:42:17'),
(203, 57, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:19', '2021-07-29 23:42:19'),
(204, 57, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 13, 1, '2021-07-29 23:42:19', '2021-07-29 23:42:19'),
(205, 61, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-29 23:56:28', '2021-07-29 23:56:28'),
(206, 61, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-29 23:58:11', '2021-07-29 23:58:11'),
(207, 61, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-29 23:58:15', '2021-07-29 23:58:15'),
(208, 61, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-07-29 23:59:57', '2021-07-29 23:59:57'),
(209, 61, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-30 00:02:03', '2021-07-30 00:02:03'),
(210, 62, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 00:04:02', '2021-07-30 00:04:02'),
(211, 62, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 00:04:35', '2021-07-30 00:04:35'),
(212, 62, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 00:04:38', '2021-07-30 00:04:38'),
(213, 62, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-30 00:12:09', '2021-07-30 00:12:09'),
(214, 63, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 00:12:58', '2021-07-30 00:12:58'),
(215, 63, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-07-30 00:13:25', '2021-07-30 00:13:25'),
(216, 64, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 00:13:52', '2021-07-30 00:13:52'),
(217, 64, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 00:14:03', '2021-07-30 00:14:03'),
(218, 64, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 00:14:07', '2021-07-30 00:14:07'),
(219, 64, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 2, 1, '2021-07-30 00:24:17', '2021-07-30 00:24:17'),
(220, 64, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-07-30 00:24:45', '2021-07-30 00:24:45'),
(221, 64, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-07-30 00:41:37', '2021-07-30 00:41:37'),
(222, 64, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-07-30 00:41:45', '2021-07-30 00:41:45'),
(223, 64, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-07-30 00:41:48', '2021-07-30 00:41:48'),
(224, 64, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-07-30 00:41:59', '2021-07-30 00:41:59'),
(225, 64, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-07-30 00:42:43', '2021-07-30 00:42:43'),
(226, 65, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 00:43:50', '2021-07-30 00:43:50'),
(227, 59, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-30 00:44:31', '2021-07-30 00:44:31'),
(228, 65, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 00:45:56', '2021-07-30 00:45:56'),
(229, 65, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 00:46:05', '2021-07-30 00:46:05'),
(230, 60, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-30 00:46:13', '2021-07-30 00:46:13'),
(231, 65, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-07-30 00:47:28', '2021-07-30 00:47:28'),
(232, 65, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-07-30 00:47:57', '2021-07-30 00:47:57'),
(233, 66, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-30 01:37:10', '2021-07-30 01:37:10'),
(234, 66, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 01:37:42', '2021-07-30 01:37:42'),
(235, 67, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-30 01:44:27', '2021-07-30 01:44:27'),
(236, 67, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 01:44:41', '2021-07-30 01:44:41'),
(237, 68, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-30 01:49:37', '2021-07-30 01:49:37'),
(238, 69, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-30 02:25:37', '2021-07-30 02:25:37'),
(239, 70, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-07-30 02:28:13', '2021-07-30 02:28:13'),
(240, 71, 'ORDER_PLACED', 'ORDER_PLACED', 1, 42, 1, '2021-07-30 02:30:28', '2021-07-30 02:30:28'),
(241, 71, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 02:30:53', '2021-07-30 02:30:53'),
(242, 72, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 04:50:57', '2021-07-30 04:50:57'),
(243, 72, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 04:52:45', '2021-07-30 04:52:45'),
(244, 72, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 04:52:49', '2021-07-30 04:52:49'),
(245, 73, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 04:59:16', '2021-07-30 04:59:16'),
(246, 73, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 05:00:14', '2021-07-30 05:00:14'),
(247, 73, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 05:00:17', '2021-07-30 05:00:17'),
(248, 74, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 05:29:01', '2021-07-30 05:29:01'),
(249, 74, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 05:29:13', '2021-07-30 05:29:13'),
(250, 74, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 05:29:17', '2021-07-30 05:29:17'),
(251, 75, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-07-30 05:31:57', '2021-07-30 05:31:57'),
(252, 75, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 05:33:12', '2021-07-30 05:33:12'),
(253, 75, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-07-30 05:33:20', '2021-07-30 05:33:20'),
(254, 70, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 07:19:38', '2021-07-30 07:19:38'),
(255, 70, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 07:24:17', '2021-07-30 07:24:17'),
(256, 70, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 07:24:19', '2021-07-30 07:24:19'),
(257, 70, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-30 07:24:19', '2021-07-30 07:24:19'),
(258, 73, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-07-30 10:40:05', '2021-07-30 10:40:05'),
(259, 74, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-07-30 11:09:03', '2021-07-30 11:09:03'),
(260, 75, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-07-30 11:12:04', '2021-07-30 11:12:04'),
(261, 66, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-30 13:03:04', '2021-07-30 13:03:04'),
(262, 67, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-30 13:03:05', '2021-07-30 13:03:05'),
(263, 68, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-30 13:03:07', '2021-07-30 13:03:07'),
(264, 69, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-30 13:03:08', '2021-07-30 13:03:08'),
(265, 70, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 19, 1, '2021-07-30 13:03:09', '2021-07-30 13:03:09'),
(266, 71, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 42, 1, '2021-07-30 13:03:11', '2021-07-30 13:03:11'),
(267, 72, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-07-30 13:04:03', '2021-07-30 13:04:03'),
(268, 76, 'ORDER_PLACED', 'ORDER_PLACED', 1, 42, 1, '2021-07-31 00:27:14', '2021-07-31 00:27:14'),
(269, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:27:42', '2021-07-31 00:27:42'),
(270, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:55:29', '2021-07-31 00:55:29'),
(271, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:55:53', '2021-07-31 00:55:53'),
(272, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:55:55', '2021-07-31 00:55:55'),
(273, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:56:42', '2021-07-31 00:56:42'),
(274, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:56:43', '2021-07-31 00:56:43'),
(275, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:56:59', '2021-07-31 00:56:59'),
(276, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:09', '2021-07-31 00:57:09'),
(277, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:17', '2021-07-31 00:57:17'),
(278, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:18', '2021-07-31 00:57:18'),
(279, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:24', '2021-07-31 00:57:24'),
(280, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:25', '2021-07-31 00:57:25'),
(281, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:31', '2021-07-31 00:57:31'),
(282, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:45', '2021-07-31 00:57:45'),
(283, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:46', '2021-07-31 00:57:46'),
(284, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:46', '2021-07-31 00:57:46'),
(285, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:57:50', '2021-07-31 00:57:50'),
(286, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:59:32', '2021-07-31 00:59:32'),
(287, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:59:33', '2021-07-31 00:59:33'),
(288, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:59:34', '2021-07-31 00:59:34'),
(289, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 00:59:38', '2021-07-31 00:59:38'),
(290, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:09', '2021-07-31 01:00:09'),
(291, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:10', '2021-07-31 01:00:10'),
(292, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:10', '2021-07-31 01:00:10'),
(293, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:10', '2021-07-31 01:00:10'),
(294, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:10', '2021-07-31 01:00:10'),
(295, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-07-31 01:00:11', '2021-07-31 01:00:11'),
(296, 77, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-02 00:14:10', '2021-08-02 00:14:10'),
(297, 77, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-02 00:14:33', '2021-08-02 00:14:33'),
(298, 77, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-02 00:14:42', '2021-08-02 00:14:42'),
(299, 78, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-02 00:17:23', '2021-08-02 00:17:23'),
(300, 76, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 42, 1, '2021-08-02 00:27:20', '2021-08-02 00:27:20'),
(301, 78, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-02 00:59:51', '2021-08-02 00:59:51'),
(302, 78, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-02 00:59:55', '2021-08-02 00:59:55'),
(303, 79, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-02 01:02:31', '2021-08-02 01:02:31'),
(304, 79, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-02 01:02:47', '2021-08-02 01:02:47'),
(305, 79, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-02 01:02:50', '2021-08-02 01:02:50'),
(306, 80, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-02 01:09:58', '2021-08-02 01:09:58'),
(307, 80, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-02 01:10:24', '2021-08-02 01:10:24'),
(308, 80, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-02 01:10:27', '2021-08-02 01:10:27'),
(309, 78, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 05:58:03', '2021-08-02 05:58:03'),
(310, 79, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 06:43:03', '2021-08-02 06:43:03'),
(311, 80, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 06:50:04', '2021-08-02 06:50:04'),
(312, 77, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 13:04:03', '2021-08-02 13:04:03'),
(313, 77, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 23:54:04', '2021-08-02 23:54:04'),
(314, 78, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 23:54:06', '2021-08-02 23:54:06'),
(315, 79, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 23:54:07', '2021-08-02 23:54:07'),
(316, 80, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-02 23:54:08', '2021-08-02 23:54:08'),
(317, 77, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-03 00:00:06', '2021-08-03 00:00:06'),
(318, 81, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 00:15:35', '2021-08-03 00:15:35'),
(319, 81, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 00:16:09', '2021-08-03 00:16:09'),
(320, 81, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 00:16:19', '2021-08-03 00:16:19'),
(321, 82, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 00:25:56', '2021-08-03 00:25:56'),
(322, 82, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 00:26:10', '2021-08-03 00:26:10'),
(323, 82, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 00:26:14', '2021-08-03 00:26:14'),
(324, 82, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 2, 1, '2021-08-03 00:49:28', '2021-08-03 00:49:28'),
(325, 82, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 2, 1, '2021-08-03 00:49:39', '2021-08-03 00:49:39'),
(326, 82, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 2, 1, '2021-08-03 00:49:41', '2021-08-03 00:49:41'),
(327, 82, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 2, 1, '2021-08-03 00:49:43', '2021-08-03 00:49:43'),
(328, 82, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 2, 1, '2021-08-03 00:49:45', '2021-08-03 00:49:45'),
(329, 82, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 2, 1, '2021-08-03 00:49:55', '2021-08-03 00:49:55'),
(330, 83, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:00:09', '2021-08-03 01:00:09'),
(331, 83, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 01:00:21', '2021-08-03 01:00:21'),
(332, 83, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:00:25', '2021-08-03 01:00:25'),
(333, 83, 'ORDER_COMPLETED', 'ORDER_COMPLETED', 10, 1, 1, '2021-08-03 01:00:44', '2021-08-03 01:00:44'),
(334, 84, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:36:57', '2021-08-03 01:36:57'),
(335, 84, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 01:37:39', '2021-08-03 01:37:39'),
(336, 84, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:37:42', '2021-08-03 01:37:42'),
(337, 81, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-08-03 01:40:29', '2021-08-03 01:40:29'),
(338, 84, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-08-03 01:40:42', '2021-08-03 01:40:42'),
(339, 78, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:41:50', '2021-08-03 01:41:50'),
(340, 85, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:45:20', '2021-08-03 01:45:20'),
(341, 85, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 01:45:40', '2021-08-03 01:45:40'),
(342, 85, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:45:44', '2021-08-03 01:45:44'),
(343, 86, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:49:32', '2021-08-03 01:49:32'),
(344, 86, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 01:50:05', '2021-08-03 01:50:05'),
(345, 86, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:50:11', '2021-08-03 01:50:11'),
(346, 87, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:55:11', '2021-08-03 01:55:11'),
(347, 87, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 01:55:23', '2021-08-03 01:55:23'),
(348, 87, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 01:55:32', '2021-08-03 01:55:32'),
(349, 87, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-03 01:57:29', '2021-08-03 01:57:29'),
(350, 87, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-03 01:58:08', '2021-08-03 01:58:08'),
(351, 87, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-03 01:58:10', '2021-08-03 01:58:10'),
(352, 87, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-03 01:58:11', '2021-08-03 01:58:11'),
(353, 87, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-03 01:58:12', '2021-08-03 01:58:12'),
(354, 87, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-03 01:58:21', '2021-08-03 01:58:21'),
(355, 88, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 01:59:58', '2021-08-03 01:59:58'),
(356, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 02:31:26', '2021-08-03 02:31:26'),
(357, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 02:47:52', '2021-08-03 02:47:52'),
(358, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 02:52:01', '2021-08-03 02:52:01'),
(359, 76, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 02:53:17', '2021-08-03 02:53:17'),
(360, 89, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 06:41:34', '2021-08-03 06:41:34'),
(361, 90, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 06:46:47', '2021-08-03 06:46:47'),
(362, 91, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 06:50:35', '2021-08-03 06:50:35'),
(363, 92, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 06:54:41', '2021-08-03 06:54:41'),
(364, 92, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:03:58', '2021-08-03 07:03:58'),
(365, 92, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:04:02', '2021-08-03 07:04:02'),
(366, 93, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 07:09:03', '2021-08-03 07:09:03'),
(367, 93, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:09:34', '2021-08-03 07:09:34'),
(368, 93, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:09:38', '2021-08-03 07:09:38'),
(369, 94, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 07:16:57', '2021-08-03 07:16:57'),
(370, 94, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:17:08', '2021-08-03 07:17:08'),
(371, 94, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:17:12', '2021-08-03 07:17:12'),
(372, 95, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 07:27:19', '2021-08-03 07:27:19'),
(373, 95, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:27:53', '2021-08-03 07:27:53'),
(374, 95, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:27:56', '2021-08-03 07:27:56'),
(375, 96, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 07:30:10', '2021-08-03 07:30:10'),
(376, 96, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:31:19', '2021-08-03 07:31:19'),
(377, 96, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:31:23', '2021-08-03 07:31:23'),
(378, 96, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 51, 1, '2021-08-03 07:31:37', '2021-08-03 07:31:37'),
(379, 96, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 51, 1, '2021-08-03 07:31:38', '2021-08-03 07:31:38'),
(380, 96, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 51, 1, '2021-08-03 07:31:39', '2021-08-03 07:31:39'),
(381, 96, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 51, 1, '2021-08-03 07:31:40', '2021-08-03 07:31:40'),
(382, 97, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-03 07:37:11', '2021-08-03 07:37:11'),
(383, 97, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-03 07:42:59', '2021-08-03 07:42:59'),
(384, 97, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-03 07:43:03', '2021-08-03 07:43:03'),
(385, 98, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-08-04 00:06:19', '2021-08-04 00:06:19'),
(386, 98, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 00:06:33', '2021-08-04 00:06:33'),
(387, 78, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 00:58:06', '2021-08-04 00:58:06'),
(388, 78, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 01:06:45', '2021-08-04 01:06:45'),
(389, 78, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 01:07:44', '2021-08-04 01:07:44'),
(390, 78, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 01:07:47', '2021-08-04 01:07:47'),
(391, 78, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 01:07:51', '2021-08-04 01:07:51'),
(392, 78, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 01:07:52', '2021-08-04 01:07:52'),
(393, 78, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 01:08:28', '2021-08-04 01:08:28'),
(394, 99, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-08-04 01:08:58', '2021-08-04 01:08:58'),
(395, 99, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 01:09:11', '2021-08-04 01:09:11'),
(396, 99, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 01:10:16', '2021-08-04 01:10:16'),
(397, 99, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 01:10:21', '2021-08-04 01:10:21'),
(398, 99, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 01:10:24', '2021-08-04 01:10:24'),
(399, 99, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 01:10:28', '2021-08-04 01:10:28'),
(400, 99, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 01:10:30', '2021-08-04 01:10:30'),
(401, 99, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 01:10:42', '2021-08-04 01:10:42'),
(402, 100, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 02:09:20', '2021-08-04 02:09:20'),
(403, 100, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 02:09:54', '2021-08-04 02:09:54'),
(404, 100, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 02:10:48', '2021-08-04 02:10:48'),
(405, 100, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 02:12:41', '2021-08-04 02:12:41'),
(406, 100, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 02:13:14', '2021-08-04 02:13:14'),
(407, 100, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 02:13:17', '2021-08-04 02:13:17'),
(408, 100, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 02:13:19', '2021-08-04 02:13:19'),
(409, 100, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 02:13:26', '2021-08-04 02:13:26'),
(410, 100, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 02:13:40', '2021-08-04 02:13:40'),
(411, 101, 'ORDER_PLACED', 'ORDER_PLACED', 1, 19, 1, '2021-08-04 02:43:24', '2021-08-04 02:43:24'),
(412, 101, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 02:43:43', '2021-08-04 02:43:43'),
(413, 101, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 02:44:38', '2021-08-04 02:44:38'),
(414, 101, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 02:44:47', '2021-08-04 02:44:47'),
(415, 101, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 02:44:50', '2021-08-04 02:44:50'),
(416, 101, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 02:44:53', '2021-08-04 02:44:53'),
(417, 101, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 02:44:55', '2021-08-04 02:44:55'),
(418, 101, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 02:47:17', '2021-08-04 02:47:17'),
(419, 102, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 04:52:03', '2021-08-04 04:52:03'),
(420, 102, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 04:53:13', '2021-08-04 04:53:13'),
(421, 102, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 04:53:37', '2021-08-04 04:53:37'),
(422, 97, 'CANCELLED_BY_CUSTOMER', 'CANCELLED_BY_CUSTOMER', 11, 1, 1, '2021-08-04 05:03:39', '2021-08-04 05:03:39'),
(423, 102, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 05:06:43', '2021-08-04 05:06:43'),
(424, 102, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 05:06:49', '2021-08-04 05:06:49'),
(425, 102, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 05:06:51', '2021-08-04 05:06:51'),
(426, 102, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 05:07:01', '2021-08-04 05:07:01'),
(427, 102, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 05:07:04', '2021-08-04 05:07:04'),
(428, 102, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 05:07:12', '2021-08-04 05:07:12'),
(429, 103, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 05:21:11', '2021-08-04 05:21:11'),
(430, 103, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 05:25:46', '2021-08-04 05:25:46'),
(431, 103, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 05:25:51', '2021-08-04 05:25:51'),
(432, 103, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 05:26:22', '2021-08-04 05:26:22'),
(433, 103, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 05:26:25', '2021-08-04 05:26:25'),
(434, 103, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 05:27:05', '2021-08-04 05:27:05'),
(435, 103, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 05:27:36', '2021-08-04 05:27:36'),
(436, 103, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 05:27:55', '2021-08-04 05:27:55'),
(437, 103, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 05:30:22', '2021-08-04 05:30:22'),
(438, 104, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 06:09:29', '2021-08-04 06:09:29'),
(439, 104, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 06:10:00', '2021-08-04 06:10:00'),
(440, 104, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 06:10:12', '2021-08-04 06:10:12'),
(441, 105, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 06:10:41', '2021-08-04 06:10:41'),
(442, 104, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 06:16:58', '2021-08-04 06:16:58'),
(443, 104, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 06:17:04', '2021-08-04 06:17:04'),
(444, 104, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 06:17:06', '2021-08-04 06:17:06');
INSERT INTO `booking_tracking_history` (`id`, `bookingId`, `reason`, `value`, `status_value`, `userId`, `type`, `created_at`, `updated_at`) VALUES
(445, 104, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 06:17:12', '2021-08-04 06:17:12'),
(446, 104, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 06:17:14', '2021-08-04 06:17:14'),
(447, 104, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 06:17:25', '2021-08-04 06:17:25'),
(448, 105, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 06:28:52', '2021-08-04 06:28:52'),
(449, 105, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 06:28:55', '2021-08-04 06:28:55'),
(450, 105, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 06:29:17', '2021-08-04 06:29:17'),
(451, 105, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 06:29:47', '2021-08-04 06:29:47'),
(452, 105, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 06:29:59', '2021-08-04 06:29:59'),
(453, 105, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 06:30:09', '2021-08-04 06:30:09'),
(454, 105, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 06:30:17', '2021-08-04 06:30:17'),
(455, 105, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 06:30:27', '2021-08-04 06:30:27'),
(456, 106, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 06:41:47', '2021-08-04 06:41:47'),
(457, 106, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-04 06:42:08', '2021-08-04 06:42:08'),
(458, 106, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-04 06:42:12', '2021-08-04 06:42:12'),
(459, 106, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-04 06:44:40', '2021-08-04 06:44:40'),
(460, 106, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-04 06:44:50', '2021-08-04 06:44:50'),
(461, 106, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-04 06:44:52', '2021-08-04 06:44:52'),
(462, 106, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-04 06:44:54', '2021-08-04 06:44:54'),
(463, 106, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-04 06:44:56', '2021-08-04 06:44:56'),
(464, 106, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-04 06:45:05', '2021-08-04 06:45:05'),
(465, 107, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-04 08:12:25', '2021-08-04 08:12:25'),
(466, 107, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-04 13:53:03', '2021-08-04 13:53:03'),
(467, 108, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-05 02:53:07', '2021-08-05 02:53:07'),
(468, 109, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-05 03:02:42', '2021-08-05 03:02:42'),
(469, 108, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-05 04:07:18', '2021-08-05 04:07:18'),
(470, 108, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-05 04:07:36', '2021-08-05 04:07:36'),
(471, 108, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-05 04:08:45', '2021-08-05 04:08:45'),
(472, 108, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-05 04:09:09', '2021-08-05 04:09:09'),
(473, 108, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-05 04:09:23', '2021-08-05 04:09:23'),
(474, 108, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-05 04:09:25', '2021-08-05 04:09:25'),
(475, 108, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-05 04:09:26', '2021-08-05 04:09:26'),
(476, 108, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-05 04:09:42', '2021-08-05 04:09:42'),
(477, 109, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-05 04:21:40', '2021-08-05 04:21:40'),
(478, 109, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-05 04:21:44', '2021-08-05 04:21:44'),
(479, 109, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-05 04:22:37', '2021-08-05 04:22:37'),
(480, 109, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-05 04:22:43', '2021-08-05 04:22:43'),
(481, 109, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-05 04:22:44', '2021-08-05 04:22:44'),
(482, 109, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-05 04:22:46', '2021-08-05 04:22:46'),
(483, 109, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-05 04:22:47', '2021-08-05 04:22:47'),
(484, 109, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-05 04:22:55', '2021-08-05 04:22:55'),
(485, 110, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-05 04:30:50', '2021-08-05 04:30:50'),
(486, 110, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-05 04:31:06', '2021-08-05 04:31:06'),
(487, 110, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-05 04:31:18', '2021-08-05 04:31:18'),
(488, 110, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-05 04:33:16', '2021-08-05 04:33:16'),
(489, 110, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-05 04:33:22', '2021-08-05 04:33:22'),
(490, 110, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-05 04:33:25', '2021-08-05 04:33:25'),
(491, 110, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-05 04:33:27', '2021-08-05 04:33:27'),
(492, 110, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-05 04:33:28', '2021-08-05 04:33:28'),
(493, 110, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-05 04:33:37', '2021-08-05 04:33:37'),
(494, 111, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-05 04:41:34', '2021-08-05 04:41:34'),
(495, 111, 'ORDER_ACCEPTED_BY_VENDOR', 'ORDER_ACCEPTED_BY_VENDOR', 2, NULL, 1, '2021-08-05 04:42:16', '2021-08-05 04:42:16'),
(496, 111, 'ORDER_IN_PROGRESS', 'ORDER_IN_PROGRESS', 4, 1, 1, '2021-08-05 04:42:19', '2021-08-05 04:42:19'),
(497, 111, 'DELIVERY_BOY_ASSIGNED', 'DELIVERY_BOY_ASSIGNED', 5, 7, 1, '2021-08-05 04:47:46', '2021-08-05 04:47:46'),
(498, 111, 'DELIVERY_BOY_STARTED_THE_JOURNEY', 'DELIVERY_BOY_STARTED_THE_JOURNEY', 6, 7, 1, '2021-08-05 04:47:51', '2021-08-05 04:47:51'),
(499, 111, 'DELIVERY_BOY_REACHED_TO_VENDOR', 'DELIVERY_BOY_REACHED_TO_VENDOR', 7, 7, 1, '2021-08-05 04:47:52', '2021-08-05 04:47:52'),
(500, 111, 'DELIVERY_BOY_PICKED_ORDER', 'DELIVERY_BOY_PICKED_ORDER', 8, 7, 1, '2021-08-05 04:47:53', '2021-08-05 04:47:53'),
(501, 111, 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 'DELIVERY_BOY_REACHED_TO_CUSTOMER', 9, 7, 1, '2021-08-05 04:47:54', '2021-08-05 04:47:54'),
(502, 111, 'DELIVERED_PRODUCT', 'DELIVERED_PRODUCT', 10, 7, 1, '2021-08-05 04:48:03', '2021-08-05 04:48:03'),
(503, 76, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 42, 1, '2021-08-10 13:04:03', '2021-08-10 13:04:03'),
(504, 112, 'ORDER_PLACED', 'ORDER_PLACED', 1, 58, 1, '2021-08-11 03:56:38', '2021-08-11 03:56:38'),
(505, 112, 'ORDER_REJECTED_BY_VENDOR', 'ORDER_REJECTED_BY_VENDOR', 3, NULL, 1, '2021-08-11 03:58:23', '2021-08-11 03:58:23'),
(506, 113, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-13 02:23:47', '2021-08-13 02:23:47'),
(507, 114, 'ORDER_PLACED', 'ORDER_PLACED', 1, 1, 1, '2021-08-18 05:20:45', '2021-08-18 05:20:45'),
(508, 114, 'AUTO_CANCELLED', 'AUTO_CANCELLED', 14, 1, 1, '2021-08-18 13:04:02', '2021-08-18 13:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `booking_various_charges`
--

CREATE TABLE `booking_various_charges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bookingId` int(11) NOT NULL,
  `charegeId` int(11) NOT NULL,
  `chargeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `created_at`, `updated_at`) VALUES
(1, 'Grocery', 'adf', '2021-07-22 06:10:04', '2021-07-22 06:10:04'),
(2, 'Fruits', 'dfag', '2021-07-22 06:18:03', '2021-07-22 06:18:03');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `company_description`, `created_at`, `updated_at`) VALUES
(1, 'Fidalgo', 'Gopal', '2021-07-22 06:18:12', '2021-07-22 06:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shopId` int(11) DEFAULT NULL,
  `coupon_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPrimary` tinyint(1) NOT NULL DEFAULT '3',
  `coupon_type` tinyint(4) NOT NULL COMMENT '0->percentage,1->fixed',
  `coupon_discount` double NOT NULL,
  `coupon_min_amount` double DEFAULT NULL,
  `coupon_max_amount` double DEFAULT NULL,
  `maximum_total_use` int(11) DEFAULT NULL,
  `maximum_per_customer_use` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `shopId`, `coupon_name`, `coupon_code`, `coupon_description`, `isPrimary`, `coupon_type`, `coupon_discount`, `coupon_min_amount`, `coupon_max_amount`, `maximum_total_use`, `maximum_per_customer_use`, `start_date`, `end_date`, `deleted_at`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 1, 'SPECIFIC FIXED', 'NIW379', 'SPECIFIC FIXED 10', 3, 1, 10, 10, 1000, 1000, 1000, '2021-07-22 17:30:00', '2022-03-31 12:00:00', NULL, '2021-07-22 06:23:54', '2021-07-22 06:23:54', NULL),
(2, 1, 'Specific percentage', 'NIW379', 'Specific percentage', 3, 0, 10, 10, 1000, 1000, 1000, '2021-07-22 17:30:00', '2021-12-31 12:00:00', NULL, '2021-07-22 06:24:15', '2021-07-22 06:29:13', NULL),
(3, 1, 'SPECIFIC FIXED', 'NIW379', 'SPECIFIC FIXED 10', 3, 1, 10, 10, 1000, 1000, 1000, '2021-07-22 17:30:00', '2021-12-31 12:00:00', '2021-07-22 06:30:15', '2021-07-22 06:24:37', '2021-07-22 06:30:15', NULL),
(4, 1, 'SPECIFIC FIXED', 'NIW379', 'SPECIFIC FIXED 10', 3, 1, 10, 10, 1000, 1000, 1000, '2021-07-22 17:30:00', '2021-12-31 12:00:00', '2021-07-22 06:30:12', '2021-07-22 06:26:35', '2021-07-22 06:30:12', NULL),
(5, NULL, 'PERCENT GLOBAL', '1FW24N', 'IT IS TEN PERCENT COUPON', 3, 0, 10, 10, 200, 100, 100, '2021-07-23 12:00:00', '2021-07-30 12:00:00', '2021-07-22 06:30:00', '2021-07-22 06:29:47', '2021-07-22 06:30:00', NULL),
(6, 1, 'SPECIFIC FIXED', '3NDGLP', 'SPECIFIC FIXED 10', 3, 0, 10, 10, 200, 100, 100, '2021-07-23 12:00:00', '2021-07-24 12:00:00', '2021-07-22 06:31:15', '2021-07-22 06:30:57', '2021-07-22 06:31:15', NULL),
(7, NULL, 'PERCENT GLOBAL', '5GHLC2', 'IT IS TEN PERCENT COUPON', 3, 0, 10, 10, 1000, 100, 100, '2021-07-22 17:35:00', '2022-01-31 12:00:00', NULL, '2021-07-22 06:32:31', '2021-07-22 06:32:31', NULL),
(8, NULL, 'GLOBAL FIXED', 'J3QOLF', 'GLOBAL FIXED 10', 3, 1, 10, 10, 1000, 1000, 1000, '2021-07-22 17:35:00', '2022-01-31 12:00:00', NULL, '2021-07-22 06:33:37', '2021-07-22 06:33:37', NULL),
(9, 1, 'Dsf', 'J6HPDE', 'dfd', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 10:01:00', '2021-07-31 12:00:00', '2021-07-28 07:42:49', '2021-07-26 23:00:05', '2021-07-28 07:42:49', NULL),
(10, 1, 'Dsf', 'J6HPDE', 'dfd', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 10:05:00', '2021-07-31 12:00:00', '2021-07-28 07:42:45', '2021-07-26 23:00:33', '2021-07-28 07:42:45', NULL),
(11, 1, 'Dsf', 'J6HPDE', 'dfd', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 10:05:00', '2021-07-31 12:00:00', '2021-07-28 07:42:42', '2021-07-26 23:01:29', '2021-07-28 07:42:42', NULL),
(12, 1, 'Dsf', 'J6HPDE', 'dfd', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 10:05:00', '2021-07-31 12:00:00', '2021-07-28 07:42:40', '2021-07-26 23:05:28', '2021-07-28 07:42:40', NULL),
(13, 1, 'Dsaf', 'F7BOVH', 'dsaf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 10:25:00', '2021-07-31 12:00:00', '2021-07-28 07:41:47', '2021-07-26 23:13:40', '2021-07-28 07:41:47', NULL),
(14, 3, 'Test coupon', 'KAOVGC', 'coupon desc', 3, 0, 10, 10, 1000, 5, 10, '2021-07-28 12:00:00', '2021-07-30 12:00:00', NULL, '2021-07-27 01:52:27', '2021-07-27 01:52:27', NULL),
(15, 3, 'Test coupon', 'KAOVGC', 'coupon desc', 3, 0, 10, 10, 1000, 5, 10, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-27 01:59:30', '2021-07-27 01:53:58', '2021-07-27 01:59:30', NULL),
(16, 3, 'Edfe', 'K1J6GY', 'fef', 3, 0, 12, 321, 312424, 21, 12, '2021-07-28 12:00:00', '2021-07-29 12:00:00', NULL, '2021-07-27 01:59:21', '2021-07-27 01:59:21', NULL),
(17, 1, 'Cv', 'TCJ8E3', 'sdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:10:00', '2021-07-31 12:00:00', '2021-07-28 07:41:37', '2021-07-27 02:06:49', '2021-07-28 07:41:37', NULL),
(18, 1, 'Cv', 'TCJ8E3', 'sdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:10:00', '2021-07-31 12:00:00', '2021-07-28 07:41:27', '2021-07-27 02:06:54', '2021-07-28 07:41:27', NULL),
(19, 1, 'Cv', 'TCJ8E3', 'sdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:10:00', '2021-07-31 12:00:00', '2021-07-28 07:41:13', '2021-07-27 02:07:05', '2021-07-28 07:41:13', NULL),
(20, 1, 'Cv', 'TCJ8E3', 'sdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:10:00', '2021-07-31 12:00:00', '2021-07-28 07:41:16', '2021-07-27 02:07:10', '2021-07-28 07:41:16', NULL),
(21, 1, 'Cv', 'TCJ8E3', 'sdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:10:00', '2021-07-31 12:00:00', '2021-07-28 07:41:19', '2021-07-27 02:07:22', '2021-07-28 07:41:19', NULL),
(22, 1, 'Fds', 'I15VPN', 'fasdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:20:00', '2021-07-31 12:00:00', '2021-07-28 07:41:21', '2021-07-27 02:18:06', '2021-07-28 07:41:21', NULL),
(23, 3, 'Rthrt', '1ZPLA9', 'rth', 3, 0, 10, 10, 12, 213, 21, '2021-07-28 12:00:00', '2021-07-29 12:00:00', NULL, '2021-07-27 02:18:37', '2021-07-27 02:18:37', NULL),
(24, 1, 'Fdas', 'OXLR0G', 'asdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:25:00', '2021-07-31 12:00:00', '2021-07-28 07:41:24', '2021-07-27 02:19:25', '2021-07-28 07:41:24', NULL),
(25, 1, 'Fdas', 'OXLR0G', 'asdf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:25:00', '2021-07-31 12:00:00', '2021-07-28 07:41:34', '2021-07-27 02:20:53', '2021-07-28 07:41:34', NULL),
(26, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:31', '2021-07-27 02:21:27', '2021-07-28 07:41:31', NULL),
(27, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:41', '2021-07-27 02:22:58', '2021-07-28 07:41:41', NULL),
(28, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:44', '2021-07-27 02:23:21', '2021-07-28 07:41:44', NULL),
(29, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:50', '2021-07-27 02:23:38', '2021-07-28 07:41:50', NULL),
(30, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:53', '2021-07-27 02:24:45', '2021-07-28 07:41:53', NULL),
(31, 1, 'Ewfwe', 'DGNCO7', 'fwef', 3, 0, 12, 1212, 45453, 3, 3, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:41:56', '2021-07-27 02:25:00', '2021-07-28 07:41:56', NULL),
(32, 1, 'Dfasd', 'LAKDG7', 'sdaf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:30:00', '2021-07-31 12:00:00', '2021-07-28 07:42:38', '2021-07-27 02:27:18', '2021-07-28 07:42:38', NULL),
(33, 1, 'Aaaaasssss', 'DF3N6U', 'adsfsdaf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 13:30:00', '2021-07-31 12:00:00', '2021-07-28 07:42:00', '2021-07-27 02:28:35', '2021-07-28 07:42:00', NULL),
(34, 1, 'Tfs', 'PQW2TH', 'sg', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 14:00:00', '2021-07-31 12:00:00', '2021-07-28 07:42:35', '2021-07-27 02:59:53', '2021-07-28 07:42:35', NULL),
(35, 3, 'Ytjhtyt', '5HKQAD', 'jytjty', 3, 1, 12, 12, 1212, 12, 12, '2021-07-28 12:00:00', '2021-07-29 12:00:00', NULL, '2021-07-27 04:19:39', '2021-07-27 04:19:39', NULL),
(36, 3, 'Rrg', 'MXIQNK', 'rgr', 3, 0, 12, 1212, 3534545, 34, 34, '2021-07-29 12:00:00', '2021-07-30 12:00:00', NULL, '2021-07-27 04:21:05', '2021-07-27 04:21:05', NULL),
(37, 1, 'Tht', 'MAKFOL', 'hthth', 3, 1, 21, 1, 23, 12, 12, '2021-07-29 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:04', '2021-07-27 04:22:04', '2021-07-28 07:42:04', NULL),
(38, 1, 'Rgr', 'BJQLTN', 'grg', 3, 0, 23, 2, 333, 1, 1, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:07', '2021-07-27 04:25:10', '2021-07-28 07:42:07', NULL),
(39, 1, 'Ff', 'EU652M', 'fdff', 3, 1, 12, 2323, 545454, 12, 2, '2021-07-29 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:11', '2021-07-27 04:27:42', '2021-07-28 07:42:11', NULL),
(40, 1, 'Dfsg', 'YSJW96', 'cjn', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 15:45:00', '2021-07-31 12:00:00', '2021-07-28 07:42:18', '2021-07-27 04:37:22', '2021-07-28 07:42:18', NULL),
(41, 1, 'Rh', 'CAYX92', 'hthth', 3, 0, 12, 121, 12121, 12, 121, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:14', '2021-07-27 07:04:40', '2021-07-28 07:42:14', NULL),
(42, 1, 'Rgr', 'TSZXAF', 'grg', 3, 0, 12, 212, 12133, 12, 222, '2021-07-28 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:23', '2021-07-27 07:07:21', '2021-07-28 07:42:23', NULL),
(43, 1, 'Fef', '5Z670Y', 'efe', 3, 0, 11, 23, 55, 23, 23, '2021-07-29 12:00:00', '2021-07-30 12:00:00', '2021-07-28 07:42:20', '2021-07-27 07:13:38', '2021-07-28 07:42:20', NULL),
(44, 1, 'Asdfds', '0NDUWI', 'asdfdsaf', 3, 0, 10, 10, 200, 100, 100, '2021-07-27 19:50:00', '2021-07-31 12:00:00', '2021-07-28 07:42:32', '2021-07-27 07:40:53', '2021-07-28 07:42:32', NULL),
(45, 1, 'Vgbdf', 'WVCFJT', 'adfga', 3, 0, 10, 10, 200, 1000, 1000, '2021-07-27 19:00:00', '2021-07-31 12:00:00', '2021-07-28 07:42:26', '2021-07-27 07:42:47', '2021-07-28 07:42:26', NULL),
(46, 1, 'Fasdf', 'NR7P30', 'asdf', 3, 1, 10, 10, 200, 1000, 1000, '2021-07-28 12:00:00', '2021-07-31 12:00:00', '2021-07-28 07:42:29', '2021-07-27 07:44:23', '2021-07-28 07:42:29', NULL),
(47, NULL, '20 % off coupon generated by admin', 'N7SCY4', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 05:07:57', '2021-08-28 05:07:57', NULL, '2021-07-27 23:37:57', '2021-07-27 23:37:57', 22),
(48, NULL, '20 % off coupon generated by admin', 'RCSD0J', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 05:07:57', '2021-08-28 05:07:57', NULL, '2021-07-27 23:37:57', '2021-07-27 23:37:57', 19),
(49, NULL, '20 % off coupon generated by admin', '352A4C', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 05:11:56', '2021-08-28 05:11:56', NULL, '2021-07-27 23:41:56', '2021-07-27 23:41:56', 23),
(50, NULL, '20 % off coupon generated by admin', 'IC9MTR', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 05:11:56', '2021-08-28 05:11:56', NULL, '2021-07-27 23:41:56', '2021-07-27 23:41:56', 22),
(51, NULL, '20 % off coupon generated by admin', 'ZF95YA', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 11:35:33', '2021-08-28 11:35:33', NULL, '2021-07-28 06:05:33', '2021-07-28 06:05:33', 5),
(52, NULL, '20 % off coupon generated by admin', 'KZCM13', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 11:35:33', '2021-08-28 11:35:33', NULL, '2021-07-28 06:05:33', '2021-07-28 06:05:33', 25),
(53, NULL, '20 % off coupon generated by admin', '510N2H', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:33:25', '2021-08-28 12:33:25', NULL, '2021-07-28 07:03:25', '2021-07-28 07:03:25', 28),
(54, NULL, '20 % off coupon generated by admin', '2C396W', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:33:25', '2021-08-28 12:33:25', NULL, '2021-07-28 07:03:25', '2021-07-28 07:03:25', 27),
(55, NULL, '20 % off coupon generated by admin', 'C297VT', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:45:34', '2021-08-28 12:45:34', NULL, '2021-07-28 07:15:34', '2021-07-28 07:15:34', 30),
(56, NULL, '20 % off coupon generated by admin', 'VSTGLH', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:45:34', '2021-08-28 12:45:34', NULL, '2021-07-28 07:15:34', '2021-07-28 07:15:34', 29),
(57, NULL, '20 % off coupon generated by admin', 'XF0CRN', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:56:11', '2021-08-28 12:56:11', NULL, '2021-07-28 07:26:11', '2021-07-28 07:26:11', 32),
(58, NULL, '20 % off coupon generated by admin', 'UDQHET', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:56:11', '2021-08-28 12:56:11', NULL, '2021-07-28 07:26:11', '2021-07-28 07:26:11', 31),
(59, NULL, '20 % off coupon generated by admin', '32O9DI', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:57:34', '2022-01-14 12:57:34', NULL, '2021-07-28 07:27:34', '2021-07-28 07:27:34', 34),
(60, NULL, '20 % off coupon generated by admin', 'JCN6EX', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 12:57:34', '2021-08-28 12:57:34', NULL, '2021-07-28 07:27:34', '2021-07-28 07:27:34', 33),
(61, NULL, '20 % off coupon generated by admin', '4IDN9S', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:00:06', '2021-08-28 13:00:06', NULL, '2021-07-28 07:30:06', '2021-07-28 07:30:06', 36),
(62, NULL, '20 % off coupon generated by admin', 'PDJYZL', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:00:06', '2021-08-28 13:00:06', NULL, '2021-07-28 07:30:06', '2021-07-28 07:30:06', 35),
(63, NULL, '20 % off coupon generated by admin', 'FDO68Z', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:06:19', '2021-08-28 13:06:19', NULL, '2021-07-28 07:36:19', '2021-07-28 07:36:19', 37),
(64, NULL, '20 % off coupon generated by admin', 'WCZG23', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:06:19', '2021-08-28 13:06:19', NULL, '2021-07-28 07:36:19', '2021-07-28 07:36:19', 35),
(65, NULL, '20 % off coupon generated by admin', '51PLUK', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:10:12', '2021-08-28 13:10:12', NULL, '2021-07-28 07:40:12', '2021-07-28 07:40:12', 39),
(66, NULL, '20 % off coupon generated by admin', '32WMO5', 'Coupon after referraring', 0, 0, 20, 100, 1000, 1, 1, '2021-07-28 13:10:12', '2021-08-28 13:10:12', NULL, '2021-07-28 07:40:12', '2021-07-28 07:40:12', 38),
(67, 2, 'Test', 'QSJRH1', 'heuh', 2, 1, 10, 100, 1000, 121, 12, '2021-08-19 12:00:00', '2021-08-20 12:00:00', NULL, '2021-08-18 05:34:17', '2021-08-18 05:37:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deliveryboy_main_documents`
--

CREATE TABLE `deliveryboy_main_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `registeration_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_upto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `drivinglicence_images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drivinglicence_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drivinglicence_expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `licence_no_plate_images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_plate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_insurence_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_insurence_valid_upto` datetime DEFAULT NULL,
  `vehicle_insurence_valid_from` datetime DEFAULT NULL,
  `vehicle_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deliveryboy_main_documents`
--

INSERT INTO `deliveryboy_main_documents` (`id`, `user_id`, `registeration_number`, `vehicle_picture`, `model`, `color`, `valid_upto`, `drivinglicence_images`, `drivinglicence_no`, `drivinglicence_expiry_date`, `licence_no_plate_images`, `no_plate`, `vehicle_insurence_image`, `policy_number`, `vehicle_insurence_valid_upto`, `vehicle_insurence_valid_from`, `vehicle_company`, `created_at`, `updated_at`) VALUES
(6, 6, 'Fhdfhf', 'https://limitless-images-test.s3.amazonaws.com/1627047066image_cropper_1627047064154.jpg', 'Ffg', 'Gggh', '2021-07-26 06:47:33', 'https://limitless-images-test.s3.amazonaws.com/1627047144image_cropper_1627047144009.jpg', 'Ghhg', '2021-07-22 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1627047161image_cropper_1627047160594.jpg', 'Ghhh', 'https://limitless-images-test.s3.amazonaws.com/1627047171image_cropper_1627047171143.jpg', 'Fhhfjf', '2021-07-31 00:00:00', '2021-07-23 00:00:00', 'Xvbfb', NULL, NULL),
(9, 2, 'Jdfn', 'https://limitless-images-test.s3.amazonaws.com/1626955589image_cropper_1626955585589.jpg', 'Ndnf', 'Ndnc', '2021-07-27 10:48:14', 'https://limitless-images-test.s3.amazonaws.com/1626955611image_cropper_1626955607865.jpg', 'Ndfb', '2034-07-30 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1626955630image_cropper_1626955626765.jpg', 'Jdjcnnf', 'https://limitless-images-test.s3.amazonaws.com/1626955647image_cropper_1626955643661.jpg', 'N Nc', '2035-07-31 00:00:00', '2021-07-23 00:00:00', 'Nfnnf', NULL, NULL),
(13, 3, 'Ndnd', 'https://limitless-images-test.s3.amazonaws.com/1626956605image_cropper_1626956601352.jpg', 'Ndnfn', 'Ndnx', '2021-07-29 07:00:59', 'https://limitless-images-test.s3.amazonaws.com/1627541979jpg', 'Gv', '2035-07-30 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1627541996jpg', 'Vbbb', 'https://limitless-images-test.s3.amazonaws.com/1627542011jpg', 'Bbhh', '2035-07-31 00:00:00', '2030-07-31 00:00:00', 'Ifjfjf', NULL, NULL),
(15, 40, 'hdfhhdxvgggg', 'https://limitless-images-test.s3.amazonaws.com/1627541928image_cropper_1627541920048.jpg', 'cbxbxb', 'fhhffh', '2021-07-29 07:48:53', 'https://limitless-images-test.s3.amazonaws.com/1627541961image_cropper_1627541958105.jpg', 'fgg.', '2021-07-28 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1627541978image_cropper_1627541973802.jpg', 'dfghh', 'https://limitless-images-test.s3.amazonaws.com/1627541990image_cropper_1627541987176.jpg', 'rttt', '2021-07-30 00:00:00', '2021-07-29 00:00:00', 'uryrrh', NULL, NULL),
(20, 7, 'Hshahaggggg', 'https://limitless-images-test.s3.amazonaws.com/1627280791image_cropper_1627280790693.jpg', 'Ahajj', 'Bssjj', '2021-07-30 07:19:54', 'https://limitless-images-test.s3.amazonaws.com/1627280819image_cropper_1627280817621.jpg', 'Shahah', '2021-07-25 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1627280835image_cropper_1627280835467.jpg', 'Ahahhasggss', 'https://limitless-images-test.s3.amazonaws.com/1627280845image_cropper_1627280844964.jpg', 'Ahhahagjfj', '2021-07-30 00:00:00', '2021-07-26 00:00:00', 'Bshah', NULL, NULL),
(21, 63, 'Nivi', 'https://limitless-images-test.s3.amazonaws.com/1629051812image_cropper_1629051808034.jpg', 'Civic', 'Grey', '2021-08-15 18:30:49', 'https://limitless-images-test.s3.amazonaws.com/1629051849image_cropper_1629051845032.jpg', 'Abc123', '2022-07-21 18:30:00', 'https://limitless-images-test.s3.amazonaws.com/1629051868image_cropper_1629051864784.jpg', 'Nivi', 'https://limitless-images-test.s3.amazonaws.com/1629051899image_cropper_1629051896272.jpg', 'Abcxyz', '2022-06-30 00:00:00', '2021-08-13 00:00:00', 'Honda', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deliveryboy_temporary_documents`
--

CREATE TABLE `deliveryboy_temporary_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `registeration_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->Pending, 2->Approved, 3->Rejected',
  `vehicle_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_upto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `drivinglicence_images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drivinglicence_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->Pending, 2->Approved, 3->Rejected',
  `drivinglicence_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drivinglicence_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drivinglicence_expiry_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `licence_no_plate_images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licence_no_plate_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->Pending, 2->Approved, 3->Rejected',
  `licence_no_plate_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_plate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_insurence_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_insurence_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1->Pending, 2->Approved, 3->Rejected',
  `vehicle_insurence_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_insurence_valid_upto` datetime DEFAULT NULL,
  `vehicle_insurence_valid_from` datetime DEFAULT NULL,
  `policy_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deliveryboy_temporary_documents`
--

INSERT INTO `deliveryboy_temporary_documents` (`id`, `user_id`, `registeration_number`, `vehicle_picture`, `vehicle_status`, `vehicle_reason`, `model`, `color`, `valid_upto`, `drivinglicence_images`, `drivinglicence_status`, `drivinglicence_reason`, `drivinglicence_no`, `drivinglicence_expiry_date`, `licence_no_plate_images`, `licence_no_plate_status`, `licence_no_plate_reason`, `no_plate`, `vehicle_insurence_image`, `vehicle_insurence_status`, `vehicle_insurence_reason`, `vehicle_insurence_valid_upto`, `vehicle_insurence_valid_from`, `policy_number`, `vehicle_company`, `created_at`, `updated_at`) VALUES
(8, 8, 'Vaab', 'https://limitless-images-test.s3.amazonaws.com/1627282896jpg', 2, NULL, 'Bh', 'Vvvb', '2021-07-26 07:01:54', '', 0, NULL, '', '0000-00-00 00:00:00', '', 0, NULL, '', '', 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 'Bbbb', NULL, '2021-07-26 01:31:54');

-- --------------------------------------------------------

--
-- Table structure for table `device_details`
--

CREATE TABLE `device_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `build_version` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `build` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `build_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `access_token_id` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_details`
--

INSERT INTO `device_details` (`id`, `user_id`, `device_id`, `device_token`, `build_version`, `platform`, `build`, `build_mode`, `created_at`, `updated_at`, `access_token_id`) VALUES
(22, 8, '5d48db6f061c6dfe', 'dtI2uRjCT8uDBTwW5parLQ:APA91bHq7XnyMMEsgOlxAqUuwHoGOmUvGf1oj3G2jrumpwEIVs1EfFZ0R6K9LZVyKjh2i_rG_FINkJckWr5C8xnqWER0aQqyu9u-huAtKELjAz7iOaamAqqbmPEtzAsHMzUC7MWaArtD', '1.0.0', '0', NULL, NULL, '2021-07-26 01:30:47', '2021-07-26 01:30:47', 'cebddd798c0eed753cc59303f00d09aa71c48b34c0bd8d7324177582cf7ab3827a8953fe6388454d'),
(26, 9, '5fc9c5ae0f167e1e', 'cZKcZ2bQSzuIn-UtprJ-5Q:APA91bG-1ZcQrpbJwTXgBH2K2-7psBY1bGGrI29SzpSU4xw22wbvQNjcuSXARkOc6BOWGAmSecmwJRuK9v5oyLOWJ7ftMDZqN7Xb2xnFqdhSMNjLojHVZYIkP4clwhPw5eMrAVojMzA3', '1.0.0', '0', NULL, NULL, '2021-07-26 04:53:57', '2021-07-26 04:53:57', 'd20af8aaef826c3e3cf7d1adafa70c559fc857489f024c450465483dbea919f5c1d31fa6e6154be1'),
(28, 10, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 06:50:06', '2021-07-26 06:50:06', '5b8bb8bed55fdb6aa9847bb7ef5bf4c7a3adc7638cdb838b7d4b1b8d2a3ad0b05eb95c0df2eb8feb'),
(30, 11, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 06:50:57', '2021-07-26 06:50:57', 'e11eda8e3cbd399771d78fef7a15d921c0713c732598cbbc5da1cdeffb99eea2af79d21a356062cc'),
(54, 14, 'dd6b71c96524afd4', 'fyr25gK0RNGHa8WFBpe0K1:APA91bHBpNhzQEjKjZBBGf2h3Zt3jbcgTo798Bsh2elDkGEhnDhMMyqERSvN5sUkeF0_AVyEHHKF4WXhp-Gs3_aI9UNfAWDX5OvOixDrbVEVamjeEaeQiOwxQs-mBjPZYkKb2WuQ7Wbb', '1.0.0', '0', NULL, NULL, '2021-07-27 01:59:34', '2021-07-27 01:59:34', 'f483b4ef4f7048cb9e49808486bed5dbd205dd161b61432997bae23bb692b1eb11cab66c76c89bb6'),
(57, 14, 'd2de3dc611c6284c', 'diUqX6wORAuOJE4T-LysLw:APA91bFYoDxJ-5rkK0yrOh-qy_HClSa2ewj-19vJNwgnlRMNRy75uq5pyRveT7-om0PncfgQyGvIm3hYOBvkJxAOxZ1lH8R3eDiNMoC_FGaZyVIBy4F6hqZhNn__g0B7srMc8deADcx2', '1.0.0', '0', NULL, NULL, '2021-07-27 03:04:30', '2021-07-27 03:04:30', '560bc908b02dadf9a6f95e5fdf40cd1e92e4338228360ba792b38dd997614b26dde8f14f1413dc07'),
(72, 14, 'b424fdc160756f11', 'eUE6_EafTxCmRuhImbSj7s:APA91bG7sfSB0pV3_QTq6AcDaRZft55RPIb1HnXeKo4rBU5U4AvVn3qKJkwZJD9fIA10XOIk2eX57TsoH5fLRNj0f-QCc2ivmpS--B_yhI_FJptn-dTGZfgAwGDev9Xl2SepZnbjd_Y1', '1.0.0', '0', NULL, NULL, '2021-07-27 04:35:53', '2021-07-27 04:35:53', '737485f5e44356e0176829bba33bad345cf1fc7a7d5bec0757f29a33d884765b3579d90785f4942a'),
(94, 14, '6b4c436b1befdecb', 'dYAMq0gFQEK9kb2zFxIa_g:APA91bH0ev0zLsIu_NMkjsOwb_-FvGG7KZpHBNFkwd5uOCyvXNmBJDbCb_FsQ_mjAmpMco95KExMAvDSy9p2LVfDZWs3SkJFTlj0EBCcWMne7FujONmYnracJT31j_ExRiMXD9OSYgp9', '1.0.0', '0', NULL, NULL, '2021-07-27 07:37:19', '2021-07-27 07:37:19', 'f74e20e094b2fd9e5477dc9ca2cde1fd9d9bf39b76145cdc1b894c6effa482ca7f5a2028e353f6a6'),
(118, 27, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:03:23', '2021-07-28 07:03:23', 'a374dfa266e14514e65817a879e7f6fe7748869de774e57ae79135f743a781900def0943ed8fb5b3'),
(120, 28, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:03:24', '2021-07-28 07:03:24', 'f0e7fd14bb7c2cc430572299908958498f3b7aaafb42578742df067cd84014fcd630d64028a1f464'),
(122, 29, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:15:33', '2021-07-28 07:15:33', '3d4ee8633ce23b3d15cc54ff265115f0dc7a42d241a505a15430c56fe5ec71552c21ff574a9d9c64'),
(124, 30, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:15:34', '2021-07-28 07:15:34', 'c8cddb9f745dc16c62dd82233aec0482916f5a45b7be47254ea9b13ad0bfaadf3059ddf035aa7cbd'),
(126, 31, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:26:09', '2021-07-28 07:26:09', '432412926b2ec2cc4f9bad1a34951ea8b0b610313975831c511b7df9f989d8c026274268404204f4'),
(128, 32, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:26:10', '2021-07-28 07:26:10', 'f955447e10ee8a7e6195525cc2a3eb635c1f8937d073250144404c4751c0c1c6a5f519776a286f2c'),
(130, 33, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:27:32', '2021-07-28 07:27:32', '700c0cc0369ecd69e00d094f9abbd86f8cd2fbfaefb91fc4fa34907cbdfb3abd2671730d6f3d6c1f'),
(132, 34, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:27:33', '2021-07-28 07:27:33', 'f61f372e16458db55e8492b613e100ea9167697cd66c8d63a94dd8bff788b02324ad4a3a72ef0940'),
(134, 35, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:30:04', '2021-07-28 07:30:04', '204bbafe26ef5e08ad9fb8bf62bdb193d06583b96a8a751d5aac95e9420b5b0cc0ce451482076276'),
(136, 36, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:30:06', '2021-07-28 07:30:06', '3d147723b6c410fc686a074959bf705dfc7602c6fe484d60effd6dc2a0b7e87c1ec2ed6892a9b66b'),
(140, 38, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:40:10', '2021-07-28 07:40:10', '540d728f07ca03f32b982f17dfaa6312940c21cb087311b863a319d90aed17cc3d6f8dcc15296050'),
(142, 39, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 07:40:11', '2021-07-28 07:40:11', '6c72fc424df4ff2101314e1c54bfb35ada3d71ae677a2f40feabfc435a8b537b9b322dcc45b75060'),
(150, 13, 'c4916e9f3739ac89', 'dkil32TpQtOR_cLvcdBe8u:APA91bGGV8S736p-8jvbcx7sBeEMurB0bM9m-PU8GNcVnqHl-Ysj3hXGDtXRxHyvPMp7nNKkwQL7Mn7Y54hZ0aaJ13MYdKLN-uUcjNLPBLRUMYbhcULjKg5O1N0LRIZncNF9fXHr6uI6', '1.0.0', '0', NULL, NULL, '2021-07-28 08:20:32', '2021-07-28 08:20:32', '1b5a4d83126ff6d70ca0eb345e9a53b55645a302126d2b33456a394af027734e6667f24ce1a68f59'),
(163, 26, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 02:07:28', '2021-07-29 02:07:28', '6064018483a1c283ba6609e7cebf0bc483ecb1c9c92c2795fd1fce51240bede0f9a49addfef8275b'),
(178, 40, '32276beca196ae35', 'fov5XPtTSg6TbwZgPhiO3E:APA91bH2DoyCuqchLuLP-n7kN6GQEOW32Mb6j-uAYq950E47NI1wzObcmOrv6qYPDlJ6cyH2j9XhbyhmPsv3hUS9txBdbkl7DL_1A4ruSIuYEDTILtCI2yMpudO251dH515NmsDtB_WF', '1.0.0', '0', NULL, NULL, '2021-07-29 04:55:19', '2021-07-29 04:55:19', '0aa1e4979a02a9e16e887df6bb83a7b8e213ded9994144fd86a114002dbb2c09a8511ab7117fd888'),
(180, 40, '3da42deb5f21f5cd', 'f0YBMK1VQH63DuJRNOO4at:APA91bEyzupZ2onPG9xMxg2tkRFdeKkooMDV9_z6B5WPsep92fdXv2bk-ye_WUlVxMIWN5YH2eLR_AVmMCDblPsKfb4ONMo3OdBASgOLpvXie9TGIIRx-88W4mwmXhLKoPv7gWNXqBnw', '1.0.0', '0', NULL, NULL, '2021-07-29 05:02:55', '2021-07-29 05:02:55', 'e4dd16488f58848a593b9338e1f114674161670a2c9d0c89244f648b5a4c8884f129e3afcd442e9a'),
(183, 40, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 05:52:49', '2021-07-29 05:52:49', 'b5f159741281df17dfcfb367b63a27faa58ed0c1edf93d54fc570b2b0b37021cb9bb9f38825800b0'),
(193, 13, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 07:32:52', '2021-07-29 07:32:52', 'd7b82d4075e7fd319cd81c042271a1cf9fbd4e3adf8a3c1bf4e1dea260411b0e813b4b71a06ed77b'),
(243, 42, '92c911f796e2691a', 'chmQW6EaSy6B_LRyoRR76U:APA91bHuHClnybFArya7bvQCXwtJh4Syim15SHhjuJQn0ILhz7AkKhy_wVRzP0RRDquKcipMC6p3AU3A8ZyGStli93bNVzXtKE_PjZfJDrJrqwztoZipnhwpMMPQlV0voXAjHBvAV-1G', '1.0.0', '0', NULL, NULL, '2021-07-31 01:04:04', '2021-07-31 01:04:04', 'f67b3a0d2ef3645451b8e4c13051029122984092ca0b48cef2d7d813f55e024c1036ab3b46119ff5'),
(278, 45, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 03:05:50', '2021-08-03 03:05:50', 'f8c27800f26f0eea9d2138b4ece5dc877813989379513c2269f44ea5f47cdafe7d706610186f9608'),
(279, 46, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 04:57:39', '2021-08-03 04:57:39', 'd8f8e50324d3f7f6cc248ccff1f60290c6b45bc4484bbd56d712ab9425bf5ee853c537418086bc4f'),
(280, 47, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 05:01:17', '2021-08-03 05:01:17', 'd0d3c6b6aba90069d939694a44c97520b2428d5e5881cb01ab25b125f49b54b702886ba98f2bf5cc'),
(281, 48, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 05:02:08', '2021-08-03 05:02:08', 'dfd3f4df49c53eac94235fb5d8bff130cfa4c0caef9bd8261a08e31b56e2d098f2b28d888d3b61bb'),
(282, 49, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 05:04:03', '2021-08-03 05:04:03', '9fcc33d869b6112b11557073a80006c408df473726a18f6b8c01b689fce917de436091acb482988c'),
(283, 50, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 05:32:03', '2021-08-03 05:32:03', '5adb0b1cacb713cec84eff339efb552694f490e2d50bf9fdf0b7cdcc91c8f47e106a356d34146d8b'),
(307, 7, '3da42deb5f21f5cd', 'd0y45JOtR_aNTHCxmyZ6Oq:APA91bHv8fjV3kzw6RkMXCPfuRszyjhpoFc2EwCqAu7rgjwm60b0Mp7C-lAWBVS0PCaTqPJ7jxlV0A9uq_JtXLFxT8f3-pyL3gzdXEVA34s5U01C_sr1_GWzeQ5pDkpNf1XmlRES9WET', '1.0.0', '0', NULL, NULL, '2021-08-04 02:43:15', '2021-08-04 02:43:15', 'a10a02b9c5325fd42f2d58ede7b8a8e84c4b41f7ed8bd84c12a557e1ea3baf1c392239e38d393fa1'),
(323, 53, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 02:59:05', '2021-08-05 02:59:05', '9bb5f4a0fc69754cf916aab5331c99bcf180fa1b8deaebfe176857a5a60769acd99e60e57bcb0a09'),
(346, 23, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 04:19:09', '2021-08-06 04:19:09', '8f1dbd8ae18686d2e44ad71781972e1e45e9e02525e900febb9078aa23fba981dd8a5e27ab1f8972'),
(361, 52, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 06:58:33', '2021-08-06 06:58:33', '88282a75619db4165f174cec9dbded5a61e235e878c3a7115808f7a7df7bc304e2db2134d803460f'),
(365, 54, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 23:32:06', '2021-08-06 23:32:06', '890a9d6a339053ec87539b3028978b3e8d6084f0e5abd1a06a6175d8715c8a13d8228f109e408d25'),
(367, 55, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 23:33:35', '2021-08-06 23:33:35', '844180a3379d94011ddc6813efc3359e89ba9956c87585d3ebe1a572b9665b03b00eecc7489b15d6'),
(387, 57, 'aaa9fb77dd31fb73', 'cKt9gsfGQrSTnBeC73geuU:APA91bEWlR4a8nbqwFp_m3Cwiz7hsjd-iZZNmYPHmlUgaxUV9oYlPepYLAnjpDjqgZOxB2TcoXgzskpfxTYHAdI3oteP4zub4b08AAmhzODe1Q7IhFoarultMfOpa7T8P3w1sNX5UCZi', '1.0.0', '0', NULL, NULL, '2021-08-08 21:25:07', '2021-08-08 21:25:07', 'a550076f90fd9222caef820ddc2287c54d5ad134b6ce1512ee3d633e964eec8fcb50e3134d288614'),
(392, 5, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-11 02:45:58', '2021-08-11 02:45:58', 'c91c2bff82f2f24a2dc6bc273cb48e30ec2c69e85977d264209983293808c8e79b2a090177c563cf'),
(395, 58, 'CBF869B6-7B6A-40B8-B8E5-138E0BC47ED8', 'cDbDN2cevUrat5Tl3y4w0k:APA91bG3zPzrf25Cr3CKQFn9-7vziejGRAh0Uzf-lQwWtU-cc-OrZZbQ1yNQaQzHrbRmvZZe66Bk2siKtY1-8llhgQmTsYxKOp1K8RGX4WIOno_WmwH8avj8CYhti6ZRpzxooZuBOu_2', '1.0.0', '1', NULL, NULL, '2021-08-11 03:47:28', '2021-08-11 03:47:28', '6886950e2ec61f25c08f3cf729173e176f0d502211ca620ef7e6032e8c9ddb30f0c1d8767eb02698'),
(416, 59, 'b8179fe1e4a076ee', 'cmiz_CHIQcOm3j77Tse0s-:APA91bEp_V1yHk8z5z8HjzjLP4WxZCGtG-jEIUti1SwbubKVmPtmeoa7P8QIm3QhA8Dw-DiSowUwQUVccdVuO6fvT7J7W_IgeE15uYjiRiqyd3sGnyLZHp_ScbYtZvTs3ySlhNXhQ5-v', '1.0.0', '0', NULL, NULL, '2021-08-15 12:04:36', '2021-08-15 12:04:36', 'a0832fd802afdb1e543616719f8b232ef67d51c051252a656666a52c37f9588176d9d146b448d985'),
(422, 64, '2064801dc71d3c19', 'emjbm_a_S36gIdBqSR4Y2X:APA91bEChBKkO3CiSUkZrERtdisTUTn73Wz4i8B59DHewRz8jZ-jBNlp9orSNipo-c_hRPFAUBUwnGnWkaJmtdinVBxiZx8yNL58aGDYrtV1fPRYISFaivmBEh1PjPVyXQYmto362zm-', '1.0.0', '0', NULL, NULL, '2021-08-16 00:48:27', '2021-08-16 00:48:27', '603811cb3718a8fc6a0abd0efbecd85036f428b73b2637ebe29d752ec3c6b0433f2c1d78eadd000e'),
(424, 65, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 03:11:12', '2021-08-16 03:11:12', 'd1211f7154eab1b448157f72580f6c38589a6847f45f296944f8638f1aae236bfbf240f93f1db65d'),
(439, 4, 'ee1f8532c3e33aa6', 'dzMBN9tTQdu-TLrbIz2Z-3:APA91bE9KiEwABogHW6KIgIc-kVbyqQOamI1D-hw30DjObp0-TCsgLs_JKeFxAd5nzFoktq0EE2ymHPhk4R071BuQpIrz3nHfBld3eINwvBGyLPtF6q_CGtQSaJrpbdYjSgCHcH6e8Nx', '1.0.0', '0', NULL, NULL, '2021-08-18 05:24:28', '2021-08-18 05:24:28', '491036adf64e3fa21e61d9e8b3b49f32b587459756541527d3308a60d6d2ea90205d96dede7f2fc4'),
(444, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 00:40:29', '2021-08-19 00:40:29', '107a7acbccdca821c4f0913ee37fa7affa007d72c12d50c9bfcc04cba8625b97d1b2e77fdab2b2c1');

-- --------------------------------------------------------

--
-- Table structure for table `failed_transfer`
--

CREATE TABLE `failed_transfer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) NOT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `failed_transfer`
--

INSERT INTO `failed_transfer` (`id`, `booking_id`, `delivery_boy_id`, `vendor_id`, `status`, `created_at`, `updated_at`) VALUES
(102, 725, NULL, 43, 1, '2021-04-03 01:58:50', '2021-04-03 02:02:04'),
(103, 730, NULL, 44, 1, '2021-04-03 02:36:31', '2021-04-03 02:39:04'),
(104, 783, 534, NULL, 1, '2021-04-06 02:45:04', '2021-04-20 02:05:04'),
(105, 804, NULL, 46, 0, '2021-04-15 01:14:29', '2021-04-15 01:14:29'),
(106, 804, 552, NULL, 1, '2021-04-15 01:14:30', '2021-04-20 02:05:05'),
(107, 805, NULL, 46, 0, '2021-04-15 01:23:50', '2021-04-15 01:23:50'),
(108, 805, 552, NULL, 1, '2021-04-15 01:23:50', '2021-04-20 02:05:07'),
(109, 808, NULL, 43, 1, '2021-04-16 06:51:41', '2021-05-23 18:30:07'),
(110, 809, NULL, 45, 0, '2021-04-20 00:56:22', '2021-04-20 00:56:22'),
(111, 823, NULL, 44, 1, '2021-04-20 01:58:55', '2021-04-20 06:31:05'),
(112, 823, 574, NULL, 1, '2021-04-20 01:58:56', '2021-04-20 02:05:09'),
(113, 824, NULL, 43, 1, '2021-04-20 02:25:13', '2021-05-23 18:30:09'),
(114, 830, NULL, 43, 1, '2021-04-20 02:35:54', '2021-05-24 07:51:06'),
(115, 840, NULL, 44, 1, '2021-04-21 05:00:11', '2021-05-24 07:51:07'),
(116, 859, NULL, 55, 0, '2021-05-24 05:05:50', '2021-05-24 05:05:50'),
(117, 859, 657, NULL, 0, '2021-05-24 05:05:50', '2021-05-24 05:05:50'),
(118, 880, NULL, 49, 0, '2021-05-27 08:12:41', '2021-05-27 08:12:41'),
(119, 880, 656, NULL, 0, '2021-05-27 08:12:41', '2021-05-27 08:12:41'),
(120, 881, NULL, 49, 0, '2021-06-01 07:36:36', '2021-06-01 07:36:36'),
(121, 881, 656, NULL, 0, '2021-06-01 07:36:36', '2021-06-01 07:36:36'),
(122, 884, NULL, 49, 0, '2021-06-01 07:42:46', '2021-06-01 07:42:46'),
(123, 884, 656, NULL, 0, '2021-06-01 07:42:46', '2021-06-01 07:42:46'),
(124, 888, NULL, 64, 0, '2021-06-04 04:47:23', '2021-06-04 04:47:23'),
(125, 888, 702, NULL, 0, '2021-06-04 04:47:24', '2021-06-04 04:47:24'),
(126, 895, NULL, 64, 0, '2021-06-14 01:17:53', '2021-06-14 01:17:53'),
(127, 895, 542, NULL, 0, '2021-06-14 01:17:53', '2021-06-14 01:17:53'),
(128, 896, NULL, 64, 0, '2021-06-14 01:37:07', '2021-06-14 01:37:07'),
(129, 896, 542, NULL, 0, '2021-06-14 01:37:07', '2021-06-14 01:37:07'),
(130, 897, NULL, 49, 0, '2021-06-14 08:15:43', '2021-06-14 08:15:43'),
(131, 897, 656, NULL, 0, '2021-06-14 08:15:44', '2021-06-14 08:15:44'),
(132, 913, NULL, 49, 0, '2021-06-15 08:07:30', '2021-06-15 08:07:30'),
(133, 920, NULL, 75, 0, '2021-07-01 23:00:34', '2021-07-01 23:00:34'),
(134, 920, 759, NULL, 0, '2021-07-01 23:00:34', '2021-07-01 23:00:34'),
(135, 911, NULL, 64, 0, '2021-07-14 01:56:15', '2021-07-14 01:56:15'),
(136, 911, 702, NULL, 0, '2021-07-14 01:56:15', '2021-07-14 01:56:15'),
(137, 936, NULL, 64, 0, '2021-07-14 02:07:49', '2021-07-14 02:07:49'),
(138, 936, 702, NULL, 0, '2021-07-14 02:07:49', '2021-07-14 02:07:49'),
(139, 1154, NULL, 46, 0, '2021-07-19 02:10:29', '2021-07-19 02:10:29'),
(140, 1154, 702, NULL, 0, '2021-07-19 02:10:29', '2021-07-19 02:10:29'),
(141, 1173, NULL, 44, 0, '2021-07-19 08:34:30', '2021-07-19 08:34:30'),
(142, 1175, NULL, 44, 0, '2021-07-20 02:01:49', '2021-07-20 02:01:49'),
(143, 1177, NULL, 44, 0, '2021-07-20 02:35:54', '2021-07-20 02:35:54'),
(144, 1177, NULL, 44, 0, '2021-07-20 02:37:47', '2021-07-20 02:37:47'),
(145, 1178, NULL, 44, 0, '2021-07-20 02:45:15', '2021-07-20 02:45:15'),
(146, 1178, 759, NULL, 0, '2021-07-20 02:45:16', '2021-07-20 02:45:16'),
(147, 1179, NULL, 44, 0, '2021-07-20 02:52:12', '2021-07-20 02:52:12'),
(148, 1179, 759, NULL, 0, '2021-07-20 02:52:13', '2021-07-20 02:52:13'),
(149, 1181, NULL, 44, 0, '2021-07-20 03:10:50', '2021-07-20 03:10:50'),
(150, 1181, 759, NULL, 0, '2021-07-20 03:10:50', '2021-07-20 03:10:50'),
(151, 1182, NULL, 44, 0, '2021-07-20 03:25:46', '2021-07-20 03:25:46'),
(152, 1182, 759, NULL, 0, '2021-07-20 03:25:46', '2021-07-20 03:25:46'),
(153, 1188, NULL, 44, 0, '2021-07-20 08:44:14', '2021-07-20 08:44:14'),
(154, 1186, NULL, 44, 0, '2021-07-20 08:45:48', '2021-07-20 08:45:48'),
(155, 1186, 759, NULL, 0, '2021-07-20 08:45:48', '2021-07-20 08:45:48'),
(156, 1188, NULL, 44, 0, '2021-07-20 08:46:17', '2021-07-20 08:46:17'),
(157, 1188, NULL, 44, 0, '2021-07-20 08:47:09', '2021-07-20 08:47:09'),
(158, 1189, NULL, 44, 0, '2021-07-20 08:51:07', '2021-07-20 08:51:07'),
(159, 1189, 759, NULL, 0, '2021-07-20 08:51:07', '2021-07-20 08:51:07'),
(160, 1197, NULL, 44, 0, '2021-07-21 02:44:00', '2021-07-21 02:44:00'),
(161, 1197, 759, NULL, 0, '2021-07-21 02:44:00', '2021-07-21 02:44:00'),
(162, 1198, NULL, 44, 0, '2021-07-21 05:26:12', '2021-07-21 05:26:12'),
(163, 1198, 759, NULL, 0, '2021-07-21 05:26:12', '2021-07-21 05:26:12'),
(164, 5, NULL, 1, 1, '2021-07-26 08:13:22', '2021-08-04 06:02:06'),
(165, 5, 2, NULL, 0, '2021-07-26 08:13:22', '2021-07-26 08:13:22'),
(166, 32, NULL, 1, 1, '2021-07-29 00:02:55', '2021-08-04 06:02:07'),
(167, 32, 2, NULL, 0, '2021-07-29 00:02:56', '2021-07-29 00:02:56'),
(168, 33, NULL, 1, 1, '2021-07-29 00:23:38', '2021-08-04 06:02:09'),
(169, 33, 7, NULL, 1, '2021-07-29 00:23:38', '2021-08-04 06:23:05'),
(170, 34, NULL, 1, 1, '2021-07-29 01:00:37', '2021-08-04 06:02:10'),
(171, 34, 6, NULL, 0, '2021-07-29 01:00:38', '2021-07-29 01:00:38'),
(172, 35, NULL, 1, 1, '2021-07-29 01:06:12', '2021-08-04 06:02:12'),
(173, 35, 7, NULL, 1, '2021-07-29 01:06:13', '2021-08-04 06:23:07'),
(174, 45, NULL, 2, 0, '2021-07-29 05:43:56', '2021-07-29 05:43:56'),
(175, 45, 40, NULL, 0, '2021-07-29 05:43:56', '2021-07-29 05:43:56'),
(176, 50, NULL, 2, 0, '2021-07-29 06:00:48', '2021-07-29 06:00:48'),
(177, 50, 40, NULL, 0, '2021-07-29 06:00:48', '2021-07-29 06:00:48'),
(178, 64, NULL, 1, 1, '2021-07-30 00:42:36', '2021-08-04 06:02:15'),
(179, 64, 7, NULL, 1, '2021-07-30 00:42:37', '2021-08-04 06:23:09'),
(180, 82, NULL, 1, 1, '2021-08-03 00:49:54', '2021-08-04 06:02:16'),
(181, 82, 2, NULL, 0, '2021-08-03 00:49:54', '2021-08-03 00:49:54'),
(182, 83, NULL, 1, 1, '2021-08-03 01:00:44', '2021-08-04 06:02:18'),
(183, 87, NULL, 1, 1, '2021-08-03 01:58:20', '2021-08-04 06:02:19'),
(184, 87, 7, NULL, 1, '2021-08-03 01:58:20', '2021-08-04 06:23:11'),
(185, 96, NULL, 1, 1, '2021-08-03 07:31:41', '2021-08-04 06:02:21'),
(186, 78, NULL, 1, 1, '2021-08-04 01:08:27', '2021-08-04 06:02:22'),
(187, 78, 7, NULL, 1, '2021-08-04 01:08:27', '2021-08-04 06:23:12'),
(188, 99, NULL, 2, 0, '2021-08-04 01:10:40', '2021-08-04 01:10:40'),
(189, 99, 7, NULL, 1, '2021-08-04 01:10:41', '2021-08-04 06:23:13'),
(190, 100, NULL, 1, 1, '2021-08-04 02:13:39', '2021-08-04 06:02:24'),
(191, 100, 7, NULL, 1, '2021-08-04 02:13:40', '2021-08-04 06:23:14'),
(192, 101, NULL, 2, 0, '2021-08-04 02:47:16', '2021-08-04 02:47:16'),
(193, 101, 7, NULL, 1, '2021-08-04 02:47:17', '2021-08-04 06:23:16'),
(194, 102, NULL, 1, 1, '2021-08-04 05:07:11', '2021-08-04 06:02:26'),
(195, 102, 7, NULL, 1, '2021-08-04 05:07:12', '2021-08-04 06:23:17'),
(196, 103, NULL, 1, 1, '2021-08-04 05:30:21', '2021-08-04 06:02:28'),
(197, 103, 7, NULL, 1, '2021-08-04 05:30:21', '2021-08-04 06:23:18'),
(198, 104, 7, NULL, 1, '2021-08-04 06:17:25', '2021-08-04 06:23:19'),
(199, 105, NULL, 1, 1, '2021-08-04 06:30:25', '2021-08-05 04:28:09'),
(200, 105, 7, NULL, 1, '2021-08-04 06:30:26', '2021-08-04 06:36:30'),
(201, 106, NULL, 1, 1, '2021-08-04 06:45:04', '2021-08-05 04:28:10'),
(202, 108, NULL, 1, 1, '2021-08-05 04:09:40', '2021-08-05 04:28:11'),
(203, 109, NULL, 1, 1, '2021-08-05 04:22:54', '2021-08-05 04:28:12'),
(204, 109, 7, NULL, 1, '2021-08-05 04:22:54', '2021-08-05 04:28:13');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `category_id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(11, 7, 'fdgh', 'dsgh', '2021-07-21 00:34:30', '2021-07-21 00:34:30'),
(12, 7, 'fdgs', 'sdfhW', '2021-07-21 00:34:30', '2021-07-21 00:34:30'),
(13, 6, 'Lorem ipsum dolor sit amet, consetetur sadipscing?', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea', '2021-08-13 01:47:53', '2021-08-13 01:47:53'),
(14, 6, 'Lorem ipsum dolor sit amet, consetetur sadipscing', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea', '2021-08-13 01:47:53', '2021-08-13 01:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE `faq_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`id`, `category`, `created_at`, `updated_at`) VALUES
(6, 'General', '2021-04-03 01:10:02', '2021-04-03 01:10:02'),
(7, 'Product', '2021-06-30 09:21:05', '2021-06-30 09:21:05'),
(8, 'digital', '2021-07-21 00:34:12', '2021-07-21 00:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `fav_unfac_shops`
--

CREATE TABLE `fav_unfac_shops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `isFavourite` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fav_unfac_shops`
--

INSERT INTO `fav_unfac_shops` (`id`, `shop_id`, `user_id`, `isFavourite`, `created_at`, `updated_at`) VALUES
(1, 1, 12, 0, '2021-07-26 22:57:23', '2021-07-26 23:17:05'),
(2, 1, 13, 0, '2021-07-27 04:37:31', '2021-07-27 07:41:24'),
(3, 1, 14, 1, '2021-07-27 04:43:18', '2021-07-27 07:39:00'),
(4, 2, 42, 0, '2021-07-31 02:43:52', '2021-07-31 02:44:01');

-- --------------------------------------------------------

--
-- Table structure for table `help_and_supports`
--

CREATE TABLE `help_and_supports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `feedback_title` text COLLATE utf8_unicode_ci NOT NULL,
  `feedback_description` text COLLATE utf8_unicode_ci NOT NULL,
  `query_status` tinyint(1) NOT NULL,
  `feedback` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `admin_reply` text COLLATE utf8_unicode_ci NOT NULL,
  `ticket_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `help_and_supports`
--

INSERT INTO `help_and_supports` (`id`, `booking_id`, `user_id`, `shop_id`, `feedback_title`, `feedback_description`, `query_status`, `feedback`, `created_at`, `updated_at`, `image`, `admin_reply`, `ticket_id`) VALUES
(25, 759, 1, 44, '0ADA20210403123451', 'bzbsbszbbs shjs hsj sjs. zbsksksj sbsjsjsn jsjsjjs z jskskjs. sjsnns', 1, NULL, '2021-04-03 07:20:22', '2021-04-03 07:20:46', '[]', 'fgyty', 'ADD05022SD0S'),
(26, 851, 632, 46, '0DAA20210513131313', 'xvvxvxvxxjfxjffxjfxfjxjfxjfxfjjfxjfxjfxfjxjgxjfxjfxjfxfjxxjgxjgxjgxitxit', 0, NULL, '2021-05-14 04:21:38', '2021-05-14 04:21:38', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1620985815jpg\"]', '', 'SA005138DSAA'),
(27, 876, 656, 49, '0AS020210527104903', 'vgtghh  bb hb hf b  h  hb  h  b  b    bb  hb bbbhhf bh  hhb', 1, NULL, '2021-05-27 05:51:29', '2021-05-27 05:51:49', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1622114465jpg\"]', 'rfagdfbdf fbfdbg graegb', 'DSAS2129D0A0'),
(28, 892, 665, 46, 'A0SD20210610104037', 'jgcjgckhckgcgj, nv, nv, nv, nv, vn, jv, jg, jg, jgxjgxgjjgxjgxfjxjfxfhxhffxfh', 0, NULL, '2021-06-11 00:50:32', '2021-06-11 00:50:32', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1623392418jpg\"]', '', 'DAS02032D0D0'),
(29, 895, 542, 64, 'DDD020210614063332', 'gdfnnfmgvhrdhhdhdjfnf ktjfjfjtg  tkkthfnhdhf fjhfhd jdjdj fjdjd fjrjf mfjj gkgkfk fnfjr gkfkff', 0, NULL, '2021-06-14 01:23:22', '2021-06-14 01:23:22', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1623653458jpg\"]', '', 'SS005322A00A'),
(30, 917, 758, 49, 'SD0A20210630144259', 'snnnfxnn bd  c f d x  d x d x f bs d d r bdbdjsc  f rd  f g jd fjeb fjd ddjdrr d', 1, NULL, '2021-06-30 09:29:36', '2021-06-30 09:29:59', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625065152jpg\",\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625065160jpg\"]', 'hilkh', 'AA005936A00A'),
(31, 920, 759, 75, 'SDA020210702042023', 'djxbnd n  dbx  d d d d  d d ddbx x  dhd d dfff   f f f f f f ff d d d d d dxb', 1, NULL, '2021-07-01 22:55:35', '2021-07-01 22:56:18', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625199899jpg\",\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625199907jpg\"]', 'ool;n', 'ASAS2535S00D'),
(32, 920, 759, 75, 'SDA020210702042023', 'nd d d d e ds d d e s s s s s ss bs e s sbsw e ss w e s s sbsw sbsw s s sbs s sbs s', 1, NULL, '2021-07-01 22:56:52', '2021-07-01 22:59:34', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625200010jpg\"]', 'khklhlkhnk', 'DSDD2652SSS0'),
(33, 920, 758, 75, 'SDA020210702042023', 'znzbzbnsnsbz d dbs d dd be s  d dd d d d d ss ds s sjs s s ssns s z', 1, NULL, '2021-07-01 23:01:24', '2021-07-01 23:01:41', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1625200270jpg\"]', 'jkvbjbkj', '00A031240DAA'),
(34, 920, 759, 75, 'SDA020210702042023', 'dnnd dd  ddbd d d d ddn dd dddd dd  dd dddnd dd d dnddd dde d', 0, NULL, '2021-07-20 03:44:01', '2021-07-20 03:44:01', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1626772433image_cropper_1626772430615.jpg\"]', '', '0DAS1401DDAS'),
(35, 1174, 753, 44, '00SS20210719145253', 'dbdx x d d dd f ff  f f f xx f ff fff f ffbf d dbdd d d dbf f f f  ff  f f', 1, NULL, '2021-07-21 00:35:21', '2021-07-21 01:01:50', '[\"https:\\/\\/limitless-images-test.s3.amazonaws.com\\/1626847507image_cropper_1626847504949.jpg\"]', 'cffdfvvf', 'ADDD0521A0DD');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `stock_quantity` bigint(20) NOT NULL,
  `price` double NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_id` bigint(20) NOT NULL,
  `shop_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `is_disabled` int(11) NOT NULL DEFAULT '0' COMMENT '0=>enable,1=>disable',
  `discount` double(8,2) NOT NULL,
  `new_price` double(8,2) NOT NULL,
  `gst_enable` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `product_name`, `product_description`, `stock_quantity`, `price`, `product_image`, `created_at`, `updated_at`, `category_name`, `company_name`, `shop_id`, `shop_name`, `category_id`, `is_disabled`, `discount`, `new_price`, `gst_enable`) VALUES
(1, 'Coffee', 'asdf', 477, 50, NULL, '2021-07-22 06:21:37', '2021-08-18 13:04:04', 'Grocery', 'Fidalgo', 1, 'Test vendor', 1, 0, 10.00, 45.00, 1),
(2, 'Wine', 'adf', 576, 10, NULL, '2021-07-22 06:22:27', '2021-08-05 04:41:34', 'Grocery', 'Fidalgo', 1, 'Test vendor', 1, 0, 10.00, 9.00, 0),
(3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 198, 80, NULL, '2021-07-23 00:28:36', '2021-08-11 03:58:23', 'Fruits', 'Fidalgo', 2, 'Reliance Shop', 2, 0, 5.00, 76.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `uuid`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `conversions_disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\TableBannersLocationWise', 1, 'f850a4ed-4afd-457f-9208-42a0088f2656', 'banner_image', '1626953942coffee_shop_circle_label_6821693', '1626953942coffee_shop_circle_label_6821693.jpg', 'image/jpeg', 'media', 'media', 57708, '[]', '{\"name\":\"coffee_shop_circle_label_6821693.jpg\",\"file_name\":\"coffee_shop_circle_label_6821693.jpg\",\"width\":368,\"height\":368,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 1, '2021-07-22 06:09:09', '2021-07-22 06:09:09'),
(2, 'App\\Models\\Category', 1, '63b9ab58-006a-4c7f-9846-6bc5917d96e3', 'Category_Image', '1626953973download(20)', '1626953973download(20).jpg', 'image/jpeg', 'media', 'media', 14703, '[]', '{\"name\":\"download (20).jpg\",\"file_name\":\"download (20).jpg\",\"width\":212,\"height\":237,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 2, '2021-07-22 06:10:04', '2021-07-22 06:10:04'),
(3, 'App\\Models\\Category', 2, 'd1a63ab1-e282-44a1-9068-e1132a9f62b6', 'Category_Image', '1626954477download(12)', '1626954477download(12).jpg', 'image/jpeg', 'media', 'media', 9641, '[]', '{\"name\":\"download (12).jpg\",\"file_name\":\"download (12).jpg\",\"width\":247,\"height\":204,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 3, '2021-07-22 06:18:03', '2021-07-22 06:18:03'),
(4, 'App\\Models\\AdminUsers', 267, '44206278-6276-49e0-a061-53c64146bef1', 'shop_image', '1626954615coffee-shop-banners-designs_23-2148607795', '1626954615coffee-shop-banners-designs_23-2148607795.jpg', 'image/jpeg', 'media', 'media', 97532, '[]', '{\"name\":\"coffee-shop-banners-designs_23-2148607795.jpg\",\"file_name\":\"coffee-shop-banners-designs_23-2148607795.jpg\",\"width\":626,\"height\":626,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 4, '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(5, 'App\\Models\\AdminUsers', 267, '94e9121d-72a5-40dc-b568-560d88737c60', 'Gallery', '1626954607banner-coffee-shop_23-2148610744', '1626954607banner-coffee-shop_23-2148610744.jpg', 'image/jpeg', 'media', 'media', 43455, '[]', '{\"name\":\"banner-coffee-shop_23-2148610744.jpg\",\"file_name\":\"banner-coffee-shop_23-2148610744.jpg\",\"width\":626,\"height\":417,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 5, '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(6, 'App\\Models\\Item', 1, '069de610-24f8-4118-9cd7-ed6f843e3217', 'product_images', '1626954689images(3)', '1626954689images(3).jpg', 'image/jpeg', 'media', 'media', 13417, '[]', '{\"name\":\"images(3).jpg\",\"file_name\":\"images(3).jpg\",\"width\":194,\"height\":259,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 6, '2021-07-22 06:21:37', '2021-07-22 06:21:37'),
(7, 'App\\Models\\Item', 2, '6fcd1062-8169-4eb0-8ab6-87d3ebe2a6e0', 'product_images', '1626954740images(2)', '1626954740images(2).jpg', 'image/jpeg', 'media', 'media', 8137, '[]', '{\"name\":\"images(2).jpg\",\"file_name\":\"images(2).jpg\",\"width\":225,\"height\":225,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 7, '2021-07-22 06:22:27', '2021-07-22 06:22:27'),
(8, 'App\\Models\\Coupon', 1, 'd45a3bae-003f-4ab4-b46e-167af5e12a30', 'coupon_image', '1626954825coupon', '1626954825coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 8, '2021-07-22 06:23:54', '2021-07-22 06:23:54'),
(9, 'App\\Models\\Coupon', 2, '42a54fdc-86ea-4ec8-a744-6245d3788070', 'coupon_image', '1626954825coupon', '1626954825coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 9, '2021-07-22 06:24:15', '2021-07-22 06:24:15'),
(10, 'App\\Models\\Coupon', 3, 'e5caedd8-720a-41d8-87b9-1ffbb5ff7fe1', 'coupon_image', '1626954825coupon', '1626954825coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 10, '2021-07-22 06:24:37', '2021-07-22 06:24:37'),
(11, 'App\\Models\\Coupon', 4, 'fcd17935-7894-4cae-8f16-2d3d9148b16d', 'coupon_image', '1626954825coupon', '1626954825coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 11, '2021-07-22 06:26:35', '2021-07-22 06:26:35'),
(12, 'App\\Models\\TableBannersLocationWise', 2, '7714be49-cc2d-4838-bb77-3521fa93c81c', 'banner_image', '1626955075images(4)', '1626955075images(4).jpg', 'image/jpeg', 'media', 'media', 10235, '[]', '{\"name\":\"images(4).jpg\",\"file_name\":\"images(4).jpg\",\"width\":306,\"height\":165,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 12, '2021-07-22 06:28:05', '2021-07-22 06:28:05'),
(13, 'App\\Models\\Coupon', 7, '33ef3808-b3ee-4034-8292-69690a2ca555', 'coupon_image', '1626955337coupon', '1626955337coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 13, '2021-07-22 06:32:31', '2021-07-22 06:32:31'),
(14, 'App\\Models\\Coupon', 8, '385bce38-f170-4ccc-8eae-2acfedd5a61c', 'coupon_image', '1626955409coupon', '1626955409coupon.png', 'image/png', 'media', 'media', 7961, '[]', '{\"name\":\"coupon.png\",\"file_name\":\"coupon.png\",\"width\":254,\"height\":198,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 14, '2021-07-22 06:33:37', '2021-07-22 06:33:37'),
(15, 'App\\Models\\AdminUsers', 268, 'e0c2a95b-3d28-4567-813e-a4dca14a5c12', 'shop_image', '1627019843groceries', '1627019843groceries.jpeg', 'image/jpeg', 'media', 'media', 14095, '[]', '{\"name\":\"groceries.jpeg\",\"file_name\":\"groceries.jpeg\",\"width\":259,\"height\":194,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 15, '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(16, 'App\\Models\\Item', 3, '4fdd41e0-1ead-4180-8268-631e24693feb', 'product_images', '1627019903coldcoffee', '1627019903coldcoffee.jpeg', 'image/jpeg', 'media', 'media', 7303, '[]', '{\"name\":\"cold coffee.jpeg\",\"file_name\":\"cold coffee.jpeg\",\"width\":183,\"height\":275,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 16, '2021-07-23 00:28:36', '2021-07-23 00:28:36'),
(17, 'App\\Models\\AdminUsers', 270, '3a1b3786-7bf5-4394-8690-96f4e5a3cc28', 'shop_image', '1629047904image', '1629047904image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 17, '2021-08-15 11:48:59', '2021-08-15 11:49:00'),
(18, 'App\\Models\\AdminUsers', 270, 'a9fa62f2-61b7-4c72-b555-d1f37b2ecdb3', 'Gallery', '1629047899image', '1629047899image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 18, '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(19, 'App\\Models\\AdminUsers', 272, '0e087bba-4fb0-4ef5-aee6-07969ed43222', 'shop_image', '1629047904image', '1629047904image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 19, '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(20, 'App\\Models\\AdminUsers', 272, '4800d2bd-d67d-41b4-b70d-da5786a86922', 'Gallery', '1629047899image', '1629047899image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 20, '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(21, 'App\\Models\\AdminUsers', 274, '8089e0ad-66cc-4bf6-820e-1f8d72f2ff54', 'shop_image', '1629047904image', '1629047904image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 21, '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(22, 'App\\Models\\AdminUsers', 274, 'ba279d33-9a1b-41fd-9bea-ec04570ecd97', 'Gallery', '1629047899image', '1629047899image.png', 'image/png', 'media', 'media', 6507, '[]', '{\"name\":\"image.png\",\"file_name\":\"image.png\",\"width\":300,\"height\":168,\"generated_conversions\":{\"thumb_200\":true}}', '[]', 22, '2021-08-15 11:50:49', '2021-08-15 11:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_08_24_000000_create_activations_table', 1),
(9, '2017_08_24_000000_create_admin_activations_table', 1),
(10, '2017_08_24_000000_create_admin_password_resets_table', 1),
(11, '2017_08_24_000000_create_admin_users_table', 1),
(12, '2018_07_18_000000_create_wysiwyg_media_table', 1),
(13, '2020_10_12_060202_create_otps_table', 1),
(14, '2020_10_14_044142_create_user_addresses_table', 1),
(15, '2020_10_21_000000_add_last_login_at_timestamp_to_admin_users_table', 1),
(16, '2020_12_02_064931_create_media_table', 1),
(17, '2020_12_02_064931_create_translations_table', 1),
(18, '2020_12_02_071839_create_categories', 1),
(19, '2020_12_02_073712_create_vendors', 1),
(20, '2020_12_03_093824_vendor_details', 1),
(21, '2020_12_03_095603_companies', 1),
(22, '2020_12_03_100929_products', 1),
(23, '2020_12_03_111207_items', 1),
(24, '2020_12_03_112057_add_category_names_to_items_table', 1),
(25, '2020_12_05_052917_create__categories_user_table', 1),
(26, '2020_12_08_065144_change_column', 2),
(27, '2020_12_08_080211_change_phone', 3),
(28, '2020_12_09_064021_add_user-type', 4),
(29, '2020_12_09_124319_add_is_active', 5),
(30, '2020_12_10_131552_add_address_ptimary', 6),
(31, '2020_12_10_132030_add_address_primary', 7),
(32, '2020_12_10_132451__is__primary', 8),
(33, '2020_12_10_132816__is_block', 8),
(34, '2020_12_11_105854_is_primary', 9),
(35, '2020_12_02_064931_create_permission_tables', 10),
(36, '2020_12_02_064936_fill_default_admin_user_and_permissions', 10),
(37, '2020_12_02_075659_fill_permissions_for_category', 10),
(38, '2020_12_02_075718_fill_permissions_for_vendor', 10),
(39, '2020_12_03_095832_fill_permissions_for_company', 10),
(40, '2020_12_03_101350_fill_permissions_for_product', 10),
(41, '2020_12_03_111428_fill_permissions_for_item', 10),
(42, '2020_12_16_050640_add_pincode_to_useraddresses_table', 11),
(43, '2020_12_15_054343_fill_permissions_for_user', 12),
(44, '2020_12_15_054419_fill_permissions_for_admin', 12),
(45, '2020_12_17_103904_add__country_iso_code', 12),
(46, '2020_12_21_071326_fill_permissions_for_admin-vendor', 13),
(47, '2020_12_21_075903_create_admin-vendor__table', 14),
(52, '2021_01_02_100259_create_stripe_token_table', 15),
(53, '2021_01_02_102613_fill_permissions_for_stripe-connect', 15),
(54, '2021_01_02_102921_fill_permissions_for_stripe-token', 15),
(55, '2021_01_04_061228_add_country_code_table', 15),
(56, '2021_01_05_054353_add_lat_long_to_admin_vendors', 16),
(57, '2021_01_05_054845_add_lat_long_to_admin_vendors', 17),
(58, '2020_12_21_081425_create_admin_vendor_table', 2),
(59, '2020_12_22_121855_add_user_type', 2),
(60, '2021_01_02_142510_fill_permissions_for_stripe-token', 18),
(61, '2021_01_03_050440_create_coupons_table', 19),
(62, '2021_01_03_160053_add_stripe_connect_id', 19),
(63, '2021_01_04_152818_fill_permissions_for_coupon', 19),
(64, '2021_01_11_102255_create_deliveryboy_temporary_documents', 20),
(65, '2021_01_13_124316_add_name_deliveryboy_temporary_doeuments', 21),
(66, '2021_01_14_074733_add_vehicle_company', 22),
(67, '2021_01_14_075348_add_vehicle_company', 23),
(68, '2020_08_13_092754_add_stripe_ids_in_users_table', 24),
(69, '2020_09_07_191331779537_create_stripe_payment_table', 24),
(70, '2020_12_01_103507_add_payment_id_in_stripe_payment_reocrd', 24),
(71, '2021_01_18_101803_add_col_availability', 25),
(72, '2021_01_18_130852_add_created_by', 25),
(73, '2021_01_08_055516_add_description_col', 26),
(74, '2021_01_08_074726_create_block_reasons_vendor', 27),
(75, '2021_01_08_115352_add_is_block', 28),
(76, '2021_01_11_103039_drop_country__i_s_o_code_admin_vendors', 28),
(77, '2021_01_12_110537_add_default_value_is_block', 28),
(78, '2021_01_14_051536_add_updated_at_reset_password', 29),
(79, '2021_01_19_103856_add_col_status', 30),
(80, '2021_01_20_081352_add_default_value_col_stat', 30),
(81, '2021_01_20_110928_rename_col_name_temp', 30),
(82, '2021_01_20_112100_rename_col_name_main', 30),
(83, '2021_01_27_053623_add_col_initems', 30),
(84, '2021_01_28_140451_booking_detail', 31),
(85, '2021_01_28_140504_order_detail', 31),
(86, '2021_01_29_051357_change_price_type_items', 31),
(87, '2021_01_29_063718_add_is_disable_col_items', 31),
(88, '2021_01_29_082118_add_timestamps_main', 31),
(89, '2021_01_29_072404_create_users_based_location', 32),
(90, '2021_01_29_112510_add_coupon_id_column_to_booking_details', 33),
(91, '2021_01_29_123538_add_cols_shoprating', 33),
(92, '2021_01_29_101351_create_charges_table', 34),
(93, '2021_01_29_103330_fill_permissions_for_charge', 34),
(94, '2021_01_29_171948_booking_tracking_history', 35),
(95, '2021_01_29_180641_various_charges', 35),
(96, '2021_01_29_181336_booking_various_charges', 35),
(97, '2021_01_30_043655_drop_order_i_d_column_from_booking_detail_table', 35),
(98, '2021_01_30_051033_add_after_service_charge_total_amount_column_in_booking_details_table', 35),
(99, '2021_01_30_071018_add_new_booking_related_columns_in_booking_details_table', 36),
(100, '2021_01_30_094836_create_new_pageforpoliciesnadterms', 36),
(101, '2021_01_30_132035_various_charges_on_booking', 37),
(102, '2021_01_31_033443_rename_various_charges_on_booking_table', 37),
(103, '2021_02_01_071215_changes_nameofcols', 37),
(104, '2021_02_01_073426_create_booking_reject_reason', 38),
(105, '2021_02_01_094922_add_shop_type', 38),
(106, '2021_02_01_103028_rename_cols', 38),
(107, '2021_01_30_124123_fill_permissions_for_booking-detail', 39),
(108, '2021_01_30_132044_drop_charges_table', 39),
(109, '2021_01_30_132903_fill_permissions_for_various-charge', 39),
(110, '2021_02_01_102930_create_table_banners_location_wise', 40),
(111, '2021_02_01_105641_add_shop_id_and_primary_column_in_coupons_table', 40),
(112, '2021_02_01_121421_add_dis_newprice_items', 40),
(113, '2021_02_02_062402_add_default_isprimary', 41),
(114, '2021_02_02_113610_add_status_cols_temp_docs', 41),
(115, '2021_02_03_054540_drop_document_reject_reason_table', 41),
(116, '2021_02_02_110838_fill_permissions_for_table-banners-location-wise', 42),
(117, '2021_02_04_063718_change_col_type_items', 43),
(118, '2021_02_05_045756_add_stripe_user_id', 44),
(119, '2021_02_05_052801_create_review_table', 44),
(120, '2021_02_05_065206_drop_is_block_adminusers', 44),
(121, '2021_02_05_065531_add_forbidden_col', 44),
(122, '2021_02_05_080620_add_phone_number_col', 44),
(130, '2021_02_09_102916_drop_col_cat', 49),
(131, '2021_02_09_115301_rename_col_categories', 50),
(132, '2021_02_09_133003_rename_cols_companies', 51),
(133, '2021_02_09_134914_add_country_code_admin_users', 51),
(134, '2021_02_10_050803_drop_profile_pic_vendor', 52),
(135, '2021_02_10_053116_rename_col_main', 53),
(136, '2021_02_10_053140_rename_col_temp', 53),
(137, '2021_02_10_060152_drop_cols_users', 54),
(138, '2021_02_10_061639_drop_table_products', 55),
(139, '2021_02_10_061657_drop_table_categories_user', 55),
(140, '2021_02_10_061712_drop_table_users_based_location', 55),
(141, '2021_02_10_061725_drop_table_vendors', 55),
(142, '2021_02_10_061736_drop_table_vendor_details', 55),
(143, '2021_02_10_071139_rename_cols_temp_stat', 56),
(144, '2021_02_10_073344_add_type_column_in_booking_tracking_history_table', 57),
(145, '2021_02_10_110600_add_cols_admin_vendors', 57),
(146, '2021_02_11_062335_rename_admin_vend_col', 58),
(148, '2021_02_11_062654_add_columns_in_booking_details_table', 59),
(149, '2021_02_11_101348_add_tip_column_to_delivery_boy', 59),
(150, '2021_02_11_104501_modify_columns_in_booking_tracking_history_table', 59),
(151, '2021_02_11_112243_modify_columns_in_booking_detail_table', 59),
(152, '2021_02_11_115028_add_instruction_column_in_booking_details_table', 59),
(153, '2021_02_11_121237_add_columns_in_review_table', 59),
(154, '2021_02_11_123046_make_tip_null_booking_details', 59),
(155, '2021_02_11_125757_add_remove_cols_booking_reject_reason', 59),
(156, '2021_02_12_100019_add_columns_in_booking_detail_table', 60),
(157, '2021_02_12_131647_make_null_charges_vendor', 60),
(158, '2020_09_15_114237_create_device_details_table', 61),
(159, '2020_10_26_112705_create_notification_table', 61),
(160, '2021_02_13_054924_create_help_and_supports_table', 62),
(161, '2021_02_13_055559_create_booking_notifications_table', 62),
(162, '2021_02_13_055649_add_cols_in_booking_details_table', 62),
(163, '2021_02_13_072337_rename_booking_detail_col', 62),
(164, '2021_02_15_045237_add_columns_in_booking_details', 62),
(165, '2021_02_15_060954_add_cols_in_helpandsupport', 62),
(166, '2021_02_13_081131_fill_permissions_for_help-and-support', 63),
(167, '2021_02_16_074834_make_null_col_booking_details', 64),
(168, '2021_02_16_115846_make_null_review', 65),
(169, '2021_02_17_052243_add_rating_booking_notification_table', 66),
(170, '2021_02_17_062955_add_booking_code', 67),
(171, '2021_02_18_053335_add_price_column_in_order_detail_table', 68),
(172, '2021_02_18_121655_default_stat_temp_table', 69),
(173, '2021_02_19_053524_add_columnin_delivery_booking_temp_table', 70),
(174, '2021_02_19_053542_add_columnin_delivery_booking_table', 70),
(175, '2021_02_19_073921_change_start_end_date', 71),
(176, '2021_02_19_125352_make_null_city_user_address', 72),
(177, '2018_08_08_100000_create_telescope_entries_table', 73),
(178, '2021_02_22_065109_add_feedback_help_support', 73),
(179, '2021_02_22_100759_create_admin_settings_table', 73),
(180, '2021_02_22_102815_null_false_admin_setting', 73),
(181, '2021_02_22_101010_fill_permissions_for_admin-setting', 74),
(182, '2021_02_22_130200_make_null_start_end_date_coupon', 75),
(183, '2021_02_23_075143_add_col_admin_setting', 76),
(184, '2021_02_23_080424_add_col_payment_status_stripe_payment_record', 76),
(185, '2021_02_25_075436_add_new_columns_in_booking_details_table', 77),
(186, '2021_02_26_101129_add_columns_in_booking_history_table', 78),
(187, '2021_03_02_055056_update_scheduled_time_column_in_booking_details_table', 79),
(188, '2021_03_03_074629_add_column_in_device_details_table', 80),
(189, '2021_03_04_101309_change_col_type_desc_admin_vendor', 81),
(190, '2021_03_05_121836_make_null_userid_bookingtrackinghistor', 82),
(191, '2021_03_09_055944_add_booking_id_column_in_reviews_table', 83),
(192, '2021_03_08_055248_create_faq_category_table', 84),
(193, '2021_03_08_055521_create_faq_table', 84),
(194, '2021_03_09_095931_create_user_address_location_table', 84),
(195, '2021_03_08_060301_fill_permissions_for_faq-category', 85),
(196, '2021_03_08_060509_fill_permissions_for_faq', 85),
(197, '2021_03_11_103643_add_column_in_admin_settings_table', 86),
(198, '2021_03_12_072249_add_columns_in_user_based_address_table', 87),
(199, '2021_03_13_050642_add_columns_in_admin_settings', 88),
(200, '2021_03_15_100915_add_columns_in_help_and_support_table', 88),
(201, '2021_03_16_132146_addcols_in_help_and_supports', 89),
(202, '2021_03_17_043551_add_ticket_id_column_in_help_and_supports_table', 90),
(203, '2021_03_17_053406_add_availability_column_in_admin_vendor_table', 91),
(204, '2021_03_17_061845_dropcol_in_help_and_supports', 92),
(205, '2021_03_17_093706_add_new_table_failed_transfers', 92),
(207, '2021_03_17_101516_make_some_columns_null_in_failed_transfers', 93),
(208, '2021_03_17_133135_add_timezone_column_in_booking_details_table', 94),
(209, '2021_03_24_052927_add_date_of_birth_column_in_user_table', 1),
(210, '2021_04_28_104548_create_app_packages_table', 95),
(211, '2021_04_28_104614_create_app_versions_table', 95),
(212, '2021_04_12_114928_create_admin_fcm_tokens_table', 96),
(213, '2021_05_25_054213_create_fav_unfac_shops_table', 96),
(214, '2021_05_31_075457_add_referral_columns_in_users_table', 97),
(215, '2021_06_01_062213_add_user_id_column_in_coupons_table', 98),
(216, '2021_06_01_075127_add_merchant_i_d_in_table_banners_location_wise_table', 99),
(217, '2021_06_08_102135_make_nullable_columns_in_table_banners_location_wise_table', 99),
(218, '2021_04_28_105359_fill_permissions_for_app-package', 100),
(219, '2021_04_28_105434_fill_permissions_for_app-version', 100),
(220, '2021_06_23_103958_create_slots_table', 100),
(221, '2021_06_24_120946_add_col_country_iso_code', 101),
(222, '2021_06_26_115526_create_various_charge_relation_table', 102),
(223, '2021_06_28_130141_add_col_admin_vendors_table', 102),
(224, '2021_06_29_095257_add_col_in_items_table', 102),
(225, '2021_06_29_105055_make_null_value_various_charge', 103),
(226, '2021_06_30_050456_default_val_gst_pst_hst', 104),
(227, '2021_06_30_050841_default_val_charges', 104),
(228, '2021_07_13_074241_add_col_bookingdetail_table', 105),
(229, '2021_07_13_081030_add_col_bookingdetail_table_new', 106),
(230, '2021_07_13_081229_add_col_orderdetail', 106),
(231, '2021_07_14_121450_add_cols_in_orderdetail', 107),
(232, '2021_07_19_062750_delfault_0_tiptodeliveryboy', 108),
(233, '2021_07_22_054710_float_rating_review_table', 109),
(234, '2021_07_23_054114_deleted_at_column_admin_vendors', 110),
(235, '2021_08_03_075831_add_cols_in_admin_fcm_token', 111);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 1),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 2),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 9),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 10),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 11),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 12),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 13),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 14),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 15),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 16),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 17),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 18),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 19),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 20),
(1, 'Brackets\\AdminAuth\\Models\\AdminUser', 27),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 28),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 29),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 36),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 38),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 63),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 64),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 65),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 66),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 67),
(1, 'App\\Models\\AdminUsers', 68),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 68),
(1, 'App\\Models\\AdminUsers', 69),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 69),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 70),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 71),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 72),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 73),
(4, 'App\\Models\\AdminUsers', 76),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 96),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 97),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 98),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 99),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 100),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 101),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 102),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 103),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 104),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 105),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 106),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 107),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 108),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 109),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 110),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 111),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 112),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 113),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 114),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 115),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 117),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 118),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 119),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 120),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 121),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 122),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 123),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 124),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 125),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 126),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 127),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 128),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 129),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 130),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 131),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 132),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 133),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 137),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 139),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 140),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 141),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 143),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 144),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 145),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 147),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 148),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 154),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 155),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 156),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 157),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 158),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 159),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 160),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 161),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 162),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 163),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 164),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 165),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 166),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 167),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 168),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 169),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 170),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 171),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 172),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 173),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 174),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 175),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 176),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 177),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 178),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 179),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 180),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 181),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 182),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 183),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 184),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 185),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 186),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 187),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 188),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 189),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 190),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 191),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 192),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 193),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 194),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 195),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 196),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 197),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 198),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 199),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 200),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 201),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 202),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 203),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 204),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 205),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 207),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 208),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 209),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 210),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 211),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 212),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 213),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 214),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 215),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 216),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 217),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 218),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 219),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 220),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 221),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 222),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 223),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 224),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 225),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 226),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 227),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 228),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 229),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 230),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 231),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 232),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 233),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 236),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 237),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 238),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 239),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 240),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 241),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 242),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 243),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 244),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 245),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 246),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 247),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 248),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 249),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 250),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 252),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 253),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 254),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 255),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 256),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 257),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 258),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 259),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 260),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 261),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 262),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 263),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 264),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 265),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 266),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 267),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 268),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 269),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 270),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 271),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 272),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 273),
(4, 'Brackets\\AdminAuth\\Models\\AdminUser', 274);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `send_by` int(11) NOT NULL,
  `send_to` int(11) NOT NULL,
  `notification_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `send_by`, `send_to`, `notification_type`, `title`, `description`, `is_read`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-22 06:38:13', '2021-07-26 08:08:56'),
(2, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0S0S20210722121108 placed successfully.', 1, '2021-07-22 06:41:10', '2021-07-27 02:04:50'),
(3, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #0S0S20210722121108 cancelled!', 1, '2021-07-22 06:41:24', '2021-07-27 02:04:50'),
(4, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #0S0S20210722121108.', 1, '2021-07-22 06:41:26', '2021-07-27 02:04:50'),
(5, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D0SA20210722121450 placed successfully.', 1, '2021-07-22 06:44:52', '2021-07-27 02:04:50'),
(6, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #D0SA20210722121450 cancelled!', 1, '2021-07-22 06:45:26', '2021-07-27 02:04:50'),
(7, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #D0SA20210722121450.', 1, '2021-07-22 06:45:28', '2021-07-27 02:04:50'),
(8, 5, 5, '1', 'Order Done Successfully!', 'Dear Shivam Singla, Your order #DASD20210723115607 placed successfully.', 1, '2021-07-23 06:26:09', '2021-07-23 06:26:38'),
(9, 0, 5, '14', 'Your Order #DASD20210723115607 got cancelled by system!', 'Your Order #DASD20210723115607 got cancelled!', 1, '2021-07-23 13:05:04', '2021-07-27 05:49:03'),
(10, 1, 8, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 0, '2021-07-26 01:31:55', '2021-07-26 01:31:55'),
(11, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-26 01:33:16', '2021-07-28 03:00:46'),
(12, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-26 01:34:33', '2021-07-28 03:00:46'),
(13, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D00D20210726125931 placed successfully.', 1, '2021-07-26 07:29:33', '2021-07-26 08:09:08'),
(14, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0DD020210726130214 placed successfully.', 1, '2021-07-26 07:32:17', '2021-07-26 08:09:08'),
(15, 1, 12, '1', 'Order Rejected!', 'Your Order #D00D20210726125931 has been rejected. Reason of Rejection : desawq nbbgh mnhbhj nbhgvgf bhgvfc bhgv.', 1, '2021-07-26 07:33:31', '2021-07-26 08:09:08'),
(16, 1, 12, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #D00D20210726125931.', 1, '2021-07-26 07:33:31', '2021-07-26 08:09:08'),
(17, 1, 12, '2', 'Order Accepted!', 'Your Order #0DD020210726130214 has been accepted.', 1, '2021-07-26 08:09:29', '2021-07-26 08:09:30'),
(18, 1, 12, '4', 'Order in progress!', 'Your Order #0DD020210726130214 is in progress.', 1, '2021-07-26 08:09:33', '2021-07-26 08:09:34'),
(19, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0DD020210726130214.', 1, '2021-07-26 08:10:05', '2021-07-26 08:41:50'),
(20, 2, 12, '5', 'Delivery Boy Assigned.', 'Dear Customer, Test Driver Assigned for your order #0DD020210726130214.', 1, '2021-07-26 08:10:34', '2021-07-26 08:10:47'),
(21, 2, 12, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Test Driver on his way to pick the order #0DD020210726130214.', 1, '2021-07-26 08:10:56', '2021-07-26 08:11:17'),
(22, 2, 12, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Test Driver reached to  Test vendor y to pick order #0DD020210726130214.', 1, '2021-07-26 08:11:34', '2021-07-26 08:11:36'),
(23, 2, 12, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Test Driver Picked the order #0DD020210726130214.', 1, '2021-07-26 08:11:50', '2021-07-26 08:12:01'),
(24, 2, 12, '9', 'Delivery Boy is almost their!', 'Dear Customer, Test Driver on his way to deliver the order #0DD020210726130214.', 1, '2021-07-26 08:12:44', '2021-07-26 08:13:03'),
(25, 2, 12, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0DD020210726130214 delivered successfully!', 1, '2021-07-26 08:13:22', '2021-07-26 08:42:03'),
(26, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:25:03', '2021-07-27 04:32:53'),
(27, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:27:24', '2021-07-27 04:32:53'),
(28, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:28:36', '2021-07-27 04:32:53'),
(29, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:53:38', '2021-07-27 04:32:53'),
(30, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:54:04', '2021-07-27 04:32:53'),
(31, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:54:50', '2021-07-27 04:32:53'),
(32, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:58:03', '2021-07-27 04:32:53'),
(33, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:58:22', '2021-07-27 04:32:53'),
(34, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 02:59:53', '2021-07-27 04:32:53'),
(35, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 03:05:14', '2021-07-27 04:32:53'),
(36, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 03:05:49', '2021-07-27 04:32:53'),
(37, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 03:06:13', '2021-07-27 04:32:53'),
(38, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:15:21', '2021-07-27 04:32:53'),
(39, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:22:05', '2021-07-27 04:32:53'),
(40, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:22:46', '2021-07-27 04:32:53'),
(41, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:25:16', '2021-07-27 04:32:53'),
(42, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:27:42', '2021-07-27 04:32:53'),
(43, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 04:33:50', '2021-07-27 04:33:50'),
(44, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 04:35:34', '2021-07-27 04:35:34'),
(45, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 04:36:02', '2021-07-27 04:36:02'),
(46, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 04:37:22', '2021-07-27 04:37:22'),
(47, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 04:37:41', '2021-07-27 04:37:41'),
(48, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:38:24', '2021-07-27 06:34:55'),
(49, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:39:32', '2021-07-27 06:34:55'),
(50, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:41:58', '2021-07-27 06:34:55'),
(51, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:43:38', '2021-07-27 06:34:55'),
(52, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:44:22', '2021-07-27 06:34:55'),
(53, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 04:45:12', '2021-07-27 06:34:55'),
(54, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 05:16:08', '2021-07-27 06:34:55'),
(55, 1, 2, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-27 05:18:14', '2021-07-28 00:18:57'),
(56, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0DA020210727105129 placed successfully.', 1, '2021-07-27 05:21:31', '2021-07-27 06:34:55'),
(57, 1, 12, '1', 'Order Rejected!', 'Your Order #0DA020210727105129 has been rejected. Reason of Rejection : desawq nbbgh mnhbhj nbhgvgf bhgvfc bhgv.', 1, '2021-07-27 05:37:40', '2021-07-27 06:34:55'),
(58, 1, 12, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #0DA020210727105129.', 1, '2021-07-27 05:37:41', '2021-07-27 06:34:55'),
(59, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DSSD20210727112717 placed successfully.', 1, '2021-07-27 05:57:19', '2021-07-27 06:34:55'),
(60, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SASS20210727113041 placed successfully.', 1, '2021-07-27 06:00:43', '2021-07-27 06:34:55'),
(61, 0, 12, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SASS20210727113041.', 1, '2021-07-27 06:01:06', '2021-07-27 06:34:55'),
(62, 0, 12, '14', 'Your Order #SASS20210727113041 got cancelled by system!', 'Your Order #SASS20210727113041 got cancelled!', 1, '2021-07-27 06:01:06', '2021-07-27 06:34:55'),
(63, 12, 12, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SSS020210727113105 placed successfully.', 1, '2021-07-27 06:01:07', '2021-07-27 06:34:55'),
(64, 0, 12, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SSS020210727113105.', 1, '2021-07-27 06:02:05', '2021-07-27 06:34:55'),
(65, 0, 12, '14', 'Your Order #SSS020210727113105 got cancelled by system!', 'Your Order #SSS020210727113105 got cancelled!', 1, '2021-07-27 06:02:06', '2021-07-27 06:34:55'),
(66, 1, 12, '2', 'Order Accepted!', 'Your Order #DSSD20210727112717 has been accepted.', 1, '2021-07-27 06:23:09', '2021-07-27 06:34:55'),
(67, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DSSD20210727112717.', 1, '2021-07-27 06:24:03', '2021-07-28 00:18:57'),
(68, 12, 12, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #DSSD20210727112717 cancelled!', 1, '2021-07-27 06:34:49', '2021-07-27 06:34:55'),
(69, 12, 2, '11', 'The order cancelled by customer!', 'Dear Test Driver, Your order #DSSD20210727112717 cancelled by customer!', 1, '2021-07-27 06:34:52', '2021-07-28 00:18:57'),
(70, 12, 12, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #DSSD20210727112717.', 1, '2021-07-27 06:34:54', '2021-07-27 06:34:55'),
(71, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:04:40', '2021-07-27 07:04:40'),
(72, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:07:21', '2021-07-27 07:34:52'),
(73, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:07:21', '2021-07-27 07:37:33'),
(74, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:07:21', '2021-07-27 23:36:34'),
(75, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:07:21', '2021-07-27 07:07:21'),
(76, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:13:38', '2021-07-27 07:34:52'),
(77, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:13:38', '2021-07-27 07:37:33'),
(78, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:13:38', '2021-07-27 23:36:34'),
(79, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:13:38', '2021-07-27 07:13:38'),
(80, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:16:31', '2021-07-27 07:34:52'),
(81, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:16:31', '2021-07-27 07:37:33'),
(82, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:16:31', '2021-07-27 23:36:34'),
(83, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:16:31', '2021-07-27 07:16:31'),
(84, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #ASDS20210727125356 placed successfully.', 1, '2021-07-27 07:23:58', '2021-07-27 07:24:11'),
(85, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #ASDS20210727125356.', 1, '2021-07-27 07:24:05', '2021-07-27 07:24:11'),
(86, 0, 19, '14', 'Your Order #ASDS20210727125356 got cancelled by system!', 'Your Order #ASDS20210727125356 got cancelled!', 1, '2021-07-27 07:24:05', '2021-07-27 07:24:11'),
(87, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:38:01', '2021-07-27 07:38:09'),
(88, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:38:01', '2021-07-27 23:36:34'),
(89, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:38:01', '2021-07-27 07:38:01'),
(90, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:38:38', '2021-07-27 23:36:34'),
(91, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:38:38', '2021-07-27 07:38:38'),
(92, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:39:09', '2021-07-27 07:39:19'),
(93, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:39:09', '2021-07-27 23:36:34'),
(94, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:39:09', '2021-07-27 07:39:09'),
(95, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:39:46', '2021-07-27 07:39:50'),
(96, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:39:46', '2021-07-27 07:39:58'),
(97, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:39:46', '2021-07-27 23:36:34'),
(98, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:39:46', '2021-07-27 07:39:46'),
(99, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:40:54', '2021-07-27 07:40:55'),
(100, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:40:54', '2021-07-27 07:41:10'),
(101, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:40:54', '2021-07-27 23:36:34'),
(102, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:40:54', '2021-07-27 07:40:54'),
(103, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:42:53', '2021-07-27 07:46:07'),
(104, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:42:53', '2021-07-27 23:36:34'),
(105, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:42:53', '2021-07-27 07:42:53'),
(106, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #SDA020210727131332 placed successfully.', 1, '2021-07-27 07:43:34', '2021-07-27 07:43:39'),
(107, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:43:52', '2021-07-27 07:46:07'),
(108, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:43:52', '2021-07-27 23:36:34'),
(109, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:43:52', '2021-07-27 07:43:52'),
(110, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:43:52', '2021-07-27 07:43:53'),
(111, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SDA020210727131332.', 1, '2021-07-27 07:44:06', '2021-07-27 07:44:12'),
(112, 0, 13, '14', 'Your Order #SDA020210727131332 got cancelled by system!', 'Your Order #SDA020210727131332 got cancelled!', 1, '2021-07-27 07:44:11', '2021-07-27 07:44:12'),
(113, 1, 14, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:44:23', '2021-07-27 07:46:07'),
(114, 1, 1, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:44:23', '2021-07-27 23:36:34'),
(115, 1, 12, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-07-27 07:44:23', '2021-07-27 07:44:23'),
(116, 1, 13, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 1, '2021-07-27 07:44:23', '2021-07-27 07:44:24'),
(117, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0S0A20210727131532 placed successfully.', 1, '2021-07-27 07:45:33', '2021-07-27 07:45:39'),
(118, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0S0A20210727131532.', 1, '2021-07-27 07:46:05', '2021-07-28 23:53:55'),
(119, 0, 13, '14', 'Your Order #0S0A20210727131532 got cancelled by system!', 'Your Order #0S0A20210727131532 got cancelled!', 1, '2021-07-27 07:46:06', '2021-07-28 23:53:55'),
(120, 23, 23, '1', 'Order Done Successfully!', 'Dear Testc, Your order #ADAD20210728054446 placed successfully.', 0, '2021-07-28 00:14:48', '2021-07-28 00:14:48'),
(121, 0, 23, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #ADAD20210728054446.', 0, '2021-07-28 00:15:05', '2021-07-28 00:15:05'),
(122, 0, 23, '14', 'Your Order #ADAD20210728054446 got cancelled by system!', 'Your Order #ADAD20210728054446 got cancelled!', 0, '2021-07-28 00:15:06', '2021-07-28 00:15:06'),
(123, 23, 23, '1', 'Order Done Successfully!', 'Dear Testc, Your order #ADDA20210728054556 placed successfully.', 0, '2021-07-28 00:15:58', '2021-07-28 00:15:58'),
(124, 0, 23, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #ADDA20210728054556.', 0, '2021-07-28 00:16:05', '2021-07-28 00:16:05'),
(125, 0, 23, '14', 'Your Order #ADDA20210728054556 got cancelled by system!', 'Your Order #ADDA20210728054556 got cancelled!', 0, '2021-07-28 00:16:05', '2021-07-28 00:16:05'),
(126, 23, 23, '1', 'Order Done Successfully!', 'Dear Testc, Your order #D0SA20210728054628 placed successfully.', 0, '2021-07-28 00:16:30', '2021-07-28 00:16:30'),
(127, 23, 23, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #D0SA20210728054628 cancelled!', 0, '2021-07-28 00:16:50', '2021-07-28 00:16:50'),
(128, 23, 23, '16', 'Order Refund Initiated!', 'Dear Testc, Your order refund is initiated for the order #D0SA20210728054628.', 0, '2021-07-28 00:16:52', '2021-07-28 00:16:52'),
(129, 23, 23, '1', 'Order Done Successfully!', 'Dear Testc, Your order #0AAA20210728054731 placed successfully.', 0, '2021-07-28 00:17:33', '2021-07-28 00:17:33'),
(130, 1, 23, '2', 'Order Accepted!', 'Your Order #0AAA20210728054731 has been accepted.', 0, '2021-07-28 00:18:22', '2021-07-28 00:18:22'),
(131, 1, 23, '4', 'Order in progress!', 'Your Order #0AAA20210728054731 is in progress.', 0, '2021-07-28 00:18:26', '2021-07-28 00:18:26'),
(132, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #A0AS20210728063514 placed successfully.', 1, '2021-07-28 01:05:16', '2021-07-28 02:41:04'),
(133, 1, 24, '1', 'Order Rejected!', 'Your Order #A0AS20210728063514 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 01:14:29', '2021-07-28 02:41:04'),
(134, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #A0AS20210728063514.', 1, '2021-07-28 01:14:29', '2021-07-28 02:41:04'),
(135, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #DAA020210728064540 placed successfully.', 1, '2021-07-28 01:15:42', '2021-07-28 02:41:04'),
(136, 1, 24, '1', 'Order Rejected!', 'Your Order #DAA020210728064540 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 01:16:05', '2021-07-28 02:41:04'),
(137, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #DAA020210728064540.', 1, '2021-07-28 01:16:05', '2021-07-28 02:41:04'),
(138, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #ADAD20210728064622 placed successfully.', 1, '2021-07-28 01:16:23', '2021-07-28 02:41:04'),
(139, 1, 24, '1', 'Order Rejected!', 'Your Order #ADAD20210728064622 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 01:16:51', '2021-07-28 02:41:04'),
(140, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #ADAD20210728064622.', 1, '2021-07-28 01:16:52', '2021-07-28 02:41:04'),
(141, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #SDAS20210728064740 placed successfully.', 1, '2021-07-28 01:17:42', '2021-07-28 02:41:04'),
(142, 1, 24, '1', 'Order Rejected!', 'Your Order #SDAS20210728064740 has been rejected. Reason of Rejection : no vendor available.', 1, '2021-07-28 01:18:05', '2021-07-28 02:41:04'),
(143, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #SDAS20210728064740.', 1, '2021-07-28 01:18:06', '2021-07-28 02:41:04'),
(144, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #SDAD20210728074359 placed successfully.', 1, '2021-07-28 02:14:01', '2021-07-28 02:41:04'),
(145, 1, 24, '1', 'Order Rejected!', 'Your Order #SDAD20210728074359 has been rejected. Reason of Rejection : no vendor available.', 1, '2021-07-28 02:15:45', '2021-07-28 02:41:04'),
(146, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #SDAD20210728074359.', 1, '2021-07-28 02:15:46', '2021-07-28 02:41:04'),
(147, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #SA0S20210728074711 placed successfully.', 1, '2021-07-28 02:17:13', '2021-07-28 02:41:04'),
(148, 1, 24, '1', 'Order Rejected!', 'Your Order #SA0S20210728074711 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 02:17:34', '2021-07-28 02:41:04'),
(149, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #SA0S20210728074711.', 1, '2021-07-28 02:17:34', '2021-07-28 02:41:04'),
(150, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #DDD020210728080544 placed successfully.', 1, '2021-07-28 02:35:51', '2021-07-28 02:41:04'),
(151, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #AAAS20210728081629 placed successfully.', 1, '2021-07-28 02:46:31', '2021-07-28 07:55:29'),
(152, 1, 24, '1', 'Order Rejected!', 'Your Order #AAAS20210728081629 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 02:47:10', '2021-07-28 07:55:29'),
(153, 1, 24, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #AAAS20210728081629.', 1, '2021-07-28 02:47:11', '2021-07-28 07:55:29'),
(154, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-28 03:02:40', '2021-07-29 00:21:52'),
(155, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #AS0020210728101928 placed successfully.', 1, '2021-07-28 04:49:30', '2021-07-29 04:30:03'),
(156, 1, 19, '1', 'Order Rejected!', 'Your Order #AS0020210728101928 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 04:50:08', '2021-07-29 04:30:03'),
(157, 0, 19, '14', 'Your Order #AS0020210728101928 got cancelled by system!', 'Your Order #AS0020210728101928 got cancelled!', 1, '2021-07-28 04:50:08', '2021-07-29 04:30:03'),
(158, 1, 19, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #AS0020210728101928.', 1, '2021-07-28 04:50:19', '2021-07-29 04:30:03'),
(159, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SDA020210728103547 placed successfully.', 1, '2021-07-28 05:05:51', '2021-07-29 04:30:03'),
(160, 1, 19, '1', 'Order Rejected!', 'Your Order #SDA020210728103547 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 05:06:45', '2021-07-29 04:30:03'),
(161, 1, 19, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #SDA020210728103547.', 1, '2021-07-28 05:06:46', '2021-07-29 04:30:03'),
(162, 1, 19, '1', 'Order Rejected!', 'Your Order #SDA020210728103547 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 05:06:46', '2021-07-29 04:30:03'),
(163, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SDDD20210728103713 placed successfully.', 1, '2021-07-28 05:07:15', '2021-07-29 04:30:03'),
(164, 1, 19, '1', 'Order Rejected!', 'Your Order #SDDD20210728103713 has been rejected. Reason of Rejection : no a valid shop.', 1, '2021-07-28 05:07:36', '2021-07-29 04:30:03'),
(165, 1, 19, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #SDDD20210728103713.', 1, '2021-07-28 05:07:37', '2021-07-29 04:30:03'),
(166, 17, 17, '1', 'Order Done Successfully!', 'Dear Shalini Test, Your order #00A020210728134402 placed successfully.', 1, '2021-07-28 08:14:04', '2021-07-29 23:22:00'),
(167, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DSDD20210728135218 placed successfully.', 1, '2021-07-28 08:22:20', '2021-07-28 23:53:55'),
(168, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0ADA20210728135408 placed successfully.', 1, '2021-07-28 08:24:10', '2021-07-28 23:53:55'),
(169, 0, 24, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DDD020210728080544.', 1, '2021-07-28 13:04:05', '2021-07-28 23:57:17'),
(170, 0, 24, '14', 'Your Order #DDD020210728080544 got cancelled by system!', 'Your Order #DDD020210728080544 got cancelled!', 1, '2021-07-28 13:04:05', '2021-07-28 23:57:17'),
(171, 0, 17, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00A020210728134402.', 1, '2021-07-28 13:04:06', '2021-07-29 23:22:00'),
(172, 0, 17, '14', 'Your Order #00A020210728134402 got cancelled by system!', 'Your Order #00A020210728134402 got cancelled!', 1, '2021-07-28 13:04:06', '2021-07-29 23:22:00'),
(173, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DSDD20210728135218.', 1, '2021-07-28 13:04:08', '2021-07-28 23:53:55'),
(174, 0, 13, '14', 'Your Order #DSDD20210728135218 got cancelled by system!', 'Your Order #DSDD20210728135218 got cancelled!', 1, '2021-07-28 13:04:08', '2021-07-28 23:53:55'),
(175, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0ADA20210728135408.', 1, '2021-07-28 13:04:10', '2021-07-28 23:53:55'),
(176, 0, 13, '14', 'Your Order #0ADA20210728135408 got cancelled by system!', 'Your Order #0ADA20210728135408 got cancelled!', 1, '2021-07-28 13:04:11', '2021-07-28 23:53:55'),
(177, 24, 24, '1', 'Order Done Successfully!', 'Dear Nagarjun, Your order #SA0A20210729045505 placed successfully.', 1, '2021-07-28 23:25:07', '2021-07-28 23:57:17'),
(178, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0DD020210729052337 placed successfully.', 1, '2021-07-28 23:53:38', '2021-07-28 23:53:55'),
(179, 1, 13, '2', 'Order Accepted!', 'Your Order #0DD020210729052337 has been accepted.', 0, '2021-07-29 00:01:52', '2021-07-29 00:01:52'),
(180, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0DD020210729052337.', 1, '2021-07-29 00:02:04', '2021-07-29 02:43:28'),
(181, 2, 13, '5', 'Delivery Boy Assigned.', 'Dear Customer, Test Driver Assigned for your order #0DD020210729052337.', 0, '2021-07-29 00:02:19', '2021-07-29 00:02:19'),
(182, 2, 13, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Test Driver on his way to pick the order #0DD020210729052337.', 0, '2021-07-29 00:02:27', '2021-07-29 00:02:27'),
(183, 2, 13, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Test Driver reached to  Test vendor y to pick order #0DD020210729052337.', 0, '2021-07-29 00:02:31', '2021-07-29 00:02:31'),
(184, 2, 13, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Test Driver Picked the order #0DD020210729052337.', 0, '2021-07-29 00:02:35', '2021-07-29 00:02:35'),
(185, 2, 13, '9', 'Delivery Boy is almost their!', 'Dear Customer, Test Driver on his way to deliver the order #0DD020210729052337.', 0, '2021-07-29 00:02:42', '2021-07-29 00:02:42'),
(186, 2, 13, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0DD020210729052337 delivered successfully!', 0, '2021-07-29 00:02:56', '2021-07-29 00:02:56'),
(187, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0SAD20210729055057 placed successfully.', 0, '2021-07-29 00:20:58', '2021-07-29 00:20:58'),
(188, 1, 13, '2', 'Order Accepted!', 'Your Order #0SAD20210729055057 has been accepted.', 0, '2021-07-29 00:22:27', '2021-07-29 00:22:27'),
(189, 1, 13, '4', 'Order in progress!', 'Your Order #0SAD20210729055057 is in progress.', 0, '2021-07-29 00:22:47', '2021-07-29 00:22:47'),
(190, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SAD20210729055057.', 1, '2021-07-29 00:23:02', '2021-07-29 00:25:47'),
(191, 7, 13, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #0SAD20210729055057.', 0, '2021-07-29 00:23:08', '2021-07-29 00:23:08'),
(192, 7, 13, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #0SAD20210729055057.', 0, '2021-07-29 00:23:17', '2021-07-29 00:23:17'),
(193, 7, 13, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #0SAD20210729055057.', 0, '2021-07-29 00:23:19', '2021-07-29 00:23:19'),
(194, 7, 13, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #0SAD20210729055057.', 0, '2021-07-29 00:23:21', '2021-07-29 00:23:21'),
(195, 7, 13, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #0SAD20210729055057.', 0, '2021-07-29 00:23:25', '2021-07-29 00:23:25'),
(196, 7, 13, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0SAD20210729055057 delivered successfully!', 0, '2021-07-29 00:23:38', '2021-07-29 00:23:38'),
(197, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-29 00:41:50', '2021-07-29 00:43:04'),
(198, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 0, '2021-07-29 00:43:38', '2021-07-29 00:43:38'),
(199, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #D00D20210729062620 placed successfully.', 0, '2021-07-29 00:56:22', '2021-07-29 00:56:22'),
(200, 1, 13, '2', 'Order Accepted!', 'Your Order #D00D20210729062620 has been accepted.', 0, '2021-07-29 00:56:52', '2021-07-29 00:56:52'),
(201, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D00D20210729062620.', 1, '2021-07-29 00:57:03', '2021-07-29 02:43:28'),
(202, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #AD0D20210729062646 placed successfully.', 0, '2021-07-29 00:57:04', '2021-07-29 00:57:04'),
(203, 1, 13, '4', 'Order in progress!', 'Your Order #D00D20210729062620 is in progress.', 0, '2021-07-29 00:57:12', '2021-07-29 00:57:12'),
(204, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D00D20210729062620.', 1, '2021-07-29 00:57:19', '2021-07-29 02:17:38'),
(205, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D00D20210729062620.', 0, '2021-07-29 00:58:03', '2021-07-29 00:58:03'),
(206, 6, 13, '5', 'Delivery Boy Assigned.', 'Dear Customer, Fggg Assigned for your order #D00D20210729062620.', 0, '2021-07-29 00:58:21', '2021-07-29 00:58:21'),
(207, 6, 13, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Fggg on his way to pick the order #D00D20210729062620.', 0, '2021-07-29 00:59:03', '2021-07-29 00:59:03'),
(208, 6, 13, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Fggg reached to  Test vendor y to pick order #D00D20210729062620.', 0, '2021-07-29 00:59:06', '2021-07-29 00:59:06'),
(209, 6, 13, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Fggg Picked the order #D00D20210729062620.', 0, '2021-07-29 00:59:08', '2021-07-29 00:59:08'),
(210, 6, 13, '9', 'Delivery Boy is almost their!', 'Dear Customer, Fggg on his way to deliver the order #D00D20210729062620.', 0, '2021-07-29 00:59:29', '2021-07-29 00:59:29'),
(211, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #00S020210729062941 placed successfully.', 0, '2021-07-29 00:59:49', '2021-07-29 00:59:49'),
(212, 6, 13, '9', 'Delivery Boy is almost their!', 'Dear Customer, Fggg on his way to deliver the order #D00D20210729062620.', 0, '2021-07-29 01:00:07', '2021-07-29 01:00:07'),
(213, 6, 13, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #D00D20210729062620 delivered successfully!', 0, '2021-07-29 01:00:38', '2021-07-29 01:00:38'),
(214, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0A0D20210729063253 placed successfully.', 0, '2021-07-29 01:02:55', '2021-07-29 01:02:55'),
(215, 1, 13, '2', 'Order Accepted!', 'Your Order #AD0D20210729062646 has been accepted.', 0, '2021-07-29 01:04:47', '2021-07-29 01:04:47'),
(216, 1, 13, '4', 'Order in progress!', 'Your Order #AD0D20210729062646 is in progress.', 0, '2021-07-29 01:05:01', '2021-07-29 01:05:01'),
(217, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AD0D20210729062646.', 1, '2021-07-29 01:05:03', '2021-07-29 02:43:28'),
(218, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AD0D20210729062646.', 0, '2021-07-29 01:05:03', '2021-07-29 01:05:03'),
(219, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AD0D20210729062646.', 1, '2021-07-29 01:05:09', '2021-07-29 02:17:38'),
(220, 7, 13, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #AD0D20210729062646.', 0, '2021-07-29 01:05:25', '2021-07-29 01:05:25'),
(221, 7, 13, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #AD0D20210729062646.', 0, '2021-07-29 01:05:33', '2021-07-29 01:05:33'),
(222, 7, 13, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #AD0D20210729062646.', 0, '2021-07-29 01:05:37', '2021-07-29 01:05:37'),
(223, 7, 13, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #AD0D20210729062646.', 0, '2021-07-29 01:05:41', '2021-07-29 01:05:41'),
(224, 7, 13, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #AD0D20210729062646.', 0, '2021-07-29 01:05:57', '2021-07-29 01:05:57'),
(225, 7, 13, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #AD0D20210729062646 delivered successfully!', 0, '2021-07-29 01:06:13', '2021-07-29 01:06:13'),
(226, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DS0020210729063749 placed successfully.', 0, '2021-07-29 01:07:57', '2021-07-29 01:07:57'),
(227, 1, 13, '2', 'Order Accepted!', 'Your Order #00S020210729062941 has been accepted.', 0, '2021-07-29 01:08:01', '2021-07-29 01:08:01'),
(228, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:08:04', '2021-07-29 02:43:28'),
(229, 1, 13, '4', 'Order in progress!', 'Your Order #00S020210729062941 is in progress.', 0, '2021-07-29 01:08:08', '2021-07-29 01:08:08'),
(230, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 0, '2021-07-29 01:08:14', '2021-07-29 01:08:14'),
(231, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:08:20', '2021-07-29 02:17:38'),
(232, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:09:03', '2021-07-29 02:17:38'),
(233, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:10:03', '2021-07-29 02:17:38'),
(234, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:11:02', '2021-07-29 02:17:38'),
(235, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:12:03', '2021-07-29 02:17:38'),
(236, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:13:03', '2021-07-29 02:17:38'),
(237, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:14:03', '2021-07-29 02:17:38'),
(238, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:15:03', '2021-07-29 02:17:38'),
(239, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:16:03', '2021-07-29 02:17:38'),
(240, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:17:02', '2021-07-29 02:17:38'),
(241, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:18:03', '2021-07-29 02:17:38'),
(242, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:19:03', '2021-07-29 02:17:38'),
(243, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:20:04', '2021-07-29 02:17:38'),
(244, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:21:03', '2021-07-29 02:17:38'),
(245, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:22:03', '2021-07-29 02:17:38'),
(246, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:23:02', '2021-07-29 02:17:38'),
(247, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:24:03', '2021-07-29 02:17:38'),
(248, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 1, '2021-07-29 01:25:03', '2021-07-29 02:17:38'),
(249, 1, 3, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 0, '2021-07-29 01:30:59', '2021-07-29 01:30:59'),
(250, 1, 40, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 0, '2021-07-29 01:31:29', '2021-07-29 01:31:29'),
(251, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00S020210729062941.', 0, '2021-07-29 01:34:03', '2021-07-29 01:34:03'),
(252, 1, 13, '2', 'Order Accepted!', 'Your Order #0A0D20210729063253 has been accepted.', 0, '2021-07-29 01:34:05', '2021-07-29 01:34:05'),
(253, 1, 13, '4', 'Order in progress!', 'Your Order #0A0D20210729063253 is in progress.', 0, '2021-07-29 01:34:09', '2021-07-29 01:34:09'),
(254, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0A0D20210729063253.', 1, '2021-07-29 01:35:03', '2021-07-29 02:43:28'),
(255, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0A0D20210729063253.', 0, '2021-07-29 01:35:03', '2021-07-29 01:35:03'),
(256, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0A0D20210729063253.', 0, '2021-07-29 01:36:03', '2021-07-29 01:36:03'),
(257, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0AAD20210729074341 placed successfully.', 0, '2021-07-29 02:13:43', '2021-07-29 02:13:43'),
(258, 1, 13, '2', 'Order Accepted!', 'Your Order #DS0020210729063749 has been accepted.', 0, '2021-07-29 02:14:23', '2021-07-29 02:14:23'),
(259, 1, 13, '4', 'Order in progress!', 'Your Order #DS0020210729063749 is in progress.', 0, '2021-07-29 02:14:29', '2021-07-29 02:14:29'),
(260, 1, 13, '2', 'Order Accepted!', 'Your Order #0AAD20210729074341 has been accepted.', 0, '2021-07-29 02:15:45', '2021-07-29 02:15:45'),
(261, 1, 13, '4', 'Order in progress!', 'Your Order #0AAD20210729074341 is in progress.', 0, '2021-07-29 02:15:48', '2021-07-29 02:15:48'),
(262, 1, 40, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 0, '2021-07-29 02:18:53', '2021-07-29 02:18:53'),
(263, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #S0DA20210729081141 placed successfully.', 0, '2021-07-29 02:41:43', '2021-07-29 02:41:43'),
(264, 1, 13, '2', 'Order Accepted!', 'Your Order #S0DA20210729081141 has been accepted.', 0, '2021-07-29 02:42:02', '2021-07-29 02:42:02'),
(265, 1, 13, '4', 'Order in progress!', 'Your Order #S0DA20210729081141 is in progress.', 0, '2021-07-29 02:42:11', '2021-07-29 02:42:11'),
(266, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DADS20210729094034 placed successfully.', 0, '2021-07-29 04:10:35', '2021-07-29 04:10:35'),
(267, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #D00S20210729095033 placed successfully.', 1, '2021-07-29 04:20:35', '2021-07-29 04:30:03'),
(268, 19, 19, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #D00S20210729095033 cancelled!', 1, '2021-07-29 04:20:58', '2021-07-29 04:30:03'),
(269, 19, 19, '16', 'Order Refund Initiated!', 'Dear Shalini, Your order refund is initiated for the order #D00S20210729095033.', 1, '2021-07-29 04:21:00', '2021-07-29 04:30:03'),
(270, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #00SD20210729095401 placed successfully.', 0, '2021-07-29 04:24:03', '2021-07-29 04:24:03'),
(271, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #00SS20210729100736 placed successfully.', 1, '2021-07-29 04:37:38', '2021-07-29 23:15:21'),
(272, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #00SS20210729100736 cancelled!', 1, '2021-07-29 04:37:38', '2021-07-29 23:15:21'),
(273, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #00SS20210729100736.', 1, '2021-07-29 04:37:42', '2021-07-29 23:15:21'),
(274, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #0D0020210729102618 placed successfully.', 1, '2021-07-29 04:56:20', '2021-07-29 05:54:37'),
(275, 1, 19, '2', 'Order Accepted!', 'Your Order #0D0020210729102618 has been accepted.', 1, '2021-07-29 04:56:43', '2021-07-29 05:54:37'),
(276, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #DDSA20210729102656 placed successfully.', 1, '2021-07-29 04:56:58', '2021-07-29 05:54:37'),
(277, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #AD0020210729104919 placed successfully.', 1, '2021-07-29 05:19:21', '2021-07-29 05:54:37'),
(278, 1, 19, '2', 'Order Accepted!', 'Your Order #AD0020210729104919 has been accepted.', 1, '2021-07-29 05:20:25', '2021-07-29 05:54:37'),
(279, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #D0AD20210729110741 placed successfully.', 1, '2021-07-29 05:37:43', '2021-07-29 05:54:37'),
(280, 1, 19, '2', 'Order Accepted!', 'Your Order #D0AD20210729110741 has been accepted.', 1, '2021-07-29 05:38:07', '2021-07-29 05:54:37'),
(281, 1, 19, '4', 'Order in progress!', 'Your Order #D0AD20210729110741 is in progress.', 1, '2021-07-29 05:40:11', '2021-07-29 05:54:37'),
(282, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:41:03', '2021-07-29 05:41:03'),
(283, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:41:03', '2021-07-29 05:41:03'),
(284, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:41:04', '2021-07-29 05:41:04'),
(285, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:41:04', '2021-07-29 05:41:04'),
(286, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:41:04', '2021-07-29 05:41:04'),
(287, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:41:05', '2021-07-29 05:41:05'),
(288, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0D0020210729102618.', 0, '2021-07-29 05:41:05', '2021-07-29 05:41:05'),
(289, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AD0020210729104919.', 0, '2021-07-29 05:41:06', '2021-07-29 05:41:06'),
(290, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0AD20210729110741.', 0, '2021-07-29 05:41:06', '2021-07-29 05:41:06'),
(291, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #AS0S20210729111134 placed successfully.', 1, '2021-07-29 05:41:36', '2021-07-29 05:54:37'),
(292, 1, 19, '2', 'Order Accepted!', 'Your Order #AS0S20210729111134 has been accepted.', 1, '2021-07-29 05:41:52', '2021-07-29 05:54:37'),
(293, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:42:02', '2021-07-29 05:42:02'),
(294, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:42:03', '2021-07-29 05:42:03'),
(295, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:42:03', '2021-07-29 05:42:03'),
(296, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AS0S20210729111134.', 0, '2021-07-29 05:42:04', '2021-07-29 05:42:04'),
(297, 40, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, ktest Assigned for your order #0D0020210729102618.', 1, '2021-07-29 05:42:25', '2021-07-29 05:54:37'),
(298, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:43:03', '2021-07-29 05:43:03'),
(299, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:43:03', '2021-07-29 05:43:03'),
(300, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:43:04', '2021-07-29 05:43:04'),
(301, 40, 19, '6', 'Delivery Boy Started the journey.', 'Dear Customer, ktest on his way to pick the order #0D0020210729102618.', 1, '2021-07-29 05:43:10', '2021-07-29 05:54:37');
INSERT INTO `notifications` (`id`, `send_by`, `send_to`, `notification_type`, `title`, `description`, `is_read`, `created_at`, `updated_at`) VALUES
(302, 40, 19, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  ktest reached to  Reliance Shop to pick order #0D0020210729102618.', 1, '2021-07-29 05:43:20', '2021-07-29 05:54:37'),
(303, 40, 19, '8', 'Delivery Boy Picked up order.', 'Dear Customer, ktest Picked the order #0D0020210729102618.', 1, '2021-07-29 05:43:28', '2021-07-29 05:54:37'),
(304, 40, 19, '9', 'Delivery Boy is almost their!', 'Dear Customer, ktest on his way to deliver the order #0D0020210729102618.', 1, '2021-07-29 05:43:33', '2021-07-29 05:54:37'),
(305, 40, 19, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0D0020210729102618 delivered successfully!', 1, '2021-07-29 05:43:56', '2021-07-29 05:54:37'),
(306, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:44:02', '2021-07-29 05:44:02'),
(307, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:44:02', '2021-07-29 05:44:02'),
(308, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:44:03', '2021-07-29 05:44:03'),
(309, 40, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, ktest Assigned for your order #AD0020210729104919.', 1, '2021-07-29 05:44:26', '2021-07-29 05:54:37'),
(310, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:45:03', '2021-07-29 05:45:03'),
(311, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:45:03', '2021-07-29 05:45:03'),
(312, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:45:03', '2021-07-29 05:45:03'),
(313, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:46:02', '2021-07-29 05:46:02'),
(314, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:46:02', '2021-07-29 05:46:02'),
(315, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:46:03', '2021-07-29 05:46:03'),
(316, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:47:03', '2021-07-29 05:47:03'),
(317, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:47:03', '2021-07-29 05:47:03'),
(318, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:47:03', '2021-07-29 05:47:03'),
(319, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:48:03', '2021-07-29 05:48:03'),
(320, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:48:03', '2021-07-29 05:48:03'),
(321, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:48:03', '2021-07-29 05:48:03'),
(322, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:49:02', '2021-07-29 05:49:02'),
(323, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:49:03', '2021-07-29 05:49:03'),
(324, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:49:04', '2021-07-29 05:49:04'),
(325, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:50:02', '2021-07-29 05:50:02'),
(326, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:50:03', '2021-07-29 05:50:03'),
(327, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:50:03', '2021-07-29 05:50:03'),
(328, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:51:03', '2021-07-29 05:51:03'),
(329, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:51:03', '2021-07-29 05:51:03'),
(330, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:51:04', '2021-07-29 05:51:04'),
(331, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:52:03', '2021-07-29 05:52:03'),
(332, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:52:03', '2021-07-29 05:52:03'),
(333, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:52:04', '2021-07-29 05:52:04'),
(334, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:53:03', '2021-07-29 05:53:03'),
(335, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:53:03', '2021-07-29 05:53:03'),
(336, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:53:03', '2021-07-29 05:53:03'),
(337, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:54:03', '2021-07-29 05:54:03'),
(338, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:54:05', '2021-07-29 05:54:05'),
(339, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:54:05', '2021-07-29 05:54:05'),
(340, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:55:03', '2021-07-29 05:55:03'),
(341, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:55:03', '2021-07-29 05:55:03'),
(342, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:55:04', '2021-07-29 05:55:04'),
(343, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:56:03', '2021-07-29 05:56:03'),
(344, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:56:03', '2021-07-29 05:56:03'),
(345, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:56:04', '2021-07-29 05:56:04'),
(346, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:57:03', '2021-07-29 05:57:03'),
(347, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:57:04', '2021-07-29 05:57:04'),
(348, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:57:04', '2021-07-29 05:57:04'),
(349, 19, 40, '11', 'The order cancelled by customer!', 'Dear ktest, Your order #AD0020210729104919 cancelled by customer!', 0, '2021-07-29 05:57:24', '2021-07-29 05:57:24'),
(350, 19, 19, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #AD0020210729104919 cancelled!', 1, '2021-07-29 05:57:24', '2021-07-29 06:30:39'),
(351, 19, 19, '16', 'Order Refund Initiated!', 'Dear Shalini, Your order refund is initiated for the order #AD0020210729104919.', 1, '2021-07-29 05:57:26', '2021-07-29 06:30:39'),
(352, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:58:02', '2021-07-29 05:58:02'),
(353, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:58:02', '2021-07-29 05:58:02'),
(354, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:58:03', '2021-07-29 05:58:03'),
(355, 19, 19, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #AS0S20210729111134 cancelled!', 1, '2021-07-29 05:59:00', '2021-07-29 06:30:39'),
(356, 19, 40, '11', 'The order cancelled by customer!', 'Dear ktest, Your order #AS0S20210729111134 cancelled by customer!', 0, '2021-07-29 05:59:01', '2021-07-29 05:59:01'),
(357, 19, 19, '16', 'Order Refund Initiated!', 'Dear Shalini, Your order refund is initiated for the order #AS0S20210729111134.', 1, '2021-07-29 05:59:03', '2021-07-29 06:30:39'),
(358, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 05:59:03', '2021-07-29 05:59:03'),
(359, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 05:59:03', '2021-07-29 05:59:03'),
(360, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 05:59:04', '2021-07-29 05:59:04'),
(361, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #A0A020210729112927 placed successfully.', 1, '2021-07-29 05:59:29', '2021-07-29 06:30:39'),
(362, 1, 19, '2', 'Order Accepted!', 'Your Order #A0A020210729112927 has been accepted.', 1, '2021-07-29 05:59:44', '2021-07-29 06:30:39'),
(363, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:00:05', '2021-07-29 06:00:05'),
(364, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:00:06', '2021-07-29 06:00:06'),
(365, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:00:06', '2021-07-29 06:00:06'),
(366, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0A020210729112927.', 0, '2021-07-29 06:00:06', '2021-07-29 06:00:06'),
(367, 40, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, ktest Assigned for your order #A0A020210729112927.', 1, '2021-07-29 06:00:16', '2021-07-29 06:30:39'),
(368, 40, 19, '6', 'Delivery Boy Started the journey.', 'Dear Customer, ktest on his way to pick the order #A0A020210729112927.', 1, '2021-07-29 06:00:23', '2021-07-29 06:30:39'),
(369, 40, 19, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  ktest reached to  Reliance Shop to pick order #A0A020210729112927.', 1, '2021-07-29 06:00:27', '2021-07-29 06:30:39'),
(370, 40, 19, '8', 'Delivery Boy Picked up order.', 'Dear Customer, ktest Picked the order #A0A020210729112927.', 1, '2021-07-29 06:00:29', '2021-07-29 06:30:39'),
(371, 40, 19, '9', 'Delivery Boy is almost their!', 'Dear Customer, ktest on his way to deliver the order #A0A020210729112927.', 1, '2021-07-29 06:00:35', '2021-07-29 06:30:39'),
(372, 40, 19, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #A0A020210729112927 delivered successfully!', 1, '2021-07-29 06:00:48', '2021-07-29 06:30:39'),
(373, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:01:03', '2021-07-29 06:01:03'),
(374, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:01:03', '2021-07-29 06:01:03'),
(375, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:01:03', '2021-07-29 06:01:03'),
(376, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #DS0D20210729113114 placed successfully.', 1, '2021-07-29 06:01:16', '2021-07-29 06:30:39'),
(377, 1, 19, '2', 'Order Accepted!', 'Your Order #DS0D20210729113114 has been accepted.', 1, '2021-07-29 06:01:58', '2021-07-29 06:30:39'),
(378, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:02:03', '2021-07-29 06:02:03'),
(379, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:02:03', '2021-07-29 06:02:03'),
(380, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:02:03', '2021-07-29 06:02:03'),
(381, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0D20210729113114.', 0, '2021-07-29 06:02:04', '2021-07-29 06:02:04'),
(382, 40, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, ktest Assigned for your order #DS0D20210729113114.', 1, '2021-07-29 06:02:10', '2021-07-29 06:30:39'),
(383, 19, 40, '11', 'The order cancelled by customer!', 'Dear ktest, Your order #DS0D20210729113114 cancelled by customer!', 0, '2021-07-29 06:02:35', '2021-07-29 06:02:35'),
(384, 19, 19, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #DS0D20210729113114 cancelled!', 1, '2021-07-29 06:02:35', '2021-07-29 06:30:39'),
(385, 19, 19, '16', 'Order Refund Initiated!', 'Dear Shalini, Your order refund is initiated for the order #DS0D20210729113114.', 1, '2021-07-29 06:02:37', '2021-07-29 06:30:39'),
(386, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:03:02', '2021-07-29 06:03:02'),
(387, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:03:03', '2021-07-29 06:03:03'),
(388, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:03:03', '2021-07-29 06:03:03'),
(389, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:04:03', '2021-07-29 06:04:03'),
(390, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:04:03', '2021-07-29 06:04:03'),
(391, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:04:04', '2021-07-29 06:04:04'),
(392, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:05:02', '2021-07-29 06:05:02'),
(393, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:05:02', '2021-07-29 06:05:02'),
(394, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:05:02', '2021-07-29 06:05:02'),
(395, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:06:03', '2021-07-29 06:06:03'),
(396, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:06:03', '2021-07-29 06:06:03'),
(397, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:06:04', '2021-07-29 06:06:04'),
(398, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:07:03', '2021-07-29 06:07:03'),
(399, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:07:13', '2021-07-29 06:07:13'),
(400, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:07:14', '2021-07-29 06:07:14'),
(401, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:08:03', '2021-07-29 06:08:03'),
(402, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:08:11', '2021-07-29 06:08:11'),
(403, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:08:11', '2021-07-29 06:08:11'),
(404, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:09:02', '2021-07-29 06:09:02'),
(405, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:09:02', '2021-07-29 06:09:02'),
(406, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:09:02', '2021-07-29 06:09:02'),
(407, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:10:05', '2021-07-29 06:10:05'),
(408, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:10:05', '2021-07-29 06:10:05'),
(409, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:10:06', '2021-07-29 06:10:06'),
(410, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:11:02', '2021-07-29 06:11:02'),
(411, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:11:02', '2021-07-29 06:11:02'),
(412, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:11:03', '2021-07-29 06:11:03'),
(413, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:12:03', '2021-07-29 06:12:03'),
(414, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:12:03', '2021-07-29 06:12:03'),
(415, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:12:03', '2021-07-29 06:12:03'),
(416, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:13:02', '2021-07-29 06:13:02'),
(417, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:13:03', '2021-07-29 06:13:03'),
(418, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:13:03', '2021-07-29 06:13:03'),
(419, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #0S0D20210729114350 placed successfully.', 0, '2021-07-29 06:13:52', '2021-07-29 06:13:52'),
(420, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:14:03', '2021-07-29 06:14:03'),
(421, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:14:03', '2021-07-29 06:14:03'),
(422, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:14:09', '2021-07-29 06:14:09'),
(423, 1, 13, '2', 'Order Accepted!', 'Your Order #0S0D20210729114350 has been accepted.', 0, '2021-07-29 06:14:11', '2021-07-29 06:14:11'),
(424, 1, 13, '4', 'Order in progress!', 'Your Order #0S0D20210729114350 is in progress.', 0, '2021-07-29 06:14:22', '2021-07-29 06:14:22'),
(425, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:15:03', '2021-07-29 06:15:03'),
(426, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:15:06', '2021-07-29 06:15:06'),
(427, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:15:12', '2021-07-29 06:15:12'),
(428, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:15:22', '2021-07-29 06:15:22'),
(429, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:15:28', '2021-07-29 06:15:28'),
(430, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 1, '2021-07-29 06:15:39', '2021-07-29 23:15:35'),
(431, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:16:04', '2021-07-29 06:16:04'),
(432, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:16:04', '2021-07-29 06:16:04'),
(433, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:16:10', '2021-07-29 06:16:10'),
(434, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:16:10', '2021-07-29 06:16:10'),
(435, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:17:03', '2021-07-29 06:17:03'),
(436, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:17:03', '2021-07-29 06:17:03'),
(437, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:17:04', '2021-07-29 06:17:04'),
(438, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:17:05', '2021-07-29 06:17:05'),
(439, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:18:03', '2021-07-29 06:18:03'),
(440, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:18:03', '2021-07-29 06:18:03'),
(441, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:18:03', '2021-07-29 06:18:03'),
(442, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:18:04', '2021-07-29 06:18:04'),
(443, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:19:02', '2021-07-29 06:19:02'),
(444, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:19:03', '2021-07-29 06:19:03'),
(445, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:19:03', '2021-07-29 06:19:03'),
(446, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:19:03', '2021-07-29 06:19:03'),
(447, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:20:05', '2021-07-29 06:20:05'),
(448, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:20:05', '2021-07-29 06:20:05'),
(449, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:20:05', '2021-07-29 06:20:05'),
(450, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:20:06', '2021-07-29 06:20:06'),
(451, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:21:03', '2021-07-29 06:21:03'),
(452, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:21:03', '2021-07-29 06:21:03'),
(453, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:21:03', '2021-07-29 06:21:03'),
(454, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:21:04', '2021-07-29 06:21:04'),
(455, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:22:02', '2021-07-29 06:22:02'),
(456, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:22:08', '2021-07-29 06:22:08'),
(457, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:22:08', '2021-07-29 06:22:08'),
(458, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:22:08', '2021-07-29 06:22:08'),
(459, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:23:02', '2021-07-29 06:23:02'),
(460, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:23:03', '2021-07-29 06:23:03'),
(461, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:23:03', '2021-07-29 06:23:03'),
(462, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:23:03', '2021-07-29 06:23:03'),
(463, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:24:04', '2021-07-29 06:24:04'),
(464, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:24:04', '2021-07-29 06:24:04'),
(465, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:24:04', '2021-07-29 06:24:04'),
(466, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:24:15', '2021-07-29 06:24:15'),
(467, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:25:03', '2021-07-29 06:25:03'),
(468, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:25:04', '2021-07-29 06:25:04'),
(469, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:25:04', '2021-07-29 06:25:04'),
(470, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:25:05', '2021-07-29 06:25:05'),
(471, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:26:03', '2021-07-29 06:26:03'),
(472, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:26:04', '2021-07-29 06:26:04'),
(473, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:26:04', '2021-07-29 06:26:04'),
(474, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:26:04', '2021-07-29 06:26:04'),
(475, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DS0020210729063749.', 0, '2021-07-29 06:27:03', '2021-07-29 06:27:03'),
(476, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAD20210729074341.', 0, '2021-07-29 06:27:03', '2021-07-29 06:27:03'),
(477, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DA20210729081141.', 0, '2021-07-29 06:27:04', '2021-07-29 06:27:04'),
(478, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0S0D20210729114350.', 0, '2021-07-29 06:27:04', '2021-07-29 06:27:04'),
(479, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DSDD20210729115746 placed successfully.', 0, '2021-07-29 06:27:48', '2021-07-29 06:27:48'),
(480, 1, 13, '2', 'Order Accepted!', 'Your Order #DSDD20210729115746 has been accepted.', 0, '2021-07-29 06:28:05', '2021-07-29 06:28:05'),
(481, 1, 13, '4', 'Order in progress!', 'Your Order #DSDD20210729115746 is in progress.', 0, '2021-07-29 06:28:09', '2021-07-29 06:28:09'),
(482, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #0AAS20210729120020 placed successfully.', 1, '2021-07-29 06:30:22', '2021-07-29 06:30:39'),
(483, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SDAD20210729120557 placed successfully.', 0, '2021-07-29 06:36:04', '2021-07-29 06:36:04'),
(484, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DA0S20210729122825 placed successfully.', 0, '2021-07-29 06:58:29', '2021-07-29 06:58:29'),
(485, 1, 13, '2', 'Order Accepted!', 'Your Order #DA0S20210729122825 has been accepted.', 0, '2021-07-29 06:58:50', '2021-07-29 06:58:50'),
(486, 1, 13, '4', 'Order in progress!', 'Your Order #DA0S20210729122825 is in progress.', 0, '2021-07-29 06:59:12', '2021-07-29 06:59:12'),
(487, 13, 13, '1', 'Order Done Successfully!', 'Dear First, Your order #DD0A20210729130257 placed successfully.', 0, '2021-07-29 07:32:59', '2021-07-29 07:32:59'),
(488, 1, 13, '2', 'Order Accepted!', 'Your Order #DD0A20210729130257 has been accepted.', 0, '2021-07-29 07:34:04', '2021-07-29 07:34:04'),
(489, 1, 13, '4', 'Order in progress!', 'Your Order #DD0A20210729130257 is in progress.', 0, '2021-07-29 07:34:09', '2021-07-29 07:34:09'),
(490, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0AAD20210729074341.', 0, '2021-07-29 07:54:06', '2021-07-29 07:54:06'),
(491, 0, 13, '14', 'Your Order #0AAD20210729074341 got cancelled by system!', 'Your Order #0AAD20210729074341 got cancelled!', 0, '2021-07-29 07:54:06', '2021-07-29 07:54:06'),
(492, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #S0DS20210729134903 placed successfully.', 1, '2021-07-29 08:19:04', '2021-07-29 23:15:21'),
(493, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #S0DS20210729134903 cancelled!', 1, '2021-07-29 08:19:06', '2021-07-29 23:15:21'),
(494, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #S0DS20210729134903.', 1, '2021-07-29 08:19:08', '2021-07-29 23:15:21'),
(495, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #S0DA20210729081141.', 0, '2021-07-29 08:22:05', '2021-07-29 08:22:05'),
(496, 0, 13, '14', 'Your Order #S0DA20210729081141 got cancelled by system!', 'Your Order #S0DA20210729081141 got cancelled!', 0, '2021-07-29 08:22:06', '2021-07-29 08:22:06'),
(497, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D0SS20210730043244 placed successfully.', 1, '2021-07-29 23:02:57', '2021-07-29 23:15:21'),
(498, 1, 1, '2', 'Order Accepted!', 'Your Order #D0SS20210730043244 has been accepted.', 1, '2021-07-29 23:03:37', '2021-07-29 23:15:21'),
(499, 1, 1, '4', 'Order in progress!', 'Your Order #D0SS20210730043244 is in progress.', 1, '2021-07-29 23:05:23', '2021-07-29 23:15:21'),
(500, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0SSS20210730045923 placed successfully.', 0, '2021-07-29 23:29:35', '2021-07-29 23:29:35'),
(501, 1, 1, '2', 'Order Accepted!', 'Your Order #0SSS20210730045923 has been accepted.', 0, '2021-07-29 23:37:14', '2021-07-29 23:37:14'),
(502, 1, 1, '4', 'Order in progress!', 'Your Order #0SSS20210730045923 is in progress.', 0, '2021-07-29 23:37:17', '2021-07-29 23:37:17'),
(503, 0, 24, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SA0A20210729045505.', 0, '2021-07-29 23:41:36', '2021-07-29 23:41:36'),
(504, 0, 24, '14', 'Your Order #SA0A20210729045505 got cancelled by system!', 'Your Order #SA0A20210729045505 got cancelled!', 0, '2021-07-29 23:41:36', '2021-07-29 23:41:36'),
(505, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00S020210729062941.', 0, '2021-07-29 23:41:38', '2021-07-29 23:41:38'),
(506, 0, 13, '14', 'Your Order #00S020210729062941 got cancelled by system!', 'Your Order #00S020210729062941 got cancelled!', 0, '2021-07-29 23:41:38', '2021-07-29 23:41:38'),
(507, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0A0D20210729063253.', 0, '2021-07-29 23:41:40', '2021-07-29 23:41:40'),
(508, 0, 13, '14', 'Your Order #0A0D20210729063253 got cancelled by system!', 'Your Order #0A0D20210729063253 got cancelled!', 0, '2021-07-29 23:41:50', '2021-07-29 23:41:50'),
(509, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DS0020210729063749.', 0, '2021-07-29 23:41:52', '2021-07-29 23:41:52'),
(510, 0, 13, '14', 'Your Order #DS0020210729063749 got cancelled by system!', 'Your Order #DS0020210729063749 got cancelled!', 0, '2021-07-29 23:41:58', '2021-07-29 23:41:58'),
(511, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00SD20210729095401.', 0, '2021-07-29 23:41:59', '2021-07-29 23:41:59'),
(512, 0, 13, '14', 'Your Order #00SD20210729095401 got cancelled by system!', 'Your Order #00SD20210729095401 got cancelled!', 0, '2021-07-29 23:42:00', '2021-07-29 23:42:00'),
(513, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DDSA20210729102656.', 0, '2021-07-29 23:42:01', '2021-07-29 23:42:01'),
(514, 0, 19, '14', 'Your Order #DDSA20210729102656 got cancelled by system!', 'Your Order #DDSA20210729102656 got cancelled!', 0, '2021-07-29 23:42:02', '2021-07-29 23:42:02'),
(515, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #D0AD20210729110741.', 0, '2021-07-29 23:42:03', '2021-07-29 23:42:03'),
(516, 0, 19, '14', 'Your Order #D0AD20210729110741 got cancelled by system!', 'Your Order #D0AD20210729110741 got cancelled!', 0, '2021-07-29 23:42:03', '2021-07-29 23:42:03'),
(517, 0, 19, '14', 'Your Order #D0AD20210729110741 got cancelled by system!', 'Your Order #D0AD20210729110741 got cancelled!', 0, '2021-07-29 23:42:04', '2021-07-29 23:42:04'),
(518, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0S0D20210729114350.', 0, '2021-07-29 23:42:10', '2021-07-29 23:42:10'),
(519, 0, 13, '14', 'Your Order #0S0D20210729114350 got cancelled by system!', 'Your Order #0S0D20210729114350 got cancelled!', 0, '2021-07-29 23:42:11', '2021-07-29 23:42:11'),
(520, 0, 13, '14', 'Your Order #0S0D20210729114350 got cancelled by system!', 'Your Order #0S0D20210729114350 got cancelled!', 0, '2021-07-29 23:42:11', '2021-07-29 23:42:11'),
(521, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DSDD20210729115746.', 0, '2021-07-29 23:42:12', '2021-07-29 23:42:12'),
(522, 0, 13, '14', 'Your Order #DSDD20210729115746 got cancelled by system!', 'Your Order #DSDD20210729115746 got cancelled!', 0, '2021-07-29 23:42:13', '2021-07-29 23:42:13'),
(523, 0, 13, '14', 'Your Order #DSDD20210729115746 got cancelled by system!', 'Your Order #DSDD20210729115746 got cancelled!', 0, '2021-07-29 23:42:13', '2021-07-29 23:42:13'),
(524, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0AAS20210729120020.', 0, '2021-07-29 23:42:14', '2021-07-29 23:42:14'),
(525, 0, 19, '14', 'Your Order #0AAS20210729120020 got cancelled by system!', 'Your Order #0AAS20210729120020 got cancelled!', 0, '2021-07-29 23:42:15', '2021-07-29 23:42:15'),
(526, 0, 19, '14', 'Your Order #0AAS20210729120020 got cancelled by system!', 'Your Order #0AAS20210729120020 got cancelled!', 0, '2021-07-29 23:42:15', '2021-07-29 23:42:15'),
(527, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SDAD20210729120557.', 0, '2021-07-29 23:42:17', '2021-07-29 23:42:17'),
(528, 0, 19, '14', 'Your Order #SDAD20210729120557 got cancelled by system!', 'Your Order #SDAD20210729120557 got cancelled!', 0, '2021-07-29 23:42:17', '2021-07-29 23:42:17'),
(529, 0, 19, '14', 'Your Order #SDAD20210729120557 got cancelled by system!', 'Your Order #SDAD20210729120557 got cancelled!', 0, '2021-07-29 23:42:17', '2021-07-29 23:42:17'),
(530, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DA0S20210729122825.', 0, '2021-07-29 23:42:18', '2021-07-29 23:42:18'),
(531, 0, 13, '14', 'Your Order #DA0S20210729122825 got cancelled by system!', 'Your Order #DA0S20210729122825 got cancelled!', 0, '2021-07-29 23:42:19', '2021-07-29 23:42:19'),
(532, 0, 13, '14', 'Your Order #DA0S20210729122825 got cancelled by system!', 'Your Order #DA0S20210729122825 got cancelled!', 0, '2021-07-29 23:42:19', '2021-07-29 23:42:19'),
(533, 0, 13, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DD0A20210729130257.', 0, '2021-07-29 23:42:20', '2021-07-29 23:42:20'),
(534, 0, 13, '14', 'Your Order #DD0A20210729130257 got cancelled by system!', 'Your Order #DD0A20210729130257 got cancelled!', 0, '2021-07-29 23:42:21', '2021-07-29 23:42:21'),
(535, 0, 13, '14', 'Your Order #DD0A20210729130257 got cancelled by system!', 'Your Order #DD0A20210729130257 got cancelled!', 0, '2021-07-29 23:42:21', '2021-07-29 23:42:21'),
(536, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #00DS20210730052626 placed successfully.', 0, '2021-07-29 23:56:28', '2021-07-29 23:56:28'),
(537, 1, 1, '2', 'Order Accepted!', 'Your Order #00DS20210730052626 has been accepted.', 0, '2021-07-29 23:58:11', '2021-07-29 23:58:11'),
(538, 1, 1, '4', 'Order in progress!', 'Your Order #00DS20210730052626 is in progress.', 0, '2021-07-29 23:58:15', '2021-07-29 23:58:15'),
(539, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210730052626.', 0, '2021-07-29 23:59:03', '2021-07-29 23:59:03'),
(540, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210730052626.', 0, '2021-07-29 23:59:03', '2021-07-29 23:59:03'),
(541, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210730052626.', 1, '2021-07-29 23:59:04', '2021-07-30 00:02:15'),
(542, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #00DS20210730052626.', 0, '2021-07-29 23:59:57', '2021-07-29 23:59:57'),
(543, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #00DS20210730052626 cancelled by customer!', 1, '2021-07-30 00:02:01', '2021-07-30 00:02:15'),
(544, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #00DS20210730052626 cancelled!', 0, '2021-07-30 00:02:01', '2021-07-30 00:02:01'),
(545, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #00DS20210730052626.', 0, '2021-07-30 00:02:03', '2021-07-30 00:02:03'),
(546, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #ADD020210730053400 placed successfully.', 0, '2021-07-30 00:04:02', '2021-07-30 00:04:02'),
(547, 1, 1, '2', 'Order Accepted!', 'Your Order #ADD020210730053400 has been accepted.', 0, '2021-07-30 00:04:35', '2021-07-30 00:04:35'),
(548, 1, 1, '4', 'Order in progress!', 'Your Order #ADD020210730053400 is in progress.', 0, '2021-07-30 00:04:38', '2021-07-30 00:04:38'),
(549, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADD020210730053400.', 0, '2021-07-30 00:05:03', '2021-07-30 00:05:03'),
(550, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADD020210730053400.', 0, '2021-07-30 00:05:04', '2021-07-30 00:05:04'),
(551, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADD020210730053400.', 1, '2021-07-30 00:05:04', '2021-07-30 00:24:30'),
(552, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #ADD020210730053400 cancelled!', 0, '2021-07-30 00:12:05', '2021-07-30 00:12:05'),
(553, 1, 2, '11', 'The order cancelled by customer!', 'Dear Test Driver, Your order #ADD020210730053400 cancelled by customer!', 0, '2021-07-30 00:12:06', '2021-07-30 00:12:06'),
(554, 1, 6, '11', 'The order cancelled by customer!', 'Dear Fggg, Your order #ADD020210730053400 cancelled by customer!', 0, '2021-07-30 00:12:06', '2021-07-30 00:12:06'),
(555, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #ADD020210730053400 cancelled by customer!', 1, '2021-07-30 00:12:06', '2021-07-30 00:24:30'),
(556, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #ADD020210730053400.', 0, '2021-07-30 00:12:08', '2021-07-30 00:12:08'),
(557, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #A0AA20210730054256 placed successfully.', 1, '2021-07-30 00:12:58', '2021-07-30 00:46:37'),
(558, 1, 1, '1', 'Order Rejected!', 'Your Order #A0AA20210730054256 has been rejected. Reason of Rejection : fasfgasdfg sdgadfsgag.', 1, '2021-07-30 00:13:25', '2021-07-30 00:46:37'),
(559, 1, 1, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #A0AA20210730054256.', 1, '2021-07-30 00:13:25', '2021-07-30 00:46:37'),
(560, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SDSS20210730054345 placed successfully.', 1, '2021-07-30 00:13:52', '2021-07-30 00:46:37'),
(561, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730054345.', 0, '2021-07-30 00:14:03', '2021-07-30 00:14:03'),
(562, 1, 1, '2', 'Order Accepted!', 'Your Order #SDSS20210730054345 has been accepted.', 1, '2021-07-30 00:14:03', '2021-07-30 00:46:37'),
(563, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730054345.', 0, '2021-07-30 00:14:03', '2021-07-30 00:14:03'),
(564, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730054345.', 1, '2021-07-30 00:14:03', '2021-07-30 00:24:30'),
(565, 1, 1, '4', 'Order in progress!', 'Your Order #SDSS20210730054345 is in progress.', 1, '2021-07-30 00:14:07', '2021-07-30 00:46:37'),
(566, 2, 6, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Test Driver assigned for the order #SDSS20210730054345.', 0, '2021-07-30 00:24:16', '2021-07-30 00:24:16'),
(567, 2, 7, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Test Driver assigned for the order #SDSS20210730054345.', 1, '2021-07-30 00:24:16', '2021-07-30 00:24:30'),
(568, 2, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Test Driver Assigned for your order #SDSS20210730054345.', 1, '2021-07-30 00:24:17', '2021-07-30 00:46:37'),
(569, 7, 2, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #SDSS20210730054345.', 0, '2021-07-30 00:24:43', '2021-07-30 00:24:43'),
(570, 7, 6, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #SDSS20210730054345.', 0, '2021-07-30 00:24:44', '2021-07-30 00:24:44'),
(571, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #SDSS20210730054345.', 1, '2021-07-30 00:24:44', '2021-07-30 00:46:37'),
(572, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #SDSS20210730054345.', 1, '2021-07-30 00:41:27', '2021-07-30 00:46:37'),
(573, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #SDSS20210730054345.', 1, '2021-07-30 00:41:39', '2021-07-30 00:46:37'),
(574, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #SDSS20210730054345.', 1, '2021-07-30 00:41:47', '2021-07-30 00:46:37'),
(575, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #SDSS20210730054345.', 1, '2021-07-30 00:41:51', '2021-07-30 00:46:37'),
(576, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #SDSS20210730054345 delivered successfully!', 1, '2021-07-30 00:42:37', '2021-07-30 00:46:37'),
(577, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DA0D20210730061346 placed successfully.', 1, '2021-07-30 00:43:50', '2021-07-30 00:46:37'),
(578, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #D0SS20210730043244 cancelled!', 1, '2021-07-30 00:44:20', '2021-07-30 00:46:37'),
(579, 1, 2, '11', 'The order cancelled by customer!', 'Dear Test Driver, Your order #D0SS20210730043244 cancelled by customer!', 0, '2021-07-30 00:44:26', '2021-07-30 00:44:26'),
(580, 1, 6, '11', 'The order cancelled by customer!', 'Dear Fggg, Your order #D0SS20210730043244 cancelled by customer!', 0, '2021-07-30 00:44:26', '2021-07-30 00:44:26'),
(581, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #D0SS20210730043244 cancelled by customer!', 1, '2021-07-30 00:44:28', '2021-07-30 00:48:22'),
(582, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #D0SS20210730043244.', 1, '2021-07-30 00:44:30', '2021-07-30 00:46:37'),
(583, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #0SSS20210730045923 cancelled!', 1, '2021-07-30 00:45:29', '2021-07-30 00:46:37'),
(584, 1, 2, '11', 'The order cancelled by customer!', 'Dear Test Driver, Your order #0SSS20210730045923 cancelled by customer!', 0, '2021-07-30 00:45:52', '2021-07-30 00:45:52'),
(585, 1, 6, '11', 'The order cancelled by customer!', 'Dear Fggg, Your order #0SSS20210730045923 cancelled by customer!', 0, '2021-07-30 00:45:53', '2021-07-30 00:45:53'),
(586, 1, 1, '2', 'Order Accepted!', 'Your Order #DA0D20210730061346 has been accepted.', 1, '2021-07-30 00:45:59', '2021-07-30 00:46:37'),
(587, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #0SSS20210730045923 cancelled by customer!', 1, '2021-07-30 00:46:00', '2021-07-30 00:48:22'),
(588, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0D20210730061346.', 0, '2021-07-30 00:46:03', '2021-07-30 00:46:03'),
(589, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0D20210730061346.', 0, '2021-07-30 00:46:05', '2021-07-30 00:46:05'),
(590, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0D20210730061346.', 1, '2021-07-30 00:46:05', '2021-07-30 00:48:22');
INSERT INTO `notifications` (`id`, `send_by`, `send_to`, `notification_type`, `title`, `description`, `is_read`, `created_at`, `updated_at`) VALUES
(591, 1, 1, '4', 'Order in progress!', 'Your Order #DA0D20210730061346 is in progress.', 1, '2021-07-30 00:46:06', '2021-07-30 00:46:37'),
(592, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #0SSS20210730045923.', 1, '2021-07-30 00:46:13', '2021-07-30 00:46:37'),
(593, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #DA0D20210730061346.', 1, '2021-07-30 00:47:22', '2021-07-30 04:43:44'),
(594, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #DA0D20210730061346 cancelled by customer!', 1, '2021-07-30 00:47:55', '2021-07-30 00:48:22'),
(595, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #DA0D20210730061346 cancelled!', 1, '2021-07-30 00:47:55', '2021-07-30 04:43:44'),
(596, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #DA0D20210730061346.', 1, '2021-07-30 00:47:56', '2021-07-30 04:43:44'),
(597, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SDSS20210730070708 placed successfully.', 0, '2021-07-30 01:37:10', '2021-07-30 01:37:10'),
(598, 1, 19, '2', 'Order Accepted!', 'Your Order #SDSS20210730070708 has been accepted.', 0, '2021-07-30 01:37:48', '2021-07-30 01:37:48'),
(599, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730070708.', 1, '2021-07-30 01:38:03', '2021-07-30 01:42:50'),
(600, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730070708.', 0, '2021-07-30 01:39:03', '2021-07-30 01:39:03'),
(601, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-30 01:41:04', '2021-07-30 01:42:50'),
(602, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-30 01:43:22', '2021-08-02 00:15:28'),
(603, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-30 01:44:09', '2021-08-02 00:15:28'),
(604, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SAAS20210730071423 placed successfully.', 0, '2021-07-30 01:44:27', '2021-07-30 01:44:27'),
(605, 1, 19, '2', 'Order Accepted!', 'Your Order #SAAS20210730071423 has been accepted.', 0, '2021-07-30 01:44:47', '2021-07-30 01:44:47'),
(606, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SAAS20210730071423.', 0, '2021-07-30 01:45:03', '2021-07-30 01:45:03'),
(607, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SAAS20210730071423.', 1, '2021-07-30 01:45:03', '2021-08-02 00:15:28'),
(608, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SAAS20210730071423.', 0, '2021-07-30 01:46:02', '2021-07-30 01:46:02'),
(609, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-30 01:49:12', '2021-08-02 00:15:28'),
(610, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SASD20210730071935 placed successfully.', 0, '2021-07-30 01:49:37', '2021-07-30 01:49:37'),
(611, 1, 7, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-07-30 01:49:54', '2021-08-02 00:15:28'),
(612, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #D0SS20210730075536 placed successfully.', 0, '2021-07-30 02:25:37', '2021-07-30 02:25:37'),
(613, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #S0SS20210730075811 placed successfully.', 0, '2021-07-30 02:28:13', '2021-07-30 02:28:13'),
(614, 42, 42, '1', 'Order Done Successfully!', 'Dear Shivam, Your order #0AAA20210730080025 placed successfully.', 1, '2021-07-30 02:30:28', '2021-07-31 02:44:31'),
(615, 1, 42, '2', 'Order Accepted!', 'Your Order #0AAA20210730080025 has been accepted.', 1, '2021-07-30 02:30:59', '2021-07-31 02:44:31'),
(616, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAA20210730080025.', 0, '2021-07-30 02:31:03', '2021-07-30 02:31:03'),
(617, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AAA20210730080025.', 0, '2021-07-30 02:32:02', '2021-07-30 02:32:02'),
(618, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #00AS20210730102056 placed successfully.', 0, '2021-07-30 04:50:57', '2021-07-30 04:50:57'),
(619, 1, 1, '2', 'Order Accepted!', 'Your Order #00AS20210730102056 has been accepted.', 0, '2021-07-30 04:52:46', '2021-07-30 04:52:46'),
(620, 1, 1, '4', 'Order in progress!', 'Your Order #00AS20210730102056 is in progress.', 0, '2021-07-30 04:52:54', '2021-07-30 04:52:54'),
(621, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210730102056.', 0, '2021-07-30 04:53:03', '2021-07-30 04:53:03'),
(622, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210730102056.', 1, '2021-07-30 04:53:03', '2021-08-02 00:15:28'),
(623, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DD0020210730102914 placed successfully.', 0, '2021-07-30 04:59:16', '2021-07-30 04:59:16'),
(624, 1, 1, '2', 'Order Accepted!', 'Your Order #DD0020210730102914 has been accepted.', 0, '2021-07-30 05:00:14', '2021-07-30 05:00:14'),
(625, 1, 1, '4', 'Order in progress!', 'Your Order #DD0020210730102914 is in progress.', 0, '2021-07-30 05:00:17', '2021-07-30 05:00:17'),
(626, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DD0020210730102914.', 1, '2021-07-30 05:01:03', '2021-08-03 00:32:55'),
(627, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DD0020210730102914.', 0, '2021-07-30 05:01:04', '2021-07-30 05:01:04'),
(628, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #A0SA20210730105859 placed successfully.', 0, '2021-07-30 05:29:01', '2021-07-30 05:29:01'),
(629, 1, 1, '2', 'Order Accepted!', 'Your Order #A0SA20210730105859 has been accepted.', 0, '2021-07-30 05:29:14', '2021-07-30 05:29:14'),
(630, 1, 1, '4', 'Order in progress!', 'Your Order #A0SA20210730105859 is in progress.', 0, '2021-07-30 05:29:17', '2021-07-30 05:29:17'),
(631, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0SA20210730105859.', 1, '2021-07-30 05:30:04', '2021-08-03 00:32:55'),
(632, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0SA20210730105859.', 0, '2021-07-30 05:30:05', '2021-07-30 05:30:05'),
(633, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0SA20210730105859.', 1, '2021-07-30 05:30:06', '2021-08-02 00:15:28'),
(634, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SDSS20210730110155 placed successfully.', 0, '2021-07-30 05:31:57', '2021-07-30 05:31:57'),
(635, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210730102056.', 0, '2021-07-30 05:32:03', '2021-07-30 05:32:03'),
(636, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DD0020210730102914.', 0, '2021-07-30 05:32:03', '2021-07-30 05:32:03'),
(637, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0SA20210730105859.', 0, '2021-07-30 05:32:04', '2021-07-30 05:32:04'),
(638, 1, 1, '2', 'Order Accepted!', 'Your Order #SDSS20210730110155 has been accepted.', 0, '2021-07-30 05:33:13', '2021-07-30 05:33:13'),
(639, 1, 1, '4', 'Order in progress!', 'Your Order #SDSS20210730110155 is in progress.', 0, '2021-07-30 05:33:21', '2021-07-30 05:33:21'),
(640, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730110155.', 0, '2021-07-30 05:34:02', '2021-07-30 05:34:02'),
(641, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730110155.', 1, '2021-07-30 05:34:03', '2021-08-02 00:15:28'),
(642, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSS20210730110155.', 0, '2021-07-30 05:35:03', '2021-07-30 05:35:03'),
(643, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0SS20210730075811.', 0, '2021-07-30 07:20:03', '2021-07-30 07:20:03'),
(644, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DD0020210730102914.', 0, '2021-07-30 10:40:07', '2021-07-30 10:40:07'),
(645, 0, 1, '14', 'Your Order #DD0020210730102914 got cancelled by system!', 'Your Order #DD0020210730102914 got cancelled!', 0, '2021-07-30 10:40:07', '2021-07-30 10:40:07'),
(646, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #A0SA20210730105859.', 0, '2021-07-30 11:09:05', '2021-07-30 11:09:05'),
(647, 0, 1, '14', 'Your Order #A0SA20210730105859 got cancelled by system!', 'Your Order #A0SA20210730105859 got cancelled!', 0, '2021-07-30 11:09:05', '2021-07-30 11:09:05'),
(648, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SDSS20210730110155.', 0, '2021-07-30 11:12:05', '2021-07-30 11:12:05'),
(649, 0, 1, '14', 'Your Order #SDSS20210730110155 got cancelled by system!', 'Your Order #SDSS20210730110155 got cancelled!', 0, '2021-07-30 11:12:06', '2021-07-30 11:12:06'),
(650, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SDSS20210730070708.', 0, '2021-07-30 13:03:05', '2021-07-30 13:03:05'),
(651, 0, 19, '14', 'Your Order #SDSS20210730070708 got cancelled by system!', 'Your Order #SDSS20210730070708 got cancelled!', 0, '2021-07-30 13:03:05', '2021-07-30 13:03:05'),
(652, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SAAS20210730071423.', 0, '2021-07-30 13:03:07', '2021-07-30 13:03:07'),
(653, 0, 19, '14', 'Your Order #SAAS20210730071423 got cancelled by system!', 'Your Order #SAAS20210730071423 got cancelled!', 0, '2021-07-30 13:03:07', '2021-07-30 13:03:07'),
(654, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SASD20210730071935.', 0, '2021-07-30 13:03:08', '2021-07-30 13:03:08'),
(655, 0, 19, '14', 'Your Order #SASD20210730071935 got cancelled by system!', 'Your Order #SASD20210730071935 got cancelled!', 0, '2021-07-30 13:03:08', '2021-07-30 13:03:08'),
(656, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #D0SS20210730075536.', 0, '2021-07-30 13:03:09', '2021-07-30 13:03:09'),
(657, 0, 19, '14', 'Your Order #D0SS20210730075536 got cancelled by system!', 'Your Order #D0SS20210730075536 got cancelled!', 0, '2021-07-30 13:03:09', '2021-07-30 13:03:09'),
(658, 0, 19, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #S0SS20210730075811.', 0, '2021-07-30 13:03:11', '2021-07-30 13:03:11'),
(659, 0, 19, '14', 'Your Order #S0SS20210730075811 got cancelled by system!', 'Your Order #S0SS20210730075811 got cancelled!', 0, '2021-07-30 13:03:11', '2021-07-30 13:03:11'),
(660, 0, 42, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0AAA20210730080025.', 1, '2021-07-30 13:03:12', '2021-07-31 02:44:31'),
(661, 0, 42, '14', 'Your Order #0AAA20210730080025 got cancelled by system!', 'Your Order #0AAA20210730080025 got cancelled!', 1, '2021-07-30 13:03:13', '2021-07-31 02:44:31'),
(662, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00AS20210730102056.', 0, '2021-07-30 13:04:05', '2021-07-30 13:04:05'),
(663, 0, 1, '14', 'Your Order #00AS20210730102056 got cancelled by system!', 'Your Order #00AS20210730102056 got cancelled!', 0, '2021-07-30 13:04:10', '2021-07-30 13:04:10'),
(664, 42, 42, '1', 'Order Done Successfully!', 'Dear Shivam, Your order #0AS020210731055713 placed successfully.', 1, '2021-07-31 00:27:14', '2021-07-31 02:44:31'),
(665, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-07-31 00:28:02', '2021-07-31 00:28:02'),
(666, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DADS20210802054402 placed successfully.', 0, '2021-08-02 00:14:10', '2021-08-02 00:14:10'),
(667, 1, 1, '2', 'Order Accepted!', 'Your Order #DADS20210802054402 has been accepted.', 0, '2021-08-02 00:14:38', '2021-08-02 00:14:38'),
(668, 1, 1, '4', 'Order in progress!', 'Your Order #DADS20210802054402 is in progress.', 0, '2021-08-02 00:14:42', '2021-08-02 00:14:42'),
(669, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #00AS20210802054721 placed successfully.', 0, '2021-08-02 00:17:23', '2021-08-02 00:17:23'),
(670, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DADS20210802054402.', 0, '2021-08-02 00:27:03', '2021-08-02 00:27:03'),
(671, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DADS20210802054402.', 1, '2021-08-02 00:27:14', '2021-08-04 04:56:10'),
(672, 0, 42, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #0AS020210731055713.', 0, '2021-08-02 00:27:21', '2021-08-02 00:27:21'),
(673, 0, 42, '14', 'Your Order #0AS020210731055713 got cancelled by system!', 'Your Order #0AS020210731055713 got cancelled!', 0, '2021-08-02 00:27:22', '2021-08-02 00:27:22'),
(674, 1, 1, '2', 'Order Accepted!', 'Your Order #00AS20210802054721 has been accepted.', 0, '2021-08-02 00:59:52', '2021-08-02 00:59:52'),
(675, 1, 1, '4', 'Order in progress!', 'Your Order #00AS20210802054721 is in progress.', 0, '2021-08-02 00:59:56', '2021-08-02 00:59:56'),
(676, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-02 01:00:05', '2021-08-03 00:32:55'),
(677, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-02 01:00:06', '2021-08-02 01:00:06'),
(678, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-02 01:00:06', '2021-08-04 04:56:10'),
(679, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SDSA20210802063229 placed successfully.', 0, '2021-08-02 01:02:31', '2021-08-02 01:02:31'),
(680, 1, 1, '2', 'Order Accepted!', 'Your Order #SDSA20210802063229 has been accepted.', 0, '2021-08-02 01:02:47', '2021-08-02 01:02:47'),
(681, 1, 1, '4', 'Order in progress!', 'Your Order #SDSA20210802063229 is in progress.', 0, '2021-08-02 01:02:50', '2021-08-02 01:02:50'),
(682, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DADS20210802054402.', 0, '2021-08-02 01:03:03', '2021-08-02 01:03:03'),
(683, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-02 01:03:03', '2021-08-02 01:03:03'),
(684, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-02 01:03:03', '2021-08-03 00:32:55'),
(685, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-02 01:03:04', '2021-08-02 01:03:04'),
(686, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-02 01:04:02', '2021-08-02 01:04:02'),
(687, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #00DS20210802063957 placed successfully.', 0, '2021-08-02 01:09:58', '2021-08-02 01:09:58'),
(688, 1, 1, '2', 'Order Accepted!', 'Your Order #00DS20210802063957 has been accepted.', 0, '2021-08-02 01:10:24', '2021-08-02 01:10:24'),
(689, 1, 1, '4', 'Order in progress!', 'Your Order #00DS20210802063957 is in progress.', 0, '2021-08-02 01:10:27', '2021-08-02 01:10:27'),
(690, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-02 01:11:02', '2021-08-03 00:32:55'),
(691, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-02 01:11:03', '2021-08-02 01:11:03'),
(692, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00AS20210802054721.', 0, '2021-08-02 05:58:05', '2021-08-02 05:58:05'),
(693, 0, 1, '14', 'Your Order #00AS20210802054721 got cancelled by system!', 'Your Order #00AS20210802054721 got cancelled!', 0, '2021-08-02 05:58:06', '2021-08-02 05:58:06'),
(694, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #SDSA20210802063229.', 0, '2021-08-02 06:43:05', '2021-08-02 06:43:05'),
(695, 0, 1, '14', 'Your Order #SDSA20210802063229 got cancelled by system!', 'Your Order #SDSA20210802063229 got cancelled!', 0, '2021-08-02 06:43:07', '2021-08-02 06:43:07'),
(696, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #00DS20210802063957.', 0, '2021-08-02 06:50:05', '2021-08-02 06:50:05'),
(697, 0, 1, '14', 'Your Order #00DS20210802063957 got cancelled by system!', 'Your Order #00DS20210802063957 got cancelled!', 0, '2021-08-02 06:50:06', '2021-08-02 06:50:06'),
(698, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DADS20210802054402.', 0, '2021-08-02 13:04:04', '2021-08-02 13:04:04'),
(699, 0, 1, '14', 'Your Order #DADS20210802054402 got cancelled by system!', 'Your Order #DADS20210802054402 got cancelled!', 0, '2021-08-02 13:04:05', '2021-08-02 13:04:05'),
(700, 0, 1, '14', 'Your Order #DADS20210802054402 got cancelled by system!', 'Your Order #DADS20210802054402 got cancelled!', 0, '2021-08-02 23:54:05', '2021-08-02 23:54:05'),
(701, 0, 1, '14', 'Your Order #00AS20210802054721 got cancelled by system!', 'Your Order #00AS20210802054721 got cancelled!', 0, '2021-08-02 23:54:07', '2021-08-02 23:54:07'),
(702, 0, 1, '14', 'Your Order #SDSA20210802063229 got cancelled by system!', 'Your Order #SDSA20210802063229 got cancelled!', 0, '2021-08-02 23:54:08', '2021-08-02 23:54:08'),
(703, 0, 1, '14', 'Your Order #00DS20210802063957 got cancelled by system!', 'Your Order #00DS20210802063957 got cancelled!', 0, '2021-08-02 23:54:09', '2021-08-02 23:54:09'),
(704, 0, 1, '14', 'Your Order #DADS20210802054402 got cancelled by system!', 'Your Order #DADS20210802054402 got cancelled!', 0, '2021-08-03 00:00:07', '2021-08-03 00:00:07'),
(705, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 00:07:03', '2021-08-03 00:07:03'),
(706, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-03 00:07:13', '2021-08-03 00:32:55'),
(707, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:07:13', '2021-08-03 00:07:13'),
(708, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-03 00:07:14', '2021-08-03 00:32:55'),
(709, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-03 00:07:14', '2021-08-03 00:07:14'),
(710, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-03 00:07:14', '2021-08-03 00:32:55'),
(711, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-03 00:07:15', '2021-08-03 00:07:15'),
(712, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 00:10:04', '2021-08-03 00:10:04'),
(713, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-03 00:10:05', '2021-08-03 00:32:55'),
(714, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:10:05', '2021-08-03 00:10:05'),
(715, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-03 00:10:06', '2021-08-03 00:32:55'),
(716, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-03 00:10:06', '2021-08-03 00:10:06'),
(717, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-03 00:10:06', '2021-08-03 00:32:55'),
(718, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-03 00:10:07', '2021-08-03 00:10:07'),
(719, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 00:11:03', '2021-08-03 00:11:03'),
(720, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-03 00:11:03', '2021-08-03 00:32:55'),
(721, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:11:03', '2021-08-03 00:11:03'),
(722, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-03 00:11:04', '2021-08-03 00:32:55'),
(723, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-03 00:11:04', '2021-08-03 00:11:04'),
(724, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-03 00:11:05', '2021-08-03 00:32:55'),
(725, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-03 00:11:05', '2021-08-03 00:11:05'),
(726, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 00:12:03', '2021-08-03 00:12:03'),
(727, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-03 00:12:04', '2021-08-03 00:32:55'),
(728, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:12:04', '2021-08-03 00:12:04'),
(729, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-03 00:12:04', '2021-08-03 00:32:55'),
(730, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-03 00:12:05', '2021-08-03 00:12:05'),
(731, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-03 00:12:05', '2021-08-03 00:32:55'),
(732, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-03 00:12:05', '2021-08-03 00:12:05'),
(733, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D0DA20210803054532 placed successfully.', 0, '2021-08-03 00:15:35', '2021-08-03 00:15:35'),
(734, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 00:15:50', '2021-08-03 00:15:50'),
(735, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 1, '2021-08-03 00:15:51', '2021-08-03 00:32:55'),
(736, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:15:52', '2021-08-03 00:15:52'),
(737, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 1, '2021-08-03 00:15:52', '2021-08-03 00:32:55'),
(738, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SDSA20210802063229.', 0, '2021-08-03 00:15:52', '2021-08-03 00:15:52'),
(739, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 1, '2021-08-03 00:15:53', '2021-08-03 00:32:55'),
(740, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00DS20210802063957.', 0, '2021-08-03 00:15:53', '2021-08-03 00:15:53'),
(741, 1, 1, '2', 'Order Accepted!', 'Your Order #D0DA20210803054532 has been accepted.', 0, '2021-08-03 00:16:15', '2021-08-03 00:16:15'),
(742, 1, 1, '4', 'Order in progress!', 'Your Order #D0DA20210803054532 is in progress.', 0, '2021-08-03 00:16:19', '2021-08-03 00:16:19'),
(743, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0SSA20210803055554 placed successfully.', 0, '2021-08-03 00:25:56', '2021-08-03 00:25:56'),
(744, 1, 1, '2', 'Order Accepted!', 'Your Order #0SSA20210803055554 has been accepted.', 0, '2021-08-03 00:26:10', '2021-08-03 00:26:10'),
(745, 1, 1, '4', 'Order in progress!', 'Your Order #0SSA20210803055554 is in progress.', 0, '2021-08-03 00:26:15', '2021-08-03 00:26:15'),
(746, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:40:24', '2021-08-03 00:40:24'),
(747, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 00:40:24', '2021-08-03 00:40:24'),
(748, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SSA20210803055554.', 0, '2021-08-03 00:40:25', '2021-08-03 00:40:25'),
(749, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:40:25', '2021-08-03 00:40:25'),
(750, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 00:40:25', '2021-08-03 00:40:25'),
(751, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SSA20210803055554.', 0, '2021-08-03 00:40:25', '2021-08-03 00:40:25'),
(752, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:40:36', '2021-08-03 00:40:36'),
(753, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 00:40:36', '2021-08-03 00:40:36'),
(754, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SSA20210803055554.', 0, '2021-08-03 00:40:37', '2021-08-03 00:40:37'),
(755, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:41:06', '2021-08-03 00:41:06'),
(756, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 00:41:06', '2021-08-03 00:41:06'),
(757, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SSA20210803055554.', 0, '2021-08-03 00:41:06', '2021-08-03 00:41:06'),
(758, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 00:41:11', '2021-08-03 00:41:11'),
(759, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 00:41:11', '2021-08-03 00:41:11'),
(760, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0SSA20210803055554.', 0, '2021-08-03 00:41:12', '2021-08-03 00:41:12'),
(761, 2, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Test Driver Assigned for your order #0SSA20210803055554.', 0, '2021-08-03 00:49:27', '2021-08-03 00:49:27'),
(762, 2, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Test Driver on his way to pick the order #0SSA20210803055554.', 0, '2021-08-03 00:49:39', '2021-08-03 00:49:39'),
(763, 2, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Test Driver reached to  Test vendor y to pick order #0SSA20210803055554.', 0, '2021-08-03 00:49:41', '2021-08-03 00:49:41'),
(764, 2, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Test Driver Picked the order #0SSA20210803055554.', 0, '2021-08-03 00:49:43', '2021-08-03 00:49:43'),
(765, 2, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Test Driver on his way to deliver the order #0SSA20210803055554.', 0, '2021-08-03 00:49:45', '2021-08-03 00:49:45'),
(766, 2, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0SSA20210803055554 delivered successfully!', 0, '2021-08-03 00:49:54', '2021-08-03 00:49:54'),
(767, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DSSS20210803063007 placed successfully.', 0, '2021-08-03 01:00:09', '2021-08-03 01:00:09'),
(768, 1, 1, '2', 'Order Accepted!', 'Your Order #DSSS20210803063007 has been accepted.', 0, '2021-08-03 01:00:22', '2021-08-03 01:00:22'),
(769, 1, 1, '4', 'Order in progress!', 'Your Order #DSSS20210803063007 is in progress.', 0, '2021-08-03 01:00:25', '2021-08-03 01:00:25'),
(770, 1, 1, '10', 'Order Completed!', 'Your Order #DSSS20210803063007 Completed.', 0, '2021-08-03 01:00:44', '2021-08-03 01:00:44'),
(771, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DA0020210803070656 placed successfully.', 0, '2021-08-03 01:36:57', '2021-08-03 01:36:57'),
(772, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #00AS20210802054721.', 0, '2021-08-03 01:37:02', '2021-08-03 01:37:02'),
(773, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DA20210803054532.', 0, '2021-08-03 01:37:03', '2021-08-03 01:37:03'),
(774, 1, 1, '2', 'Order Accepted!', 'Your Order #DA0020210803070656 has been accepted.', 0, '2021-08-03 01:37:39', '2021-08-03 01:37:39'),
(775, 1, 1, '4', 'Order in progress!', 'Your Order #DA0020210803070656 is in progress.', 0, '2021-08-03 01:37:43', '2021-08-03 01:37:43'),
(776, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0020210803070656.', 0, '2021-08-03 01:38:03', '2021-08-03 01:38:03'),
(777, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #D0DA20210803054532 cancelled!', 0, '2021-08-03 01:40:26', '2021-08-03 01:40:26'),
(778, 1, 2, '11', 'The order cancelled by customer!', 'Dear Test Driver, Your order #D0DA20210803054532 cancelled by customer!', 0, '2021-08-03 01:40:26', '2021-08-03 01:40:26'),
(779, 1, 6, '11', 'The order cancelled by customer!', 'Dear Fggg, Your order #D0DA20210803054532 cancelled by customer!', 0, '2021-08-03 01:40:26', '2021-08-03 01:40:26'),
(780, 1, 3, '11', 'The order cancelled by customer!', 'Dear Test Driver Two, Your order #D0DA20210803054532 cancelled by customer!', 0, '2021-08-03 01:40:26', '2021-08-03 01:40:26'),
(781, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #D0DA20210803054532.', 0, '2021-08-03 01:40:28', '2021-08-03 01:40:28'),
(782, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #DA0020210803070656 cancelled!', 0, '2021-08-03 01:40:39', '2021-08-03 01:40:39'),
(783, 1, 3, '11', 'The order cancelled by customer!', 'Dear Test Driver Two, Your order #DA0020210803070656 cancelled by customer!', 0, '2021-08-03 01:40:39', '2021-08-03 01:40:39'),
(784, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #DA0020210803070656.', 0, '2021-08-03 01:40:41', '2021-08-03 01:40:41'),
(785, 1, 1, '4', 'Order in progress!', 'Your Order #00AS20210802054721 is in progress.', 0, '2021-08-03 01:41:50', '2021-08-03 01:41:50'),
(786, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DDAD20210803071518 placed successfully.', 0, '2021-08-03 01:45:20', '2021-08-03 01:45:20'),
(787, 1, 1, '2', 'Order Accepted!', 'Your Order #DDAD20210803071518 has been accepted.', 0, '2021-08-03 01:45:41', '2021-08-03 01:45:41'),
(788, 1, 1, '4', 'Order in progress!', 'Your Order #DDAD20210803071518 is in progress.', 0, '2021-08-03 01:45:44', '2021-08-03 01:45:44'),
(789, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DDAD20210803071518.', 0, '2021-08-03 01:46:02', '2021-08-03 01:46:02'),
(790, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DDAD20210803071518.', 1, '2021-08-03 01:46:03', '2021-08-04 04:56:10'),
(791, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DDAD20210803071518.', 0, '2021-08-03 01:48:03', '2021-08-03 01:48:03'),
(792, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DDAD20210803071518.', 0, '2021-08-03 01:49:03', '2021-08-03 01:49:03'),
(793, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #ADDA20210803071930 placed successfully.', 0, '2021-08-03 01:49:32', '2021-08-03 01:49:32'),
(794, 1, 1, '2', 'Order Accepted!', 'Your Order #ADDA20210803071930 has been accepted.', 0, '2021-08-03 01:50:06', '2021-08-03 01:50:06'),
(795, 1, 1, '4', 'Order in progress!', 'Your Order #ADDA20210803071930 is in progress.', 0, '2021-08-03 01:50:11', '2021-08-03 01:50:11'),
(796, 0, 6, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADDA20210803071930.', 0, '2021-08-03 01:51:02', '2021-08-03 01:51:02'),
(797, 0, 3, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADDA20210803071930.', 0, '2021-08-03 01:52:03', '2021-08-03 01:52:03'),
(798, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SSA020210803072509 placed successfully.', 0, '2021-08-03 01:55:11', '2021-08-03 01:55:11'),
(799, 1, 1, '2', 'Order Accepted!', 'Your Order #SSA020210803072509 has been accepted.', 0, '2021-08-03 01:55:23', '2021-08-03 01:55:23'),
(800, 1, 1, '4', 'Order in progress!', 'Your Order #SSA020210803072509 is in progress.', 0, '2021-08-03 01:55:33', '2021-08-03 01:55:33'),
(801, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SSA020210803072509.', 1, '2021-08-03 01:56:03', '2021-08-04 04:56:10'),
(802, 0, 2, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SSA020210803072509.', 0, '2021-08-03 01:57:03', '2021-08-03 01:57:03'),
(803, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #SSA020210803072509.', 0, '2021-08-03 01:57:29', '2021-08-03 01:57:29'),
(804, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #SSA020210803072509.', 0, '2021-08-03 01:58:08', '2021-08-03 01:58:08'),
(805, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #SSA020210803072509.', 0, '2021-08-03 01:58:09', '2021-08-03 01:58:09'),
(806, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #SSA020210803072509.', 0, '2021-08-03 01:58:11', '2021-08-03 01:58:11'),
(807, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #SSA020210803072509.', 0, '2021-08-03 01:58:12', '2021-08-03 01:58:12'),
(808, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #SSA020210803072509 delivered successfully!', 0, '2021-08-03 01:58:20', '2021-08-03 01:58:20'),
(809, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #ADAD20210803072956 placed successfully.', 0, '2021-08-03 01:59:58', '2021-08-03 01:59:58'),
(810, 0, 40, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0AS020210731055713.', 0, '2021-08-03 02:26:02', '2021-08-03 02:26:02'),
(811, 1, 42, '2', 'Order Accepted!', 'Your Order #0AS020210731055713 has been accepted.', 0, '2021-08-03 02:31:27', '2021-08-03 02:31:27'),
(812, 1, 42, '2', 'Order Accepted!', 'Your Order #0AS020210731055713 has been accepted.', 0, '2021-08-03 02:47:53', '2021-08-03 02:47:53'),
(813, 1, 42, '2', 'Order Accepted!', 'Your Order #0AS020210731055713 has been accepted.', 0, '2021-08-03 02:52:02', '2021-08-03 02:52:02'),
(814, 1, 42, '2', 'Order Accepted!', 'Your Order #0AS020210731055713 has been accepted.', 0, '2021-08-03 02:53:18', '2021-08-03 02:53:18'),
(815, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0SSS20210803121132 placed successfully.', 0, '2021-08-03 06:41:34', '2021-08-03 06:41:34'),
(816, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SDD020210803121645 placed successfully.', 0, '2021-08-03 06:46:47', '2021-08-03 06:46:47'),
(817, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SS0D20210803122034 placed successfully.', 0, '2021-08-03 06:50:35', '2021-08-03 06:50:35'),
(818, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0DA020210803122439 placed successfully.', 0, '2021-08-03 06:54:41', '2021-08-03 06:54:41'),
(819, 1, 1, '2', 'Order Accepted!', 'Your Order #0DA020210803122439 has been accepted.', 0, '2021-08-03 07:03:59', '2021-08-03 07:03:59'),
(820, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0DA020210803122439.', 1, '2021-08-03 07:04:03', '2021-08-04 04:56:10'),
(821, 1, 1, '4', 'Order in progress!', 'Your Order #0DA020210803122439 is in progress.', 0, '2021-08-03 07:04:08', '2021-08-03 07:04:08'),
(822, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DA0D20210803123901 placed successfully.', 0, '2021-08-03 07:09:03', '2021-08-03 07:09:03'),
(823, 1, 1, '2', 'Order Accepted!', 'Your Order #DA0D20210803123901 has been accepted.', 0, '2021-08-03 07:09:35', '2021-08-03 07:09:35'),
(824, 1, 1, '4', 'Order in progress!', 'Your Order #DA0D20210803123901 is in progress.', 0, '2021-08-03 07:09:38', '2021-08-03 07:09:38'),
(825, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0D20210803123901.', 1, '2021-08-03 07:10:05', '2021-08-04 04:56:10'),
(826, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #ADD020210803124655 placed successfully.', 0, '2021-08-03 07:16:57', '2021-08-03 07:16:57'),
(827, 1, 1, '2', 'Order Accepted!', 'Your Order #ADD020210803124655 has been accepted.', 0, '2021-08-03 07:17:08', '2021-08-03 07:17:08'),
(828, 1, 1, '4', 'Order in progress!', 'Your Order #ADD020210803124655 is in progress.', 0, '2021-08-03 07:17:13', '2021-08-03 07:17:13'),
(829, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ADD020210803124655.', 1, '2021-08-03 07:18:02', '2021-08-04 04:56:10'),
(830, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DDDD20210803125717 placed successfully.', 0, '2021-08-03 07:27:19', '2021-08-03 07:27:19'),
(831, 1, 1, '2', 'Order Accepted!', 'Your Order #DDDD20210803125717 has been accepted.', 0, '2021-08-03 07:27:53', '2021-08-03 07:27:53'),
(832, 1, 1, '4', 'Order in progress!', 'Your Order #DDDD20210803125717 is in progress.', 0, '2021-08-03 07:27:56', '2021-08-03 07:27:56'),
(833, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DDDD20210803125717.', 1, '2021-08-03 07:28:02', '2021-08-04 04:56:10'),
(834, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DAAA20210803130008 placed successfully.', 1, '2021-08-03 07:30:10', '2021-08-04 02:12:04'),
(835, 1, 1, '2', 'Order Accepted!', 'Your Order #DAAA20210803130008 has been accepted.', 1, '2021-08-03 07:31:19', '2021-08-04 02:12:04'),
(836, 1, 1, '4', 'Order in progress!', 'Your Order #DAAA20210803130008 is in progress.', 1, '2021-08-03 07:31:23', '2021-08-04 02:12:04'),
(837, 51, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer,  on his way to pick the order #DAAA20210803130008.', 1, '2021-08-03 07:31:37', '2021-08-04 02:12:04'),
(838, 51, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,   reached to  Test vendor y to pick order #DAAA20210803130008.', 1, '2021-08-03 07:31:38', '2021-08-04 02:12:04'),
(839, 51, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer,  Picked the order #DAAA20210803130008.', 1, '2021-08-03 07:31:39', '2021-08-04 02:12:04'),
(840, 51, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer,  on his way to deliver the order #DAAA20210803130008.', 1, '2021-08-03 07:31:39', '2021-08-04 02:12:04'),
(841, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #S0DS20210803130709 placed successfully.', 1, '2021-08-03 07:37:11', '2021-08-04 02:12:04'),
(842, 1, 1, '2', 'Order Accepted!', 'Your Order #S0DS20210803130709 has been accepted.', 1, '2021-08-03 07:42:59', '2021-08-04 02:12:04'),
(843, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #S0DS20210803130709.', 1, '2021-08-03 07:43:02', '2021-08-04 04:56:10'),
(844, 1, 1, '4', 'Order in progress!', 'Your Order #S0DS20210803130709 is in progress.', 1, '2021-08-03 07:43:03', '2021-08-04 02:12:04'),
(845, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #SAAD20210804053617 placed successfully.', 0, '2021-08-04 00:06:19', '2021-08-04 00:06:19'),
(846, 1, 19, '2', 'Order Accepted!', 'Your Order #SAAD20210804053617 has been accepted.', 0, '2021-08-04 00:06:34', '2021-08-04 00:06:34'),
(847, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SAAD20210804053617.', 1, '2021-08-04 00:07:03', '2021-08-04 04:56:10'),
(848, 7, 6, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #00AS20210802054721.', 0, '2021-08-04 00:58:05', '2021-08-04 00:58:05'),
(849, 7, 3, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #00AS20210802054721.', 0, '2021-08-04 00:58:05', '2021-08-04 00:58:05'),
(850, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #00AS20210802054721.', 1, '2021-08-04 00:58:05', '2021-08-04 02:12:04'),
(851, 7, 6, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #00AS20210802054721.', 0, '2021-08-04 01:06:44', '2021-08-04 01:06:44'),
(852, 7, 3, '5', 'Another Delivery Boy Assigned!', 'Dear Delivery Boy, Gsgaysu assigned for the order #00AS20210802054721.', 0, '2021-08-04 01:06:44', '2021-08-04 01:06:44'),
(853, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #00AS20210802054721.', 1, '2021-08-04 01:06:44', '2021-08-04 02:12:04'),
(854, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #00AS20210802054721.', 1, '2021-08-04 01:07:44', '2021-08-04 02:12:04'),
(855, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #00AS20210802054721.', 1, '2021-08-04 01:07:46', '2021-08-04 02:12:04'),
(856, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #00AS20210802054721.', 1, '2021-08-04 01:07:49', '2021-08-04 02:12:04'),
(857, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #00AS20210802054721.', 1, '2021-08-04 01:07:52', '2021-08-04 02:12:04'),
(858, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #00AS20210802054721 delivered successfully!', 1, '2021-08-04 01:08:27', '2021-08-04 02:12:04'),
(859, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #AD0020210804063856 placed successfully.', 0, '2021-08-04 01:08:58', '2021-08-04 01:08:58'),
(860, 1, 19, '2', 'Order Accepted!', 'Your Order #AD0020210804063856 has been accepted.', 0, '2021-08-04 01:09:12', '2021-08-04 01:09:12'),
(861, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #AD0020210804063856.', 1, '2021-08-04 01:10:03', '2021-08-04 04:56:10'),
(862, 7, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #AD0020210804063856.', 0, '2021-08-04 01:10:16', '2021-08-04 01:10:16'),
(863, 7, 19, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #AD0020210804063856.', 0, '2021-08-04 01:10:20', '2021-08-04 01:10:20'),
(864, 7, 19, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Reliance Shop to pick order #AD0020210804063856.', 0, '2021-08-04 01:10:23', '2021-08-04 01:10:23'),
(865, 7, 19, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #AD0020210804063856.', 0, '2021-08-04 01:10:27', '2021-08-04 01:10:27'),
(866, 7, 19, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #AD0020210804063856.', 0, '2021-08-04 01:10:29', '2021-08-04 01:10:29'),
(867, 7, 19, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #AD0020210804063856 delivered successfully!', 0, '2021-08-04 01:10:41', '2021-08-04 01:10:41'),
(868, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DA0S20210804073912 placed successfully.', 1, '2021-08-04 02:09:20', '2021-08-04 02:12:04'),
(869, 1, 1, '2', 'Order Accepted!', 'Your Order #DA0S20210804073912 has been accepted.', 1, '2021-08-04 02:09:55', '2021-08-04 02:12:04'),
(870, 1, 1, '4', 'Order in progress!', 'Your Order #DA0S20210804073912 is in progress.', 1, '2021-08-04 02:10:51', '2021-08-04 02:12:04'),
(871, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DA0S20210804073912.', 1, '2021-08-04 02:11:03', '2021-08-04 04:56:10'),
(872, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #DA0S20210804073912.', 0, '2021-08-04 02:12:30', '2021-08-04 02:12:30'),
(873, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #DA0S20210804073912.', 0, '2021-08-04 02:13:13', '2021-08-04 02:13:13'),
(874, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #DA0S20210804073912.', 0, '2021-08-04 02:13:17', '2021-08-04 02:13:17'),
(875, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #DA0S20210804073912.', 0, '2021-08-04 02:13:18', '2021-08-04 02:13:18'),
(876, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #DA0S20210804073912.', 0, '2021-08-04 02:13:21', '2021-08-04 02:13:21'),
(877, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #DA0S20210804073912 delivered successfully!', 0, '2021-08-04 02:13:40', '2021-08-04 02:13:40'),
(878, 19, 19, '1', 'Order Done Successfully!', 'Dear Shalini, Your order #D0AD20210804081322 placed successfully.', 0, '2021-08-04 02:43:24', '2021-08-04 02:43:24'),
(879, 1, 19, '2', 'Order Accepted!', 'Your Order #D0AD20210804081322 has been accepted.', 0, '2021-08-04 02:43:44', '2021-08-04 02:43:44'),
(880, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0AD20210804081322.', 1, '2021-08-04 02:44:03', '2021-08-04 04:56:10'),
(881, 7, 19, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #D0AD20210804081322.', 0, '2021-08-04 02:44:38', '2021-08-04 02:44:38'),
(882, 7, 19, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #D0AD20210804081322.', 0, '2021-08-04 02:44:41', '2021-08-04 02:44:41'),
(883, 7, 19, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Reliance Shop to pick order #D0AD20210804081322.', 0, '2021-08-04 02:44:49', '2021-08-04 02:44:49'),
(884, 7, 19, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #D0AD20210804081322.', 0, '2021-08-04 02:44:52', '2021-08-04 02:44:52'),
(885, 7, 19, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #D0AD20210804081322.', 0, '2021-08-04 02:44:54', '2021-08-04 02:44:54'),
(886, 7, 19, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #D0AD20210804081322 delivered successfully!', 0, '2021-08-04 02:47:17', '2021-08-04 02:47:17');
INSERT INTO `notifications` (`id`, `send_by`, `send_to`, `notification_type`, `title`, `description`, `is_read`, `created_at`, `updated_at`) VALUES
(887, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #ASDD20210804102201 placed successfully.', 0, '2021-08-04 04:52:03', '2021-08-04 04:52:03'),
(888, 1, 1, '2', 'Order Accepted!', 'Your Order #ASDD20210804102201 has been accepted.', 0, '2021-08-04 04:53:13', '2021-08-04 04:53:13'),
(889, 1, 1, '4', 'Order in progress!', 'Your Order #ASDD20210804102201 is in progress.', 0, '2021-08-04 04:53:37', '2021-08-04 04:53:37'),
(890, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #ASDD20210804102201.', 0, '2021-08-04 05:03:03', '2021-08-04 05:03:03'),
(891, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #S0DS20210803130709 cancelled by customer!', 0, '2021-08-04 05:03:26', '2021-08-04 05:03:26'),
(892, 1, 1, '11', 'The order cancelled successfully!', 'Dear Customer, Your order #S0DS20210803130709 cancelled!', 0, '2021-08-04 05:03:26', '2021-08-04 05:03:26'),
(893, 1, 7, '11', 'The order cancelled by customer!', 'Dear Gsgaysu, Your order #S0DS20210803130709 cancelled by customer!', 0, '2021-08-04 05:03:32', '2021-08-04 05:03:32'),
(894, 1, 1, '16', 'Order Refund Initiated!', 'Dear Test Custmer, Your order refund is initiated for the order #S0DS20210803130709.', 0, '2021-08-04 05:03:39', '2021-08-04 05:03:39'),
(895, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #ASDD20210804102201.', 0, '2021-08-04 05:06:43', '2021-08-04 05:06:43'),
(896, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #ASDD20210804102201.', 0, '2021-08-04 05:06:49', '2021-08-04 05:06:49'),
(897, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #ASDD20210804102201.', 0, '2021-08-04 05:06:51', '2021-08-04 05:06:51'),
(898, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #ASDD20210804102201.', 0, '2021-08-04 05:06:52', '2021-08-04 05:06:52'),
(899, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #ASDD20210804102201.', 0, '2021-08-04 05:07:03', '2021-08-04 05:07:03'),
(900, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #ASDD20210804102201 delivered successfully!', 0, '2021-08-04 05:07:12', '2021-08-04 05:07:12'),
(901, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #A0DS20210804105109 placed successfully.', 0, '2021-08-04 05:21:11', '2021-08-04 05:21:11'),
(902, 1, 1, '2', 'Order Accepted!', 'Your Order #A0DS20210804105109 has been accepted.', 0, '2021-08-04 05:25:47', '2021-08-04 05:25:47'),
(903, 1, 1, '4', 'Order in progress!', 'Your Order #A0DS20210804105109 is in progress.', 0, '2021-08-04 05:25:51', '2021-08-04 05:25:51'),
(904, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0DS20210804105109.', 0, '2021-08-04 05:26:02', '2021-08-04 05:26:02'),
(905, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #A0DS20210804105109.', 0, '2021-08-04 05:26:15', '2021-08-04 05:26:15'),
(906, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #A0DS20210804105109.', 0, '2021-08-04 05:26:25', '2021-08-04 05:26:25'),
(907, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #A0DS20210804105109.', 0, '2021-08-04 05:27:00', '2021-08-04 05:27:00'),
(908, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #A0DS20210804105109.', 0, '2021-08-04 05:27:35', '2021-08-04 05:27:35'),
(909, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #A0DS20210804105109.', 0, '2021-08-04 05:27:51', '2021-08-04 05:27:51'),
(910, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #A0DS20210804105109 delivered successfully!', 0, '2021-08-04 05:30:21', '2021-08-04 05:30:21'),
(911, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DADD20210804113921 placed successfully.', 0, '2021-08-04 06:09:29', '2021-08-04 06:09:29'),
(912, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #DADD20210804113921.', 0, '2021-08-04 06:10:05', '2021-08-04 06:10:05'),
(913, 1, 1, '2', 'Order Accepted!', 'Your Order #DADD20210804113921 has been accepted.', 0, '2021-08-04 06:10:06', '2021-08-04 06:10:06'),
(914, 1, 1, '4', 'Order in progress!', 'Your Order #DADD20210804113921 is in progress.', 0, '2021-08-04 06:10:12', '2021-08-04 06:10:12'),
(915, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D0AA20210804114034 placed successfully.', 0, '2021-08-04 06:10:41', '2021-08-04 06:10:41'),
(916, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #DADD20210804113921.', 0, '2021-08-04 06:16:58', '2021-08-04 06:16:58'),
(917, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #DADD20210804113921.', 0, '2021-08-04 06:17:04', '2021-08-04 06:17:04'),
(918, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #DADD20210804113921.', 0, '2021-08-04 06:17:06', '2021-08-04 06:17:06'),
(919, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #DADD20210804113921.', 0, '2021-08-04 06:17:08', '2021-08-04 06:17:08'),
(920, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #DADD20210804113921.', 0, '2021-08-04 06:17:14', '2021-08-04 06:17:14'),
(921, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #DADD20210804113921 delivered successfully!', 0, '2021-08-04 06:17:25', '2021-08-04 06:17:25'),
(922, 1, 1, '2', 'Order Accepted!', 'Your Order #D0AA20210804114034 has been accepted.', 0, '2021-08-04 06:28:53', '2021-08-04 06:28:53'),
(923, 1, 1, '4', 'Order in progress!', 'Your Order #D0AA20210804114034 is in progress.', 0, '2021-08-04 06:28:56', '2021-08-04 06:28:56'),
(924, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0AA20210804114034.', 0, '2021-08-04 06:29:03', '2021-08-04 06:29:03'),
(925, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #D0AA20210804114034.', 0, '2021-08-04 06:29:16', '2021-08-04 06:29:16'),
(926, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #D0AA20210804114034.', 0, '2021-08-04 06:29:46', '2021-08-04 06:29:46'),
(927, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #D0AA20210804114034.', 0, '2021-08-04 06:29:49', '2021-08-04 06:29:49'),
(928, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #D0AA20210804114034.', 0, '2021-08-04 06:30:04', '2021-08-04 06:30:04'),
(929, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #D0AA20210804114034.', 0, '2021-08-04 06:30:11', '2021-08-04 06:30:11'),
(930, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #D0AA20210804114034 delivered successfully!', 0, '2021-08-04 06:30:26', '2021-08-04 06:30:26'),
(931, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0DAS20210804121146 placed successfully.', 0, '2021-08-04 06:41:47', '2021-08-04 06:41:47'),
(932, 1, 1, '2', 'Order Accepted!', 'Your Order #0DAS20210804121146 has been accepted.', 0, '2021-08-04 06:42:09', '2021-08-04 06:42:09'),
(933, 1, 1, '4', 'Order in progress!', 'Your Order #0DAS20210804121146 is in progress.', 0, '2021-08-04 06:42:12', '2021-08-04 06:42:12'),
(934, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0DAS20210804121146.', 0, '2021-08-04 06:43:03', '2021-08-04 06:43:03'),
(935, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #0DAS20210804121146.', 0, '2021-08-04 06:44:39', '2021-08-04 06:44:39'),
(936, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #0DAS20210804121146.', 0, '2021-08-04 06:44:45', '2021-08-04 06:44:45'),
(937, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #0DAS20210804121146.', 0, '2021-08-04 06:44:51', '2021-08-04 06:44:51'),
(938, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #0DAS20210804121146.', 0, '2021-08-04 06:44:53', '2021-08-04 06:44:53'),
(939, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #0DAS20210804121146.', 0, '2021-08-04 06:44:55', '2021-08-04 06:44:55'),
(940, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0DAS20210804121146 delivered successfully!', 0, '2021-08-04 06:45:05', '2021-08-04 06:45:05'),
(941, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #DA0S20210804134223 placed successfully.', 0, '2021-08-04 08:12:25', '2021-08-04 08:12:25'),
(942, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DA0S20210804134223.', 0, '2021-08-04 13:53:05', '2021-08-04 13:53:05'),
(943, 0, 1, '14', 'Your Order #DA0S20210804134223 got cancelled by system!', 'Your Order #DA0S20210804134223 got cancelled!', 0, '2021-08-04 13:53:06', '2021-08-04 13:53:06'),
(944, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #SSD020210805082305 placed successfully.', 0, '2021-08-05 02:53:07', '2021-08-05 02:53:07'),
(945, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #A0AD20210805083241 placed successfully.', 0, '2021-08-05 03:02:42', '2021-08-05 03:02:42'),
(946, 1, 1, '2', 'Order Accepted!', 'Your Order #SSD020210805082305 has been accepted.', 0, '2021-08-05 04:07:18', '2021-08-05 04:07:18'),
(947, 1, 1, '4', 'Order in progress!', 'Your Order #SSD020210805082305 is in progress.', 0, '2021-08-05 04:07:36', '2021-08-05 04:07:36'),
(948, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #SSD020210805082305.', 0, '2021-08-05 04:08:03', '2021-08-05 04:08:03'),
(949, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #SSD020210805082305.', 0, '2021-08-05 04:08:44', '2021-08-05 04:08:44'),
(950, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #SSD020210805082305.', 0, '2021-08-05 04:09:09', '2021-08-05 04:09:09'),
(951, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #SSD020210805082305.', 0, '2021-08-05 04:09:22', '2021-08-05 04:09:22'),
(952, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #SSD020210805082305.', 0, '2021-08-05 04:09:24', '2021-08-05 04:09:24'),
(953, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #SSD020210805082305.', 0, '2021-08-05 04:09:26', '2021-08-05 04:09:26'),
(954, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #SSD020210805082305 delivered successfully!', 0, '2021-08-05 04:09:41', '2021-08-05 04:09:41'),
(955, 1, 1, '2', 'Order Accepted!', 'Your Order #A0AD20210805083241 has been accepted.', 0, '2021-08-05 04:21:41', '2021-08-05 04:21:41'),
(956, 1, 1, '4', 'Order in progress!', 'Your Order #A0AD20210805083241 is in progress.', 0, '2021-08-05 04:21:45', '2021-08-05 04:21:45'),
(957, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #A0AD20210805083241.', 0, '2021-08-05 04:22:03', '2021-08-05 04:22:03'),
(958, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #A0AD20210805083241.', 0, '2021-08-05 04:22:36', '2021-08-05 04:22:36'),
(959, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #A0AD20210805083241.', 0, '2021-08-05 04:22:42', '2021-08-05 04:22:42'),
(960, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #A0AD20210805083241.', 0, '2021-08-05 04:22:44', '2021-08-05 04:22:44'),
(961, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #A0AD20210805083241.', 0, '2021-08-05 04:22:45', '2021-08-05 04:22:45'),
(962, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #A0AD20210805083241.', 0, '2021-08-05 04:22:47', '2021-08-05 04:22:47'),
(963, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #A0AD20210805083241 delivered successfully!', 0, '2021-08-05 04:22:54', '2021-08-05 04:22:54'),
(964, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #D0DS20210805100047 placed successfully.', 0, '2021-08-05 04:30:50', '2021-08-05 04:30:50'),
(965, 1, 1, '2', 'Order Accepted!', 'Your Order #D0DS20210805100047 has been accepted.', 0, '2021-08-05 04:31:07', '2021-08-05 04:31:07'),
(966, 1, 1, '4', 'Order in progress!', 'Your Order #D0DS20210805100047 is in progress.', 0, '2021-08-05 04:31:18', '2021-08-05 04:31:18'),
(967, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #D0DS20210805100047.', 0, '2021-08-05 04:32:03', '2021-08-05 04:32:03'),
(968, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #D0DS20210805100047.', 0, '2021-08-05 04:33:16', '2021-08-05 04:33:16'),
(969, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #D0DS20210805100047.', 0, '2021-08-05 04:33:21', '2021-08-05 04:33:21'),
(970, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #D0DS20210805100047.', 0, '2021-08-05 04:33:24', '2021-08-05 04:33:24'),
(971, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #D0DS20210805100047.', 0, '2021-08-05 04:33:27', '2021-08-05 04:33:27'),
(972, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #D0DS20210805100047.', 0, '2021-08-05 04:33:28', '2021-08-05 04:33:28'),
(973, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #D0DS20210805100047 delivered successfully!', 0, '2021-08-05 04:33:37', '2021-08-05 04:33:37'),
(974, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Custmer, Your order #0DS020210805101132 placed successfully.', 1, '2021-08-05 04:41:34', '2021-08-13 23:04:41'),
(975, 1, 1, '2', 'Order Accepted!', 'Your Order #0DS020210805101132 has been accepted.', 1, '2021-08-05 04:42:16', '2021-08-13 23:04:41'),
(976, 1, 1, '4', 'Order in progress!', 'Your Order #0DS020210805101132 is in progress.', 1, '2021-08-05 04:42:20', '2021-08-13 23:04:41'),
(977, 0, 7, '15', 'Hurray! You got a new Order request.', 'Hurray! You got a new order request for order #0DS020210805101132.', 0, '2021-08-05 04:43:02', '2021-08-05 04:43:02'),
(978, 7, 1, '5', 'Delivery Boy Assigned.', 'Dear Customer, Gsgaysu Assigned for your order #0DS020210805101132.', 1, '2021-08-05 04:47:46', '2021-08-13 23:04:41'),
(979, 7, 1, '6', 'Delivery Boy Started the journey.', 'Dear Customer, Gsgaysu on his way to pick the order #0DS020210805101132.', 1, '2021-08-05 04:47:50', '2021-08-13 23:04:41'),
(980, 7, 1, '7', 'Delivery Boy Reached to Vendor.', 'Dear Customer,  Gsgaysu reached to  Test vendor y to pick order #0DS020210805101132.', 1, '2021-08-05 04:47:52', '2021-08-13 23:04:41'),
(981, 7, 1, '8', 'Delivery Boy Picked up order.', 'Dear Customer, Gsgaysu Picked the order #0DS020210805101132.', 1, '2021-08-05 04:47:53', '2021-08-13 23:04:41'),
(982, 7, 1, '9', 'Delivery Boy is almost their!', 'Dear Customer, Gsgaysu on his way to deliver the order #0DS020210805101132.', 1, '2021-08-05 04:47:54', '2021-08-13 23:04:41'),
(983, 7, 1, '10', 'Congratulations! Your order delivered successfully.', 'Dear Customer, Your order #0DS020210805101132 delivered successfully!', 1, '2021-08-05 04:48:03', '2021-08-13 23:04:41'),
(984, 0, 42, '14', 'Your Order #0AS020210731055713 got cancelled by system!', 'Your Order #0AS020210731055713 got cancelled!', 0, '2021-08-10 13:04:04', '2021-08-10 13:04:04'),
(985, 58, 58, '1', 'Order Done Successfully!', 'Dear Test, Your order #D0SA20210811092637 placed successfully.', 1, '2021-08-11 03:56:38', '2021-08-11 03:58:41'),
(986, 1, 58, '1', 'Order Rejected!', 'Your Order #D0SA20210811092637 has been rejected. Reason of Rejection : mmm.', 1, '2021-08-11 03:58:22', '2021-08-11 03:58:41'),
(987, 1, 58, '16', 'Order Refund Initiated!', 'Dear Customer! Your Order payment refund initiated for the order #D0SA20210811092637.', 1, '2021-08-11 03:58:23', '2021-08-11 03:58:41'),
(988, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Customers, Your order #AS0A20210813075345 placed successfully.', 1, '2021-08-13 02:23:47', '2021-08-13 23:04:41'),
(989, 1, 63, '15', 'Documents Approval process!', 'We have updated the status of documents, kindly review and perform action if any needed. Thanks for your patience!', 1, '2021-08-15 13:00:49', '2021-08-15 13:03:16'),
(990, 1, 1, '1', 'Order Done Successfully!', 'Dear Test Customers, Your order #DDAD20210818105043 placed successfully.', 0, '2021-08-18 05:20:45', '2021-08-18 05:20:45'),
(991, 2, 5, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(992, 2, 19, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(993, 2, 24, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(994, 2, 26, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(995, 2, 42, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(996, 2, 58, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:17', '2021-08-18 05:34:17'),
(997, 2, 5, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(998, 2, 19, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(999, 2, 24, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(1000, 2, 26, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(1001, 2, 42, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(1002, 2, 58, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:40', '2021-08-18 05:34:40'),
(1003, 2, 5, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1004, 2, 19, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1005, 2, 24, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1006, 2, 26, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1007, 2, 42, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1008, 2, 58, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:34:55', '2021-08-18 05:34:55'),
(1009, 2, 5, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1010, 2, 19, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1011, 2, 24, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1012, 2, 26, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1013, 2, 42, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1014, 2, 58, '0', 'New coupon created', 'Your fav shop has just created a new coupon for you!', 0, '2021-08-18 05:37:29', '2021-08-18 05:37:29'),
(1015, 0, 1, '16', 'Order Refund Initiated!', 'Dear Customer, Your Order refund is initiated for the Order #DDAD20210818105043.', 0, '2021-08-18 13:04:04', '2021-08-18 13:04:04'),
(1016, 0, 1, '14', 'Your Order #DDAD20210818105043 got cancelled by system!', 'Your Order #DDAD20210818105043 got cancelled!', 0, '2021-08-18 13:04:04', '2021-08-18 13:04:04');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00069c25cf69e8f95ca616e78bba13b8b6b4f12d941ada652727c4582795e538be6b448e9c85540c', 2, 3, 'Api access token', '[]', 0, '2021-07-29 02:06:03', '2021-07-29 02:06:03', '2022-07-29 07:36:03'),
('003471b7820b73b7d9903945b559c35edd08431e8b9b92514010085466a79b9b11f59474789f4855', 629, 3, 'Api access token', '[]', 0, '2021-05-05 05:05:00', '2021-05-05 05:05:00', '2022-05-05 10:35:00'),
('0054eba2d0fb4e4380ee474efc75008ffc942497807e8c1c1bc9c649fe90ac08d6fdac9363c7238d', 60, 3, 'Api access token', '[]', 1, '2021-08-18 23:37:28', '2021-08-18 23:37:28', '2022-08-19 05:07:28'),
('0085b0da12e3957e9fa82f95207ae8081523b74f3da31118481b8d8d10dca4484d3921984f4e887c', 759, 3, 'Api access token', '[]', 1, '2021-07-20 03:44:30', '2021-07-20 03:44:30', '2022-07-20 09:14:30'),
('009b82a28bfa4f1541591350d2e4e8c39e5985ff98fcd79e1ec571c4fbddff4e5f2fda651b5e1d3a', 559, 3, 'Api access token', '[]', 0, '2021-04-14 02:27:39', '2021-04-14 02:27:39', '2022-04-14 07:57:39'),
('00ab53e396c343999e79e0fab979c72cf1d6f5ea16b945954050a332b0341162baa4db5497134405', 707, 3, 'Api access token', '[]', 0, '2021-06-08 07:12:42', '2021-06-08 07:12:42', '2022-06-08 12:42:42'),
('00e00b955c7f42642c0216609306fbee95383422135598ca877c866d85ca01e2ac0e7964904adeec', 1, 3, 'Api access token', '[]', 1, '2021-07-27 23:36:18', '2021-07-27 23:36:18', '2022-07-28 05:06:18'),
('00f3d2e0ad773a137bbe52a5a5a39928e2b2202a16362ae9c96130e8cc46b30156430b6505892abf', 611, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:43', '2021-05-04 00:40:43', '2022-05-04 06:10:43'),
('010775aaa902b2224f25b056c9d919dd62f22e96ca83c8f91547ea1113981207732aa521b889b263', 674, 3, 'Api access token', '[]', 0, '2021-06-02 07:06:01', '2021-06-02 07:06:01', '2022-06-02 12:36:01'),
('0150a77a9d80b5ecb69e2dd5389da40423bc0d24053c01fde1458d3cd86bfe7d9361056e7f4694fe', 685, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:35', '2021-06-10 02:04:35', '2022-06-10 07:34:35'),
('01618dca99dfb9b400b22fdb228e9efec27df77af79349c5ad7755acf228f4d921ad49239ac50215', 561, 3, 'Api access token', '[]', 1, '2021-04-14 05:30:27', '2021-04-14 05:30:27', '2022-04-14 11:00:27'),
('016de2911b33e6a54f2da767d675708c78d489a0e6d452f8e6f9256ad57d442f8ed06c42adcff4b6', 669, 3, 'Api access token', '[]', 0, '2021-06-02 00:01:50', '2021-06-02 00:01:50', '2022-06-02 05:31:50'),
('017cd2daf6dc08ab3d69a78a3ca4212ac71a37b1a431f1c6f8b7b5a18e911ff323936a5da8b58389', 16, 3, 'Api access token', '[]', 1, '2021-07-27 04:33:31', '2021-07-27 04:33:31', '2022-07-27 10:03:31'),
('018e5959067c617492e61712d1fc984d8736daa497f4d0db65df9e18dfcebc73b228fc9bfc06a8cb', 666, 3, 'Api access token', '[]', 1, '2021-06-01 05:29:04', '2021-06-01 05:29:04', '2022-06-01 10:59:04'),
('019ccedbc6df6364e90d581438190144ec9872312fb62acd1d19e773db2469a623b1f44c4d76cf12', 399, 3, 'Api access token', '[]', 0, '2021-03-19 04:24:06', '2021-03-19 04:24:06', '2022-03-19 09:54:06'),
('01cd6dfcc2d2da249c721cbdccdfaa0a0426e6c0e59577f2ed6ccc9e8adf86e0a42efb80637dde65', 17, 3, 'Api access token', '[]', 0, '2021-07-28 07:50:26', '2021-07-28 07:50:26', '2022-07-28 13:20:26'),
('01fcde50bf3fb96fa2ac9722cb93118c5e988c81c513b8e876b51509d96adf9147b43391385b5ff0', 620, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:06', '2021-06-02 08:16:06', '2022-06-02 13:46:06'),
('0224aeb8c0f7e7ae48b59db42a140b3644212ab8948f17a9a41456cdc4eeeea8ffd4af92b0ffddb1', 17, 3, 'Api access token', '[]', 1, '2021-07-28 07:49:02', '2021-07-28 07:49:02', '2022-07-28 13:19:02'),
('022a7f9b4a7330da1044588076833ee5c5dd7e3f69c295cbe9ffb346298551b4e22046937c3d44f7', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:42:40', '2021-07-15 07:42:40', '2022-07-15 13:12:40'),
('0234d50529cef4693926dc653e138c6200943027ba5f7d9a38cb95dfa1d5a0b74819bead0910e666', 623, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:48', '2021-05-02 08:41:48', '2022-05-02 14:11:48'),
('023d2162293c5e7e92f574ff1497b817ebad56e20e12220c935c0d8546e501a0ed66d572519dd7bc', 468, 3, 'Api access token', '[]', 0, '2021-03-25 04:44:15', '2021-03-25 04:44:15', '2022-03-25 10:14:15'),
('02525e9a6399007f96b97820b9ee6670b2048571088f7da208d933302fd73685e3525e8a147d28ed', 414, 3, 'Api access token', '[]', 0, '2021-03-19 04:35:48', '2021-03-19 04:35:48', '2022-03-19 10:05:48'),
('0269969bc124a4d89d8e505ce7b8b18da306df970cffdabee70dfdc2aa81e4aa646bbb5c6c8d7fd2', 556, 3, 'Api access token', '[]', 0, '2021-04-14 00:51:13', '2021-04-14 00:51:13', '2022-04-14 06:21:13'),
('0295a12eedd8b1714c81094a962ca0b60b58a4f018ea9e29b4cf7340bd03715ce6782dd1ce07be34', 770, 3, 'Api access token', '[]', 0, '2021-07-16 00:23:10', '2021-07-16 00:23:10', '2022-07-16 05:53:10'),
('029c3f13aa82bfd568beaf62aed58bde21fc126b2d7fc87ce30aeaec4b1894e7e9d861f183e8686a', 715, 3, 'Api access token', '[]', 0, '2021-06-08 09:44:13', '2021-06-08 09:44:13', '2022-06-08 15:14:13'),
('02a392bba4fa21081249ffc99684181846e40ba7f104f3376cca6a70bc979b2231848ee880b04c02', 543, 3, 'Api access token', '[]', 1, '2021-04-06 01:47:39', '2021-04-06 01:47:39', '2022-04-06 07:17:39'),
('02bf1717c72d783273c397013fef36d248ddca55f02e74f664660389ac08c933d84458818767fe88', 471, 3, 'Api access token', '[]', 1, '2021-03-26 00:48:36', '2021-03-26 00:48:36', '2022-03-26 06:18:36'),
('02c061f7ee7affa7d92691cc8d1abac69ce923d24bce4f51a3a0226e9b1cf745d04b28536fc91ebb', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:27:31', '2021-05-11 00:27:31', '2022-05-11 05:57:31'),
('02cd6fd769e544ff8d24107c83aa494d221070997aa62177eeef50018185a3fde26d4cd515a8dab3', 547, 3, 'Api access token', '[]', 0, '2021-04-15 07:31:39', '2021-04-15 07:31:39', '2022-04-15 13:01:39'),
('033f30b08591e5425d3ac2235a879e5577538413739f9889bb90a5118c3a3460683158232b23b0ea', 23, 3, 'Api access token', '[]', 0, '2021-08-05 06:58:34', '2021-08-05 06:58:34', '2022-08-05 12:28:34'),
('034191b2b70a440a28190e4835c3563921db174b62b1ad2fc58b5ded6758158d342f462023be3226', 773, 3, 'Api access token', '[]', 0, '2021-07-20 01:56:09', '2021-07-20 01:56:09', '2022-07-20 07:26:09'),
('0345da3912fb5f879355241f1861adff01b869ae92b3a7be881d7c1c3a0cc3a5a53131e424a73194', 536, 3, 'Api access token', '[]', 0, '2021-04-05 02:22:55', '2021-04-05 02:22:55', '2022-04-05 07:52:55'),
('035e0357111ef8499a7a9965640640d53a05ce061f46274d5a8d6ac0452d8f05c82c5a212e1d169f', 421, 3, 'Api access token', '[]', 0, '2021-03-22 05:23:46', '2021-03-22 05:23:46', '2022-03-22 10:53:46'),
('0388cd14b85622c075598e96cdd1fc4889997eb34818eee815edfd38e7969df73c589b09b6a37dd0', 531, 3, 'Api access token', '[]', 0, '2021-05-13 09:57:36', '2021-05-13 09:57:36', '2022-05-13 15:27:36'),
('03df837a8d6c19dfa902f3a34a2d2e8dd3065853c7d6b2e19035165673c519cf9ca6f011b4abcc83', 762, 3, 'Api access token', '[]', 0, '2021-07-06 04:35:29', '2021-07-06 04:35:29', '2022-07-06 10:05:29'),
('040ac8239f4222a106599fe5b505c173c58ea0e452b8bf54f842cc21817bce4818428c540039d93c', 1, 3, 'Api access token', '[]', 0, '2021-07-29 04:30:59', '2021-07-29 04:30:59', '2022-07-29 10:00:59'),
('0436614ab4e8e7a1c86ebf6c7cc7b76d7dd453f877fd081adc69a6f4cd971ed248e9dda26fd304c9', 462, 3, 'Api access token', '[]', 0, '2021-03-24 05:52:32', '2021-03-24 05:52:32', '2022-03-24 11:22:32'),
('04bfcbbfdb0e8fee0b27e55cf4602eaeca48bc976f0b80d8cf60965b908f45b14496bdbd6e5b1ca5', 470, 3, 'Api access token', '[]', 1, '2021-03-25 06:06:05', '2021-03-25 06:06:05', '2022-03-25 11:36:05'),
('04c056269811db852447b1f95fbc7af423cfc78d18cc21767a3d43766cde727f2049dd67ef62ebd3', 6, 3, 'Api access token', '[]', 1, '2021-08-03 01:36:09', '2021-08-03 01:36:09', '2022-08-03 07:06:09'),
('04c10c3ba90380564b67cecc496199e6c29a4923a0d7a8080b9fb6ff4710c310758aed09eb1a61f5', 768, 3, 'Api access token', '[]', 0, '2021-07-14 06:31:47', '2021-07-14 06:31:47', '2022-07-14 12:01:47'),
('04ce834952e07337389caf0b0132d28b0944a3fcd89b832ed5699634dd463ca8d4b2c7aace36a429', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:13:06', '2021-07-20 02:13:06', '2022-07-20 07:43:06'),
('04f6964923c2efad7bc4afb0676866e16a82175cc93840724c6614ce9df3c6514f0d80ad187872c8', 3, 3, 'Api access token', '[]', 1, '2021-07-29 01:29:16', '2021-07-29 01:29:16', '2022-07-29 06:59:16'),
('050577d081247b659d61282b7733aad43f44f2a48a8bb426bcab0c782ae089cf88d8ed69f720596c', 1, 3, 'Api access token', '[]', 0, '2021-07-30 05:28:56', '2021-07-30 05:28:56', '2022-07-30 10:58:56'),
('052c1f52a63686772e855514767aa28d849a72ce0bfe92276cc929f9551d2ae2993f1f911490d6d1', 765, 3, 'Api access token', '[]', 0, '2021-07-12 01:42:21', '2021-07-12 01:42:21', '2022-07-12 07:12:21'),
('05a4c26b32a439e4c3d28f74d9989aab84b2c8a13b6db41b054500d0ac8a467c05cd9c399236311a', 665, 3, 'Api access token', '[]', 1, '2021-06-09 04:38:47', '2021-06-09 04:38:47', '2022-06-09 10:08:47'),
('05eb43a6b9d5101ed02d52dfe88cb5f863cd34c5796295745fe871171233ab53ae8fef5c5e475b24', 15, 3, 'Api access token', '[]', 0, '2021-07-27 04:09:06', '2021-07-27 04:09:06', '2022-07-27 09:39:06'),
('05feeddf989377208318194128e6fc3688afc40bb8675fc388b36b533a8881f46383b39040b1e4fe', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:44:51', '2021-07-20 02:44:51', '2022-07-20 08:14:51'),
('0607e3854dad7219d44b9023f8e258bf54f04f93033cfcdd61ff1f975e91c58f0a580c20a53839f3', 539, 3, 'Api access token', '[]', 0, '2021-04-08 00:12:16', '2021-04-08 00:12:16', '2022-04-08 05:42:16'),
('060d9560848f1b55eb6d606695e5aac26df9a9cc5fcdf172665f6e6a5edcaf22d9090120f0a913b8', 19, 3, 'Api access token', '[]', 0, '2021-07-27 07:21:06', '2021-07-27 07:21:06', '2022-07-27 12:51:06'),
('06468c9b839baef8c0732f6c0d2f39ba32537b57fa0e80c1a6b66d5f253b476de3d0ede9f2144e61', 774, 3, 'Api access token', '[]', 1, '2021-07-19 09:12:03', '2021-07-19 09:12:03', '2022-07-19 14:42:03'),
('067b05b80a5ee5f3f551b5955f10d20db85d7365d0a5dcff962da94326ba2ca59c90ca314a30e095', 705, 3, 'Api access token', '[]', 1, '2021-06-04 05:09:32', '2021-06-04 05:09:32', '2022-06-04 10:39:32'),
('067b74efefbdd462311714cdd8c6494ad99e03edb2e673c7bf8dcc10125946b0d253280b4372d0d9', 5, 3, 'Api access token', '[]', 1, '2021-08-07 04:25:28', '2021-08-07 04:25:28', '2022-08-07 09:55:28'),
('0682f72c80d0852f28b102854fa5728403cbec3c4131a58410ab3f2a2117fb80bb0d56387c70270f', 455, 3, 'Api access token', '[]', 0, '2021-03-23 22:46:02', '2021-03-23 22:46:02', '2022-03-24 04:16:02'),
('068845badb555dd25f5f184b666778b9701712a22e7a6043a3f5baa551a93c059947b1800c1e8989', 40, 3, 'Api access token', '[]', 1, '2021-07-29 02:21:42', '2021-07-29 02:21:42', '2022-07-29 07:51:42'),
('068846142abc997fdea294c07fe722ca52cf458f699b37157a53426754a145c1a41429f54448c185', 773, 3, 'Api access token', '[]', 0, '2021-07-20 06:40:27', '2021-07-20 06:40:27', '2022-07-20 12:10:27'),
('068c855fe1a6f837b5f0d09ff2bd58b25459336a412472c673a5c830612e46a268d889e637d6e44f', 646, 3, 'Api access token', '[]', 0, '2021-05-14 05:29:33', '2021-05-14 05:29:33', '2022-05-14 10:59:33'),
('06b1aac5d38fc2b76c24d2bf5475e768420aff1d94dad0e7867793397c80f5f996481137934e5657', 5, 3, 'Api access token', '[]', 0, '2021-08-07 04:12:07', '2021-08-07 04:12:07', '2022-08-07 09:42:07'),
('06bd5a898a2e5d531e88d18cf250aa3b39b5a5455f085e1c3341185e5c835b682fdef4d54991316f', 1, 3, 'Api access token', '[]', 0, '2021-08-05 04:30:44', '2021-08-05 04:30:44', '2022-08-05 10:00:44'),
('06d3e2d154b3d05e03f75779eda7b89f38032b6d89025a6bd5f98541ed6854a2054ac1b976c61a6f', 665, 3, 'Api access token', '[]', 0, '2021-07-19 01:03:14', '2021-07-19 01:03:14', '2022-07-19 06:33:14'),
('0700aa37aaf392b95b10c8cd3a27033f520ad59461de8cb2993e9010eb232556c969f5d249c1661d', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:41', '2021-05-04 00:46:41', '2022-05-04 06:16:41'),
('070665e7ccb9f0fc28c884eaf1e2f7cc37499f5db96af0e6b6c0c0afc42bfcb300ddbdad4caa8cdb', 447, 3, 'Api access token', '[]', 0, '2021-03-23 04:09:41', '2021-03-23 04:09:41', '2022-03-23 09:39:41'),
('07680749180ef68cc8f543773a267f4f25211b2a214ed7d1d2afc7d6b357df311291da422f786c42', 488, 3, 'Api access token', '[]', 0, '2021-04-01 05:30:25', '2021-04-01 05:30:25', '2022-04-01 11:00:25'),
('077b627b8c1a98e35cfba6b12b1618cc02f6b0d459def9345eb1960511c85dd6cdf464f002d3f4f6', 519, 3, 'Api access token', '[]', 0, '2021-04-02 07:21:36', '2021-04-02 07:21:36', '2022-04-02 12:51:36'),
('077eee138febb2880d29fd12ca889295408a37fd53fcc046e61dfe6aa97b339f0424a2fdc15ab5e0', 699, 3, 'Api access token', '[]', 0, '2021-06-03 00:40:57', '2021-06-03 00:40:57', '2022-06-03 06:10:57'),
('07df0cf5af52ec79e8d3e014f00da0f0956d8f483c35099efa356f4136f3021b2c9f60ef7287f7b4', 718, 3, 'Api access token', '[]', 0, '2021-06-08 09:49:26', '2021-06-08 09:49:26', '2022-06-08 15:19:26'),
('07fca9f2683a9c2aa67afc843558ca241ce4d8d71afe27977db68d8854c5397b7c4de83320545449', 401, 3, 'Api access token', '[]', 1, '2021-03-17 23:47:51', '2021-03-17 23:47:51', '2022-03-18 05:17:51'),
('080d14dc4a87f34ceb84ee29440ad87ecf449b0bfdd100e166ce3870185f1b603d32531e6ee4991a', 510, 3, 'Api access token', '[]', 0, '2021-04-02 06:05:30', '2021-04-02 06:05:30', '2022-04-02 11:35:30'),
('082a12c3242a49fe0c53f9dbdeb71136c17764ea9a2a52b2513a843e56f4d02ae965dd341fc1105b', 620, 3, 'Api access token', '[]', 0, '2021-05-07 05:11:41', '2021-05-07 05:11:41', '2022-05-07 10:41:41'),
('083a2f7d824d6c03318607c8fb5a5187e8e5666017aea4140c9e47b07a0e9cd01d25f8a2fb2a8507', 589, 3, 'Api access token', '[]', 0, '2021-05-02 07:14:23', '2021-05-02 07:14:23', '2022-05-02 12:44:23'),
('08637529baefc8cc86a20f5d338fa20e4d968fb5ea782eb26f81dede3316d809a17ac599cf9bd2e7', 498, 3, 'Api access token', '[]', 0, '2021-03-30 05:38:21', '2021-03-30 05:38:21', '2022-03-30 11:08:21'),
('08780805a412c7acaad6c10e5f34e0abae16f7c9595e9770ab14edd6c27a3b249a1d2e1c18b43cbc', 542, 3, 'Api access token', '[]', 0, '2021-04-05 05:30:27', '2021-04-05 05:30:27', '2022-04-05 11:00:27'),
('0888263146b1b5fba206ab0aa30a7c4feefcb2bc1ffcea7c14b7dee442eb3773c64a2f1acdc174d2', 40, 3, 'Api access token', '[]', 1, '2021-07-29 02:17:41', '2021-07-29 02:17:41', '2022-07-29 07:47:41'),
('08a394d5b671f8e5895d9565bf14af16319625b3b66f8f1810dca115fae882f29d5ed9e1ae39c69c', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:04', '2021-05-04 01:23:04', '2022-05-04 06:53:04'),
('08aab1d679defd01249768031977275200ea1530aafb039d5a28b7486d0a2589968709b9f4e6e120', 5, 3, 'Api access token', '[]', 1, '2021-07-31 00:24:36', '2021-07-31 00:24:36', '2022-07-31 05:54:36'),
('08b1c53963ea3de3e9a810e6e4313db3cd7547dcb6f288054967649daeaddb3ed806e531d4a204f0', 449, 3, 'Api access token', '[]', 0, '2021-03-23 05:46:47', '2021-03-23 05:46:47', '2022-03-23 11:16:47'),
('08b44720b1308392a85ef49b2af533c4515d34f9d7d1edc230383af08e5735b2e800dc595af01231', 405, 3, 'Api access token', '[]', 0, '2021-03-19 01:53:19', '2021-03-19 01:53:19', '2022-03-19 07:23:19'),
('08c1270584e13cfc829f34d93474cbbc53b68424bf78b003dc7b3334f7ce9a8c6682c107ec65ea65', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:43', '2021-05-04 01:27:43', '2022-05-04 06:57:43'),
('08c6f075d3a9187e2c562bd018ecace1df6e1cfbe387d0342fdeefea77cd9b60a598f24f841aeb10', 7, 3, 'Api access token', '[]', 1, '2021-08-02 00:21:11', '2021-08-02 00:21:11', '2022-08-02 05:51:11'),
('08f35e5db8a395dcaec51e7ee58b2f2888ff200b1538b4ccb93df9833530e4a5757741037f596063', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:57:15', '2021-05-27 04:57:15', '2022-05-27 10:27:15'),
('0944f7684620f7c55d786b2990c6fc0fc30b54f18ac3b916bc93d0eb0bf6d267e03be2d3356c5150', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:03:25', '2021-07-16 06:03:25', '2022-07-16 11:33:25'),
('094990470491c76fa795bf5c0ee25b10be013e89ca2d1e8a0b61883b523252cb47b38afe1b4e5253', 670, 3, 'Api access token', '[]', 0, '2021-06-02 05:44:51', '2021-06-02 05:44:51', '2022-06-02 11:14:51'),
('094b14c56e1c3019e97ef8f1446b0eb1a63fb8bf5c2b63c0dd4d700e0b67376d187a16079ecd9e61', 754, 3, 'Api access token', '[]', 1, '2021-06-24 07:52:43', '2021-06-24 07:52:43', '2022-06-24 13:22:43'),
('09779af0ecc9ea484a170b24148be0f7d84efbada9e411d1734461a0d9ac80be07ea7986c848e71d', 493, 3, 'Api access token', '[]', 0, '2021-03-30 05:53:58', '2021-03-30 05:53:58', '2022-03-30 11:23:58'),
('09a55eedb57372e4d1db73eeff718d6c6faefab27f26b4451c0802d9cc1f4aaf7ee29e2010e1b68f', 18, 3, 'Api access token', '[]', 0, '2021-07-27 07:06:06', '2021-07-27 07:06:06', '2022-07-27 12:36:06'),
('09e73c00084d50f9535521b6bb289be621faadec1f1a2350245416e8a2e199958fec61a23957d319', 399, 1, 'Api access token', '[]', 0, '2021-03-17 06:23:50', '2021-03-17 06:23:50', '2022-03-17 11:53:50'),
('0a1063a38ac9dd720a4dbb1ae050c89fd6cf08c5fa2c1f42034b239fc580f890bd3b3a1eb9646889', 1, 3, 'Api access token', '[]', 1, '2021-07-27 04:24:50', '2021-07-27 04:24:50', '2022-07-27 09:54:50'),
('0a34bfd74304eefa364771594f98cb47d361c2014e87ee2eac212057c9f5cb54b6fdca9afb6daa82', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:06:16', '2021-07-26 07:06:16', '2022-07-26 12:36:16'),
('0a40c8ee110bd647de6db6dd29a22873e6cd7cc65e9700bb53db2dc2f0bc3636ed96dc6ad6a9ce87', 750, 3, 'Api access token', '[]', 0, '2021-06-23 04:46:14', '2021-06-23 04:46:14', '2022-06-23 10:16:14'),
('0a6e743f1b1b6460589bccb933e69569cadc6631f8224515bac03fd62c1cd1d11f5c2963ec19696a', 488, 3, 'Api access token', '[]', 1, '2021-04-01 00:12:45', '2021-04-01 00:12:45', '2022-04-01 05:42:45'),
('0a7f768af1662ecadeb98b5b141aa14228a4e284f5c0e9a678feeb4b538d0a968fa11d38f104efa7', 770, 3, 'Api access token', '[]', 0, '2021-07-15 23:57:57', '2021-07-15 23:57:57', '2022-07-16 05:27:57'),
('0a9832e33ba2473384e6c7f3766f45496e90366a3b5509b2384417240d2f296d3a38d9ff85f880b2', 641, 3, 'Api access token', '[]', 1, '2021-05-13 01:00:11', '2021-05-13 01:00:11', '2022-05-13 06:30:11'),
('0aa1e4979a02a9e16e887df6bb83a7b8e213ded9994144fd86a114002dbb2c09a8511ab7117fd888', 40, 3, 'Api access token', '[]', 0, '2021-07-29 04:55:19', '2021-07-29 04:55:19', '2022-07-29 10:25:19'),
('0ab319cce27954ca85275c8646a72cee1d915b657e2149fc2649c99589b73513ee13a74f6c8a0d7f', 639, 3, 'Api access token', '[]', 0, '2021-06-02 06:34:50', '2021-06-02 06:34:50', '2022-06-02 12:04:50'),
('0ac851c62bf54f7fcef19ddd5901aeb529d9b85aedc421e36f3db58feeab4cd05d1026eec6c6a983', 676, 3, 'Api access token', '[]', 0, '2021-06-02 07:22:51', '2021-06-02 07:22:51', '2022-06-02 12:52:51'),
('0ada107b7ed782fe5c859664ca705e9a9084d8bc0332144e6a9eef89050d051cd4fd9f4e096a7f13', 612, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:18', '2021-05-04 00:49:18', '2022-05-04 06:19:18'),
('0ae9fcf24d584180a58a00f8e23493e86bba9e83577e73aaebc3096a3b9cfe52c02e556e7fdbdca6', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:35:49', '2021-07-20 02:35:49', '2022-07-20 08:05:49'),
('0b0e9a5cb3f1ce1598004a15e96537534b9ac246a164c7f782b5ff730874a1c4cc1e68985036c934', 1, 3, 'Api access token', '[]', 0, '2021-08-03 06:43:21', '2021-08-03 06:43:21', '2022-08-03 12:13:21'),
('0b1e7580e22ea3b07fa03e7f3d5699298fa5017bd4358d2f80aa3422677c6c8c453b20aefc95b50f', 702, 3, 'Api access token', '[]', 0, '2021-06-04 01:35:09', '2021-06-04 01:35:09', '2022-06-04 07:05:09'),
('0b3fd9ae3104c3904d973209a5b73261b8916daaf1f1a3f45e69ce08c9652d4b9e78f239c839d659', 24, 3, 'Api access token', '[]', 0, '2021-07-28 06:53:05', '2021-07-28 06:53:05', '2022-07-28 12:23:05'),
('0bb4c57c3d656c8ae746b109b6714b56e833fd59f6628b0a96fab7896ac575cd9d775a889e18b526', 625, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:06', '2021-05-04 00:52:06', '2022-05-04 06:22:06'),
('0c114be65b3ee72fbf784280143001b94fadde5fa4732ae6a14bb7207688e56344955244750ee695', 656, 3, 'Api access token', '[]', 0, '2021-05-26 05:30:09', '2021-05-26 05:30:09', '2022-05-26 11:00:09'),
('0c19d98971e25f090bbd4a9e52b75eea4bfc507b942810dd9d9a50b00517468e30b33383efadc121', 7, 3, 'Api access token', '[]', 0, '2021-08-02 00:10:58', '2021-08-02 00:10:58', '2022-08-02 05:40:58'),
('0c6c916046782db3f9f5bbd358f0572b1c7725f943201825613fcdd857f9709a215795a471cdcf4e', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:20:44', '2021-07-26 07:20:44', '2022-07-26 12:50:44'),
('0c89793bf825fe1ca3c10ffb97fd046a63948795b9cfc1c7b5e0b9412eafc40e622cf941f95dfd64', 504, 3, 'Api access token', '[]', 0, '2021-04-01 07:10:48', '2021-04-01 07:10:48', '2022-04-01 12:40:48'),
('0c8cbc7125aaaa9b1baf4d8c74a9af03fa4e5bcec1c728a65789f29463fd1ea937f0d7fa5ec753af', 7, 3, 'Api access token', '[]', 1, '2021-07-29 02:09:21', '2021-07-29 02:09:21', '2022-07-29 07:39:21'),
('0c9878d94c41b548b2dbba9fd99fb51904f426f393aa6110d1a2151b1724f6213ca157d239f565b7', 467, 3, 'Api access token', '[]', 0, '2021-03-25 04:22:58', '2021-03-25 04:22:58', '2022-03-25 09:52:58'),
('0c9ae7791ad20cba399e2a2243cf2d8b4ece298e03837d73fed85170d335749248475ff87ddd934c', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:28:49', '2021-07-17 00:28:49', '2022-07-17 05:58:49'),
('0cbd58295915d003a4c15ace540963282202d9e132d5cdab0626006dd61b2cdca195e5dcd9e4a6e8', 533, 3, 'Api access token', '[]', 0, '2021-04-23 08:32:50', '2021-04-23 08:32:50', '2022-04-23 14:02:50'),
('0cc143afbb1a685d59bc081de230fdccfe8eb2b926558f7754bcdda29af99cbec6b6accf1799edd7', 764, 3, 'Api access token', '[]', 1, '2021-07-11 23:50:19', '2021-07-11 23:50:19', '2022-07-12 05:20:19'),
('0d04c4ab7c1cb05cd267a57cf6a417166613e1a7e7908c244e4faaabbb29e4a017b9e86a72ad9795', 534, 3, 'Api access token', '[]', 1, '2021-04-05 01:40:51', '2021-04-05 01:40:51', '2022-04-05 07:10:51'),
('0d4c20d23f4aede5243330fef62c2d4237e3dc8a973c54741ea17bc34eafcbacffd29675f30d9faa', 61, 3, 'Api access token', '[]', 0, '2021-08-13 01:52:42', '2021-08-13 01:52:42', '2022-08-13 07:22:42'),
('0d77087a698b1e13d0107f326dc48b6c9d2c10bb71fd828b42ce648b9cf018388f7f4592ea2d3b9d', 52, 3, 'Api access token', '[]', 0, '2021-08-06 01:16:47', '2021-08-06 01:16:47', '2022-08-06 06:46:47'),
('0d7c4bfa36ccfb75ab0586d6de019569865d801ed8810c5708553281f8ae3c04ec8da9b965b154e3', 544, 3, 'Api access token', '[]', 1, '2021-04-15 01:32:16', '2021-04-15 01:32:16', '2022-04-15 07:02:16'),
('0db552574e549a3ed7422758ac549c47fec2a5ac857b7846faa9dd6e4f9bc4db9ac540a61d4f1996', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:42:36', '2021-07-16 06:42:36', '2022-07-16 12:12:36'),
('0dc7d00a5963d67b1700ff6568ca5a05fffd866d899de732e31cea4387614104bf2b37adac17a4dc', 531, 3, 'Api access token', '[]', 0, '2021-04-05 23:24:15', '2021-04-05 23:24:15', '2022-04-06 04:54:15'),
('0e179309524f6a739b7edfa3eaba0c328df97b40ba5ba5790c3332fb073c73bb5e39a297e8303479', 2, 3, 'Api access token', '[]', 1, '2021-08-03 01:54:51', '2021-08-03 01:54:51', '2022-08-03 07:24:51'),
('0e6ede25bfee924309c4fc3ec6eacc681ca7038fbfa7efa7cd80792261270732208600e470e2ae8d', 633, 3, 'Api access token', '[]', 1, '2021-05-11 04:15:22', '2021-05-11 04:15:22', '2022-05-11 09:45:22'),
('0e6f3e555aeb8cf80844340781d1d722574928d8122a0b9db635e1b0d20b8ef55195e47fcf73e38b', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:50:19', '2021-05-04 06:50:19', '2022-05-04 12:20:19'),
('0eb5e4b11c8be9b2260609fe46931c6805ed9c06c4c16bbc2d3b3d094416ad02b5753a30c7445a7c', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:07:46', '2021-05-12 09:07:46', '2022-05-12 14:37:46'),
('0eda9dea7c4b4fc9ba2a7bff1fdf99ecee06a77b3474316b055a8ca9f00a442919745cf7a48bbf80', 638, 3, 'Api access token', '[]', 1, '2021-05-12 09:41:30', '2021-05-12 09:41:30', '2022-05-12 15:11:30'),
('0f0e7b0939635c7e0860cdfbef197fc3ead847d20d4b6e83be585a92df085393996977f589302d56', 489, 3, 'Api access token', '[]', 1, '2021-03-31 04:03:07', '2021-03-31 04:03:07', '2022-03-31 09:33:07'),
('0f1cff2dd882853745ec9c74509dfa2e27b3502b0a9e6d1d8188d371c08e203a250b03fcce49dcb7', 748, 3, 'Api access token', '[]', 0, '2021-06-17 08:29:09', '2021-06-17 08:29:09', '2022-06-17 13:59:09'),
('0f5560e6048fff8ed2a69f3fff0b026f3a89cc3888ce16cebb04c7a2512b0da30c5decfe3522045d', 656, 3, 'Api access token', '[]', 1, '2021-05-24 05:57:10', '2021-05-24 05:57:10', '2022-05-24 11:27:10'),
('0f6953d01354831c0eb2729c3bc31b3d174e3b9e77e8e0b8f63ab03a091be3d489d11b5183cc37f0', 512, 3, 'Api access token', '[]', 1, '2021-04-02 07:05:57', '2021-04-02 07:05:57', '2022-04-02 12:35:57'),
('0f720a9b0d6571e9637c4d09b3e087823cfe3bd0014ce172baf0766bf750bd3e3cba735757f64386', 484, 3, 'Api access token', '[]', 0, '2021-03-26 05:09:24', '2021-03-26 05:09:24', '2022-03-26 10:39:24'),
('0f7b67d80015303cdf53a60736fd06540b44f0c359844ee1b330cc99564db9eeec7c1272c076fc4a', 17, 3, 'Api access token', '[]', 1, '2021-07-29 04:51:55', '2021-07-29 04:51:55', '2022-07-29 10:21:55'),
('0f941f9c386ea6d33a26f7d886f68c1709fffa01df6bd301bbb9bc85b37e502296fa4133b0fee7b2', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:33:44', '2021-05-11 01:33:44', '2022-05-11 07:03:44'),
('0fa1323bdbb591acd7e59f58fde6f01cbb31c323e910eeb245c494fa59fd9f92d498e8f6d8383552', 424, 3, 'Api access token', '[]', 0, '2021-03-22 05:38:48', '2021-03-22 05:38:48', '2022-03-22 11:08:48'),
('0fd27b4fc03f1a3da5b9c1144dee24383b71be567e40ba4ec92e033854cd513259d16ad91dab148e', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:29:22', '2021-05-08 05:29:22', '2022-05-08 10:59:22'),
('0fd61cf58891373ca0fb95c070ea3675ff745e5c0edbf420c67400682b5ae1fe0d85357d53dbe26b', 5, 3, 'Api access token', '[]', 0, '2021-08-07 01:54:37', '2021-08-07 01:54:37', '2022-08-07 07:24:37'),
('10277824cf48516f4ca2a9afefb90785da69c1f8f3b26dab432a41688a485722f6f17d76bad217a4', 552, 3, 'Api access token', '[]', 0, '2021-04-14 05:20:39', '2021-04-14 05:20:39', '2022-04-14 10:50:39'),
('103c6630127489d1092521ab166db0d05c79e9bc14446d0967798706fa3ce548eb19e95a2a060363', 7, 3, 'Api access token', '[]', 1, '2021-07-30 02:23:25', '2021-07-30 02:23:25', '2022-07-30 07:53:25'),
('10616a766036ec122476b73c02ae2fcb2e20e55d54c83b677f0e4dbb5f36c7e677b6bf17bffba394', 52, 3, 'Api access token', '[]', 0, '2021-08-04 05:48:39', '2021-08-04 05:48:39', '2022-08-04 11:18:39'),
('107a7acbccdca821c4f0913ee37fa7affa007d72c12d50c9bfcc04cba8625b97d1b2e77fdab2b2c1', 1, 3, 'Api access token', '[]', 0, '2021-08-19 00:40:29', '2021-08-19 00:40:29', '2022-08-19 06:10:29'),
('10bec44f8172dfd4ddf84b24ba2dcecb11c50d0817337fe9e8ad792c7052510ce76f3249a379838a', 517, 3, 'Api access token', '[]', 0, '2021-04-02 07:13:42', '2021-04-02 07:13:42', '2022-04-02 12:43:42'),
('10f76f399adb594fe1ab3b731c84206eb9007fb60653a01d2e143a7b542662a124075842a466e3a4', 773, 3, 'Api access token', '[]', 0, '2021-07-16 01:55:35', '2021-07-16 01:55:35', '2022-07-16 07:25:35'),
('1119cbe9b0afd26ef0a033dc90ad6bb87da29311237f473022ce8031cd139f525994cd09d69fe5b6', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:04:43', '2021-07-26 07:04:43', '2022-07-26 12:34:43'),
('1120796d81587f121c9ef1ea415069e22f4b1df65243a31577fd7df662e0f1c0128deacbdd7a562e', 19, 3, 'Api access token', '[]', 0, '2021-07-29 04:55:48', '2021-07-29 04:55:48', '2022-07-29 10:25:48'),
('113fb28f344c949c208bf22dbe2e7a4fd9684b1f73e1eb9344c9f23c7e0e90a3aebbe5588d23b883', 513, 3, 'Api access token', '[]', 0, '2021-04-02 06:47:30', '2021-04-02 06:47:30', '2022-04-02 12:17:30'),
('114ebe533c043e9e4243fc61f678778ab5a732ee2261b7c1731ea40d273fd40e017b8b43a6e4aee4', 460, 3, 'Api access token', '[]', 0, '2021-03-24 04:43:50', '2021-03-24 04:43:50', '2022-03-24 10:13:50'),
('116b820a63d4784a39cee201b08edce069dad120d20e4c070227ceeacfaedff05d2d1718d91cab90', 573, 3, 'Api access token', '[]', 0, '2021-04-20 02:20:27', '2021-04-20 02:20:27', '2022-04-20 07:50:27'),
('1177ad2997fe1c1984b806b384a53710ba0702ac3e361ef1771aa79bac933bb6434a6fbd4556859d', 409, 3, 'Api access token', '[]', 0, '2021-03-18 07:17:10', '2021-03-18 07:17:10', '2022-03-18 12:47:10'),
('1197da0812377c3e91ecacb5870007bed1f2a47a425e0c96c66acac39907be28a892de5de4c3a60e', 493, 3, 'Api access token', '[]', 1, '2021-03-31 05:35:19', '2021-03-31 05:35:19', '2022-03-31 11:05:19'),
('11a1b89bb989c1d5e7c4e1f57553a03a701f6842621892d22c109f172fb43c8fe41f63d8139a5b1d', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:49:17', '2021-05-11 01:49:17', '2022-05-11 07:19:17'),
('11da2dc96af4bc74d7f5a82b53cf1f1dc558eb8fe71c01ee108d2fb5467cc3d080a8e01958b545fd', 612, 3, 'Api access token', '[]', 0, '2021-05-02 09:21:43', '2021-05-02 09:21:43', '2022-05-02 14:51:43'),
('11eb3c28cec3db710c77f64121e90655dc2e376155216327f66ebd5703e422405c2dffde0983199f', 6, 3, 'Api access token', '[]', 0, '2021-08-03 01:57:34', '2021-08-03 01:57:34', '2022-08-03 07:27:34'),
('11ef8dc48f052df13b01d42bc246234c59575df44b86d1c90a14e62fa9986ed4e9ab8c652d761905', 611, 3, 'Api access token', '[]', 0, '2021-05-02 08:06:19', '2021-05-02 08:06:19', '2022-05-02 13:36:19'),
('1226ee9139362dfe7d0963ab9a55b5c92e240ff29d1a6b9753ffa5f738e9b575497dd22d5b28220a', 1, 3, 'Api access token', '[]', 0, '2021-08-13 01:41:49', '2021-08-13 01:41:49', '2022-08-13 07:11:49'),
('1255ce24ced6b899bf0176f6f86858109a62e8f95eabc5b71ea9cad2a08423204badc1af47a52c94', 585, 3, 'Api access token', '[]', 0, '2021-04-21 22:51:23', '2021-04-21 22:51:23', '2022-04-22 04:21:23'),
('12640e86618a51037ae4eacaeb831d906f50c7f0d838120e41df9e269265302608f7521e535774db', 773, 3, 'Api access token', '[]', 0, '2021-07-20 04:53:59', '2021-07-20 04:53:59', '2022-07-20 10:23:59'),
('1288239613bdc0a63dc03ee05a79ade134eb33f13279d1af8732235ecc4e16fb71c6157b20109210', 546, 3, 'Api access token', '[]', 0, '2021-04-08 05:07:38', '2021-04-08 05:07:38', '2022-04-08 10:37:38'),
('128949c1ba867ccb1826ab1027e346c9eec96328a05b53df946d1800551e61b1acd80a9beeb43b9d', 656, 3, 'Api access token', '[]', 1, '2021-05-26 05:31:06', '2021-05-26 05:31:06', '2022-05-26 11:01:06'),
('12d7b6f100984b10446d09b2bb8cc1d4bb4bc3097daed630b20417f3744d900e2c71677af4f62c24', 34, 3, 'Api access token', '[]', 0, '2021-07-28 07:27:33', '2021-07-28 07:27:33', '2022-07-28 12:57:33'),
('131afb324464cdb0607f14aab4aa590de0619a92ed0f34a20ada9079b210f675c78390985a140976', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:23:46', '2021-03-22 05:23:46', '2022-03-22 10:53:46'),
('1323133c0508e4ec412dacf54e4823c8ae6b6fecaa9e9f834560ba6b940c1a15d41110f22353a5bb', 35, 3, 'Api access token', '[]', 0, '2021-07-28 07:30:04', '2021-07-28 07:30:04', '2022-07-28 13:00:04'),
('132ac2b2a565f84c938bd2805f53415791649b19791fae8e3d3477ac29f64656a7d70c8afad0ad42', 708, 3, 'Api access token', '[]', 0, '2021-06-08 07:12:43', '2021-06-08 07:12:43', '2022-06-08 12:42:43'),
('132e65bdbd9528cf1c35aed72f9646b3918e8e8da35684518a6f12fe75325dd72c49c09c58bacab5', 527, 3, 'Api access token', '[]', 0, '2021-04-03 01:45:29', '2021-04-03 01:45:29', '2022-04-03 07:15:29'),
('1337d0d76b39bbdfefb555488fe6323e6f7ec35f5168f23068e098eb100a784abba963ba3145b5e5', 773, 3, 'Api access token', '[]', 0, '2021-07-19 08:25:50', '2021-07-19 08:25:50', '2022-07-19 13:55:50'),
('134a5fc9ebddd95b1467c473984d33cfb21b65e70c16deff03721588a43d5126ba95d14e670eb687', 774, 3, 'Api access token', '[]', 0, '2021-07-19 09:12:02', '2021-07-19 09:12:02', '2022-07-19 14:42:02'),
('1371adf701bb40217491d2f0c0d84fcd08be07f79a7ab2bdc8cb4726116b77508feb4620f6d4a853', 504, 3, 'Api access token', '[]', 0, '2021-04-02 02:45:13', '2021-04-02 02:45:13', '2022-04-02 08:15:13'),
('13ade1c92f7b8c176a884cfaa93ad87709e1968651607e13eef92b3e85b98434bb20a6c29eecc5f7', 612, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:19', '2021-05-15 06:22:19', '2022-05-15 11:52:19'),
('13d430602281c72a679707258b303bc3af831b0aecdc89f7266792867680c8b296ce197c638db1b3', 493, 3, 'Api access token', '[]', 1, '2021-03-30 00:28:09', '2021-03-30 00:28:09', '2022-03-30 05:58:09'),
('13fca012221e2f0b093f4f6ad2d5d695a0be5c0730225924281186d12113b77f2d4c0993d339edf0', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:41:35', '2021-05-12 04:41:35', '2022-05-12 10:11:35'),
('1400b82a6c0df34b901b0d8e996b9a0b99dea99cf34e588ed6ec6449a8a4c04b6c073318e3537237', 665, 3, 'Api access token', '[]', 0, '2021-06-17 01:15:19', '2021-06-17 01:15:19', '2022-06-17 06:45:19'),
('1406eabd1a64e54805dae4cbd3a3e8a7fc3b3afcc5ccc07d4454f089987847656c88bb34be42ecad', 1, 3, 'Api access token', '[]', 0, '2021-08-02 01:09:54', '2021-08-02 01:09:54', '2022-08-02 06:39:54'),
('141eeef42d88ad328721e3da0b25eacdb1405401b52c699a681edd6250697b756d9f677569811cef', 499, 3, 'Api access token', '[]', 0, '2021-03-31 02:31:02', '2021-03-31 02:31:02', '2022-03-31 08:01:02'),
('142ced7cd6ecc707ab322751f334ad5a8e1db2ae4d3b0726664c67cd6faeb9bd644e9c2ac735a2b4', 634, 3, 'Api access token', '[]', 0, '2021-05-12 01:02:53', '2021-05-12 01:02:53', '2022-05-12 06:32:53'),
('142d6daeb2e417c8bec37047421bb9798ad3eda5bb0029c7ebce9920f09eb4a72fd27f459ded63b3', 17, 3, 'Api access token', '[]', 0, '2021-07-27 07:24:29', '2021-07-27 07:24:29', '2022-07-27 12:54:29'),
('1465e0df2378ef7995031874f01cd81b510f351d75060c15b891495dd4d469b1c9328507c82688bb', 638, 3, 'Api access token', '[]', 0, '2021-05-12 08:53:04', '2021-05-12 08:53:04', '2022-05-12 14:23:04'),
('1474a1b016d7b82efb386ffc27003b124369d6cd65ba1b4d9bc8bdfafec3c4010b9ab4d65b869c91', 1, 3, 'Api access token', '[]', 0, '2021-07-22 23:24:05', '2021-07-22 23:24:05', '2022-07-23 04:54:05'),
('148a89af507d39613f7a21e633ba1c3b0a96bb4f942250e3b4b982635b5711702278aeb5ff916e83', 753, 3, 'Api access token', '[]', 1, '2021-07-21 01:16:30', '2021-07-21 01:16:30', '2022-07-21 06:46:30'),
('14d1d06951e19d0fb62a72864f2fa8b415864447f10b8b1f17cb5b996e40d8bcaff9181ec427d6c8', 433, 3, 'Api access token', '[]', 1, '2021-03-23 02:38:26', '2021-03-23 02:38:26', '2022-03-23 08:08:26'),
('150db2c20f70e25add34d8026d4149289c96f442c225791c0de6a9f192f57d78580c1e968da28a39', 569, 3, 'Api access token', '[]', 0, '2021-04-18 23:26:42', '2021-04-18 23:26:42', '2022-04-19 04:56:42'),
('1512356170023d4638feca0342885242d54b252c07a6627262c11482f6770596e8efc7d6d46d5efe', 665, 3, 'Api access token', '[]', 0, '2021-05-30 23:22:29', '2021-05-30 23:22:29', '2022-05-31 04:52:29'),
('152c03511dbfd9876c39a4a74210607c03a233a176fcba1dd3fa352da3d5aee04f27e1c85e837e88', 529, 3, 'Api access token', '[]', 0, '2021-04-03 06:35:51', '2021-04-03 06:35:51', '2022-04-03 12:05:51'),
('1559844fad26b7b7d331599146e95cf4782ec36d0c0809b0ac75b8e129023ec7384dbd568e8f8123', 5, 3, 'Api access token', '[]', 0, '2021-08-07 04:08:44', '2021-08-07 04:08:44', '2022-08-07 09:38:44'),
('156b48d4df50e525a32c893a078acb15a44ede12fefe30b03631ddbf4fd5fc5a1b4e475ad1f87854', 645, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('15731d9a6a8e3ce8838ab1958cdb9d61a7151b415c09309cf3e725038e9904585edb5f1bf0379dbc', 552, 3, 'Api access token', '[]', 1, '2021-04-14 00:36:40', '2021-04-14 00:36:40', '2022-04-14 06:06:40'),
('15a927b95ee379b9fd4984041df743d5faba600667ed5db10394e91e7107695f347373e53d39697f', 5, 3, 'Api access token', '[]', 1, '2021-08-11 00:21:11', '2021-08-11 00:21:11', '2022-08-11 05:51:11'),
('15b1886b6510d77b51f99c6d238470f9dd4e2fca7e70864ba42e45ce0d63587aead73b9ad365f5b0', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:34:37', '2021-05-12 02:34:37', '2022-05-12 08:04:37'),
('15cbc457993b3d9f9c89857e4ebd631ade8521d4feb86d8c2602377a1ad04c46350ed4284d9d7dc5', 472, 3, 'Api access token', '[]', 0, '2021-03-26 00:57:52', '2021-03-26 00:57:52', '2022-03-26 06:27:52'),
('16064cbabbf6b150b181a56d3d41883fe0f2a39c254ddf2aed1df05b1e95b1a3649a5fefdb0e074f', 2, 3, 'Api access token', '[]', 1, '2021-07-23 07:40:37', '2021-07-23 07:40:37', '2022-07-23 13:10:37'),
('160db09a91a460745db1051f458a3bf980efcb2cf7c71d0c86e07d15a4eb82b9a6b4abd0cb82f32c', 490, 3, 'Api access token', '[]', 0, '2021-03-26 06:30:34', '2021-03-26 06:30:34', '2022-03-26 12:00:34'),
('163118a483e7841d69801e98982a7b12ef5a970f9b3efc479e4f4e1b2d71630d440a438b0ea2e75a', 411, 3, 'Api access token', '[]', 0, '2021-03-18 08:14:23', '2021-03-18 08:14:23', '2022-03-18 13:44:23'),
('16436406783dded49b1ccf1ecfe42508ad4fca16b91be2ccf2a0350fd8d8ecdb33c62aae5a945a49', 544, 3, 'Api access token', '[]', 0, '2021-04-06 04:20:39', '2021-04-06 04:20:39', '2022-04-06 09:50:39'),
('166e0bac8a01e1328c4747544568e818d6283d5a5e1e4f1448ace3b739f2cc6deee724eb433fbf21', 531, 3, 'Api access token', '[]', 0, '2021-05-13 09:58:32', '2021-05-13 09:58:32', '2022-05-13 15:28:32'),
('167967ddd33ebfdc8d5bbad2ca2accf9b750863bf039c0d4f53dbb0a456cda63f659975f00531862', 496, 3, 'Api access token', '[]', 1, '2021-03-30 00:44:39', '2021-03-30 00:44:39', '2022-03-30 06:14:39'),
('16b178718620e3f5bdc4d4e5352a4abfaaee5e4ea42e20f208ecb2477c2ce7069bca1e42581f45c1', 636, 3, 'Api access token', '[]', 0, '2021-05-12 04:56:23', '2021-05-12 04:56:23', '2022-05-12 10:26:23'),
('16bf6925a15ca8eaf379232b29428be6e4fd6249430b019acf45541d85d76e57705ecc9c9c12eeb8', 702, 3, 'Api access token', '[]', 1, '2021-06-04 04:39:00', '2021-06-04 04:39:00', '2022-06-04 10:09:00'),
('172c9f490c19b5a2018df6f9b5cc7110853fe84ac9e777045a467d4262f59a80caaf6aaf50a4e9fe', 396, 3, 'Api access token', '[]', 0, '2021-03-18 06:51:51', '2021-03-18 06:51:51', '2022-03-18 12:21:51'),
('174204c2308ba2e403dec52904fddeb9abd1fce545db646cf118b7ebd2886db4eea7001056cc81d4', 474, 3, 'Api access token', '[]', 0, '2021-03-26 02:33:52', '2021-03-26 02:33:52', '2022-03-26 08:03:52'),
('179c0d8a880c00d6e7433282d861af57b7335920c5605afed443e65ab23eeacd57e6830e417462c7', 741, 3, 'Api access token', '[]', 0, '2021-06-15 04:37:48', '2021-06-15 04:37:48', '2022-06-15 10:07:48'),
('17a6b60a05f0818c7c0c775b5a0d5962bc6bb61e2e809aaabe5afd2aa679313c087bf9bbe63b8e9f', 773, 3, 'Api access token', '[]', 0, '2021-07-20 03:24:06', '2021-07-20 03:24:06', '2022-07-20 08:54:06'),
('17dfa53470bd3c741c1f96d04b511d1365ad38eb517bce01e760bc32af439d09d989db6e5e6604d7', 63, 3, 'Api access token', '[]', 1, '2021-08-15 13:01:37', '2021-08-15 13:01:37', '2022-08-15 18:31:37'),
('18048e9f492d24600e5bf81af1c7f0582b7ed661cbdeded1fad51c5fa6d9f545cb86820afc9c85a5', 493, 3, 'Api access token', '[]', 1, '2021-03-30 07:10:25', '2021-03-30 07:10:25', '2022-03-30 12:40:25'),
('1822a35054f4d34536e1ec4a221b409d8cf32091d44758a7e128404576f897e302d9f76f3540e0a1', 782, 3, 'Api access token', '[]', 0, '2021-07-21 05:03:53', '2021-07-21 05:03:53', '2022-07-21 10:33:53'),
('18251ec22d40bd42b743e5db52cc30f42b356cfba99312be8b0e68735f35a279172fe603fc10e4b8', 28, 3, 'Api access token', '[]', 0, '2021-07-28 07:03:24', '2021-07-28 07:03:24', '2022-07-28 12:33:24'),
('184000326d47ad3049bca2f7544450167d6d9a49c646daab9cad5a6639b2681690889cf47a793704', 412, 3, 'Api access token', '[]', 0, '2021-03-18 08:54:51', '2021-03-18 08:54:51', '2022-03-18 14:24:51'),
('185d526f4311c3f05128da312e378c5ca96b775cb07e818fe9d654583480391243172c30af75dbf3', 638, 3, 'Api access token', '[]', 0, '2021-05-12 08:10:36', '2021-05-12 08:10:36', '2022-05-12 13:40:36'),
('186f13efad64d95668b8304fbc4d3c5e9fadd999303d3ff15b15a2787bb39158d5e335e3eb8a0800', 725, 3, 'Api access token', '[]', 0, '2021-06-10 01:45:22', '2021-06-10 01:45:22', '2022-06-10 07:15:22'),
('187c8d4812d38fef242f8177b849316d7f3fac738dd5930c80e2a7cc4b7ff0fda7ed81a191bb0851', 2, 3, 'Api access token', '[]', 1, '2021-08-03 01:34:26', '2021-08-03 01:34:26', '2022-08-03 07:04:26'),
('188c241edfd955cf6a7d48d2c7ac76243b7e4c7627725eff1eb70857668a31c65c982e81aa2e063d', 672, 3, 'Api access token', '[]', 0, '2021-06-02 06:57:16', '2021-06-02 06:57:16', '2022-06-02 12:27:16'),
('18a0fd9735efcdef2d6646d3c743a706f20b2576e8fc7191f6e84846577aa0f06f957d38040aab39', 393, 3, 'Api access token', '[]', 0, '2021-03-22 01:24:26', '2021-03-22 01:24:26', '2022-03-22 06:54:26'),
('18a31388eabf82551cb694b8c6da4a4bce0ca5cb2204fc99c362ad20d9e5853cd86950a2762d0f71', 541, 3, 'Api access token', '[]', 1, '2021-04-05 04:51:38', '2021-04-05 04:51:38', '2022-04-05 10:21:38'),
('18aac73b31505e345225a38fbf9ce83a9c4fb88f9b61dd2c228f5790422105e1f58c050b51bd4def', 552, 3, 'Api access token', '[]', 0, '2021-04-14 07:34:18', '2021-04-14 07:34:18', '2022-04-14 13:04:18'),
('18b09e0db6955f9c7a11f4b588a696b618c994964c7acc5d1ac857baf301eb5a0c924b8378ab01e4', 396, 3, 'Api access token', '[]', 1, '2021-03-18 01:16:33', '2021-03-18 01:16:33', '2022-03-18 06:46:33'),
('190ede13f7e52319c472cdcb1498e80bf8d88e4c1275deb464126855d6c12a2ae537c1d8930f9466', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:15:28', '2021-06-15 01:15:28', '2022-06-15 06:45:28'),
('1920904cbb94f95d62a326b8e8557268721acdcd46ed6f080b5dc10f9dd3c2e02f842c90e06e2197', 30, 3, 'Api access token', '[]', 0, '2021-07-28 07:15:34', '2021-07-28 07:15:34', '2022-07-28 12:45:34'),
('195024354bdb46074f7e46efc5f55984ca22cb9b2336734984172e6ca6df3963318456049fd64326', 529, 3, 'Api access token', '[]', 0, '2021-04-03 07:41:19', '2021-04-03 07:41:19', '2022-04-03 13:11:19'),
('1951bdb2da6253bfafad519dcf3940a9b69a082019c44aaff34b14191fddb870e77985f5a4a78f1f', 7, 3, 'Api access token', '[]', 1, '2021-07-26 00:55:44', '2021-07-26 00:55:44', '2022-07-26 06:25:44'),
('196c928bd5646a4e45d66a9d35b14c6faaf76c6bf091ada36fe7e557f4dc88ccd09f08ce20d0155a', 428, 3, 'Api access token', '[]', 1, '2021-03-22 05:57:39', '2021-03-22 05:57:39', '2022-03-22 11:27:39'),
('1998889556969c24b9e9cf1dd2120953f2f457e4ace726006b5342e9eee1e4a8b0712b28ac59140a', 26, 3, 'Api access token', '[]', 1, '2021-07-28 06:49:21', '2021-07-28 06:49:21', '2022-07-28 12:19:21'),
('19d95aa3c90188e2920918098bf4d7b9faf9ae40bc5ec61b11567c987cdd486b4d3b2d24204029dd', 773, 3, 'Api access token', '[]', 0, '2021-07-21 06:35:20', '2021-07-21 06:35:20', '2022-07-21 12:05:20'),
('19fe663ffc5fc389b8abc81dedff4163d113c45ff55eda28d82f0cff3a080146f30e5a92afa41cdc', 488, 3, 'Api access token', '[]', 0, '2021-03-26 07:41:42', '2021-03-26 07:41:42', '2022-03-26 13:11:42'),
('1a0a4b132a6ad64b649caf42a21b506994fd935022cbbb1ac25e13a2499d98d3bc2d7bca2731c731', 7, 3, 'Api access token', '[]', 0, '2021-07-26 01:19:32', '2021-07-26 01:19:32', '2022-07-26 06:49:32'),
('1a2d9c8f286911fb452a8bd1e58760b7beb3fa04549202f08ea6f1efb082bc99e97f7210473f0f39', 486, 3, 'Api access token', '[]', 0, '2021-04-01 06:05:23', '2021-04-01 06:05:23', '2022-04-01 11:35:23'),
('1a5bf83c4225059010154c8caa0580f285ddf3a3e1859182ac74ba0c3167a1996e2ae00041f7e584', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:54', '2021-05-04 01:11:54', '2022-05-04 06:41:54'),
('1a73c31e3ff0aed9edb442f978e177e8e52db5ce5524f902a6318ee29175d93efea2a73c6b06d3c9', 759, 3, 'Api access token', '[]', 0, '2021-07-21 02:47:03', '2021-07-21 02:47:03', '2022-07-21 08:17:03'),
('1aa13134ee3e4cb2c47d972c53882a564c62cef875e17244eb3b5d964b607256322e797d9bee93e9', 587, 3, 'Api access token', '[]', 0, '2021-04-29 04:01:09', '2021-04-29 04:01:09', '2022-04-29 09:31:09'),
('1acfb7ca541a2bc3e6bb8f8fab4ccf69114073eb1ce9f548432508efd0afe460cb90c49f1e0889e1', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:48:02', '2021-07-20 02:48:02', '2022-07-20 08:18:02'),
('1b101fa1e87dcb7181e125b758d774b1630fc31f57bb98496a04512e01c592288bffde83f67c192a', 534, 3, 'Api access token', '[]', 0, '2021-04-06 02:21:57', '2021-04-06 02:21:57', '2022-04-06 07:51:57'),
('1b2bce29f87cc53c84a4525fc643848016c70b6cc45bfa09f592e47902767ad6f76f7ee13c47bf28', 549, 3, 'Api access token', '[]', 0, '2021-04-12 07:38:47', '2021-04-12 07:38:47', '2022-04-12 13:08:47'),
('1b41ab3e571e14e2cc49609297e9e7855b19e7164544a19956c29fdf69768e5fb9480925027c79d4', 407, 3, 'Api access token', '[]', 0, '2021-03-18 06:48:59', '2021-03-18 06:48:59', '2022-03-18 12:18:59'),
('1b51f73c9388db5df86b719d26b4167fa564ef683d80233537503464eef43f7828dcdfd0a8d4cc90', 484, 3, 'Api access token', '[]', 1, '2021-03-26 05:09:24', '2021-03-26 05:09:24', '2022-03-26 10:39:24'),
('1b5a4d83126ff6d70ca0eb345e9a53b55645a302126d2b33456a394af027734e6667f24ce1a68f59', 13, 3, 'Api access token', '[]', 0, '2021-07-28 08:20:32', '2021-07-28 08:20:32', '2022-07-28 13:50:32'),
('1b86691fe4d88a6592a0e8f840c45edbcb34e32b5b7e213fd8bca4edde61c2be6b63b07485ecf7af', 618, 3, 'Api access token', '[]', 0, '2021-05-04 05:10:36', '2021-05-04 05:10:36', '2022-05-04 10:40:36'),
('1ba0e77a42220202f453934e99e98db51a8979c829d309a7e13cf2e1f1641451b78313a76addffe6', 717, 3, 'Api access token', '[]', 0, '2021-06-08 09:49:24', '2021-06-08 09:49:24', '2022-06-08 15:19:24'),
('1ba17df8d45e71e62ed288f620bcbb639ba8066112d9393b2a0da89743520ba3be1a8e5b98eef53d', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:36:32', '2021-05-11 01:36:32', '2022-05-11 07:06:32'),
('1bcd8b9cf99b965a8ad6611f3b32cc80b6403f101e2c358b77f1b410b7cbad8cf14f87d8856aff5d', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:56:01', '2021-05-13 06:56:01', '2022-05-13 12:26:01'),
('1bf10ee170df2479173ef04aaa7e4e68f824d0c28c5a6496879200eeb393c28c06d919bb8865da5d', 4, 3, 'Api access token', '[]', 1, '2021-07-28 03:01:35', '2021-07-28 03:01:35', '2022-07-28 08:31:35'),
('1c0772ecf6c893f31dc3df7dea2f48566f2a728daa32f0c7f63aa45fc8176e046f8ca34a682e6488', 765, 3, 'Api access token', '[]', 0, '2021-07-12 04:48:29', '2021-07-12 04:48:29', '2022-07-12 10:18:29'),
('1c1e90d9d75ad463d21629cb7d0a252f218af8a816ad9a794b36c62ceb785a8d78ab37caaf3ba8d7', 2, 3, 'Api access token', '[]', 1, '2021-07-30 05:35:29', '2021-07-30 05:35:29', '2022-07-30 11:05:29'),
('1c2be3d3097e7da130a5e3791f1502c65b0fec1cef59fed1aa56215f19e5514f7783ceb1898fe23d', 52, 3, 'Api access token', '[]', 0, '2021-08-06 01:16:00', '2021-08-06 01:16:00', '2022-08-06 06:46:00'),
('1c4829e1c128a6b07bf6cfe94778df8b0910fed6884bb9b5534bde135a253e9e9abe5235fee6b79e', 711, 3, 'Api access token', '[]', 0, '2021-06-08 09:31:06', '2021-06-08 09:31:06', '2022-06-08 15:01:06'),
('1c8847eee55e3d248bdffa123b1774dd05e223938598b9aa7d3ab2a34d88981ff8981ee68043f7d0', 690, 3, 'Api access token', '[]', 0, '2021-06-02 23:45:20', '2021-06-02 23:45:20', '2022-06-03 05:15:20'),
('1d187341a840d03c611ec871a3672f44ad57bde5dedce0e536da53090eb9db4a8dceefb4293fd1cd', 1, 3, 'Api access token', '[]', 1, '2021-07-27 04:20:42', '2021-07-27 04:20:42', '2022-07-27 09:50:42'),
('1d1984aa1c67d2ea9fc60ea1798c466d614202ec130ef59682e0e5f528b47c03c6ae7d1f8177b29e', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:21:14', '2021-07-16 06:21:14', '2022-07-16 11:51:14'),
('1d46556fb0c67cef9327c60812f6f028313d6a06348e9d830181d2e68b8fa2f7f009c5808cfcfe95', 568, 3, 'Api access token', '[]', 0, '2021-04-15 23:38:17', '2021-04-15 23:38:17', '2022-04-16 05:08:17'),
('1d4c78ded8d1cd3fa814dd0cc311fecfec537655c0ea1493a0e88a1a70fec4ae649bacc49625338e', 619, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:35', '2021-05-02 08:31:35', '2022-05-02 14:01:35'),
('1d830cb68e1b082105cffd99fd4e87ec851d93fdeb140b6b6d75e7cbedd9acf4f7bbdff726e7017e', 13, 3, 'Api access token', '[]', 0, '2021-07-29 06:13:46', '2021-07-29 06:13:46', '2022-07-29 11:43:46'),
('1d89375a39d867b2b3d66be46a2dc3058f2e33ea480632656ef4bd1a3dbd3f43beedd27fadbc880b', 457, 3, 'Api access token', '[]', 0, '2021-03-24 02:49:08', '2021-03-24 02:49:08', '2022-03-24 08:19:08'),
('1d9fbc117a360202c544b69564c58499301ba196ddf5278f6f7416fd8a275cb1cb30f75592ca277d', 661, 3, 'Api access token', '[]', 0, '2021-05-26 00:40:00', '2021-05-26 00:40:00', '2022-05-26 06:10:00'),
('1da6d7af53793e2f77af700f22663053fc8ead8da6c8334548a64462e7505c0b387f5ea71d90a353', 469, 3, 'Api access token', '[]', 0, '2021-03-25 04:48:06', '2021-03-25 04:48:06', '2022-03-25 10:18:06'),
('1ded84ba8572e6f79c1621aaa2c4dab936870916926b8972893987289ebf3bd73cc0af3fcb9d4c38', 618, 3, 'Api access token', '[]', 0, '2021-05-08 00:34:58', '2021-05-08 00:34:58', '2022-05-08 06:04:58'),
('1e3037d77bdac6290c0a4d27915caa878b287852c06f9cfb345594d2f278abbdf3083d1df2f242b6', 52, 3, 'Api access token', '[]', 0, '2021-08-06 02:44:39', '2021-08-06 02:44:39', '2022-08-06 08:14:39'),
('1e387dd975d3d66deca19352e77e25fd629a38718275380e2eac5160f14dec6a8a6f172e7c3780d9', 518, 3, 'Api access token', '[]', 0, '2021-04-02 07:16:15', '2021-04-02 07:16:15', '2022-04-02 12:46:15'),
('1e40bcd20612b621ff311dc8cfe8cb6872adea342aa89938f0a2ca178456740d2a034e554f59709b', 421, 3, 'Api access token', '[]', 0, '2021-03-22 05:23:41', '2021-03-22 05:23:41', '2022-03-22 10:53:41'),
('1e683696ba5894b4266582024acb3efbd16ba06a0fc36d2ffdca123764eaab7da225bed9378d6913', 759, 3, 'Api access token', '[]', 1, '2021-07-20 02:40:38', '2021-07-20 02:40:38', '2022-07-20 08:10:38'),
('1e8049a95042a8cf0faa55b81f1bec807f2e3b50aab96ae27932149582df8173099191eb1f840289', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:27:13', '2021-05-11 01:27:13', '2022-05-11 06:57:13'),
('1ef3604d47b8510ccf3c9bc38484998cfb3f6e9c4ce04c5714c53e54cd27d0f8b98fb57e44505203', 702, 3, 'Api access token', '[]', 1, '2021-06-15 07:42:01', '2021-06-15 07:42:01', '2022-06-15 13:12:01'),
('1f1bc1b3057ea77fa6c3c9aac60a0bc8b20a65f98964ec0128cc3e75cd0cd6ffd1b9d42a204fa3cc', 623, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:31', '2021-05-07 07:04:31', '2022-05-07 12:34:31'),
('1f94c1b1ede59443afc5760415482f81e791ddd4fd36322c8a38c138bf3b212bcc5adefdc0e4b1e6', 405, 3, 'Api access token', '[]', 0, '2021-03-23 00:16:07', '2021-03-23 00:16:07', '2022-03-23 05:46:07'),
('1fc4ae2617270fabd3f825f18e00eda51fbe1ed7f4fab369296ede6fa32f5045d1d2fd6d6a219730', 414, 3, 'Api access token', '[]', 1, '2021-03-18 23:45:16', '2021-03-18 23:45:16', '2022-03-19 05:15:16'),
('200ec62bceba44f8fe9419e95b6d2c101a4598618445e6684b869c571db834b1d9da488cac302947', 443, 3, 'Api access token', '[]', 0, '2021-03-23 02:52:38', '2021-03-23 02:52:38', '2022-03-23 08:22:38'),
('20404a471c8b3902810edb433869db9a47f5266bb6be169fa16dfe7a854067b87d857b07e6420a0e', 486, 3, 'Api access token', '[]', 1, '2021-03-31 02:49:06', '2021-03-31 02:49:06', '2022-03-31 08:19:06'),
('204bbafe26ef5e08ad9fb8bf62bdb193d06583b96a8a751d5aac95e9420b5b0cc0ce451482076276', 35, 3, 'Api access token', '[]', 0, '2021-07-28 07:30:04', '2021-07-28 07:30:04', '2022-07-28 13:00:04'),
('208f5bc7e9fc12166a076823a02eeda9fbb20f07ac228069a331954416f519af3ff5ebbb2ace947e', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:50:28', '2021-05-08 05:50:28', '2022-05-08 11:20:28'),
('2090916baa5ab421b43179f126601e976fa3f83e19640ce5c557992e79cbd467988b247e38d65f6a', 7, 3, 'Api access token', '[]', 0, '2021-08-02 00:13:13', '2021-08-02 00:13:13', '2022-08-02 05:43:13'),
('20a61243041c7901eb45dbfeda85a15205cb690966862aa9e3293bda99ccba1bb7d7efc462386cb7', 495, 3, 'Api access token', '[]', 0, '2021-03-30 00:50:43', '2021-03-30 00:50:43', '2022-03-30 06:20:43'),
('20a7945c5b0b98fe676739bf9c218d6d4ded3d8df5ec0b420704482a6fa84290fea6ddbfbe20eed8', 401, 3, 'Api access token', '[]', 0, '2021-03-22 05:51:02', '2021-03-22 05:51:02', '2022-03-22 11:21:02'),
('20bb478aaa4042e7e8a0f6b55f8747b0883ba88ab4c1b775baac50aa19bdec75dc2f98baedd0a575', 720, 3, 'Api access token', '[]', 0, '2021-06-10 01:36:45', '2021-06-10 01:36:45', '2022-06-10 07:06:45'),
('20d15b9298282b1372b959fc18f0d53abd1d1cc00d2c0f8e46e16294b9e71a685ecc85f235d29b83', 611, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:41', '2021-05-04 00:46:41', '2022-05-04 06:16:41');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('21107c227921d853ffd6e8c91d7ef401eefdb0562cb6e632bdc3d3e2deb70662a023e0050e763a0a', 52, 3, 'Api access token', '[]', 0, '2021-08-06 02:36:26', '2021-08-06 02:36:26', '2022-08-06 08:06:26'),
('21289062ca96ca3ee6760aae0727fdb636c2a5e53cc46cbff1f1de1144e43df94d30219fa7d208e9', 552, 3, 'Api access token', '[]', 1, '2021-04-14 00:16:42', '2021-04-14 00:16:42', '2022-04-14 05:46:42'),
('217150d3e63bc78de75a693ad2bf7186f172912065d4f766ada9b28a9b88e081df232514712fbe81', 665, 3, 'Api access token', '[]', 0, '2021-05-30 23:22:29', '2021-05-30 23:22:29', '2022-05-31 04:52:29'),
('21779d45240e120b654ccd76e4bbd585507b8dbe53474d8c0c97aa368193aeb29569df14f0f3d644', 564, 3, 'Api access token', '[]', 1, '2021-04-15 06:58:04', '2021-04-15 06:58:04', '2022-04-15 12:28:04'),
('21a641bbe476b54afbb6dc5b35ecffff65716e8f07ed8cb2519033d7636708bcc40ff14b01062549', 1, 3, 'Api access token', '[]', 1, '2021-08-13 01:57:34', '2021-08-13 01:57:34', '2022-08-13 07:27:34'),
('21a97f56b9110210572c9fe5e7952e47b82f8d3f5189bec7bad0d8f6c07bc55b39a25e47594afed4', 773, 3, 'Api access token', '[]', 0, '2021-07-20 04:41:53', '2021-07-20 04:41:53', '2022-07-20 10:11:53'),
('21ad04f09f759a7a0dfb186d617083a5d9522bbe99e16bf66091c11953a1efb63fd98e767302984b', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:12:06', '2021-07-15 07:12:06', '2022-07-15 12:42:06'),
('21feaffeced00fa2b59eb286aee01273b5b14d1cb65e4c1342c319f9b28c7f14cdd9da4d6a642ccf', 563, 3, 'Api access token', '[]', 1, '2021-04-15 05:53:44', '2021-04-15 05:53:44', '2022-04-15 11:23:44'),
('21ffd00a2465354c8b87722f6ad3d8d3e129d392c2266a8538d5921c951e0bdc38ca0e74e6799205', 485, 3, 'Api access token', '[]', 1, '2021-03-26 05:35:17', '2021-03-26 05:35:17', '2022-03-26 11:05:17'),
('22157d8bc410f8a0d95476fb876b009df2928504cd701bc015217e06ee222dab4aee195acf57ecd2', 489, 3, 'Api access token', '[]', 1, '2021-03-26 06:24:24', '2021-03-26 06:24:24', '2022-03-26 11:54:24'),
('221c77ff5ccb5062a88253cb8755c2f0f2a9a865aa0b37e34a6c685c6eea4ed9ab783c4a20be14d4', 679, 3, 'Api access token', '[]', 0, '2021-06-02 07:47:38', '2021-06-02 07:47:38', '2022-06-02 13:17:38'),
('22434046b672767247f2c1a9c4a5439e00a6f2aab573a9f1d2d5e4936fbde8e4a91f86ec4cbd4afb', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:50:19', '2021-05-04 06:50:19', '2022-05-04 12:20:19'),
('22605aea99f85f242b3603574dbcef06c7de3ad04dc36736a41a9943e9fccf5ea7e982637911b910', 2, 3, 'Api access token', '[]', 0, '2021-07-30 00:22:52', '2021-07-30 00:22:52', '2022-07-30 05:52:52'),
('226adf9cb0818abad73e9a46ddd8598e47e990e052b589840bffebe7ae9d1856cf58d83c49a647d0', 37, 3, 'Api access token', '[]', 1, '2021-07-28 07:35:49', '2021-07-28 07:35:49', '2022-07-28 13:05:49'),
('2288063f48e0e07f1bd0f46a55377475d82f71e9646cdd3e2c573fb5b3dd9c07bef9301a87784826', 702, 3, 'Api access token', '[]', 1, '2021-07-21 01:06:57', '2021-07-21 01:06:57', '2022-07-21 06:36:57'),
('228ce758de961a7ad43472a71d2138090f9a4f76b3288ffd020b46cdc4613e5e754270ad2a946eed', 686, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('2298310022b1aeaf53142f7bd517d82dd531c7fda5164988da71da59a8bcab93a61f849b7ed454cf', 53, 3, 'Api access token', '[]', 0, '2021-08-05 02:54:24', '2021-08-05 02:54:24', '2022-08-05 08:24:24'),
('229ce0b505c0b7b5038a5e321c7ff233842965f01fa2eb12ac338e604b639460c73ad7255d259093', 486, 3, 'Api access token', '[]', 0, '2021-04-02 00:54:40', '2021-04-02 00:54:40', '2022-04-02 06:24:40'),
('22a0a224410a43257efae5575d2a3fc250183c200af38408f32a619a20241d6299963f62739badcf', 555, 3, 'Api access token', '[]', 0, '2021-04-14 00:49:49', '2021-04-14 00:49:49', '2022-04-14 06:19:49'),
('22dcb89fc333d572b7786cce85a0511e3960cb4c4455f9f3401db56dd563b3a93639a4c00fbdc292', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:13:41', '2021-05-11 06:13:41', '2022-05-11 11:43:41'),
('22e000a88554ef6d21376aca5505dcaaa3e9b94f2b327a0958a9daaa1fe0a02d43f6d0ac5626079a', 403, 3, 'Api access token', '[]', 1, '2021-03-17 23:47:16', '2021-03-17 23:47:16', '2022-03-18 05:17:16'),
('22f645fa2b79a829d61185e2ed56c2fc8ade369a869e7e47eb8f43374fe3024c7af7056272e4e821', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:54:50', '2021-05-12 04:54:50', '2022-05-12 10:24:50'),
('2304cbd60a1d2a925dfe41021eb0b8e8a2ae179c1656d26f1b6f3e0af11628562f80025821682b9e', 595, 3, 'Api access token', '[]', 0, '2021-05-02 07:38:35', '2021-05-02 07:38:35', '2022-05-02 13:08:35'),
('230d98c5e355ac8f4cfe2ed20b6298930a89eb13bd23818fadbcddd02b4dc188db25fc46e80eb0a9', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:27:51', '2021-05-08 05:27:51', '2022-05-08 10:57:51'),
('231a1078886caaf2cc5bc4062824ae3c39bfb0160621e8219067c334208f245898cdc6ae84f356fc', 434, 3, 'Api access token', '[]', 0, '2021-03-23 23:29:21', '2021-03-23 23:29:21', '2022-03-24 04:59:21'),
('2343811cb74df39a6bf2537f853e545e5e3746fbc9b55b2cc418d471f5289df7dde12dea086f5b96', 455, 3, 'Api access token', '[]', 0, '2021-03-23 22:46:02', '2021-03-23 22:46:02', '2022-03-24 04:16:02'),
('2344e4d72ac383c0c09f36e3f3641f1d797378ac247f526c3c80616b6e97e6020a452304e738cefa', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:40', '2021-05-04 01:27:40', '2022-05-04 06:57:40'),
('2390f191a9e9b7f843abc9340bbea9adfd4565210f57e12482bae1ad5606a204f7839c14fd60c63f', 542, 3, 'Api access token', '[]', 1, '2021-04-05 06:18:28', '2021-04-05 06:18:28', '2022-04-05 11:48:28'),
('239cb5793884ef61162d7db78b2d44c278542944d026d0070febb528a15ea91c595d8db9e0d24312', 17, 3, 'Api access token', '[]', 1, '2021-07-28 07:50:26', '2021-07-28 07:50:26', '2022-07-28 13:20:26'),
('23d7cd682fae9754263f413d7a234238f8f55a0f836d8b63232e7f74680eb727b329ba06ce9e6e3b', 712, 3, 'Api access token', '[]', 0, '2021-06-08 09:31:07', '2021-06-08 09:31:07', '2022-06-08 15:01:07'),
('243ed0464247338fe84ba487ec90647c6ee0fe6a895483ab8b9971c9275f93868ccdab49325e384f', 453, 3, 'Api access token', '[]', 1, '2021-03-23 07:01:47', '2021-03-23 07:01:47', '2022-03-23 12:31:47'),
('2448377a0d86a604652ddde9a8e4123c1062e5e6c5b6f7ceb0c07ef14e24906f7ec86320434af309', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:43:03', '2021-05-08 05:43:03', '2022-05-08 11:13:03'),
('245f2e5fa81d199fda0d54aa5c52e7fd2fcb1a7c58e3f8871483331fe4683e7182975253d16a7e5d', 622, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:41', '2021-05-04 00:40:41', '2022-05-04 06:10:41'),
('2469f7f01399d7f8a6e8d6d4b60b3c40f769a192e67053e4e9d152e3b0a8e7905acd32d298744d1b', 488, 3, 'Api access token', '[]', 1, '2021-03-26 06:22:35', '2021-03-26 06:22:35', '2022-03-26 11:52:35'),
('248ad877b04e6d1aee1d8f878ab2c815d45568caba44c6648b9e4094c8dd274aa4ebe1b2e0c24983', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:36', '2021-05-02 08:31:36', '2022-05-02 14:01:36'),
('24ce7759e6843dcae3df360cfdca1922719aab6aa9c5f360ccd8d9ea59484bc43cf135d2d363ea85', 1, 3, 'Api access token', '[]', 0, '2021-08-02 00:17:18', '2021-08-02 00:17:18', '2022-08-02 05:47:18'),
('24eb2fe9cc0761f4b9f8a1e916ef150c82b1126ce5d69f0c6126ab63b60cf0451c2c132e9e1fa40e', 618, 3, 'Api access token', '[]', 0, '2021-05-08 01:26:00', '2021-05-08 01:26:00', '2022-05-08 06:56:00'),
('2503385608987f7b014c4f4d9ead4655ed5fdacbbd26108a597e0898f92beec2718e34fd648bd0ca', 531, 3, 'Api access token', '[]', 0, '2021-04-05 00:47:41', '2021-04-05 00:47:41', '2022-04-05 06:17:41'),
('25085fa2a1d38d13b3bee67340c57f345caaeca6dcfdf9f50cde244fdd1ef70902b3c2b21d6da014', 667, 3, 'Api access token', '[]', 0, '2021-06-03 00:19:15', '2021-06-03 00:19:15', '2022-06-03 05:49:15'),
('2524e56f0f16c864a91dc2b7970cf511061e3a1fe5de6249d3b617917b964570039c2d88b8eacc95', 626, 3, 'Api access token', '[]', 0, '2021-05-13 01:34:24', '2021-05-13 01:34:24', '2022-05-13 07:04:24'),
('25299005a8fef8da2f93a638fff881dd8a8b1dd42f3fb71c8e60a0f4060bce025a29dd7023315253', 432, 3, 'Api access token', '[]', 0, '2021-03-23 22:53:30', '2021-03-23 22:53:30', '2022-03-24 04:23:30'),
('253c2784fbe991aa82b2909dc50941586457de4ac032b7169b35898cdc34be69edb8851f90a68288', 770, 3, 'Api access token', '[]', 0, '2021-07-15 23:30:33', '2021-07-15 23:30:33', '2022-07-16 05:00:33'),
('2541faba833edae018d90dead205ff33c29ba60221cd79253e1fdf4790ff659664c02c82d3ac94b7', 415, 3, 'Api access token', '[]', 0, '2021-03-19 00:01:19', '2021-03-19 00:01:19', '2022-03-19 05:31:19'),
('254459984a6cf70ba51df26bc35c39a4c9a345202b83d1fc8ac2ac48e0bcfef4dd79af1fb81defb5', 770, 3, 'Api access token', '[]', 0, '2021-07-15 06:46:59', '2021-07-15 06:46:59', '2022-07-15 12:16:59'),
('2565e79cbd9f831b50b9948740633e13acaea52337c159fd4191822e333eb7325ac29d7ee6daa9f9', 434, 3, 'Api access token', '[]', 1, '2021-03-26 01:57:40', '2021-03-26 01:57:40', '2022-03-26 07:27:40'),
('25be5832457d92cbbfda480d9a609c365f9d966996fc6712eab3c71f19af1c4ef54fb8f26ce88d68', 759, 3, 'Api access token', '[]', 0, '2021-07-19 08:34:25', '2021-07-19 08:34:25', '2022-07-19 14:04:25'),
('25fae4e01c6f249500ddfa8eeb532b7cef80ffa938904597139eaa5e47a1239bf67e09e866571081', 4, 3, 'Api access token', '[]', 0, '2021-07-23 00:24:36', '2021-07-23 00:24:36', '2022-07-23 05:54:36'),
('2615aa3fb9c30a4d2b57d1248b38fff4feecc0ccaec6686daf40c840dfef953a8715f9c7ed84d4e7', 564, 3, 'Api access token', '[]', 0, '2021-04-15 06:58:04', '2021-04-15 06:58:04', '2022-04-15 12:28:04'),
('261a148af0bebd6a2448759c0dc0dbae8a08e30d0b28d3f9fb06e8ac7fb1af858232bed31de086fb', 688, 3, 'Api access token', '[]', 0, '2021-06-02 22:49:46', '2021-06-02 22:49:46', '2022-06-03 04:19:46'),
('261ece2450b6c4bba292b3f9d4d9b2c54d745694944b19d9ec52237ebeb581ef27f093fbaf89f9a8', 605, 3, 'Api access token', '[]', 0, '2021-05-02 07:52:33', '2021-05-02 07:52:33', '2022-05-02 13:22:33'),
('263e87c58f335276cb132336318e1a709dd103a42d91ec2e79f9fa8d1d3602cf1615792b606c48ae', 440, 3, 'Api access token', '[]', 1, '2021-03-23 02:23:17', '2021-03-23 02:23:17', '2022-03-23 07:53:17'),
('26490b4d284aba81e347aa642f4d19e2e0f1ea7d576bf23e3c873889497eee564974aae9bc7c280d', 773, 3, 'Api access token', '[]', 0, '2021-07-21 05:37:30', '2021-07-21 05:37:30', '2022-07-21 11:07:30'),
('264d6a6ac6c704f1ff9bb52a82442de645ddfe72348ed8b91818c56d0dc3bd8506a576914cc945f8', 612, 3, 'Api access token', '[]', 0, '2021-07-16 01:50:36', '2021-07-16 01:50:36', '2022-07-16 07:20:36'),
('266470fffcd5dd24b30fe80a42312fe82535ee778a8ff3da434278c1b922cbcceb35f3030d35d148', 500, 3, 'Api access token', '[]', 0, '2021-03-31 02:49:30', '2021-03-31 02:49:30', '2022-03-31 08:19:30'),
('267eab14293954ba3a936cc0e446e285f82f8d15b78b7860ca8a6500c8dfd87428f0ccb6e3faa8e3', 436, 3, 'Api access token', '[]', 0, '2021-03-23 01:12:03', '2021-03-23 01:12:03', '2022-03-23 06:42:03'),
('2697a27e094bfc4b08a59180b865b3dfb034eadcd69d9dad2dac94b584fa3fac75eca0d18dcdedb3', 735, 3, 'Api access token', '[]', 0, '2021-07-20 07:52:46', '2021-07-20 07:52:46', '2022-07-20 13:22:46'),
('26a544efdaf8d655fde7e563f4f4c1098920f526d4c26d51e452d85e587e65eb9b9eebf8ae4ceffb', 17, 3, 'Api access token', '[]', 1, '2021-07-29 06:54:34', '2021-07-29 06:54:34', '2022-07-29 12:24:34'),
('26bc3002eb037d621e18926c7b76122f00d9c4b6440f7ca70832b377aa6b6f2c569a3f6648b1251f', 407, 3, 'Api access token', '[]', 0, '2021-03-18 06:42:28', '2021-03-18 06:42:28', '2022-03-18 12:12:28'),
('26ee7a8d7b3f3ecc6935e1dc05b8d697e5fd293aa856a0fb4f13fa0567458cc49f7d28b2875e6a8e', 533, 3, 'Api access token', '[]', 0, '2021-06-08 08:01:22', '2021-06-08 08:01:22', '2022-06-08 13:31:22'),
('2717f666f87fd2a405f8ded24f38803a64513b6ffd39b576faf6c43b157419f1c5fc3f7c3acf8a17', 584, 3, 'Api access token', '[]', 0, '2021-04-21 06:49:18', '2021-04-21 06:49:18', '2022-04-21 12:19:18'),
('272c0e8cd3072f187c574f7dcbff80c557bd1c8d6a76dde94d82bd5c267cea48970e6cbc6158c329', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:47:42', '2021-05-27 04:47:42', '2022-05-27 10:17:42'),
('2744a0856226b1b2177d695a053b59314481e721a21ebf71936beee89ba304c73b5c2efad6368e30', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:52:09', '2021-05-04 06:52:09', '2022-05-04 12:22:09'),
('2746fb777b52500c5badcc747b160fb99cad9a4d850ec63840100912679f024cd5eaf46c9ef82562', 398, 3, 'Api access token', '[]', 0, '2021-03-18 00:01:44', '2021-03-18 00:01:44', '2022-03-18 05:31:44'),
('274d903f0495759d98ea686a4e7429a319e36accf2f9011b356c1d84c566c5e398c854f0172101fe', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:08:16', '2021-06-15 01:08:16', '2022-06-15 06:38:16'),
('27560578f888367e7fcb23ac1ff4335e19e9078d462880ee7970be5aad1bb944b16ad34cdc370289', 587, 3, 'Api access token', '[]', 0, '2021-04-29 01:48:15', '2021-04-29 01:48:15', '2022-04-29 07:18:15'),
('275df11c6f630560525efb77b64666a595307da7756e972ea3abc6bbb4b68e7ec71f884774580273', 785, 3, 'Api access token', '[]', 0, '2021-07-21 06:52:56', '2021-07-21 06:52:56', '2022-07-21 12:22:56'),
('276b39adfd9d17d03e6939600975cf0f9aa896db8587fadc622f067a535958c3016c552af2417bb8', 1, 3, 'Api access token', '[]', 0, '2021-08-03 01:59:53', '2021-08-03 01:59:53', '2022-08-03 07:29:53'),
('279c7f56acb0edf8b368654a66635e2ba3a67c221108cd00fc798f8466bd5896cfdd4d64cb1a2fe4', 620, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:32', '2021-05-07 07:04:32', '2022-05-07 12:34:32'),
('27cac0b751fdd142ce824b81e1c2309747eeb4305762e9409d40e0ae66f5eb9e7bfe6412c8986c0e', 718, 3, 'Api access token', '[]', 0, '2021-06-08 09:49:26', '2021-06-08 09:49:26', '2022-06-08 15:19:26'),
('27d4da1a5dc1b82758dc23b8f3790610a10d1afc344dcfaa5f7c6f2a4e673d8d2c52d904d7a9b60d', 639, 3, 'Api access token', '[]', 1, '2021-05-12 06:57:33', '2021-05-12 06:57:33', '2022-05-12 12:27:33'),
('28302d84bdf1623007b0cf5e164d22a09772434462a5ee22738d7500919b3b9fae1e45b535719984', 519, 3, 'Api access token', '[]', 1, '2021-04-02 07:21:36', '2021-04-02 07:21:36', '2022-04-02 12:51:36'),
('284090db19167082e180968bdf5d3f66fbe4acf0bf806acc600adff3a521dc7d953b319667c840aa', 773, 3, 'Api access token', '[]', 0, '2021-07-21 02:20:50', '2021-07-21 02:20:50', '2022-07-21 07:50:50'),
('284d984a2fd04cc420b5763ca251585ac44d601fdf0de5180ab5d7a38da90001cc556a6c8a0b8d13', 17, 3, 'Api access token', '[]', 0, '2021-07-29 23:19:48', '2021-07-29 23:19:48', '2022-07-30 04:49:48'),
('2863c803fde6c183d2b1f1eede0f920e5f9462837559d1e93ecc151becabb804ca63b2738f1d8433', 1, 3, 'Api access token', '[]', 1, '2021-07-27 04:14:27', '2021-07-27 04:14:27', '2022-07-27 09:44:27'),
('28768da044ae63bccd0f6f912879f2a154bd2ca93a41a150b5d19719c5d62a42abf69f66dc7c21d2', 618, 3, 'Api access token', '[]', 0, '2021-05-08 04:28:35', '2021-05-08 04:28:35', '2022-05-08 09:58:35'),
('289a60f936a5e9ed7aefd891df19ba630bc63daa1e97aba300e5acd561e42655ce6882bde2b3f3b8', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:15:45', '2021-05-12 03:15:45', '2022-05-12 08:45:45'),
('28d7a233cd92a4ffa629ec5fbd6e27617d99fbd3ef41ae0fe2d4f754424dd842971a7760b90e5d81', 451, 3, 'Api access token', '[]', 0, '2021-03-23 06:02:38', '2021-03-23 06:02:38', '2022-03-23 11:32:38'),
('29a2222e6ca19d8f187bbd592adf508ed8f22a616e05cf34b83cf1df8043857ff8ec39669a8a4c53', 738, 3, 'Api access token', '[]', 1, '2021-06-15 03:04:32', '2021-06-15 03:04:32', '2022-06-15 08:34:32'),
('29dcb699d0d5a56b7876c2c945e5d5f47ff122c63e11eea3395fbf9caa73a288b966309a3f90d68d', 5, 3, 'Api access token', '[]', 0, '2021-07-30 01:40:33', '2021-07-30 01:40:33', '2022-07-30 07:10:33'),
('2a49f87099c943086a0706b74f20829b2a4d02de8be32c7363869d2d9db6bf3cad1ff3c25c6996a5', 631, 3, 'Api access token', '[]', 0, '2021-05-09 02:06:54', '2021-05-09 02:06:54', '2022-05-09 07:36:54'),
('2a4d0258ed56d1c8d8811c3fc0603ea47581f34145ec2f05726944f22ef3011be25541ad40be6014', 487, 3, 'Api access token', '[]', 1, '2021-03-30 04:59:08', '2021-03-30 04:59:08', '2022-03-30 10:29:08'),
('2ab0442800bed6c3dad2663f1cb890fc4eb8d04934a7c19e5c49698bd67eda98716055cc0ffa958d', 716, 3, 'Api access token', '[]', 0, '2021-06-08 09:44:14', '2021-06-08 09:44:14', '2022-06-08 15:14:14'),
('2abae8796a975737b706ab8aeb2169126d973d4758fa098441a28797c20b5e0a00455765b815df79', 17, 3, 'Api access token', '[]', 1, '2021-07-28 07:52:09', '2021-07-28 07:52:09', '2022-07-28 13:22:09'),
('2ae19a07acd575d929c85dc51e2f8005f8625d4d7cfb1c91fe0d0046138bc3f97eade1b2cc00d5b8', 553, 3, 'Api access token', '[]', 1, '2021-04-14 00:17:08', '2021-04-14 00:17:08', '2022-04-14 05:47:08'),
('2afbcb2cd4ee9dcd372afb8acc6aba8c40e722cf175bd18348b341fdc3e94dc2d446b3685b07dbdb', 700, 3, 'Api access token', '[]', 0, '2021-06-03 01:01:21', '2021-06-03 01:01:21', '2022-06-03 06:31:21'),
('2b09fd114b9a0070fb5c207cf02e8bbcedc0db79263cbc53a45c2b988521711ffd2b987ec3563290', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:32:28', '2021-07-17 00:32:28', '2022-07-17 06:02:28'),
('2b4f5dcaa0d4ce5c60aa931b368c0587ae8af986218693ccf4f42e06e160969ff6eccd47cbaf098d', 555, 3, 'Api access token', '[]', 0, '2021-04-15 06:41:30', '2021-04-15 06:41:30', '2022-04-15 12:11:30'),
('2b68cd7649b0f279d1b5f5da6721f5c28b1607491fde37aca9887ef5971219b47e482d0c331b8bac', 60, 3, 'Api access token', '[]', 0, '2021-08-17 06:12:53', '2021-08-17 06:12:53', '2022-08-17 11:42:53'),
('2b7b019ac181fb900fc7d2918b0caddca1c14c37e2054d1e25196d41970b57c5ec8c20e3c6166d72', 753, 3, 'Api access token', '[]', 1, '2021-06-30 08:52:13', '2021-06-30 08:52:13', '2022-06-30 14:22:13'),
('2bbb7b244381e3b41ee3e7c1f55d37345768b97c3f16a655e41981ce197dddd26e36070c08b1d631', 393, 3, 'Api access token', '[]', 0, '2021-03-18 00:38:28', '2021-03-18 00:38:28', '2022-03-18 06:08:28'),
('2bf9586ec4233c5b1cf1ea2cf068cd107b0a9159165120bb7ee266fc37a5a193c64baf0d625e5175', 547, 3, 'Api access token', '[]', 1, '2021-04-29 02:21:19', '2021-04-29 02:21:19', '2022-04-29 07:51:19'),
('2c164b0770e8603e984681b57c4049e03ff8bb2c691c75e3905c6520a1d3641f46c816ceb807ad01', 486, 3, 'Api access token', '[]', 0, '2021-03-31 04:18:56', '2021-03-31 04:18:56', '2022-03-31 09:48:56'),
('2c3c46e173374a0260624d915be1c88537fd8619e48812cf681b09e7f3f55454e79ed26c5d73ac4f', 458, 3, 'Api access token', '[]', 1, '2021-03-24 04:15:19', '2021-03-24 04:15:19', '2022-03-24 09:45:19'),
('2c506abaf93e6e34e479ea157002299944a5dce0182f0ac03254655c43b1f6d687f6ca17d3cb6712', 5, 3, 'Api access token', '[]', 0, '2021-08-07 04:30:23', '2021-08-07 04:30:23', '2022-08-07 10:00:23'),
('2c5b93e8052986e3079fb9170e7ee7efa35d08023efdf572ac3190e220ecb114caad74a2a89c601f', 52, 3, 'Api access token', '[]', 0, '2021-08-04 05:50:08', '2021-08-04 05:50:08', '2022-08-04 11:20:08'),
('2c5f1e90632991c89fb54bb2d22c82cef1daa20b7f111c79f258d67cba69d0a3e0941e33179498fc', 393, 1, 'Api access token', '[]', 1, '2021-03-17 06:22:04', '2021-03-17 06:22:04', '2022-03-17 11:52:04'),
('2c67dbd1bfb7281ceb863bcfa30a6e0a82479fb39d192b7e704fa8767e0fd6443e15ae0c144ee398', 2, 3, 'Api access token', '[]', 1, '2021-08-03 00:32:36', '2021-08-03 00:32:36', '2022-08-03 06:02:36'),
('2c6f2858e7ef666466d2299112bb1eac2d44bf3a139ab5fb0480bd793986e5921cbf2a3e19c305ab', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:02:59', '2021-07-14 02:02:59', '2022-07-14 07:32:59'),
('2c83775f672ec848e790ea87c965750a99b77b34c2134c25fbb3d6dddba87aeef480d1a2d6ef66cd', 705, 3, 'Api access token', '[]', 0, '2021-06-04 05:10:48', '2021-06-04 05:10:48', '2022-06-04 10:40:48'),
('2ca448b847b3eaf3694ebd740f6b8c5a7c76298b092fc02d66aa13e1be3d1c817412e9aaba3d45e6', 532, 3, 'Api access token', '[]', 0, '2021-04-05 00:03:39', '2021-04-05 00:03:39', '2022-04-05 05:33:39'),
('2cbc89068dd139abad4435367a1fc6237983734b5b2b50c0092e7bd4e448577e02efd03db57feff0', 413, 3, 'Api access token', '[]', 1, '2021-03-18 23:42:30', '2021-03-18 23:42:30', '2022-03-19 05:12:30'),
('2d00b911ec4482d338cc2a3d147b333a379341e8752768c2403b08ee288da1027846c89a9ef8e1d6', 402, 3, 'Api access token', '[]', 0, '2021-03-17 23:33:24', '2021-03-17 23:33:24', '2022-03-18 05:03:24'),
('2d1ff0fe81d8b5dd6fa9733d014768dbc363971f7124dc6c0106b8ee44ab0f1e1d69d33b0e2e3f45', 663, 3, 'Api access token', '[]', 0, '2021-05-26 04:38:28', '2021-05-26 04:38:28', '2022-05-26 10:08:28'),
('2d244c781354e7984ad00c8d1b224d53ccbcad3ea7081816c2bb5eb0b1d11753e4eab698811e1f10', 448, 3, 'Api access token', '[]', 1, '2021-03-23 05:10:02', '2021-03-23 05:10:02', '2022-03-23 10:40:02'),
('2d5177a4e49a76457b38368d1952527eece56c23f98939fcda4f280aec45facdaf5784bf8d0549d3', 744, 3, 'Api access token', '[]', 0, '2021-06-16 02:33:28', '2021-06-16 02:33:28', '2022-06-16 08:03:28'),
('2d5ba7803c1fb10c9eb23188c8dfc11ece106548eafb6cb593ab2d5e2691777fd00e1e7e2e992563', 489, 3, 'Api access token', '[]', 0, '2021-03-31 04:04:11', '2021-03-31 04:04:11', '2022-03-31 09:34:11'),
('2d8300000cb9268930b0643551821a3efdb7f81e96a1c6305726a288af11344ca3421b7a9cbf514a', 486, 3, 'Api access token', '[]', 1, '2021-04-01 01:16:55', '2021-04-01 01:16:55', '2022-04-01 06:46:55'),
('2dd0b665adbd72cf981784b2a821535720869c400b6fc7b10267dcfe4c803001a81fe63c43724ad0', 550, 3, 'Api access token', '[]', 0, '2021-04-13 23:36:31', '2021-04-13 23:36:31', '2022-04-14 05:06:31'),
('2e1248c817fd68cacdf996173d47970e1b63f110c8d331eff1fa6022b8e54e96916bad3f009c7704', 400, 3, 'Api access token', '[]', 0, '2021-03-22 07:49:38', '2021-03-22 07:49:38', '2022-03-22 13:19:38'),
('2e7bb98dbbb5f2d42cc443aafcce0f448120680cc1b64eed128b91b5858305ab4538d8bceb691d54', 656, 3, 'Api access token', '[]', 0, '2021-05-31 08:14:41', '2021-05-31 08:14:41', '2022-05-31 13:44:41'),
('2eb8aebac8d77a2bbf1e7a89f9629f905569abc0f51d5e58d24273b166c0ccf3fb6119aadbaa4996', 7, 3, 'Api access token', '[]', 1, '2021-08-03 23:56:43', '2021-08-03 23:56:43', '2022-08-04 05:26:43'),
('2ec4bd276a0c812f0f401e9bb64ec4291ed11c6224aa836e7a51432e70745ac640d337e9891542ca', 505, 3, 'Api access token', '[]', 0, '2021-04-01 02:46:36', '2021-04-01 02:46:36', '2022-04-01 08:16:36'),
('2edf0ba420a7435b198ac396d74d2b681264341ced7bd453ddab17e3e8da098856ab31d2f60ff96b', 759, 3, 'Api access token', '[]', 0, '2021-07-22 01:12:11', '2021-07-22 01:12:11', '2022-07-22 06:42:11'),
('2ee33ef3f4d6876081a07dad96c5736fc2b5368b75732ea6371cf1404fbd5363d54e033cf93230d0', 720, 3, 'Api access token', '[]', 0, '2021-06-10 01:36:45', '2021-06-10 01:36:45', '2022-06-10 07:06:45'),
('2ef675291875614ef8c3999f100efdbeb9c59ed9c1455449ab909f49b3b8e81cc14680c5ed6ab65a', 579, 3, 'Api access token', '[]', 1, '2021-05-06 01:50:39', '2021-05-06 01:50:39', '2022-05-06 07:20:39'),
('2f01c0ce7cd0b1222ef6080b441c90ea1e2631a4aac41f71635a89eb8f880f80c85f2e2f71731e7a', 579, 3, 'Api access token', '[]', 1, '2021-05-05 04:26:58', '2021-05-05 04:26:58', '2022-05-05 09:56:58'),
('2f9c55d2d0f7be0ff5b257db3a895b1d14484794b2f2438fc68a3b4b25fa6774ff9beb1cded54f90', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:10:43', '2021-05-08 02:10:43', '2022-05-08 07:40:43'),
('2fe53f0ab78bca7dea715cfd511f1a0080045890ac53dd1f19124d023757e7c18ed72c7079026b44', 1, 3, 'Api access token', '[]', 0, '2021-07-30 04:59:12', '2021-07-30 04:59:12', '2022-07-30 10:29:12'),
('2ff735d82f7205dcf7611484857e3c08675022fc43827e59aa241a7ff97dad9c718308a88430e183', 466, 3, 'Api access token', '[]', 0, '2021-03-25 02:32:40', '2021-03-25 02:32:40', '2022-03-25 08:02:40'),
('2ffa0ba717f5a9eb935c039b888be706b470d9fa410b3308b596669ce1ea45c79291fc9093f89447', 521, 3, 'Api access token', '[]', 0, '2021-04-02 23:14:36', '2021-04-02 23:14:36', '2022-04-03 04:44:36'),
('2fffa73bb3254a83fd14837d1669971b49fa2f387a4eb54b7373e7e62dacc77d475172be752d1053', 552, 3, 'Api access token', '[]', 1, '2021-04-15 00:01:16', '2021-04-15 00:01:16', '2022-04-15 05:31:16'),
('3008ce28991f793afd17fc644fd49cb22a85e82c594b66a4abdefc2a3c1fbdf1bc5c96036e623cce', 434, 3, 'Api access token', '[]', 1, '2021-03-23 04:42:04', '2021-03-23 04:42:04', '2022-03-23 10:12:04'),
('30482444184dfa98a75e013e8115276b8a86be3ba5916c379587902e9c729d809838461d9b32ac4d', 486, 3, 'Api access token', '[]', 0, '2021-03-31 04:05:21', '2021-03-31 04:05:21', '2022-03-31 09:35:21'),
('308e94ceddd1efc534c78947ce90b56754d8582877f8f7b03b497e331ccd9997a24d9e32a0e5ef31', 504, 3, 'Api access token', '[]', 0, '2021-04-01 23:38:09', '2021-04-01 23:38:09', '2022-04-02 05:08:09'),
('30afb45afa406101b7b21a664221752c0c75b31422f55facae979cff9844534dced0b7deb51fddca', 7, 3, 'Api access token', '[]', 1, '2021-07-30 00:55:32', '2021-07-30 00:55:32', '2022-07-30 06:25:32'),
('30cb5677a94fc37c0436c23819a54ccd724dc6bf1b7a4c770bbaf62ee8a588a80a8e9a0f77c5d1a2', 506, 3, 'Api access token', '[]', 0, '2021-04-02 05:10:09', '2021-04-02 05:10:09', '2022-04-02 10:40:09'),
('30fffc290df40ded59b268d2752b4ead7d6ecbdbeb2dba6db461c0ff20148614a38094797a49380a', 626, 3, 'Api access token', '[]', 0, '2021-05-04 07:24:41', '2021-05-04 07:24:41', '2022-05-04 12:54:41'),
('31204ae68d37e8e29654dfb150005f78bca3d7967c2d08971087999cb286c67fcec0019d4eca0c0f', 24, 3, 'Api access token', '[]', 0, '2021-07-28 07:55:20', '2021-07-28 07:55:20', '2022-07-28 13:25:20'),
('3197f7d1855f738fe22c516702e96b404a08946adad903c01a3caaca35c85fca6837896151757cc2', 503, 3, 'Api access token', '[]', 0, '2021-03-31 04:32:31', '2021-03-31 04:32:31', '2022-03-31 10:02:31'),
('31995b17d737b3733024856d9e883eec45e1304f970f8448bbf57854ce43c2fccc8eb6269ad6c1d3', 763, 3, 'Api access token', '[]', 0, '2021-07-06 04:41:46', '2021-07-06 04:41:46', '2022-07-06 10:11:46'),
('31a0d693cfc4440a372bd19c483ddb8522c9a450ed65a75ce3d7131ebc040df2a274c92f983e6b55', 488, 3, 'Api access token', '[]', 1, '2021-04-01 02:55:36', '2021-04-01 02:55:36', '2022-04-01 08:25:36'),
('31a29257993c3574552006f51335c90aab4a2766997aff6761cb9e540a5d8a01cdf0894334cb02b4', 579, 3, 'Api access token', '[]', 1, '2021-05-06 00:02:23', '2021-05-06 00:02:23', '2022-05-06 05:32:23'),
('31bf05a09b07d6ba3de644fa1c7e1071b0680ba3ff66c927e16f2d38d1facf61afaa25e701a8a077', 534, 3, 'Api access token', '[]', 1, '2021-04-05 05:00:29', '2021-04-05 05:00:29', '2022-04-05 10:30:29'),
('31da4b7e720c512520500c37da265652be69a919ee42ff329e155e7be0321556bf01a755653115f5', 639, 3, 'Api access token', '[]', 0, '2021-05-28 07:28:18', '2021-05-28 07:28:18', '2022-05-28 12:58:18'),
('320d3701c99619083ca4a6d6389f0c9719f902a86e9dbbe678c4ae6b56f909c1cad8089512ee73e3', 1, 3, 'Api access token', '[]', 0, '2021-07-30 00:12:53', '2021-07-30 00:12:53', '2022-07-30 05:42:53'),
('320daf21702012f7e260dbb4151fa4432f86aff4372d2528f2f8c3ae4194dbf2543f2f5c0074c95e', 393, 3, 'Api access token', '[]', 0, '2021-03-22 06:34:29', '2021-03-22 06:34:29', '2022-03-22 12:04:29'),
('32292db1f5b2b1297da1c6032c241c41d61cab6b3e6477a5848f88b1474316486b2b97f9a17381d3', 554, 3, 'Api access token', '[]', 1, '2021-06-02 21:19:51', '2021-06-02 21:19:51', '2022-06-03 02:49:51'),
('324e9c9c00c760f2b66c9550dc603f27a5d430aeb07e638520a9c0b90fcadb2eefd70b865f2776ce', 1, 3, 'Api access token', '[]', 0, '2021-07-29 04:37:33', '2021-07-29 04:37:33', '2022-07-29 10:07:33'),
('32cca4356f35cc23620361551711fc81ed71d64e73ff866bbae2dd991a8731a82356a6d90b166b8b', 396, 1, 'Api access token', '[]', 1, '2021-03-17 06:20:03', '2021-03-17 06:20:03', '2022-03-17 11:50:03'),
('32d37061cc568f725ecd3d504841fb5704229326c284951810a10e4bf175679d15a7e0421ebdf5e7', 51, 3, 'Api access token', '[]', 0, '2021-08-03 07:31:35', '2021-08-03 07:31:35', '2022-08-03 13:01:35'),
('32fa83be3f67d125663eb78583bd2d1cdbbc5b871662135844c3db1fdd541fd9bd54d1b25c76dece', 488, 3, 'Api access token', '[]', 0, '2021-03-31 01:02:02', '2021-03-31 01:02:02', '2022-03-31 06:32:02'),
('33635b80b020c14e59067e1096746959fb44fc80626255d1e9d21021054cdf0253003f3534251476', 411, 3, 'Api access token', '[]', 1, '2021-03-18 08:14:23', '2021-03-18 08:14:23', '2022-03-18 13:44:23'),
('33726f649d240b9a9d7d9515a32994bcf7aa7949ca11dcf246bda1ec6070fabcb9fa7eb62568cac7', 7, 3, 'Api access token', '[]', 0, '2021-07-26 00:55:44', '2021-07-26 00:55:44', '2022-07-26 06:25:44'),
('3377c36b742b0dcc16f4c7a7725540748590cfc8ea8edc267e945eab911d7c94e56119da022ff470', 404, 3, 'Api access token', '[]', 0, '2021-03-18 00:41:01', '2021-03-18 00:41:01', '2022-03-18 06:11:01'),
('33f4a3cd68c40dd3bca9cfcc4f6ad27e97499b19894152b69e5000ff6091da7aedf2a7dd1765e51d', 432, 3, 'Api access token', '[]', 1, '2021-03-24 04:50:03', '2021-03-24 04:50:03', '2022-03-24 10:20:03'),
('34106b43e59e5ab34704b0662afe7023ca957a9d42a84f090858924da73d3143052664ec6513e552', 624, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:20', '2021-05-04 00:49:20', '2022-05-04 06:19:20'),
('342d368288fc82c7ebd8e73ae62e87577af09c5efba1b6fd9816f140e2736e522ffc91f0861c6aba', 646, 3, 'Api access token', '[]', 0, '2021-05-14 05:07:01', '2021-05-14 05:07:01', '2022-05-14 10:37:01'),
('344cebf3c8e8e461fda8ccdf9d0690813bbd45ee07759a1574051640ce26a84174f7fd880c2dd8ab', 618, 3, 'Api access token', '[]', 0, '2021-05-04 07:15:49', '2021-05-04 07:15:49', '2022-05-04 12:45:49'),
('346ce4eada4f865573237e8b3abe5457a3ab1be8d1a6be697dedff119a1ea27cc33c1ec17c780db9', 570, 3, 'Api access token', '[]', 0, '2021-04-19 02:07:21', '2021-04-19 02:07:21', '2022-04-19 07:37:21'),
('3486a8d8cc5d625d62ae9b5a63266477f00b832ff4284243af428e0cce97675e5e168abdc194878f', 626, 3, 'Api access token', '[]', 0, '2021-05-29 06:59:40', '2021-05-29 06:59:40', '2022-05-29 12:29:40'),
('349a6f01f584806199b3aa76c0f8fbe5ffb3f0b6827aa072f7a117179a0f2b6b20aebefe41b75892', 753, 3, 'Api access token', '[]', 0, '2021-06-24 06:59:26', '2021-06-24 06:59:26', '2022-06-24 12:29:26'),
('34a2a04b036c51ebed5889605ad32ecac35e1627e955caacde52cd42337d1563920a85e5cfa9e2d6', 740, 3, 'Api access token', '[]', 0, '2021-06-15 04:28:49', '2021-06-15 04:28:49', '2022-06-15 09:58:49'),
('34a5d6101599be6f0f3c498b71a3a4ca2efc8cba6e4675a56b57bf1e89e46ef400c52736f5926bf0', 503, 3, 'Api access token', '[]', 0, '2021-03-31 04:32:31', '2021-03-31 04:32:31', '2022-03-31 10:02:31'),
('34e3f142947d337961bf37f28911c024392742e4028fbe9ec4b9480995c370983b857c7b1635fff7', 612, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:46', '2021-05-02 08:41:46', '2022-05-02 14:11:46'),
('34e9491e48707da39b1ad41c2049f732b104d54afb665bd09e3eaf9bf8b3e5431bf44a89d34252c8', 533, 3, 'Api access token', '[]', 1, '2021-04-06 00:59:59', '2021-04-06 00:59:59', '2022-04-06 06:29:59'),
('350657ce646029dbb342370c40fd580facf438d5b2292145505aab20e85fade7cdc5a40dc3f64600', 552, 3, 'Api access token', '[]', 1, '2021-04-14 23:44:56', '2021-04-14 23:44:56', '2022-04-15 05:14:56'),
('35105abf069cc329db3b77f76a11a5861a3cdc616f29c7b5f9a6f919c0225d1b3a3ccd8577ee7568', 620, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:14', '2021-06-02 08:17:14', '2022-06-02 13:47:14'),
('356a4963537186522e1ce449eaaae436242309fd73c601b0acfc6b095bb8d27fd387a0fc2921dbc2', 536, 3, 'Api access token', '[]', 0, '2021-04-05 02:22:55', '2021-04-05 02:22:55', '2022-04-05 07:52:55'),
('359a1e87f6b1eda8a466b1d49c750ad8fb91e908b09aa367ae71ea275a7b8db915e0abcfbbccc2e9', 13, 3, 'Api access token', '[]', 1, '2021-07-27 04:42:54', '2021-07-27 04:42:54', '2022-07-27 10:12:54'),
('35abfb24fc93506e31f671191a34ad618026e8ecb2637deeec1419497f2f584ea50f8b44d80f26da', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:34:48', '2021-05-12 09:34:48', '2022-05-12 15:04:48'),
('35d0bf5863e2c394f060a52b3126d94f977379d66a7633e74760b06d196546541b1c680232a40037', 649, 3, 'Api access token', '[]', 0, '2021-05-14 10:49:20', '2021-05-14 10:49:20', '2022-05-14 16:19:20'),
('35fa2eb4f09a08a0c78165ced80bd650cb53bfac8a176f0c694523c846b64798fd19c623b3f84dbe', 4, 3, 'Api access token', '[]', 1, '2021-07-27 07:11:28', '2021-07-27 07:11:28', '2022-07-27 12:41:28'),
('360487703bf7af63f64428d9602a0fde6ac8dc0af2525578e191c0475650c222a665614ccdaab5c4', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:24:51', '2021-07-26 07:24:51', '2022-07-26 12:54:51'),
('362ac4432075c271d06c935672182aaf2d71d6d0c3facdc91686de283674ac0f0f823072ab882c3f', 751, 3, 'Api access token', '[]', 0, '2021-06-24 06:46:04', '2021-06-24 06:46:04', '2022-06-24 12:16:04'),
('3634939f30c23cbded30503f7cfe40a010330f1b9f4bcfdd6870e2ca9344d7ca5602a550163877d8', 626, 3, 'Api access token', '[]', 0, '2021-05-04 06:58:55', '2021-05-04 06:58:55', '2022-05-04 12:28:55'),
('3644e3021940b42cbaffa059f511aa1ee1cd5d54f2039d5146551aa1b40450bd5b365b997b30bb3f', 734, 3, 'Api access token', '[]', 0, '2021-06-14 10:02:17', '2021-06-14 10:02:17', '2022-06-14 15:32:17'),
('3655beea9f7810e960f18c63e6dd6b89b87e4db23c6a5260f6c0264db2b41a922f3cd7eade0d9d54', 703, 3, 'Api access token', '[]', 0, '2021-07-21 03:54:04', '2021-07-21 03:54:04', '2022-07-21 09:24:04'),
('365974daa748e37de4caff23c970da50f255db8a10d62eb29d8bbeba91b4aa3084f49223d759cdc7', 7, 3, 'Api access token', '[]', 0, '2021-08-04 01:02:26', '2021-08-04 01:02:26', '2022-08-04 06:32:26'),
('3676c0756ac4b3db32d0200c34f6bebef490b1b37f732cef0330fb29939ebbb8b042c2b72dbbda70', 486, 3, 'Api access token', '[]', 0, '2021-03-30 23:55:23', '2021-03-30 23:55:23', '2022-03-31 05:25:23'),
('36c79a71f8da1f852720285921939ec1fe0ab055bf2d6a465966f1c7b67608d9aaea21e3fdbebecc', 533, 3, 'Api access token', '[]', 0, '2021-06-08 07:58:11', '2021-06-08 07:58:11', '2022-06-08 13:28:11'),
('36d7b50ff605cf5c8d98711c1cf959d6bf1ed607cc63385d4905c3a189da69602b90f93bf6863d5d', 587, 3, 'Api access token', '[]', 0, '2021-04-29 01:48:15', '2021-04-29 01:48:15', '2022-04-29 07:18:15'),
('36f543df406d80d20564b562ea13ac4df7bab58ac960431bf51f6176432cd5caa20105cba1c2a8bb', 441, 3, 'Api access token', '[]', 0, '2021-03-23 02:31:49', '2021-03-23 02:31:49', '2022-03-23 08:01:49'),
('36f8fd498b37e239586e322a481da9fc11feaab0c55577cbce968e281781a406ef5367fef0ec12fd', 407, 3, 'Api access token', '[]', 1, '2021-03-18 06:53:44', '2021-03-18 06:53:44', '2022-03-18 12:23:44'),
('371f1063a1c8bf4a4524ce3fe3380e9cf06927a8bc9d8434b1deb6b9f76ea3f1070fe9e775d6b12e', 17, 3, 'Api access token', '[]', 1, '2021-07-28 08:09:55', '2021-07-28 08:09:55', '2022-07-28 13:39:55'),
('3733af593e8cf3881f0fad8408e89a66506716cbface1bff4e1b384b9d83e949051270e542bdfbfc', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:04', '2021-05-04 01:23:04', '2022-05-04 06:53:04'),
('3767261a6dc55cfc02d973ebb76ad3d71078bb00ada2d2459560cd202b62547cbcfd457a9fd9f9ab', 662, 3, 'Api access token', '[]', 0, '2021-05-26 00:46:24', '2021-05-26 00:46:24', '2022-05-26 06:16:24'),
('376fe337531b643cabc425a6d91bfe05bd1b66b4d2efec1233de34d7aa5a60932fe35065e1cc2718', 626, 3, 'Api access token', '[]', 0, '2021-06-04 06:41:15', '2021-06-04 06:41:15', '2022-06-04 12:11:15'),
('37b8b177b97b719e4ef7fbf9b71a825a9e8c201407c4a5a9780e54b26dc74c23cd7b813749275685', 56, 3, 'Api access token', '[]', 1, '2021-08-06 23:49:38', '2021-08-06 23:49:38', '2022-08-07 05:19:38'),
('38133e60728eb685a9e3caa48dc4a4ec51a59a2bb9de03c41caf34061395dac1e6a494092a7585cb', 415, 3, 'Api access token', '[]', 1, '2021-03-19 01:21:44', '2021-03-19 01:21:44', '2022-03-19 06:51:44'),
('3839e3fd34a27ee39a833e55bc45b2538ebedcb8d8f840d07e650b5df23dcc680484a96119fd089f', 784, 3, 'Api access token', '[]', 0, '2021-07-21 06:49:48', '2021-07-21 06:49:48', '2022-07-21 12:19:48'),
('38af44f3a08f17fc160ac3439902861763fdb9a918a838fbf97de96e3cdcfe92a6b8e46881d381e2', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:59:43', '2021-05-11 00:59:43', '2022-05-11 06:29:43'),
('38b64d4bc75bf5fb7973ef65d5634c014e8a1f41566f941f85b0dbcc1f03a43fc87ba554bccd87a2', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:00:29', '2021-05-11 06:00:29', '2022-05-11 11:30:29'),
('38c48199559a9ca9c05a3094f799f3b72ee4f840506786fea8d0e678c7e7829db73656291a56d3a8', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:54:32', '2021-07-26 07:54:32', '2022-07-26 13:24:32'),
('38cd272481b5de1c5ce532b32be10686b4f07213006685edd1da7580ab822313a49709eb82202e2a', 438, 3, 'Api access token', '[]', 0, '2021-03-24 05:40:25', '2021-03-24 05:40:25', '2022-03-24 11:10:25'),
('38d5d1edef6802b63dfc0f513b3872aecf2c06ba60859ae22e42c6404bc1036f54cbb70ed43c67ff', 534, 3, 'Api access token', '[]', 1, '2021-04-05 04:59:23', '2021-04-05 04:59:23', '2022-04-05 10:29:23'),
('3905101d9f93de7852d57f02e326585e8a9f38bd07207cdfacca6e23d618bed7dca32ebbc5747534', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:47:03', '2021-03-22 05:47:03', '2022-03-22 11:17:03'),
('392264c431ebaecbafa17cfcdb1f3fcd66018f31950e0c3467a842cbeed307fdba9132df1994b063', 728, 3, 'Api access token', '[]', 0, '2021-06-10 01:47:48', '2021-06-10 01:47:48', '2022-06-10 07:17:48'),
('3922dd9619cbbacfd7c1199d9765659029a4951ddbfe502275769ae35b0e900c5b669dd5155c8f4f', 667, 3, 'Api access token', '[]', 0, '2021-06-01 07:40:19', '2021-06-01 07:40:19', '2022-06-01 13:10:19'),
('392beac4003d0f95e855bfd86aec5df0ba62088922acbf3c2f20bd7211c2c916ccdf57cfa9daae57', 556, 3, 'Api access token', '[]', 0, '2021-04-14 00:51:13', '2021-04-14 00:51:13', '2022-04-14 06:21:13'),
('3935d62e2f0cb82f97d48a0f9213e5f932a13066479848b24e137dd93ca2af9d67ee4639087421d2', 572, 3, 'Api access token', '[]', 0, '2021-04-20 00:58:42', '2021-04-20 00:58:42', '2022-04-20 06:28:42'),
('3938af225f9a880c1d9d4a37cdccc79c1550be3efab63cce4c7a34933b26d12823711310d131d414', 478, 3, 'Api access token', '[]', 1, '2021-03-26 04:19:29', '2021-03-26 04:19:29', '2022-03-26 09:49:29'),
('393f75b7eb89c12db87fd569498b3c935077f9f0f0a6865dd8450ba94f20dcbac5530665f0cdc5e7', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:43:11', '2021-07-15 07:43:11', '2022-07-15 13:13:11'),
('39483d9b4328678de718517f1feeca18de6cb4afe096fc7bd8610506bf93628497bbe48c69aa4f94', 686, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:35', '2021-06-10 02:04:35', '2022-06-10 07:34:35'),
('3970703a3a229383745d2f648348e4b773211b9244a04b9db2fb41669733ccea371aed3d20e922c6', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:35:09', '2021-07-20 02:35:09', '2022-07-20 08:05:09'),
('398400266942fe3b6baa2f92d1fbd9686c23df1787b60c54fadc40d3dc3df5673f475ed142bce236', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:47:42', '2021-05-08 02:47:42', '2022-05-08 08:17:42'),
('398e5bf6fcc21e5ed34e6cbc439ba01de55af5b0c5601f8fb3b8cfa7a764163d6da2de51d64360de', 691, 3, 'Api access token', '[]', 0, '2021-06-02 23:45:21', '2021-06-02 23:45:21', '2022-06-03 05:15:21'),
('399d8927a55b2b0778181ac305dc03375c9ae65cec16a5c17553187d61f8a948c22eae6f7e7ab07a', 753, 3, 'Api access token', '[]', 1, '2021-06-30 08:44:37', '2021-06-30 08:44:37', '2022-06-30 14:14:37'),
('39a8e8e8fd213946e1cf5c7e1bbead873028cace8b9f9999b631e42ab981872873fdbed870094411', 523, 3, 'Api access token', '[]', 0, '2021-04-03 00:12:10', '2021-04-03 00:12:10', '2022-04-03 05:42:10'),
('39aae62cb30187d94e7e86ee12ebf22549ae3b3f2c9a604e935d55d0c9c3c20fc0ec586792a42231', 642, 3, 'Api access token', '[]', 0, '2021-05-13 02:21:32', '2021-05-13 02:21:32', '2022-05-13 07:51:32'),
('39baa91b19416183f089e6c72da39968aa691e85a487322ac726df51474ac565f128e51d59f0d452', 3, 3, 'Api access token', '[]', 1, '2021-08-03 01:36:31', '2021-08-03 01:36:31', '2022-08-03 07:06:31'),
('39cb6d5555b383860cbe5039c1f2e2cd2180b4c983468de1fe0b39733ba0842ea681ddc8ef84632d', 531, 3, 'Api access token', '[]', 1, '2021-04-05 04:05:52', '2021-04-05 04:05:52', '2022-04-05 09:35:52'),
('39cdbad8c65bf98c18d87f6fd65d042cf20f07a6b8daad4a6930c182f03a8699ff0f7ff9bbbdd0bc', 19, 3, 'Api access token', '[]', 1, '2021-07-29 06:35:04', '2021-07-29 06:35:04', '2022-07-29 12:05:04'),
('39d3a7ac0c72574dc7d5145d89719ed2a263615460e2e82d2334394705b0aa96c3230bf8c90dab4d', 432, 3, 'Api access token', '[]', 1, '2021-03-23 05:31:48', '2021-03-23 05:31:48', '2022-03-23 11:01:48'),
('39d62ed93f2d4c10df384372f2a02ce43dc22ffdf00c5e1df88b15227ab8c81cc6775dadf8ae27a7', 491, 3, 'Api access token', '[]', 0, '2021-03-26 06:35:30', '2021-03-26 06:35:30', '2022-03-26 12:05:30'),
('3a2594f6270c93e2c18ee155275ded53fa5f77295bc6bf835e86a3125e2add524a64bf950349b13c', 771, 3, 'Api access token', '[]', 0, '2021-07-15 09:13:33', '2021-07-15 09:13:33', '2022-07-15 14:43:33'),
('3a47c2a02ee043ca6891f231edbb1a757792cdc09151362ad9e97106264da611f63e761e061b9d3a', 461, 3, 'Api access token', '[]', 0, '2021-03-24 05:28:08', '2021-03-24 05:28:08', '2022-03-24 10:58:08'),
('3a90982517cc4e38be46cd595b00a10740225f0c955c68a17990fd9aa480ed29fe184b4ba47e06ac', 681, 3, 'Api access token', '[]', 0, '2021-06-02 07:52:42', '2021-06-02 07:52:42', '2022-06-02 13:22:42'),
('3acd6836d847c88727ef3e8131cd7bc2363b4edbd233bccdd880db680b1396af470548e9c47f7720', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:10:52', '2021-07-26 07:10:52', '2022-07-26 12:40:52'),
('3ae9f179187d109d3e1762bd156cdf47df466567111b58d1765f886b4efbe6356b57b6655b498586', 612, 3, 'Api access token', '[]', 0, '2021-05-13 02:28:57', '2021-05-13 02:28:57', '2022-05-13 07:58:57'),
('3aecc450afaaffd62cc289889b915ea58066162266b31c2f4b543941f44aa9804dbb458779b3632f', 1, 3, 'Api access token', '[]', 0, '2021-07-30 05:31:52', '2021-07-30 05:31:52', '2022-07-30 11:01:52'),
('3affba41947bc97672eddd643c760117037eb9c9196dffbf0ba66f15f72d2f8f2a4c7f53580ae969', 644, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('3b0d36eba88ba4b5a9ee9e1b9552df2a95a2fb61c2155fe177f7293faac95256d38ddddc66635ca8', 5, 3, 'Api access token', '[]', 0, '2021-08-06 23:35:40', '2021-08-06 23:35:40', '2022-08-07 05:05:40'),
('3b0d98d731e910944b7891fff52517db45bef6fac4f40200d91b546d3da01d29aaf225798c523ae5', 418, 3, 'Api access token', '[]', 0, '2021-03-22 00:31:00', '2021-03-22 00:31:00', '2022-03-22 06:01:00'),
('3b1a85664c46231528b18b842472dc1b276262155f83de8f40800f2f244cf31393a9eb02fbced9e7', 552, 3, 'Api access token', '[]', 0, '2021-04-15 23:47:51', '2021-04-15 23:47:51', '2022-04-16 05:17:51'),
('3b22184f4fcfac83b764e0ba6d0e8ab3159cf0fff375cf021b6368bd6515e02d7c34e3e456d66ff7', 414, 3, 'Api access token', '[]', 0, '2021-03-18 23:45:16', '2021-03-18 23:45:16', '2022-03-19 05:15:16'),
('3b626e1baaedc1335dcdd4238eddc466f1048ab3867367b10f2e91ba5bf36e51f4a92d0f4681553e', 770, 3, 'Api access token', '[]', 0, '2021-07-15 10:03:03', '2021-07-15 10:03:03', '2022-07-15 15:33:03'),
('3bb3be40f4490b61c86954ef94e4fdf0118c5ed5efaff5428e584379f608f9d73e99753245f6ea06', 393, 3, 'Api access token', '[]', 1, '2021-03-18 04:14:03', '2021-03-18 04:14:03', '2022-03-18 09:44:03'),
('3bdd022274f030f1755842bcdb8c514ef6ca7837e08f47fc1c7e4058ce048623f13ca4205a579acb', 24, 3, 'Api access token', '[]', 0, '2021-07-28 06:53:05', '2021-07-28 06:53:05', '2022-07-28 12:23:05'),
('3bf1f8e5c96d7f3fbc1bd3709b0f3ee9aa7a968c1f941cda587763970de56df2b6ae448e8f03b273', 653, 3, 'Api access token', '[]', 0, '2021-05-17 04:49:38', '2021-05-17 04:49:38', '2022-05-17 10:19:38'),
('3c0c8f3ff6cd91a087a34467a113fe492994f5b034ff6ed73460d2785e00a29dd473c9ab1bf6b58d', 420, 3, 'Api access token', '[]', 0, '2021-03-22 05:13:50', '2021-03-22 05:13:50', '2022-03-22 10:43:50'),
('3c2ae611275638c20835aa44a87478d6d20ac30dd09e56ba275deb08aebe35843cefd0eec006ac14', 1, 3, 'Api access token', '[]', 0, '2021-07-22 06:07:46', '2021-07-22 06:07:46', '2022-07-22 11:37:46'),
('3c3887f7f7525e01172b550e06ef2f9ff77a0c27a9812741d0003db869e523cf5effac900ef9da21', 19, 3, 'Api access token', '[]', 0, '2021-07-29 06:30:01', '2021-07-29 06:30:01', '2022-07-29 12:00:01'),
('3c69abf305abb13507306484f8a3d8278cafc5d3e3b2b824bfc3c736132574d005399443a34b8910', 665, 3, 'Api access token', '[]', 0, '2021-07-19 00:32:08', '2021-07-19 00:32:08', '2022-07-19 06:02:08'),
('3c752304dcacd86c230135a96efe95857cc417c3afb9da7e31df6d156dac9d80f50a201e730592bc', 488, 3, 'Api access token', '[]', 0, '2021-04-02 00:31:04', '2021-04-02 00:31:04', '2022-04-02 06:01:04'),
('3cfcd60942268fc97e5edb0dfa64da37c4c2bbd5294b535357654f76917a8bd3ecc324f0bb104a41', 626, 3, 'Api access token', '[]', 0, '2021-06-04 06:52:08', '2021-06-04 06:52:08', '2022-06-04 12:22:08'),
('3d06069d907681f12425b2bca7b820e2bc1848b44dbca30eb068890e663d64d7f954c7f27015b2ee', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:47:20', '2021-08-06 05:47:20', '2022-08-06 11:17:20'),
('3d147723b6c410fc686a074959bf705dfc7602c6fe484d60effd6dc2a0b7e87c1ec2ed6892a9b66b', 36, 3, 'Api access token', '[]', 0, '2021-07-28 07:30:06', '2021-07-28 07:30:06', '2022-07-28 13:00:06'),
('3d2f8eaf527eaa58e9847100446c70112676cb0673bba5bf892d73fbe21064b867216b439bc35e2a', 65, 3, 'Api access token', '[]', 0, '2021-08-16 03:11:12', '2021-08-16 03:11:12', '2022-08-16 08:41:12'),
('3d43c204bb5e528af2757b5b2b19096116211360f910bb2c04c6a73d0dc43b958331169c2174d356', 710, 3, 'Api access token', '[]', 0, '2021-06-08 09:27:01', '2021-06-08 09:27:01', '2022-06-08 14:57:01'),
('3d4ee8633ce23b3d15cc54ff265115f0dc7a42d241a505a15430c56fe5ec71552c21ff574a9d9c64', 29, 3, 'Api access token', '[]', 0, '2021-07-28 07:15:33', '2021-07-28 07:15:33', '2022-07-28 12:45:33'),
('3d9c0df62f9983c2cdf28e237a84d27c00df82cfd7fe39955e3b0f3c8b92432f4e004952bcfb53bb', 538, 3, 'Api access token', '[]', 1, '2021-04-05 05:15:14', '2021-04-05 05:15:14', '2022-04-05 10:45:14'),
('3da15d00d39bc5736affe86c92df2004b85a50171ae7b42878f9a90ebfa82f7337072c3629cd1e5d', 555, 3, 'Api access token', '[]', 0, '2021-04-19 06:54:17', '2021-04-19 06:54:17', '2022-04-19 12:24:17'),
('3db8a5edbabe309212296771df1c5e14e604ded26dabe2513ccb2bd1d3bd43dfd81b92575c719330', 734, 3, 'Api access token', '[]', 0, '2021-07-15 06:43:46', '2021-07-15 06:43:46', '2022-07-15 12:13:46'),
('3dc2b51da07b613bbad3b74edf7119aeb86d361764f8045d41b5f1d3ad7350854f52546a0366e5c7', 757, 3, 'Api access token', '[]', 1, '2021-06-30 08:56:38', '2021-06-30 08:56:38', '2022-06-30 14:26:38'),
('3dc2e2b1104ea6d1311a0d1a7ab4256993b0fe90c4afbba84c668110d7d21fc84629b982141fac88', 701, 3, 'Api access token', '[]', 0, '2021-06-03 01:01:22', '2021-06-03 01:01:22', '2022-06-03 06:31:22'),
('3de499a52b6ea29c87d6300ba133be9736146bd058b25bb123fc8e36c16a73eaf3acfb6d346232aa', 478, 3, 'Api access token', '[]', 0, '2021-03-26 04:19:29', '2021-03-26 04:19:29', '2022-03-26 09:49:29'),
('3de4a5d0810d5a19b4cb766a340353ce8781f553f9cdd581066fc106383325920278c0181a814031', 5, 3, 'Api access token', '[]', 1, '2021-07-27 04:18:38', '2021-07-27 04:18:38', '2022-07-27 09:48:38'),
('3de700851f5cf63ff78225fcc60ac25fc84dda98690d1ba9f425b830a1abababfa26a38a14cce6ac', 738, 3, 'Api access token', '[]', 0, '2021-06-15 03:04:32', '2021-06-15 03:04:32', '2022-06-15 08:34:32'),
('3e1fb7b6e30c915cd6748a0c7552371c14c03a7afd852307966c1632acec372d300cb5ede2161b05', 4, 3, 'Api access token', '[]', 1, '2021-07-27 07:09:08', '2021-07-27 07:09:08', '2022-07-27 12:39:08'),
('3e2f7f009aeaddf6a5ae99d1b0cb034d296a989c3e3d2d17090db40c4717490c908f4c44542d937f', 612, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:39', '2021-05-04 00:40:39', '2022-05-04 06:10:39'),
('3e3a0af770cd5aacbdd57cb5995dd2308812f8803a1b66fef92bd95be272bb3ead9087580e923119', 17, 3, 'Api access token', '[]', 0, '2021-07-29 06:54:34', '2021-07-29 06:54:34', '2022-07-29 12:24:34'),
('3e6f07ab3d779b1682b7acf8fba0d3f06eb136718bd69c016e7fed8900506a3ea853dd68aa7a863f', 626, 3, 'Api access token', '[]', 0, '2021-05-27 02:20:42', '2021-05-27 02:20:42', '2022-05-27 07:50:42'),
('3e723b9554f557d19b79b5f8c3b316e45892f149052aa17661325ab0decf91d8dc7865efbfba5180', 702, 3, 'Api access token', '[]', 0, '2021-06-04 01:35:09', '2021-06-04 01:35:09', '2022-06-04 07:05:09'),
('3e73165a3dc4ccb7cb0db5c077901bd85e59f216706dcb5afb064be4d560bb6fd7676b387db0041f', 392, 1, 'Api access token', '[]', 1, '2021-03-17 07:45:28', '2021-03-17 07:45:28', '2022-03-17 13:15:28'),
('3e8168730586e6988bb244f15e88cd28330b1be8273c90cf22299e5402b8a48036b8120579452612', 626, 3, 'Api access token', '[]', 0, '2021-05-27 05:28:12', '2021-05-27 05:28:12', '2022-05-27 10:58:12'),
('3e88b6990868f20b8235143617894a1700fdd91cbf1095a0cfdc77f3b6a711baa25caa762c0bedf3', 17, 3, 'Api access token', '[]', 1, '2021-07-28 07:48:15', '2021-07-28 07:48:15', '2022-07-28 13:18:15'),
('3ed609f0051c886dafeebb1bddf3c0c173e827aa63574762d36f9d718e78fcaefdf25f80fbdefc14', 614, 3, 'Api access token', '[]', 0, '2021-05-02 08:14:30', '2021-05-02 08:14:30', '2022-05-02 13:44:30'),
('3f7834d40afd2ec666097cc08ea7577a92cc8edbe7d14863491e5a0f647f6c53f1a403e4677a7844', 611, 3, 'Api access token', '[]', 0, '2021-05-02 08:34:24', '2021-05-02 08:34:24', '2022-05-02 14:04:24'),
('3f78e388f678d5d7bb2ab5d41d76c5ebec9dab69211d5ab1fca1e13665d320f5760b24f53c59cf17', 450, 3, 'Api access token', '[]', 1, '2021-03-23 05:28:56', '2021-03-23 05:28:56', '2022-03-23 10:58:56'),
('3f8edf63f0aecbb48393d67fee77fc6b1e2f870c2491b87ae21b432601462b8bb37092d1fd9717f0', 431, 3, 'Api access token', '[]', 1, '2021-03-23 00:49:45', '2021-03-23 00:49:45', '2022-03-23 06:19:45'),
('3fac5704d32ec943f28faafa2e1f1d8773979168e2479f5b70123b8572b0fc5ec6a4ce58affcf56d', 438, 3, 'Api access token', '[]', 1, '2021-03-23 01:42:43', '2021-03-23 01:42:43', '2022-03-23 07:12:43'),
('3fd54a2b6f8eddda7dbf6ea19c379a4f7ed96849bcbe759ebf8ce06dce4ba74b3a25b05e742f3886', 648, 3, 'Api access token', '[]', 1, '2021-05-14 06:08:33', '2021-05-14 06:08:33', '2022-05-14 11:38:33'),
('402dc1116002bad6779fb32b37f97193426749ae1e95e70e0ab665d17e2a30c621bb1fd663df1718', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:05', '2021-05-04 01:23:05', '2022-05-04 06:53:05'),
('40336827fcb7c5391be0e89424c3ded83cb89122f1f03921818c0714afbed24c0e8e6969cd855eba', 586, 3, 'Api access token', '[]', 0, '2021-04-21 23:36:52', '2021-04-21 23:36:52', '2022-04-22 05:06:52'),
('4058b2b871c31fe314ba3dde461dc1d829696729981197fb627fd6f247ff542dbe915dff3e3e3c6f', 443, 3, 'Api access token', '[]', 0, '2021-03-23 23:05:31', '2021-03-23 23:05:31', '2022-03-24 04:35:31'),
('405dec493706be18fe040500d3373711a7cbe45681d8bdf36b9017dad578f736b6a80d439696e37a', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:00:57', '2021-05-08 02:00:57', '2022-05-08 07:30:57'),
('4073faff7123bbb1a65c88279b1e9958aa46318651c88fad0371e7df38d7b6c932a96ab224ada1c5', 491, 3, 'Api access token', '[]', 0, '2021-03-26 06:35:30', '2021-03-26 06:35:30', '2022-03-26 12:05:30'),
('409595ef9e141f8f6bf9d7f5276ee2233745470fac0b949b1fdd95a904e4f8f831fd0a3719b97a0f', 762, 3, 'Api access token', '[]', 0, '2021-07-06 04:35:18', '2021-07-06 04:35:18', '2022-07-06 10:05:18'),
('40aebf813c5ec95312a9fcc6aacf3557b581b95729ca36cdd312b6f903a624599859081053eac2ae', 783, 3, 'Api access token', '[]', 0, '2021-07-21 05:36:20', '2021-07-21 05:36:20', '2022-07-21 11:06:20'),
('40cc12061fd711b2a412a7b03ba8e06cfaee44dcbf54f7fa84273aa75ca6b6bb4c35037e6817e9cf', 447, 3, 'Api access token', '[]', 0, '2021-03-23 04:09:41', '2021-03-23 04:09:41', '2022-03-23 09:39:41'),
('40e2dd4ab63e21dd9bda67ee051c4b1aa6ea74bb596759bd0fba6fcd83b2a308dd1fd3ab42bfb9f6', 416, 3, 'Api access token', '[]', 0, '2021-03-19 02:11:22', '2021-03-19 02:11:22', '2022-03-19 07:41:22'),
('40e782294d3bebc5c002b3fcd8af27758b4e3bdac8d00c6e39161363b92446121cd819343f7691c4', 657, 3, 'Api access token', '[]', 0, '2021-05-24 04:32:01', '2021-05-24 04:32:01', '2022-05-24 10:02:01');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('40fc1db2b291c925d1b0e4f62a65cd3c084d27d85b8f6b4ad99fc5c573e81c632fa5b950b0da96bd', 486, 3, 'Api access token', '[]', 0, '2021-03-26 06:09:23', '2021-03-26 06:09:23', '2022-03-26 11:39:23'),
('4118be267311bc2002b6bea411f8ced918dccaafea59bc3038617997fd41f6a04ebf2d7a1deb1bc9', 626, 3, 'Api access token', '[]', 0, '2021-05-11 05:20:22', '2021-05-11 05:20:22', '2022-05-11 10:50:22'),
('4120e7b5af87b8d7a626a854d3e47b68d61036528edb41cee4f095d655affde6abd0ce8f1f4d8a1d', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:19:11', '2021-07-28 06:19:11', '2022-07-28 11:49:11'),
('415db6a0d88f3fe7b0a6a35d27cb85547156bb3791370cca297f7de163debfcf43054f37939f9b2f', 426, 3, 'Api access token', '[]', 1, '2021-03-22 05:47:04', '2021-03-22 05:47:04', '2022-03-22 11:17:04'),
('417550649c469cedfdb4237dc8f64f43c7543b27a41891b9aff5327b38a579d1b4ab575873ec4626', 688, 3, 'Api access token', '[]', 0, '2021-06-02 21:20:14', '2021-06-02 21:20:14', '2022-06-03 02:50:14'),
('418245acadb962d3c0fa3bea14f18da841e65721808397755aaf3329328885dcb75b2b8cc8c11b06', 643, 3, 'Api access token', '[]', 0, '2021-05-13 08:14:59', '2021-05-13 08:14:59', '2022-05-13 13:44:59'),
('41b7af3f4ad81267d0f001bfe87602de5a73c7057a7828dec240e2d7e63190fdce6d731f93859ca4', 649, 3, 'Api access token', '[]', 0, '2021-05-14 10:50:10', '2021-05-14 10:50:10', '2022-05-14 16:20:10'),
('41c777938c9a24965313499aebb9001bdbb844edbd8c2e25f7b279e853b6b15cac9cd2be6b514a39', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:52:20', '2021-05-04 06:52:20', '2022-05-04 12:22:20'),
('41e0d69c3c2d3396f4a50459521a14dd28e4416da60ad66f7a1e12dcd28ccac7493f2e54ff1b1618', 535, 3, 'Api access token', '[]', 1, '2021-04-05 02:03:47', '2021-04-05 02:03:47', '2022-04-05 07:33:47'),
('41e2ad180a79515c1f78de121f3f03ae0f82dee242d2fd9e93dd53e4474ae718375881863e081aef', 626, 3, 'Api access token', '[]', 0, '2021-05-11 04:39:24', '2021-05-11 04:39:24', '2022-05-11 10:09:24'),
('41e8bb921bf93d80ce9361360416d08971e61b05b0de39704a4bbb95206dc409206282d943b1f38f', 392, 1, 'Api access token', '[]', 1, '2021-03-17 07:45:47', '2021-03-17 07:45:47', '2022-03-17 13:15:47'),
('4238eb5d6ca2a02eecfe1a4c06b158ac411e19bb00c840fd904571c8ac77b2ad5063e9ef6178e2bd', 668, 3, 'Api access token', '[]', 1, '2021-06-01 08:23:33', '2021-06-01 08:23:33', '2022-06-01 13:53:33'),
('423b25a8d42e30d9e534f460a4b476d93840a55352c2f6a7afd293c0c4e6094e5f8a21f317c1009d', 495, 3, 'Api access token', '[]', 0, '2021-03-30 00:44:21', '2021-03-30 00:44:21', '2022-03-30 06:14:21'),
('42413fc26c25527f6e919bac4d0698b7e5e1f6a153f2712fe9141508c0aba244cca1ebb3a6cd07b8', 33, 3, 'Api access token', '[]', 0, '2021-07-28 07:27:32', '2021-07-28 07:27:32', '2022-07-28 12:57:32'),
('42527adf087702a360745367beb05c35eae68468849550c6f5ff2b0661c8d23487f2862bc4e0e8ee', 707, 3, 'Api access token', '[]', 0, '2021-06-08 07:12:42', '2021-06-08 07:12:42', '2022-06-08 12:42:42'),
('4278a514f37fa86b870b21a2b1edc2a6833e20ade0b74dab8b48e8e9205c6f2da62679ab54ab2e86', 483, 3, 'Api access token', '[]', 0, '2021-03-26 05:04:59', '2021-03-26 05:04:59', '2022-03-26 10:34:59'),
('42b16a45595e0eccea822ed42c802d0ea0f26b197fada2626a1fb7fea3308c4b596007a8b4cb5ce5', 445, 3, 'Api access token', '[]', 1, '2021-03-23 02:54:29', '2021-03-23 02:54:29', '2022-03-23 08:24:29'),
('42ca7d2472a8976ade29d7d8ad44fe386eaa0f6eaef70546beb18b60ec17f28449e7609b700f1deb', 547, 3, 'Api access token', '[]', 0, '2021-04-09 12:57:13', '2021-04-09 12:57:13', '2022-04-09 18:27:13'),
('42d0348cb6c70e193f474218aa5c06eb9893370d55958963c98f0da79455fde68821b319c031f5ac', 660, 3, 'Api access token', '[]', 1, '2021-05-25 23:55:40', '2021-05-25 23:55:40', '2022-05-26 05:25:40'),
('42f04e50c7925d6e4f300866f535d73bebd0adcd54c26be59e6844a590768144ab8df2c764c92d56', 658, 3, 'Api access token', '[]', 1, '2021-05-24 05:12:43', '2021-05-24 05:12:43', '2022-05-24 10:42:43'),
('432412926b2ec2cc4f9bad1a34951ea8b0b610313975831c511b7df9f989d8c026274268404204f4', 31, 3, 'Api access token', '[]', 0, '2021-07-28 07:26:09', '2021-07-28 07:26:09', '2022-07-28 12:56:09'),
('4363365ced00e370102310736439f405e4ff0ff482c881bb1d6868744e993749bb247dc3da006b48', 523, 3, 'Api access token', '[]', 0, '2021-04-03 00:21:29', '2021-04-03 00:21:29', '2022-04-03 05:51:29'),
('4394a85349f89af009522d9137b5684f259b4a31f716455095c291029f0a175ed48f5ac537408d51', 737, 3, 'Api access token', '[]', 0, '2021-06-15 01:23:37', '2021-06-15 01:23:37', '2022-06-15 06:53:37'),
('4399d5e4519831debe38f5407ad12c48949c2a618876ddc8ad8a48255bae6b296ce8079a8a7258d7', 759, 3, 'Api access token', '[]', 0, '2021-07-20 03:25:39', '2021-07-20 03:25:39', '2022-07-20 08:55:39'),
('43ad1925f520de187c58c7dfe2f6f6d223bbaa18d25e94785055550a6521c8ade7ba43a52a0ef7a7', 1, 3, 'Api access token', '[]', 0, '2021-07-29 08:18:57', '2021-07-29 08:18:57', '2022-07-29 13:48:57'),
('43c10a792f2d2bfe5d60acc6f619e859fdc5970fa8397aeb5183de1f455f80f1dc9d4f4ca3c41f85', 455, 3, 'Api access token', '[]', 0, '2021-03-23 22:50:20', '2021-03-23 22:50:20', '2022-03-24 04:20:20'),
('43d1cb9ec5b71d7f4c62f17298327deb28e8964334623c196a7a08263fd08e2c8569cd93b084e2ff', 603, 3, 'Api access token', '[]', 0, '2021-05-02 07:48:08', '2021-05-02 07:48:08', '2022-05-02 13:18:08'),
('43f890f81b70fcd9cbea13f6f9ac900bbd66ab6dc0f7be2a7eb194f564ff570471e2dabab4f3fc59', 639, 3, 'Api access token', '[]', 0, '2021-05-13 08:44:53', '2021-05-13 08:44:53', '2022-05-13 14:14:53'),
('4410c2f25268a520591d88416bd65f856b3bd5f871b3503365e2316316037eac0f4d1a1bb76f1b86', 773, 3, 'Api access token', '[]', 0, '2021-07-19 08:24:33', '2021-07-19 08:24:33', '2022-07-19 13:54:33'),
('4412840f8d07c3139848799bc635a722142ed70fb4752c7915d018ece81992c9ffe7eb7079c7f709', 17, 3, 'Api access token', '[]', 1, '2021-07-27 07:24:42', '2021-07-27 07:24:42', '2022-07-27 12:54:42'),
('443b6618a8bfc70e30e8c6ca6dae192a614f24c0c23eaa549b5e0bcbf399b215a74d4358dff2f8b3', 6, 3, 'Api access token', '[]', 1, '2021-08-03 01:46:45', '2021-08-03 01:46:45', '2022-08-03 07:16:45'),
('443d960ccc90bc780287bbc80755cd49c620a3f71c9fff4af457121e4649c1af46db46f29a84c55a', 1, 3, 'Api access token', '[]', 0, '2021-08-17 23:28:31', '2021-08-17 23:28:31', '2022-08-18 04:58:31'),
('447fe9782d1d99343912109183f563b4156c18ddac4c1bf3523b8326455cd18d14dcf85c20fde479', 495, 3, 'Api access token', '[]', 1, '2021-04-01 00:23:05', '2021-04-01 00:23:05', '2022-04-01 05:53:05'),
('448ba4110e5a6848998bb20a357d68fd784b7ca8fcaa2fde26916dc361cef17d38cf6cde89b521d3', 407, 3, 'Api access token', '[]', 0, '2021-03-18 06:49:38', '2021-03-18 06:49:38', '2022-03-18 12:19:38'),
('451400970e82c9b434bc8fa7429bca87ee8a8fc22b86752318427c1f9c8ca37cf41250746fe6af5b', 457, 3, 'Api access token', '[]', 1, '2021-03-24 02:49:08', '2021-03-24 02:49:08', '2022-03-24 08:19:08'),
('451d139344a4cfc536841daeaf9e76344d0ba1eb9b169de5e9a5c43d9ad24501aa9fd7225c0de462', 1, 3, 'Api access token', '[]', 0, '2021-07-29 23:29:18', '2021-07-29 23:29:18', '2022-07-30 04:59:18'),
('451ee287c104879d8497446e4d477615be075dcd5f17d77b81497bf88f195819eb2143edb241998e', 40, 3, 'Api access token', '[]', 1, '2021-07-29 01:27:58', '2021-07-29 01:27:58', '2022-07-29 06:57:58'),
('452d228bdba32a6411b020d8a52a26f27c3076227a4a1a830e17c558951f34443b0ed663de48a098', 407, 3, 'Api access token', '[]', 1, '2021-03-18 06:37:49', '2021-03-18 06:37:49', '2022-03-18 12:07:49'),
('452edb6c5580f747f5273175a9a0be7284a7f5a8e419883ba698b9f06469a912f562a8d8eb47b7df', 572, 3, 'Api access token', '[]', 1, '2021-04-20 00:58:42', '2021-04-20 00:58:42', '2022-04-20 06:28:42'),
('456c355e93bfe3f2180bce9fff6de95b9d1998459842bdd0cdd8d7790040afab4cc5b845017bd940', 547, 3, 'Api access token', '[]', 0, '2021-04-09 12:57:13', '2021-04-09 12:57:13', '2022-04-09 18:27:13'),
('459485def39ca59769afe6487a97a61dd3db12e67544a414f12d5c5e800ca25ae4e922060a417045', 26, 3, 'Api access token', '[]', 1, '2021-07-28 06:49:04', '2021-07-28 06:49:04', '2022-07-28 12:19:04'),
('45a27f6853dc134fa2cba9e72f629e6dcebd60647ef93fdb913f629a0170f56979e98eb11af2c4d2', 665, 3, 'Api access token', '[]', 1, '2021-07-02 04:27:38', '2021-07-02 04:27:38', '2022-07-02 09:57:38'),
('45a81e140aa9c069714efe2d191999071ae3d31ab6be21f4260c2af5d2a0e07b7481d6bdb0b3a9ab', 551, 3, 'Api access token', '[]', 1, '2021-04-14 00:05:51', '2021-04-14 00:05:51', '2022-04-14 05:35:51'),
('45b2e4e71e0419442ed90192c80bba8a3b5643e3110b7edd8fae633d61ac0b6d55876692bc28c427', 773, 3, 'Api access token', '[]', 0, '2021-07-20 06:33:15', '2021-07-20 06:33:15', '2022-07-20 12:03:15'),
('45b449bf54b53e827c29752bc7528d21592a4ddcfb56537688fc7ea34df61f9f155f5a95b5650377', 454, 3, 'Api access token', '[]', 1, '2021-03-25 04:47:32', '2021-03-25 04:47:32', '2022-03-25 10:17:32'),
('45fde4659fd53d68f97a7f7c1b15b9bdf50f1ab9aca756d437deffac861aeca5a97e145bd8b27aae', 668, 3, 'Api access token', '[]', 0, '2021-06-11 07:47:17', '2021-06-11 07:47:17', '2022-06-11 13:17:17'),
('4605530effe284ffbdc7f572c5562dd51e206e1e4015333cc6965db32d734695996a464cb1d4f6e7', 533, 3, 'Api access token', '[]', 0, '2021-06-08 08:25:47', '2021-06-08 08:25:47', '2022-06-08 13:55:47'),
('461814cd19a7c3cd640b259a88ccc03f327eff6e698045a2e08a63ab00d32b0eeb697cb2c2f9808b', 770, 3, 'Api access token', '[]', 0, '2021-07-16 00:12:21', '2021-07-16 00:12:21', '2022-07-16 05:42:21'),
('46365e552ce1c9ccfebe4296eb921b95b04d8250236ea8ae40b66504061fafd50a42c42b907bf131', 19, 3, 'Api access token', '[]', 1, '2021-07-27 07:23:38', '2021-07-27 07:23:38', '2022-07-27 12:53:38'),
('4661cbc073742c830eacbac86cb9dae62bb5923d4f3f04e81924de7a166f4cf84823f2081815ca78', 533, 3, 'Api access token', '[]', 0, '2021-06-08 09:20:47', '2021-06-08 09:20:47', '2022-06-08 14:50:47'),
('4696ad8d354ea1dafdde7c3c58d15dc9e4b75b8799bc15c454f6806a5896413ecd47a7971e473446', 596, 3, 'Api access token', '[]', 0, '2021-05-02 07:40:11', '2021-05-02 07:40:11', '2022-05-02 13:10:11'),
('46b5af420bdcd1653086933ce41a1e30b4ae8eaefaae19f5c3e43efb546316c37b0125a810154f44', 438, 3, 'Api access token', '[]', 1, '2021-03-23 05:36:35', '2021-03-23 05:36:35', '2022-03-23 11:06:35'),
('46cae9429fd91c6fa62436633cef22f299bce93f77fd828906349751b469705f6b7e0f46a2dce16f', 528, 3, 'Api access token', '[]', 1, '2021-04-08 05:07:05', '2021-04-08 05:07:05', '2022-04-08 10:37:05'),
('46f0f559fd2b906457cd91516c8e0f2145b6add8dd266b6425b6ccbbd480cd261ed1dd9af89a033d', 722, 3, 'Api access token', '[]', 0, '2021-06-10 01:41:24', '2021-06-10 01:41:24', '2022-06-10 07:11:24'),
('46fa3d4988342fc117eeb85a7063b59687e5b8bc2ee02f4f6d81755de116050f9c55e97a41dadfd5', 4, 3, 'Api access token', '[]', 1, '2021-07-26 04:51:07', '2021-07-26 04:51:07', '2022-07-26 10:21:07'),
('46fda0997e9a70257f637b4dcbf542ebee3aed93a1946371179f33823255c8bab7bd6434104fb6f3', 36, 3, 'Api access token', '[]', 0, '2021-07-28 07:30:06', '2021-07-28 07:30:06', '2022-07-28 13:00:06'),
('4706cd38b205f615872bf988911cc0d064ec5f72732085656ce7e99a14aa00bd4c36160d3156fbb9', 682, 3, 'Api access token', '[]', 0, '2021-06-02 07:52:43', '2021-06-02 07:52:43', '2022-06-02 13:22:43'),
('471e89e4364d65828f41c5a0debc1cda6554a44422abe822af9a59119be367d58217fe087aa3b152', 520, 3, 'Api access token', '[]', 1, '2021-04-02 07:27:06', '2021-04-02 07:27:06', '2022-04-02 12:57:06'),
('473298eaecb64113a4d58ff2a23a65f93e0945f78473211f1058ff73dd245ed0687bec55e87744c0', 64, 3, 'Api access token', '[]', 0, '2021-08-16 00:48:27', '2021-08-16 00:48:27', '2022-08-16 06:18:27'),
('4771cfc68df024a1b1482cc9add085c9a83216884107aee1f29dda96e2be796aadff426c694537e4', 579, 3, 'Api access token', '[]', 1, '2021-04-29 00:29:40', '2021-04-29 00:29:40', '2022-04-29 05:59:40'),
('47729fd0fd02d1295f11ae5d3384708c06b5d1765dad90fed9aa2add727ebc4a1b0e6e9256a41611', 660, 3, 'Api access token', '[]', 0, '2021-05-29 00:57:33', '2021-05-29 00:57:33', '2022-05-29 06:27:33'),
('4780c7eb2f1bef2a94dd5d84b93a7bed3a2cce52351fc03cd0d34958657d018bb674aaab4db98f96', 449, 3, 'Api access token', '[]', 1, '2021-03-23 05:15:28', '2021-03-23 05:15:28', '2022-03-23 10:45:28'),
('478785c3f0ab6527c2acae3d8d17585caf1040529294664f5b85db58bb434253fa30162eb2dda93e', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:19:11', '2021-07-28 06:19:11', '2022-07-28 11:49:11'),
('47c1fb6a07919767a322bf3db060e3350e367c6e07e07a8e6be09015897961adac04de8def6b5964', 534, 3, 'Api access token', '[]', 1, '2021-04-06 07:46:19', '2021-04-06 07:46:19', '2022-04-06 13:16:19'),
('47e3c745b04c2e40a8e50d34dcf0d5a7b749338e6430cbab0936f0e202087849f02c66d98eebc401', 779, 3, 'Api access token', '[]', 0, '2021-07-21 01:04:52', '2021-07-21 01:04:52', '2022-07-21 06:34:52'),
('47ee3c7e7e97aa22a0deec71f625e0616e7a354c8547b2e9d9bb1abbecd63a4e9062ea4678bc3b87', 538, 3, 'Api access token', '[]', 0, '2021-04-05 02:55:26', '2021-04-05 02:55:26', '2022-04-05 08:25:26'),
('47f6132dd7ddb0ab87d1fff65568fecf206af5c74ada5a9dc425fbac5b8ff9e3023421f147c35aad', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:17:54', '2021-07-26 07:17:54', '2022-07-26 12:47:54'),
('4811900484d9235a95617361a54eca10afe3d583b650baeee5d3635d8e487f879d29e15686513669', 561, 3, 'Api access token', '[]', 0, '2021-04-14 05:30:27', '2021-04-14 05:30:27', '2022-04-14 11:00:27'),
('481e4adf146224a7e2f724040b969ab00f71af994f429f250069f1a9c4ec331fc10d82206f5a574a', 632, 3, 'Api access token', '[]', 0, '2021-05-09 04:30:04', '2021-05-09 04:30:04', '2022-05-09 10:00:04'),
('48812c5fd16104f67d06135ed36f76bd26345a1c32d722c56f48ab0fc494023a45d56449bcd6ed07', 512, 3, 'Api access token', '[]', 1, '2021-04-02 06:25:03', '2021-04-02 06:25:03', '2022-04-02 11:55:03'),
('4887635e51c382d025516831b0870a4b86248249744b9e67f3cb6e7752265451d497958b4b2b6f07', 396, 3, 'Api access token', '[]', 0, '2021-03-18 01:06:07', '2021-03-18 01:06:07', '2022-03-18 06:36:07'),
('488e1bbcf6091758f00ea82625fd745b400c8313078e6705dcb143c0e4e0118acd11556aa8c76502', 409, 3, 'Api access token', '[]', 1, '2021-03-18 07:17:10', '2021-03-18 07:17:10', '2022-03-18 12:47:10'),
('48944e1681d2e529dff8f7173919bec2a4197a614146c606fd05860ea9bfaef0f7531241dfea4059', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:08:16', '2021-06-15 01:08:16', '2022-06-15 06:38:16'),
('489665a3fd7dd2a3382f249fbc4951d8c8006dadba3724e8e5c82d15df82684cbddbf216d8ce1197', 7, 3, 'Api access token', '[]', 1, '2021-07-29 00:42:32', '2021-07-29 00:42:32', '2022-07-29 06:12:32'),
('48e2c20c1a1be0fe6afdd9d9b4c3a8b72a83872e280c684a0aaba629203503d13b6f23451137b7eb', 522, 3, 'Api access token', '[]', 0, '2021-04-03 00:04:37', '2021-04-03 00:04:37', '2022-04-03 05:34:37'),
('48efacb08ec79b238f2e521137377936ae84aba751db2a2b78d18ea041fa41c88ce6102d374c1eca', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:37:28', '2021-05-27 04:37:28', '2022-05-27 10:07:28'),
('48f249bc0b1ba49257b187c584b07cddc103450f34c7b3ce5c26a16f08cb14d4aee87479946657f6', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:45:57', '2021-05-08 02:45:57', '2022-05-08 08:15:57'),
('4904f40a3c9f722f0199e7fd220e534a5dbdc75086d108f11f970329449486dc95fffc4a96fb49c8', 778, 3, 'Api access token', '[]', 0, '2021-07-21 00:28:28', '2021-07-21 00:28:28', '2022-07-21 05:58:28'),
('491036adf64e3fa21e61d9e8b3b49f32b587459756541527d3308a60d6d2ea90205d96dede7f2fc4', 4, 3, 'Api access token', '[]', 0, '2021-08-18 05:24:28', '2021-08-18 05:24:28', '2022-08-18 10:54:28'),
('493187c44a6da523d962574c2c499b08c7eff98315258114ac33ef0436ffcf7209698d4d835b9c77', 405, 3, 'Api access token', '[]', 0, '2021-03-19 02:53:52', '2021-03-19 02:53:52', '2022-03-19 08:23:52'),
('4935ef3338bbaa3c0512dea26d2e6a062c6471b0eab82024899026670d5f691081ae67c9708af557', 780, 3, 'Api access token', '[]', 1, '2021-07-21 01:33:17', '2021-07-21 01:33:17', '2022-07-21 07:03:17'),
('4936f1b2a091de7b59fe144ea5afb43f45321a53c4c38415d381565329cd937b51d2e1ac7d73abab', 647, 3, 'Api access token', '[]', 0, '2021-05-14 05:31:34', '2021-05-14 05:31:34', '2022-05-14 11:01:34'),
('49391adb80a179d4799ab91657e67600ad071dc77a0fcbbd36a8a389eaca7c955b76163485c2e24e', 662, 3, 'Api access token', '[]', 0, '2021-07-08 04:40:09', '2021-07-08 04:40:09', '2022-07-08 10:10:09'),
('493e50833af50ec3e93d789fca96522393a6546789ac46b167346deffeb3bbfd1e4d88ae37d879e5', 16, 3, 'Api access token', '[]', 0, '2021-07-27 04:33:31', '2021-07-27 04:33:31', '2022-07-27 10:03:31'),
('4949137f4904d52db65036acfa6988cfe3e1315ce77f88b2aa786eeca21128bc3fe13f50a894df97', 773, 3, 'Api access token', '[]', 0, '2021-07-20 06:54:53', '2021-07-20 06:54:53', '2022-07-20 12:24:53'),
('4958be0935f85d5272041ba231e873dbe91150662e5bb8c279401f320f5a1c78f05e6faef559e597', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:42:57', '2021-05-12 04:42:57', '2022-05-12 10:12:57'),
('4971bb9a99bb87631ff92960c405522074a9ebbac2dffc86263b2e29801bf9f1e1a892e610a04b78', 623, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:53', '2021-05-04 01:11:53', '2022-05-04 06:41:53'),
('498b00ab6896f6fc3ea6d2420382b3bfd6ba34c8d6ce61283e4366cc5488b1363bff8bcb85409ec4', 393, 3, 'Api access token', '[]', 0, '2021-03-19 04:36:04', '2021-03-19 04:36:04', '2022-03-19 10:06:04'),
('49f2ff580e9908540b4410b2438f7ec92d4537e100cd6c327162fd400c5bf78ef73b8faee0845084', 23, 3, 'Api access token', '[]', 0, '2021-08-05 06:56:21', '2021-08-05 06:56:21', '2022-08-05 12:26:21'),
('49f7a505d7bda721b5bc20625fe06aaf476f0bbaa136b38a2940166d67b49df26843e3b67a1c232b', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:13:42', '2021-05-12 09:13:42', '2022-05-12 14:43:42'),
('49ff3e55800d073c163456b485fab77ad6ac29981c9cad481d5cf1f4c6e5b8bc1d1b0d2c19b7c9b2', 621, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:46', '2021-05-02 08:41:46', '2022-05-02 14:11:46'),
('4a12a492ca5217f18276319f028039aa6c747d1fad96b6c2b1ead30f267da21204b1f1d4c57f4434', 623, 3, 'Api access token', '[]', 0, '2021-05-13 02:29:00', '2021-05-13 02:29:00', '2022-05-13 07:59:00'),
('4a191db36f8d48fd519956eb73745bc622cc61cd9923536cef376acc394e42feff3549df4183b936', 639, 3, 'Api access token', '[]', 1, '2021-05-12 05:42:17', '2021-05-12 05:42:17', '2022-05-12 11:12:17'),
('4a34813f769fd007cbbd4d5f34db1bf0caf1ade1cd591b1dc21b9d01c9c70b38ed68bb144575b848', 13, 3, 'Api access token', '[]', 0, '2021-07-29 02:13:38', '2021-07-29 02:13:38', '2022-07-29 07:43:38'),
('4a3c60771aa9e232b8831ccc775d12084179cccc2bf1976f04a76534bbfbe46d043201907932c2bd', 759, 3, 'Api access token', '[]', 0, '2021-07-20 08:50:57', '2021-07-20 08:50:57', '2022-07-20 14:20:57'),
('4a4ba5b4e16e679121e0dad8d4bc70ed8ed2cd4eb8f65debe649b1114b4dbb984ce276de1754db75', 68, 3, 'Api access token', '[]', 0, '2021-08-18 05:10:18', '2021-08-18 05:10:18', '2022-08-18 10:40:18'),
('4a8500498028eb0b998ed008ffc4e044df98f80de11ae36ba2924d371357d1a0dd485d52657e9b0d', 67, 3, 'Api access token', '[]', 1, '2021-08-17 06:12:21', '2021-08-17 06:12:21', '2022-08-17 11:42:21'),
('4a8eb76f436af20fc739466407a747baceb5996996ff781014fe61e6dd21dba7fd1125c8448bb186', 669, 3, 'Api access token', '[]', 0, '2021-06-02 00:04:14', '2021-06-02 00:04:14', '2022-06-02 05:34:14'),
('4aa54e12b0a1b6344c348c5b8fea168542df32399e388ca962db3a46302f97616e4ead05ded6557b', 621, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:38', '2021-05-04 00:40:38', '2022-05-04 06:10:38'),
('4aa67eeb8f009c459b61121cd14430e1b07bbe78d43e38cbf98e9ca88c0ae01c4f58eb1a73a372cb', 626, 3, 'Api access token', '[]', 0, '2021-05-13 01:38:44', '2021-05-13 01:38:44', '2022-05-13 07:08:44'),
('4b127193b17573fe32f4f1d5bfffb091d5565ed1c6b817fe1e8a890056721a2869c11970d1cbd64d', 483, 3, 'Api access token', '[]', 0, '2021-03-26 05:04:59', '2021-03-26 05:04:59', '2022-03-26 10:34:59'),
('4b764fb98e3d30f685968e58731e381eb97bb658211ce727c93258e001faec9a1f73c69d9e2ef92b', 17, 3, 'Api access token', '[]', 0, '2021-07-29 06:54:48', '2021-07-29 06:54:48', '2022-07-29 12:24:48'),
('4babe8504685ff12675d5b891a295cf876d64064864b99fb29df6c041088ab1036dacd258d42a0d9', 574, 3, 'Api access token', '[]', 0, '2021-04-20 01:06:26', '2021-04-20 01:06:26', '2022-04-20 06:36:26'),
('4bbf54d91f0d753d59fcb5c362180b9e70d451d80826b2be3241caa49a4213b6f43e0a0d3bd40675', 7, 3, 'Api access token', '[]', 1, '2021-07-29 00:21:38', '2021-07-29 00:21:38', '2022-07-29 05:51:38'),
('4bc60d375a2c9205a2c94ef38901b1b74f3da0d2b4b101708d4b571f715a11c4df0f8b8a05d4e3ec', 431, 3, 'Api access token', '[]', 0, '2021-03-23 00:49:45', '2021-03-23 00:49:45', '2022-03-23 06:19:45'),
('4bda031ab2da86f61cc7e82a2f5159d278a6eb5eb67c4b035c79fc830ef4cca618cfb7a6785fdcce', 764, 3, 'Api access token', '[]', 0, '2021-07-12 00:19:37', '2021-07-12 00:19:37', '2022-07-12 05:49:37'),
('4bea954883351c4c25e1015ad35ad994ef33151d481c7b085a59643559e1bece1beb82849ccc08e1', 711, 3, 'Api access token', '[]', 0, '2021-06-08 09:31:06', '2021-06-08 09:31:06', '2022-06-08 15:01:06'),
('4c2aa8891347896e3e9bd89596c446c62f17c64ddb396fa8fa95d067f580333443eb73c8f59931e7', 9, 3, 'Api access token', '[]', 0, '2021-07-26 04:53:57', '2021-07-26 04:53:57', '2022-07-26 10:23:57'),
('4c5313b8a56f6088a453a426e42778aec1f9878519965999dcbf33d73dec4b6f3cbc8221797cc156', 482, 3, 'Api access token', '[]', 1, '2021-03-26 04:58:43', '2021-03-26 04:58:43', '2022-03-26 10:28:43'),
('4c5a87e9056725c2814edab9460c6778565ea1d2380825708b7f734548fdf3b9e6658f3651fd5449', 555, 3, 'Api access token', '[]', 0, '2021-05-24 06:01:34', '2021-05-24 06:01:34', '2022-05-24 11:31:34'),
('4c6bd0af7a5ce09a82214258d6ac519decb6973ce52795c6b5373e60b11487b1c515f7198643ff1e', 704, 3, 'Api access token', '[]', 1, '2021-06-04 05:08:18', '2021-06-04 05:08:18', '2022-06-04 10:38:18'),
('4c70e81dd2b75c44f601590176e83eae0dbd146c6f5a0da2ee1b9bf61d1dc347eead0033998ba8cc', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:49:27', '2021-05-12 04:49:27', '2022-05-12 10:19:27'),
('4c8d63b36d7d093d44e5c6e7979cebe712b4dc2d72a6e76e417d53befc821a18761c2b022ebe5665', 7, 3, 'Api access token', '[]', 1, '2021-08-03 07:27:35', '2021-08-03 07:27:35', '2022-08-03 12:57:35'),
('4caba79e4b9daaafa40ad451767e832e9a303cfbdeebd6dd4e0ea4fc3456927584d38ef9ec3ac35f', 555, 3, 'Api access token', '[]', 1, '2021-04-15 05:54:54', '2021-04-15 05:54:54', '2022-04-15 11:24:54'),
('4caefb57a34d26fc98dc7214dff3ecf76c69eade7e18aeb921dab8d29a990c8bb12848189acd6b60', 639, 3, 'Api access token', '[]', 0, '2021-05-12 06:57:35', '2021-05-12 06:57:35', '2022-05-12 12:27:35'),
('4d06ff2b770d9c14d98f7f939aff12b7ed903791dcd7b85ddc90330c15dce47ed18fab39d906c1d6', 404, 3, 'Api access token', '[]', 0, '2021-03-18 01:05:17', '2021-03-18 01:05:17', '2022-03-18 06:35:17'),
('4d1c9332dbb0ecb0952fd1e0e6b9d589a3ad6ec3a713ae73b124cb789660a0d1a0ec35297f22e4db', 7, 3, 'Api access token', '[]', 0, '2021-08-04 00:42:53', '2021-08-04 00:42:53', '2022-08-04 06:12:53'),
('4d3262f103c9ee9d9569db7f0464f71618cf93bb8ec818ca0a24c0161488da691aa73f4c21a49e0a', 404, 3, 'Api access token', '[]', 0, '2021-03-18 00:41:01', '2021-03-18 00:41:01', '2022-03-18 06:11:01'),
('4d374ef0ee1c42fd3b3eed9ed2dbcf7fb19f956cd2c881b849820280b5fcfe91cba9c307a76e2cab', 471, 3, 'Api access token', '[]', 0, '2021-03-26 00:50:19', '2021-03-26 00:50:19', '2022-03-26 06:20:19'),
('4d429488559f1eb4e67f4d004e6942c0d7f7b69f5f938e4e103330c5268f616f0a269e855bca663a', 3, 3, 'Api access token', '[]', 1, '2021-08-03 01:47:11', '2021-08-03 01:47:11', '2022-08-03 07:17:11'),
('4d67ef12ca01c8fe75b8b89fc132df002894933c8ed8fb59aebbdb2ce896f7e1b28e0bc9ae1ab281', 1, 3, 'Api access token', '[]', 1, '2021-08-13 02:35:49', '2021-08-13 02:35:49', '2022-08-13 08:05:49'),
('4d6b888bb720ed371894f202c62f41dcdf646c6dd69977b612312a8adfe8a63698d549a2d9643c02', 17, 3, 'Api access token', '[]', 0, '2021-07-28 07:48:15', '2021-07-28 07:48:15', '2022-07-28 13:18:15'),
('4d957ed15d7b6cf7f6487f0d0927ed8a7dd97eeff807e34fa1b03d67ef36427bca9d677ef7251ce5', 7, 3, 'Api access token', '[]', 0, '2021-07-30 02:43:21', '2021-07-30 02:43:21', '2022-07-30 08:13:21'),
('4da6bfab1719b8941e818284eb238f6207df3523bec7da6824731f51ddee43622c596ca7cf735463', 539, 3, 'Api access token', '[]', 0, '2021-04-05 02:59:30', '2021-04-05 02:59:30', '2022-04-05 08:29:30'),
('4dc38685fe8f505b14a298cf9f215f852f4a3f347a7e267405fb5753d00a4f1bdd3e40162e443ab9', 428, 3, 'Api access token', '[]', 0, '2021-03-22 05:57:38', '2021-03-22 05:57:38', '2022-03-22 11:27:38'),
('4dc43de91fec073d063d94d7f612c77be38594c467a87b26d06426d123c8d704101cace11e9a2190', 17, 3, 'Api access token', '[]', 0, '2021-07-27 05:13:57', '2021-07-27 05:13:57', '2022-07-27 10:43:57'),
('4de8644569f983398983fab53eadb17022a77b43a2df207fb557dbd558946e375a1e70d3170e85f1', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:11:38', '2021-05-12 03:11:38', '2022-05-12 08:41:38'),
('4decd9fb42226b9c715838684c8ad6391ca3a91c23473259c2937fc7d8cd250f6c41454f37013317', 611, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:09', '2021-05-04 00:52:09', '2022-05-04 06:22:09'),
('4df952d22c5206ee796a17f5b2bbbb71eb07dc65e8cfd6a857bccde0ea0bb2ad966045f1b5dc052d', 623, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:08', '2021-05-04 00:52:08', '2022-05-04 06:22:08'),
('4e0f073025c19740080ccb93c682eb3b81fd3357140ca4e12523ffcec205eb5e2450620204afab43', 7, 3, 'Api access token', '[]', 0, '2021-08-03 23:29:31', '2021-08-03 23:29:31', '2022-08-04 04:59:31'),
('4e4399466fb29e9d98e1afe26cf7eafdda4c25355b5d0a39157f97d509c9bd61e898259a0de0fae3', 488, 3, 'Api access token', '[]', 1, '2021-03-31 04:32:22', '2021-03-31 04:32:22', '2022-03-31 10:02:22'),
('4e4ac6143cd19ffc233828e2c20a73f88eb22b7a2943b3bfaa3df192b0f09ff4cff7917ea06f32a6', 41, 3, 'Api access token', '[]', 1, '2021-07-29 07:57:54', '2021-07-29 07:57:54', '2022-07-29 13:27:54'),
('4e57ec1c4301547fe8bdc826e50ff9e54ee396bb6dcd38b66a487a5aad81ebb6327a5cd87309fb11', 398, 1, 'Api access token', '[]', 0, '2021-03-17 07:46:37', '2021-03-17 07:46:37', '2022-03-17 13:16:37'),
('4e9534e964706df11d9009b19dd866883a3e4c1d5176c819c4f45df20cd7fe95f3bc0e341ae9f1ac', 579, 3, 'Api access token', '[]', 1, '2021-05-06 01:54:19', '2021-05-06 01:54:19', '2022-05-06 07:24:19'),
('4ea93def347216b351eb5b9b0043dc3ae5fc21887e41fed2b848879392047914fe055250690cbaf7', 427, 3, 'Api access token', '[]', 1, '2021-03-22 05:50:54', '2021-03-22 05:50:54', '2022-03-22 11:20:54'),
('4ead5121ba106bae64cc5902dd9f039564f20abd077373ef22d11c3977c8feb062f700aa70ca1238', 533, 3, 'Api access token', '[]', 1, '2021-05-10 06:18:17', '2021-05-10 06:18:17', '2022-05-10 11:48:17'),
('4eb14727030902ba2ae9f5d9383b88f4c19ecf22add8562f9255cf019fd46027086eb7f44ef101b9', 398, 3, 'Api access token', '[]', 0, '2021-03-19 02:35:46', '2021-03-19 02:35:46', '2022-03-19 08:05:46'),
('4ece948d5596df6700c55bac0b6d406b1c88a4b631c1127b0194438766dd597bf85419d9066de55d', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:02:18', '2021-05-12 03:02:18', '2022-05-12 08:32:18'),
('4eda8d22836af4a9c7858b3f21fb6f2ee80638760194f2a9cc38d8b82b72314a87878deea3c2c24f', 717, 3, 'Api access token', '[]', 0, '2021-06-08 09:49:24', '2021-06-08 09:49:24', '2022-06-08 15:19:24'),
('4ef3608a66add41f1255d16e6f18e801c64bc75a7ede87f1b44d321b2cf1b889d1e4529a60ba908f', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:20:11', '2021-07-26 07:20:11', '2022-07-26 12:50:11'),
('4f1090b3d05eb50be196f66edae62cb9b0252caf57b5184354378b0cff1fc49d94367a015e44ca95', 19, 3, 'Api access token', '[]', 0, '2021-07-27 07:21:06', '2021-07-27 07:21:06', '2022-07-27 12:51:06'),
('4f32df924136e1f2ab87f5abba8cd0b36524b4ca8c48de788648e5ad31299247cc181350f9135d03', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:19:11', '2021-07-28 06:19:11', '2022-07-28 11:49:11'),
('4f8cdc75c2fd06c2cb862e9dafeb7b9c560365598dce23ba5f7b4cae0bac735830e56ebe301e776a', 665, 3, 'Api access token', '[]', 1, '2021-06-04 02:38:58', '2021-06-04 02:38:58', '2022-06-04 08:08:58'),
('4fab5256811c982640ea5572795b0f6ab507e81c5a0ef811ffa822a36ef5ad13b730eb32d247fa1a', 504, 3, 'Api access token', '[]', 1, '2021-04-01 05:39:31', '2021-04-01 05:39:31', '2022-04-01 11:09:31'),
('4fc9ec7dc64e9edb3338dfd9982d68ffe42562e7539d23a146654c839cdd548df21b685d86f72a06', 749, 3, 'Api access token', '[]', 0, '2021-06-17 08:41:05', '2021-06-17 08:41:05', '2022-06-17 14:11:05'),
('4fef91d24911fb2af746227ae9c7ff9a4b7993d2c1065698b007301882d3b6ecb2c064671c1bad81', 451, 3, 'Api access token', '[]', 0, '2021-03-23 06:02:38', '2021-03-23 06:02:38', '2022-03-23 11:32:38'),
('50358f5427e53499195a824e3150f31f30d3eb7c8644af156c12eb2834fd498b3502ad3f2e0538e9', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:09:56', '2021-07-20 02:09:56', '2022-07-20 07:39:56'),
('503b829a6b801f444ea25fd36bba09ff7e1d0896585ca466761c4d69f9cfdd570b23de293f394886', 670, 3, 'Api access token', '[]', 0, '2021-06-02 05:44:51', '2021-06-02 05:44:51', '2022-06-02 11:14:51'),
('5048a3297074a565b7ad0fce1dc7165c0e2114b87cd02220adb94bc0fdc78da295eb191c2ac0bbc8', 486, 3, 'Api access token', '[]', 0, '2021-03-26 06:45:55', '2021-03-26 06:45:55', '2022-03-26 12:15:55'),
('504ec978105b6a117ba46ef45d54c062d5a1326003330c31c90662e42cb2ca8c7929eac9f39c1829', 4, 3, 'Api access token', '[]', 1, '2021-07-29 05:06:07', '2021-07-29 05:06:07', '2022-07-29 10:36:07'),
('507a7407bc6202b76f21ad9d72f610af0817540fab68ace6c5c9fee0c0a195bf230506ac81eb3d86', 688, 3, 'Api access token', '[]', 1, '2021-06-02 21:23:07', '2021-06-02 21:23:07', '2022-06-03 02:53:07'),
('5094842b952e6fd26a43943f5dc205bb37a94f0f9757d3b4714addaed71e4fa4261df1e28f07faef', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:46:43', '2021-07-28 06:46:43', '2022-07-28 12:16:43'),
('50aecd34e50545b5633834f765d22c8ad3300cb0d4e22f60ee6f62f5e1bc13033505921d5a26086e', 611, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:54', '2021-05-04 01:11:54', '2022-05-04 06:41:54'),
('50d75f50b4f42a4fff05dee44de9a520a8e004db9759055f5220449dc9e6b9025ce83169f824f51e', 622, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:24', '2021-05-15 06:22:24', '2022-05-15 11:52:24'),
('5114829d7f84799c67ac95778840bd4ec10d62a0d2a59025dfc2d27aff23c3ab344c2914a5ad0382', 778, 3, 'Api access token', '[]', 0, '2021-07-21 00:28:25', '2021-07-21 00:28:25', '2022-07-21 05:58:25'),
('512093b09325ad4498d9aa743e636f372a8b9303152ffdeff91999f14703f5f8471bab660b2d7042', 759, 3, 'Api access token', '[]', 0, '2021-07-20 03:38:15', '2021-07-20 03:38:15', '2022-07-20 09:08:15'),
('517c410e56c0348039c8ba3ef9fd913b66f3d116bc1bbec85e84e44803dd710ea2636ac4e2216c36', 777, 3, 'Api access token', '[]', 0, '2021-07-21 00:23:02', '2021-07-21 00:23:02', '2022-07-21 05:53:02'),
('51afd95a168d68a31e6e00306b285f45455cd3410f24b8eebcc7f7da5ead97f9bbf231981260ea23', 719, 3, 'Api access token', '[]', 0, '2021-06-10 01:36:44', '2021-06-10 01:36:44', '2022-06-10 07:06:44'),
('51b080b947254bca55146ceaf402a1537d55be2d476d0385303efe65eb9c91ea3b283558d3b3705b', 618, 3, 'Api access token', '[]', 0, '2021-05-08 00:38:29', '2021-05-08 00:38:29', '2022-05-08 06:08:29'),
('51bf74fd6935f2c5c7d83fd48d01c392ef32b8fd7ca8a2bfd1176cfabe5f3c14800a9e08be990f35', 1, 3, 'Api access token', '[]', 0, '2021-08-13 04:17:38', '2021-08-13 04:17:38', '2022-08-13 09:47:38'),
('51d939ad52ed620147a1b5870e17f312329db09c2998418902ec068da3e3831a8dc967595bc0234d', 735, 3, 'Api access token', '[]', 0, '2021-06-15 00:57:13', '2021-06-15 00:57:13', '2022-06-15 06:27:13'),
('524a6d5ea46779b13542f07aab2ac3940d65c4726d66fa6d81ffac9581abd20d6f4d7694f55e98c7', 466, 3, 'Api access token', '[]', 1, '2021-03-25 02:52:27', '2021-03-25 02:52:27', '2022-03-25 08:22:27'),
('5254f316e00d29459d551954ed2de9138eb9d78cc34b04b61fa04ccb9ca40d86b953fa21c06a2194', 486, 3, 'Api access token', '[]', 1, '2021-03-26 07:52:05', '2021-03-26 07:52:05', '2022-03-26 13:22:05'),
('5269328ce992b548a24486ebbbda2f817640a6d8d282ad809fc150990bf537468ccec2da5a812fd8', 449, 3, 'Api access token', '[]', 0, '2021-03-24 06:02:39', '2021-03-24 06:02:39', '2022-03-24 11:32:39'),
('526ebe31914e4a8b5eff4413a9e30ed1cbcdfae0412735a46b21641628b35b5cc204ad811149e98f', 495, 3, 'Api access token', '[]', 1, '2021-03-31 22:52:45', '2021-03-31 22:52:45', '2022-04-01 04:22:45'),
('52720b0a15c4fbf9b2f7c6ad18c7e96549e2e020ad1c0a9b0ac8999fab7711c5b29c2b9071b8070c', 517, 3, 'Api access token', '[]', 0, '2021-04-02 07:13:42', '2021-04-02 07:13:42', '2022-04-02 12:43:42'),
('527217516012b1b24813d05b971e98a6a0fa328370901e652392e32b0e047b2543ed26c48e55fc60', 562, 3, 'Api access token', '[]', 1, '2021-04-14 23:56:09', '2021-04-14 23:56:09', '2022-04-15 05:26:09'),
('527ce5f70e3e13852c4b71c13aae952c6cf723b2c87feab53e4dd3686eb8ecb5eb0e7d4b79aaac55', 24, 3, 'Api access token', '[]', 1, '2021-07-29 04:33:53', '2021-07-29 04:33:53', '2022-07-29 10:03:53'),
('529c642cc4449513272aeeee46fd82df6fda36e89b435a5d88117176763d98f07864aa7b02f75766', 612, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:45', '2021-05-02 08:41:45', '2022-05-02 14:11:45'),
('52beec3346f8c0fc0ecb50f44db7ea96f5c35c2ecb1a3acc76ab39d90ecc3aa058416deefd9db121', 1, 3, 'Api access token', '[]', 0, '2021-08-03 01:36:53', '2021-08-03 01:36:53', '2022-08-03 07:06:53'),
('52d06aaab39b7d6e04d0bdf1d817ff74445468d591fd59ee7bce1844fdf6d17dc35b8417e097f5e3', 773, 3, 'Api access token', '[]', 0, '2021-07-19 05:05:05', '2021-07-19 05:05:05', '2022-07-19 10:35:05'),
('52e29199e429d91e630432cccf8a5d019d57d3e6e8bf3fc6639538f92f28a9f7d884f75027134e79', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:51:01', '2021-05-12 02:51:01', '2022-05-12 08:21:01'),
('52fb74c9f86d267f1cf0f038e4d1927b8737998668735a8962d2885193ca79af89f4d3cbceaf2832', 5, 3, 'Api access token', '[]', 1, '2021-08-10 06:42:13', '2021-08-10 06:42:13', '2022-08-10 12:12:13'),
('532c50ee19830b28fd7b9f9ff0879a8ab7ed3a95f36ecbf204f29a96739601b30dae3ce73f14a603', 7, 3, 'Api access token', '[]', 0, '2021-07-30 05:28:43', '2021-07-30 05:28:43', '2022-07-30 10:58:43'),
('532dc44f0111291fe4b2cf108901a71fab147b88cf4579b29b7403b362abe5a371aba08672b8d87c', 426, 3, 'Api access token', '[]', 0, '2021-03-22 05:47:03', '2021-03-22 05:47:03', '2022-03-22 11:17:03'),
('53558211a38fe2d6109bd9d32f3bc6caf2333a806caf14432186c401b05970022922337121d76f82', 17, 3, 'Api access token', '[]', 0, '2021-07-27 07:24:42', '2021-07-27 07:24:42', '2022-07-27 12:54:42'),
('536936f106632308ab659e7d26da11723ec4a040fb30e557148c1c6ff6ba59b41618468a1725d024', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:34:23', '2021-05-02 08:34:23', '2022-05-02 14:04:23'),
('536ec7d901e2fe2bb262ab634842f475d98fabc1ad920ad0ffb8742672cc4d3882b31f51f83a0a67', 19, 3, 'Api access token', '[]', 0, '2021-07-28 04:49:09', '2021-07-28 04:49:09', '2022-07-28 10:19:09'),
('538033af6c7a8b5ce3677a72cbbce144a0ecc262d213193c7e17cd957ae6d0fa289267f80dc147ae', 647, 3, 'Api access token', '[]', 0, '2021-05-14 07:08:20', '2021-05-14 07:08:20', '2022-05-14 12:38:20'),
('5381643edf9a4852a3f06854034ea45646408eb0fa1818f0ae2b3d3bd097c850c62a249ac3dfa6ae', 770, 3, 'Api access token', '[]', 0, '2021-07-15 06:44:45', '2021-07-15 06:44:45', '2022-07-15 12:14:45'),
('538b80118631d86150db7b344b79a44dfddeffda36fb9f92abf773277d4521907851e4a030705bdb', 433, 3, 'Api access token', '[]', 0, '2021-03-24 04:18:57', '2021-03-24 04:18:57', '2022-03-24 09:48:57'),
('53a947c26e1068790e95c11fbc30e987f89d563dfb66fa9c098a8a8efbe1fe9d0c2cc6cefcf70dd9', 638, 3, 'Api access token', '[]', 0, '2021-05-12 07:38:27', '2021-05-12 07:38:27', '2022-05-12 13:08:27'),
('53e361b6a5503b1dc19399d316ee2234434e6a45594cb0c7efd20719df7e696e685b064c6553331a', 480, 3, 'Api access token', '[]', 0, '2021-03-26 04:27:33', '2021-03-26 04:27:33', '2022-03-26 09:57:33'),
('540d728f07ca03f32b982f17dfaa6312940c21cb087311b863a319d90aed17cc3d6f8dcc15296050', 38, 3, 'Api access token', '[]', 0, '2021-07-28 07:40:10', '2021-07-28 07:40:10', '2022-07-28 13:10:10'),
('54109a00a7371f646604a1d8a5814fe9c3fbaccaf11f9ba66cdf26494026f881ef1e34414aa8eb00', 692, 3, 'Api access token', '[]', 0, '2021-06-03 00:12:59', '2021-06-03 00:12:59', '2022-06-03 05:42:59'),
('543f05517fecd92592167517c79011fa8a59f3ca6d61fd9dd821da712d137f806243d668e37c4bb9', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:15:46', '2021-05-12 03:15:46', '2022-05-12 08:45:46'),
('54728f92d138731d6e8e9d421ea99dc42a3d85fb416e03866a27d3e092adc31915da0cfa795de97e', 664, 3, 'Api access token', '[]', 0, '2021-05-30 06:50:51', '2021-05-30 06:50:51', '2022-05-30 12:20:51'),
('548d620f5972dce37de6f3b9f7e661ec74c39ff375d536783e45da8c95a130282a6c5d46b0912634', 533, 3, 'Api access token', '[]', 0, '2021-06-08 08:09:17', '2021-06-08 08:09:17', '2022-06-08 13:39:17'),
('549f560989c757b789169f10e2953a6ee6cd28396affbb499a1a936c46fa9fe29ffd56adb9f321ae', 618, 3, 'Api access token', '[]', 0, '2021-05-08 03:30:28', '2021-05-08 03:30:28', '2022-05-08 09:00:28'),
('54e905ff5f28b4c76015446ff5a37901b53eccbbf577024fc3ebf9e5e242f758e5c4382a67c8e375', 758, 3, 'Api access token', '[]', 1, '2021-06-30 08:58:35', '2021-06-30 08:58:35', '2022-06-30 14:28:35'),
('54eff2055b2dbbec046a45114df4bfdfbba7df4fb3c39fa211f1a630e0b13b8e20afa534e12225a9', 555, 3, 'Api access token', '[]', 0, '2021-04-15 05:18:34', '2021-04-15 05:18:34', '2022-04-15 10:48:34'),
('552d145ca45da565de2b2ff35221b80878e3848d27a25c01dcbed40fd13f4b51f874c5d41555d823', 53, 3, 'Api access token', '[]', 0, '2021-08-05 02:54:24', '2021-08-05 02:54:24', '2022-08-05 08:24:24'),
('55581b050008f5d3c8995b50603c9563edb4bc05de81c38b6281080232de738d8a42614e0fdf853e', 612, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:16', '2021-05-15 06:22:16', '2022-05-15 11:52:16'),
('555b6ce88e6e0bbf446dc8e8c5a979633131ba650602e109c5c33486c19b385e90f68034b9e21b59', 5, 3, 'Api access token', '[]', 0, '2021-08-07 01:53:11', '2021-08-07 01:53:11', '2022-08-07 07:23:11'),
('557c32d6527380d93f76f9bd633d9ea4fbcca83e8bb5f77f9fec4027a336ab456314ac28185d7260', 697, 3, 'Api access token', '[]', 0, '2021-06-03 00:35:50', '2021-06-03 00:35:50', '2022-06-03 06:05:50'),
('55b04d0089f9bbffdb539920e0640147bfbd8f3d86aff69ee44a03bae0ccfee76d251e22e9fd08d5', 434, 3, 'Api access token', '[]', 0, '2021-03-23 01:03:45', '2021-03-23 01:03:45', '2022-03-23 06:33:45'),
('55c55ce086622b599dd2e8df443722fe795c3390f1b6124869ee8073ea34cbb1914693595a14492d', 743, 3, 'Api access token', '[]', 1, '2021-06-15 07:41:38', '2021-06-15 07:41:38', '2022-06-15 13:11:38'),
('55c56e8e65cdd21be6c4cf5489c7db974036c59f03c973c74b1073e653f00d33f6ec9520d8fe6e06', 727, 3, 'Api access token', '[]', 0, '2021-06-10 01:47:47', '2021-06-10 01:47:47', '2022-06-10 07:17:47'),
('55ffbd7092d0b42d6d09e2ecd6cd6728cc06d8003d371dded9f7295e836d938b6a33070422a0b231', 759, 3, 'Api access token', '[]', 0, '2021-07-01 22:24:57', '2021-07-01 22:24:57', '2022-07-02 03:54:57'),
('560bc908b02dadf9a6f95e5fdf40cd1e92e4338228360ba792b38dd997614b26dde8f14f1413dc07', 14, 3, 'Api access token', '[]', 0, '2021-07-27 03:04:30', '2021-07-27 03:04:30', '2022-07-27 08:34:30'),
('56263435ade03ff637abcdcaf997c5277054adca775756914657408ca9f68bbd2fa9b326b4ed6ed7', 524, 3, 'Api access token', '[]', 0, '2021-04-03 04:19:06', '2021-04-03 04:19:06', '2022-04-03 09:49:06'),
('563279f512a7aa464e27541c1fc3a5b6d4a0d91eaa2866d81657ef27f2c2b2d7de324d7575d4e749', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:03', '2021-05-04 01:23:03', '2022-05-04 06:53:03'),
('564aa679f30db4b862c0c673539f77730b32b180191b4fcfa7148bdab4f06d7ab10d29fa1368240d', 19, 3, 'Api access token', '[]', 1, '2021-07-28 05:05:25', '2021-07-28 05:05:25', '2022-07-28 10:35:25'),
('56734442b24dfea752b63a68bdfa546b6aa545566887ffcd4a60a3dba4aac327c5d2ece2638281a9', 623, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:22', '2021-05-04 00:49:22', '2022-05-04 06:19:22'),
('5683e315d6a571df28f60429a891c37c38789ef22e0e4facdc389b962d0e67e910340e1cd73962d0', 429, 3, 'Api access token', '[]', 1, '2021-03-22 06:04:20', '2021-03-22 06:04:20', '2022-03-22 11:34:20'),
('56cd298b1f511286aa610aa09dee85b7b462c373371c8364a0d0135cc617920c3bf335bdbdb00c87', 528, 3, 'Api access token', '[]', 1, '2021-04-06 04:20:59', '2021-04-06 04:20:59', '2022-04-06 09:50:59'),
('56ebcf3bd0e85f639f9e68e82ddd800da38f059bb473ba46ce8eddbac8892265243c054cee68a285', 492, 3, 'Api access token', '[]', 0, '2021-03-26 07:39:21', '2021-03-26 07:39:21', '2022-03-26 13:09:21'),
('56f134c2a5483f8f36b74f13c4f40c0e587d85c69a84d777d2d2f9e0a87154ef9cd3f6dd9a9dd0f8', 56, 3, 'Api access token', '[]', 0, '2021-08-06 23:49:38', '2021-08-06 23:49:38', '2022-08-07 05:19:38'),
('573ceb79943acaf43eab72ece018d4900251598b74f46c459d501ab18fd3d453a14465747b693cf4', 7, 3, 'Api access token', '[]', 0, '2021-07-30 00:40:00', '2021-07-30 00:40:00', '2022-07-30 06:10:00'),
('578bafa31c6c573d1b038c434e4fcc14e1e142c78570506eab30a0f432106446d7e57f3ad895438a', 625, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:27', '2021-05-07 07:04:27', '2022-05-07 12:34:27'),
('57924aa14c783cfaed95bd85c0a33206e4f8be0366e644a7bcd3075a61632794d5ef5f3c755eeaa9', 516, 3, 'Api access token', '[]', 0, '2021-04-02 22:00:33', '2021-04-02 22:00:33', '2022-04-03 03:30:33'),
('57bdb39e000362ae11a0da86b35663cabf34d1f4e01cbdd6b160a6edb2c5a636ee45358885403ffe', 531, 3, 'Api access token', '[]', 0, '2021-05-13 09:56:13', '2021-05-13 09:56:13', '2022-05-13 15:26:13'),
('57e79101c25bf9f84000767f123707486d351cf7262023c6a3f2a316c06caa903949f18c0ac3d9bf', 764, 3, 'Api access token', '[]', 0, '2021-07-11 23:50:19', '2021-07-11 23:50:19', '2022-07-12 05:20:19'),
('57ecab8547806099eac3a6e857ce9ae0626f8d5dfcf66c24597e88eb609616e990303288d78178c7', 25, 3, 'Api access token', '[]', 0, '2021-07-28 05:29:49', '2021-07-28 05:29:49', '2022-07-28 10:59:49'),
('58026934277668cd0a26103d1d8286459a67af2af694f5b58eb77c951de2216d7b2e5dd571e2a0b5', 489, 3, 'Api access token', '[]', 0, '2021-03-31 04:04:04', '2021-03-31 04:04:04', '2022-03-31 09:34:04'),
('5817eebe75a45f944883544bbf575905a230ea82d9ec1727952d2d80beae9f7cd2c9d9e31b6521d5', 563, 3, 'Api access token', '[]', 0, '2021-04-15 05:53:44', '2021-04-15 05:53:44', '2022-04-15 11:23:44'),
('589e133dfd660345873376524c82f8d59da631cda5d2818c3e37cb921aeab185518c7eeebd93fb25', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:49:21', '2021-07-28 06:49:21', '2022-07-28 12:19:21'),
('590aec0578385d3e6eecc3bb592b111cec1acad5e497bdf98d0a52295445756b794cdb92e2e46a44', 434, 3, 'Api access token', '[]', 0, '2021-03-23 01:03:45', '2021-03-23 01:03:45', '2022-03-23 06:33:45'),
('594a9d596977a03e2701fdfd9d825e56f2c46e2f813d7fd4ec9e32e7bdbb82af3791fd10c4adf30d', 623, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:07', '2021-05-04 01:23:07', '2022-05-04 06:53:07'),
('59671b8e935b657f8810184ef9ed7f428435c05c10c4d800fa030b8e17d2190ed8c1a7b6597b21cb', 486, 3, 'Api access token', '[]', 1, '2021-03-26 08:07:02', '2021-03-26 08:07:02', '2022-03-26 13:37:02'),
('596d70e4e67e8694fe6af9fc7c308b5a4116f16ab77ef543a252616b31d9d3b9e03193c6a253e5a3', 611, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:32', '2021-05-07 07:04:32', '2022-05-07 12:34:32'),
('59796b85f2cb7d12375993bdc629af05c8a4f34bdb9d1a559730b416d1d2728ed9f6f0a5fb52d2e9', 397, 1, 'Api access token', '[]', 0, '2021-03-17 06:20:38', '2021-03-17 06:20:38', '2022-03-17 11:50:38'),
('597976302c63834d698bd26e8d2580b352ec5dbb3f476d048bb861083a546cf06b6efb8512b566c3', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:39:43', '2021-07-17 00:39:43', '2022-07-17 06:09:43'),
('59923559b272e60f9534cedba163509e5cf65c10d7001285c05c31b9af1a2b9268c4772647c934e1', 471, 3, 'Api access token', '[]', 0, '2021-03-26 00:24:19', '2021-03-26 00:24:19', '2022-03-26 05:54:19'),
('59969fd9f9a6ff31d2e37733678dab7a1d4a1639a94dd97e5570e64dbe1e6e82bbaf4232b9f4f723', 421, 3, 'Api access token', '[]', 1, '2021-03-22 05:23:47', '2021-03-22 05:23:47', '2022-03-22 10:53:47'),
('59a149b46bba5174bace5e57ca457959ba96d0c1d453e3cddb9eca0dd2c7b444d0f06e0349d53cfe', 1, 3, 'Api access token', '[]', 0, '2021-08-13 02:22:22', '2021-08-13 02:22:22', '2022-08-13 07:52:22'),
('59a5a98455d1e2a0553cdaea36c0eb0b548142742e501bec15b197d6f1fd2e7c47a10b682fadd362', 634, 3, 'Api access token', '[]', 1, '2021-05-12 02:23:59', '2021-05-12 02:23:59', '2022-05-12 07:53:59'),
('5a02f165c197aae8d4c34bb50a5c4db6189273de516e3e65ea67c3d48ea4874ba87a2d35025e6936', 521, 3, 'Api access token', '[]', 1, '2021-04-02 23:14:36', '2021-04-02 23:14:36', '2022-04-03 04:44:36'),
('5a21e6c4e2378fc7bc2ac3057ade60fb985e132a3cb40ba8659f7d380eafd11c49e38ca061d29941', 398, 3, 'Api access token', '[]', 0, '2021-03-18 01:53:18', '2021-03-18 01:53:18', '2022-03-18 07:23:18'),
('5a2876865477f00f1674d35a4a8e2158ea81e7ce922efacbacb305ad0244ee54d68d1ecf2d60ef02', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:37:43', '2021-05-08 02:37:43', '2022-05-08 08:07:43'),
('5a3334b749806f7c37de9fc83cd93c3234240578b5af65234318850f0f1a770ab44effce32ece087', 626, 3, 'Api access token', '[]', 0, '2021-06-04 06:49:50', '2021-06-04 06:49:50', '2022-06-04 12:19:50'),
('5a5d62f75330589bd181b083412f7c42c805661b2582d7a013c6003e7875a6909c59beebbcb65a29', 660, 3, 'Api access token', '[]', 0, '2021-05-29 00:50:33', '2021-05-29 00:50:33', '2022-05-29 06:20:33'),
('5ac788e034007a7bbe628fddff4324a6acc5eccc68ac8732e16f2ad2cff39c618f65dd94831d139f', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:39', '2021-05-04 01:27:39', '2022-05-04 06:57:39'),
('5ad830b3c0e45a7bb937decbe3fc76489f51e38a061180f932159351d5470b93972d6f25d8ffc937', 68, 3, 'Api access token', '[]', 1, '2021-08-18 05:10:18', '2021-08-18 05:10:18', '2022-08-18 10:40:18'),
('5adb0b1cacb713cec84eff339efb552694f490e2d50bf9fdf0b7cdcc91c8f47e106a356d34146d8b', 50, 3, 'Api access token', '[]', 0, '2021-08-03 05:32:02', '2021-08-03 05:32:02', '2022-08-03 11:02:02'),
('5ae316f8e09ccb7c702b38759e39cfae508f9b4b564bcd88d030a7acdf063b04f99637ec74be147a', 62, 3, 'Api access token', '[]', 1, '2021-08-13 02:19:06', '2021-08-13 02:19:06', '2022-08-13 07:49:06'),
('5b4f7de6117551a078fc426bddd070b46f0100dc0e2d8e297865aaa0e9ba7bc0e63678f3c7607c54', 67, 3, 'Api access token', '[]', 0, '2021-08-17 06:06:27', '2021-08-17 06:06:27', '2022-08-17 11:36:27'),
('5b677b35d954e9e79a44f340c24e3629c47177eb1384bc5e74b3b61e859d6760fd0676692a95b451', 773, 3, 'Api access token', '[]', 0, '2021-07-21 05:14:16', '2021-07-21 05:14:16', '2022-07-21 10:44:16'),
('5b67f149ba32b25543d9bb7d7fb592acc838d0889171f57378fd02a6a401baf51de622e0e6ac1672', 449, 3, 'Api access token', '[]', 0, '2021-03-23 05:15:28', '2021-03-23 05:15:28', '2022-03-23 10:45:28'),
('5b8bb8bed55fdb6aa9847bb7ef5bf4c7a3adc7638cdb838b7d4b1b8d2a3ad0b05eb95c0df2eb8feb', 10, 3, 'Api access token', '[]', 0, '2021-07-26 06:50:06', '2021-07-26 06:50:06', '2022-07-26 12:20:06'),
('5b8c5087b700fc7b2c03be7f3c0d7f927c15e0c3eba6be1d11928e2d1dcedadca41740f3f2f7d2e5', 485, 3, 'Api access token', '[]', 0, '2021-03-26 05:35:17', '2021-03-26 05:35:17', '2022-03-26 11:05:17'),
('5b8ef291e5910fe6ae3412179e6456878775ca5d9ec32f12247eb922f806737af3328d8c8c93eae3', 408, 3, 'Api access token', '[]', 1, '2021-03-18 04:30:17', '2021-03-18 04:30:17', '2022-03-18 10:00:17'),
('5ba0af5223dc06aa0c811b2ed6c59223a70c560469debf77cf83336fcef4c4804909a967074f0c19', 502, 3, 'Api access token', '[]', 1, '2021-04-01 02:29:40', '2021-04-01 02:29:40', '2022-04-01 07:59:40'),
('5bb1a3e72aa388b0abcf442e0376adea2941fc15af1f5990179b054b97e0b017cbaef8757a8d9ec8', 752, 3, 'Api access token', '[]', 0, '2021-06-24 06:49:49', '2021-06-24 06:49:49', '2022-06-24 12:19:49'),
('5bc8d3f802a816b9eed6156e1db1137e88a02a1dc411b1d5a1c7f932f82cf3b080a39abc8c04ed0e', 642, 3, 'Api access token', '[]', 1, '2021-05-13 02:17:44', '2021-05-13 02:17:44', '2022-05-13 07:47:44'),
('5bdbfaa89a08ba44c4279348b51817ec98d1c612b219c9724e2724a32bdea483a4e0f313ce480957', 401, 3, 'Api access token', '[]', 0, '2021-03-17 23:26:41', '2021-03-17 23:26:41', '2022-03-18 04:56:41'),
('5c0d72678d7ff342d8d59bb9a0a84828abe0579987e8f4b81264e569cc35d167d36599b4ddbe57a9', 415, 3, 'Api access token', '[]', 0, '2021-03-18 23:45:19', '2021-03-18 23:45:19', '2022-03-19 05:15:19'),
('5c2a4cabf354a8d84bc65227f8a5e8176f6ff98b14ec72a16dc4b5c9a646d5af6ef6f582eb3cf8e0', 573, 3, 'Api access token', '[]', 1, '2021-04-20 01:04:01', '2021-04-20 01:04:01', '2022-04-20 06:34:01'),
('5c46a962ee5856b8621a014b63ab4119e3fa8e0b3ebccd9a6946c6262c166d7078645e9af9084d36', 529, 3, 'Api access token', '[]', 0, '2021-04-03 06:35:51', '2021-04-03 06:35:51', '2022-04-03 12:05:51'),
('5c510a640fcdfb2e7ab2cda57f21da049f51307abedcdad4d64148cd516ea7a8fafe5cc1d6ae2d05', 542, 3, 'Api access token', '[]', 1, '2021-04-05 05:15:43', '2021-04-05 05:15:43', '2022-04-05 10:45:43'),
('5c5d6bc65c6905fe83c9fdda2329395980531e752b2d0d635babecf1d9d531c455fe03c729da8b30', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:07:42', '2021-05-08 02:07:42', '2022-05-08 07:37:42'),
('5c98a47ff4d2954057f9937c7da4da50add4187e3c3ba3f329fb570fdb2c0da22a78270baf2c6b52', 734, 3, 'Api access token', '[]', 0, '2021-07-21 00:21:22', '2021-07-21 00:21:22', '2022-07-21 05:51:22'),
('5cbdb50ec2896fa82e96c23a13087d50d8c0917ad4d5a8ed053d3fbfbb666373d35566bc10124a57', 443, 3, 'Api access token', '[]', 0, '2021-03-24 22:55:37', '2021-03-24 22:55:37', '2022-03-25 04:25:37'),
('5ce157932a0d482e67ac0bc4dfe7d3e63c5fb1c60225b699fa4b114fd4e00b7edbddee0f4a013652', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:26:30', '2021-05-12 09:26:30', '2022-05-12 14:56:30'),
('5cfebf2e3a12b2cda5be460345eb2a736ccfd9fdce4fe40f700da8525a67ed7b53c43370bf087165', 626, 3, 'Api access token', '[]', 0, '2021-05-11 07:20:07', '2021-05-11 07:20:07', '2022-05-11 12:50:07'),
('5d0cee290b116442e539fe98c44456a1593c1681e9123a4fcfad31a747506c8dd32278e1f97f15f3', 531, 3, 'Api access token', '[]', 0, '2021-05-13 10:00:01', '2021-05-13 10:00:01', '2022-05-13 15:30:01'),
('5d4cc1a4a354738a7bdf082df226e95f9f2dad680c7128c9dbf7edb802fd683e7d3aa8f820584529', 753, 3, 'Api access token', '[]', 1, '2021-07-22 02:38:59', '2021-07-22 02:38:59', '2022-07-22 08:08:59'),
('5d975d53e5b7d74f7c68000c05a1ab5de5d667281d50d183b8957bab8238753269d6331706045769', 24, 3, 'Api access token', '[]', 1, '2021-07-29 07:10:55', '2021-07-29 07:10:55', '2022-07-29 12:40:55'),
('5da24e7a5a26cfffeb9acb0752e4793bd50bfdd9963ca2db8151dcf2062f5e6219d22ef660249a89', 4, 3, 'Api access token', '[]', 0, '2021-07-30 02:42:46', '2021-07-30 02:42:46', '2022-07-30 08:12:46'),
('5da454511417696e6e010529fe734c839e771c3e2b9dd2467e160b2aeda117e6c2f040f1153d0ab9', 428, 3, 'Api access token', '[]', 0, '2021-03-22 05:57:37', '2021-03-22 05:57:37', '2022-03-22 11:27:37'),
('5db862b396ab7e05165d2a882e25bb458ce14b9290bcacc9336303767b93bf177417418fd5471bae', 495, 3, 'Api access token', '[]', 0, '2021-04-01 04:28:47', '2021-04-01 04:28:47', '2022-04-01 09:58:47'),
('5dbec98e47a673c44d625a97730cad2914272dd65bd5a099f0ca2bb9e162fce59d6ea6ba586e412d', 773, 3, 'Api access token', '[]', 0, '2021-07-19 06:38:54', '2021-07-19 06:38:54', '2022-07-19 12:08:54');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('5dcb573cf079010efed5757cc40966102f106e1e961a43a50d69ee9e3bf8a01969a12685b0602957', 623, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:40', '2021-05-04 00:46:40', '2022-05-04 06:16:40'),
('5ddbdae5252872b8907107f0cde0e96ead4f03b4d26d7f10abd9ca57d3ac9700f4088f32128d9ecd', 481, 3, 'Api access token', '[]', 1, '2021-03-26 04:54:29', '2021-03-26 04:54:29', '2022-03-26 10:24:29'),
('5df2d35349d7db15963b35683ca1cdca7aed50441a51fda4386901c7f2f491c2ed69e734886e74d7', 618, 3, 'Api access token', '[]', 0, '2021-05-08 01:38:19', '2021-05-08 01:38:19', '2022-05-08 07:08:19'),
('5e0ba1829bfef015df14e355df96b769901156bcd3453ee86a296f4dfbad1ab8e0a49ffb919200b3', 637, 3, 'Api access token', '[]', 0, '2021-05-12 04:56:24', '2021-05-12 04:56:24', '2022-05-12 10:26:24'),
('5e13bb4f0a64bd571b8bda6c7d196fe5e5129f2fd3f466801dfc7d1931f963914e7ed9fbacda7ac5', 702, 3, 'Api access token', '[]', 1, '2021-07-21 00:01:51', '2021-07-21 00:01:51', '2022-07-21 05:31:51'),
('5e175d3944c80c05cceeb748fc48e895165c7148ae42aa7261bb657f1006472d3cfabd9625d9caa1', 535, 3, 'Api access token', '[]', 0, '2021-04-05 02:03:47', '2021-04-05 02:03:47', '2022-04-05 07:33:47'),
('5e2c49a80d1303c85645ba3e518a7e070780b790c8ac14061d090bfef437821a654f8a4fe286bea8', 12, 3, 'Api access token', '[]', 1, '2021-07-26 07:24:20', '2021-07-26 07:24:20', '2022-07-26 12:54:20'),
('5e2c5f2eb1b2c097bf9d52468983d2ab22b403a4fe0b9222912009cacfd08153b4c27807f0c154fe', 393, 3, 'Api access token', '[]', 1, '2021-03-18 07:36:41', '2021-03-18 07:36:41', '2022-03-18 13:06:41'),
('5e47aa9c46ad5d5f05c8f85c8f238291c41e53f51aed82fe44cb5139930aba68b1db2516f655ddf0', 13, 3, 'Api access token', '[]', 0, '2021-07-29 02:41:38', '2021-07-29 02:41:38', '2022-07-29 08:11:38'),
('5e4b3f797ed27bbe01fc7f9808e26e419b01a02a4ba214c38aa05ffef0960ce396160c267105ed8f', 584, 3, 'Api access token', '[]', 0, '2021-04-21 06:49:18', '2021-04-21 06:49:18', '2022-04-21 12:19:18'),
('5e5b6da611260fe27c402692f12fffac9e24db8f703fa6ed2a786a4552fd8e77c633e3f5a48514f1', 396, 3, 'Api access token', '[]', 0, '2021-03-18 07:33:59', '2021-03-18 07:33:59', '2022-03-18 13:03:59'),
('5e5ec1c0911682f71872a6abd32a5d472598425039045dc8d3cef9b9ce4e204238190690472f7453', 432, 3, 'Api access token', '[]', 0, '2021-03-23 22:56:08', '2021-03-23 22:56:08', '2022-03-24 04:26:08'),
('5e643b61d8947a40201578395c45069fd21e510151df8f68ad08b790556b54d23119154d2e67f9a9', 454, 3, 'Api access token', '[]', 0, '2021-03-23 07:22:44', '2021-03-23 07:22:44', '2022-03-23 12:52:44'),
('5e6534102142c55661d2da10794f9739e905e16a800db46008fde9bb822c44c424108571a4c18dc2', 751, 3, 'Api access token', '[]', 0, '2021-06-24 06:46:18', '2021-06-24 06:46:18', '2022-06-24 12:16:18'),
('5e7c7eab886a07f7521bb6268311999f59467650750041d5d05235e07a3850a7f78fb514c1a7a56c', 454, 3, 'Api access token', '[]', 0, '2021-03-23 07:22:44', '2021-03-23 07:22:44', '2022-03-23 12:52:44'),
('5e8a89e9c63b3d030b74642def46e4a18fca50afebe609e8cc9117c863c4e33fc8bad679ac108a12', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:40:07', '2021-05-12 02:40:07', '2022-05-12 08:10:07'),
('5ed65eee82b31dfde949029fe2fff3085a78bdbeccf62333064f86561c405655e42b588686e8df31', 5, 3, 'Api access token', '[]', 1, '2021-08-07 04:20:55', '2021-08-07 04:20:55', '2022-08-07 09:50:55'),
('5ef09fe1d855f24a14ba93ddb71530b519394c59eb09465adc6f0fa1d917114a11509c1f6b69d62d', 7, 3, 'Api access token', '[]', 1, '2021-08-03 01:43:55', '2021-08-03 01:43:55', '2022-08-03 07:13:55'),
('5ef416eab7df67ca65cfe9004865a63534062b575d763418bfc868002f67a22fbbdcdd0aed6bda67', 542, 3, 'Api access token', '[]', 1, '2021-06-16 23:35:52', '2021-06-16 23:35:52', '2022-06-17 05:05:52'),
('5f13eb39eab30c87e582a309b14b3d193e6750aff539c14c5d77bbdf6aafb3f6a81006d98d636b16', 5, 3, 'Api access token', '[]', 1, '2021-07-30 02:22:16', '2021-07-30 02:22:16', '2022-07-30 07:52:16'),
('5f1a65d28aef9aef8cc61f29c2dccf5cf9b2b1ce1cb47b873196297ccf37be8b5f7b79cf76cd29c8', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:26:11', '2021-05-11 06:26:11', '2022-05-11 11:56:11'),
('5f4cde52d459b7220eb67e5393adf4e30aec9c54daf30a14b62804aa7499e15cb82c60bfb93029de', 58, 3, 'Api access token', '[]', 0, '2021-08-11 03:47:28', '2021-08-11 03:47:28', '2022-08-11 09:17:28'),
('5f4f647076640f05b930424caa19e205f8ee6863adf7deb863f56da785e7c7ad818cd7f4906d0ea4', 393, 3, 'Api access token', '[]', 1, '2021-03-18 03:37:48', '2021-03-18 03:37:48', '2022-03-18 09:07:48'),
('5f603c1a36ef9efe4f19f8df7d75aa84ef5c46cb50e53b23cbd33c0001c47be6c229a71833cc10e9', 771, 3, 'Api access token', '[]', 0, '2021-07-15 09:13:33', '2021-07-15 09:13:33', '2022-07-15 14:43:33'),
('5f9f6fd34f7ed883b8da5c9b40ac9733a93f6d7ecdc2f66c5309f4a470f2a738e1f6d78a83787129', 7, 3, 'Api access token', '[]', 0, '2021-07-30 02:40:12', '2021-07-30 02:40:12', '2022-07-30 08:10:12'),
('5fd6a0b89755dcb3ca4fe47d3a5caf6f1ffa173e2541a790859d7f4abcdca0ce878222c265293d6f', 620, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:12', '2021-05-13 08:41:12', '2022-05-13 14:11:12'),
('5fe700902af2dc43d3a0dce4de84d50a45401cf1f3595c7a724aa6c14ab388a9bd4fbc021f378f53', 665, 3, 'Api access token', '[]', 1, '2021-06-04 02:02:38', '2021-06-04 02:02:38', '2022-06-04 07:32:38'),
('603811cb3718a8fc6a0abd0efbecd85036f428b73b2637ebe29d752ec3c6b0433f2c1d78eadd000e', 64, 3, 'Api access token', '[]', 0, '2021-08-16 00:48:27', '2021-08-16 00:48:27', '2022-08-16 06:18:27'),
('6064018483a1c283ba6609e7cebf0bc483ecb1c9c92c2795fd1fce51240bede0f9a49addfef8275b', 26, 3, 'Api access token', '[]', 0, '2021-07-29 02:07:28', '2021-07-29 02:07:28', '2022-07-29 07:37:28'),
('6091f9e394267c4da6973f40bf8eaa32e3a5076afadfe60b38c830a13ad1cab452a97a553f43bfd5', 770, 3, 'Api access token', '[]', 0, '2021-07-16 05:59:59', '2021-07-16 05:59:59', '2022-07-16 11:29:59'),
('60a53016fee0ea66b4916578707d4d49aa56d6b0627290a7ce1a6db61cbd997ea37ace7911bde475', 558, 3, 'Api access token', '[]', 0, '2021-04-14 01:37:20', '2021-04-14 01:37:20', '2022-04-14 07:07:20'),
('60bdb0700b2df60058f976efcbc0161326188e0badcdcfbc99a63f8984dd38db746dacf5f7a6b782', 52, 3, 'Api access token', '[]', 0, '2021-08-06 04:17:06', '2021-08-06 04:17:06', '2022-08-06 09:47:06'),
('6101b81eba17c120ef45f26c01f2848f24872dc4dbb36a3b58b68857024f55bb6b1a82fdf65c711e', 63, 3, 'Api access token', '[]', 0, '2021-08-15 12:15:45', '2021-08-15 12:15:45', '2022-08-15 17:45:45'),
('61331600556228f4b728aaed95fe4b517bb957b7cd80f2c4c6038a16e9a103fcbd6e568e85431b02', 398, 1, 'Api access token', '[]', 0, '2021-03-17 06:20:56', '2021-03-17 06:20:56', '2022-03-17 11:50:56'),
('614d27e888cdba8d5e01d5cd5c05cd067d07693c65e768c15273690de7b2365579ea8c8fce1da22c', 528, 3, 'Api access token', '[]', 0, '2021-04-03 04:40:55', '2021-04-03 04:40:55', '2022-04-03 10:10:55'),
('61638f71090b20ab15032b8150d453cf5d9e8ee6b8dac7e0ebd8e88cb52b3a494c0cfe50c76111e2', 479, 3, 'Api access token', '[]', 1, '2021-03-26 04:26:21', '2021-03-26 04:26:21', '2022-03-26 09:56:21'),
('6189df374ec2aac59e1d7ec76955ba0df6f64658e3c24fb0709b8a50af68ad5e1da5f0906516ea64', 52, 3, 'Api access token', '[]', 0, '2021-08-04 05:08:44', '2021-08-04 05:08:44', '2022-08-04 10:38:44'),
('61c202cd841a358a3439bc6fe81b449b953ec362876e7b17a5887c0913cae2d01c924a14111f7b6b', 543, 3, 'Api access token', '[]', 1, '2021-04-06 01:51:13', '2021-04-06 01:51:13', '2022-04-06 07:21:13'),
('61f02a4042796fc3137f266460818b702a9957d051c8dcd3a16ee85d6dedeea450c4d003813d37fa', 639, 3, 'Api access token', '[]', 0, '2021-05-12 05:42:19', '2021-05-12 05:42:19', '2022-05-12 11:12:19'),
('624cae28ef48c4907031cfcac8889f390e5371fa9409c0df54ddc8a4289752acac7442255b5f40ea', 7, 3, 'Api access token', '[]', 1, '2021-07-29 00:48:32', '2021-07-29 00:48:32', '2022-07-29 06:18:32'),
('6261da176444a09f238c9a7ec6473570d97e6f71e807345f09fcd371db757541a96cb299216108e7', 13, 3, 'Api access token', '[]', 1, '2021-07-27 04:27:22', '2021-07-27 04:27:22', '2022-07-27 09:57:22'),
('6268b9fcac82c7a7b745fd212f456dd17371b03e98e5d9964563224c6b12359c7b84efcb2fe45f6a', 408, 3, 'Api access token', '[]', 0, '2021-03-18 08:17:53', '2021-03-18 08:17:53', '2022-03-18 13:47:53'),
('62d822f79bbafc67629e46f95a065ddc912b70fafe28f58e7dd966b67e10329c6bcdb7d7465edc29', 620, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:25', '2021-05-15 06:22:25', '2022-05-15 11:52:25'),
('630e1e306671eb1ca3dac4b557a6d27a075658dfe135f842a21b7778ecbb945227dbcda08feddc58', 656, 3, 'Api access token', '[]', 0, '2021-05-26 05:16:33', '2021-05-26 05:16:33', '2022-05-26 10:46:33'),
('63403d622fda801cab6edc841352f194c3e501e6f16b5e123911b303367853740b2a3cec33f7f8d7', 504, 3, 'Api access token', '[]', 0, '2021-03-31 06:05:31', '2021-03-31 06:05:31', '2022-03-31 11:35:31'),
('636c48b557be839b4ee2a9ba7dbffcf61b727921a037f0e9a676f330bc18b43510c2055bea1fb9fd', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:43', '2021-05-04 01:27:43', '2022-05-04 06:57:43'),
('636f59370531ae508e9df6418b424d59ce16ba183089497bd0514df8286217d2c7f48c2bbe9f194d', 681, 3, 'Api access token', '[]', 0, '2021-06-02 07:52:42', '2021-06-02 07:52:42', '2022-06-02 13:22:42'),
('6378a5ba4273e50e5e17fdaad51946e23f6642c8ebb461a75b4c8ced1158a81e5ff18e85a93483aa', 552, 3, 'Api access token', '[]', 0, '2021-04-14 00:13:45', '2021-04-14 00:13:45', '2022-04-14 05:43:45'),
('63796892fb68c9baf81c37b93a515571b2d41a037ed89f61392d1711a616f532f83ac6fe8abab8b7', 665, 3, 'Api access token', '[]', 0, '2021-05-31 04:26:23', '2021-05-31 04:26:23', '2022-05-31 09:56:23'),
('6397ef6e54377cff95e73bdecf1b4ab46364f1191c0251324e250574a1f3f1149a11be2b143fd047', 740, 3, 'Api access token', '[]', 0, '2021-06-15 04:28:49', '2021-06-15 04:28:49', '2022-06-15 09:58:49'),
('6398a9fc4db23494c733696326552e1e0f14ba7f623ded9d64e7f9686ae683ef63420a363ff760bc', 6, 3, 'Api access token', '[]', 0, '2021-07-29 00:57:33', '2021-07-29 00:57:33', '2022-07-29 06:27:33'),
('6450ef62e3b2920f770a27060a3d7151ded9cd14be135d50b3b7175a877b4a6d34047e9ca048385a', 658, 3, 'Api access token', '[]', 0, '2021-05-24 05:12:43', '2021-05-24 05:12:43', '2022-05-24 10:42:43'),
('64623fa1d7dce9162c85447e1c7d1848be49edb1892bcef2dcbc8c7db86f751d31904ee708a53c3a', 531, 3, 'Api access token', '[]', 0, '2021-04-04 23:41:34', '2021-04-04 23:41:34', '2022-04-05 05:11:34'),
('6468f46eb0f16cc959ee23d358f6060a3fe75dd754ef1b41dcae233f44fdd0ef007785b243c50e80', 764, 3, 'Api access token', '[]', 0, '2021-07-08 07:26:08', '2021-07-08 07:26:08', '2022-07-08 12:56:08'),
('64753df9c666d32295bc8b111fb3ad33ac4edd327ff41962388fde9ce1505d373bea53eb5a71a11f', 756, 3, 'Api access token', '[]', 1, '2021-06-30 08:53:41', '2021-06-30 08:53:41', '2022-06-30 14:23:41'),
('647c392d5aa29f4559e53c2dcbab3377d36f410ee86043b0bfa2f8142ed4dddc7e43993d9b33b38c', 482, 3, 'Api access token', '[]', 0, '2021-03-26 04:58:43', '2021-03-26 04:58:43', '2022-03-26 10:28:43'),
('648bd941f3fcffff79cdbc10d2e0ee3131c82129e4cd003127d16098dbe98af98478b0faf6d5b4d0', 17, 3, 'Api access token', '[]', 0, '2021-07-28 07:52:09', '2021-07-28 07:52:09', '2022-07-28 13:22:09'),
('6492b9dbc037c0197b2d8098cdc4bb0b741befea76aee324606b80f1647a3daf19db6d29442fad5d', 579, 3, 'Api access token', '[]', 0, '2021-04-20 03:25:00', '2021-04-20 03:25:00', '2022-04-20 08:55:00'),
('64cf9bb3a6314487619b31e405939fdf00fdb8a569adb27ea49b07556ac4901fa661d9ccc0ed9a1b', 13, 3, 'Api access token', '[]', 1, '2021-07-27 04:28:36', '2021-07-27 04:28:36', '2022-07-27 09:58:36'),
('64ed24d840dfd4be3b1da823888d30f68a0b6c23f24234c76860bec9ea63662434e8117beaeec362', 663, 3, 'Api access token', '[]', 0, '2021-05-26 04:38:28', '2021-05-26 04:38:28', '2022-05-26 10:08:28'),
('6526b0767a6f9a723004449508987624e1bb2e290dff2b6a29a07b031f1288fc1ccfd99121fd7d9a', 662, 3, 'Api access token', '[]', 0, '2021-05-26 00:46:24', '2021-05-26 00:46:24', '2022-05-26 06:16:24'),
('653dc2d1ae2f405a7bed2b2091882376fab519df7afec686c6c206633b18d5e637a3b38d42fe38f7', 771, 3, 'Api access token', '[]', 0, '2021-07-15 09:14:21', '2021-07-15 09:14:21', '2022-07-15 14:44:21'),
('65570d2061350289140942a6f4ec22893b776af86b250bfb468ff45e3c99bb96f8e736c5f62391d0', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:19:10', '2021-07-26 07:19:10', '2022-07-26 12:49:10'),
('658ea428f50f3978dd7ae6feed56262acf956a7fb8ce88d93985a3ef0f813309ece1a062b92d0cff', 494, 3, 'Api access token', '[]', 0, '2021-03-30 00:37:08', '2021-03-30 00:37:08', '2022-03-30 06:07:08'),
('65e8339dd81fe98abb7dea47f15ade0ba9723560e5e44d2bf131156affbc4d0bce16db4afcd4d2f1', 553, 3, 'Api access token', '[]', 0, '2021-04-14 05:56:39', '2021-04-14 05:56:39', '2022-04-14 11:26:39'),
('661efbd20f1f6e65156b05dd18b5fd91d105f759188df10170bd2998550a50c4dee1a17b9a8bf2a3', 724, 3, 'Api access token', '[]', 0, '2021-06-10 01:42:50', '2021-06-10 01:42:50', '2022-06-10 07:12:50'),
('66447008a6cf2b8c32688084d53ec5c8e9605f0aa003dd36e840c1748b8690c57747ffa2b68e780f', 534, 3, 'Api access token', '[]', 0, '2021-04-05 01:40:51', '2021-04-05 01:40:51', '2022-04-05 07:10:51'),
('6647081103a2b967164a3b257854534f98f3cb4f00883867b20edd66a1996e10376551ba0b562a56', 683, 3, 'Api access token', '[]', 0, '2021-06-02 08:03:48', '2021-06-02 08:03:48', '2022-06-02 13:33:48'),
('6653c89bf2959c08dace4423f7e0505f578c59153282919dfe27317db5a0c76facf4c60999b0e2f5', 543, 3, 'Api access token', '[]', 0, '2021-04-06 01:47:39', '2021-04-06 01:47:39', '2022-04-06 07:17:39'),
('665b99e84e24c01458efd9255a2c87347931b8ebe9bfb4ad4619f20fc435c04570b3701d7707da97', 639, 3, 'Api access token', '[]', 0, '2021-05-12 07:03:46', '2021-05-12 07:03:46', '2022-05-12 12:33:46'),
('666b946820a3c9b7cd52f6efe7ec28e14c52fca8e531de86f685d69c95a2d3f828862920f77d367e', 507, 3, 'Api access token', '[]', 1, '2021-04-01 04:48:42', '2021-04-01 04:48:42', '2022-04-01 10:18:42'),
('667e669d93b3ceb4d6515f8e0564883346a246702ca387107bcb117b6c9e2389a05aa0479762abd4', 567, 3, 'Api access token', '[]', 0, '2021-04-15 23:33:53', '2021-04-15 23:33:53', '2022-04-16 05:03:53'),
('66da87b09f7d0efa87e828dd26390a6f9f9d1bf3c3ab274c16a9c5b355ab47e55fdfebc365a3a5ca', 433, 3, 'Api access token', '[]', 0, '2021-03-25 01:28:52', '2021-03-25 01:28:52', '2022-03-25 06:58:52'),
('66e1a30322b3bbdf9c6d010310f30722548d7a4f5eb5cd487af773c3dc6e32d6c331f60345cbed86', 456, 3, 'Api access token', '[]', 1, '2021-03-23 22:52:12', '2021-03-23 22:52:12', '2022-03-24 04:22:12'),
('6723164683138a6897ad6366edfd16849e2fac9d7f57928f92fa74062168d0f8139ddebe2714bc9c', 60, 3, 'Api access token', '[]', 1, '2021-08-17 06:07:32', '2021-08-17 06:07:32', '2022-08-17 11:37:32'),
('6741b0d87d37a1e5f2576d04ff21da2c2c26089dd79efbf80214f1d3df90ed1e333a54f5166eada9', 575, 3, 'Api access token', '[]', 0, '2021-04-20 01:57:48', '2021-04-20 01:57:48', '2022-04-20 07:27:48'),
('674c1641ed004266a81d48c35abe839a9e1eefe2815bc612eb07a295e31171ab0328e7bd81149839', 688, 3, 'Api access token', '[]', 1, '2021-06-02 21:20:14', '2021-06-02 21:20:14', '2022-06-03 02:50:14'),
('675823a526e64e18d3fd10c560b54bc0bb53ac7d60496db6b050bfa783a0d0a08adfa767be7921e9', 5, 3, 'Api access token', '[]', 1, '2021-07-23 02:29:25', '2021-07-23 02:29:25', '2022-07-23 07:59:25'),
('676e24f73d22dfaacffdfe9a7debe33fa2b4d6d954c15cc41c77f1309308497971ce23b4f068084b', 474, 3, 'Api access token', '[]', 1, '2021-03-26 02:22:27', '2021-03-26 02:22:27', '2022-03-26 07:52:27'),
('6777dc5f66bd9566c38e367b264f7dc2eac3232fa2e4e8be81c6d326bfff25435df87d51b3a3fa1f', 638, 3, 'Api access token', '[]', 0, '2021-05-12 07:54:46', '2021-05-12 07:54:46', '2022-05-12 13:24:46'),
('678f39bfe353171a71646092fc22bfabd49b0d69a35fbf8a3b0992b274fed289c57c2415f9c975cb', 626, 3, 'Api access token', '[]', 0, '2021-05-31 07:45:45', '2021-05-31 07:45:45', '2022-05-31 13:15:45'),
('67bd94dd25d1b4767dfc6923c473d920540b61326cf5153a3348fd6a199c42e4484e3cf6379d388a', 533, 3, 'Api access token', '[]', 0, '2021-06-10 02:02:26', '2021-06-10 02:02:26', '2022-06-10 07:32:26'),
('67d599a2281eed159de3899509d64f12945e1cad770d59d657a370872598de42118dfcba5a43e0eb', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:47:41', '2021-05-08 02:47:41', '2022-05-08 08:17:41'),
('67d92414ffb0ca7a0d66ab7e17cede6e5aa5a6fbc1e023aadf86f4c25bcb27856d8589984477925c', 779, 3, 'Api access token', '[]', 1, '2021-07-21 01:04:52', '2021-07-21 01:04:52', '2022-07-21 06:34:52'),
('67eba1e2821581fff2a9e11d1a7a0bfe5c63bbfabc8f0d60a8933943296ffe7d9e0b28ce8193b96b', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:08', '2021-05-04 00:52:08', '2022-05-04 06:22:08'),
('67f80b2e452b0e1fc087a2743978a1bdfccf3516bc8be6ec6d92c998439f1b786301050a0e3f6942', 618, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:35', '2021-05-02 08:31:35', '2022-05-02 14:01:35'),
('6886950e2ec61f25c08f3cf729173e176f0d502211ca620ef7e6032e8c9ddb30f0c1d8767eb02698', 58, 3, 'Api access token', '[]', 0, '2021-08-11 03:47:28', '2021-08-11 03:47:28', '2022-08-11 09:17:28'),
('68af77d5e96c44c614ed29483817285456c4ae7b80f6b2e746cc9682b4c03633710e160406b46499', 44, 3, 'Api access token', '[]', 1, '2021-08-03 01:43:23', '2021-08-03 01:43:23', '2022-08-03 07:13:23'),
('68c1cd43b222e5c6b1723cb351fb8c09920e47d0c448db425ed491edc94b1ff4c01f96b5415d07ec', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:36:18', '2021-07-16 06:36:18', '2022-07-16 12:06:18'),
('68d6e7beeb2ca8dc65c05430ec469c613634645364f57445b98874524031595a357132281522e544', 425, 3, 'Api access token', '[]', 0, '2021-03-22 05:46:46', '2021-03-22 05:46:46', '2022-03-22 11:16:46'),
('68de46f4eaccefc2d9fa8ce03300800b80691e5edcc187f4a646a54d6e413704465f1bbeba218a44', 545, 3, 'Api access token', '[]', 1, '2021-04-08 05:04:06', '2021-04-08 05:04:06', '2022-04-08 10:34:06'),
('6900a1235e6799a544604898bff8039745c44a339104da1ffcf8deb0aa13bb92114771a84cbd5e1c', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:03:49', '2021-07-14 02:03:49', '2022-07-14 07:33:49'),
('691587cedecb83a398efa1a6cd09c7df6e9b3c929ce11af8ec70cffad8d9f930794509a9359587e9', 621, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:39', '2021-05-04 00:40:39', '2022-05-04 06:10:39'),
('691b27985cf4d2d733dc48336b0036b61b8e6db153466fa28369a5f07b3b4ab6d193e978657ce27d', 12, 3, 'Api access token', '[]', 1, '2021-07-26 07:21:32', '2021-07-26 07:21:32', '2022-07-26 12:51:32'),
('692987c563be306273b3e2c6fac6709c67a882b70bba1207b48e9de9400d5df06fd9151c33e45fad', 528, 3, 'Api access token', '[]', 0, '2021-04-03 04:40:55', '2021-04-03 04:40:55', '2022-04-03 10:10:55'),
('69428169af0ba6d7ef2b83968422f4b98af20acb047638c5d1b323144d69fcd3edfd5fcb4a588bd9', 507, 3, 'Api access token', '[]', 0, '2021-04-02 00:46:52', '2021-04-02 00:46:52', '2022-04-02 06:16:52'),
('695a41d1a9556f4c6fdb97822335f951bebbb792f81eb799eb5097a4c118bf8a64126fa6d9503832', 592, 3, 'Api access token', '[]', 0, '2021-05-02 07:34:25', '2021-05-02 07:34:25', '2022-05-02 13:04:25'),
('6960b8cb60a9eae0a8a61fc6e3155572506e9d5fc4f5e2bd07e60e2b3d572a58b37fc0ba984f02b9', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:51:52', '2021-05-04 06:51:52', '2022-05-04 12:21:52'),
('6962b791d4762c40142f3d907194a7c6a29b0b9b9b60ebe0ba1a37b14ad67ba2bd3fa484a1b4cc72', 38, 3, 'Api access token', '[]', 0, '2021-07-28 07:40:10', '2021-07-28 07:40:10', '2022-07-28 13:10:10'),
('699e13ce2aec57781a7a813a56c83ac708f0a5c78aef57f79c1a9e008b7bdeca35b92d46b02be59b', 464, 3, 'Api access token', '[]', 0, '2021-03-24 06:36:58', '2021-03-24 06:36:58', '2022-03-24 12:06:58'),
('69b1afaea0afa08f504e577af9b7ae687ab429ece2e3988fec3b3fb82622697a6600c6c8f5c20bf6', 418, 3, 'Api access token', '[]', 0, '2021-03-22 00:31:00', '2021-03-22 00:31:00', '2022-03-22 06:01:00'),
('69ff9ea2ce597e3954cb98c5346521173370c3c4e2ab9cfb8cdef3c02ae54443f82ac5cfbccdf846', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:55:35', '2021-08-06 05:55:35', '2022-08-06 11:25:35'),
('6a3663109769b203bd566a8bc9f117ff4ef9c586768ceb0b8f254b62f59ddc3d3d70cbe5b692c7f8', 569, 3, 'Api access token', '[]', 1, '2021-04-18 23:26:42', '2021-04-18 23:26:42', '2022-04-19 04:56:42'),
('6a414519c1a4273de13477bea6a2b2311b1d4d5ea9c44102f4be6cde1b6d959c75e3ace15b67729e', 727, 3, 'Api access token', '[]', 0, '2021-06-10 01:47:47', '2021-06-10 01:47:47', '2022-06-10 07:17:47'),
('6a43c346942edeaaa0c2a02db5bac4dd0622df0c36db611f96fad817ef37cc7aebb5986dccc706f8', 52, 3, 'Api access token', '[]', 0, '2021-08-06 01:11:05', '2021-08-06 01:11:05', '2022-08-06 06:41:05'),
('6a6330640bb5e02324acec298495fcd2c02d14012771022b26cd7487975e989cf444b3fc837c3cce', 526, 3, 'Api access token', '[]', 0, '2021-04-03 01:23:47', '2021-04-03 01:23:47', '2022-04-03 06:53:47'),
('6ad7c2429b39893214307d629bdcce34c36b524e009d3ac33e9765d8801b9a0aabe8c24cd4cceafc', 17, 3, 'Api access token', '[]', 0, '2021-07-29 23:17:16', '2021-07-29 23:17:16', '2022-07-30 04:47:16'),
('6b3482e5e8a81e2aef68dbe0b51ed6f9c1fce0101300e7893f7f9bb84dd0814c3ac4cf1bc5e5bb62', 626, 3, 'Api access token', '[]', 0, '2021-05-11 07:00:27', '2021-05-11 07:00:27', '2022-05-11 12:30:27'),
('6b67ba866660aca1df864ce8157aa9059a5e8be18430fec7107e7f25ef1739713ba677b506591467', 611, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:26', '2021-05-15 06:22:26', '2022-05-15 11:52:26'),
('6b688109d7d14e262292499a68f98136dd623fe463fdb87913537af2d0b98d731314661e8dc21050', 773, 3, 'Api access token', '[]', 0, '2021-07-19 07:00:58', '2021-07-19 07:00:58', '2022-07-19 12:30:58'),
('6bc31aa68831e324e6f9ffae2c38b175620e55e9a08187daa23e8cfdb98691adda2fb7dc32732ca0', 17, 3, 'Api access token', '[]', 0, '2021-07-29 23:19:48', '2021-07-29 23:19:48', '2022-07-30 04:49:48'),
('6bf357241a78480e161a69c8121e26209049566c297b58121df336c03d2a5c3007431b791a1d8475', 57, 3, 'Api access token', '[]', 0, '2021-08-08 21:25:07', '2021-08-08 21:25:07', '2022-08-09 02:55:07'),
('6c56f438df0ffb80b1f6af63763be65bc75ed165c7f9e4cba5dc9fd5c8fa2e5182cbc1dd512cc66c', 43, 3, 'Api access token', '[]', 1, '2021-08-02 00:12:45', '2021-08-02 00:12:45', '2022-08-02 05:42:45'),
('6c6428294f71ed0a66cb99e77c8abbd9352d26cddabbc95718711cf8eff6bc4d723feb71b756e047', 622, 3, 'Api access token', '[]', 0, '2021-05-13 02:28:59', '2021-05-13 02:28:59', '2022-05-13 07:58:59'),
('6c6d43620dc579277f754945df91a934c12c3c5046c2abc4b903a760c1b852581d27265ab19c3516', 612, 3, 'Api access token', '[]', 0, '2021-05-02 08:06:20', '2021-05-02 08:06:20', '2022-05-02 13:36:20'),
('6c72fc424df4ff2101314e1c54bfb35ada3d71ae677a2f40feabfc435a8b537b9b322dcc45b75060', 39, 3, 'Api access token', '[]', 0, '2021-07-28 07:40:11', '2021-07-28 07:40:11', '2022-07-28 13:10:11'),
('6c85c27792b7f03fdf18ed885b94c4ab0c79e8aeec45b1e2037592bb748c723184e59f3906e4ede9', 1, 3, 'Api access token', '[]', 0, '2021-08-05 02:53:02', '2021-08-05 02:53:02', '2022-08-05 08:23:02'),
('6c8adea805752530e01326c14540211c2d3a171ff1f185735bdf5410adfa35a6a02d00ce253ac687', 7, 3, 'Api access token', '[]', 0, '2021-08-03 07:36:04', '2021-08-03 07:36:04', '2022-08-03 13:06:04'),
('6c92865e3f987839553099b44f17e8e9f0d1725e32cab7e3e4cd0fc8367309a83301f28a47d87d1a', 23, 3, 'Api access token', '[]', 0, '2021-08-06 01:08:53', '2021-08-06 01:08:53', '2022-08-06 06:38:53'),
('6c92fbc0c3447ef8967b8d802311059e5857b127d165f0a39665bbf6ee76a315b171b640b3f018b0', 472, 3, 'Api access token', '[]', 0, '2021-03-26 00:57:52', '2021-03-26 00:57:52', '2022-03-26 06:27:52'),
('6c9f6e095d5a15d9c1ad1232e27f4538ffed691f46d4e55bd68a99642fa488ba2f227243a586168d', 487, 3, 'Api access token', '[]', 0, '2021-03-26 06:12:55', '2021-03-26 06:12:55', '2022-03-26 11:42:55'),
('6c9ff1e8bdcdcaff79a7cd1ff0d59051bd8494c56a785a2c4bc2ad2d10515602e876e39ce5c6dedc', 486, 3, 'Api access token', '[]', 0, '2021-03-31 02:11:05', '2021-03-31 02:11:05', '2022-03-31 07:41:05'),
('6cade487cc884ed12b4a431857e1566be83167e6b8479612ebc325b215df3927eff43735cb804026', 579, 3, 'Api access token', '[]', 0, '2021-04-30 07:01:18', '2021-04-30 07:01:18', '2022-04-30 12:31:18'),
('6cb4216a60693b816dadf8b82751d84afe443aece8ac44a2697874cb44262e2ed8cf7abd8a6ec3cb', 666, 3, 'Api access token', '[]', 0, '2021-06-01 05:29:04', '2021-06-01 05:29:04', '2022-06-01 10:59:04'),
('6cd91bcc8a1df7c4a71ae0e90f5c7a39ca3ccb9156be93e5f46dc44b7ace9bc401db7dfb40a5b327', 488, 3, 'Api access token', '[]', 0, '2021-04-02 01:09:34', '2021-04-02 01:09:34', '2022-04-02 06:39:34'),
('6ce63a5dd3a192398196ec15cd442fd0b256bf411cd549115dcdaee2103aa0e0dc42faa936118220', 656, 3, 'Api access token', '[]', 1, '2021-05-24 04:31:35', '2021-05-24 04:31:35', '2022-05-24 10:01:35'),
('6ce9c0363bec67ddc1b967bdb4d972c86e7e4976ae67270448b43c42b4731730d4c456aa96cddacf', 498, 3, 'Api access token', '[]', 1, '2021-03-30 05:38:21', '2021-03-30 05:38:21', '2022-03-30 11:08:21'),
('6cf32f3f27385b59b82cd0329ee5368ea41ced3b8b69af23b94388b73dd0e7a8a04de54a7645a622', 526, 3, 'Api access token', '[]', 0, '2021-04-03 01:23:47', '2021-04-03 01:23:47', '2022-04-03 06:53:47'),
('6d1513399c063fe421b576c66785bb0758b84b7d5da32887270592eb1808ff2bf21b9768536d0667', 772, 3, 'Api access token', '[]', 0, '2021-07-16 01:52:29', '2021-07-16 01:52:29', '2022-07-16 07:22:29'),
('6d485300a5ccd014c9e882bf36a6ce25f0da7648718601ac1301dea9fbc01ccf6f7d559c482d1840', 466, 3, 'Api access token', '[]', 1, '2021-03-26 00:08:49', '2021-03-26 00:08:49', '2022-03-26 05:38:49'),
('6d4be238a1013c09580b321cdba1bf9e49a54c16bbbf6f73d6f1ca88d0a8007432730c457452509e', 449, 3, 'Api access token', '[]', 1, '2021-03-23 05:38:57', '2021-03-23 05:38:57', '2022-03-23 11:08:57'),
('6d4f368ca3cce0fab3f8f51c1e2a7e905b06426f3ac84e5ea4d9e43e87f9faf646d494f1a94227fe', 393, 3, 'Api access token', '[]', 0, '2021-03-18 03:01:58', '2021-03-18 03:01:58', '2022-03-18 08:31:58'),
('6d5d620555a053c02e9b2a84e047207da1ee942c324eea2ad05bc68a74355a384c383fa8523ffcc7', 745, 3, 'Api access token', '[]', 0, '2021-06-17 02:14:53', '2021-06-17 02:14:53', '2022-06-17 07:44:53'),
('6d66992bd4a0a23908c54d523c79a744405dada9f59b7be327f0aa30f0e151e383d7278f2f815972', 1, 3, 'Api access token', '[]', 1, '2021-08-13 01:37:38', '2021-08-13 01:37:38', '2022-08-13 07:07:38'),
('6d99685d7688399fd952f3fd3681e6d4cad78320550550338b08fd66f472a2ebebf32b9cf39e8938', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:14:28', '2021-07-26 07:14:28', '2022-07-26 12:44:28'),
('6db77fbbc434f8ce3d53cc39339358833b1ecd285ec8dfa18207af99c98e0443db4b4ca3277a074a', 1, 3, 'Api access token', '[]', 0, '2021-08-03 01:55:06', '2021-08-03 01:55:06', '2022-08-03 07:25:06'),
('6dd111ebca656bbd041fd8c566e41eb6345b57ee94876eefef429b9e196f9759f5f31f3299eb9de9', 400, 3, 'Api access token', '[]', 0, '2021-03-19 01:12:01', '2021-03-19 01:12:01', '2022-03-19 06:42:01'),
('6de353de1ce82292b16388683886507b4b037e0edb7531004d5d3c05deb6ca27ccffc3a3235777a3', 519, 3, 'Api access token', '[]', 1, '2021-04-02 07:26:12', '2021-04-02 07:26:12', '2022-04-02 12:56:12'),
('6ded6120e82ad51dbba781305066a11b4f2ca1320ab3fd1998ca7308bc94ddc8dd4c64cc72808240', 684, 3, 'Api access token', '[]', 0, '2021-06-02 08:03:50', '2021-06-02 08:03:50', '2022-06-02 13:33:50'),
('6e05153dc374f3ac7bd4f80fb1526edd9485ae50bd6d89efd19f9355eea3dc711421053feeda6b01', 731, 3, 'Api access token', '[]', 0, '2021-06-10 02:02:13', '2021-06-10 02:02:13', '2022-06-10 07:32:13'),
('6e16963f688c68324609b2838e6ad75c3c7320d1c5edc45becda62e36564410b74bb40d6a6ac2919', 400, 3, 'Api access token', '[]', 0, '2021-03-22 07:53:34', '2021-03-22 07:53:34', '2022-03-22 13:23:34'),
('6e1839991b38512d9a6ad00e17ce9bbee2e03797f5ff67366ae3f6387dc250b384dc80cd7d319252', 624, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:21', '2021-05-15 06:22:21', '2022-05-15 11:52:21'),
('6e5d87779f06830175786000c73a6b89e90debddce80315e585b50165c90d13714a9db1da4bc7d0e', 59, 3, 'Api access token', '[]', 0, '2021-08-11 09:24:04', '2021-08-11 09:24:04', '2022-08-11 14:54:04'),
('6e63501d26a71a4b1afa2a5d70d58515351753173b7c80575f4d7c7fb5eb49e8048cda6888ea3eca', 427, 3, 'Api access token', '[]', 0, '2021-03-22 05:50:53', '2021-03-22 05:50:53', '2022-03-22 11:20:53'),
('6e67f90fcf8965c72a9dae79221e6e8cc121db5fb4e48e5f5ac74370a7a0837768fed351d193ab9a', 1, 3, 'Api access token', '[]', 0, '2021-08-03 06:54:36', '2021-08-03 06:54:36', '2022-08-03 12:24:36'),
('6e977d59b160d99804d9cccb596e6991ab9da135645c5b6a6ea230f1f87c6b809af86a4a84ca0378', 618, 3, 'Api access token', '[]', 0, '2021-05-04 06:58:57', '2021-05-04 06:58:57', '2022-05-04 12:28:57'),
('6e9c176ab7fa033738432afc92d97db3d52b86f40575022c337803f507a5882f9e9c87be49e1436d', 571, 3, 'Api access token', '[]', 0, '2021-04-19 06:43:51', '2021-04-19 06:43:51', '2022-04-19 12:13:51'),
('6edebfea7f639cbc6bd882429ffc73b6307b6e4e1f7b484b781c28645fbe411b35c4b96f039fa632', 759, 3, 'Api access token', '[]', 0, '2021-07-01 22:46:39', '2021-07-01 22:46:39', '2022-07-02 04:16:39'),
('6ee0f32e296c09de39e9669b9d4e432a6c8258f24c6066c7bf4545c0205678f010f5cdd6bc2d3d5c', 5, 3, 'Api access token', '[]', 1, '2021-07-27 04:10:38', '2021-07-27 04:10:38', '2022-07-27 09:40:38'),
('6eea132b3f5cb9b15f3ce718fa9cb2fd6b6687776f7656e29e927a0c57fdd42efdfa9c5b41f74e10', 639, 3, 'Api access token', '[]', 0, '2021-05-12 05:39:19', '2021-05-12 05:39:19', '2022-05-12 11:09:19'),
('6f101eceeb1586546cb0f3d153ae40b8c1f6e8d87e34b2bacee29ca7aa410590ab531b0de4b6335a', 759, 3, 'Api access token', '[]', 1, '2021-07-01 23:42:52', '2021-07-01 23:42:52', '2022-07-02 05:12:52'),
('6f1965e5b305386b17a7f76580c81b5dfeda8611b85060c5f75c0c54bc6cf06849403488bae14bd4', 393, 3, 'Api access token', '[]', 1, '2021-03-22 04:56:08', '2021-03-22 04:56:08', '2022-03-22 10:26:08'),
('6f20c15022da39224a2f5234a7e14e686d203b429dc03ed1e36576f665ea6481524b1211ee253def', 393, 3, 'Api access token', '[]', 1, '2021-03-22 02:55:13', '2021-03-22 02:55:13', '2022-03-22 08:25:13'),
('6f22f86237dff25f69c9f7af5a88c3c6a8a4128f655c24bbf298f88e861b42985cabb3d9cb2aebde', 429, 3, 'Api access token', '[]', 0, '2021-03-22 06:04:14', '2021-03-22 06:04:14', '2022-03-22 11:34:14'),
('6f3b1fc3202350b281ef29e574daa810f0d078b48d19a1006963c083f08d1dcd4efd1f2a45eeeefe', 724, 3, 'Api access token', '[]', 0, '2021-06-10 01:42:50', '2021-06-10 01:42:50', '2022-06-10 07:12:50'),
('6f5a1688516ecd96f47a3b0a5db9a92300836742fed3c4273724c3ea8ad12b10ca0c6901ce877128', 5, 3, 'Api access token', '[]', 0, '2021-08-07 01:55:55', '2021-08-07 01:55:55', '2022-08-07 07:25:55'),
('6f69b26fb9fd571c6df1c8527edec7ece0a3747e6f1b217faac7c290f8a8f67582bf3a56b6a7e23b', 398, 1, 'Api access token', '[]', 0, '2021-03-17 06:20:56', '2021-03-17 06:20:56', '2022-03-17 11:50:56'),
('6fb859ed4089b850f81f8311be454569cd5522f8372362c54227a0467c4820be983e90b927725732', 546, 3, 'Api access token', '[]', 1, '2021-04-08 05:07:38', '2021-04-08 05:07:38', '2022-04-08 10:37:38'),
('6fbd3a115bcdee671d27f8c45d21d12e5fe369500611687b3c8ffcad9920ba93dc6c9fea7b3f88f4', 17, 3, 'Api access token', '[]', 0, '2021-07-29 23:17:16', '2021-07-29 23:17:16', '2022-07-30 04:47:16'),
('6ff0f22a878c949854abb4bbf961d1666c8a65a5cb3b5e3912b164f90a6d7eb63b080368098dbff0', 661, 3, 'Api access token', '[]', 0, '2021-05-26 00:47:49', '2021-05-26 00:47:49', '2022-05-26 06:17:49'),
('6ff308584cfb844b895d03e7df70e989b5b552411df8a40f8fc362fef8f6c38a616cd87314ec8b26', 680, 3, 'Api access token', '[]', 0, '2021-06-02 07:47:39', '2021-06-02 07:47:39', '2022-06-02 13:17:39'),
('6ff556983a80422bd28f6aca411f3e7d82be10216cd24e329f4ce9f0f2f69e0f7faaabede748ba4e', 410, 3, 'Api access token', '[]', 0, '2021-03-18 07:47:01', '2021-03-18 07:47:01', '2022-03-18 13:17:01'),
('700c0cc0369ecd69e00d094f9abbd86f8cd2fbfaefb91fc4fa34907cbdfb3abd2671730d6f3d6c1f', 33, 3, 'Api access token', '[]', 0, '2021-07-28 07:27:32', '2021-07-28 07:27:32', '2022-07-28 12:57:32'),
('70386dfb5e97ae373928bbb22bf647261d3d2b856500bbe42a40cc6cbb0e5a03273665e54f077d70', 687, 3, 'Api access token', '[]', 0, '2021-06-02 11:12:11', '2021-06-02 11:12:11', '2022-06-02 16:42:11'),
('7041ddf06212705e4b31803b2cd6ba81638b15cadbb00cdf7827d90fa847cff1eac4d84945ad5fe7', 655, 3, 'Api access token', '[]', 0, '2021-05-24 04:26:18', '2021-05-24 04:26:18', '2022-05-24 09:56:18'),
('704bd01eb186039c90e873cdecb9fa66ba4d5e832c42f1d5ab45eccec74725c94527453c5e8baf8b', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:13', '2021-06-10 02:37:13', '2022-06-10 08:07:13'),
('705b1cffe1a993460a21f3c5ebecc6ad02205e825ba5ebe24a59e756901f611c830394f49be937f2', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:51', '2021-05-04 01:11:51', '2022-05-04 06:41:51'),
('70be0aba161cd6dd463a8203726a0db6d5b3a0660ea5e1213678698e997862465d8b6aa72c84b3c3', 411, 3, 'Api access token', '[]', 1, '2021-03-18 08:14:44', '2021-03-18 08:14:44', '2022-03-18 13:44:44'),
('70c12d164972bed5361c1fafa973df26f0055db8ca355325faffc01e3313986c8eac5a6e73c5bf7b', 1, 3, 'Api access token', '[]', 0, '2021-07-29 23:02:41', '2021-07-29 23:02:41', '2022-07-30 04:32:41'),
('70d3b889e16a5186d54f999756acc73977359a67d8b688ce63082f25d035a539846528291ef81887', 480, 3, 'Api access token', '[]', 0, '2021-03-26 04:27:33', '2021-03-26 04:27:33', '2022-03-26 09:57:33'),
('70fc0442bedfdcba81aa5300a873b71ae5c9560de182049227c2f2d800d347786b83f4ed2e0493fd', 495, 3, 'Api access token', '[]', 1, '2021-03-30 00:47:42', '2021-03-30 00:47:42', '2022-03-30 06:17:42'),
('71246bf42ec99eefbf4ae3177ed94c271117022a07bb8860ad619a89ac0b4ee896d22b2a771852a5', 621, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:20', '2021-05-15 06:22:20', '2022-05-15 11:52:20'),
('712e927d745fd959cde238087171e1f88e902fe0b32ca22142e8c08cd068feffc535a8f6b969023e', 782, 3, 'Api access token', '[]', 0, '2021-07-21 05:03:53', '2021-07-21 05:03:53', '2022-07-21 10:33:53'),
('713a3a893d47a0cf75f5d4d556ea92dda98fb644e66d73c9abc00a2bc20c081d6fdf3e4c31f3b097', 1, 3, 'Api access token', '[]', 0, '2021-07-30 00:13:43', '2021-07-30 00:13:43', '2022-07-30 05:43:43'),
('7186a06a1193d6e9defcd1f8a8250923a4d0d83e90fa51a8cf67b031447b5bd43ea3844b93679573', 436, 3, 'Api access token', '[]', 1, '2021-03-23 02:59:28', '2021-03-23 02:59:28', '2022-03-23 08:29:28'),
('71b5e49c2c96e190bfc7bea5f3df1aa8bdc8f9c4b4346b53337d1230b2d4ec038347c7ad6dbb1fea', 531, 3, 'Api access token', '[]', 0, '2021-04-05 02:38:43', '2021-04-05 02:38:43', '2022-04-05 08:08:43'),
('71d3aab4af198d8c2784a22976d7c88d1b524e08cd96d173d373d7c438c7f4bd56703b7c9683d9f6', 400, 3, 'Api access token', '[]', 0, '2021-03-22 07:49:50', '2021-03-22 07:49:50', '2022-03-22 13:19:50'),
('71d4588b0333bae868b1ede06a81277f17ed54b100cb97fa9d2204ae5fb22d10e49232fea5b413b3', 486, 3, 'Api access token', '[]', 0, '2021-04-01 00:26:04', '2021-04-01 00:26:04', '2022-04-01 05:56:04'),
('71ed05765c05cee37bc84f5c9cf5f5d2cb64c03d6a0ecaa4f85c26f67aa774cda083e6da5ac316c4', 665, 3, 'Api access token', '[]', 1, '2021-07-13 01:19:26', '2021-07-13 01:19:26', '2022-07-13 06:49:26'),
('71f5e59ccc1faf0a887136f50b5d484e390006190af43d8464b99518c1826942ed1261fdaec7ff2d', 487, 3, 'Api access token', '[]', 0, '2021-03-30 05:24:23', '2021-03-30 05:24:23', '2022-03-30 10:54:23'),
('7208b65e1840ce6ef9f04f2388b19a18284a5696e465110a67b6ff177de98a4746290d64e23abca5', 1, 3, 'Api access token', '[]', 0, '2021-07-29 23:22:38', '2021-07-29 23:22:38', '2022-07-30 04:52:38'),
('7217c6ddf776e1bb21cbb304656d32e460b2c4ac85c45caf083417cdd6827be55e9f5e70188264e0', 773, 3, 'Api access token', '[]', 0, '2021-07-16 01:55:20', '2021-07-16 01:55:20', '2022-07-16 07:25:20'),
('72230665b7f23f7845350e07f18e8a2c151e2d02d48a469d9ad2366db16b018aa2bf16c84abea2d0', 435, 3, 'Api access token', '[]', 0, '2021-03-23 01:09:44', '2021-03-23 01:09:44', '2022-03-23 06:39:44'),
('72585ad81331de8040bcdf446e7fd38df74a52b4eb01bf2b9f490211d477768f39ad582ddc058601', 533, 3, 'Api access token', '[]', 1, '2021-04-06 02:27:45', '2021-04-06 02:27:45', '2022-04-06 07:57:45'),
('72752a90bdea706ad42bcb6156046ed96b5d8eb36ccd1a5565bc04cc9cb7a956b08d399dac4fc9b2', 665, 3, 'Api access token', '[]', 0, '2021-06-10 01:34:04', '2021-06-10 01:34:04', '2022-06-10 07:04:04'),
('730fb62fa1814b451c61e7c9c6e3b789a8743ec25868a5c8ea0fbe33989bc80795bab438176cf941', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:38', '2021-06-10 02:37:38', '2022-06-10 08:07:38'),
('73440b5eb2395868a0ac414a6cc28f53e2fc20a2583db0d7f11e3b883727fd19f6afac5d645bf539', 578, 3, 'Api access token', '[]', 0, '2021-04-20 02:22:30', '2021-04-20 02:22:30', '2022-04-20 07:52:30'),
('734985a0d06f2b1737628f94a6432d4c6200f06a0972e01d9738546077bb14055c925839755e3c4e', 768, 3, 'Api access token', '[]', 0, '2021-07-14 06:32:06', '2021-07-14 06:32:06', '2022-07-14 12:02:06'),
('73665d4d55ebbf3fc0f9fa9c4c3468c693e5ba05b008a31fc10adde9b4e91f2df2e60e11816bc625', 493, 3, 'Api access token', '[]', 0, '2021-03-30 00:28:09', '2021-03-30 00:28:09', '2022-03-30 05:58:09'),
('737485f5e44356e0176829bba33bad345cf1fc7a7d5bec0757f29a33d884765b3579d90785f4942a', 14, 3, 'Api access token', '[]', 0, '2021-07-27 04:35:53', '2021-07-27 04:35:53', '2022-07-27 10:05:53'),
('738d7bcd99f4fdec881141794facdbe31bba8bd379f257fffd75489676e862afdf69543adf0ec966', 419, 3, 'Api access token', '[]', 0, '2021-03-22 04:56:33', '2021-03-22 04:56:33', '2022-03-22 10:26:33'),
('73c4aa2706c7481fb06de257e535579d5478464d45e11b001310b38f9a718a578aebd2f2cadf473a', 576, 3, 'Api access token', '[]', 0, '2021-04-20 02:08:30', '2021-04-20 02:08:30', '2022-04-20 07:38:30'),
('73ce3add821bcab3163fff732226ccb9447056930557f9c7b10be6dc9f41599f5de5ecf5c824400a', 393, 3, 'Api access token', '[]', 1, '2021-03-18 04:14:53', '2021-03-18 04:14:53', '2022-03-18 09:44:53'),
('73da9363fb9d9de4ffa4d9fdb9553c3af9ebb016f0af685b01c47445409d8e143dc59095d94261b3', 393, 3, 'Api access token', '[]', 1, '2021-03-18 07:39:54', '2021-03-18 07:39:54', '2022-03-18 13:09:54'),
('73e5b43befb48938acd8c3ccefde9bc7dc70b96672b94cf8889cdc72a163269c2d5aceffeba92b4f', 698, 3, 'Api access token', '[]', 0, '2021-06-03 00:35:51', '2021-06-03 00:35:51', '2022-06-03 06:05:51'),
('742de550d4664e8b17c9e833d5aafdc97f0fcb8bbb165697b08f7a6fb02b3ef2eb4d35c6e62cf43f', 407, 3, 'Api access token', '[]', 1, '2021-03-18 02:30:45', '2021-03-18 02:30:45', '2022-03-18 08:00:45'),
('744db1515b3b5e55c978c706a0c384501d62ee6e24c44464b1f7e0e14223e186bc98fe4ff9323e93', 765, 3, 'Api access token', '[]', 0, '2021-07-12 01:42:21', '2021-07-12 01:42:21', '2022-07-12 07:12:21'),
('74612567976482d1b927507eb45575ad667424c9dba586b9723e31f442fbd628c89392cd6b0b5287', 758, 3, 'Api access token', '[]', 0, '2021-06-30 08:59:47', '2021-06-30 08:59:47', '2022-06-30 14:29:47'),
('7462d477b26254fc0bcd244a97f6d53288ba9a52a45da05a2dd152392642fdd62dd12905a3092fe3', 52, 3, 'Api access token', '[]', 0, '2021-08-06 01:15:15', '2021-08-06 01:15:15', '2022-08-06 06:45:15'),
('7499bb1844a8f8ff63d2b2518a50ceadaba19d4e3d00180573c7ba0128a7bc05978d1e7596fdef25', 655, 3, 'Api access token', '[]', 1, '2021-05-24 04:26:19', '2021-05-24 04:26:19', '2022-05-24 09:56:19'),
('74dceefef4ac29da41609e2991f802192e00e57d3eadbbea36711c9a2ead50b07df2229a720b0e82', 656, 3, 'Api access token', '[]', 1, '2021-05-24 05:29:30', '2021-05-24 05:29:30', '2022-05-24 10:59:30'),
('7525356e7955a1cfe82c5c3aa1ef8bee9420f321453bc7d78170d03b6f64a53692833fce143c8d82', 642, 3, 'Api access token', '[]', 0, '2021-05-13 02:17:44', '2021-05-13 02:17:44', '2022-05-13 07:47:44'),
('75809f2335cadbf5a8a558df7bf26c934684ed359f429a8649012fbd1e14cf82135c83501634eb78', 555, 3, 'Api access token', '[]', 0, '2021-04-15 05:50:15', '2021-04-15 05:50:15', '2022-04-15 11:20:15'),
('759b3179ebd9d800f33c5a65fa033b032d12a40a96665f76324f6a5bda427eb0c794caaf86ac17a4', 407, 3, 'Api access token', '[]', 0, '2021-03-18 02:30:45', '2021-03-18 02:30:45', '2022-03-18 08:00:45'),
('75b1bd80bba5e484247bebd680689fb0996a597ea760c0a54872d736ae389eceafd2d7e6ecffda0e', 52, 3, 'Api access token', '[]', 0, '2021-08-06 05:53:14', '2021-08-06 05:53:14', '2022-08-06 11:23:14'),
('75c94f674de3c8af4a16276338fad6e80f3a5a1cfed4ff33ee003daa7a8751c072b10d90d26369f3', 42, 3, 'Api access token', '[]', 0, '2021-07-30 02:27:46', '2021-07-30 02:27:46', '2022-07-30 07:57:46'),
('75d085a0eb80c8edea69ed134f60f1b148ab9a36c3f6f9211a92113ffbceaf0a76b4990afcecf288', 526, 3, 'Api access token', '[]', 1, '2021-04-03 04:49:03', '2021-04-03 04:49:03', '2022-04-03 10:19:03'),
('75eab0291269b4e1507d618ca1cef2029d9acd1c6dc9802931565e55ce063a01e9df88174cc705bc', 678, 3, 'Api access token', '[]', 0, '2021-06-02 07:24:41', '2021-06-02 07:24:41', '2022-06-02 12:54:41'),
('76105d464301326267317a012879bc71553c04e3c1783f3f78bd5cbcc19fea737cb76a762d727a28', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:35:45', '2021-07-17 00:35:45', '2022-07-17 06:05:45'),
('7610ae5c13249d88136c516581263c090a9e749fdf9b7bd5a0b3cc03ffb746b777601fdcac26c9aa', 770, 3, 'Api access token', '[]', 0, '2021-07-16 01:19:29', '2021-07-16 01:19:29', '2022-07-16 06:49:29'),
('762577b2f0bb51c68b9fe870667fd17dbd1b53c931119bcfadbea67238b1e6862962f493c73cd189', 60, 3, 'Api access token', '[]', 0, '2021-08-11 23:04:55', '2021-08-11 23:04:55', '2022-08-12 04:34:55'),
('76313c5c08f9c1316f78808ca44b87db354a8a91ed9159de0bb74d7f7f760b0686932984a8f78e11', 489, 3, 'Api access token', '[]', 0, '2021-03-26 06:24:24', '2021-03-26 06:24:24', '2022-03-26 11:54:24'),
('7648460d250924d937d9a233980c7b71b092d8f60feea6a7cb896a77e1ee96e1c496d02b3b094467', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:20:37', '2021-07-26 07:20:37', '2022-07-26 12:50:37'),
('764c8dabc61efaa7423c2b338f1b24530ecf803444972ea93e9e18b14751e317ff7fbd09f1317813', 706, 3, 'Api access token', '[]', 0, '2021-06-07 02:54:52', '2021-06-07 02:54:52', '2022-06-07 08:24:52'),
('7680ac366af44d0d6d085e4b3b12d0a0771704655230d49d4b4ee932df70db1828eead37fe3bf44a', 7, 3, 'Api access token', '[]', 1, '2021-08-03 07:17:46', '2021-08-03 07:17:46', '2022-08-03 12:47:46'),
('769dd6865f06ed7d1cb942cf4155baad7ac7a69697d30529747d2dfcaaec9597c43e4ca10c52d5e8', 435, 3, 'Api access token', '[]', 0, '2021-03-23 01:09:44', '2021-03-23 01:09:44', '2022-03-23 06:39:44'),
('76a252af9416fa6bfd5fc017e05c159130d77f5573b284e5f76564ea6de87bf5413b391a445a8b43', 554, 3, 'Api access token', '[]', 0, '2021-04-14 00:35:47', '2021-04-14 00:35:47', '2022-04-14 06:05:47'),
('76cf8b61311afc4d9aa770df2aa471f25b674cb1017d5ce2602490b46413e79d1e0bbb2bf377e020', 657, 3, 'Api access token', '[]', 1, '2021-06-02 22:59:59', '2021-06-02 22:59:59', '2022-06-03 04:29:59'),
('770066d213cd6fb29cec60a2c12a0acb8ef52b8ac9c45b99951ec546694b90169b669a9d2e81dd8c', 15, 3, 'Api access token', '[]', 1, '2021-07-27 04:09:06', '2021-07-27 04:09:06', '2022-07-27 09:39:06'),
('77070e5deecc673f4427c1b7517a05d586da0098847c1f9b39ee2dd54b4a554fe2c342ca473504c2', 582, 3, 'Api access token', '[]', 0, '2021-04-21 04:18:44', '2021-04-21 04:18:44', '2022-04-21 09:48:44'),
('774c7576a0011ee482ee1c4307672f7a24d3d35c0beff2506ad13694b3155582d3fcc00d905d1c41', 601, 3, 'Api access token', '[]', 0, '2021-05-02 07:47:37', '2021-05-02 07:47:37', '2022-05-02 13:17:37'),
('775772bdc248f902b46cf96b851fe84249da0466e498864bcc509f77e8aef3d6512a9514a15de2f7', 544, 3, 'Api access token', '[]', 1, '2021-04-06 04:20:39', '2021-04-06 04:20:39', '2022-04-06 09:50:39'),
('7785651284736d9479ddda19ed9ea9d944334048b5ba1531014ba65a5eea57ead528f2ca6ee16fc0', 770, 3, 'Api access token', '[]', 0, '2021-07-15 06:49:24', '2021-07-15 06:49:24', '2022-07-15 12:19:24'),
('77bf5841a8a48c6fdff42a44e74a78fce3b9fc710776c588b6a1e5ca9e1bad3711a595c36116627c', 424, 3, 'Api access token', '[]', 1, '2021-03-22 05:38:49', '2021-03-22 05:38:49', '2022-03-22 11:08:49'),
('77c1fd8624167da39803c388ee99be45e008e059d4a59f60b32a5501bf7947b06a7b2ad4ece6be1f', 393, 3, 'Api access token', '[]', 0, '2021-03-22 00:55:39', '2021-03-22 00:55:39', '2022-03-22 06:25:39'),
('77d7a00178ccab0187fdbddf88e6cb4468e0f7195e9e4da392c36ad5fccff0eaabf6c33b8041478d', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:21:48', '2021-05-12 09:21:48', '2022-05-12 14:51:48'),
('77fd29f65fef881191b4a7f2120731e063f3b0f1b18c40dd59f80f176da10b6c38c5dd55c53b6524', 498, 3, 'Api access token', '[]', 1, '2021-03-30 23:41:56', '2021-03-30 23:41:56', '2022-03-31 05:11:56'),
('7862db90794bd5125890647b1a8ccc375f45cbda0dd7a0e05a6da0ca7a44a2d7616d430b1422867f', 446, 3, 'Api access token', '[]', 1, '2021-03-23 03:01:00', '2021-03-23 03:01:00', '2022-03-23 08:31:00'),
('786a8a481808c8634b1983dd815c0d92971632c2f6daff09dff090dbb8e352d3173a5e968dc17ff4', 538, 3, 'Api access token', '[]', 1, '2021-04-05 04:42:11', '2021-04-05 04:42:11', '2022-04-05 10:12:11'),
('786edea1030a464db96cde1a2cf8931972a100fb78f7a6035fd88be0fabaa7d37e6bf530fc4b3c44', 763, 3, 'Api access token', '[]', 1, '2021-07-06 04:41:46', '2021-07-06 04:41:46', '2022-07-06 10:11:46'),
('787097600866c1eec2c1f77b22b782518ba3d13ea5a37163117bf1e04f7352fb554f2c75812f7ffa', 695, 3, 'Api access token', '[]', 0, '2021-06-03 00:34:30', '2021-06-03 00:34:30', '2022-06-03 06:04:30'),
('78a1a9cae904097b5b57afddfa95437dd9b03000b25b93090fafc879b797afbc70bca945159c7592', 560, 3, 'Api access token', '[]', 0, '2021-04-14 23:44:42', '2021-04-14 23:44:42', '2022-04-15 05:14:42'),
('78b942ef0b0bd5d81a532c130c00cecfdc31fc337b079fe5c32b59d971fda464d54d9580643e28f5', 639, 3, 'Api access token', '[]', 0, '2021-05-12 05:39:18', '2021-05-12 05:39:18', '2022-05-12 11:09:18'),
('78f726ff3361d28fff733bcf7bf6c23c8dc53e41556f3143f6b82b2b1c8cabb5a94c8ec906a08719', 428, 3, 'Api access token', '[]', 0, '2021-03-22 05:57:34', '2021-03-22 05:57:34', '2022-03-22 11:27:34'),
('78fc7bfd6d02ae240c01a05b4f2209754a5e66512d180cc7e0fda824ed161611e8545b2ccd3b7d67', 1, 3, 'Api access token', '[]', 0, '2021-08-05 03:02:38', '2021-08-05 03:02:38', '2022-08-05 08:32:38'),
('790cf3abda3d3c842876a5fd564882a0685de2daae3376e245d384c750ab44daebdb29e95beb358e', 587, 3, 'Api access token', '[]', 1, '2021-04-30 00:33:41', '2021-04-30 00:33:41', '2022-04-30 06:03:41'),
('792f7d58ad202de7b08b5146944a40811b8617f2a8183fb27f42bede522a2fcc75b6973aaa21c94a', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:40:05', '2021-05-12 02:40:05', '2022-05-12 08:10:05'),
('794eeef9395184e42f27782ffa73d66a802faaac8153b79019c133544a3092898f2f9cfe952887c3', 760, 3, 'Api access token', '[]', 0, '2021-07-05 05:51:25', '2021-07-05 05:51:25', '2022-07-05 11:21:25'),
('796f2ec99894aa58f8045af8002d5eccaf2ea54653261b3ad79febcbcb217feb595643a201d1a7fa', 594, 3, 'Api access token', '[]', 0, '2021-05-02 07:38:31', '2021-05-02 07:38:31', '2022-05-02 13:08:31'),
('79af69d12a0e1f05fc22a729fc39b38896a27fc9cb49a804754e90395c4d43e7ac61511b6ce7de30', 449, 3, 'Api access token', '[]', 1, '2021-03-23 05:20:22', '2021-03-23 05:20:22', '2022-03-23 10:50:22'),
('79cd12d035282886333dfc0689e0a2f0d0e0ea46fbc510ca4823584b0481b1f9e20e0a5f2bf19336', 504, 3, 'Api access token', '[]', 0, '2021-04-02 05:26:32', '2021-04-02 05:26:32', '2022-04-02 10:56:32'),
('7a19ab754116cedd7b8630c98b7b3def7906868336652edf8945973fc2aa95cbe53121e236ca058e', 432, 3, 'Api access token', '[]', 0, '2021-03-23 00:56:12', '2021-03-23 00:56:12', '2022-03-23 06:26:12'),
('7a3fdacb215e86409d8bdd357fc16ee854ecbc4a5da03c5babf4e8d1f1cb64cfe1597962ce61c4c3', 753, 3, 'Api access token', '[]', 1, '2021-06-30 08:52:55', '2021-06-30 08:52:55', '2022-06-30 14:22:55'),
('7a6f2fccd79ea4847d56797743ed2a021a79fcfeb94659876e2c9adc03795d33630f28130e1a50cb', 694, 3, 'Api access token', '[]', 0, '2021-06-03 00:29:43', '2021-06-03 00:29:43', '2022-06-03 05:59:43'),
('7a96e85fc7d8ef173091ca17b25a6c3d057c604f216b6c31104a7c4747b929d42c4389b91732f9f2', 676, 3, 'Api access token', '[]', 0, '2021-06-02 07:22:51', '2021-06-02 07:22:51', '2022-06-02 12:52:51'),
('7a99c1642cef0177b0a1bd035bbcd8a717f715889a109a6b25455188dbcddd24ebb93b3c62100662', 471, 3, 'Api access token', '[]', 0, '2021-03-26 00:24:19', '2021-03-26 00:24:19', '2022-03-26 05:54:19'),
('7abf4a87b271470ef0069cc0c10c85271d0448380e2186c7feb3925f5b23e68d4562ea1e713a147b', 52, 3, 'Api access token', '[]', 0, '2021-08-06 05:08:26', '2021-08-06 05:08:26', '2022-08-06 10:38:26'),
('7b13ef421b58b58ed4148a8362f17d133263930cc4be58eb092a06f27620c0565f6fa788f899546c', 525, 3, 'Api access token', '[]', 0, '2021-04-03 01:21:37', '2021-04-03 01:21:37', '2022-04-03 06:51:37'),
('7b65cf14e88c57b249101266a4257d251ecef2f020e99bcb84ed71c09b8630b7bfc909d34f9e2379', 13, 3, 'Api access token', '[]', 1, '2021-07-27 04:26:05', '2021-07-27 04:26:05', '2022-07-27 09:56:05'),
('7b65f6fedfa834fb0ef80b9bfafd40a20e2c4d017d252e647cbae5124cfd0c6e609ac3622e0d0459', 1, 3, 'Api access token', '[]', 1, '2021-07-23 07:53:18', '2021-07-23 07:53:18', '2022-07-23 13:23:18'),
('7b6ac0d477fbe75850c19b1d537543994dea2647c0f660b1916ee2265356d472fc3a4c580be7e018', 432, 3, 'Api access token', '[]', 1, '2021-03-23 05:37:45', '2021-03-23 05:37:45', '2022-03-23 11:07:45'),
('7b8786dea7d13973a3fc5a391f625fbd31828d47f83386a642a1bfeaa458b568729cd3565c90f035', 656, 3, 'Api access token', '[]', 0, '2021-06-01 07:33:37', '2021-06-01 07:33:37', '2022-06-01 13:03:37'),
('7bb55043d16ca334956dd8692f64b30e30f2f87f27e40e58d8b5d195a564398ed04ff667970b7da7', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:39', '2021-05-04 01:27:39', '2022-05-04 06:57:39'),
('7bd8eb12b16b5ef8d6eed17c54dd6296439f5eb159b6b6768983fd69e70d96072fd10c0efc161028', 400, 3, 'Api access token', '[]', 0, '2021-03-22 06:54:18', '2021-03-22 06:54:18', '2022-03-22 12:24:18'),
('7c377e4c2ffdf0cac0ddb48ed2025588b483984d1d06201b62f1b8e046a6d00b455b72f7f9ff1e7a', 415, 3, 'Api access token', '[]', 1, '2021-03-18 23:45:19', '2021-03-18 23:45:19', '2022-03-19 05:15:19'),
('7c8af3804ff174440a05021289577d73552cf9e3ae0b6de0c0a86dc91c4aaaf062406878226e5a5c', 586, 3, 'Api access token', '[]', 0, '2021-04-21 23:36:52', '2021-04-21 23:36:52', '2022-04-22 05:06:52'),
('7c9b2e9af99de3bbca9a6ed75d2543f9f44b5ee7edb1d069749a1f2fa2d43b8b99c41f596e86ee51', 652, 3, 'Api access token', '[]', 0, '2021-05-17 04:48:22', '2021-05-17 04:48:22', '2022-05-17 10:18:22'),
('7ca13d9cb04de84e643c7e6271ee7e9b24142728e0196217ac7b73f5f10e44c3a77f5ac6cb1bbab0', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:21:51', '2021-07-17 00:21:51', '2022-07-17 05:51:51'),
('7cc3167d4c9d94df48ca4a9f49a294e49fd4770a4b7594899a6d192444abb6331599cf69ba5adacd', 742, 3, 'Api access token', '[]', 0, '2021-06-15 04:37:56', '2021-06-15 04:37:56', '2022-06-15 10:07:56'),
('7cf929fc7cc3625ba67d5967bab633f71019ec6adc4f7e0e3512994bfacaeb6ca9d3c1f606faebe7', 1, 3, 'Api access token', '[]', 0, '2021-08-03 01:45:15', '2021-08-03 01:45:15', '2022-08-03 07:15:15'),
('7cfb6196829fbeafd20927993c60497591d41676267532a3d8fd83a6349e78c46440db194d416d12', 398, 3, 'Api access token', '[]', 0, '2021-03-18 05:30:32', '2021-03-18 05:30:32', '2022-03-18 11:00:32'),
('7d3b2b87d73bc948dd820a0bd2576048d5be8e29f21d8e5a2a9dfcf988925fe808b8a6d209a48759', 400, 1, 'Api access token', '[]', 0, '2021-03-17 06:23:50', '2021-03-17 06:23:50', '2022-03-17 11:53:50');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('7d534685313e1237668fccfaf70e70be146c33b0c7df301698b6654b72492b430b5536b219d3cbb4', 755, 3, 'Api access token', '[]', 0, '2021-06-24 07:53:05', '2021-06-24 07:53:05', '2022-06-24 13:23:05'),
('7d60aca0eccc176e0d7542cd9b5e9d4525aca3ab6c95a1e9c213b69d1e82337012cb67cce8e58a77', 1, 3, 'Api access token', '[]', 1, '2021-07-27 02:04:45', '2021-07-27 02:04:45', '2022-07-27 07:34:45'),
('7d96d0ef525582c4e050d75b34ea514c339503637437f6e451dc661f3de0feb54d72a739aa5110ab', 17, 3, 'Api access token', '[]', 0, '2021-07-29 04:51:55', '2021-07-29 04:51:55', '2022-07-29 10:21:55'),
('7dbb2f771c6f18c78e2d58fb4a2262cd98226d8c42c0487a793e9c5828629c6540d53f3f789eed8f', 450, 3, 'Api access token', '[]', 0, '2021-03-23 05:28:56', '2021-03-23 05:28:56', '2022-03-23 10:58:56'),
('7dbda56d18e77a068c3182bbf70418f79227a4a2a8905b2bce3fcfb1c8b445ddd58b9b3317a84891', 709, 3, 'Api access token', '[]', 0, '2021-06-08 09:27:00', '2021-06-08 09:27:00', '2022-06-08 14:57:00'),
('7e76a113cb9b090406545f4588edff68d9fea525a8558162c3fa1d047d62acc8f2de514c898ca8c0', 604, 3, 'Api access token', '[]', 0, '2021-05-02 07:49:57', '2021-05-02 07:49:57', '2022-05-02 13:19:57'),
('7eb13ac07444c72a96b3ce00d6866d557f8191d70fec4635b24bb873339dc206eb65933cf5d57e79', 725, 3, 'Api access token', '[]', 0, '2021-06-10 01:45:22', '2021-06-10 01:45:22', '2022-06-10 07:15:22'),
('7eb4ba10dde468a8729650f73c030a5ba0ac23736d065e9e912e0ff347f50b332f7d264f6b4ace6c', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:51:04', '2021-05-12 02:51:04', '2022-05-12 08:21:04'),
('7ec421a32bf8b36071b441712ad9210a13c0a3bd14a94d670b9b072bf26520ebe20ef98393710140', 526, 3, 'Api access token', '[]', 0, '2021-07-02 04:24:15', '2021-07-02 04:24:15', '2022-07-02 09:54:15'),
('7f7b4b78cd5d88987412cd9085827e543b80f8e825422317690ccf253f05fc9d1bb43bb252ee365a', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:54:05', '2021-05-04 06:54:05', '2022-05-04 12:24:05'),
('7f85d02c974f03f56f792e2f2212ea6df99d8c4e5f590da39bb843d59a542409c534f49e654077de', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:57:23', '2021-05-13 06:57:23', '2022-05-13 12:27:23'),
('7fbd75514d72a3ae76de346a3952541d2c5e4ab556b26a7dcf93f2e63729ffd195aae97f898d3710', 531, 3, 'Api access token', '[]', 1, '2021-04-12 00:42:33', '2021-04-12 00:42:33', '2022-04-12 06:12:33'),
('7fd7c0bb6d4c3f51c748cc1256fedbbf64e6179c60b4ae222c803ee673f95b0e261e0300b468d4ac', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:24:51', '2021-05-11 06:24:51', '2022-05-11 11:54:51'),
('7ff8b259135ee589b5b1e178adaa29e2a0c657ec884687be0ce2f53f96b3a6957adda048cea0e7b3', 665, 3, 'Api access token', '[]', 1, '2021-06-04 01:53:02', '2021-06-04 01:53:02', '2022-06-04 07:23:02'),
('800ac0e3f2dd00b96f28c3b25c6adf2ea705e806f554592bfff0b458c158a39522a3a7c9f64b886f', 21, 3, 'Api access token', '[]', 0, '2021-07-27 07:36:38', '2021-07-27 07:36:38', '2022-07-27 13:06:38'),
('800d5ffa87c8df77a1ddb27d4a3038d741165db09aa8b6ffa4d841cdfc4f765827679fcb9728d98a', 622, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:21', '2021-05-04 00:49:21', '2022-05-04 06:19:21'),
('803c9e535efc03166f9d5ca57beeff62040e6062eb2b78a53b9451e71be01e327d934bc4aa7b6293', 67, 3, 'Api access token', '[]', 1, '2021-08-17 06:06:27', '2021-08-17 06:06:27', '2022-08-17 11:36:27'),
('8043f5d400b870387ddb5915d7103f1f1db33e4a6ac1cf8bb398b40f0de0509bc4d92a4812056155', 531, 3, 'Api access token', '[]', 0, '2021-04-13 04:48:15', '2021-04-13 04:48:15', '2022-04-13 10:18:15'),
('805375b972c1dbf7c449cebfc5d295e67dd16892255404c572d48bbd29acc81fd24b4f3936bb0bb2', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:44', '2021-06-10 02:37:44', '2022-06-10 08:07:44'),
('805c99c8f7bb717ff2e5d4380a0ebf8ee9e621d17bad61cd8728abd7cbfabcf74b18d3d8b8eea7ef', 773, 3, 'Api access token', '[]', 0, '2021-07-19 06:59:04', '2021-07-19 06:59:04', '2022-07-19 12:29:04'),
('80a42fbf3b682c2cb5f04b681f50cc8a2633f9fe8777200c04e9bcd1337a600c04993831afc54f23', 52, 3, 'Api access token', '[]', 0, '2021-08-06 04:18:25', '2021-08-06 04:18:25', '2022-08-06 09:48:25'),
('80a664f2a69d5d622ab76226e65403dc7cae5308a2c50bac3be4c13da670988e3381a0b443992dbf', 7, 3, 'Api access token', '[]', 0, '2021-08-04 00:51:01', '2021-08-04 00:51:01', '2022-08-04 06:21:01'),
('80cc5f6993697c22d1694663db3068da488c18031a1b14c8a48700977dead0565cd92b5a18d73608', 397, 1, 'Api access token', '[]', 1, '2021-03-17 06:29:05', '2021-03-17 06:29:05', '2022-03-17 11:59:05'),
('80de495dc33d583f9d3811df3d4451729bfb921b2c6d3233ce1cf9146e9801ef4173cad10e4db9f8', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:24:41', '2021-07-16 06:24:41', '2022-07-16 11:54:41'),
('80fae25daa34f3fa0b2f83395e7c153d7d8eeb09cdb30894245bc8e83639d4caf1677250a9e87703', 785, 3, 'Api access token', '[]', 0, '2021-07-21 06:52:56', '2021-07-21 06:52:56', '2022-07-21 12:22:56'),
('813457005c18dbe833999ca9e090ad750ad51bd3aed946290fdaa393aec784058e4ca9fcb2bc499c', 14, 3, 'Api access token', '[]', 0, '2021-07-27 01:59:34', '2021-07-27 01:59:34', '2022-07-27 07:29:34'),
('814e06c2ae8ea2a9ead12741999d2acee4eddbb89ec49b72afb17c3a4263589c4a5a9ec40a7636ab', 616, 3, 'Api access token', '[]', 0, '2021-05-02 08:22:09', '2021-05-02 08:22:09', '2022-05-02 13:52:09'),
('815b9d6fa60752ed79bf4b4a16a85f56d54f2ef63fa359a120fa24fee0b3bcaa705fb3154a342f2d', 52, 3, 'Api access token', '[]', 0, '2021-08-06 05:55:16', '2021-08-06 05:55:16', '2022-08-06 11:25:16'),
('8169bd12f9fad1e26ceab22f8600a3379ef4399d50f1eb177fe499ef079c9384c574952755ee8851', 627, 3, 'Api access token', '[]', 0, '2021-05-04 07:15:48', '2021-05-04 07:15:48', '2022-05-04 12:45:48'),
('81745c4f2c97f1d254bcc2db5c8d8854b31bd4b9fa208e31e1fb49dd30d6170b5d975c3836fdbf37', 1, 3, 'Api access token', '[]', 0, '2021-08-03 01:49:28', '2021-08-03 01:49:28', '2022-08-03 07:19:28'),
('8179630df4c7ad62e0d9e3ae0e710c4ad1af4656968dfbf5fa4134a258416f2e44b6dab2404a6b40', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:11:36', '2021-05-12 03:11:36', '2022-05-12 08:41:36'),
('81ee6b226935814349878c269170fbfcd15028bd0e4b2354bd7f42ad61c0bd464b2d11094ea710f7', 63, 3, 'Api access token', '[]', 1, '2021-08-15 12:56:31', '2021-08-15 12:56:31', '2022-08-15 18:26:31'),
('8269f04047500c684b9ce764388d36ffb917893b0b0c7267a04978339b74ff7185d22184a0732796', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:13:00', '2021-07-20 02:13:00', '2022-07-20 07:43:00'),
('826ccfc7cb26d56cc46f814dbe6a2f5dfa9d2dbe5fa30a1d988d31d72ec4f3149bd89eeb9c4bcf1a', 659, 3, 'Api access token', '[]', 1, '2021-06-14 08:16:43', '2021-06-14 08:16:43', '2022-06-14 13:46:43'),
('8283b513dc096ddf6c02900e6a8a1bed9f6563d115a7d87ac024f94250b449d207a388dc5db7e42e', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:05:24', '2021-07-16 06:05:24', '2022-07-16 11:35:24'),
('829674499a3e6aaeadb7ea9235f61c1a03238b86e31958de8e66d7d37ef19264650f98196b568d7d', 541, 3, 'Api access token', '[]', 0, '2021-04-05 04:51:38', '2021-04-05 04:51:38', '2022-04-05 10:21:38'),
('82f79ee2d30fa0d48fc616859ad15f1feabda92ed619dfa66df47c7c71d284a6cea32a649d0e7b7b', 573, 3, 'Api access token', '[]', 0, '2021-04-20 01:04:01', '2021-04-20 01:04:01', '2022-04-20 06:34:01'),
('8303f5f5bbb373cec2289b4955fe388a1451e551c04522037915cd1e17c2d2b5d06989045506cc33', 759, 3, 'Api access token', '[]', 0, '2021-07-01 22:11:15', '2021-07-01 22:11:15', '2022-07-02 03:41:15'),
('830b40f692e075ad4d77e2c3dc9576f0e375c57fad5681bd1912132cee2e9e522460cc7e15b6dc8a', 523, 3, 'Api access token', '[]', 0, '2021-04-03 00:12:10', '2021-04-03 00:12:10', '2022-04-03 05:42:10'),
('8355b02de268162193e3f292899d3ae76822d78bc46f991411196bae7a5be81addb539bc7d2f0605', 1, 3, 'Api access token', '[]', 0, '2021-08-04 06:10:31', '2021-08-04 06:10:31', '2022-08-04 11:40:31'),
('837ccb24fb15e9fafacf0e649b834a67132d30d49ca20f0964a73d37ead5bdbd711d9cfb660ec8df', 673, 3, 'Api access token', '[]', 0, '2021-06-02 07:01:36', '2021-06-02 07:01:36', '2022-06-02 12:31:36'),
('83a4096e59ad9f578b6d0aa54558a7622bfc1aec782c836fb6c9197abb09fe273b0c65c6c1fc971b', 773, 3, 'Api access token', '[]', 0, '2021-07-16 06:39:49', '2021-07-16 06:39:49', '2022-07-16 12:09:49'),
('83e5fa3eb6c9b18d34711fcd96ffc3190366f0f4972ad9b05d9fc4dcee379ea1345d3e540416d999', 659, 3, 'Api access token', '[]', 1, '2021-05-28 03:20:55', '2021-05-28 03:20:55', '2022-05-28 08:50:55'),
('83f270d896964ff179565500301ac9e0752dfbaa269825fa97c38b04bd013a32c416622d21562588', 747, 3, 'Api access token', '[]', 0, '2021-06-17 08:25:14', '2021-06-17 08:25:14', '2022-06-17 13:55:14'),
('83f6a8a14c80846721c7484a9435a54850736875cf07c49a4bdf540e89d8f6ce66e43c0d1d8abf4b', 533, 3, 'Api access token', '[]', 1, '2021-04-05 01:22:26', '2021-04-05 01:22:26', '2022-04-05 06:52:26'),
('8406337b39a58b711168a736f1f84b17117009c7b6905a0a4a60899bf3c7c733b912f92df8c0d64d', 530, 3, 'Api access token', '[]', 0, '2021-04-04 22:58:19', '2021-04-04 22:58:19', '2022-04-05 04:28:19'),
('842203197b2977ae8100efbe59ae8bafa67122e86a0842e00fdfcfc75bb546b0fe1ec5db875bc280', 436, 3, 'Api access token', '[]', 0, '2021-03-23 01:12:03', '2021-03-23 01:12:03', '2022-03-23 06:42:03'),
('843e97f98a598f49cff4d3f02768541a0b955e0ef36e96648121313471ee759b27ef5bf8f49803d7', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:34:35', '2021-05-12 02:34:35', '2022-05-12 08:04:35'),
('844180a3379d94011ddc6813efc3359e89ba9956c87585d3ebe1a572b9665b03b00eecc7489b15d6', 55, 3, 'Api access token', '[]', 0, '2021-08-06 23:33:35', '2021-08-06 23:33:35', '2022-08-07 05:03:35'),
('845a6a5d2387ee9f2adced8f19a07cdc247053020d04ce30d8d4e5e09076950d46d1630f5781cc09', 689, 3, 'Api access token', '[]', 0, '2021-06-02 21:26:59', '2021-06-02 21:26:59', '2022-06-03 02:56:59'),
('847066729838443d7dc95c3ef5c90c98b11ffe583da4915d58b4bc40b1ab91cda198a57f29bd6323', 685, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:35', '2021-06-10 02:04:35', '2022-06-10 07:34:35'),
('8474edd2813346a39aac2b177f3aec97bf8376a66ba96ba23a3a4245019b3e1d363c1d6d52ffa050', 434, 3, 'Api access token', '[]', 1, '2021-03-24 07:13:43', '2021-03-24 07:13:43', '2022-03-24 12:43:43'),
('84b07fc6d353b72377089dfcebe40b7c9f951fcf657e85340c9726381a54eee55f085c27e44f87a0', 489, 3, 'Api access token', '[]', 0, '2021-03-31 04:03:58', '2021-03-31 04:03:58', '2022-03-31 09:33:58'),
('84c1eb03f39c3656026700a18839017ec23bf13c7638f191da1301c721e2be8d32e839e5606ae7a4', 1, 3, 'Api access token', '[]', 0, '2021-08-04 02:09:08', '2021-08-04 02:09:08', '2022-08-04 07:39:08'),
('84c9ba1f8927224d6599c4bc9d8072bd14cc311fe671e0b157a84bb7528dcc436e669f67269930f1', 634, 3, 'Api access token', '[]', 1, '2021-05-12 01:02:53', '2021-05-12 01:02:53', '2022-05-12 06:32:53'),
('84d255104edcf79043a18fe64b2fca62a505da47d0e807c0992e1a2dfc514e5f74b843d49c3735a6', 398, 3, 'Api access token', '[]', 0, '2021-03-22 01:43:10', '2021-03-22 01:43:10', '2022-03-22 07:13:10'),
('84eabc89760a7aabf402d8f053d4bf2b288211ee3b357be4fc1fab681a3dfd09c755f75826089083', 397, 3, 'Api access token', '[]', 0, '2021-03-18 05:21:55', '2021-03-18 05:21:55', '2022-03-18 10:51:55'),
('84f6832fc65fc0554dbc509810229f750832bc5b267f727c1173c30964c034c1cfc1513893b48bbe', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:13:49', '2021-07-26 07:13:49', '2022-07-26 12:43:49'),
('8518543dccebfd0ca4c3736adbb4960d718e1db77cf0d1e935419783ec174c075d7e4b10fce0d25b', 626, 3, 'Api access token', '[]', 0, '2021-05-11 05:27:31', '2021-05-11 05:27:31', '2022-05-11 10:57:31'),
('855b47a1591201f47553f17210db76ce2302ed9ceccbd8255f2c899be76d1be938e89f5f0279e38c', 419, 3, 'Api access token', '[]', 0, '2021-03-22 04:56:33', '2021-03-22 04:56:33', '2022-03-22 10:26:33'),
('856ab505097dfdd438ea1adbf03bd88151ab925643c1bdf295eb08305c5c93571541f155e241fbf5', 626, 3, 'Api access token', '[]', 0, '2021-05-29 02:48:47', '2021-05-29 02:48:47', '2022-05-29 08:18:47'),
('857525bff68e10b31446b71784291f09170a5eb21c69d1fb225119b7befd7b211588683db4edd386', 554, 3, 'Api access token', '[]', 0, '2021-04-15 04:28:19', '2021-04-15 04:28:19', '2022-04-15 09:58:19'),
('858cc6bb7148f80c9c5df5d96ffd08cd3c47b2981e1f35a2ce512d316217256a8c53d41d9e28dd18', 575, 3, 'Api access token', '[]', 1, '2021-04-20 01:57:49', '2021-04-20 01:57:49', '2022-04-20 07:27:49'),
('85a3513a3aa44c23cfc294edd6854408fca18a2cb58dd103169214dc46abd5c7e589429c9b7e0f1f', 638, 3, 'Api access token', '[]', 1, '2021-05-12 06:04:51', '2021-05-12 06:04:51', '2022-05-12 11:34:51'),
('85b317af11c5eafd57c42d543dc3b5808311971da72ef366b64c9165b318fc165c1f9f169b1dfd6e', 743, 3, 'Api access token', '[]', 0, '2021-06-15 07:41:38', '2021-06-15 07:41:38', '2022-06-15 13:11:38'),
('85bb6b99af9dda01ac779da39c0bc02a87d4c362830df94bb74529a78627705a7544938490978aab', 764, 3, 'Api access token', '[]', 0, '2021-07-08 07:25:59', '2021-07-08 07:25:59', '2022-07-08 12:55:59'),
('85f872aefea896880440a29c2e43d86ee81e50955495682489eb230a1e04f8744b1986c882f3d663', 488, 3, 'Api access token', '[]', 0, '2021-03-26 06:13:38', '2021-03-26 06:13:38', '2022-03-26 11:43:38'),
('8619e62936efddab5431ac946034e644872c8922d233cd25ac0bdb0db92391ff2841c83cc7fedf85', 665, 3, 'Api access token', '[]', 0, '2021-05-31 00:28:46', '2021-05-31 00:28:46', '2022-05-31 05:58:46'),
('862cf7b7c49f486d1c10d2418871c3575fd197d1a577245e8a31d77bcf6e1bce034893dd1d4333f6', 7, 3, 'Api access token', '[]', 1, '2021-07-30 00:56:36', '2021-07-30 00:56:36', '2022-07-30 06:26:36'),
('866791660556b0adb4914400329fd4565dd7004ae8ec64ea6c3e08eaaeb487f0ac8f98725eee77f5', 686, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:15', '2021-06-02 08:17:15', '2022-06-02 13:47:15'),
('86b95e630cbdd7f53d7fc7a28d783a6e099a709666bc0e947a0585f1cfc18b4eaa5db682d589e892', 415, 3, 'Api access token', '[]', 0, '2021-03-19 04:32:31', '2021-03-19 04:32:31', '2022-03-19 10:02:31'),
('86eae6ed183c9e947d85e6236d2d277b7370259eeb2bf16cf743b37e2bae1e862f8aee70ee00efa5', 12, 3, 'Api access token', '[]', 1, '2021-07-27 04:32:47', '2021-07-27 04:32:47', '2022-07-27 10:02:47'),
('86ef97e2384d6e4d64920ed4aa6022cec0f67a1b6e9400bba3342e96450a2717397be01749842f65', 709, 3, 'Api access token', '[]', 0, '2021-06-08 09:27:00', '2021-06-08 09:27:00', '2022-06-08 14:57:00'),
('870b4122edf0be6645a7442f2364de9cfc7c0e0cf36054ecf3d83a28d663074204bc88972af84baf', 759, 3, 'Api access token', '[]', 0, '2021-07-01 23:15:49', '2021-07-01 23:15:49', '2022-07-02 04:45:49'),
('873cb557714077043e8692d2121d40683fe0cad615b5e9cc4b314c3cc7d187f10a788adb6d692b92', 555, 3, 'Api access token', '[]', 1, '2021-04-15 06:50:58', '2021-04-15 06:50:58', '2022-04-15 12:20:58'),
('87426303280db7d3533d7c839c1cdae25744ed7cd89443eececc3cbfb6d3d5ce2efa725e5b2f495b', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:02:53', '2021-07-14 02:02:53', '2022-07-14 07:32:53'),
('87a592e4f394cbaeb64e4c9be1c0de8f74a71f7fc1aaf459d8b0262fad77d9eb8add6f1eb549bdf9', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:58:42', '2021-05-27 04:58:42', '2022-05-27 10:28:42'),
('87bca6a845636b26083bf63ae427ef2adba7dcc86c302607eb5b58cd9dadd5ba956b5321d3616d5c', 659, 3, 'Api access token', '[]', 1, '2021-05-28 04:34:59', '2021-05-28 04:34:59', '2022-05-28 10:04:59'),
('87e160bb1d50df6f91c0ff1163ff11fa1dbbcc0427dd4fdba887a6cec1ed1ebb39e3817b2f6e3c9a', 468, 3, 'Api access token', '[]', 0, '2021-03-25 07:31:26', '2021-03-25 07:31:26', '2022-03-25 13:01:26'),
('87e79ac873b714e3f0f99ccbd488070436a0c9b1574156e6c190b4a8fa3d87fa52c8a0fc47c623c7', 422, 3, 'Api access token', '[]', 0, '2021-03-22 06:04:18', '2021-03-22 06:04:18', '2022-03-22 11:34:18'),
('8809677961fbf326e32df4b5468b54148283ffc84175fa33b7f88c7d19534b3e7908ea2cc7e65430', 618, 3, 'Api access token', '[]', 0, '2021-05-08 00:31:49', '2021-05-08 00:31:49', '2022-05-08 06:01:49'),
('8823991d0171dc91a03fb3cc2396d6cfcfcdab58f9d58a454cb84b2eff1acd1ae5ce8b06db607474', 662, 3, 'Api access token', '[]', 0, '2021-07-08 00:14:04', '2021-07-08 00:14:04', '2022-07-08 05:44:04'),
('88282a75619db4165f174cec9dbded5a61e235e878c3a7115808f7a7df7bc304e2db2134d803460f', 52, 3, 'Api access token', '[]', 0, '2021-08-06 06:58:33', '2021-08-06 06:58:33', '2022-08-06 12:28:33'),
('8857c84a7f8ef19817a8436cb72c687ee3847fc68a93f25d4b02456f23523d8c60b2dfa298f70227', 1, 3, 'Api access token', '[]', 0, '2021-08-03 07:16:53', '2021-08-03 07:16:53', '2022-08-03 12:46:53'),
('887f51c60021833222abde66547b77943ab036191e8e687ec201f5ec2d3f453fd8c788e7cc07f6fe', 782, 3, 'Api access token', '[]', 0, '2021-07-22 01:12:12', '2021-07-22 01:12:12', '2022-07-22 06:42:12'),
('888a643541f9bba65503fb5b6d974e388aceb306eeb691ab06c5d829bf18eba84890967837f1f450', 510, 3, 'Api access token', '[]', 0, '2021-04-02 06:05:30', '2021-04-02 06:05:30', '2022-04-02 11:35:30'),
('88ce64ab82ea56f06a4170c18919ca5a66270075318ed208014499285676b032441e068d543fd0cf', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:22', '2021-05-04 00:49:22', '2022-05-04 06:19:22'),
('88d03a416038dab18465249a2b1628376b6d50495bc9b1c4777a9c16425636b4c68aa2d4a5b40b7d', 20, 3, 'Api access token', '[]', 1, '2021-07-27 07:21:54', '2021-07-27 07:21:54', '2022-07-27 12:51:54'),
('88e94f35ccbc60fcb432c06b698b108ac0c8c917dcf8115f72ed4e94254f0cccdb2ff979e3e9328e', 618, 3, 'Api access token', '[]', 0, '2021-05-08 00:33:33', '2021-05-08 00:33:33', '2022-05-08 06:03:33'),
('88f5d27b6598b4479cb858776fe4f223856c7f8c6802c7b528aed2612e9a702fb7445beac3856e56', 627, 3, 'Api access token', '[]', 0, '2021-05-04 07:15:48', '2021-05-04 07:15:48', '2022-05-04 12:45:48'),
('890415208d7d87bc0e4eb336d380504357f4bbc8b2c651de163864ef26d49441dea60c2c861ac204', 659, 3, 'Api access token', '[]', 1, '2021-06-15 03:10:58', '2021-06-15 03:10:58', '2022-06-15 08:40:58'),
('890a9d6a339053ec87539b3028978b3e8d6084f0e5abd1a06a6175d8715c8a13d8228f109e408d25', 54, 3, 'Api access token', '[]', 0, '2021-08-06 23:32:06', '2021-08-06 23:32:06', '2022-08-07 05:02:06'),
('8946fcd57de8ca22a6472bf305ac734db788e498b2f4386359462dc9c90a59a3f20cf3f76daf3926', 769, 3, 'Api access token', '[]', 0, '2021-07-15 06:44:09', '2021-07-15 06:44:09', '2022-07-15 12:14:09'),
('89d1c9585132a57beb3960fc15eb9b5bbb4d32a0514caac88c218d71ea3ced22d33e8775d737dc30', 549, 3, 'Api access token', '[]', 0, '2021-04-12 07:38:47', '2021-04-12 07:38:47', '2022-04-12 13:08:47'),
('89ebb05cf5116d93ffd560ac422b393b74e7c16b8e63397d59acd35dd3546f7ce3aee21a25161e4e', 737, 3, 'Api access token', '[]', 0, '2021-06-15 01:23:37', '2021-06-15 01:23:37', '2022-06-15 06:53:37'),
('89fa7d4c94df2b431bcc5b76e2c29758b0f912a5d7715b97ab006e3f5526e508d499bdf45ab109b4', 492, 3, 'Api access token', '[]', 0, '2021-03-26 07:39:21', '2021-03-26 07:39:21', '2022-03-26 13:09:21'),
('8a32d800f8424dfddc850729f8db9e37fb5bb93ab545c476a317b6dc20a139222989711ba67e4a51', 542, 3, 'Api access token', '[]', 0, '2021-06-16 00:00:08', '2021-06-16 00:00:08', '2022-06-16 05:30:08'),
('8a4b4c4a04e2ae7c10f45a7788d9105b637c1f33fa40f22fc4672480a69bc8eaabd99ccc2e2c240a', 659, 3, 'Api access token', '[]', 0, '2021-05-24 05:35:02', '2021-05-24 05:35:02', '2022-05-24 11:05:02'),
('8a72715588346e878b59b74a3ec8b0544a8cf60cffb1cbc3a9de8fe02b0c99c3463d586bcbd3c6af', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:16:56', '2021-05-08 05:16:56', '2022-05-08 10:46:56'),
('8aa2d0eaa2f1c0c69f4dab45deb1a7b2b685ebc69303e2c93b46b4843a51ecb68b42d3e023ef1991', 515, 3, 'Api access token', '[]', 1, '2021-04-02 07:05:32', '2021-04-02 07:05:32', '2022-04-02 12:35:32'),
('8aa72e34c449c4fc5bf952e723e805648f4de0d5121c53022d94b127502103305f7c7ed5d73707bd', 6, 3, 'Api access token', '[]', 0, '2021-07-23 08:00:41', '2021-07-23 08:00:41', '2022-07-23 13:30:41'),
('8aaad3ab6367783d40b6fb41fb2bf933d875affd34d8394a5c444926d5fd53d08e74cc4066f71be5', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:50', '2021-05-04 01:11:50', '2022-05-04 06:41:50'),
('8aba2b1c47c76d467d78ae3b25e59829df256dffe24c5330b3c37a25a2635a9bf9f36dfdf186bd1e', 643, 3, 'Api access token', '[]', 0, '2021-05-13 08:31:45', '2021-05-13 08:31:45', '2022-05-13 14:01:45'),
('8aca1c5a61852cf9b2e4cca7e82a1837d412dde423e3f89d71340ac9836d4bad5944df45ade5ed67', 618, 3, 'Api access token', '[]', 0, '2021-05-02 08:34:22', '2021-05-02 08:34:22', '2022-05-02 14:04:22'),
('8ada188ddd7620d23aed71283a852acdcb52e61daba327d9e4162b65cd1cdd519ed8a36602aae126', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:22:14', '2021-05-11 06:22:14', '2022-05-11 11:52:14'),
('8aee78d6dc45fb1d7b8c42236bec3748752bddc3880b725fcf72a2ea3a2cc1388bb573ba9dbb0c78', 542, 3, 'Api access token', '[]', 0, '2021-04-05 06:12:06', '2021-04-05 06:12:06', '2022-04-05 11:42:06'),
('8b0140beb70e5c9e40022419c35395eab4ca15e2a7f7ed7ab6394bc0efd666b3d5eff25dd83e9423', 495, 3, 'Api access token', '[]', 0, '2021-04-01 00:30:13', '2021-04-01 00:30:13', '2022-04-01 06:00:13'),
('8b207164469591832636abb3c16b2befd090df3d4edc401ac5f500bdf175ed852026ef2552aed6ba', 44, 3, 'Api access token', '[]', 0, '2021-08-03 01:43:23', '2021-08-03 01:43:23', '2022-08-03 07:13:23'),
('8b39674bb73f6cbe7625191db6ef404ae07e15ab460c84429bdb95cbdfc9b2f7eb7d9e9d033e1294', 765, 3, 'Api access token', '[]', 1, '2021-07-12 04:45:41', '2021-07-12 04:45:41', '2022-07-12 10:15:41'),
('8b48e35ade7e16e690b61337fac6737f620572a7a4407ac96039b6337b025e014c530231da5a8b7d', 540, 3, 'Api access token', '[]', 0, '2021-04-05 04:06:37', '2021-04-05 04:06:37', '2022-04-05 09:36:37'),
('8b5c654e5985b6d0aecc02902347511a8d1bb76b0dd05a34774696a4a432226ae941b6b53c4572d0', 587, 3, 'Api access token', '[]', 1, '2021-04-29 02:24:31', '2021-04-29 02:24:31', '2022-04-29 07:54:31'),
('8b62f785c6a2f1134ec748cbd8bdd1974e8515a97ffa0d2797bfec9abf951fce7b910541bc6057c5', 625, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:51', '2021-05-04 01:11:51', '2022-05-04 06:41:51'),
('8b7341245309b34e1b75388d74a83ce4ad6dfee39602b19bcc2b9fee0b18bb382abb2b84173808cc', 620, 3, 'Api access token', '[]', 0, '2021-05-13 02:29:00', '2021-05-13 02:29:00', '2022-05-13 07:59:00'),
('8b8b6b360e04bfbf26b8e94b4023698f0c6dbab90f213b01ab1ce3775a843ac921dd75e05e8bdcd2', 17, 3, 'Api access token', '[]', 0, '2021-07-28 07:49:02', '2021-07-28 07:49:02', '2022-07-28 13:19:02'),
('8b90d2846193809e6ba61b9dde9afde5ecbbed8b6edc9d9e6ae87c1d10335d757fb6d543a21f665e', 708, 3, 'Api access token', '[]', 0, '2021-06-08 07:12:42', '2021-06-08 07:12:42', '2022-06-08 12:42:42'),
('8bb3123db8e832b72d6b5faccbfac571d5461ad4d208df6a3d85327e66efc28089504092f4ce9509', 430, 3, 'Api access token', '[]', 0, '2021-03-23 00:15:41', '2021-03-23 00:15:41', '2022-03-23 05:45:41'),
('8bd232e9e1773d1c0b08e87a8976db8f650d2a2287d088c61e743b22669a324e50358b8c1a0b17ce', 732, 3, 'Api access token', '[]', 0, '2021-06-10 02:02:14', '2021-06-10 02:02:14', '2022-06-10 07:32:14'),
('8be832dd2b37d0cda2ab1dc78b119d34afc8bcdc76ea884005432792bee1508628fbaf4435ca8ece', 672, 3, 'Api access token', '[]', 0, '2021-06-02 06:57:16', '2021-06-02 06:57:16', '2022-06-02 12:27:16'),
('8c7e755980d8110ba6903bd59258ec5a3f61052d71672f7027dec193201dcb794a2722dacc8baccf', 506, 3, 'Api access token', '[]', 0, '2021-04-01 04:17:40', '2021-04-01 04:17:40', '2022-04-01 09:47:40'),
('8c8b73394f0940bae875487e81aaf06dcadba0aea0525164732c8e8de91590761a097da494d356ae', 473, 3, 'Api access token', '[]', 0, '2021-03-26 02:21:39', '2021-03-26 02:21:39', '2022-03-26 07:51:39'),
('8c8ca89aa171367671168745022a774a57f55418a65800a25404830238daccbacca3325ecfd272d6', 555, 3, 'Api access token', '[]', 0, '2021-04-19 06:50:29', '2021-04-19 06:50:29', '2022-04-19 12:20:29'),
('8c97c8bd81199660f186083044ecb82c32f011c54f65b6219e89b4abd99a6193b2301f9f3a6c43ad', 5, 3, 'Api access token', '[]', 0, '2021-07-27 05:48:08', '2021-07-27 05:48:08', '2022-07-27 11:18:08'),
('8cc26cc9c92c481c44d295b5d1e2dd2578b89651e9c1c76d04ae0472e858c49a712cf1baa78e9f65', 2, 3, 'Api access token', '[]', 0, '2021-07-30 01:39:35', '2021-07-30 01:39:35', '2022-07-30 07:09:35'),
('8ce5555302b8d44ec938c9863023975c1b03c7cc2c0324ba0e28a68acf34f99c5030e27953847d3d', 773, 3, 'Api access token', '[]', 0, '2021-07-19 02:19:47', '2021-07-19 02:19:47', '2022-07-19 07:49:47'),
('8cea6ec3db1bec99cd14259399f1a490ed50626a79ffd2535bd971f98b273148f72e456e50918982', 13, 3, 'Api access token', '[]', 1, '2021-07-27 01:58:49', '2021-07-27 01:58:49', '2022-07-27 07:28:49'),
('8cecfc86401807cd65a1e6bc086be7bf5b84887d929e45c572bee098a3e5f9ae200982a349ebadc3', 665, 3, 'Api access token', '[]', 1, '2021-07-06 04:38:48', '2021-07-06 04:38:48', '2022-07-06 10:08:48'),
('8d0279a9cf4c0ce4d8e75b2e89ac9f9616f8cda1ba73f365dda9c4bd2cf7b81be918d7b05594532b', 51, 3, 'Api access token', '[]', 1, '2021-08-03 07:35:10', '2021-08-03 07:35:10', '2022-08-03 13:05:10'),
('8d16c37e2dbb105c0d70a5b0118397dc0b8132ea48e0eea9eaf915e7c477ffc169563cc1b4d7417c', 509, 3, 'Api access token', '[]', 0, '2021-04-02 03:33:53', '2021-04-02 03:33:53', '2022-04-02 09:03:53'),
('8d5e45014c404098ca587bdd95da73fd02bbab6116e572da3409aaac43b9502d8dbb4316961bd044', 623, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:25', '2021-05-15 06:22:25', '2022-05-15 11:52:25'),
('8d962a01fb0d62861fd41098d3c35232a3c3b5b9cd9ce62329ff6519e9a02bce1785f8cd57bc7426', 490, 3, 'Api access token', '[]', 1, '2021-03-26 06:30:34', '2021-03-26 06:30:34', '2022-03-26 12:00:34'),
('8deac87461d48d1346cef85baa61f47feb6c6d18439cb915fda9b9dd75a94d4b70536889c7583997', 587, 3, 'Api access token', '[]', 0, '2021-04-29 04:52:04', '2021-04-29 04:52:04', '2022-04-29 10:22:04'),
('8df7977f91d4d9d69c9221472c8d4fe0cc55b0ec5c9d342a9f67fd6ce700328339a89c6fbf40c481', 533, 3, 'Api access token', '[]', 0, '2021-04-14 05:50:45', '2021-04-14 05:50:45', '2022-04-14 11:20:45'),
('8dfe0edb11542b141d0fbf0eb6d3d1f203cb579a55d258e2272715a0b90db71df6d2ff6e702b83da', 685, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('8e0acdd63ef847c796df2bf8207220969dd80eed4891ecd84a8d4d09f49511a90a59cad225765550', 393, 3, 'Api access token', '[]', 0, '2021-03-18 00:07:27', '2021-03-18 00:07:27', '2022-03-18 05:37:27'),
('8e2e8a027de529bb4a0be63f75554000a16771120fef1bd4cef3e7fdaf1c870138d5ebb9e7f8d679', 753, 3, 'Api access token', '[]', 1, '2021-06-24 06:59:26', '2021-06-24 06:59:26', '2022-06-24 12:29:26'),
('8e3574937be359d85e18ab3c06a7ce0c83192e986c3949e13ba9fbfd53a414ecd8e502f5e8832937', 13, 3, 'Api access token', '[]', 0, '2021-07-29 06:27:43', '2021-07-29 06:27:43', '2022-07-29 11:57:43'),
('8e542f56c61c846e577010a5d5f1ae8e9b516dc238f7af1f5d2f277ee74201f5f89c9310e689e5ca', 403, 3, 'Api access token', '[]', 0, '2021-03-17 23:47:16', '2021-03-17 23:47:16', '2022-03-18 05:17:16'),
('8e6dbe9d1d0af1777377ff5c49e901cea6b79ca310a8c921e91acf83d567dea5fe824f42a4d79890', 694, 3, 'Api access token', '[]', 0, '2021-06-03 00:29:43', '2021-06-03 00:29:43', '2022-06-03 05:59:43'),
('8ea9baf65862d8ba41bcaa98df2dc3221529abe3efa480edbc63c55b66889d2827aacb5d82acf5bd', 32, 3, 'Api access token', '[]', 0, '2021-07-28 07:26:10', '2021-07-28 07:26:10', '2022-07-28 12:56:10'),
('8eb6cc628307d828e000a02616df65414786993fe35e764974841780f50e56a587317e59a07727bb', 776, 3, 'Api access token', '[]', 0, '2021-07-20 23:32:23', '2021-07-20 23:32:23', '2022-07-21 05:02:23'),
('8f1dbd8ae18686d2e44ad71781972e1e45e9e02525e900febb9078aa23fba981dd8a5e27ab1f8972', 23, 3, 'Api access token', '[]', 0, '2021-08-06 04:19:09', '2021-08-06 04:19:09', '2022-08-06 09:49:09'),
('8f59f667a6eff3358dc46d41f8535b860610ca36b468fe143e5c4800a2d63c5921c7387ee5bdec4f', 52, 3, 'Api access token', '[]', 0, '2021-08-04 05:07:31', '2021-08-04 05:07:31', '2022-08-04 10:37:31'),
('8f67276b0e88e0ca7a88f48c512bbaed3ccfcc658a7258291324573c79f150c2f0d91369ea53fecf', 479, 3, 'Api access token', '[]', 0, '2021-03-26 04:26:20', '2021-03-26 04:26:20', '2022-03-26 09:56:20'),
('8f86f21eb0f36e11762da4ef1df2a5e045fc8a6dc6bf26ccf96487bdb0d3ed4609e5e3595beb751f', 393, 3, 'Api access token', '[]', 0, '2021-03-18 03:52:20', '2021-03-18 03:52:20', '2022-03-18 09:22:20'),
('8f8879701abefe2b2191aa6122bc88bfcdd87b86e677a70ffe91a3d382bd9b422579e98ffe3bf21a', 524, 3, 'Api access token', '[]', 0, '2021-04-03 00:55:26', '2021-04-03 00:55:26', '2022-04-03 06:25:26'),
('8fbc24875142a6718628c205019e85b5e8a0f2822ccac4ce1faebc182d71773fe556ac8a85edf267', 434, 3, 'Api access token', '[]', 0, '2021-03-24 07:55:14', '2021-03-24 07:55:14', '2022-03-24 13:25:14'),
('8ff1e1b8398bfab33b1bc360398ccf5ece04f844f469b775bb849a03eb298f73449f1435c79efd05', 690, 3, 'Api access token', '[]', 0, '2021-06-02 23:45:20', '2021-06-02 23:45:20', '2022-06-03 05:15:20'),
('901c674355c3308d1d179d63e9e08282fc5df603ad5ae104c72971c8a1a9da132b23778565868676', 405, 3, 'Api access token', '[]', 0, '2021-03-18 06:52:24', '2021-03-18 06:52:24', '2022-03-18 12:22:24'),
('90323dde5f2fb792b030efbd0527e1bef8105d2bf7cf9226687637b814c33f065d1c4ce242c84e79', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:57:38', '2021-03-22 05:57:38', '2022-03-22 11:27:38'),
('9033e803287b248ace9172d4ad159cae49b3b483c9c4ea75d1488a4ff2bc1fea96871a9bfc7fb366', 618, 3, 'Api access token', '[]', 0, '2021-05-08 04:02:03', '2021-05-08 04:02:03', '2022-05-08 09:32:03'),
('90405212d8374ebd18676c3c99c0e645c7d51091cfb34780e8af5499891af5ce63f78c438a03cba4', 525, 3, 'Api access token', '[]', 0, '2021-04-03 06:02:09', '2021-04-03 06:02:09', '2022-04-03 11:32:09'),
('905e3e139a501d6d56419588b7e002864e16085b616da05137413165576732c54e178a0579da1a1c', 1, 3, 'Api access token', '[]', 0, '2021-08-03 00:15:29', '2021-08-03 00:15:29', '2022-08-03 05:45:29'),
('90ad55075af5be5fce7bda0bafc74d3261d6816bb72a9dff4676d08564558584838f1a33c3efeec1', 553, 3, 'Api access token', '[]', 1, '2021-04-14 07:34:19', '2021-04-14 07:34:19', '2022-04-14 13:04:19'),
('91031a1371e2811848f5f290e3c35780df3a4c4396161ec7eb03f83ec77de43d4db4b02c7c0f9d21', 1, 3, 'Api access token', '[]', 1, '2021-08-03 01:33:53', '2021-08-03 01:33:53', '2022-08-03 07:03:53'),
('917337414497c796333311dd2896a25a68d3d66cb1135453b0636aceb44e031c10f10fb40f579ecd', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:13:07', '2021-07-20 02:13:07', '2022-07-20 07:43:07'),
('91785338f5bf76b7454594b27159d2458b3936455c90bc4b18424b2ba421ec630244098242627b56', 13, 3, 'Api access token', '[]', 0, '2021-07-29 04:10:31', '2021-07-29 04:10:31', '2022-07-29 09:40:31'),
('91938cd7540653ce700240d6b25d6b1675a2071ae5189f7e11fd8f0fc6b37a29d5fa7846153d908c', 414, 3, 'Api access token', '[]', 0, '2021-03-19 01:23:43', '2021-03-19 01:23:43', '2022-03-19 06:53:43'),
('91b37d561c31a9e7bdd2d10cc2c10c75ff839f68e057949b6100897ad01bd6e4c25e455d2ee27b40', 691, 3, 'Api access token', '[]', 0, '2021-06-02 23:45:21', '2021-06-02 23:45:21', '2022-06-03 05:15:21'),
('91cef60b7e087c16aad42b882c8b703529643d8c9c5656bf21a5a08cc894eb7a7279d8324e76f489', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:52:23', '2021-08-06 05:52:23', '2022-08-06 11:22:23'),
('91d8ac1953eef06d2692a64723b4327d40c6617b54b297f75f879f1b5815079f799a2645b4096602', 744, 3, 'Api access token', '[]', 1, '2021-06-16 02:33:28', '2021-06-16 02:33:28', '2022-06-16 08:03:28'),
('91e456bf39dae942ef3a79f8905e9374b3fc5837f1eabdd90094919eb839146a1337cba68732f70d', 460, 3, 'Api access token', '[]', 0, '2021-03-24 04:43:50', '2021-03-24 04:43:50', '2022-03-24 10:13:50'),
('91f41b555d330ae87c3876d3969731b1c1f49333eb4013b0ed1e7e818a3fdf68dcc24f75b875558f', 759, 3, 'Api access token', '[]', 0, '2021-07-20 08:44:05', '2021-07-20 08:44:05', '2022-07-20 14:14:05'),
('9248ed6ea560197764c2eefc7fcf424a17edea52537e4c33bc20ae9c67fd077c6e4500bb9a4f33c2', 639, 3, 'Api access token', '[]', 1, '2021-05-12 05:52:46', '2021-05-12 05:52:46', '2022-05-12 11:22:46'),
('925cc570e3bc7ca94dddb54ffb431af90f20dd641e1dab30799df41adb285e16bf77b1f3f39438ca', 516, 3, 'Api access token', '[]', 1, '2021-04-02 23:05:34', '2021-04-02 23:05:34', '2022-04-03 04:35:34'),
('927a83776908c1b0bf8e2cca834d2347f01025ab0c3a00d6daf4c2c8bc3c710cbb257ce421541c0a', 489, 3, 'Api access token', '[]', 0, '2021-04-02 01:20:24', '2021-04-02 01:20:24', '2022-04-02 06:50:24'),
('92934b4a04c2139fd8f3c26c4fd682b3e13163d1fc0fdd9d285ca3fbb38fbc9f78980d4ca755c577', 657, 3, 'Api access token', '[]', 1, '2021-05-26 02:11:13', '2021-05-26 02:11:13', '2022-05-26 07:41:13'),
('92adc466da96d0500d4ae93d544272336ac86d72aba4e06f7a8f66e81c3b75ea276f441637a35834', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:51:08', '2021-07-20 02:51:08', '2022-07-20 08:21:08'),
('92e3129f3fff3254818a784c134968c7df70ddbf7dd637a3f540ca38cc91541f2fb08fbac2799bfb', 580, 3, 'Api access token', '[]', 0, '2021-04-20 03:40:49', '2021-04-20 03:40:49', '2022-04-20 09:10:49'),
('932e1e175343b1dd111cd12519d3e26f27f718d8fb7f7d23d454fcb808cc05a647fbfb6c79ca1be9', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:08:36', '2021-05-08 02:08:36', '2022-05-08 07:38:36'),
('933e1cc284126affb9053a9c325a0f595f17d6b32b7747d40e976e0dc83f6389dff54d28707d3845', 509, 3, 'Api access token', '[]', 0, '2021-04-02 03:33:53', '2021-04-02 03:33:53', '2022-04-02 09:03:53'),
('9368bf9707b5f2802b3e7b2cb794c71cb0ed759b8154e61119f52866b657c4a150fa11f0d1506a36', 612, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:23', '2021-05-15 06:22:23', '2022-05-15 11:52:23'),
('938562b258aa0aae19776a43e26986e9d1f872f4b1c7316666609cdb048eb2e05fa969db39726026', 612, 3, 'Api access token', '[]', 0, '2021-05-08 04:39:31', '2021-05-08 04:39:31', '2022-05-08 10:09:31'),
('93d10495d63d1b52dff5ec3fb7afc50d97eb41e994154780de8ac9a0a976d7ebdaceda7939b8e878', 433, 3, 'Api access token', '[]', 0, '2021-03-23 01:03:33', '2021-03-23 01:03:33', '2022-03-23 06:33:33'),
('93d9fb5cf7ea8cdf728e191b45a99775c2963b7a809c871265806673bf6525235418e36b6e7ed9e6', 633, 3, 'Api access token', '[]', 1, '2021-05-11 04:21:02', '2021-05-11 04:21:02', '2022-05-11 09:51:02'),
('93dbb2c41049db4e9c69211b843d11cd5b693114983c094ad40a3da15fd5c0f3096e152caa27318f', 60, 3, 'Api access token', '[]', 1, '2021-08-17 05:50:49', '2021-08-17 05:50:49', '2022-08-17 11:20:49'),
('940b27ac3ac189a1038bbb20a92aabe4813b2ec0a54123ac6b8ed1d1e042221fb887372e8a094ffb', 716, 3, 'Api access token', '[]', 0, '2021-06-08 09:44:14', '2021-06-08 09:44:14', '2022-06-08 15:14:14'),
('941d7c1a0e542630e4b442bc7881ccd21a41c52034be249ab29799694a5ce7e40c8f85241d7f1eb4', 494, 3, 'Api access token', '[]', 0, '2021-03-30 00:37:42', '2021-03-30 00:37:42', '2022-03-30 06:07:42'),
('94452cdfdc7a8884b4037dfabb71522c55a21d93578f74dd3f2dc988a00c9b5cf122cc6a06d174c0', 518, 3, 'Api access token', '[]', 1, '2021-04-02 07:20:37', '2021-04-02 07:20:37', '2022-04-02 12:50:37'),
('945f3f546781aac12257ca9aa59084b382d8697eb9879062ac1e03fbccdeb3726cd31e56b0e55ba3', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:38', '2021-05-04 01:27:38', '2022-05-04 06:57:38'),
('9464d17f467ee9fca55e4a6f3fb84690fdd2114e6a06729bdc64bdfab025899e3ccf9eb8a33755aa', 742, 3, 'Api access token', '[]', 0, '2021-06-15 04:37:56', '2021-06-15 04:37:56', '2022-06-15 10:07:56'),
('94a9570dc08beeb8e8b466a9ab77f2126cb3d09244d42e4cce6dd3c333acf96a115fc7e8de878b1c', 581, 3, 'Api access token', '[]', 1, '2021-04-20 05:52:24', '2021-04-20 05:52:24', '2022-04-20 11:22:24'),
('94b9c8d2f4f2d8bddb7c3f8def81a2563a7e6d77ad43c94edff2bfc4892e77d5a45900e2ab108ce7', 641, 3, 'Api access token', '[]', 0, '2021-05-13 01:00:11', '2021-05-13 01:00:11', '2022-05-13 06:30:11'),
('94eb39749463ea04ce289f93d48973a066407ccf259e19cd9f68f0c8830434dc8d3e23aa3ccf718b', 420, 3, 'Api access token', '[]', 0, '2021-03-22 05:13:50', '2021-03-22 05:13:50', '2022-03-22 10:43:50'),
('94fe160e22f53f810e52c03a4cbbbd597b08ce92f785eb0b07aea7c1a63e0efb5f4cd6cf03f0a038', 702, 3, 'Api access token', '[]', 1, '2021-07-16 00:36:43', '2021-07-16 00:36:43', '2022-07-16 06:06:43'),
('950e6c21c0f1848917a57330d1126f60d97a0c28c552fff5c3b08c0088ffe8d12e7a7cae7bdfd696', 42, 3, 'Api access token', '[]', 1, '2021-07-31 00:26:34', '2021-07-31 00:26:34', '2022-07-31 05:56:34'),
('952a2b998de75a079a7f30bbaf0e5665ddcc07cfac16a2d2d599cd71a871522983eac689209704b5', 729, 3, 'Api access token', '[]', 0, '2021-06-10 01:59:34', '2021-06-10 01:59:34', '2022-06-10 07:29:34'),
('9560eb0daa099c528972898741fc9b200d4131795d744e700c61a2713fb2977d9bf20ba00430b120', 7, 3, 'Api access token', '[]', 0, '2021-07-30 01:29:13', '2021-07-30 01:29:13', '2022-07-30 06:59:13'),
('95659ef75007dd7f4e9447b569d5305fdb1c2690de0724f2bd84ab93bd99b33473eabed583da12d9', 505, 3, 'Api access token', '[]', 1, '2021-04-01 02:46:36', '2021-04-01 02:46:36', '2022-04-01 08:16:36'),
('957f433b779537519f6701fd0e4afe674b4ff9ce56a47442cfc193f8950df961302a42f9da93f729', 552, 3, 'Api access token', '[]', 1, '2021-04-14 00:13:45', '2021-04-14 00:13:45', '2022-04-14 05:43:45'),
('959e1a0efbf72496f0801a7145d8ecfc5b5f931627819ef86345824c337521ec29fa9e5104089b78', 6, 3, 'Api access token', '[]', 1, '2021-08-03 07:11:13', '2021-08-03 07:11:13', '2022-08-03 12:41:13'),
('95a8a825ed513607be41f45121f1932df6865d2d241385de392f9c581ea829bd85ead0c5f5946462', 668, 3, 'Api access token', '[]', 1, '2021-06-03 00:13:55', '2021-06-03 00:13:55', '2022-06-03 05:43:55'),
('95b0528e544bb27d814322aa10056aacbec57db8d420a30d9e80cf7a2f7a2ed7670b2d2a6f6d4790', 1, 3, 'Api access token', '[]', 0, '2021-07-30 00:03:58', '2021-07-30 00:03:58', '2022-07-30 05:33:58'),
('95e21bfb69dc0d3728442c6693d45cec21d92bbe2172cac9e1ae1b8c6698360212fd681a9e609b8b', 495, 3, 'Api access token', '[]', 0, '2021-04-01 04:32:19', '2021-04-01 04:32:19', '2022-04-01 10:02:19'),
('95e651cdfdad61bcd3f903adfa0e0105ed6dc4603425eae9398e1e6df400b08811142369bf3baaca', 734, 3, 'Api access token', '[]', 0, '2021-07-15 06:39:41', '2021-07-15 06:39:41', '2022-07-15 12:09:41'),
('960bcd1702d5c0406814a76c68adc47900604ebbdc047bb5bf62d483fe4d01beed409362c6136f9b', 449, 3, 'Api access token', '[]', 0, '2021-03-24 22:58:58', '2021-03-24 22:58:58', '2022-03-25 04:28:58'),
('9611ac797010cbf17c84e446d361bfd130d9831abd7eb72094683abf6466cbcf005060e91e0f2977', 531, 3, 'Api access token', '[]', 1, '2021-04-05 23:53:41', '2021-04-05 23:53:41', '2022-04-06 05:23:41'),
('96207c88d47a247a1cda3e6b92a1cc2fb7a4359570bba344339d6226898df54f67fe938e382074e1', 597, 3, 'Api access token', '[]', 0, '2021-05-02 07:40:15', '2021-05-02 07:40:15', '2022-05-02 13:10:15'),
('962bd9363f3771a5ddae6b896de9ca689c66780327abaa4f671ba2f1aa0114713a4b093bbe155fec', 508, 3, 'Api access token', '[]', 0, '2021-04-01 06:58:26', '2021-04-01 06:58:26', '2022-04-01 12:28:26'),
('964c92655df2838a983d1eb13b5790ddb6cd7b403ba9bee2809497275571c45a44800f70110def88', 60, 3, 'Api access token', '[]', 0, '2021-08-13 01:15:39', '2021-08-13 01:15:39', '2022-08-13 06:45:39'),
('966fc49bba8d22ddf997b4d0dcd05c9f3a455800bae06273b2119b667a63eaf7b6c399d7d65a714c', 734, 3, 'Api access token', '[]', 0, '2021-07-15 06:40:33', '2021-07-15 06:40:33', '2022-07-15 12:10:33'),
('9694b9f6224bf6262783d0bffd0bacf184d567cd499e545edd74119638ee56748ea866145dceabcf', 638, 3, 'Api access token', '[]', 0, '2021-05-12 08:14:58', '2021-05-12 08:14:58', '2022-05-12 13:44:58'),
('969889368f8a5f462b54c4871d6deaba95733a0d486fb4f8f3d9f8a2e3093e2a8741c41a4bf9bb5d', 23, 3, 'Api access token', '[]', 0, '2021-08-04 05:40:54', '2021-08-04 05:40:54', '2022-08-04 11:10:54'),
('96a158c4b74a39326240c8186ec36c02e64235c54e019ab17fd425740f7858cf7416e248f8517b45', 772, 3, 'Api access token', '[]', 0, '2021-07-16 01:52:34', '2021-07-16 01:52:34', '2022-07-16 07:22:34'),
('96ad7bd7b8ae8958c715a7a10fde358fb32a26e36f76915d0ca87f881264e28a0b8e7cbefecad003', 502, 3, 'Api access token', '[]', 0, '2021-03-31 04:52:08', '2021-03-31 04:52:08', '2022-03-31 10:22:08'),
('96c6e15e5e09e44c00c89b00856ea1329f876808b81a011fbf3324cc4d7f02ecb839e3809155f203', 463, 3, 'Api access token', '[]', 0, '2021-03-24 06:13:24', '2021-03-24 06:13:24', '2022-03-24 11:43:24'),
('9719ad6edcfe6ac6e82121633c34cb0059b171fc198aa46051313f9fe6f5883559f8821555fb97f9', 754, 3, 'Api access token', '[]', 0, '2021-06-24 07:52:07', '2021-06-24 07:52:07', '2022-06-24 13:22:07'),
('9738824b38723596c5e2d575fc4f90641127331a7830b6b1172308bf09054a4fa4296c94d282e439', 533, 3, 'Api access token', '[]', 1, '2021-04-05 02:26:55', '2021-04-05 02:26:55', '2022-04-05 07:56:55'),
('97a5e9e21356be64d5c2f3f7c157cdc7df16c945682254ea518e7de54ab59374a9949d57d0fd9cf2', 486, 3, 'Api access token', '[]', 0, '2021-03-30 23:29:16', '2021-03-30 23:29:16', '2022-03-31 04:59:16'),
('97c8c28f22420f5bcef3fe760205d3f7c334836bbf01ee0f252533b4a295f21c3b7c59a225f8da7f', 513, 3, 'Api access token', '[]', 1, '2021-04-02 06:47:30', '2021-04-02 06:47:30', '2022-04-02 12:17:30'),
('97eca43b290c04e9583fd4de880ab88e58de9406400f5d1df26fb14b70c9c04cc5f0e04eb2a91cbc', 40, 3, 'Api access token', '[]', 0, '2021-07-29 01:31:59', '2021-07-29 01:31:59', '2022-07-29 07:01:59'),
('97ee9b7b0855ad4ffc1ef3e95a8fbb6403450f845d857932390be3c25de6119b18ca7ee6c01cabc0', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:36', '2021-05-02 08:31:36', '2022-05-02 14:01:36'),
('9826ab02ea7cac99876d402402f61d2adb979170cd3a452a506cb811369b29c953d6366ea219226d', 1, 3, 'Api access token', '[]', 0, '2021-08-02 01:02:26', '2021-08-02 01:02:26', '2022-08-02 06:32:26'),
('984c93d152aafd2bb4f92ace20beaf9b4c8085ec2e4cab6f810cd13543caca2402dd87c3c93dadc5', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:37:13', '2021-05-08 05:37:13', '2022-05-08 11:07:13'),
('987000309c8875854a3220e47df9fdc7ef12fb82f43f7c5c5442b8fee111ddcf9607e7e49206f775', 613, 3, 'Api access token', '[]', 0, '2021-05-02 08:11:13', '2021-05-02 08:11:13', '2022-05-02 13:41:13'),
('987fb25a917d17393a5498f33a9e555c4bd214ad401756e94e092d22bbe9263c61f6397c231e48c9', 24, 3, 'Api access token', '[]', 0, '2021-07-29 07:10:54', '2021-07-29 07:10:54', '2022-07-29 12:40:54'),
('98d318776a2c0702d05ab2b24f78d277dc501d843526e8edc265806349edeca7dd86d62eeb25f82f', 692, 3, 'Api access token', '[]', 0, '2021-06-03 00:12:54', '2021-06-03 00:12:54', '2022-06-03 05:42:54'),
('98f281c4b0a496bd458f3903d8b056eb91f13cc4b966cf38f7dd35100c3f400812033c37dff55de5', 443, 3, 'Api access token', '[]', 0, '2021-03-23 23:08:49', '2021-03-23 23:08:49', '2022-03-24 04:38:49'),
('98f31e605b4e9f764163803b4ec797314c8d105f2acf401d4dd60150874792896b41887da4552255', 555, 3, 'Api access token', '[]', 0, '2021-04-15 06:58:42', '2021-04-15 06:58:42', '2022-04-15 12:28:42'),
('98f5576b5cc3d080c95a8abddc771cc11c2d32769b40382a1c943e74c8167e04e7361e6e63545e5e', 444, 3, 'Api access token', '[]', 1, '2021-03-23 02:53:57', '2021-03-23 02:53:57', '2022-03-23 08:23:57'),
('98fbb6b0118606bee7724020aca153217e92f67fc6b401fe27600c03d8052b1ae9a0bd7801987847', 486, 3, 'Api access token', '[]', 1, '2021-03-31 00:15:26', '2021-03-31 00:15:26', '2022-03-31 05:45:26'),
('991d454996ad37cf5ade9b76861f35c6f26aaaf819d9c2aad3798435d845db28f2a1d666b605a510', 705, 3, 'Api access token', '[]', 0, '2021-06-04 05:09:32', '2021-06-04 05:09:32', '2022-06-04 10:39:32'),
('993de87fa73248321c69d79254b1aa64c968c44993e329150af314ce22c771d66523c31d16c30acf', 673, 3, 'Api access token', '[]', 0, '2021-06-02 07:01:36', '2021-06-02 07:01:36', '2022-06-02 12:31:36'),
('999c9dcd779c0a7fd5b12bb06801c9b7f5fd4ca21e9e471f498f5bc5e7f98cb75220b9bd5ea5a8d6', 23, 3, 'Api access token', '[]', 0, '2021-08-05 07:12:10', '2021-08-05 07:12:10', '2022-08-05 12:42:10'),
('99ab78df9f6b24342bdcd3418933f216ebd0c1e3f5d647cb587feac1c95fdd1021259a7ef9947623', 675, 3, 'Api access token', '[]', 0, '2021-06-02 07:18:58', '2021-06-02 07:18:58', '2022-06-02 12:48:58'),
('99c9311692be841a054427953ef20aea01d3f0a9dc063cb92a6f2735b0a4aa77f6249c0249c51008', 531, 3, 'Api access token', '[]', 0, '2021-05-13 07:33:14', '2021-05-13 07:33:14', '2022-05-13 13:03:14'),
('99d6f3d73c42f7f21b2c6067abf315da0df870bd8eb8f178d3f8043a2eec6a91a756df831a4a5bd2', 21, 3, 'Api access token', '[]', 1, '2021-07-27 07:36:38', '2021-07-27 07:36:38', '2022-07-27 13:06:38'),
('9a097b11701ec3376a98a79fbc092fe4d02c55aafc75803ce88727e813d2df789af701f0667f5ab2', 531, 3, 'Api access token', '[]', 0, '2021-05-13 02:43:02', '2021-05-13 02:43:02', '2022-05-13 08:13:02'),
('9a0eaee956e1062bc97999bd0f0545280a0ca1ac11985fbbfbe4588012677b9b7df808d5c44de1f8', 608, 3, 'Api access token', '[]', 0, '2021-05-02 07:56:18', '2021-05-02 07:56:18', '2022-05-02 13:26:18'),
('9a3c5138ae4ce26e638c500dba23a74fd69b4d018f1c45267dd20c13d251e74e5da79f02d4f97d44', 13, 3, 'Api access token', '[]', 1, '2021-07-27 02:57:40', '2021-07-27 02:57:40', '2022-07-27 08:27:40'),
('9a4212e7098c3a642f42b7a824b8e24df0a7cbcc721737dcbf0d5ee6307734e90736d4c0ceec3b60', 697, 3, 'Api access token', '[]', 0, '2021-06-03 00:35:50', '2021-06-03 00:35:50', '2022-06-03 06:05:50'),
('9a4c88fed69f7b14fede27095e9cfbecff0282f130d90bad4f7f0061f228bb10bc01c45a3a933ffd', 770, 3, 'Api access token', '[]', 0, '2021-07-16 00:36:18', '2021-07-16 00:36:18', '2022-07-16 06:06:18'),
('9a65745eacb2bb8268214641127e815027f6a8a14ebfe11e1b38c1fdd7aabd92dae997e25f3d2ea2', 625, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:06', '2021-05-04 00:52:06', '2022-05-04 06:22:06'),
('9a6b62e541871b0e61b8a3ab770742d9b3b2f9e4143e50fa913d933df7a8cc87314134694bc3639b', 761, 3, 'Api access token', '[]', 0, '2021-07-05 06:18:17', '2021-07-05 06:18:17', '2022-07-05 11:48:17'),
('9ad21a215d59398bcbac11c3aa8e6ff74c2d5b53a6c4a7e7ad21395acd0d69ccb561cf0137cf07b5', 500, 3, 'Api access token', '[]', 1, '2021-03-31 02:49:30', '2021-03-31 02:49:30', '2022-03-31 08:19:30'),
('9ae59a8249a288137fe4c078f4b12ba958d3f98e4b2d719b7972826ad623cfc186b6be998e2d49e4', 499, 3, 'Api access token', '[]', 1, '2021-03-31 02:31:02', '2021-03-31 02:31:02', '2022-03-31 08:01:02'),
('9ae8545841d46f368cfece11eb0f83acd8c22da78305fadc093a9e2ac70b7f114b19b00d5b545de0', 646, 3, 'Api access token', '[]', 0, '2021-05-14 05:07:01', '2021-05-14 05:07:01', '2022-05-14 10:37:01'),
('9b0b15a21f765798b283d258ed2c43c58f32471d89c6ad784f0f7800d020aa24f12fa4acbc86b4bd', 19, 3, 'Api access token', '[]', 0, '2021-07-29 07:18:59', '2021-07-29 07:18:59', '2022-07-29 12:48:59'),
('9b38c93e6f6faf8d1abea3f8bff708349c679d19395e621420d6a4792e414bca87432b8f119632b7', 7, 3, 'Api access token', '[]', 1, '2021-07-30 01:30:19', '2021-07-30 01:30:19', '2022-07-30 07:00:19'),
('9b5f862dbc4715f6be954671d95f7a5baa1885f39be6b349777adb07beabaa4361e3771199c400a8', 19, 3, 'Api access token', '[]', 1, '2021-07-28 05:30:50', '2021-07-28 05:30:50', '2022-07-28 11:00:50'),
('9ba5524aecfd7a83573ad08fd0d23433098282ceecb5f374c1028eb955a89210e897fcfdd37450a7', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:52:09', '2021-05-04 06:52:09', '2022-05-04 12:22:09'),
('9bb5f4a0fc69754cf916aab5331c99bcf180fa1b8deaebfe176857a5a60769acd99e60e57bcb0a09', 53, 3, 'Api access token', '[]', 0, '2021-08-05 02:59:05', '2021-08-05 02:59:05', '2022-08-05 08:29:05'),
('9bd480d37bcd58fba5f651fa1519bc72726e5239bbac1b0ae38b9b60da85ebaf527694c8d2233e4b', 626, 3, 'Api access token', '[]', 0, '2021-05-04 06:58:55', '2021-05-04 06:58:55', '2022-05-04 12:28:55'),
('9be4fe13721090709db37c851ead46c51e588e74d33cd030f204e26406e7edb00a102efe71ef1f11', 655, 3, 'Api access token', '[]', 1, '2021-05-24 04:26:36', '2021-05-24 04:26:36', '2022-05-24 09:56:36'),
('9c32326bd84eb3ce9cedf7d46a48a0234c4a8cb3988d6a26ebb97a6ed29edb33d2554a8ad7bc865d', 487, 3, 'Api access token', '[]', 1, '2021-03-26 06:12:55', '2021-03-26 06:12:55', '2022-03-26 11:42:55'),
('9c535f1f030e06ee049f8a4428b13270ccfda9433f7a796a41c6ad41b4ef365de2a44413ee22c8c3', 458, 3, 'Api access token', '[]', 0, '2021-03-24 04:15:19', '2021-03-24 04:15:19', '2022-03-24 09:45:19'),
('9c59078b6e06b5add303c6876aef5880e2238d9f7b7851d40818cf5e6ee307fa28cb3c4f9a135496', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:25:57', '2021-05-11 00:25:57', '2022-05-11 05:55:57'),
('9c673073f7668c843231d61bbb49bb1de55f9335d7de4f153648ee6872425beb58f7fd53e1605b61', 625, 3, 'Api access token', '[]', 0, '2021-05-13 02:28:58', '2021-05-13 02:28:58', '2022-05-13 07:58:58'),
('9ca563d94e2ae7463ee67647ae0c6e0448b01642b8788ffd78772ee729b65685c8bbcc06578cdc39', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:20:44', '2021-05-08 02:20:44', '2022-05-08 07:50:44'),
('9ca8754b399c811a95217701994cd951fbc7b96d09d4e216a67084087acc84ae03e690f187f95854', 773, 3, 'Api access token', '[]', 0, '2021-07-20 08:47:52', '2021-07-20 08:47:52', '2022-07-20 14:17:52'),
('9cbfb48868b2149af8a41f3bd998301f286fbb701b4e62dbf87daf888f7c6b550c7afa3b50827cec', 524, 3, 'Api access token', '[]', 1, '2021-04-03 01:13:46', '2021-04-03 01:13:46', '2022-04-03 06:43:46'),
('9d17d65456ea029783b9fac2515cb528efd0d4022fcd544d87ac16133086421d6e1b8f90521c55ec', 456, 3, 'Api access token', '[]', 0, '2021-03-23 22:52:12', '2021-03-23 22:52:12', '2022-03-24 04:22:12'),
('9d2b3770eb729fad170ebc51a501d78c95f4ad4a603b04fb465eb5f3b2a81c2c6f1a16b95308d16e', 767, 3, 'Api access token', '[]', 0, '2021-07-14 05:19:23', '2021-07-14 05:19:23', '2022-07-14 10:49:23'),
('9d7f655c2a316cc4fcb4c0e9d084d3544659661f60e50cf9f4184d65dad31666788efc3cba51a687', 542, 3, 'Api access token', '[]', 1, '2021-06-16 23:39:11', '2021-06-16 23:39:11', '2022-06-17 05:09:11'),
('9d8472d702b01a5f24f0e0063793c5625f548b930aece6e29d9a465a5c649c61ccda675edeeef505', 5, 3, 'Api access token', '[]', 0, '2021-08-07 01:58:23', '2021-08-07 01:58:23', '2022-08-07 07:28:23'),
('9dd662180064ec04a8089d3152ce1b5345841ff59c2b75a577cdbf29054192119cf60ae59a958e7f', 1, 3, 'Api access token', '[]', 0, '2021-07-30 00:43:43', '2021-07-30 00:43:43', '2022-07-30 06:13:43'),
('9de6f4c5bb4e789b0c7d73f5a489b8c6ef89eec6cf2d9f9ce017ab9337962e0e3482e8370962c42d', 63, 3, 'Api access token', '[]', 1, '2021-08-15 12:15:45', '2021-08-15 12:15:45', '2022-08-15 17:45:45'),
('9ded5e28399cebba0bb3317be8c780fba464ec21911fa3a8867c6ff90bb38fa47bb4cebec337d6bc', 568, 3, 'Api access token', '[]', 1, '2021-04-15 23:38:17', '2021-04-15 23:38:17', '2022-04-16 05:08:17'),
('9df9886049e2246a789ea27796cea9e805368d99c4ba173d48278f35f0a0df79d8e2b835a87c508e', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:52:20', '2021-05-04 06:52:20', '2022-05-04 12:22:20'),
('9e0d14fdad334768ddabf578c6d41f77d12a2cecd9598643bbdcc5b0805c3299955311b441cb3b32', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:34:23', '2021-05-02 08:34:23', '2022-05-02 14:04:23'),
('9e1138e081706e628ccc1a613483ef816a56b36ba95d27cd9fa2b398356581a5c3371c11eb650971', 13, 3, 'Api access token', '[]', 1, '2021-07-27 04:34:42', '2021-07-27 04:34:42', '2022-07-27 10:04:42'),
('9e1516b8e4c5b5a72c1809fbbad24886545e3905fb06ca32159d72900b7378e47f8e5da8ccc78407', 533, 3, 'Api access token', '[]', 0, '2021-06-08 09:16:44', '2021-06-08 09:16:44', '2022-06-08 14:46:44'),
('9e192d0fcc690c8c2a263bd670096b1a6bcc7b576307af5f69c95d05787ede4de29b3c277b6861c4', 516, 3, 'Api access token', '[]', 1, '2021-04-02 21:33:59', '2021-04-02 21:33:59', '2022-04-03 03:03:59');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('9e301fec393a1113b4c1ad16734bd81de7f1e2e12b587b3a86185c3aceedb62537b7ff4ea30ad71a', 537, 3, 'Api access token', '[]', 1, '2021-04-05 02:23:52', '2021-04-05 02:23:52', '2022-04-05 07:53:52'),
('9e4c9c0b5a06d6a56040761d11a073957363161229c4564a5721eb03f910edd84f6c4d451fc5a4f5', 759, 3, 'Api access token', '[]', 0, '2021-07-20 03:10:45', '2021-07-20 03:10:45', '2022-07-20 08:40:45'),
('9e916cf4a7fb10c4a18f0559a249a030821aed7ad8a4b097461b9a156b58efe7357b498a4eda948f', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:04:43', '2021-07-26 07:04:43', '2022-07-26 12:34:43'),
('9e96b9587e565648fa56d1d170f569a11690e5512abb6ed23feaa0845130229ef645e152e379efba', 571, 3, 'Api access token', '[]', 1, '2021-04-19 06:43:51', '2021-04-19 06:43:51', '2022-04-19 12:13:51'),
('9eccce909a1ff99b9af8796a0027d50c23fb8614a9fe32e9247f51acfb3a3b75c9ceda84e72d4bbd', 525, 3, 'Api access token', '[]', 1, '2021-04-03 07:45:00', '2021-04-03 07:45:00', '2022-04-03 13:15:00'),
('9f2aa09e99dfe98046abd6575770b6aa269492d900013d63d98eec777bafbf10a917dcc8d141f2a0', 589, 3, 'Api access token', '[]', 0, '2021-05-06 04:22:29', '2021-05-06 04:22:29', '2022-05-06 09:52:29'),
('9f2e5c43cf25e70290ded6df23e4e29c89fc15bc9a12d8cd7aeac75b119a359d9ea2173fcbbdec13', 462, 3, 'Api access token', '[]', 1, '2021-03-24 05:52:32', '2021-03-24 05:52:32', '2022-03-24 11:22:32'),
('9f3ca9d36c82ef9b5fd6e40bd28ff2ad3fa313571bad26873f7ca91f27c535937a1cda12d575ac94', 618, 3, 'Api access token', '[]', 0, '2021-05-12 03:02:20', '2021-05-12 03:02:20', '2022-05-12 08:32:20'),
('9f4df7d4582d848b68743b50333567a5eab01600837826cc997293aa9a4982af22a379b47771c7ad', 685, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:15', '2021-06-02 08:17:15', '2022-06-02 13:47:15'),
('9f56b1046429b74b6e15835236dde18e781f742562a6845710c64bc91d9ee51cae648f3432e7682d', 757, 3, 'Api access token', '[]', 0, '2021-06-30 08:56:38', '2021-06-30 08:56:38', '2022-06-30 14:26:38'),
('9fa1cd8358c945b28376c8c5397871fbd80107a40689abe5fe73dc89f87b318067e33691000b7dc3', 3, 3, 'Api access token', '[]', 1, '2021-07-22 06:52:37', '2021-07-22 06:52:37', '2022-07-22 12:22:37'),
('9fa23a3492276c96a5a242291949b73967b0c812b20697b1dd8ca31f0d82041b228e8c0a424889d2', 732, 3, 'Api access token', '[]', 0, '2021-06-10 02:02:14', '2021-06-10 02:02:14', '2022-06-10 07:32:14'),
('9fcc33d869b6112b11557073a80006c408df473726a18f6b8c01b689fce917de436091acb482988c', 49, 3, 'Api access token', '[]', 0, '2021-08-03 05:04:03', '2021-08-03 05:04:03', '2022-08-03 10:34:03'),
('a0038b34df0e8dde8c8cf415870d42cc09a916387f93a6b98419e18335a5e9dd492eb002a5979efd', 12, 3, 'Api access token', '[]', 0, '2021-07-26 07:16:11', '2021-07-26 07:16:11', '2022-07-26 12:46:11'),
('a03e7b850e0cdf2b0d3a0e391f3013e29368f10a085ac952fa835c66624a72b7034d69dc52288208', 626, 3, 'Api access token', '[]', 0, '2021-05-27 05:24:31', '2021-05-27 05:24:31', '2022-05-27 10:54:31'),
('a076dd00fe291d8b16f0fb5017cd10c06841cf720fa6026e9ae23f1f3549177798cf585c95889963', 652, 3, 'Api access token', '[]', 0, '2021-05-17 04:48:22', '2021-05-17 04:48:22', '2022-05-17 10:18:22'),
('a0832fd802afdb1e543616719f8b232ef67d51c051252a656666a52c37f9588176d9d146b448d985', 59, 3, 'Api access token', '[]', 0, '2021-08-15 12:04:36', '2021-08-15 12:04:36', '2022-08-15 17:34:36'),
('a08f14432368f06a5a3848de0d711576bc6c46bc81e616396bf1b7ae54e25c91f8e6effee8ab185e', 560, 3, 'Api access token', '[]', 0, '2021-04-15 01:04:49', '2021-04-15 01:04:49', '2022-04-15 06:34:49'),
('a0a3749d251d49cb5f654fdb73ed3867f53483df9178422972b000b5d97096de03d5f635be99f8cb', 630, 3, 'Api access token', '[]', 0, '2021-05-08 00:41:23', '2021-05-08 00:41:23', '2022-05-08 06:11:23'),
('a0b2423f928b5c8a4fef0bbd17230dcf1f310a9cc00692206012062c6462415a32ef9a4207522998', 423, 3, 'Api access token', '[]', 1, '2021-03-22 05:36:16', '2021-03-22 05:36:16', '2022-03-22 11:06:16'),
('a0bc6f5fe38f8bb61b84eef3add9c6097f839b130769396176712c61b1830de8473c8bde26975483', 699, 3, 'Api access token', '[]', 1, '2021-06-03 00:40:57', '2021-06-03 00:40:57', '2022-06-03 06:10:57'),
('a0d17771ebffb0b42cfd898e4db9f3cb4157d98e440076e37c4e256b9b01d9b8f281ef8f3106f7c2', 473, 3, 'Api access token', '[]', 0, '2021-03-26 02:21:39', '2021-03-26 02:21:39', '2022-03-26 07:51:39'),
('a0d2f035330b2c0731fa2c5a950feaeea7751c3690e59f6657209b70ef28031f401348a2c8465981', 585, 3, 'Api access token', '[]', 0, '2021-04-21 22:51:23', '2021-04-21 22:51:23', '2022-04-22 04:21:23'),
('a0e74e5a42097e5c6a30b5c4e523b843aa22bcb4ce933909d5a879d81978ac9a0abd167cd6ac7e83', 620, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:12', '2021-05-13 08:41:12', '2022-05-13 14:11:12'),
('a10a02b9c5325fd42f2d58ede7b8a8e84c4b41f7ed8bd84c12a557e1ea3baf1c392239e38d393fa1', 7, 3, 'Api access token', '[]', 0, '2021-08-04 02:43:15', '2021-08-04 02:43:15', '2022-08-04 08:13:15'),
('a1258ef961e5a7f9e657c504cf79c19e3f814decff8a30826fdf110cec4c417ffea76dca5c7b45d1', 436, 3, 'Api access token', '[]', 1, '2021-03-23 02:56:19', '2021-03-23 02:56:19', '2022-03-23 08:26:19'),
('a12b84224ab1f2cfc21c15b940403bb003d8dc7c6c6f3f2938a24d8e4d5f289c84af0adc7bd01b0a', 486, 3, 'Api access token', '[]', 0, '2021-03-26 06:49:01', '2021-03-26 06:49:01', '2022-03-26 12:19:01'),
('a1405c51c77516a1fafc0b54398b546f3156f63831a10892906154a990de4e0bb03e3dbf4d1d9a8e', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:10:01', '2021-07-20 02:10:01', '2022-07-20 07:40:01'),
('a16cdb0a23af7f30415bd64c4da8e0935ac09b91f7ef0ab8446bd12cb1a10e2c252df5d8486e9103', 506, 3, 'Api access token', '[]', 0, '2021-04-01 04:17:40', '2021-04-01 04:17:40', '2022-04-01 09:47:40'),
('a19340dc381164a7893876cb5e1976492b43f7b7a847a610919e267e64107123c153442665a7476b', 460, 3, 'Api access token', '[]', 0, '2021-03-25 01:29:20', '2021-03-25 01:29:20', '2022-03-25 06:59:20'),
('a1c43d4ec94356cd9386380dcd7dcc8e645f417b51121d22b92d685110a38b9da27e7e96e4fdd567', 551, 3, 'Api access token', '[]', 0, '2021-04-14 00:05:51', '2021-04-14 00:05:51', '2022-04-14 05:35:51'),
('a1d45218f72d04c2b347d158c8c1a8c7d534329a34364d9743c42d66ba4e7792835085394971d64f', 55, 3, 'Api access token', '[]', 0, '2021-08-06 23:33:35', '2021-08-06 23:33:35', '2022-08-07 05:03:35'),
('a1e285b9c89d3be9fb088cf78816f5b67875a1772bf16c6f16e86272cfe5747668f64792b95c1a7d', 752, 3, 'Api access token', '[]', 0, '2021-06-24 06:49:44', '2021-06-24 06:49:44', '2022-06-24 12:19:44'),
('a1eadbf131e4687e791326ce0eaee519bdcf9f9e20a0780cbfce16db444d2b0957c9da02aa742804', 622, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:30', '2021-05-07 07:04:30', '2022-05-07 12:34:30'),
('a27c2f5758e18d90fc6bc613cbdcfe1bdc19f23ce5284307b2e67853f331efa6db68d9fa2908b0b4', 7, 3, 'Api access token', '[]', 0, '2021-07-30 01:48:26', '2021-07-30 01:48:26', '2022-07-30 07:18:26'),
('a290498e1ceb472c400ab5762e8eb266ca5a8e8e5b9a3d8a982554604f7da93ac7d46afd2ad77ffd', 424, 3, 'Api access token', '[]', 0, '2021-03-22 05:38:44', '2021-03-22 05:38:44', '2022-03-22 11:08:44'),
('a2921125e9c5c7c933e2dfefa094865e72568ab88374428eb1fa8efcbdba9fbcab2bcd73ad4ca75d', 637, 3, 'Api access token', '[]', 0, '2021-05-12 04:56:24', '2021-05-12 04:56:24', '2022-05-12 10:26:24'),
('a2b8bd9c7e16d335e193d23ec79f5721224026de18bd1cf9b09d503127c600b181aa415a678ba859', 735, 3, 'Api access token', '[]', 0, '2021-06-15 00:57:13', '2021-06-15 00:57:13', '2022-06-15 06:27:13'),
('a2c59f23039807caf49d1d4b2978cab8e34e7a154a7c4267a91b1de2245caa0dc54bc7ffa9dc9089', 24, 3, 'Api access token', '[]', 0, '2021-07-27 23:56:13', '2021-07-27 23:56:13', '2022-07-28 05:26:13'),
('a2f57ee078697df33b07853e41efe88440093427448a30a1e2b86a0112d2d51cf912f6e1ac065195', 612, 3, 'Api access token', '[]', 0, '2021-05-08 04:52:25', '2021-05-08 04:52:25', '2022-05-08 10:22:25'),
('a2fd3b2a13adf86c6a6df6cef173fc895b6851c256a37f458e44e233a26b86faf47974c615c429fe', 20, 3, 'Api access token', '[]', 0, '2021-07-27 07:21:54', '2021-07-27 07:21:54', '2022-07-27 12:51:54'),
('a374dfa266e14514e65817a879e7f6fe7748869de774e57ae79135f743a781900def0943ed8fb5b3', 27, 3, 'Api access token', '[]', 0, '2021-07-28 07:03:23', '2021-07-28 07:03:23', '2022-07-28 12:33:23'),
('a37cc1b829ca0399a50bef9bd5e84a40ce2b0707cc83b0a10257e0c12de653326bc69df3e9cfdda3', 581, 3, 'Api access token', '[]', 0, '2021-04-20 05:52:24', '2021-04-20 05:52:24', '2022-04-20 11:22:24'),
('a399878eb0c39bd9393ea940356027aa3bdc11e56640472693a111ff98bb4cc9e3305452f4ce6b07', 607, 3, 'Api access token', '[]', 0, '2021-05-02 07:55:26', '2021-05-02 07:55:26', '2022-05-02 13:25:26'),
('a3a5d90dda0e7598cb5312f1a77149d5efd6a3cb41968c202eae5ad0a5c001eaf7f1e022f0637819', 467, 3, 'Api access token', '[]', 0, '2021-03-25 04:22:58', '2021-03-25 04:22:58', '2022-03-25 09:52:58'),
('a3a81dad07826530c88f341ad85568f404f99ba445e7c48b88ebc3ed8b5cd4f063f375bcd850d796', 548, 3, 'Api access token', '[]', 0, '2021-04-09 13:30:26', '2021-04-09 13:30:26', '2022-04-09 19:00:26'),
('a3db0a21fefbefe25b476c29cd1a1cf219a2acd08942e72ec441e5bebe37a42b304e165e199d1bcb', 665, 3, 'Api access token', '[]', 0, '2021-07-02 04:30:34', '2021-07-02 04:30:34', '2022-07-02 10:00:34'),
('a3ffb62f56954656452ace2c4aa08c197f97f2be682a7912c9ca3acfa3aeca850d75f0a859119e51', 671, 3, 'Api access token', '[]', 0, '2021-06-02 06:48:34', '2021-06-02 06:48:34', '2022-06-02 12:18:34'),
('a412e8f897ced895bcfcad5223316f9127e8210e6b242f457427e0957958ffd55153571c10031e10', 659, 3, 'Api access token', '[]', 1, '2021-05-28 07:11:29', '2021-05-28 07:11:29', '2022-05-28 12:41:29'),
('a44857e1398bb6afa69b780adc66fb08c7af3503f0b70edcadfdbfcec5b64fa251653a365dbf3c23', 5, 3, 'Api access token', '[]', 0, '2021-08-07 01:57:17', '2021-08-07 01:57:17', '2022-08-07 07:27:17'),
('a44c6fdb01ee4f187f69bd6fccd0cad15b18f5a8e583754c28e95e6b24f6ae99acc0b648290dd67a', 13, 3, 'Api access token', '[]', 0, '2021-07-29 06:58:22', '2021-07-29 06:58:22', '2022-07-29 12:28:22'),
('a4508ea3fc9992612bdf43b0c64e122d77a741761403db1528b184467ada26b2513089161298c6a0', 534, 3, 'Api access token', '[]', 1, '2021-04-05 01:59:08', '2021-04-05 01:59:08', '2022-04-05 07:29:08'),
('a487c0a7612528b962c764e7f23da72a4953c5255f2be0d8dc50dcc3c3f05d676c211c311c387a13', 3, 3, 'Api access token', '[]', 1, '2021-07-30 05:31:24', '2021-07-30 05:31:24', '2022-07-30 11:01:24'),
('a4912a5124b85cb9c2246c7147c2729abe3ea8e99a4aa5ea74aa2e8558f194d245244047954e3b41', 764, 3, 'Api access token', '[]', 0, '2021-07-12 00:19:37', '2021-07-12 00:19:37', '2022-07-12 05:49:37'),
('a4ac1c6643251abe04333ce666c9ec6337e7f37f1a5059df20c335383b23edcd6c3d17ae74b2af51', 25, 3, 'Api access token', '[]', 0, '2021-07-28 05:29:49', '2021-07-28 05:29:49', '2022-07-28 10:59:49'),
('a4bdcca600b686618b05a41986ebc83bdefb7e012d8fa389f29eba676b51124d52c129ee3b966f44', 474, 3, 'Api access token', '[]', 0, '2021-03-26 02:22:27', '2021-03-26 02:22:27', '2022-03-26 07:52:27'),
('a4bee3a14adaad8eae61b638ecff665ae14fad95f9bdc6f737451faaefdc30cbab9cf1f2a973f2f6', 448, 3, 'Api access token', '[]', 1, '2021-03-24 04:16:22', '2021-03-24 04:16:22', '2022-03-24 09:46:22'),
('a4da112e01ef5c4082f2f4121eb6b33e28e5d01960d0851aaee9c2c8383205c278edffb2f725542e', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:59:53', '2021-07-15 07:59:53', '2022-07-15 13:29:53'),
('a50dce3f1772db05a59ea40bed81023881e91357e80b9a2c4cee488538d233aa8d757f2fa5e1a0bf', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:49', '2021-05-02 08:41:49', '2022-05-02 14:11:49'),
('a52c4e5b73cd7dd9d3a42ffd6c0b1885243c377d5196882819f4017e658cb0af78d35f74260cb7cc', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:43:19', '2021-08-06 05:43:19', '2022-08-06 11:13:19'),
('a5324766b0a0c9e96c7ca6065cd9777e7de0ac549033876fd0e168d910172d7870945b9b21aaeddb', 481, 3, 'Api access token', '[]', 0, '2021-03-26 04:54:29', '2021-03-26 04:54:29', '2022-03-26 10:24:29'),
('a53e592ef37995594d75ac53e730e5ceb7b76b8fe074c8baed4cd5a9be69ec59c015ea7f38020b1d', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:41:37', '2021-05-12 04:41:37', '2022-05-12 10:11:37'),
('a550076f90fd9222caef820ddc2287c54d5ad134b6ce1512ee3d633e964eec8fcb50e3134d288614', 57, 3, 'Api access token', '[]', 0, '2021-08-08 21:25:07', '2021-08-08 21:25:07', '2022-08-09 02:55:07'),
('a572de10f0d9375e67aa1e07264669d658cfc3976127885b92e2af4f2017c57f2a9c36d5bcc2555a', 441, 3, 'Api access token', '[]', 1, '2021-03-23 02:31:49', '2021-03-23 02:31:49', '2022-03-23 08:01:49'),
('a5ca102ce4e7124d5ce41f31e0322c8777e5f9738b435087f9bef56da8368938b939441ff6b30cd6', 742, 3, 'Api access token', '[]', 0, '2021-06-15 04:37:57', '2021-06-15 04:37:57', '2022-06-15 10:07:57'),
('a5cdfc1ee9a3996161ba4ac7dcddbfbf61afd112ccdab1d77ea11b01d2294b7e057b58dd71a26d2c', 512, 3, 'Api access token', '[]', 0, '2021-04-02 06:25:03', '2021-04-02 06:25:03', '2022-04-02 11:55:03'),
('a5da0c533e8de83a14b7592c715e39dac5e7eaa69fa82bf5611ec29fc767717a0d587c888659a156', 719, 3, 'Api access token', '[]', 0, '2021-06-10 01:36:44', '2021-06-10 01:36:44', '2022-06-10 07:06:44'),
('a5e2ffabb29c2dd50c2efbb2cf77030ccf27b120247dc6db0dd45c2222df28eeafd5a5dfeb327155', 52, 3, 'Api access token', '[]', 0, '2021-08-06 04:16:12', '2021-08-06 04:16:12', '2022-08-06 09:46:12'),
('a616e42cc806de1e6ef43acdfadde738b8c4e62697632defc15540b6e5ca8248c149b3e49043e064', 5, 3, 'Api access token', '[]', 0, '2021-07-23 02:29:25', '2021-07-23 02:29:25', '2022-07-23 07:59:25'),
('a62918d2101307998fbd121ed47b8aeb86f9fc2ca8ed6e4500ea3299c16f9ad0f802e29d2aa212cd', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:24:31', '2021-07-15 07:24:31', '2022-07-15 12:54:31'),
('a6370aefa7f6857826e82823d517e255a3f62801f997582240675970ac41915e2d005e061f95e4c9', 504, 3, 'Api access token', '[]', 0, '2021-03-31 06:05:31', '2021-03-31 06:05:31', '2022-03-31 11:35:31'),
('a63d41f8205cea1e06df525688dbd06788c9c6054fb651d0d3c29a71f7748cce93a7c095999b4cc5', 400, 3, 'Api access token', '[]', 0, '2021-03-18 06:20:31', '2021-03-18 06:20:31', '2022-03-18 11:50:31'),
('a64deb527ea26f9641df084fe0251f0e2568ddb4884ef3664319d6204c1adee27402867540ca631c', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:38:48', '2021-03-22 05:38:48', '2022-03-22 11:08:48'),
('a66ea6a2a46e1d8d96c6b52414f91091de3f03b4294eac8d61ff2954b822c578d96019d4b894a87d', 612, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:27', '2021-05-07 07:04:27', '2022-05-07 12:34:27'),
('a685cfa1b00780292caa9a77c565386c220a6e66209f51776d55168f5d0b03897ef4bffb213ddb99', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:54:48', '2021-05-12 04:54:48', '2022-05-12 10:24:48'),
('a6a402e77fee88788ee7c432544a4d9fd2f7000fda55e69130ee9900a4840bb2fc207e165c8cde3b', 770, 3, 'Api access token', '[]', 0, '2021-07-15 06:46:03', '2021-07-15 06:46:03', '2022-07-15 12:16:03'),
('a6bf2f228156eb6fa2d95fd29c9e0c722421a5ac9c42f889f55a8a69bffe31a8141a0b131dc03d99', 773, 3, 'Api access token', '[]', 0, '2021-07-16 07:00:45', '2021-07-16 07:00:45', '2022-07-16 12:30:45'),
('a6f8c333caff8151878deeeb8e3e46dc106b3008fc1fe862c3e61db6abae692d7da60b583e0379f9', 13, 3, 'Api access token', '[]', 1, '2021-07-27 07:34:47', '2021-07-27 07:34:47', '2022-07-27 13:04:47'),
('a75d6ee51f753f16c54e3804267eb784db03c857d17d507d2a9ae2fa35f8f3c09f4b093af9c5ccc1', 5, 3, 'Api access token', '[]', 0, '2021-08-06 23:31:49', '2021-08-06 23:31:49', '2022-08-07 05:01:49'),
('a7840e070ca4cb7adf0d5bd22a6c1ebed9e92cfba3dd2322b8082c196936913fa989c4b72646a7e1', 542, 3, 'Api access token', '[]', 0, '2021-06-11 00:35:11', '2021-06-11 00:35:11', '2022-06-11 06:05:11'),
('a79cc16d3da2c9090c9692ffbe8ca6861edf948ebae2f4a2482790e50152d76dae785c592e195d92', 626, 3, 'Api access token', '[]', 0, '2021-05-27 01:57:37', '2021-05-27 01:57:37', '2022-05-27 07:27:37'),
('a7a6a9301b5330e50b325c8ac108ed8e7c92322566972af3dede092b6ed261616fd4e1eec98c696b', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:49:04', '2021-07-28 06:49:04', '2022-07-28 12:19:04'),
('a7a9046fdc5f0b8485407c9074ed745ade93fd19d1ebe8c7f23c254e69a712042ce908c876281928', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:30:29', '2021-05-11 06:30:29', '2022-05-11 12:00:29'),
('a7d751c5777d65d0e1a11f0e556820100c64064e2e5f327b9e88ba361ee2a5507d23fa11997237c0', 7, 3, 'Api access token', '[]', 1, '2021-07-29 07:58:23', '2021-07-29 07:58:23', '2022-07-29 13:28:23'),
('a7ddadee79f2f57f22ae607fe5048a03a2db5dbe5df866071e0563784993f233f36de61a5bb8ffe8', 25, 3, 'Api access token', '[]', 1, '2021-07-28 05:29:49', '2021-07-28 05:29:49', '2022-07-28 10:59:49'),
('a81b5c325bcf06c0f7e6509dc4fdd8d7eb9993332d035b1a2c7445ea6f3db0ca894e3bab81d658de', 639, 3, 'Api access token', '[]', 0, '2021-05-12 05:52:47', '2021-05-12 05:52:47', '2022-05-12 11:22:47'),
('a82fcc84d0c2caf9eb1d83c27be880ede4d81e6aee6c271658e4428c90ab5610780eab04b0dd3dce', 408, 3, 'Api access token', '[]', 0, '2021-03-18 11:17:09', '2021-03-18 11:17:09', '2022-03-18 16:47:09'),
('a8410c0977bb46c1b85ddfcd3dd6d47152281a7f0fc98e23ded7296460db3fac8ce0e8ba0e45c718', 9, 3, 'Api access token', '[]', 1, '2021-07-26 04:53:57', '2021-07-26 04:53:57', '2022-07-26 10:23:57'),
('a84a5fa56d387d7111b7867cefc9ea1c09925a067c677a83c79ca2057f64f0f3e54a4c268a3c61ab', 626, 3, 'Api access token', '[]', 0, '2021-05-27 02:23:49', '2021-05-27 02:23:49', '2022-05-27 07:53:49'),
('a87346868d9dfe96289cee84a1a4a163acf8b49be60baaa1a37ef57b6d654c8f4e63a9063d9e77db', 4, 3, 'Api access token', '[]', 0, '2021-07-26 01:58:04', '2021-07-26 01:58:04', '2022-07-26 07:28:04'),
('a8e6eb698ddf1d34c31d5aa4a78d202ebe67a32184c85e77e155501a75aeddc644b11c1d960f1812', 39, 3, 'Api access token', '[]', 0, '2021-07-28 07:40:11', '2021-07-28 07:40:11', '2022-07-28 13:10:11'),
('a8f1971a4e8dd264a83bf1483be20b35648fe18f946ae347fb4f0254963ae68a6e8ac75c5e36ec23', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:08:26', '2021-06-15 01:08:26', '2022-06-15 06:38:26'),
('a90ad2d0c28590d8ccebb768d79081e13e3e9d9ef07bdc6da3fdb4436f5f7c372d7be9e7b12c3138', 668, 3, 'Api access token', '[]', 1, '2021-06-14 08:19:54', '2021-06-14 08:19:54', '2022-06-14 13:49:54'),
('a90d5ca34707e1196c6f5c486d5dc3cb2c9ae2460166323556ce1f90cfb1d2299f690cbfb510247f', 531, 3, 'Api access token', '[]', 0, '2021-04-07 02:03:15', '2021-04-07 02:03:15', '2022-04-07 07:33:15'),
('a913d06c024dc1dae6376972a9c600476c9a8e443ef59103826fc76e6dd9088aaf5af5beb7d60571', 698, 3, 'Api access token', '[]', 0, '2021-06-03 00:35:51', '2021-06-03 00:35:51', '2022-06-03 06:05:51'),
('a93850ec60017b4147284555b5b3756f4f949fbc8633b85df833ae6dc3492470b0b1267e76c2248b', 775, 3, 'Api access token', '[]', 0, '2021-07-19 09:14:30', '2021-07-19 09:14:30', '2022-07-19 14:44:30'),
('a93fe2854fc182d0a118e666785da553199d05e23c388271adf82349fd904ddfec03c658be437291', 758, 3, 'Api access token', '[]', 0, '2021-06-30 08:58:35', '2021-06-30 08:58:35', '2022-06-30 14:28:35'),
('a97271f01593f9ce5b8e6614c3c182d9f0b6a97e3740cc3021965a0fdd817e94355f89879f99119a', 557, 3, 'Api access token', '[]', 0, '2021-04-14 01:33:33', '2021-04-14 01:33:33', '2022-04-14 07:03:33'),
('a9997508faf02ba2755f83979e869c38e702f60a1796c9cd1665330e6e4d4e83c7e082262c0afaf6', 565, 3, 'Api access token', '[]', 0, '2021-04-15 07:45:37', '2021-04-15 07:45:37', '2022-04-15 13:15:37'),
('a99fd4167367ba1aafee5a18adee79afc1d22582bb75b93cc686fea8b72580ff420ce60ae03a34ae', 726, 3, 'Api access token', '[]', 0, '2021-06-10 01:45:24', '2021-06-10 01:45:24', '2022-06-10 07:15:24'),
('a9c83731ece7786679fbbe2aaec8b6735a0c6a09389035c56872c17155f8425ded109bfe4bf45373', 530, 3, 'Api access token', '[]', 0, '2021-04-04 22:58:19', '2021-04-04 22:58:19', '2022-04-05 04:28:19'),
('aa12ab65b3596f54d590f404ab7e093c8c3013cc56efcc336fc2d7265ced55df41f7d0fc24cb4a9e', 617, 3, 'Api access token', '[]', 0, '2021-05-02 08:22:11', '2021-05-02 08:22:11', '2022-05-02 13:52:11'),
('aa51a6b7010afd7b3062c6ae30906a7ad86ecba1b6063ef74cea32a37e2a638644db709773b0b07b', 665, 3, 'Api access token', '[]', 0, '2021-06-01 00:28:41', '2021-06-01 00:28:41', '2022-06-01 05:58:41'),
('aa62fb3abd4843949e765fbd8ae1a8a4e2084059bc2c352a3baef43e874c2555a6cd9eda01696d54', 759, 3, 'Api access token', '[]', 0, '2021-07-01 22:07:52', '2021-07-01 22:07:52', '2022-07-02 03:37:52'),
('aa696b974c9a95d0fa6aa6cf4939a8f1dd05e8dfe78b95dc48020aae9298fd96a0a9a80195afe3bd', 686, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('aa9714791ce9655ccd1d3fd9065c8bdd7b37d9eecb4af3ccd403e68088c74baae26638dc24d60e5a', 565, 3, 'Api access token', '[]', 0, '2021-04-15 07:45:37', '2021-04-15 07:45:37', '2022-04-15 13:15:37'),
('aa99e5048c7811b8d663f4b4f4e0997bbf295dee2dd179c723256c4aeb3a81cf293ab42aa97898d1', 759, 3, 'Api access token', '[]', 0, '2021-07-21 05:39:45', '2021-07-21 05:39:45', '2022-07-21 11:09:45'),
('aabb71eaed069858303c2f78f224427a002320882f7383025003fc7101dcc62b187253d1a84c6c4f', 579, 3, 'Api access token', '[]', 0, '2021-04-30 00:00:10', '2021-04-30 00:00:10', '2022-04-30 05:30:10'),
('aac3d9feaba9c6a032d35348f788f9cf1af3f6b19ab85869b38bc013c495616439b6db709f68606a', 407, 3, 'Api access token', '[]', 1, '2021-03-18 06:31:38', '2021-03-18 06:31:38', '2022-03-18 12:01:38'),
('aadccb389b811674b36804063e72fdaba25c52578b124ac04b942c2335b3971c29ae3e426d45c46a', 4, 3, 'Api access token', '[]', 0, '2021-07-27 06:40:13', '2021-07-27 06:40:13', '2022-07-27 12:10:13'),
('aaf6f0d426deaf97709e487a725668b024b309c202f4a1ea428a12613a16d8fb8c9a341ae25b300c', 620, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:34', '2021-06-10 02:04:34', '2022-06-10 07:34:34'),
('ab079136cb412d70a896a5b3f91d05b3929a995707c4fdb2fc90882f88c2a9451fd80718dbc6cbbd', 417, 3, 'Api access token', '[]', 0, '2021-03-19 02:17:18', '2021-03-19 02:17:18', '2022-03-19 07:47:18'),
('ab9e97ef83107e886a8d5ecd893d5a00c3941957ea5fceb201b2e29c25b9996c1e5e0e5c47442724', 537, 3, 'Api access token', '[]', 0, '2021-06-02 07:04:07', '2021-06-02 07:04:07', '2022-06-02 12:34:07'),
('abd0f06f76804d2d5913acab4c65db8ce0d6a8c14fa2888ba2474f1efee10fabd2f275b1de3ec6a2', 5, 3, 'Api access token', '[]', 1, '2021-08-09 23:36:27', '2021-08-09 23:36:27', '2022-08-10 05:06:27'),
('abd2886eb30f99f8c3c53e8e937de4ee8a890a5abf8096c67a6fc7a4e82e2efaefbc1ea611b675c3', 423, 3, 'Api access token', '[]', 0, '2021-03-22 05:39:29', '2021-03-22 05:39:29', '2022-03-22 11:09:29'),
('ac00bcf8036fb9b5cff8e499e8ef4e6ce21109569981b696b92a41ad2e81d7cd2108ea939e6873f1', 638, 3, 'Api access token', '[]', 0, '2021-05-12 09:30:47', '2021-05-12 09:30:47', '2022-05-12 15:00:47'),
('ac11e8c00663580898eca072d421af2a5744dacdbdae16fbaa4266c3538ba64a342679dd4f5c6bd7', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:17:09', '2021-07-15 07:17:09', '2022-07-15 12:47:09'),
('ac22aec6b6e2d083205083f12129f6fb16b087bf3925c1ca42bf12044b0402016e1e54c919027706', 552, 3, 'Api access token', '[]', 1, '2021-04-15 05:18:51', '2021-04-15 05:18:51', '2022-04-15 10:48:51'),
('ac9aae80303308a9877f5d6356452670df473e7f29f9c7ae2fbc4845a410a4543eb7f1449a586926', 542, 3, 'Api access token', '[]', 0, '2021-04-05 06:33:14', '2021-04-05 06:33:14', '2022-04-05 12:03:14'),
('acb5f842542d11c9e59f4c12c9f44ef694c0bf6c94a9b60ac89880075aa95c281bd110466abb7a23', 508, 3, 'Api access token', '[]', 0, '2021-04-01 06:58:26', '2021-04-01 06:58:26', '2022-04-01 12:28:26'),
('acceeca58b4b64d5d2f0876b2d9920984f841ac70876293160632442f50a999dea0b8c9d6133689e', 534, 3, 'Api access token', '[]', 1, '2021-04-05 08:57:29', '2021-04-05 08:57:29', '2022-04-05 14:27:29'),
('acf3d77b6f03ffa0103e6038bb181f3f61a3ed8e2dffdcd84059b41d43576545d1dc787802ae661c', 1, 3, 'Api access token', '[]', 0, '2021-07-29 23:56:24', '2021-07-29 23:56:24', '2022-07-30 05:26:24'),
('ad052e1cce607463a170c1db1a419934d7a6206810ea4eed604eb1578840a77c9687034487991c59', 645, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('ad1d0d7fa5a19f45ab696539be0ec8dafc66c7deb03bf796ba34d1ad22e05f9cbb962e15199c6c64', 638, 3, 'Api access token', '[]', 0, '2021-05-12 05:28:44', '2021-05-12 05:28:44', '2022-05-12 10:58:44'),
('ad36888c96d297a47d4300810fc738b71a356c20abfb156a1bf2b0dc86895e5e9eb08774e8df1527', 755, 3, 'Api access token', '[]', 1, '2021-06-24 07:53:05', '2021-06-24 07:53:05', '2022-06-24 13:23:05'),
('ad6c7ecd1e26e2d2e6ac9cc9cfa4efa3b9594a7195ff5542b6c8e6b65839725484ac4a8a081ecdce', 577, 3, 'Api access token', '[]', 0, '2021-04-20 02:27:51', '2021-04-20 02:27:51', '2022-04-20 07:57:51'),
('ad77298ee99188305de149872cf2cf9a942f29dd4b56f6d82d8a5ccfe191c083341ac28a0891df96', 647, 3, 'Api access token', '[]', 0, '2021-05-14 05:31:34', '2021-05-14 05:31:34', '2022-05-14 11:01:34'),
('ad86583cf546c69d65ba237382149ae85af2171f3b001f48763f2250e09ddab001ce16a747c8307c', 759, 3, 'Api access token', '[]', 0, '2021-07-20 08:47:00', '2021-07-20 08:47:00', '2022-07-20 14:17:00'),
('adc14f8201662449d89d86003db3d963a878a7193cfe326329198d078dfc6f1fb59b846d55dd6869', 4, 3, 'Api access token', '[]', 1, '2021-07-28 04:46:46', '2021-07-28 04:46:46', '2022-07-28 10:16:46'),
('adf124292b02bf9e5151f93cd6a12b579487b83dd95ed0ab6405cfdb291a29806e57e300c5f2961e', 656, 3, 'Api access token', '[]', 0, '2021-05-24 04:31:35', '2021-05-24 04:31:35', '2022-05-24 10:01:35'),
('ae11e41010e2d550c9575273b83eefb149d2aac1e688c2a77fb310cb4c3f44750a2d04c994e5e137', 460, 3, 'Api access token', '[]', 0, '2021-03-24 07:24:11', '2021-03-24 07:24:11', '2022-03-24 12:54:11'),
('ae26e205955eaa57b726d2a75f696d2cdc72b42017bafe8aa0312fc0ea7565a6b5e285b16e2f10af', 638, 3, 'Api access token', '[]', 0, '2021-05-12 05:54:03', '2021-05-12 05:54:03', '2022-05-12 11:24:03'),
('ae40a4d0202e22e2b5b14dae88426c3564681052056bc75e4bd59b9120fca30de4ec44c515e9c5f2', 459, 3, 'Api access token', '[]', 0, '2021-03-24 04:18:18', '2021-03-24 04:18:18', '2022-03-24 09:48:18'),
('ae42fcc0f9b32af178efc45f6a05b63f686b7321cdb44a69e7eadb977911b746dbcdf620c8b97990', 689, 3, 'Api access token', '[]', 0, '2021-06-02 21:26:59', '2021-06-02 21:26:59', '2022-06-03 02:56:59'),
('ae49ce94cc8b5a105c42550c35accec4bd65be2b7b89e4eb386f62fdd1d95bad032a85ef00786661', 773, 3, 'Api access token', '[]', 0, '2021-07-20 05:02:33', '2021-07-20 05:02:33', '2022-07-20 10:32:33'),
('ae681b7aaf3876f88ff610cc6fa12822f1800976f323db883a71eb3130bb4a1fa39e3fedb8aefaa6', 393, 3, 'Api access token', '[]', 0, '2021-03-18 23:53:14', '2021-03-18 23:53:14', '2022-03-19 05:23:14'),
('aec07d95f703ad07e0f7a3b04cf1fa9416e7d469ffda6a3095b13f9837fc8634a9687142c5081948', 423, 3, 'Api access token', '[]', 0, '2021-03-22 05:36:16', '2021-03-22 05:36:16', '2022-03-22 11:06:16'),
('aee8ab28ae312b00d9d383ac2ea5c3cd381b275f3b0ed981b81ab682c262ff16c0e46bcfe7e46fbc', 770, 3, 'Api access token', '[]', 0, '2021-07-15 23:24:45', '2021-07-15 23:24:45', '2022-07-16 04:54:45'),
('af138f268369b90f7a28c4012dfcfac763ab71bba8bae633044e2215c87dcaa881cddb91aa4ff4f1', 432, 3, 'Api access token', '[]', 1, '2021-03-23 05:14:43', '2021-03-23 05:14:43', '2022-03-23 10:44:43'),
('af252ab2b8853f54c3267cd8b2a62eed22f37c27134e17c9e75fc3d6862be28fdc9b61a0e80d58e5', 453, 3, 'Api access token', '[]', 1, '2021-03-24 05:19:32', '2021-03-24 05:19:32', '2022-03-24 10:49:32'),
('af4234ac8b6aca7e5800e37acbb23a2a2bbe6b9609759be8ab79726d15b34e00a7090d1b6c23793f', 542, 3, 'Api access token', '[]', 1, '2021-06-08 07:58:36', '2021-06-08 07:58:36', '2022-06-08 13:28:36'),
('afa73113d3e30cbcf2b748853e0886cd381b99cda4d871924e80aae48ba7ae5f7999cae350cc9829', 24, 3, 'Api access token', '[]', 0, '2021-07-27 23:56:13', '2021-07-27 23:56:13', '2022-07-28 05:26:13'),
('afa806ee1d45920da221ddac1dd4feaf8545f6b43364effea896609895bb61da95dffa5ec0fcf670', 13, 3, 'Api access token', '[]', 0, '2021-07-27 01:58:49', '2021-07-27 01:58:49', '2022-07-27 07:28:49'),
('afb65c9c67fee2febbd5d96f8134da60b87b68beabd948502983dedded0b27b6c4a216d7f62509fe', 2, 3, 'Api access token', '[]', 0, '2021-07-23 07:38:01', '2021-07-23 07:38:01', '2022-07-23 13:08:01'),
('b0126efac43ca3aec27850eecb4a2502598efdef2e9eecf3025a5c8c5abed5009ee2764f8c7f84dd', 622, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:48', '2021-05-02 08:41:48', '2022-05-02 14:11:48'),
('b018fd1baf14ca87726ab9b4bd91890fc873facc8720fd91f35e8c58c99f110d898d91852546185c', 659, 3, 'Api access token', '[]', 1, '2021-06-01 06:58:02', '2021-06-01 06:58:02', '2022-06-01 12:28:02'),
('b06e16d3daaff6feca6921e351b33a3a7f6173f928cb065e7150b2f8d93e7564abac540d4ef73268', 773, 3, 'Api access token', '[]', 0, '2021-07-19 06:42:33', '2021-07-19 06:42:33', '2022-07-19 12:12:33'),
('b07597b8ffe6d381c1961ed8ad4bc43fb8df1f79e0e6de5aef384656d3b0b7b4a5f4d137cdf7eea8', 723, 3, 'Api access token', '[]', 0, '2021-06-10 01:42:49', '2021-06-10 01:42:49', '2022-06-10 07:12:49'),
('b0856a2919d135641c280fc41db05c41b39b9dabaf5b0560d7dbec8a0ee2b3202a29ab4287418bc1', 24, 3, 'Api access token', '[]', 1, '2021-07-27 23:56:13', '2021-07-27 23:56:13', '2022-07-28 05:26:13'),
('b0938ab6236722c4668a2dd2f5f5bf75520e3aff901f84d577409d584e5dd8e17a584d121107a451', 2, 3, 'Api access token', '[]', 1, '2021-08-02 00:59:23', '2021-08-02 00:59:23', '2022-08-02 06:29:23'),
('b0a170c9e6ea6f276217f5cdfe64071bda0997b4d158778c0c7bc81de7bb571aca2af7b7c691e4c2', 7, 3, 'Api access token', '[]', 1, '2021-07-30 02:22:34', '2021-07-30 02:22:34', '2022-07-30 07:52:34'),
('b0a8dc3ef9537a028cf664201c6f776cab427269cba3829f75a6609cee69875efaf0cc9a2ba30ae2', 6, 3, 'Api access token', '[]', 1, '2021-07-29 00:37:38', '2021-07-29 00:37:38', '2022-07-29 06:07:38'),
('b0b41adfe84e1250176818d677fb9a3e0107f089c26df8b163e90157b4083225aa5d827961542cc8', 611, 3, 'Api access token', '[]', 0, '2021-05-13 02:29:01', '2021-05-13 02:29:01', '2022-05-13 07:59:01'),
('b0c711909fc11b80f18b6fc10acda812c4897b60ca19c2f1c3832722c2debbb431e618cc05166384', 657, 3, 'Api access token', '[]', 0, '2021-05-24 04:32:01', '2021-05-24 04:32:01', '2022-05-24 10:02:01'),
('b0e4f389b52c030c01e304716738d3fee8738c22b124096138a290376438c0576f18b5d0a131198d', 612, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:50', '2021-05-04 01:11:50', '2022-05-04 06:41:50'),
('b101ff147a7703d3a3e4d84d414123d90c78f1f0cb91c08a51a20038d1ca0f9d9a00f06467b0c899', 405, 3, 'Api access token', '[]', 1, '2021-03-18 02:05:34', '2021-03-18 02:05:34', '2022-03-18 07:35:34'),
('b130e55ae4b05469211fb2dee15b1e44b8ae319b5b3e30c4e870659b590b71d866615cc0b4679790', 502, 3, 'Api access token', '[]', 1, '2021-03-31 04:28:34', '2021-03-31 04:28:34', '2022-03-31 09:58:34'),
('b15815d1221009bd5d81d0b5bf7881632173620cd5173df59223c37eeb680122cc6267381a6501b9', 626, 3, 'Api access token', '[]', 0, '2021-05-11 04:52:50', '2021-05-11 04:52:50', '2022-05-11 10:22:50'),
('b191c24542e89a227a14658673766700ae475380db1b6d3d43b952d2511e03b21b5cd1af0021aa79', 467, 3, 'Api access token', '[]', 0, '2021-03-25 06:08:12', '2021-03-25 06:08:12', '2022-03-25 11:38:12'),
('b1af7b9631334bb6d7d089e20704716a62365a3bfd2e5eef686dcf1df1244a8caa471acf0f96de7a', 448, 3, 'Api access token', '[]', 0, '2021-03-23 05:10:02', '2021-03-23 05:10:02', '2022-03-23 10:40:02'),
('b223360f80657a0e8bf3e6ee78e651fa5cffdffec6bbede4f4e913e8b7e91f823c0ef50b9c59b676', 677, 3, 'Api access token', '[]', 0, '2021-06-02 07:24:40', '2021-06-02 07:24:40', '2022-06-02 12:54:40'),
('b24ececac0dd8c3719c28aa15851482f8b174d5b4e37bef2b39c0608c1690c8d3a1bc2dd15c93d95', 520, 3, 'Api access token', '[]', 0, '2021-04-02 07:27:06', '2021-04-02 07:27:06', '2022-04-02 12:57:06'),
('b2891166a08576a7bc8a029e0fbdae8e84f94b4aa22467e2f3598bb3b39c507fdd384f935dd0ee9f', 20, 3, 'Api access token', '[]', 0, '2021-07-27 07:21:54', '2021-07-27 07:21:54', '2022-07-27 12:51:54'),
('b2ae591fce231cb89174986673528019659344ed3f18c739c07682752fe8c7d1ee7b7fbdc57bb44f', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:54:14', '2021-05-27 04:54:14', '2022-05-27 10:24:14'),
('b2d8f7f1235cfa7dc918405818f660be7d23a40381050548766e14e5625e9c87892bf8dd1856651d', 620, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:34', '2021-06-10 02:04:34', '2022-06-10 07:34:34'),
('b318c68496174af185fb438d42dc9bf01d04b368b932464c0e8e398e9976431ba374042ae70a79b8', 1, 3, 'Api access token', '[]', 1, '2021-08-13 01:48:15', '2021-08-13 01:48:15', '2022-08-13 07:18:15'),
('b3304b0866954a877b75548d890646d9f83f61055ac1eba5742c32b804ebe89db3b0c7da676493ae', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:34:23', '2021-07-17 00:34:23', '2022-07-17 06:04:23'),
('b3601eac405f4a1f4774224eda2b52ea652b64d88a140e8b551ab55bdf7d1a0b6b47d541af1b96e6', 618, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:35', '2021-05-02 08:31:35', '2022-05-02 14:01:35'),
('b37fdb853cb9c0ec1a45d3a6e492ad226b2647f97d81490e0d779bd611e9101980387e89f54a33c4', 23, 3, 'Api access token', '[]', 0, '2021-08-05 06:59:20', '2021-08-05 06:59:20', '2022-08-05 12:29:20'),
('b384db55c48fd6de078ed3a7288c6fd0ea793772e69f075af75628e71f3861409e9707f49dd3b43c', 19, 3, 'Api access token', '[]', 1, '2021-07-30 01:06:24', '2021-07-30 01:06:24', '2022-07-30 06:36:24'),
('b3a5215979a5ade79f2162757e3d25488090c042af96b63675ab3fce83519724a83c56419175a2de', 511, 3, 'Api access token', '[]', 1, '2021-04-02 06:18:37', '2021-04-02 06:18:37', '2022-04-02 11:48:37'),
('b3d212d068cb5b8d2ee869d58c8df438e86b8f342f11f473390a3dd1201e34ad112ff45cfbfb8563', 60, 3, 'Api access token', '[]', 0, '2021-08-11 23:04:55', '2021-08-11 23:04:55', '2022-08-12 04:34:55'),
('b3fc86e08b0c2f58ef92c20faa9679cb9c475c31720c6ed0f7ffaf5f12e0aa5e8b3103b942cae854', 393, 1, 'Api access token', '[]', 1, '2021-03-17 06:33:55', '2021-03-17 06:33:55', '2022-03-17 12:03:55'),
('b4007171e30780d92f7b3c3350662aed2ebda9beec0e946912e13255f302c872a11ad126b5a3edb0', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:13:57', '2021-05-11 01:13:57', '2022-05-11 06:43:57'),
('b41a87b1a0dc28d1702f99a2c5ecc4d58f8e1dfce0a01a84a847dca3e18ba59c49e7ff61f57ca893', 612, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:37', '2021-05-04 00:40:37', '2022-05-04 06:10:37'),
('b44ab23d349154dcbb7717e5c36a09a5e00abe7e9fb4138059f62153b1e41cb1c8a8cb05e9765fe1', 654, 3, 'Api access token', '[]', 0, '2021-05-19 06:35:32', '2021-05-19 06:35:32', '2022-05-19 12:05:32'),
('b4999ebdcaf538b00811eb1f2da44d7a2916cd0a599a22ebda25db5a2b1aa4f043ba08c919d1f91d', 639, 3, 'Api access token', '[]', 1, '2021-05-12 07:08:18', '2021-05-12 07:08:18', '2022-05-12 12:38:18'),
('b4b9103d2dbc7c8d6b3a8b4b63550bb7f3f8246a274c01cdd8f021671627e2ee61df8a694e7cdfb3', 677, 3, 'Api access token', '[]', 0, '2021-06-02 07:24:40', '2021-06-02 07:24:40', '2022-06-02 12:54:40'),
('b4be9c00688d6ef3793ba22c2804d74f0e3786fb726e1e163f8a0a1dc596ca531d16e8bd4beef967', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:23:46', '2021-03-22 05:23:46', '2022-03-22 10:53:46'),
('b4c2fab1bfa8b25b26c74dd651bb0b23291853bbfcde75054548b650df5d3e757fcf98610249c185', 559, 3, 'Api access token', '[]', 0, '2021-04-14 02:27:39', '2021-04-14 02:27:39', '2022-04-14 07:57:39'),
('b4ea8bb861332df9a5f03656d944ea33872e460f91a05d9dafaa40baed22afb3198bae867c7f7ca0', 775, 3, 'Api access token', '[]', 0, '2021-07-19 09:14:35', '2021-07-19 09:14:35', '2022-07-19 14:44:35'),
('b4f1cca604e5ac58cfcbb1ea0d5d231e1817e6cf7e59b39e57cc366a2a19eeeb1d84e6c7a58a2a2a', 17, 3, 'Api access token', '[]', 0, '2021-07-27 07:24:19', '2021-07-27 07:24:19', '2022-07-27 12:54:19'),
('b5038dae5dba8e7128fb09a90b326f79d4ef314f98f28674a784e7b514cbcfad31989ef5660b0c4b', 759, 3, 'Api access token', '[]', 0, '2021-07-20 08:46:05', '2021-07-20 08:46:05', '2022-07-20 14:16:05'),
('b548159ddff97843bea606158cc2c9b988bae1906b3fdb0d624c61f89d6543178930da48017f8b73', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:10:33', '2021-05-12 02:10:33', '2022-05-12 07:40:33'),
('b56fe9cbb3552369f91b2863bc4c6e0f3a928eaf5da4394d2b0cafa6bdc4eb94cfadca5c125156de', 19, 3, 'Api access token', '[]', 1, '2021-07-29 04:19:48', '2021-07-29 04:19:48', '2022-07-29 09:49:48'),
('b571b144b9c2a3d280ef7ec8fb557f1d5970f572410c52b35002f4ba8b75bd8a051de3905dcbb31b', 476, 3, 'Api access token', '[]', 0, '2021-03-26 02:37:31', '2021-03-26 02:37:31', '2022-03-26 08:07:31'),
('b58aaecc675f179187e8383a7d009d17bdb94495565e4ec3249947740a74f736fa34b5278032f961', 507, 3, 'Api access token', '[]', 1, '2021-04-01 04:29:07', '2021-04-01 04:29:07', '2022-04-01 09:59:07'),
('b5df0eadcc32b1ef2d4e74f380e17064372a9c58fe9970c8b81503fe860839e412ca984ddb8e2e10', 611, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:23', '2021-05-04 00:49:23', '2022-05-04 06:19:23'),
('b5f159741281df17dfcfb367b63a27faa58ed0c1edf93d54fc570b2b0b37021cb9bb9f38825800b0', 40, 3, 'Api access token', '[]', 0, '2021-07-29 05:52:49', '2021-07-29 05:52:49', '2022-07-29 11:22:49'),
('b61ad4c80f712f926ba88bb972904be80702dff7ab4bfb9d69be413260712af1c7b070c4e40aea8b', 397, 3, 'Api access token', '[]', 0, '2021-03-19 01:16:17', '2021-03-19 01:16:17', '2022-03-19 06:46:17'),
('b61b01f2955963f266f432780d79f2f3bcb13c6657cdd51fa542341fa1173a664c5d77f4d8cfed10', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:30:42', '2021-07-20 02:30:42', '2022-07-20 08:00:42'),
('b6323b9ff90e5a23a6b36112b9385ec1c9b92c4b274e8eb438c1637d983c5bb7d5182beaa9afb3a8', 1, 3, 'Api access token', '[]', 0, '2021-08-03 06:39:59', '2021-08-03 06:39:59', '2022-08-03 12:09:59'),
('b63c44f499be2e0cc46144313008443cb2fd405ecf497330a8d11477a361bb0d459613719af676de', 625, 3, 'Api access token', '[]', 0, '2021-05-13 02:28:57', '2021-05-13 02:28:57', '2022-05-13 07:58:57'),
('b642dd356a50c7d8c0b55640cc5c22f2b050b7224e5c35fc60c058e2a9d133d4b1b1b4aa5025b231', 527, 3, 'Api access token', '[]', 0, '2021-04-03 07:02:20', '2021-04-03 07:02:20', '2022-04-03 12:32:20'),
('b649e5731d71b261707666fd07458481c9110d0b14cad05314a1d1f49205a3d5742ea5a4efcfb6dd', 598, 3, 'Api access token', '[]', 0, '2021-05-02 07:40:50', '2021-05-02 07:40:50', '2022-05-02 13:10:50'),
('b6ac75cb4e8f86d0c772b9db26738ec6108a84dccbc237bacbae8f8b17827f983824a6c8ad5e5567', 442, 3, 'Api access token', '[]', 0, '2021-03-23 02:48:08', '2021-03-23 02:48:08', '2022-03-23 08:18:08'),
('b6debd2719938261d30e97281b38bb7b0ef1efa966db17a222bca9e2d55b4f81ca3a8a81c78ac163', 393, 3, 'Api access token', '[]', 0, '2021-03-18 03:27:14', '2021-03-18 03:27:14', '2022-03-18 08:57:14'),
('b6f8e1a90e2ae10ec0d71d359f4b1da6a8386e3a4134fe4191963c1d23afce1418df4f248d3f4190', 740, 3, 'Api access token', '[]', 0, '2021-06-15 04:28:49', '2021-06-15 04:28:49', '2022-06-15 09:58:49'),
('b6ff8591389fa4220e1ef50a7de382c3cfc9f07910065939bcd4d62499933126b6e034cf1c90254a', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:09:33', '2021-05-08 02:09:33', '2022-05-08 07:39:33'),
('b71d971294cc600a5e9fd15512f8b31872d83924e60f7d842ffe52f5fa92c560c86a3e30811a865e', 498, 3, 'Api access token', '[]', 1, '2021-03-30 05:42:14', '2021-03-30 05:42:14', '2022-03-30 11:12:14'),
('b750fb71b51659d16c31cbb66844e9389f7717416da1b5a9f30e09d5492f89071760ff2e254ab7e1', 5, 3, 'Api access token', '[]', 0, '2021-08-06 00:55:36', '2021-08-06 00:55:36', '2022-08-06 06:25:36'),
('b7c410fb6a02a26a9dd9c0dbb749b929d6a5a82d7e20617fcbe9dd916a23cc2b2d255573af45d7b8', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:07', '2021-05-04 01:23:07', '2022-05-04 06:53:07'),
('b7c62bfa87d8a0373cdd35ceb6afc574450594d64b211565c20dce5f1573ca7c65878da1691a4c07', 426, 3, 'Api access token', '[]', 0, '2021-03-22 05:47:04', '2021-03-22 05:47:04', '2022-03-22 11:17:04'),
('b8083c5ec47a1fbd58a2e5939870c058b03c30f6a511dba803676fdf618ec319c20b88328ca1f3ec', 31, 3, 'Api access token', '[]', 0, '2021-07-28 07:26:09', '2021-07-28 07:26:09', '2022-07-28 12:56:09'),
('b809d0406f23a439dacea0fe6ed3a0504fb55dae496665cf9e2536fe822480e01c913c7177ef07a6', 2, 3, 'Api access token', '[]', 1, '2021-07-30 04:56:47', '2021-07-30 04:56:47', '2022-07-30 10:26:47'),
('b859c97a7d6be04ac216f67e167488bc2fb228255378a5c7b7aa3fd08ebc22b3c262909f07823249', 612, 3, 'Api access token', '[]', 0, '2021-05-02 09:26:42', '2021-05-02 09:26:42', '2022-05-02 14:56:42'),
('b86a86375038013d73e03a565fbf7fa8e11a62f8d224ffce3ccf10056da44acf133c95d216c128d7', 704, 3, 'Api access token', '[]', 0, '2021-06-04 05:08:18', '2021-06-04 05:08:18', '2022-06-04 10:38:18'),
('b87dc3a4b52d5992130b3639683afd3e1a424f1e51f9c58915f68b31875875dc635fd5e80035db28', 17, 3, 'Api access token', '[]', 0, '2021-07-27 05:13:57', '2021-07-27 05:13:57', '2022-07-27 10:43:57'),
('b88be79b9f1098ef83c59ab6097a72dae5c9b04ff398b4ffa730c74ddad171e0f893476fa0821236', 621, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:17', '2021-05-15 06:22:17', '2022-05-15 11:52:17'),
('b89515a1781ba6f53dfbf19decb23f63ea5400db73b9df334137c3ee6d7aa3302ba227b507042e35', 497, 3, 'Api access token', '[]', 1, '2021-03-30 04:09:25', '2021-03-30 04:09:25', '2022-03-30 09:39:25'),
('b8b10d922e797acf36025957405b5f04fcf7da4ff4c2a09aef9f0a94a1f83d30c7aef18518c5fb51', 620, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:06', '2021-06-02 08:16:06', '2022-06-02 13:46:06'),
('b8ea0462400d47659b57052ab62b6d77d78bcb9ab75e5a35cd8b1dd865daf1f9e12fb83e45802bac', 750, 3, 'Api access token', '[]', 0, '2021-07-22 00:58:16', '2021-07-22 00:58:16', '2022-07-22 06:28:16'),
('b8f746e528fc5abf6df139ec1a9d012fc1a53f76c5cc25251e444b000d4c4861ad9ec01197fca3cd', 398, 3, 'Api access token', '[]', 1, '2021-03-17 23:37:35', '2021-03-17 23:37:35', '2022-03-18 05:07:35'),
('b90b77191ee63c566dd99c681959ee3737eb9a7ea8d1f989d3165f2475620268945ab2bc674579e2', 442, 3, 'Api access token', '[]', 0, '2021-03-23 02:48:08', '2021-03-23 02:48:08', '2022-03-23 08:18:08'),
('b941f2250d996e007a7fbb1e066f45dc989bfd9f9594d49f3009bd541e6c381124aa24108cd0c21b', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:42:10', '2021-05-11 06:42:10', '2022-05-11 12:12:10'),
('b9912598a80424ee4e11bda0ff8323471500a146753cdd752e82e607bcc5fdf75dd7989b8a1892ec', 770, 3, 'Api access token', '[]', 0, '2021-07-16 00:05:09', '2021-07-16 00:05:09', '2022-07-16 05:35:09'),
('b9a027dddd5347f11348d2046eb5fbcd604f2dc7366b7f6dfb2e7d2c7a74370fd904d82cb413a37e', 773, 3, 'Api access token', '[]', 0, '2021-07-20 06:43:36', '2021-07-20 06:43:36', '2022-07-20 12:13:36'),
('b9a93c486d7ecc9841af68e819e7d25fa252d10cbc706fcdeb1b7aa196a854abeff76073d76abd44', 781, 3, 'Api access token', '[]', 0, '2021-07-21 02:17:55', '2021-07-21 02:17:55', '2022-07-21 07:47:55'),
('ba3d6af4355b35210dd3ebe5160edd0d3118d5f0fc3f092d8bc4c10a4bfc7be6169ec3724fec1825', 675, 3, 'Api access token', '[]', 0, '2021-06-02 07:18:58', '2021-06-02 07:18:58', '2022-06-02 12:48:58'),
('ba86d2161319aecabd764b02853b25812f7eedf1e3820471de2a82d86d4ca0e4984209c299f15ddf', 519, 3, 'Api access token', '[]', 1, '2021-04-02 22:15:52', '2021-04-02 22:15:52', '2022-04-03 03:45:52'),
('ba88801dbc94fbbb9b0b9ce3d46740af386d05172e7692f7150ec2bd737219c083f8fb906f5eeb12', 495, 3, 'Api access token', '[]', 1, '2021-04-01 04:29:48', '2021-04-01 04:29:48', '2022-04-01 09:59:48'),
('ba94e58d58324e19587d46840cab5cecafcb6c5c1f97dca9250376e04e33b29431fc50edc9f46347', 657, 3, 'Api access token', '[]', 1, '2021-05-26 04:07:30', '2021-05-26 04:07:30', '2022-05-26 09:37:30'),
('bad6d3438e495211bc4728b0130931d452ae8c8ef4b9ae79d070e5eb1cefbe2649965d848eaa9659', 533, 3, 'Api access token', '[]', 0, '2021-06-08 09:14:58', '2021-06-08 09:14:58', '2022-06-08 14:44:58'),
('bae8578afc6cf63a50a62d129bcc33244c03c1b171976494e601d435e97ece6bfd17e686b914af3c', 393, 3, 'Api access token', '[]', 0, '2021-03-18 00:50:39', '2021-03-18 00:50:39', '2022-03-18 06:20:39'),
('bafcbd6f7deb2a5f22fce68ba1c028de7205dd1828649935992ffa7e5ecf67d2bced51eaad7ee7e1', 26, 3, 'Api access token', '[]', 0, '2021-07-29 02:07:28', '2021-07-29 02:07:28', '2022-07-29 07:37:28'),
('bb3817ed33b6303cff5e06e695e186050e3789810fec8aec75d7d5e0b0c7a86c64b4983b102594d4', 664, 3, 'Api access token', '[]', 1, '2021-05-30 06:50:51', '2021-05-30 06:50:51', '2022-05-30 12:20:51'),
('bb3a531774e8835421153ec5fb75c0b9d765584c2732747a25ba38c7f85b735d4499cd056d5cb1e5', 17, 3, 'Api access token', '[]', 1, '2021-07-27 07:24:29', '2021-07-27 07:24:29', '2022-07-27 12:54:29'),
('bb41da1db024c052f38008d7b3be671c131b22f3ec3f98f28eed85b277876f979f3dae851e8d63a2', 685, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('bb5769ae7a89400f14deab1167af31cde9ef0b0abb705fdf01442e853801cc205ea83dd1574926a3', 668, 3, 'Api access token', '[]', 1, '2021-06-09 05:08:26', '2021-06-09 05:08:26', '2022-06-09 10:38:26'),
('bb8305956b23d452837ec8d89a8e5c001ea89ca0a64b71de989de4472bbc0e4a8a8a660d8a6f3e71', 653, 3, 'Api access token', '[]', 0, '2021-05-17 04:49:38', '2021-05-17 04:49:38', '2022-05-17 10:19:38'),
('bbb0d0edf703de0be3ad26a93810963b05dda1e193f1d7daca33abb0c18623120fdac2642668d2a3', 746, 3, 'Api access token', '[]', 0, '2021-06-17 02:46:15', '2021-06-17 02:46:15', '2022-06-17 08:16:15'),
('bbc9e166cad5137ca66136c0ba6d6dcb257999f9fce30656fed9f874fa873859f3a92678077a3af7', 611, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:08', '2021-05-04 01:23:08', '2022-05-04 06:53:08'),
('bbd08c4744786f71ffe6010d791359f9a1001f484d170106708a00fa822bffc29ed706e16327f1d6', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:17:23', '2021-07-14 02:17:23', '2022-07-14 07:47:23'),
('bbd65d3aa453f2769428d58ea058cfc9f0b4adde01eb5c6a8010e36b4f76aaa3e059c4afb06a3631', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:08', '2021-05-04 00:52:08', '2022-05-04 06:22:08'),
('bbd7534616ddf18b159b52b805f59a7d0c194ba8ad92f658d74b10c40d0694d037a5418af54674e5', 19, 3, 'Api access token', '[]', 0, '2021-07-29 05:19:03', '2021-07-29 05:19:03', '2022-07-29 10:49:03'),
('bbd997e4f05bc34e3ee0cdc356eec452e9acc9a686dba70b47bc3ca60d8d37f1a25456bb6c26cacf', 680, 3, 'Api access token', '[]', 0, '2021-06-02 07:47:39', '2021-06-02 07:47:39', '2022-06-02 13:17:39'),
('bbdf5bbb02f57b1d29547e0863c3227feee4bffd81ce1c747064dcf9e44c369b5503083a3751adf5', 400, 3, 'Api access token', '[]', 0, '2021-03-22 07:49:02', '2021-03-22 07:49:02', '2022-03-22 13:19:02'),
('bc1e6b386c5676e8b63ec8c940a96ea3b8aa79caa49b5eafb2379e76260bda0a8bff5351ef44b9e0', 621, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:38', '2021-05-04 00:46:38', '2022-05-04 06:16:38'),
('bc532a427f42fe59912db41a10b4514e55c1edfee70f2a22f1cf1f8767ea5d2f92db5d9833cd012f', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:49:47', '2021-08-06 05:49:47', '2022-08-06 11:19:47'),
('bc5635baf7018ae195be51f831f5dcf30c6f41dc152542932e356abbcf36ea18650160d04c8a157f', 631, 3, 'Api access token', '[]', 0, '2021-05-09 02:06:54', '2021-05-09 02:06:54', '2022-05-09 07:36:54'),
('bc7110399f5926b99c9b1dd7d3a30420253895262b0bef7cfeac274b1c52c16b06278236f8c0b420', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:15:13', '2021-05-08 05:15:13', '2022-05-08 10:45:13'),
('bca35cc675be814285c4dbf33cb40e387127dde04d9febdec3d82f179098764f2c98ecbbfc6de075', 713, 3, 'Api access token', '[]', 0, '2021-06-08 09:41:21', '2021-06-08 09:41:21', '2022-06-08 15:11:21'),
('bcacbd290183158f0fd81141509f19762b5411816dd272ad9cc2c6c10c7ed9d3f886c3ab5aae5c01', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:10:31', '2021-05-12 02:10:31', '2022-05-12 07:40:31'),
('bcda1c9a36be3ffeae74624b263a9b569eee16d3be26b4190c5192e511b2225fbf912c8ff16321d1', 413, 3, 'Api access token', '[]', 0, '2021-03-18 23:42:30', '2021-03-18 23:42:30', '2022-03-19 05:12:30'),
('bd1f77d7e12887921be85c387998f9af63dc090b9b99fde50adbeace594c00f00dd2a9a93c68a9b0', 678, 3, 'Api access token', '[]', 0, '2021-06-02 07:24:41', '2021-06-02 07:24:41', '2022-06-02 12:54:41'),
('bd5b10d111556c5886872828bce761c27bef4a9d29bf165e807d50f17aa4a229cf2a650820ad9598', 60, 3, 'Api access token', '[]', 0, '2021-08-12 02:35:11', '2021-08-12 02:35:11', '2022-08-12 08:05:11'),
('bd5e84d71e47a871ab86641ca3de5f92634dc249bce0129ec0e236716c922a2535b5c441ead97d59', 574, 3, 'Api access token', '[]', 0, '2021-04-20 01:06:26', '2021-04-20 01:06:26', '2022-04-20 06:36:26'),
('bd7a462b00da151b7530df88b7a67a4578a55baffcd1effbe5e82f60f2da46d67239794de779080a', 432, 3, 'Api access token', '[]', 0, '2021-03-23 22:57:57', '2021-03-23 22:57:57', '2022-03-24 04:27:57'),
('bd7b67a798bba7ec632cb3866132e20da19d6d04c97be96acd96aed1ff75c4899a4f278b15ff2657', 468, 3, 'Api access token', '[]', 0, '2021-03-25 04:44:15', '2021-03-25 04:44:15', '2022-03-25 10:14:15'),
('bdb6820202b4989900eed8968a2ca28602b5b524c9226d4d3d0ce0970c19da0494675b3c1cf8ce52', 685, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('bdce46449040d3fa8b2065ab347d79ff8182c9bb82add0bbad17e87fc5eed7102237e249bed95ddd', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:15:28', '2021-06-15 01:15:28', '2022-06-15 06:45:28'),
('bdd678e86591fb5bfa8643e1d0393fe8d21054d59f1344dcd7d7c5c7536d9dc846a940ce2c392128', 497, 3, 'Api access token', '[]', 1, '2021-03-30 04:05:20', '2021-03-30 04:05:20', '2022-03-30 09:35:20'),
('bdebd91e7cb53d4398c2fe4a3bec142e5c1645190b53a2c82b04ca1834836853c160ccdc69150f42', 759, 3, 'Api access token', '[]', 0, '2021-07-21 02:26:55', '2021-07-21 02:26:55', '2022-07-21 07:56:55'),
('be05c4b0bdf503f57b94a8c3dd2f71ae1cfec52112d33de55e20eec396ff761f5ce5c966ec8e81ef', 453, 3, 'Api access token', '[]', 0, '2021-03-23 07:01:47', '2021-03-23 07:01:47', '2022-03-23 12:31:47'),
('be11049dce839ba05d4310671c228a1880ad48194ab59640791457dde7de4978e426deaafdbb7801', 521, 3, 'Api access token', '[]', 0, '2021-04-02 23:54:59', '2021-04-02 23:54:59', '2022-04-03 05:24:59'),
('be270b28fc87a2dd16153e3c9ed8be0c3331138136a4463503692d0dfb40581f3ec7002a4f99b730', 513, 3, 'Api access token', '[]', 0, '2021-04-02 06:57:25', '2021-04-02 06:57:25', '2022-04-02 12:27:25'),
('be3fdc303dd3e4711fc96cba72300eb9bcff0bb2e274e43d61df39f92e4afecf41628981038d5699', 714, 3, 'Api access token', '[]', 0, '2021-06-08 09:41:22', '2021-06-08 09:41:22', '2022-06-08 15:11:22'),
('be671836ae6ac8f138acbf799558987c94a6c1d2d8da1b2ae00393d1c0cd6c01cd30317ef882146f', 1, 3, 'Api access token', '[]', 0, '2021-07-26 22:53:35', '2021-07-26 22:53:35', '2022-07-27 04:23:35'),
('bec7f05f817d01f5d703caa7171195112fe98db4dda43832461ec816ecc02124af019fcc76b866e6', 17, 3, 'Api access token', '[]', 0, '2021-07-27 05:13:57', '2021-07-27 05:13:57', '2022-07-27 10:43:57'),
('bf0016b0ba2397dc27bb272e55a9bef7b41d2bdb3f3900bc72ba9cb03812dfda27f3d7c499e95b49', 712, 3, 'Api access token', '[]', 0, '2021-06-08 09:31:07', '2021-06-08 09:31:07', '2022-06-08 15:01:07'),
('bf55df33ff99fb1aec25f8c6a547dd00d52e48a17eb08c8d49e9bdecd0602d4e275f6322091b05c7', 515, 3, 'Api access token', '[]', 1, '2021-04-02 07:01:49', '2021-04-02 07:01:49', '2022-04-02 12:31:49'),
('bf80f595a3f2c4638c80d1ebcd67ff0ab6aa711e721e9fac7e7d0c7b15aaff80a5856c924ebcedae', 527, 3, 'Api access token', '[]', 0, '2021-04-03 07:13:26', '2021-04-03 07:13:26', '2022-04-03 12:43:26');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('bf98bbb0b00fd57e9fef34d5c84fa78a1f9ee1f634692220e16c8043cd60df257031ef96aa801eed', 612, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:37', '2021-05-04 00:46:37', '2022-05-04 06:16:37'),
('bfc1ee9250d825f9fad91114df0703c5be25a46c5a498f25754596165129b213a47024b4702d6a89', 702, 3, 'Api access token', '[]', 1, '2021-07-20 23:39:55', '2021-07-20 23:39:55', '2022-07-21 05:09:55'),
('bfe5858a934a989f47e11f4260fd8c14d70320c4fe628aee1304a633c5301f9938bbaccb3255c675', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:24:47', '2021-05-11 00:24:47', '2022-05-11 05:54:47'),
('c0157eeb178e57cf19da2e9fb83fd5762de8aabf369929332d9dad784b684e74158dfde0ee5a4ab9', 532, 3, 'Api access token', '[]', 1, '2021-04-05 00:03:39', '2021-04-05 00:03:39', '2022-04-05 05:33:39'),
('c0168c908188baf7902fdbe26f9879f4f04fc7354e4614a20285dd298b8a3a1b51e1a6f7c5a68745', 542, 3, 'Api access token', '[]', 0, '2021-06-17 02:38:18', '2021-06-17 02:38:18', '2022-06-17 08:08:18'),
('c0292ed7d55ef83fd28c5937a836f1c2dc125a63501cb2137e0bacbabca4277e20963aa020c5a08c', 5, 3, 'Api access token', '[]', 0, '2021-08-06 23:34:55', '2021-08-06 23:34:55', '2022-08-07 05:04:55'),
('c0435ab6d52421f88a1557108032a25365824d1bddf4d1c3c51201df0616b340a2520b1ca0840d9b', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:48:17', '2021-08-06 05:48:17', '2022-08-06 11:18:17'),
('c07bf6b7dd21dc826b33136727602ef451ab1bfb44e8d71f8183e7367d0dbf8a05dafe6a72cca79a', 759, 3, 'Api access token', '[]', 0, '2021-07-21 03:14:19', '2021-07-21 03:14:19', '2022-07-21 08:44:19'),
('c08eeb288a43139cf9d02e8575a5ae562783307aca640ba652e5e37280b1e311e3dbfbafb07ebafe', 401, 3, 'Api access token', '[]', 0, '2021-03-17 23:26:41', '2021-03-17 23:26:41', '2022-03-18 04:56:41'),
('c090ca16816f1826b0c26d1bb4098771bf1ab654fe5b3c06a8b8725c1e9b6cf23d9ab1c40b128457', 1, 3, 'Api access token', '[]', 0, '2021-08-03 00:25:47', '2021-08-03 00:25:47', '2022-08-03 05:55:47'),
('c09a1d2385050039913c8d35846d92479a15651c28fdf1eebc172fbfa1a0929716ee690f04fd1d69', 393, 1, 'Api access token', '[]', 0, '2021-03-17 06:20:04', '2021-03-17 06:20:04', '2022-03-17 11:50:04'),
('c0d2c7f454cdf1df34a2b80012787050dc9d8c2e881ccbc2ee0a033608570512067abfbafc3a6121', 555, 3, 'Api access token', '[]', 0, '2021-05-24 05:33:54', '2021-05-24 05:33:54', '2022-05-24 11:03:54'),
('c0e89dcf6b611005435b0571728c9188c84f7edd303df8c75c39019f710fd7ea316ca09b32343206', 459, 3, 'Api access token', '[]', 1, '2021-03-24 04:18:18', '2021-03-24 04:18:18', '2022-03-24 09:48:18'),
('c150b78754c575543ed0067ce0ee95a1a6bb594388fc5e03cafc4dcc151d349a8449a1a48fdaac6e', 398, 3, 'Api access token', '[]', 0, '2021-03-18 05:08:19', '2021-03-18 05:08:19', '2022-03-18 10:38:19'),
('c166e74fb2d95f96fdfd23db598760a1ac0bc51755e2a254da4da9089eaf52b5e7877860f3bfe5e1', 23, 3, 'Api access token', '[]', 0, '2021-08-05 06:52:10', '2021-08-05 06:52:10', '2022-08-05 12:22:10'),
('c16e126b545aa54fe0cd1be6bf5e77beb37e61f03694622d873ffb871cd1e99c004cf09eadf8f5a0', 624, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:20', '2021-05-04 00:49:20', '2022-05-04 06:19:20'),
('c1c100d74fcaade87590a3174e7ae39b3200587e187da2c758e97ff3f83c7e99a3f3589dd2cf0876', 582, 3, 'Api access token', '[]', 0, '2021-04-21 06:37:07', '2021-04-21 06:37:07', '2022-04-21 12:07:07'),
('c1ceb89bfef063b2e6367e7160b4037772eaeb5af2caf870329d9044982e2c0d5161d904538935a5', 424, 3, 'Api access token', '[]', 0, '2021-03-22 05:38:47', '2021-03-22 05:38:47', '2022-03-22 11:08:47'),
('c20730771f3992cd3262bb0a74c6720e0188a0575ec73a770972548f5c315dff3c006199f8e89bbb', 773, 3, 'Api access token', '[]', 0, '2021-07-21 05:29:09', '2021-07-21 05:29:09', '2022-07-21 10:59:09'),
('c2109b5c9630ad8af9b30953e779385f0fe96abab90fa68a9d1f2686275d39f993a0165307249ce6', 622, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:48', '2021-05-02 08:41:48', '2022-05-02 14:11:48'),
('c21cb817d2f42cc975ab68523fe87b3d8efe9d4bf59f68a5d36eab7e8f855f5382e7db6d9b5977b0', 433, 3, 'Api access token', '[]', 0, '2021-03-24 00:50:14', '2021-03-24 00:50:14', '2022-03-24 06:20:14'),
('c247cb5ededf7f2abbd61ac4f47e20b8684e9d6af3c64ba4a3f652b892c4e68d05ed1ce2c7350d8f', 22, 3, 'Api access token', '[]', 0, '2021-07-27 23:37:02', '2021-07-27 23:37:02', '2022-07-28 05:07:02'),
('c252f59364a760b655cc9a5934209a6e6b28db7f32b939ce119a5e968907c7ff2920af919df78d47', 683, 3, 'Api access token', '[]', 0, '2021-06-02 08:03:48', '2021-06-02 08:03:48', '2022-06-02 13:33:48'),
('c287a5ed9c1c4bc8b6873440263f35ce07c9e866ce0faa3fa0ab7fb956312ddbaec0dd197e90e668', 531, 3, 'Api access token', '[]', 0, '2021-04-09 01:10:36', '2021-04-09 01:10:36', '2022-04-09 06:40:36'),
('c30c0b65cb56d5491f16555a2a1f1586ef53a000b4e5b73f918369a8aa15102057531291c82eb98e', 60, 3, 'Api access token', '[]', 0, '2021-08-18 05:40:16', '2021-08-18 05:40:16', '2022-08-18 11:10:16'),
('c3401a99a13e90866665df9ddd952e0bc227ba20069fe3942116fb9ea8a94b7a9f66273f80dec61c', 23, 3, 'Api access token', '[]', 0, '2021-08-05 06:57:33', '2021-08-05 06:57:33', '2022-08-05 12:27:33'),
('c341a298a2d50245a23b02328601f41d5de98c1c1da501c41ab61afe83d70defe44ee3f8b603d084', 686, 3, 'Api access token', '[]', 0, '2021-06-10 02:04:35', '2021-06-10 02:04:35', '2022-06-10 07:34:35'),
('c352d5bd5adcb24a696d348ba05821c5c3b47a4f216f0c710e2cd94f6f9dafa2d015456fed98768b', 686, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:15', '2021-06-02 08:17:15', '2022-06-02 13:47:15'),
('c38289a872b0ad45ff0a91dc7cf28a217118edf4d7300d194e0d37f7c903510ae1e372dbb084c4df', 1, 3, 'Api access token', '[]', 1, '2021-08-18 05:24:56', '2021-08-18 05:24:56', '2022-08-18 10:54:56'),
('c3834af9d060568af68092123d797884cc9dced8beda1ca7452bd63d70571fa282fcd9ac3d5a9f81', 626, 3, 'Api access token', '[]', 0, '2021-05-31 07:40:37', '2021-05-31 07:40:37', '2022-05-31 13:10:37'),
('c3846cb3e8206b367a3307f0c95f2e255c714a497a49fd57e0a8004b2c37a9a08099875b491e6cb9', 525, 3, 'Api access token', '[]', 0, '2021-04-03 01:21:37', '2021-04-03 01:21:37', '2022-04-03 06:51:37'),
('c3f8167af4f4670d931a7b64d1883ae0f16e84ad1235ec44fe268672c34b00de4b125347be9166e8', 625, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:23', '2021-05-15 06:22:23', '2022-05-15 11:52:23'),
('c44f37c11b658c8e4c3e26c177caab9fb2da5afa7d13668db74fc3431b54790ba8c62239ace68664', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:08:16', '2021-06-15 01:08:16', '2022-06-15 06:38:16'),
('c45fe247cada58a5d7f4c9bb737669ad6664d99666d0a4f98fade68bde677739354585435f0804c4', 638, 3, 'Api access token', '[]', 0, '2021-05-12 08:05:51', '2021-05-12 08:05:51', '2022-05-12 13:35:51'),
('c481ef413e3dc4bb8db8fd9e65e725c896d19d422ce35ca75fc7b6cb4a5d7be89b8926d663df152b', 436, 3, 'Api access token', '[]', 1, '2021-03-23 02:48:56', '2021-03-23 02:48:56', '2022-03-23 08:18:56'),
('c4b011a802aadba93d2bf7a35da493099f37860f63120de62288f1395d17547d74f125fbb8ee2f7f', 24, 3, 'Api access token', '[]', 1, '2021-07-29 23:16:53', '2021-07-29 23:16:53', '2022-07-30 04:46:53'),
('c4c6581390702d50a9a80be4a4deea62ee4d611963fa807a39b0736be90fe39d72afd65b8b96b0f4', 633, 3, 'Api access token', '[]', 0, '2021-05-11 03:58:41', '2021-05-11 03:58:41', '2022-05-11 09:28:41'),
('c4d94ef0985e21015f4fb8fe3ca456c87bc2b2a2fa5bd74aa44283a1902ee592a1710cad72b78eaa', 400, 3, 'Api access token', '[]', 0, '2021-03-22 06:06:06', '2021-03-22 06:06:06', '2022-03-22 11:36:06'),
('c4f5599b621e4ba89d8ea06ed4e07f413d648abab3420cc2802277b1ca5a94f061ebc26c6f568389', 516, 3, 'Api access token', '[]', 0, '2021-04-02 07:09:06', '2021-04-02 07:09:06', '2022-04-02 12:39:06'),
('c4ffa3253cfd9d7c294da8f7b2678ba2a6360046ace0bd0df5810270136cd2de5a294ce97111b6f1', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:38:20', '2021-05-12 02:38:20', '2022-05-12 08:08:20'),
('c52c1b746371603a026a0b085a9539c3db9740c0fb6adbd20153c51327e73987b548036a6cbad1d8', 504, 3, 'Api access token', '[]', 0, '2021-04-01 06:24:38', '2021-04-01 06:24:38', '2022-04-01 11:54:38'),
('c5326f68fa89cc2f3122704646335d75ae68d92ee37b4f2031431e1b4b55172fb2a523d6600066a6', 399, 1, 'Api access token', '[]', 0, '2021-03-17 06:23:50', '2021-03-17 06:23:50', '2022-03-17 11:53:50'),
('c539bae84c026f0f0df56f6edb689c6e5f0ce0f6d178fc0e871042174bdca3588e219c533c3ec73b', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:51:52', '2021-05-04 06:51:52', '2022-05-04 12:21:52'),
('c53e5a89bb3001c1e0e8f3dc704e3a62f048a8c24fcd54e7c6a109a579ccb493b2db9774c8c5d97d', 771, 3, 'Api access token', '[]', 0, '2021-07-15 09:14:28', '2021-07-15 09:14:28', '2022-07-15 14:44:28'),
('c54d668217ad8404082a563d8319fd37b1bffeede0d9d8a3f3d4882063abe4c6f18782c9105ebdeb', 684, 3, 'Api access token', '[]', 0, '2021-06-02 08:03:50', '2021-06-02 08:03:50', '2022-06-02 13:33:50'),
('c57e91b0669dcb1c68c7c1477b4ba2c6f9c24bb6a953621c871940cdef5662626c040dc2299cba93', 635, 3, 'Api access token', '[]', 0, '2021-05-12 04:55:37', '2021-05-12 04:55:37', '2022-05-12 10:25:37'),
('c5a06702bf6821657bc876e5e525072b0cc261cc459186007cbdf328a9c7d6f51a813740cc2ea893', 463, 3, 'Api access token', '[]', 1, '2021-03-24 06:13:24', '2021-03-24 06:13:24', '2022-03-24 11:43:24'),
('c5bb24cf8c07d96a79b14681a9e53234ab4f8caf2cf72a481dc495d77735a83b238d9fa7da9ff523', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:42', '2021-05-04 00:40:42', '2022-05-04 06:10:42'),
('c5fe593602bb4e049b593ab6cfd26bc3a11b8231357b684e7d03db14030c0cda815f2b24f5fbd239', 734, 3, 'Api access token', '[]', 0, '2021-06-14 10:02:17', '2021-06-14 10:02:17', '2022-06-14 15:32:17'),
('c647654df2c7c0e551a69352d5a01801c733c40fbf061650e3850b37a10ada37038ed91a130a7aa3', 446, 3, 'Api access token', '[]', 0, '2021-03-23 03:01:00', '2021-03-23 03:01:00', '2022-03-23 08:31:00'),
('c6adfabc6c89d415236dd9c847e27d09ac188cea6053ce704766231ac0255ba51ef0e5ca7d81a11d', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:01:45', '2021-07-20 02:01:45', '2022-07-20 07:31:45'),
('c6d44a7d8ddedb861e5ea11466f7694c016515d9b6be21eb13a897edfdf0f701f8ae597701f406a9', 392, 3, 'Api access token', '[]', 1, '2021-03-17 23:37:17', '2021-03-17 23:37:17', '2022-03-18 05:07:17'),
('c6d8bf00b2530cccd019e6744fe8cbbb36458bc2d2410ab9e9e07ce08899a2caaf90a3e705b3cbe5', 547, 3, 'Api access token', '[]', 1, '2021-04-29 02:50:24', '2021-04-29 02:50:24', '2022-04-29 08:20:24'),
('c6ee54929c0e44c7bc9da07fc9354e40c5dbaf6748d0b49fc1da397430bd7200d7906706e2743563', 1, 3, 'Api access token', '[]', 0, '2021-07-30 02:24:13', '2021-07-30 02:24:13', '2022-07-30 07:54:13'),
('c708061ebc89cb45710b082985c62b5ae5557af305aa76c7f94c42b4f9bbbd37e6c34ddfc0e48cf6', 602, 3, 'Api access token', '[]', 0, '2021-05-02 07:48:07', '2021-05-02 07:48:07', '2022-05-02 13:18:07'),
('c720407d9756a83a893efec7ce3644643a641a629437543d846651f686e9e3e9ae5224f0cc5bf6cc', 427, 3, 'Api access token', '[]', 0, '2021-03-22 05:50:49', '2021-03-22 05:50:49', '2022-03-22 11:20:49'),
('c7666513db61cabcf4dc44b0eb71ea7c41f05441828babaf5e5d157ba8de8c3b93bb06df57180e9a', 2, 3, 'Api access token', '[]', 1, '2021-08-02 01:08:48', '2021-08-02 01:08:48', '2022-08-02 06:38:48'),
('c76a086e4d2709a6f96a25f631013008817c6a75fcc2f8490a1c74e0d72ef03b39369c9be99f61d8', 393, 3, 'Api access token', '[]', 0, '2021-03-18 03:39:55', '2021-03-18 03:39:55', '2022-03-18 09:09:55'),
('c792efafeac28e0589711a8c51cdeb8ccbcf5edd0af88621bc24a5e73b42f1f04341fc8167728b53', 396, 3, 'Api access token', '[]', 0, '2021-03-22 05:41:54', '2021-03-22 05:41:54', '2022-03-22 11:11:54'),
('c7e310857ad3a0176bb38dd8fa3739cb4f0e40fc04b9ab1c69a6be04c4aa8d594a66b7ce3adfa757', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:38', '2021-06-10 02:37:38', '2022-06-10 08:07:38'),
('c87122c5e334815bde82ba92424b03108e67663b1c22933c196b7e81086b61f0dccd676f7a443c28', 393, 3, 'Api access token', '[]', 0, '2021-03-18 02:56:25', '2021-03-18 02:56:25', '2022-03-18 08:26:25'),
('c878be8916f057754fb905fe7b407f7e6a8933f7b84ecd45e7c93ffe23660c6bba231eaad393e079', 489, 3, 'Api access token', '[]', 0, '2021-03-31 04:04:20', '2021-03-31 04:04:20', '2022-03-31 09:34:20'),
('c8a5a6dcd5de8a811500640cf09e2dbb9248833d0b92f56951c03f69e7e21b52c89076f6091df6e2', 590, 3, 'Api access token', '[]', 0, '2021-05-02 07:31:14', '2021-05-02 07:31:14', '2022-05-02 13:01:14'),
('c8cddb9f745dc16c62dd82233aec0482916f5a45b7be47254ea9b13ad0bfaadf3059ddf035aa7cbd', 30, 3, 'Api access token', '[]', 0, '2021-07-28 07:15:34', '2021-07-28 07:15:34', '2022-07-28 12:45:34'),
('c8fff5a96fbb9d510274cfae975deabc626107dd8aa2e6be0e817c15d48867ee294893a09a274196', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:29:40', '2021-05-11 00:29:40', '2022-05-11 05:59:40'),
('c91c2bff82f2f24a2dc6bc273cb48e30ec2c69e85977d264209983293808c8e79b2a090177c563cf', 5, 3, 'Api access token', '[]', 0, '2021-08-11 02:45:58', '2021-08-11 02:45:58', '2022-08-11 08:15:58'),
('c956b99e1ee3bb1ef526604d5f1017aab0fde52cb229be9bcb85b331ab906e3f3e85013b3377f4a4', 495, 3, 'Api access token', '[]', 1, '2021-03-30 00:44:21', '2021-03-30 00:44:21', '2022-03-30 06:14:21'),
('c992139e31899c710ec56a3cff6c132c1ebfba9bba075d8973a04321b5e693e3a0d3dc03cb11f7ba', 643, 3, 'Api access token', '[]', 1, '2021-05-13 08:14:59', '2021-05-13 08:14:59', '2022-05-13 13:44:59'),
('c9926c15d936f538eeea318bea3e0c0c4efb3b06e36222228cf9c42453cc4248917a3aa82e5e7ab6', 701, 3, 'Api access token', '[]', 0, '2021-06-03 01:01:22', '2021-06-03 01:01:22', '2022-06-03 06:31:22'),
('c99bb237e2c1b4d3dd510f59ce92780704ae77523783699ac8627f45e28083cd076ff6117225dedf', 1, 3, 'Api access token', '[]', 0, '2021-08-03 07:08:58', '2021-08-03 07:08:58', '2022-08-03 12:38:58'),
('c9a6ec07058a6b5bb9a5fecc554899a643d256cd676f24f7e3a09c745fff405bf2a40eb057929f9a', 620, 3, 'Api access token', '[]', 0, '2021-05-04 06:54:05', '2021-05-04 06:54:05', '2022-05-04 12:24:05'),
('c9b3e9ca3df0c72612ae475cd43c17fd39fce34c309242b4874705142eba4aefc41d4a0c4438f041', 486, 3, 'Api access token', '[]', 1, '2021-03-26 06:09:23', '2021-03-26 06:09:23', '2022-03-26 11:39:23'),
('c9de33d9754d0890510a628ee7b9dfa007407a36764a3ee478606ccb9e8966e137e65eceaf268703', 584, 3, 'Api access token', '[]', 0, '2021-04-21 07:20:48', '2021-04-21 07:20:48', '2022-04-21 12:50:48'),
('c9e54e91a05eb678efbf7b2a66b81a2b39f718269ab6b13825940aa74cc9ec66063e77bb1abf3b7d', 410, 3, 'Api access token', '[]', 1, '2021-03-18 07:47:01', '2021-03-18 07:47:01', '2022-03-18 13:17:01'),
('ca001454f5e8c106633cbbcb972b4a87fd608fd0c79d0e457259b76819c1e967408eb22f10275757', 583, 3, 'Api access token', '[]', 0, '2021-04-21 06:42:16', '2021-04-21 06:42:16', '2022-04-21 12:12:16'),
('ca2d0745b617c0f9251bcbf4ef89473851da51ba91b4ad0c7dfc56ce8e6e33e390b991ab3a339175', 398, 1, 'Api access token', '[]', 0, '2021-03-17 07:52:23', '2021-03-17 07:52:23', '2022-03-17 13:22:23'),
('ca3c0c4c5ea57c92cc27f38a3c4d464c00b81e37ca592332beb215adf367f2bc9ee2289f58c9d5c6', 730, 3, 'Api access token', '[]', 0, '2021-06-10 01:59:35', '2021-06-10 01:59:35', '2022-06-10 07:29:35'),
('ca88205dfd2dd494d3d62017d94260999b9b08b4768dbbc36eb1ddd4cd75d169203629e4358ce0ad', 612, 3, 'Api access token', '[]', 0, '2021-05-13 02:28:56', '2021-05-13 02:28:56', '2022-05-13 07:58:56'),
('ca91d79d24691e0c602b47b6832fb57b4bc6d6c18a27343c9023c8962e2ffabc16586ee7f0bb66a4', 437, 3, 'Api access token', '[]', 0, '2021-03-23 01:18:56', '2021-03-23 01:18:56', '2022-03-23 06:48:56'),
('caa65ef2d7ff1b7df67cf2e973d9d2ae9f69c45cf8015cc80c64aa6171bb66496d784a7a94867b13', 393, 3, 'Api access token', '[]', 1, '2021-03-18 04:00:59', '2021-03-18 04:00:59', '2022-03-18 09:30:59'),
('cad314e472f59302ce571799897e9eadbfaa47dd286f5218e18ad3f9c53b8cec491969fc51f56de2', 626, 3, 'Api access token', '[]', 0, '2021-05-20 09:10:55', '2021-05-20 09:10:55', '2022-05-20 14:40:55'),
('cae54da06c0083c8aa66e4396594386fa75cf72b0ab1b75d1ff93d1ac7cdc8493c5bbfef5637cdeb', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:19', '2021-06-10 02:37:19', '2022-06-10 08:07:19'),
('caf0dee76d5c98c24e000e4c2011c17c4417797af0df406217c219c4ad98138f1f0c7a96d4349d46', 37, 3, 'Api access token', '[]', 0, '2021-07-28 07:35:49', '2021-07-28 07:35:49', '2022-07-28 13:05:49'),
('cb01697e5bc74e72295883b416c9cd35874b0428b453c9fa83debd1b49cbe9a97048ea68c913c0ff', 2, 3, 'Api access token', '[]', 1, '2021-07-22 06:35:47', '2021-07-22 06:35:47', '2022-07-22 12:05:47'),
('cb1806f49a73ea4ea84e39e265e20b0fe4ade05914d02affa7f915139612537c4d1d08a1523da929', 773, 3, 'Api access token', '[]', 0, '2021-07-19 04:51:23', '2021-07-19 04:51:23', '2022-07-19 10:21:23'),
('cb4efcaaf6e32cbd183e27f9821eceffe941a3cff603dc23903a41d228cd95b6dbca3c0109186d6a', 703, 3, 'Api access token', '[]', 0, '2021-06-04 04:24:45', '2021-06-04 04:24:45', '2022-06-04 09:54:45'),
('cb5949a6ebf35efd2d26a6c2674faacac14c47e17bc48f974405748c2d12c7e3b59ac5967b1ec21c', 739, 3, 'Api access token', '[]', 0, '2021-06-15 04:17:45', '2021-06-15 04:17:45', '2022-06-15 09:47:45'),
('cb68bcceb3d969a08d42d3d973ca02028a2de2bf8902c6e8fc2da69120b934b992971ecd7310928b', 449, 3, 'Api access token', '[]', 1, '2021-03-24 04:50:46', '2021-03-24 04:50:46', '2022-03-24 10:20:46'),
('cb80cb694d81bf3f63b5e9cf365566bbd1991259a1a0a9ba8bb92af9ed6dd6e4b128e13c46191ccf', 706, 3, 'Api access token', '[]', 0, '2021-06-07 02:54:52', '2021-06-07 02:54:52', '2022-06-07 08:24:52'),
('cb83b8b89295ccd4f35dd7db3962961e6334d4bb796bce312ca26674df1768ca858ae1e59805a228', 23, 3, 'Api access token', '[]', 0, '2021-08-04 05:42:14', '2021-08-04 05:42:14', '2022-08-04 11:12:14'),
('cbd048b4f850b1efe786f90f188a8c49a9fde9c729aefd04e922de54f62e663aacc176459d0f9441', 17, 3, 'Api access token', '[]', 0, '2021-07-29 23:21:23', '2021-07-29 23:21:23', '2022-07-30 04:51:23'),
('cbd8d992e4f1d451e247516b5559533ffdf04d94c4a5d2ffb09c08e36d344028da984d09746dd1cc', 5, 3, 'Api access token', '[]', 0, '2021-08-10 07:25:36', '2021-08-10 07:25:36', '2022-08-10 12:55:36'),
('cbfe9eb4e1ab253e296ef3204747524790ef00ba91b53a9933364d41078fa89aa98171800a1812c4', 7, 3, 'Api access token', '[]', 1, '2021-07-26 01:12:22', '2021-07-26 01:12:22', '2022-07-26 06:42:22'),
('cc1317f5702398b970b98d4eafcc24eea07fb9783f6083df01abae6ed07b1b74c2845a125a2a592c', 7, 3, 'Api access token', '[]', 0, '2021-08-04 01:02:00', '2021-08-04 01:02:00', '2022-08-04 06:32:00'),
('cc443f6ad1a7378eb5d19fe1dec6369c8cc9629e56aa91e95618884d824c77f7c4cea92188cc487d', 722, 3, 'Api access token', '[]', 0, '2021-06-10 01:41:24', '2021-06-10 01:41:24', '2022-06-10 07:11:24'),
('cc826689b2d08f0d7fd349c8bbc568e4ff43dc938a6f71ecb95459e2c1f9a0801433304c641ec91c', 606, 3, 'Api access token', '[]', 0, '2021-05-02 07:55:25', '2021-05-02 07:55:25', '2022-05-02 13:25:25'),
('cc859af8a8ee724382c17f1eed16e7b0f3f3995fe96d6dff773e42da45b3fb77f1cc0717f9c84252', 4, 3, 'Api access token', '[]', 1, '2021-08-11 02:55:06', '2021-08-11 02:55:06', '2022-08-11 08:25:06'),
('ccb4a8524ec59609fae7f57c6392798c2cb0af846293ebc0681cf1d72ee460042a57427562d431e6', 507, 3, 'Api access token', '[]', 0, '2021-04-01 04:17:56', '2021-04-01 04:17:56', '2022-04-01 09:47:56'),
('ccd3163b9cda6a2ffde40ac4a702f4d60138afb81f52824b5ce1d42e51d13356ac4621b67f061646', 2, 3, 'Api access token', '[]', 1, '2021-07-29 00:53:41', '2021-07-29 00:53:41', '2022-07-29 06:23:41'),
('ccec23b4071fca7f4eea3b2c933a8827041c65240b862f48cf0b6e9b0380b346ed0bcc050754e8e0', 580, 3, 'Api access token', '[]', 0, '2021-04-20 03:40:49', '2021-04-20 03:40:49', '2022-04-20 09:10:49'),
('cd6c24bddd5def255b34335304b351c223ddd6e6399cebca046f745b963b7778fd8e2f71c493bf60', 19, 3, 'Api access token', '[]', 0, '2021-07-30 01:36:48', '2021-07-30 01:36:48', '2022-07-30 07:06:48'),
('cd887e378147bca954c6632a3bae408eb566d94d84e80c104870eef266ff30089a1d753f6e881512', 619, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:35', '2021-05-02 08:31:35', '2022-05-02 14:01:35'),
('cdab03af92c95e112b7aa3d214d741fd99802639e59de5382f7a997f1dd3b3283d099bb0fc32add9', 773, 3, 'Api access token', '[]', 0, '2021-07-19 06:45:57', '2021-07-19 06:45:57', '2022-07-19 12:15:57'),
('cdc11343285f5bab17b313993e4e045a512431f9d7e8798522e50c7a6ef800b8d47b0a9f1813fbdf', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:16:19', '2021-07-14 02:16:19', '2022-07-14 07:46:19'),
('ce1d61d33ea1875bb19c28fde7cd853ff05bcbb7341e1234f260c863e5a25e5d3c17aedb152815f5', 622, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:41', '2021-05-04 01:27:41', '2022-05-04 06:57:41'),
('ce3accca0430094d964f3edaf7c4f2d17457e0458717da0450febb483d84d3abd3aecd709a756efe', 439, 3, 'Api access token', '[]', 0, '2021-03-23 02:13:09', '2021-03-23 02:13:09', '2022-03-23 07:43:09'),
('ce425b04581e6e3a7123e319ca150c58de5e97bac7467ed2ea18a2de30b1dbe406c1903e6ca81e0a', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:42', '2021-05-04 00:40:42', '2022-05-04 06:10:42'),
('ce47c4c167cdca3250052fec7511a509d2f4acf91fa1977a902e8966fb18f3d8b9299c1ff8da2203', 515, 3, 'Api access token', '[]', 0, '2021-04-02 06:56:19', '2021-04-02 06:56:19', '2022-04-02 12:26:19'),
('ce6ff38267712a40058bbd6348617351322375570faac28f2e7144a55a55e9d29e7bf36d29b767ef', 623, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:42', '2021-05-04 01:27:42', '2022-05-04 06:57:42'),
('ce797e06efa90cf2e9cc5f068d8f540f29e5d17f602e165469dc631da5799fb9928470ded2741f62', 630, 3, 'Api access token', '[]', 0, '2021-05-08 01:07:15', '2021-05-08 01:07:15', '2022-05-08 06:37:15'),
('ce9cea5db6501a7aee9556684eb2e15c860be60eaf362b5580e628ac83703bdbb2611a1764af7f7d', 618, 3, 'Api access token', '[]', 0, '2021-05-12 02:38:18', '2021-05-12 02:38:18', '2022-05-12 08:08:18'),
('cea9b86ee6cf3353d14d427327cbf0f7787efa42e825fc62cff87725cff335f0f0b331918c6be03d', 638, 3, 'Api access token', '[]', 0, '2021-05-12 08:39:14', '2021-05-12 08:39:14', '2022-05-12 14:09:14'),
('cebddd798c0eed753cc59303f00d09aa71c48b34c0bd8d7324177582cf7ab3827a8953fe6388454d', 8, 3, 'Api access token', '[]', 0, '2021-07-26 01:30:47', '2021-07-26 01:30:47', '2022-07-26 07:00:47'),
('ced0d96c7a16fbdd8ec94109a17c10c21e8450eec792a691456ebad47032b26cd90118a1ad5eace5', 671, 3, 'Api access token', '[]', 0, '2021-06-02 06:48:34', '2021-06-02 06:48:34', '2022-06-02 12:18:34'),
('cef2563c0dd1848540940a117b9c357b9b549e50fb0bae69837251d0b7da9fa5f03b79e3378a0140', 1, 3, 'Api access token', '[]', 0, '2021-08-18 02:55:19', '2021-08-18 02:55:19', '2022-08-18 08:25:19'),
('cefee562d7c60055f9bfd8669b007e775a3871826881cc7f5ee2507c58d0da6c3556d6e6028dba7b', 416, 3, 'Api access token', '[]', 0, '2021-03-19 02:11:22', '2021-03-19 02:11:22', '2022-03-19 07:41:22'),
('cf0400a6de21415aeedbd31bbe8bd98f98ee91c9fdd4bb6748187958e836ceba670b4be4d3ebb2b6', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:13:10', '2021-07-14 02:13:10', '2022-07-14 07:43:10'),
('cf28b427b601554dc092e79a4d225f02571ca43d4371d996164c8bf422cd153c1691afb701c58754', 511, 3, 'Api access token', '[]', 0, '2021-04-02 06:36:12', '2021-04-02 06:36:12', '2022-04-02 12:06:12'),
('cf2a7638922c203f9cca345ce497fec80510dd8ea66cb96ff5b42a498ee93f6de011db07d28045d6', 560, 3, 'Api access token', '[]', 0, '2021-04-14 05:17:49', '2021-04-14 05:17:49', '2022-04-14 10:47:49'),
('cf40fcc1028da233d02034621e43b149033a5352efca678b55b41d8b61da3e227a9d7b42b4130174', 538, 3, 'Api access token', '[]', 0, '2021-04-05 02:44:48', '2021-04-05 02:44:48', '2022-04-05 08:14:48'),
('cf9fe7121357ec93c5f314f76d7e65c62339d6b5a6f968bed722d7978fdb6a911adaba47df28c733', 430, 3, 'Api access token', '[]', 0, '2021-03-23 00:15:41', '2021-03-23 00:15:41', '2022-03-23 05:45:41'),
('cfb5576425fed08716459791ea2ed991dd5b31d2217802e29808d017e567e1ed261ee15208be20d8', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:57:27', '2021-05-13 06:57:27', '2022-05-13 12:27:27'),
('cfb94e510ac2d192da849fe32b92f182461a08b22d59db18c35798591ba240db68032e370b794228', 623, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:48', '2021-05-02 08:41:48', '2022-05-02 14:11:48'),
('cfbd6fcd418c379d3c0b4b4c770b13182de424b335729d8bcb74f0b8e51665e26706dfcb803bb2fc', 444, 3, 'Api access token', '[]', 0, '2021-03-23 02:53:57', '2021-03-23 02:53:57', '2022-03-23 08:23:57'),
('cfbdff7b3a552ffd2b98f8c79171296c4a1a9d12dc25470414fb8950e6dcca7e5326d296ac52ba09', 769, 3, 'Api access token', '[]', 0, '2021-07-15 06:44:09', '2021-07-15 06:44:09', '2022-07-15 12:14:09'),
('cfdd2346ffadc070572de8742c2e2ff141e3e9c03a11915033f8960924a438716bccb68bce01ccb0', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:32:11', '2021-05-27 04:32:11', '2022-05-27 10:02:11'),
('d02e79e408e80424d1369ecedbfb560dfa4664c364d9a416a6d9d15ed65ac1179e5a154f810c1168', 24, 3, 'Api access token', '[]', 0, '2021-07-29 04:33:53', '2021-07-29 04:33:53', '2022-07-29 10:03:53'),
('d0394d50024359c50aea89bae6c7591d4894d1620aabcc3798cbcd4331a004cdbeb25b402ef0603f', 686, 3, 'Api access token', '[]', 0, '2021-06-02 08:16:07', '2021-06-02 08:16:07', '2022-06-02 13:46:07'),
('d04601566f05fdedb4d182aaec9e966051f0b4af8bf7932a574f06aa285891e8e8d8c4655d55c272', 550, 3, 'Api access token', '[]', 0, '2021-04-13 23:36:31', '2021-04-13 23:36:31', '2022-04-14 05:06:31'),
('d07fb95bb84f4ffaddbf88f9bfa3a5b027471260d4733c09d61f8c7d03a1b62fcd4d37c4e0b2c84d', 497, 3, 'Api access token', '[]', 0, '2021-03-30 04:05:19', '2021-03-30 04:05:19', '2022-03-30 09:35:19'),
('d09749fea76bf8b2690945aa4f91a25f44da7bc7af826cfe7415271d28272d0fd1b6d97099e048f3', 531, 3, 'Api access token', '[]', 0, '2021-04-23 08:43:38', '2021-04-23 08:43:38', '2022-04-23 14:13:38'),
('d0aa040204e2fc5ff4745681523673ad8bcc1ac78064481bdbb1c86423400c778e9b437b97b521fa', 5, 3, 'Api access token', '[]', 0, '2021-08-06 05:04:49', '2021-08-06 05:04:49', '2022-08-06 10:34:49'),
('d0b7ed92abf5e510909f5037c994f5cc30c15f4abe1cf809d095fdb2fb0feba80e53ed947779f989', 656, 3, 'Api access token', '[]', 1, '2021-05-24 05:31:49', '2021-05-24 05:31:49', '2022-05-24 11:01:49'),
('d0c2a557b828597bf6c71ec758378d8d20fc7dfb7e2553a16d0c8d53989fac93ebaccea7d5b8768f', 504, 3, 'Api access token', '[]', 0, '2021-04-01 05:52:08', '2021-04-01 05:52:08', '2022-04-01 11:22:08'),
('d0d3c6b6aba90069d939694a44c97520b2428d5e5881cb01ab25b125f49b54b702886ba98f2bf5cc', 47, 3, 'Api access token', '[]', 0, '2021-08-03 05:01:17', '2021-08-03 05:01:17', '2022-08-03 10:31:17'),
('d0e63a471e92da6d90194de163aa2783a98ddee0e38f14253eb524b37f104b66c092545c651c7c1e', 714, 3, 'Api access token', '[]', 0, '2021-06-08 09:41:22', '2021-06-08 09:41:22', '2022-06-08 15:11:22'),
('d0f091dec5a29b23eb66c8fbc70d05f9552d5f2f763bbe78cea93f161c26c735e109aee221629aa6', 543, 3, 'Api access token', '[]', 0, '2021-04-06 01:48:59', '2021-04-06 01:48:59', '2022-04-06 07:18:59'),
('d1007c25f9cf1030d879cfd4fa317ceccdb6d664b2bfc62da73d2a135e62031c5e1717c3de1cbabe', 488, 3, 'Api access token', '[]', 1, '2021-04-01 02:53:37', '2021-04-01 02:53:37', '2022-04-01 08:23:37'),
('d1026cc7ae0e269898bb75a932d4c03c1e299d2981b071355aa2aedf1fc1c779fa04a5c33ed53807', 626, 3, 'Api access token', '[]', 0, '2021-05-11 05:13:01', '2021-05-11 05:13:01', '2022-05-11 10:43:01'),
('d1211f7154eab1b448157f72580f6c38589a6847f45f296944f8638f1aae236bfbf240f93f1db65d', 65, 3, 'Api access token', '[]', 0, '2021-08-16 03:11:12', '2021-08-16 03:11:12', '2022-08-16 08:41:12'),
('d12a46656e8ca569b3c624237e0af052b0d809515568a492375ec3bc574424d577dda618695d868b', 623, 3, 'Api access token', '[]', 0, '2021-05-04 00:40:41', '2021-05-04 00:40:41', '2022-05-04 06:10:41'),
('d14c8cd98640be068d0c6bab950829430c28c95a72fb9b5773409bbd30e67fd3292fe97f56046033', 771, 3, 'Api access token', '[]', 0, '2021-07-15 09:15:17', '2021-07-15 09:15:17', '2022-07-15 14:45:17'),
('d15632390939878b4466ec751111f1b7f15e0321b60b9ee6163e2089c7a6742ceb03d7d53bb06d98', 660, 3, 'Api access token', '[]', 0, '2021-05-29 02:36:44', '2021-05-29 02:36:44', '2022-05-29 08:06:44'),
('d15f1bf63ce014c692e8eabaf500f7a3a807df7cc2fae3d060348f5c7257facf0d4d9be963867dc3', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:29:09', '2021-05-11 01:29:09', '2022-05-11 06:59:09'),
('d16e43cd858da08b649fc088a72c6df3744a4c86c7e466268b30cd2508eca0e867398ab34ef62004', 470, 3, 'Api access token', '[]', 0, '2021-03-25 06:06:05', '2021-03-25 06:06:05', '2022-03-25 11:36:05'),
('d1cf654df5a6445e5bd9b9e7fa2232007ec36dabf96655bfea581bcacdfbd7f2511d81fc2951db61', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:56:01', '2021-05-13 06:56:01', '2022-05-13 12:26:01'),
('d1ea764eb43961adb3e06c1ca31d927a306a086b146a97a211a5b536f95e8bdbc3320b0150cd2daa', 632, 3, 'Api access token', '[]', 1, '2021-05-09 04:30:04', '2021-05-09 04:30:04', '2022-05-09 10:00:04'),
('d1f097a665e7afa36c314ff16def4696eaf09bcbce175e89fab57a4545796f6b9a558e6f3d7c361f', 496, 3, 'Api access token', '[]', 0, '2021-03-30 00:44:39', '2021-03-30 00:44:39', '2022-03-30 06:14:39'),
('d1f0e29632ff9dd146a3d5e081be7152d01a8e0388999f885742be28fffaad521276a4bcd8e4c2a5', 7, 3, 'Api access token', '[]', 1, '2021-07-28 03:00:36', '2021-07-28 03:00:36', '2022-07-28 08:30:36'),
('d20af8aaef826c3e3cf7d1adafa70c559fc857489f024c450465483dbea919f5c1d31fa6e6154be1', 9, 3, 'Api access token', '[]', 0, '2021-07-26 04:53:57', '2021-07-26 04:53:57', '2022-07-26 10:23:57'),
('d24b28e3e82251a7256c02c79335931fcb0ee80c4d1f55a2a868aa833704d883789c44b07f07d297', 486, 3, 'Api access token', '[]', 0, '2021-03-31 23:38:50', '2021-03-31 23:38:50', '2022-04-01 05:08:50'),
('d251d855e3485418582c67a882298e6190477a1b8dac017ffe7bfbfefadd158edd6f239f985bb15c', 400, 1, 'Api access token', '[]', 0, '2021-03-17 06:23:50', '2021-03-17 06:23:50', '2022-03-17 11:53:50'),
('d27db8ffbc767456e7803605c2a3a9d07108016d11284999b124aec06c26b9ea0f0cb5159d618768', 628, 3, 'Api access token', '[]', 0, '2021-05-05 04:30:41', '2021-05-05 04:30:41', '2022-05-05 10:00:41'),
('d2853ec0598868377df50d4c8d9228ebda66124d30bceab055efa24844d2a948d317ee9582f769a8', 609, 3, 'Api access token', '[]', 0, '2021-05-02 07:56:18', '2021-05-02 07:56:18', '2022-05-02 13:26:18'),
('d2d075d40d8bb6ca606e7885d15ce46a8c4cb3913132f903de54160e32a2d8ccd2bb84b66c29ab8f', 626, 3, 'Api access token', '[]', 0, '2021-05-11 07:02:50', '2021-05-11 07:02:50', '2022-05-11 12:32:50'),
('d2dd6fa1b9151d9fb61414623648b8559020bf769f056a984cf42ca205f1b86f03c713e30fd2bb6c', 537, 3, 'Api access token', '[]', 0, '2021-04-05 02:23:52', '2021-04-05 02:23:52', '2022-04-05 07:53:52'),
('d2edbacfdc1cd07c3655c2e77c94b9240bc4f1940bff90692be1ab22258c4d24d5a89ac08359a128', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:54', '2021-05-04 01:11:54', '2022-05-04 06:41:54'),
('d300d11b81d99896af4f560e9c6cbbd504385ae106ead26fbf1ca84b85cf7e68db54433899e64cd6', 687, 3, 'Api access token', '[]', 1, '2021-06-02 11:12:11', '2021-06-02 11:12:11', '2022-06-02 16:42:11'),
('d3098d7717913cad68aa6575676c8f3b8f01d9fdd77a6c92441f4410c25c0c3eeaea3fc6b8110b77', 552, 3, 'Api access token', '[]', 1, '2021-04-15 01:36:07', '2021-04-15 01:36:07', '2022-04-15 07:06:07'),
('d3436a0f8d8e245c3126acff6daa906226e89955d98ca4fc23d36ce03a5cc3a5f056825c8347b65f', 640, 3, 'Api access token', '[]', 0, '2021-05-12 05:39:20', '2021-05-12 05:39:20', '2022-05-12 11:09:20'),
('d36b39c57f03d7e96243d8f10f4602860bede4c278dc5f37e2b35b89ec553bd1516fccef1f7a4bfb', 696, 3, 'Api access token', '[]', 0, '2021-06-03 00:34:31', '2021-06-03 00:34:31', '2022-06-03 06:04:31'),
('d3955e83b8bc1c1fbdb880e25ff6ebc4265e757b8499ba53d5eef90b2c3335722341c6bce0aa0fac', 682, 3, 'Api access token', '[]', 0, '2021-06-02 07:52:43', '2021-06-02 07:52:43', '2022-06-02 13:22:43'),
('d39debfa133f9c8d6a91e0592ad2a0e19f6f76c3e21dd8cc8dd25c6aae2c5dc970c118c009e492cb', 693, 3, 'Api access token', '[]', 0, '2021-06-03 00:29:42', '2021-06-03 00:29:42', '2022-06-03 05:59:42'),
('d39e608073ee50b57adc8419eec40e2267f64d56023ebf7c6af64573d5e1397bfae099504a46a487', 516, 3, 'Api access token', '[]', 1, '2021-04-02 21:13:20', '2021-04-02 21:13:20', '2022-04-03 02:43:20'),
('d3a85da8daaff2d3905b60f3da13e15afae5ff1323304b147060b8c77e3df891ddb92e72e661fb94', 628, 3, 'Api access token', '[]', 1, '2021-05-05 04:30:41', '2021-05-05 04:30:41', '2022-05-05 10:00:41'),
('d3e24d87fd7eb61c8668e2d0e59818d5d1686988f31ce7f74873bb271196684c22230912fda02410', 726, 3, 'Api access token', '[]', 0, '2021-06-10 01:45:24', '2021-06-10 01:45:24', '2022-06-10 07:15:24'),
('d4015960e4bae453e61ec4fcd4aa5d3c178b7a20a48952590d02215e7c4e9bb51beb9e7f676d2455', 421, 3, 'Api access token', '[]', 0, '2021-03-22 05:23:45', '2021-03-22 05:23:45', '2022-03-22 10:53:45'),
('d41e34c5c3a778418cb7b26e8bb08c3b21ea7a9adff05e327cbb091bee7799cbd6beb0697096a42e', 773, 3, 'Api access token', '[]', 0, '2021-07-19 06:37:50', '2021-07-19 06:37:50', '2022-07-19 12:07:50'),
('d42b24c35bf6660165b46517de6b3681ccbb1602614a9a4c8065309563f1d515a80bb76fa246007f', 554, 3, 'Api access token', '[]', 1, '2021-04-15 07:22:21', '2021-04-15 07:22:21', '2022-04-15 12:52:21'),
('d48711f34a111635171f884795b697717cd06b6ca7e81940f32e7f19d7133aec310842396c5091ef', 654, 3, 'Api access token', '[]', 0, '2021-05-19 06:24:27', '2021-05-19 06:24:27', '2022-05-19 11:54:27'),
('d4da6dd0f778001da4cd4423cc8d7144f0a9c7dac413ce2cd0f9ab85b28dc63b166e449fe2950f23', 17, 3, 'Api access token', '[]', 0, '2021-07-28 07:47:28', '2021-07-28 07:47:28', '2022-07-28 13:17:28'),
('d56105b636605eb89f11c809c80b0741a153c27a1cbef10a612ef636f9643e92ada95464d28871d4', 587, 3, 'Api access token', '[]', 1, '2021-04-29 04:13:47', '2021-04-29 04:13:47', '2022-04-29 09:43:47'),
('d5ab9d326ec670ee496e7265e01e8dd9a8ac9c7281c21634613a9316f92cb918a9a96f19ae9e4729', 414, 3, 'Api access token', '[]', 1, '2021-03-19 01:22:52', '2021-03-19 01:22:52', '2022-03-19 06:52:52'),
('d5e4858e85c7a2f595ca212637ce7e1ebf09467ac6718d39c568b2f8ee7918ba5493e964925fe8fb', 396, 3, 'Api access token', '[]', 0, '2021-03-19 05:25:58', '2021-03-19 05:25:58', '2022-03-19 10:55:58'),
('d5fe6e3debffc6b34b3b1a0576b4338dfaff957ce1ad4e289043e370bb518c4dd9290e08cb1bccf6', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:03:10', '2021-07-15 07:03:10', '2022-07-15 12:33:10'),
('d60921cd4baea170a08ff143ace847455fa9f442af4daf31ee9e2db5e6473c2e326e6c9e476f27b5', 773, 3, 'Api access token', '[]', 0, '2021-07-20 08:42:27', '2021-07-20 08:42:27', '2022-07-20 14:12:27'),
('d639c2600dfe0ebc319b5f92fb66a3f707674a66bf12014a204f59358e4673ad7fe96c2aba625e54', 650, 3, 'Api access token', '[]', 0, '2021-05-14 11:03:02', '2021-05-14 11:03:02', '2022-05-14 16:33:02'),
('d648ad23646c3eabccf6433def5ffa55de345b797aa03205c0624137a4d9dc2fa5b98bf0a4657860', 1, 3, 'Api access token', '[]', 0, '2021-08-05 04:41:29', '2021-08-05 04:41:29', '2022-08-05 10:11:29'),
('d66722c95589c8dacc16548e8f6f2a3818183db1f86bf5304774f3812d3330c0458e9dc1693a12c0', 427, 3, 'Api access token', '[]', 0, '2021-03-22 05:50:52', '2021-03-22 05:50:52', '2022-03-22 11:20:52'),
('d676dbed7828818463f7cdfe84381ee82460fa4003f706ef501f5f5f6be2dc06fc257914acecdbf3', 554, 3, 'Api access token', '[]', 0, '2021-04-14 00:35:47', '2021-04-14 00:35:47', '2022-04-14 06:05:47'),
('d6efaa7b762e9147911ddd1315f70bee657d63e8c11a4e75fe98344ac2aebefe33b53024db530eb6', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:57:27', '2021-05-13 06:57:27', '2022-05-13 12:27:27'),
('d712c0529ffdf0730583ee43617bfb258062fc487112bee45802e20eb0f5e961c7022f07a81933dd', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:52:21', '2021-07-20 02:52:21', '2022-07-20 08:22:21'),
('d71b85b6b71e3f76e5464e57790f9c9e0452d13c7fbf5999180b27fa25a285f9ce1c0828239cc477', 432, 3, 'Api access token', '[]', 1, '2021-03-23 05:45:42', '2021-03-23 05:45:42', '2022-03-23 11:15:42'),
('d732d4a0cbc188306164a9974e953e5846031de7706215f877230e83834aa06bc3064b119a3fa5e5', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:13:56', '2021-05-11 06:13:56', '2022-05-11 11:43:56'),
('d7586d67931743d5957c034bee73c814d92d7172844b9c41490ccae5ddb5c676fc017c14f98d8eeb', 542, 3, 'Api access token', '[]', 1, '2021-06-16 22:54:48', '2021-06-16 22:54:48', '2022-06-17 04:24:48'),
('d76d9ccd78bdd5f1cd9b13b65e47782dd840011a93c48063b1732ea913bc2b873b352ba9e9fde961', 445, 3, 'Api access token', '[]', 1, '2021-03-23 03:00:25', '2021-03-23 03:00:25', '2022-03-23 08:30:25'),
('d773452a9798f268638c47f02f6a8afd33451a9e2f819bc0e0c961a9d0081ee0aae1eff429f67a0f', 555, 3, 'Api access token', '[]', 0, '2021-04-15 05:42:19', '2021-04-15 05:42:19', '2022-04-15 11:12:19'),
('d79d8a418cb475fbe9b3cc410c966bcdb60b0c4358582ba682c787e344d26105996dc380c46186e7', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:13:58', '2021-05-11 06:13:58', '2022-05-11 11:43:58'),
('d7b82d4075e7fd319cd81c042271a1cf9fbd4e3adf8a3c1bf4e1dea260411b0e813b4b71a06ed77b', 13, 3, 'Api access token', '[]', 0, '2021-07-29 07:32:52', '2021-07-29 07:32:52', '2022-07-29 13:02:52'),
('d7cfbe75e3af1eb8744494e39467006ed65577fc7ebcdbe5c6a0959bf1400573f9d526374da990f5', 625, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:22', '2021-05-15 06:22:22', '2022-05-15 11:52:22'),
('d7fc211e78b20098508ef95b50a4c86ba14417890fd31db2c366a52af0d8c74a387ccbd1ba58b08b', 51, 3, 'Api access token', '[]', 1, '2021-08-03 07:35:31', '2021-08-03 07:35:31', '2022-08-03 13:05:31'),
('d80b417ede750e978ef73ddc132bb5d57af99d40358b25d5bcf6ba78ffebd8207a4525033e2ef97a', 560, 3, 'Api access token', '[]', 0, '2021-04-14 05:17:49', '2021-04-14 05:17:49', '2022-04-14 10:47:49'),
('d82bf08629e070aed2b02fdcf5cd3f601ebef755efdc2eb9f3c1a8e23f902bcdc83c3649cd305910', 661, 3, 'Api access token', '[]', 0, '2021-05-26 00:40:00', '2021-05-26 00:40:00', '2022-05-26 06:10:00'),
('d85d5c687c055d852b3098a606bc5db30c71a426495a78fd995e98d47214c23cd2827ddbccc8e9d1', 515, 3, 'Api access token', '[]', 1, '2021-04-02 06:56:19', '2021-04-02 06:56:19', '2022-04-02 12:26:19'),
('d8962d65d5e3e68092a9d0252c6084f9d3de82ad2b313c1ec98d091362e00dc44da9dba614714b74', 518, 3, 'Api access token', '[]', 1, '2021-04-02 23:39:59', '2021-04-02 23:39:59', '2022-04-03 05:09:59'),
('d896b50aef2c5590c8077b07ad80c1be6fef6e0bd0855a4012cc037b3a1156ab09f1dcf009259d7b', 5, 3, 'Api access token', '[]', 0, '2021-08-06 23:29:28', '2021-08-06 23:29:28', '2022-08-07 04:59:28'),
('d898173a5c76a63c4a5b4bc3c7bcbafa8cadd97e2d267c1fd9a35fc9a420cfa4143d27393568c63c', 745, 3, 'Api access token', '[]', 0, '2021-06-17 02:14:46', '2021-06-17 02:14:46', '2022-06-17 07:44:46'),
('d8986d12708283330e443d8e97af70276839a164d5c4ba3d31ee60cd47b57f8d4a656b3d59df100b', 396, 3, 'Api access token', '[]', 0, '2021-03-19 01:47:42', '2021-03-19 01:47:42', '2022-03-19 07:17:42'),
('d8f8e50324d3f7f6cc248ccff1f60290c6b45bc4484bbd56d712ab9425bf5ee853c537418086bc4f', 46, 3, 'Api access token', '[]', 0, '2021-08-03 04:57:39', '2021-08-03 04:57:39', '2022-08-03 10:27:39'),
('d90e00b6c79db76cf047d585b696ec178093b06f140af5e422c97332800b16b58e8568bccb31d044', 518, 3, 'Api access token', '[]', 1, '2021-04-02 07:16:15', '2021-04-02 07:16:15', '2022-04-02 12:46:15'),
('d9221bc49bffeedb2a7a67a21686d5f88d276cc707c74d11aae481ccda7646413b4f9c9278675193', 434, 3, 'Api access token', '[]', 0, '2021-03-23 23:53:37', '2021-03-23 23:53:37', '2022-03-24 05:23:37'),
('d93ca88e86d2b5a97166ba43f7c6d93ca6af65fffffec717f13ef4935eb9e4b5d18a8d8ec2b64afb', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:31:36', '2021-05-02 08:31:36', '2022-05-02 14:01:36'),
('d94b0d2e714bd9488ba2cd5d608db530451ee03d8397b54e3a2aa901c5ae5859484a50dae3a69f4c', 393, 3, 'Api access token', '[]', 1, '2021-03-19 02:16:30', '2021-03-19 02:16:30', '2022-03-19 07:46:30'),
('d960a324771798b9abc71bb757038dfce573eee64d6a2eea66d73691e6621fa3e3c399dd3a74a60e', 401, 3, 'Api access token', '[]', 0, '2021-03-17 23:45:37', '2021-03-17 23:45:37', '2022-03-18 05:15:37'),
('d979059d6cddc45343132ae2ceb766ebc07f74347b905e11429a5e34a9b118543d0462db74f0809f', 462, 3, 'Api access token', '[]', 1, '2021-03-24 05:55:39', '2021-03-24 05:55:39', '2022-03-24 11:25:39'),
('d98cae389a0c2d1eb3b6b9b35ad0a7aadfab96a76e10ef9efcb517281280e5ddff4012fc80b345d4', 23, 3, 'Api access token', '[]', 0, '2021-07-27 23:41:36', '2021-07-27 23:41:36', '2022-07-28 05:11:36'),
('d990254ea4ca72a8b9830b1e714c25b8cf4832b8a38b0877c8ae8f854c5ec863bcb55ab65e34c0e8', 531, 3, 'Api access token', '[]', 1, '2021-04-04 23:41:35', '2021-04-04 23:41:35', '2022-04-05 05:11:35'),
('d99c7169aceae23845a4c95d06a2b33595b4103cc45b25ceb8555d773b77f7df73b1ca2805ccd8bf', 405, 3, 'Api access token', '[]', 1, '2021-03-18 02:01:36', '2021-03-18 02:01:36', '2022-03-18 07:31:36'),
('d9b894a8827bc67c80286b72b1cc542f029fbd19f58f74624c37089bd04c60c2a4385863d156a104', 452, 3, 'Api access token', '[]', 0, '2021-03-23 07:00:08', '2021-03-23 07:00:08', '2022-03-23 12:30:08'),
('da04e3674f594a8038d9514a519516731f030e4e3ccd308256babaf1275f8d446b77bb568cbb2389', 600, 3, 'Api access token', '[]', 0, '2021-05-02 07:45:03', '2021-05-02 07:45:03', '2022-05-02 13:15:03'),
('da0db2c9496d8c9bea003f424cd13a94f22fbf3591084993a73dfa25bcacac7ab31865024f02e2ec', 5, 3, 'Api access token', '[]', 1, '2021-08-07 01:40:01', '2021-08-07 01:40:01', '2022-08-07 07:10:01'),
('da19c0ce98342e72988876d32f6097470d3e5dfad13fea16d1179f5a666acd029ecf437292d61d84', 659, 3, 'Api access token', '[]', 1, '2021-06-14 08:12:02', '2021-06-14 08:12:02', '2022-06-14 13:42:02'),
('da1ea2bd810c813d18604daff6742106815746b7fc173afd68c3727d0d57cffc80d730eb9a07bbc7', 429, 3, 'Api access token', '[]', 0, '2021-03-22 06:04:18', '2021-03-22 06:04:18', '2022-03-22 11:34:18'),
('da52548923270f1afb5386bed3387e60d18b442f183eee913791c639894bbf5ecf02a8a51c4992ee', 620, 3, 'Api access token', '[]', 0, '2021-05-07 05:11:41', '2021-05-07 05:11:41', '2022-05-07 10:41:41'),
('da727528f8d42d8ac1afbcc54b650368f1f17ec219d00d047a989d5dd246f93ca3b4a7f38f6e31c7', 502, 3, 'Api access token', '[]', 0, '2021-03-31 04:28:34', '2021-03-31 04:28:34', '2022-03-31 09:58:34'),
('da9c995dff7832f6795ca95fa5beb13942923c50669038a3d64bf107bb7e018c1cfa7d3bdce8c1e2', 45, 3, 'Api access token', '[]', 0, '2021-08-03 03:05:50', '2021-08-03 03:05:50', '2022-08-03 08:35:50'),
('da9fb74ed4357388a8ec365f6adadf9f6a72776ef11df4ac57cd5707b4b3f841bc359a908bb4206d', 555, 3, 'Api access token', '[]', 1, '2021-04-15 06:49:29', '2021-04-15 06:49:29', '2022-04-15 12:19:29'),
('dab9df6e76d28511d43a963e04f30c60b318378f81b9a39824b30a237d3b5b00e8370924af475b1a', 40, 3, 'Api access token', '[]', 0, '2021-07-29 01:27:58', '2021-07-29 01:27:58', '2022-07-29 06:57:58'),
('daf6a89829bad1a031a60e615aa6f80915f807801b7412e703b5d3ddb4edcc664bd16e6a927ca85e', 710, 3, 'Api access token', '[]', 0, '2021-06-08 09:27:01', '2021-06-08 09:27:01', '2022-06-08 14:57:01'),
('db3a9f89817c36c0fb1837469e0e6d4a11ae24f6430d4c0c61051c43d0a5eba2d2f15b508015e9a5', 507, 3, 'Api access token', '[]', 1, '2021-04-01 04:17:56', '2021-04-01 04:17:56', '2022-04-01 09:47:56'),
('db8b829b1a3f7a528adfbd364c883d2354433638ff52699502602193db1d91a360ca9d0638cdea22', 434, 3, 'Api access token', '[]', 0, '2021-03-24 07:48:25', '2021-03-24 07:48:25', '2022-03-24 13:18:25'),
('db8fd63d938837526cb36ad5ff70102cde2feae73c4d76a8a05720e40e7e88f72eb3ae825eb56dba', 665, 3, 'Api access token', '[]', 0, '2021-07-14 04:40:55', '2021-07-14 04:40:55', '2022-07-14 10:10:55'),
('dba0422e4d270413956ce657a6e85bff98520c40e80bf4d1adda00bfd93062ae6ce52f052d99588e', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:00:30', '2021-05-11 01:00:30', '2022-05-11 06:30:30'),
('dba39b501bfcdb28af859fb9d7f65dee4ebbd9a653bed687521fc1044178c0ba0c1f9def5da85b10', 399, 1, 'Api access token', '[]', 0, '2021-03-17 07:45:06', '2021-03-17 07:45:06', '2022-03-17 13:15:06'),
('dbe13a91aa78d388e50423125fb3cabc674cf9784a3b64c5d266ee4ec8188424dd8c18dea9f45f42', 729, 3, 'Api access token', '[]', 0, '2021-06-10 01:59:34', '2021-06-10 01:59:34', '2022-06-10 07:29:34'),
('dc0c9fce36904ca12c49b66df7909d98eca404fb8c56f39c3b55a8a93d9047e0e4f6803289ec274d', 26, 3, 'Api access token', '[]', 0, '2021-07-28 06:46:43', '2021-07-28 06:46:43', '2022-07-28 12:16:43'),
('dc0d4bc78ce08822364d5c8bca3f54819053d712ec33e9573dba8fc430f8ab5f1887c9d47e940935', 511, 3, 'Api access token', '[]', 0, '2021-04-02 06:18:37', '2021-04-02 06:18:37', '2022-04-02 11:48:37'),
('dc1037f6b134f78209d76ad2ba8ee3652a742ec02c4c176e74b42c3f399aee433bcd84645e2b0e8f', 753, 3, 'Api access token', '[]', 1, '2021-07-21 00:15:42', '2021-07-21 00:15:42', '2022-07-21 05:45:42'),
('dc370f4c38ce48c73c0c049bd5f77b279da03c854c66ebf26555a0c6f178a75060296ebc511aada5', 52, 3, 'Api access token', '[]', 0, '2021-08-04 05:07:31', '2021-08-04 05:07:31', '2022-08-04 10:37:31'),
('dc94f39e2c109758738194ae500e2b5147776c9a945f5687a59a6558e3c169a444c41fbefc78dbfb', 552, 3, 'Api access token', '[]', 0, '2021-04-15 05:14:33', '2021-04-15 05:14:33', '2022-04-15 10:44:33'),
('dcada1596b98e87e272ff92a874c014074b32c881d54111086122431b628d4193243c86334865629', 773, 3, 'Api access token', '[]', 0, '2021-07-22 01:27:04', '2021-07-22 01:27:04', '2022-07-22 06:57:04'),
('dcb3d81ecac51af853f0a36ebd11696d82db3a48b342a4f139d000254057240b13b581e739d0e95e', 770, 3, 'Api access token', '[]', 0, '2021-07-15 06:45:06', '2021-07-15 06:45:06', '2022-07-15 12:15:06'),
('dcca83562d8879194374c6d42c3623bfbddc7e1b8454175b449f377b86bfa03ab703ab166d8ffbf5', 626, 3, 'Api access token', '[]', 0, '2021-05-27 04:52:22', '2021-05-27 04:52:22', '2022-05-27 10:22:22'),
('dccc7a13d9c44e104891a29f11f54c364a439c7a7ee1fb971e816f73e70aef20cb81ae9aa302efc7', 759, 3, 'Api access token', '[]', 1, '2021-07-01 22:13:56', '2021-07-01 22:13:56', '2022-07-02 03:43:56'),
('dcedc9e813e3557554f8b251b6b9fb0a4e89a0939a59ab13aef760633423a2f107c9758c486572fb', 2, 3, 'Api access token', '[]', 0, '2021-07-22 06:35:47', '2021-07-22 06:35:47', '2022-07-22 12:05:47'),
('dcfa2a46bd4cd9dad9c68e955eb1b8602e232b1b529fd6b16a2eab24144d806052e0b660a7ad790d', 746, 3, 'Api access token', '[]', 0, '2021-06-17 02:46:11', '2021-06-17 02:46:11', '2022-06-17 08:16:11'),
('dd1e77f0ca162ff6451694b92c5dd9c21f4c8a95ebd7210d8147da44edcee045806a5ee18b0508b6', 562, 3, 'Api access token', '[]', 0, '2021-04-14 23:56:09', '2021-04-14 23:56:09', '2022-04-15 05:26:09'),
('dd1f4a50def9304d3782c69db23677611602f01b6db9ba88620f86b4b8a80151f5a7113653dfef06', 466, 3, 'Api access token', '[]', 0, '2021-03-26 04:34:57', '2021-03-26 04:34:57', '2022-03-26 10:04:57'),
('dd84e57ee57a2440d5d53a5226b77a590ae13f7ed74fec71bc90b6e39a97c7e35539adc65a2712b9', 784, 3, 'Api access token', '[]', 0, '2021-07-21 06:49:48', '2021-07-21 06:49:48', '2022-07-21 12:19:48'),
('ddaa82680025f6422effcdcdb7ce9d2546d7cfd070562627e66eb6b4dbc0a9d5012a5b1fa6133d90', 773, 3, 'Api access token', '[]', 0, '2021-07-16 07:10:00', '2021-07-16 07:10:00', '2022-07-16 12:40:00'),
('ddbb1982d924c6204ff08ba69f765ef12a2938928d05b0d0e9f633c180c7667cea9ff8840aabb953', 488, 3, 'Api access token', '[]', 1, '2021-03-26 06:13:39', '2021-03-26 06:13:39', '2022-03-26 11:43:39'),
('ddbe5d081a4c600ba65a2b830d0be74a71be846731e9e0ab03e6397b5b6fe4fda1beaed30fd2dcff', 633, 3, 'Api access token', '[]', 1, '2021-05-11 03:58:41', '2021-05-11 03:58:41', '2022-05-11 09:28:41'),
('ddc2dfba9e85cdfc8668e351b1242243f80e3e56a60591273addae337b97eec53b2861067bf10919', 1, 3, 'Api access token', '[]', 1, '2021-08-18 05:31:37', '2021-08-18 05:31:37', '2022-08-18 11:01:37'),
('ddea805ea70f97054910e1e48465163333f883cee2d9a8b5e4e79d33969f1c0c95d31a557cd0de1e', 489, 3, 'Api access token', '[]', 0, '2021-03-31 06:53:17', '2021-03-31 06:53:17', '2022-03-31 12:23:17'),
('de1bd3ed1ec7424ffd2f4e489614a69e061b76cd7820280034cc3cecaa503e4431f0f2956cabebf5', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:23:51', '2021-05-11 06:23:51', '2022-05-11 11:53:51'),
('de2043488e76b73cffb0a4b23b1c251a15b28ba53fefe45362ae1e710050cb1bb09b4ccaec33188b', 776, 3, 'Api access token', '[]', 0, '2021-07-20 23:32:23', '2021-07-20 23:32:23', '2022-07-21 05:02:23'),
('de220f6e6f76d514a4610b3bea472c138c43661947b7910ad864fe497e06a39072ae753c5eb69571', 10, 3, 'Api access token', '[]', 0, '2021-07-26 06:50:06', '2021-07-26 06:50:06', '2022-07-26 12:20:06'),
('de26785a4c14e59a6269fa9eb51af220dd9107aae6e5ce43148ff5097806f30a19d4ef9c9a49a3e6', 7, 3, 'Api access token', '[]', 0, '2021-07-29 23:46:47', '2021-07-29 23:46:47', '2022-07-30 05:16:47'),
('de71e46bea33c7670fedb014f9d64d110310a86a63a350cbfcbbfb79f860537907e017a33e064fdc', 735, 3, 'Api access token', '[]', 0, '2021-06-22 01:01:42', '2021-06-22 01:01:42', '2022-06-22 06:31:42'),
('de7ad2b5e4320587d17f1c5b187c6ee699bb21abc1f4b0e40a3e29c38a24c747616e7373279bb3e3', 53, 3, 'Api access token', '[]', 0, '2021-08-05 02:57:24', '2021-08-05 02:57:24', '2022-08-05 08:27:24'),
('de7e8116ff74428b314842db964989ea1caf3bf3e10ea2dc3717caa405162b9c0d0d5bc19df98d91', 400, 3, 'Api access token', '[]', 0, '2021-03-22 07:52:07', '2021-03-22 07:52:07', '2022-03-22 13:22:07'),
('dea8df78bbada6da5cb933aa85ca1d9af5bc27e2cee0a13771d578b082ff1752977b5331033e512a', 767, 3, 'Api access token', '[]', 0, '2021-07-14 05:19:18', '2021-07-14 05:19:18', '2022-07-14 10:49:18'),
('debc9d098ec5fcf281e08938218573a2d98dc2dcc381a9ed13077107307876f3359f5b1fe51ec73d', 639, 3, 'Api access token', '[]', 1, '2021-05-12 07:03:45', '2021-05-12 07:03:45', '2022-05-12 12:33:45'),
('ded34d5698d0c6de80aa208a0541c178525cc74169b4710c2db31a0f6cfe2c9f4aff9a7d0e823881', 437, 3, 'Api access token', '[]', 0, '2021-03-23 01:18:56', '2021-03-23 01:18:56', '2022-03-23 06:48:56'),
('dedf1bc9c1f30f071a95b172e89c8ae87103bd15789fa5285ee26117e69af329e297bd29ee0b91ec', 533, 3, 'Api access token', '[]', 0, '2021-04-05 08:50:50', '2021-04-05 08:50:50', '2022-04-05 14:20:50'),
('df0993a9ac8cd6bee8f11506b8b63413cad9534f1de9ff0b85b5fcf0bee37a6c13c9504c70d1636f', 570, 3, 'Api access token', '[]', 0, '2021-04-19 02:07:21', '2021-04-19 02:07:21', '2022-04-19 07:37:21'),
('df17008cb4d04432fcd09a3e64338585fd177ee328c664b4ab83671174b40f94978851abd409f3ae', 759, 3, 'Api access token', '[]', 0, '2021-07-20 06:37:19', '2021-07-20 06:37:19', '2022-07-20 12:07:19'),
('df19c2b63ac115d507ec93a0d8e87955a351cbaedf8fd0a0b821601310c2c436436c0bbdf3a53897', 626, 3, 'Api access token', '[]', 0, '2021-05-11 01:34:37', '2021-05-11 01:34:37', '2022-05-11 07:04:37'),
('df318dd8f11e9e5ded3c2005bd462d8745534b92d6a72759d03d1515827c0e45507a3005841241b0', 618, 3, 'Api access token', '[]', 0, '2021-05-08 04:36:17', '2021-05-08 04:36:17', '2022-05-08 10:06:17'),
('df83241e7185a483cf903527ca37e9f49514cff2b3b6e583d7df9df89dd13490e11f725edfd12c5c', 59, 3, 'Api access token', '[]', 1, '2021-08-11 09:24:04', '2021-08-11 09:24:04', '2022-08-11 14:54:04'),
('df951be736ac70141b460cc08487172afd779dd2d873da9e78c21ca60ce4a23d75de0dfb3b98e649', 522, 3, 'Api access token', '[]', 0, '2021-04-03 00:04:37', '2021-04-03 00:04:37', '2022-04-03 05:34:37'),
('dfaba0401c5ffa7528218d1cc29696020169fb8077c68a25471a936c0eecf92beb94073b3cea35ae', 443, 3, 'Api access token', '[]', 1, '2021-03-23 02:52:38', '2021-03-23 02:52:38', '2022-03-23 08:22:38');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('dfad2cb53563f5e0e2fa66049146db87622fc2a5ca062cb6051dce324c2531dafae6eec56967ce67', 8, 3, 'Api access token', '[]', 0, '2021-07-26 01:30:47', '2021-07-26 01:30:47', '2022-07-26 07:00:47'),
('dfb6112fe0814fa16fac08346ab5d3f361f8030c132e17f768d00dc7d3f5901a897e4d2bdcf2bda8', 730, 3, 'Api access token', '[]', 0, '2021-06-10 01:59:35', '2021-06-10 01:59:35', '2022-06-10 07:29:35'),
('dfd3f4df49c53eac94235fb5d8bff130cfa4c0caef9bd8261a08e31b56e2d098f2b28d888d3b61bb', 48, 3, 'Api access token', '[]', 0, '2021-08-03 05:02:08', '2021-08-03 05:02:08', '2022-08-03 10:32:08'),
('e0178e7e457ff4039b752682e1b6684d7a344963251f240a391775e49b7306b4d19bd04e2027ebc4', 514, 3, 'Api access token', '[]', 0, '2021-04-02 06:48:34', '2021-04-02 06:48:34', '2022-04-02 12:18:34'),
('e02d56407196422c59597d77778cb73712668862b4caee200e332c77874406b0201ce5f6e28ca440', 52, 3, 'Api access token', '[]', 0, '2021-08-06 04:58:16', '2021-08-06 04:58:16', '2022-08-06 10:28:16'),
('e04084a84fa2e30d76ad8fbfe55a1876e3ee48caea037905cf87ce6d32100850372069432a55d4f6', 547, 3, 'Api access token', '[]', 1, '2021-04-29 04:08:10', '2021-04-29 04:08:10', '2022-04-29 09:38:10'),
('e051c8c397ffb4e75e38e6378999d5def8e4be9427b0d50fd794e65f9ed5a875f62d8053248b771f', 524, 3, 'Api access token', '[]', 1, '2021-04-03 00:55:26', '2021-04-03 00:55:26', '2022-04-03 06:25:26'),
('e0951816ff36a14b006939e5d17770b606478d1a200ef6e021eab8e5ed1501479060ffa5b7f70d82', 696, 3, 'Api access token', '[]', 0, '2021-06-03 00:34:31', '2021-06-03 00:34:31', '2022-06-03 06:04:31'),
('e09659d6d2b63cfc8e4f9f343f8bc364b71082aa200cad226808035c486254fc46f4ab91064e9b81', 721, 3, 'Api access token', '[]', 0, '2021-06-10 01:41:23', '2021-06-10 01:41:23', '2022-06-10 07:11:23'),
('e09fde3b94131522d4985b00711754814022d93ab4de0cf711b28f59f2b88037e398d1bc2c245c1b', 621, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:47', '2021-05-02 08:41:47', '2022-05-02 14:11:47'),
('e0bb5a790a603c49a2d2446ea449c6b0ae154b4a0e7c889df96f266265651e967279f540efe6a237', 17, 3, 'Api access token', '[]', 1, '2021-07-29 23:21:23', '2021-07-29 23:21:23', '2022-07-30 04:51:23'),
('e10aecb64cbfcbd872b020307a3c72cab79dbcfe985ff903f2f5dcee18ab28b6bec21acfe7257771', 17, 3, 'Api access token', '[]', 1, '2021-07-27 07:24:19', '2021-07-27 07:24:19', '2022-07-27 12:54:19'),
('e11ea613dfa9a8f9652bdb4c3f61762f33131f2558d3f5ab11c874593f900e56b9b10b8b1d2344f7', 520, 3, 'Api access token', '[]', 1, '2021-04-02 21:35:14', '2021-04-02 21:35:14', '2022-04-03 03:05:14'),
('e11eda8e3cbd399771d78fef7a15d921c0713c732598cbbc5da1cdeffb99eea2af79d21a356062cc', 11, 3, 'Api access token', '[]', 0, '2021-07-26 06:50:57', '2021-07-26 06:50:57', '2022-07-26 12:20:57'),
('e11f7117786a9c079bef5d8a9feb9f56a3b99cee8abbfbb86df2e46474132e0a4475c7dca3c10391', 723, 3, 'Api access token', '[]', 0, '2021-06-10 01:42:49', '2021-06-10 01:42:49', '2022-06-10 07:12:49'),
('e141afbbc1192e6557c3152b92dbe66f8077bead94019b665e0a4f1c666f6eea9ae20a6f9c70e507', 759, 3, 'Api access token', '[]', 0, '2021-07-20 06:56:33', '2021-07-20 06:56:33', '2022-07-20 12:26:33'),
('e195fc2aaeecf94c1336882ede515ccd84adf68fcca022b5f30288344882b26f933b11bb6d54cc3d', 533, 3, 'Api access token', '[]', 0, '2021-06-08 08:03:09', '2021-06-08 08:03:09', '2022-06-08 13:33:09'),
('e19c18aa4f8f032cbc030d4b3530468363223a0f2fb234c2b515fdb4bcba591083d09fa49f806288', 679, 3, 'Api access token', '[]', 0, '2021-06-02 07:47:38', '2021-06-02 07:47:38', '2022-06-02 13:17:38'),
('e19e838c348e9ccbefa047e062de6d5576d7e7c791c3060172e8c32c4cf41dd75395aa04e9dbbeeb', 1, 3, 'Api access token', '[]', 0, '2021-08-04 08:12:20', '2021-08-04 08:12:20', '2022-08-04 13:42:20'),
('e1a65dfc83625ad4451717c24912e9ce7839d7ea61b3b8459de304198ca60251f1a898bb167df1d3', 759, 3, 'Api access token', '[]', 0, '2021-07-21 02:28:58', '2021-07-21 02:28:58', '2022-07-21 07:58:58'),
('e1bf37bf764e09633deeff889895b57794ccbfd3965f6225a81a604165e69c41237283f6da595df0', 625, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:29', '2021-05-07 07:04:29', '2022-05-07 12:34:29'),
('e1d81aca0b58cd3733f8743e9f478462d3c04eda936a832fc8a8916dd1cb8fb2c5635adda8da20fc', 1, 3, 'Api access token', '[]', 0, '2021-07-29 04:32:57', '2021-07-29 04:32:57', '2022-07-29 10:02:57'),
('e20e928ac126ce83c1e35331511cbae1d16ef3db22e13e8bc33675941e51988c80884b53a3ff4e22', 11, 3, 'Api access token', '[]', 0, '2021-07-26 06:50:52', '2021-07-26 06:50:52', '2022-07-26 12:20:52'),
('e2357e46f81ff4268a68bf638597d8b06781a479792d5d33724848ef2b93433704e0854382e45dc7', 407, 3, 'Api access token', '[]', 0, '2021-03-18 06:50:19', '2021-03-18 06:50:19', '2022-03-18 12:20:19'),
('e243615b6023c9f3e81c8baebe64f22c6541220f022b67ae39f641eb89b3908a85ef9b9a3e199190', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:21:14', '2021-07-20 02:21:14', '2022-07-20 07:51:14'),
('e2464fe58efd11826f4f4cd6524f6573687016a0b641844586416b7dfbcb0307d1cc7b4120b10182', 569, 3, 'Api access token', '[]', 0, '2021-04-19 01:52:47', '2021-04-19 01:52:47', '2022-04-19 07:22:47'),
('e2912314dae662fd0f3f6cae9043272d7dbc2b0b83a01e8d11debd87dbbc0ae58ecfe50d0cfab40f', 731, 3, 'Api access token', '[]', 0, '2021-06-10 02:02:13', '2021-06-10 02:02:13', '2022-06-10 07:32:13'),
('e294032ef98436228f2652ae7295201baee9169fcf9d893abf7748576cda496b88226991bb504f0e', 654, 3, 'Api access token', '[]', 0, '2021-05-19 06:24:27', '2021-05-19 06:24:27', '2022-05-19 11:54:27'),
('e2bafcc96cfbad210d63837ce097005bb60e576ae21bd60856b47b79e03707a6dc6f11a7c470f8db', 4, 3, 'Api access token', '[]', 0, '2021-07-23 00:24:36', '2021-07-23 00:24:36', '2022-07-23 05:54:36'),
('e2bb9bc22a7dd51e06ba9c43c6877241cd93b65c56c8489658646dc5dc54d987713b0d3b4db5c334', 766, 3, 'Api access token', '[]', 0, '2021-07-14 02:10:26', '2021-07-14 02:10:26', '2022-07-14 07:40:26'),
('e2e5b1ae98e02835ff56381d4eb42090a57916aa09b18904c940b2b5fbc46789c597e7eb2961380e', 60, 3, 'Api access token', '[]', 0, '2021-08-11 23:18:01', '2021-08-11 23:18:01', '2022-08-12 04:48:01'),
('e2e7be78fb209514e6198c119fd07ec86c54b2f98b269b87fbe35d2ce1dd89abaeba1e9b86e55b37', 659, 3, 'Api access token', '[]', 1, '2021-06-01 07:34:10', '2021-06-01 07:34:10', '2022-06-01 13:04:10'),
('e2f5c409a5f69d6eeaf93fddc30e9cf2e1c5147a2f020e09483a177c78bbc92f2adcd34a4c5434cb', 582, 3, 'Api access token', '[]', 0, '2021-04-21 04:18:44', '2021-04-21 04:18:44', '2022-04-21 09:48:44'),
('e30edaf015a3ce4191fca9575d9ba5be5cd4878797ce64523e67860b12035eb0ff04087e89e897a8', 553, 3, 'Api access token', '[]', 0, '2021-04-14 00:17:08', '2021-04-14 00:17:08', '2022-04-14 05:47:08'),
('e31c3d07adf4a1c8c581c857c26c8f33599db48ec9891b1a43b971bb3894f62e542c49777a6b57e7', 498, 3, 'Api access token', '[]', 0, '2021-03-31 02:01:33', '2021-03-31 02:01:33', '2022-03-31 07:31:33'),
('e32533bad251334e41a465ef33396f5151cb54b3c37c73cab36cb4795f29e9dbb26ac87505a822fd', 773, 3, 'Api access token', '[]', 0, '2021-07-20 08:12:13', '2021-07-20 08:12:13', '2022-07-20 13:42:13'),
('e3477ceed00e0d30be07902959fe12b0d0fd75a6d90e7bd8fe449982f8534779a9b4e28a7121c39a', 703, 3, 'Api access token', '[]', 0, '2021-06-04 04:24:45', '2021-06-04 04:24:45', '2022-06-04 09:54:45'),
('e35490267a9da3505e5f242fbe28490d8cdc5a4c2e5353d7e1977d5edf4055bb2ccbb8237a48f975', 51, 3, 'Api access token', '[]', 0, '2021-08-03 07:31:35', '2021-08-03 07:31:35', '2022-08-03 13:01:35'),
('e363e655c46cfb8f975b7d06379db990dd399d83926db0be44e7681df37fd5cbb126c3e344e1cc0e', 393, 3, 'Api access token', '[]', 1, '2021-03-18 04:42:09', '2021-03-18 04:42:09', '2022-03-18 10:12:09'),
('e368d47235b087a2bdea3f668a30312c21654081ea50e64571f18b5e39fa4cb614f5b058f47774ee', 66, 3, 'Api access token', '[]', 1, '2021-08-17 05:58:13', '2021-08-17 05:58:13', '2022-08-17 11:28:13'),
('e37ae3a6df5a0ff51b46a8d1fc1bc05c32daf936058a208ed4247167672060008f9f7419f2286d0e', 736, 3, 'Api access token', '[]', 0, '2021-06-15 01:08:26', '2021-06-15 01:08:26', '2022-06-15 06:38:26'),
('e38ce99869f1f2ed7a6c987c528b52af75e45131eda87deffe77f73284660e0f1d8decef3eaebf17', 531, 3, 'Api access token', '[]', 1, '2021-04-12 04:12:12', '2021-04-12 04:12:12', '2022-04-12 09:42:12'),
('e38f4e823cee8f4d877baef2f30f07f2f56991a1d1096029a827cbf15d59f01ccae7f18c79fa9161', 649, 3, 'Api access token', '[]', 0, '2021-05-14 10:49:20', '2021-05-14 10:49:20', '2022-05-14 16:19:20'),
('e390d46dc4489ab72cfcf8b652461cba7256f097c14bef6758b3b2347b02ee38f9e6c69a6e35e76f', 759, 3, 'Api access token', '[]', 0, '2021-07-21 03:56:41', '2021-07-21 03:56:41', '2022-07-21 09:26:41'),
('e39d1ec409abaa8d87e1771381bd81422bc6d628dfdf34b71034d7bea1e491ef71096919d6c61dcf', 665, 3, 'Api access token', '[]', 0, '2021-05-31 00:38:13', '2021-05-31 00:38:13', '2022-05-31 06:08:13'),
('e39f4dad642e7c482a7d0317bebe42c6b29a6ed5379eef8b5e340377846e936188239bfeb60ef1d3', 579, 3, 'Api access token', '[]', 0, '2021-04-20 03:14:05', '2021-04-20 03:14:05', '2022-04-20 08:44:05'),
('e3d64b9f5c6be18bf137517981ab1fac485ce50295d57c58c412c64685fba976dfeecb5366470d0c', 498, 3, 'Api access token', '[]', 0, '2021-03-30 06:19:07', '2021-03-30 06:19:07', '2022-03-30 11:49:07'),
('e40bf2faca6b4b02f36be72231330e43dead9676503fd027f434ec0ec9d949a29b390e163df68d74', 618, 3, 'Api access token', '[]', 0, '2021-05-08 03:45:48', '2021-05-08 03:45:48', '2022-05-08 09:15:48'),
('e453a9b430cb5e25a27941dde1be43e9b621691d4fc13226b051165b8d30096f993e48ff022b8147', 3, 3, 'Api access token', '[]', 0, '2021-07-22 06:52:37', '2021-07-22 06:52:37', '2022-07-22 12:22:37'),
('e49842214e303f434c32b5e5f085adae2b0f31c4034ae8982a5685d6d8afb2b510093b8f1b07bd6e', 422, 3, 'Api access token', '[]', 0, '2021-03-22 05:50:53', '2021-03-22 05:50:53', '2022-03-22 11:20:53'),
('e4a257181a52272c5129038082a847bdd00d377faa32ff1c921cbeda2713a1e51a4d214e560670fd', 501, 3, 'Api access token', '[]', 0, '2021-03-31 04:03:51', '2021-03-31 04:03:51', '2022-03-31 09:33:51'),
('e4b8e35f0c1b3d12b3736afd74e69e4e08b8d963689d8f0c9f594e020d4515b861ae33620a4b3e93', 540, 3, 'Api access token', '[]', 0, '2021-04-05 04:06:37', '2021-04-05 04:06:37', '2022-04-05 09:36:37'),
('e4d004ebf590d6b7d7db9898d920930fbc7e3ec75c118fc88c6c29b829f8e66534349100506dc13c', 18, 3, 'Api access token', '[]', 0, '2021-07-27 07:06:06', '2021-07-27 07:06:06', '2022-07-27 12:36:06'),
('e4dd16488f58848a593b9338e1f114674161670a2c9d0c89244f648b5a4c8884f129e3afcd442e9a', 40, 3, 'Api access token', '[]', 0, '2021-07-29 05:02:55', '2021-07-29 05:02:55', '2022-07-29 10:32:55'),
('e4dd94d08e9e015a780c0d66400bec1f1f7264f8d2a4ba39f493b7dea087cb7c1b3c143bf812fb4a', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:49:22', '2021-05-04 00:49:22', '2022-05-04 06:19:22'),
('e4edfa24a167ba924397fa5115c82c32759a03cd3bae31db03b343904b4b4c9acab30d62064d9f2e', 777, 3, 'Api access token', '[]', 1, '2021-07-21 00:23:02', '2021-07-21 00:23:02', '2022-07-21 05:53:02'),
('e51fa6af63615c356d6e8466616fda816d8d9fe5d9e50f182e7cd4b4998b1d3741fb8f606ae686ae', 626, 3, 'Api access token', '[]', 0, '2021-05-11 00:24:48', '2021-05-11 00:24:48', '2022-05-11 05:54:48'),
('e52f5f3fe7276c3eee5d8824aed3a8c1625749332120ec64a5e74e7bbf170b9f96afa7fe68672351', 770, 3, 'Api access token', '[]', 0, '2021-07-15 09:46:25', '2021-07-15 09:46:25', '2022-07-15 15:16:25'),
('e5675a20bdfcfc724f6480c94d84c4d1cbd4f2f3146887dffab6173857e8fa810a3462ab61f858c9', 615, 3, 'Api access token', '[]', 0, '2021-05-02 08:17:33', '2021-05-02 08:17:33', '2022-05-02 13:47:33'),
('e5ada34a42e52847a7bcf22a8a54e79e425447b998fc36b2972412a645c0fc044ea0737ff50f677e', 759, 3, 'Api access token', '[]', 1, '2021-07-01 22:07:52', '2021-07-01 22:07:52', '2022-07-02 03:37:52'),
('e5dae6666f320e09a0b1cd2255b345e2fcb2ccefe18801a2bc6baa9bb55116a77b06cb292fd24cf5', 545, 3, 'Api access token', '[]', 0, '2021-04-08 05:04:06', '2021-04-08 05:04:06', '2022-04-08 10:34:06'),
('e5fc9500edc6fd7227a56787eedb94a3eca78e0d71a156e7a270ece46ffda3f6da8c586fc8620aca', 501, 3, 'Api access token', '[]', 0, '2021-03-31 04:03:51', '2021-03-31 04:03:51', '2022-03-31 09:33:51'),
('e6027eb019779bd111b36ce8e4d959e85fcb8e037ed4f10dc462d37ffece44743cec0ca3ae610769', 660, 3, 'Api access token', '[]', 0, '2021-05-25 23:55:40', '2021-05-25 23:55:40', '2022-05-26 05:25:40'),
('e6337315eecc5e0ffc1ef98b4502ae066c1a712bc11f7ce5b44fc42f4a2723867530e22859af9e90', 486, 3, 'Api access token', '[]', 0, '2021-03-26 07:55:47', '2021-03-26 07:55:47', '2022-03-26 13:25:47'),
('e690419f09b03978a8239ce080a6d9cffcefa232c9adef608cad163e50033d8cf441cd02f93d93ff', 620, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:41', '2021-05-04 00:46:41', '2022-05-04 06:16:41'),
('e6c1dba974b4669eb3d944f531e4c960fbf974a9336c04a5e10386009967c8ab17feb622cf661278', 393, 3, 'Api access token', '[]', 0, '2021-03-19 04:03:25', '2021-03-19 04:03:25', '2022-03-19 09:33:25'),
('e6e0a3ba6daf5352cfdeedf8333b1de0f4aaadb3220c715c3d33c5d56709c1da8105de86290cfd17', 453, 3, 'Api access token', '[]', 0, '2021-03-23 07:14:50', '2021-03-23 07:14:50', '2022-03-23 12:44:50'),
('e6ec2e1a80ff323f3e6735b85da2acf910ed2b7572dff380a066bba9ed575a8e26a8a0ed5a778944', 773, 3, 'Api access token', '[]', 0, '2021-07-19 08:33:14', '2021-07-19 08:33:14', '2022-07-19 14:03:14'),
('e70b48b7c9ff2a0a62af4f18fccb4f707c8284e1affaea76beccbce0d96542ab676710413a562eb5', 626, 3, 'Api access token', '[]', 0, '2021-05-27 05:35:08', '2021-05-27 05:35:08', '2022-05-27 11:05:08'),
('e76b68e0c372dbdc46e0b1c762b64338fa60b6742e35e557239f61edc40d990f88e547ea647e32dc', 661, 3, 'Api access token', '[]', 0, '2021-06-15 08:02:35', '2021-06-15 08:02:35', '2022-06-15 13:32:35'),
('e7708b734d24c6dd7995ecc49932079e851e72c4fcf76b85481ff4718545ce4ad902b00df59c07a8', 533, 3, 'Api access token', '[]', 0, '2021-05-13 08:50:43', '2021-05-13 08:50:43', '2022-05-13 14:20:43'),
('e77a42df962ce03a0eeea6d2befac0e9030c5ef0bb7ae9ffac71a129b545fbd51ed98715fb3a3446', 620, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:07', '2021-05-04 01:23:07', '2022-05-04 06:53:07'),
('e7850de0660b1fa9a2d395a2d9a9152e5d827f4a3823fccb81ef1c41d0ed21c8e9d80374f4f37714', 659, 3, 'Api access token', '[]', 1, '2021-05-28 07:52:08', '2021-05-28 07:52:08', '2022-05-28 13:22:08'),
('e796fd1c5b7fc695a6c37c5ec43828de4a2e220b488c9b5860f2fd9cc6749715cade233ba3cb4072', 645, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('e7bc939137460c5d2e48f3e6a242bf908370b437437930c623f8cb33090b93e43e7de7e471f4cf9c', 24, 3, 'Api access token', '[]', 0, '2021-07-29 23:16:53', '2021-07-29 23:16:53', '2022-07-30 04:46:53'),
('e7c2aa7cc544588328a03ce44f7165f7e41b8e1bebb489991c50ce0c1c98e13bafbc4b07c2c9ceed', 620, 3, 'Api access token', '[]', 0, '2021-05-13 06:57:23', '2021-05-13 06:57:23', '2022-05-13 12:27:23'),
('e7d3ca46ba89f9590f5adf79b34ac07d1111097a3b8f1bb7131bd69be54707d88c170ea2b5d0f366', 620, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:14', '2021-06-02 08:17:14', '2022-06-02 13:47:14'),
('e7e319762b7a4ac9c6cb37ab79a1259d36b5d92b3e2a021d1527bbbec83a98cd84cf2747f2bc3821', 432, 3, 'Api access token', '[]', 0, '2021-03-23 22:56:46', '2021-03-23 22:56:46', '2022-03-24 04:26:46'),
('e7ff217dbd4105c430dc9ea18f0147a485ea45fb424714f33e44939e2f282e1de341b6759e07bc27', 557, 3, 'Api access token', '[]', 1, '2021-04-14 01:33:33', '2021-04-14 01:33:33', '2022-04-14 07:03:33'),
('e82222a24c0a37857cb3d49448f2c9577c729c4d3abb0cb08de4532a101377d67407a2181860e6ef', 702, 3, 'Api access token', '[]', 1, '2021-06-16 02:34:33', '2021-06-16 02:34:33', '2022-06-16 08:04:33'),
('e826882fbfb15269af19de8c8d014546f31539dfbd259df0fe0fd7f5551b43863b36ade1e930e185', 773, 3, 'Api access token', '[]', 0, '2021-07-19 08:50:43', '2021-07-19 08:50:43', '2022-07-19 14:20:43'),
('e836a77bc3c2780c12b0b238d2ab6dff583109039eba20ae297d3535eb915a6b26ff3e1ae21ad30e', 519, 3, 'Api access token', '[]', 1, '2021-04-02 21:10:08', '2021-04-02 21:10:08', '2022-04-03 02:40:08'),
('e839f0e7171213adb8cc7784f1cc1cb6850a2451d79ead861e9f8f9bf7d9af0b08810ab089603ff6', 1, 3, 'Api access token', '[]', 0, '2021-08-03 07:30:05', '2021-08-03 07:30:05', '2022-08-03 13:00:05'),
('e83f8bd810a32cc7bf83770c0421b40cefaf266e37933a3b0ceae1b85c17295f3c83d96f4a0fab90', 737, 3, 'Api access token', '[]', 0, '2021-06-15 01:23:37', '2021-06-15 01:23:37', '2022-06-15 06:53:37'),
('e8580c784e9fc78c35881bd329b6d6691db500c6ab09d88f2d4933f624aae14e397b57abe8dad0fa', 531, 3, 'Api access token', '[]', 1, '2021-04-08 07:13:13', '2021-04-08 07:13:13', '2022-04-08 12:43:13'),
('e8612c3f38f014714a6c7e2b8d6f8ee556b6fade3fe5b73c141f91c41c9b8f099a45d119b1bcdf6e', 393, 3, 'Api access token', '[]', 0, '2021-03-18 02:53:10', '2021-03-18 02:53:10', '2022-03-18 08:23:10'),
('e86974b201729990f76f2080f0dcc1423ea81dd752eb6fce25467f47655efcc93e0edbaca94b7d53', 531, 3, 'Api access token', '[]', 0, '2021-05-12 05:17:55', '2021-05-12 05:17:55', '2022-05-12 10:47:55'),
('e89bafc5a015f51bd4a6bf44a1cea215e03c52a894e15342331a12b95e5f23a7a503cdfbb36fbdd5', 7, 3, 'Api access token', '[]', 1, '2021-07-29 02:24:27', '2021-07-29 02:24:27', '2022-07-29 07:54:27'),
('e8f7b3be3e5b07d58c9ecb8a4f66637093c8e7cb59ca4a26033078ef3d757274f2a066b1c8d3cec5', 666, 3, 'Api access token', '[]', 0, '2021-06-02 20:09:27', '2021-06-02 20:09:27', '2022-06-03 01:39:27'),
('e913700b9f6d23a23bf6520adde9395606842335bc55f4cae9f866138fc823259ea6403269d20733', 407, 3, 'Api access token', '[]', 1, '2021-03-18 02:36:33', '2021-03-18 02:36:33', '2022-03-18 08:06:33'),
('e91661baab87f57e46789a300f4fcfcc5409617f00912629eedcda57df851e89cbbaf9bbd6e14d04', 429, 3, 'Api access token', '[]', 0, '2021-03-22 06:04:19', '2021-03-22 06:04:19', '2022-03-22 11:34:19'),
('e92da6d07ea4b99aaa9cc729bd10f4fdae269ee03c32a21e73f64e3ccd6e91bb0a786b9ab85901b1', 773, 3, 'Api access token', '[]', 0, '2021-07-21 05:21:33', '2021-07-21 05:21:33', '2022-07-21 10:51:33'),
('e92fa2014bbcf7ec77dca18037edd885711f8cf11bd75a97c2335593ee4d927c1d85633d769467b6', 3, 3, 'Api access token', '[]', 1, '2021-07-29 01:31:55', '2021-07-29 01:31:55', '2022-07-29 07:01:55'),
('e94823deb57f5835ae4084e1bde8a3684d8420814a9454cf1e3be11d1d5104cc93e899be7c7ad105', 29, 3, 'Api access token', '[]', 0, '2021-07-28 07:15:33', '2021-07-28 07:15:33', '2022-07-28 12:45:33'),
('e963c21d8fc8b8e72d976b7cfee398be9e5a5949ba55bfc228c074883e33b8f54ff6fbd4294e6295', 402, 3, 'Api access token', '[]', 1, '2021-03-17 23:33:24', '2021-03-17 23:33:24', '2022-03-18 05:03:24'),
('e963c493d0271f4c57e8320c5f1df3bd5a8524d717d2d00ec1f76c035a162cac626c72a73b678b02', 634, 3, 'Api access token', '[]', 1, '2021-05-12 01:32:04', '2021-05-12 01:32:04', '2022-05-12 07:02:04'),
('e96847709f00365d050b3bc0164b0366931a1411b29ce90892d3c30a6b0e47b62f323cb8f2b57868', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:10:00', '2021-07-20 02:10:00', '2022-07-20 07:40:00'),
('e970dd9bbc2fb464366754640127f3c91e372fdbf9c199406a58c5691fff36836e94743ccdbdf65c', 579, 3, 'Api access token', '[]', 0, '2021-04-20 03:28:44', '2021-04-20 03:28:44', '2022-04-20 08:58:44'),
('e987ffaf9a673c69c56c23075183d3074b9a56cc8b7eba296021c023de8b84fc25970be5f9749167', 620, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:32', '2021-05-07 07:04:32', '2022-05-07 12:34:32'),
('e995afe2b683476ed6c45412ea5f68a61963a571a0f02fd7f3aa14016d79bd7e075020a07934888d', 445, 3, 'Api access token', '[]', 0, '2021-03-23 02:54:29', '2021-03-23 02:54:29', '2022-03-23 08:24:29'),
('e998568b1de693718538e255b215c7da654848534dc2b493d7b19fd975b6d49556f8014e4e863397', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:34:01', '2021-06-10 02:34:01', '2022-06-10 08:04:01'),
('e99f7f7cb41b7788a2629dfcea54b28a766c9ec22468a8866364734740639ccfcd81bd2531cf9da6', 665, 3, 'Api access token', '[]', 1, '2021-06-01 02:01:52', '2021-06-01 02:01:52', '2022-06-01 07:31:52'),
('e9af2d2f63cebdfd8b4411d777cdd92a240dae481f97cf3b2e53e219e1d9a35202fd6d28bd075ffb', 504, 3, 'Api access token', '[]', 0, '2021-04-01 08:24:13', '2021-04-01 08:24:13', '2022-04-01 13:54:13'),
('e9c1fec6d7ee686bf829e5a0d39c4ba35872e23fb8e21aec18e15f838a85441b4e676017eb420ab5', 52, 3, 'Api access token', '[]', 0, '2021-08-06 05:12:52', '2021-08-06 05:12:52', '2022-08-06 10:42:52'),
('e9c346011989f40c6b5f0fe30439697aa30d1d4e97bb64ebf407ea42331e77b016364ce6958fa10e', 452, 3, 'Api access token', '[]', 0, '2021-03-23 07:00:08', '2021-03-23 07:00:08', '2022-03-23 12:30:08'),
('e9cb4bdd92da5e2b512c0810e9843e009a633e8f519323215bb90b04d9b3021b99ee4aa9b9523f88', 558, 3, 'Api access token', '[]', 0, '2021-04-14 01:37:20', '2021-04-14 01:37:20', '2022-04-14 07:07:20'),
('e9d91755d83a41333c10861bbc3234d6a0eaceea5d2e4d71a9038507a06d136de139f3908c22fee8', 651, 3, 'Api access token', '[]', 0, '2021-05-15 04:11:21', '2021-05-15 04:11:21', '2022-05-15 09:41:21'),
('e9df6586071a75decdb21ef647bc2a3745d93c1c19a3c912b8e83746449ac44d999b301a2843b2d8', 542, 3, 'Api access token', '[]', 1, '2021-06-17 01:40:10', '2021-06-17 01:40:10', '2022-06-17 07:10:10'),
('e9e80ef4368c3dbac095cffafd6e0f15980f7c960e65b46cd832a1a474334d3b5bc9e8976b9f63f1', 54, 3, 'Api access token', '[]', 0, '2021-08-06 23:32:06', '2021-08-06 23:32:06', '2022-08-07 05:02:06'),
('ea12eeb6383dbceace6ae0c1d95af8f1d91108d439dd8987745e64ea0fdcc45a19567ff7e753da09', 475, 3, 'Api access token', '[]', 1, '2021-03-26 02:26:59', '2021-03-26 02:26:59', '2022-03-26 07:56:59'),
('ea46478bab9f9c93e1b20dc0e7ceb69bcdc379ca0a82aa118f84ccb9a0380687a57c17c0edeb1169', 393, 3, 'Api access token', '[]', 1, '2021-03-17 08:04:32', '2021-03-17 08:04:32', '2022-03-17 13:34:32'),
('ea4784da2f24454791ff6dd4d70d6aa476a2e1178f92e1d130a33bce071b2b4ed54424ea1e6d6229', 665, 3, 'Api access token', '[]', 1, '2021-07-13 04:25:41', '2021-07-13 04:25:41', '2022-07-13 09:55:41'),
('ea482dca67c0375f106e00f30fb0cd3fdb2edc1c7a6d8ac9f397de776e3dbb63fd0e10fe4d549b10', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:33:18', '2021-05-08 02:33:18', '2022-05-08 08:03:18'),
('ea93e779d07b76d46a8207bd285ec12b146f6e8ba09954a1a56860a504badb0b6835f0b437e4699c', 622, 3, 'Api access token', '[]', 0, '2021-05-04 01:11:53', '2021-05-04 01:11:53', '2022-05-04 06:41:53'),
('eafba91cb2ae8d8663f386d661c61ca4d8173a5c60eff1e8f16f2e354972b481c0a1f2e418997213', 5, 3, 'Api access token', '[]', 0, '2021-08-07 04:38:23', '2021-08-07 04:38:23', '2022-08-07 10:08:23'),
('eb723719043a0f56288e624a26eb7430990f3b00cf4f1f246967550b7f8206735111a0d47afe0fb2', 638, 3, 'Api access token', '[]', 1, '2021-05-12 09:53:56', '2021-05-12 09:53:56', '2022-05-12 15:23:56'),
('eb7bbdf07e0138a35e5e59eb010f64d88bc3c11737d7d574cf51acb6b11a8eb11fe5452e22142893', 620, 3, 'Api access token', '[]', 0, '2021-05-13 02:29:00', '2021-05-13 02:29:00', '2022-05-13 07:59:00'),
('ebb30da58a4b2da344d1a642387441c4030c76d388e70d9154911be7bb2fd71e25bc6565689e74e0', 780, 3, 'Api access token', '[]', 0, '2021-07-21 01:33:17', '2021-07-21 01:33:17', '2022-07-21 07:03:17'),
('ebd55d73b10e8d840b443fa881f6495713801b8fc43f212f08e537b7356c7d85e85656fc11c786d2', 520, 3, 'Api access token', '[]', 1, '2021-04-02 21:11:04', '2021-04-02 21:11:04', '2022-04-03 02:41:04'),
('ec0dbc2aab8cba4084c7f585f3b535117b424c4de44db294d339d771cc4d2f6d1ba1718c25b9e2af', 619, 3, 'Api access token', '[]', 0, '2021-05-02 08:34:23', '2021-05-02 08:34:23', '2022-05-02 14:04:23'),
('ec143e046b1b79a66bdcdedc7e62ded6d97279e1f136ec2a1b635b2fe6a77b4bda320e1dcc7e6d49', 469, 3, 'Api access token', '[]', 0, '2021-03-25 04:48:06', '2021-03-25 04:48:06', '2022-03-25 10:18:06'),
('ec23cecac0492b5cbb80d682b6ed74534cda034d9d2b4fe8376d7a88db223cf5a043deec7e353e7b', 23, 3, 'Api access token', '[]', 0, '2021-08-04 05:41:36', '2021-08-04 05:41:36', '2022-08-04 11:11:36'),
('ec383eb60f76fdc4a88bbd31597b73791bd8225e451f4725971a15c01153289af7cfe5cf04770f0e', 555, 3, 'Api access token', '[]', 0, '2021-04-14 00:49:49', '2021-04-14 00:49:49', '2022-04-14 06:19:49'),
('ec3f5fbd4095ac3d1e5c3f7fa9fb7dd7d2ae0f9f502c90e7ae6510e741d564ac16ab282943a1de39', 434, 3, 'Api access token', '[]', 1, '2021-03-26 02:39:46', '2021-03-26 02:39:46', '2022-03-26 08:09:46'),
('ec4585c0b8c8a3b277659f2e9c7c41d9e633bf73d31a74784370d4d2ec5f2bf215f644e2b8593c6a', 589, 3, 'Api access token', '[]', 0, '2021-05-02 07:14:23', '2021-05-02 07:14:23', '2022-05-02 12:44:23'),
('ec5f2029d1859bf15f3533dc1ef907975f4fdc0b2288961348558c34449a6769603c50863056d28b', 12, 3, 'Api access token', '[]', 1, '2021-07-27 05:19:44', '2021-07-27 05:19:44', '2022-07-27 10:49:44'),
('ec60ddbb7e6f32ee8e82819776b66a82fbaa73b01fcfce93f3c2ac0467528a146c11b24ffdbddf49', 393, 3, 'Api access token', '[]', 0, '2021-03-21 23:15:39', '2021-03-21 23:15:39', '2022-03-22 04:45:39'),
('ec80590444fda1e1508f7c84876b482bd6c8a64e5bca2406a9e7f04c20433a44ccd9cc644c5787af', 624, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:21', '2021-05-15 06:22:21', '2022-05-15 11:52:21'),
('ec809bf5ac6c47ff845521f5699f4a0b235305aaa2bd330bd370e5f2da25b80327ffb57eef54fab4', 466, 3, 'Api access token', '[]', 1, '2021-03-25 02:32:40', '2021-03-25 02:32:40', '2022-03-25 08:02:40'),
('ecd32922d7d27ad5e298ec00d1dfa81b59a854fbf44a9f24b6bf888a5dfd69005bea7029da9d8405', 516, 3, 'Api access token', '[]', 0, '2021-04-02 07:09:06', '2021-04-02 07:09:06', '2022-04-02 12:39:06'),
('ecdc46b9e14606ca144556718a5c2d9d2f8653ae3709612ddd80f6471a06aa6bf7fd987425dc62d2', 41, 3, 'Api access token', '[]', 0, '2021-07-29 07:57:54', '2021-07-29 07:57:54', '2022-07-29 13:27:54'),
('ed029a4e28cd3163c6c646b44c3e34ffe8b72a4ba180aa1b4c5d59aa9ac82fb82e8a4c2c593e0bd5', 433, 3, 'Api access token', '[]', 1, '2021-03-23 01:03:33', '2021-03-23 01:03:33', '2022-03-23 06:33:33'),
('ed40e2bc547c5044ec94529792f4504217bdbe590af4c0b3b5bcf3cba8ee1bc89392a3ddfc1d3ce5', 577, 3, 'Api access token', '[]', 0, '2021-04-20 02:09:33', '2021-04-20 02:09:33', '2022-04-20 07:39:33'),
('ed6ecdea9d815a4015db6b836b0dbefbde59ffa8dd2d8de0083b0922770bcc3d1c168c5804f6a674', 2, 3, 'Api access token', '[]', 1, '2021-08-03 01:45:02', '2021-08-03 01:45:02', '2022-08-03 07:15:02'),
('ed746369a74abf94a5dd9569b076ea12769caa53fd8b43c0bc441af37fe94c83b6d88f94823b5dc8', 750, 3, 'Api access token', '[]', 0, '2021-07-22 00:56:04', '2021-07-22 00:56:04', '2022-07-22 06:26:04'),
('ed90a23301664adf95057843c9a771022b1e70d4b8e8ad1af955d9dcef4c745cd719eb3770fea3a7', 534, 3, 'Api access token', '[]', 0, '2021-04-05 08:58:34', '2021-04-05 08:58:34', '2022-04-05 14:28:34'),
('edf81c01547169719870b487065c2b5d28c1264596c0170f270706bf432576716fcbb9861e808a6a', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:45:56', '2021-05-08 02:45:56', '2022-05-08 08:15:56'),
('ee1a3cbe11678f4211b096d666a60ae093d39e8e76238edc9d5cfb93215e8b3c1710c175ff390d82', 438, 3, 'Api access token', '[]', 1, '2021-03-23 02:30:25', '2021-03-23 02:30:25', '2022-03-23 08:00:25'),
('ee27db09e8218daf5258e41e4123b36d2aecfb9908b10fa4c3df6ff8b00a7b43d9e6b3d5c465b9b1', 612, 3, 'Api access token', '[]', 0, '2021-05-07 07:04:28', '2021-05-07 07:04:28', '2022-05-07 12:34:28'),
('ee42f62515dd953135fd4d8dcc5e66f95badf083b6e69940ec4ef3c5bfbf77b1c15c97901033fe0a', 5, 3, 'Api access token', '[]', 1, '2021-08-07 04:12:35', '2021-08-07 04:12:35', '2022-08-07 09:42:35'),
('ee7b8ed3fb3a031d828fe602ee7dcc944442acb2c9033d400fa2aed992e1d0e2aebb9a4eeaddcfcb', 577, 3, 'Api access token', '[]', 1, '2021-04-20 02:09:33', '2021-04-20 02:09:33', '2022-04-20 07:39:33'),
('ee9fca45c5b944d2c7e78f27301c0c37a8a776aec6f179a25634471ea410b3b2a9074f72cf6c0a24', 567, 3, 'Api access token', '[]', 0, '2021-04-15 23:33:53', '2021-04-15 23:33:53', '2022-04-16 05:03:53'),
('eec00ffeb3d96f98eb03ba245c708e36e875f4b29ddd1316b1f3f200756a56e4044fd23bad77af22', 23, 3, 'Api access token', '[]', 0, '2021-08-06 01:06:59', '2021-08-06 01:06:59', '2022-08-06 06:36:59'),
('eed90fb47366b3cabc6428a662274eaf583c24d25fd210b376407ade99a11c0fe4df59990ee2a872', 770, 3, 'Api access token', '[]', 0, '2021-07-15 07:17:10', '2021-07-15 07:17:10', '2022-07-15 12:47:10'),
('ef033c678342b9f52635cd1a9e9fec386be67cb6e274070c386d1c01ce58425def3c17f5595812a2', 665, 3, 'Api access token', '[]', 0, '2021-06-14 04:14:46', '2021-06-14 04:14:46', '2022-06-14 09:44:46'),
('ef3e133f4763e7b40a93e3eb011ce36bea3c7073717d521a86de7d2b84dc26d7b06fd30bfab55fa8', 630, 3, 'Api access token', '[]', 0, '2021-05-08 00:41:23', '2021-05-08 00:41:23', '2022-05-08 06:11:23'),
('ef4c1895d62094abd0ed3a9c3a67e6ab2eb3576d4ee36d650f764ed397df677f68eb505eb77fd26f', 27, 3, 'Api access token', '[]', 0, '2021-07-28 07:03:23', '2021-07-28 07:03:23', '2022-07-28 12:33:23'),
('ef5b24673a54577d84cd9b4546b964f678fa245153e2cf6ec3f99135d1e807ffb07aea8482710fab', 622, 3, 'Api access token', '[]', 0, '2021-05-04 01:23:06', '2021-05-04 01:23:06', '2022-05-04 06:53:06'),
('ef67186bd7e81bec140f858f8bbdd69ca65905c4d3d495d04a304818e5850cd2578e7d6be8e8b101', 7, 3, 'Api access token', '[]', 1, '2021-08-03 01:35:46', '2021-08-03 01:35:46', '2022-08-03 07:05:46'),
('ef993d2ff1357256ffb1561549dd5de5e9ec86338fd2ed077f781f8faa2233e8e1080a1d69ef9f59', 695, 3, 'Api access token', '[]', 0, '2021-06-03 00:34:30', '2021-06-03 00:34:30', '2022-06-03 06:04:30'),
('efa8090e02420e623c3a4bad437b84afa6f553b72ad21d19769096e78cf6db6a4516a14924c9ffef', 773, 3, 'Api access token', '[]', 0, '2021-07-20 02:41:27', '2021-07-20 02:41:27', '2022-07-20 08:11:27'),
('efaaa82a0898cf68425c6091bc40f153403c6c47a9f50235c3cd22a1bc7cc0fda8dd46f802d89405', 18, 3, 'Api access token', '[]', 0, '2021-07-27 07:06:06', '2021-07-27 07:06:06', '2022-07-27 12:36:06'),
('efba3515c118d55655def048e8295b44b5bfdfbc14570ba6c48a19ea68957c52e28760f35da5cca6', 1, 3, 'Api access token', '[]', 1, '2021-08-18 05:04:15', '2021-08-18 05:04:15', '2022-08-18 10:34:15'),
('efcbc899c2c44217983db2375d4e17a6b5506e94e3cd0bb4d537111d7860d88e52a530c5207aea83', 612, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:04', '2021-05-04 00:52:04', '2022-05-04 06:22:04'),
('efdda2bfb9346227474726e88064860ec533aa5688f57851b8e13b247b86e76b44d8675baa5d0b09', 526, 3, 'Api access token', '[]', 0, '2021-07-02 04:24:27', '2021-07-02 04:24:27', '2022-07-02 09:54:27'),
('efecb6bd455ec709ff35a721896ea4cd6de0cea518d6cf8844a8f26795722e35fe8c8ff3f6bce6f3', 783, 3, 'Api access token', '[]', 1, '2021-07-21 05:36:20', '2021-07-21 05:36:20', '2022-07-21 11:06:20'),
('eff7e76b000fa084d22af455ad8c89f557c1bfb17a81d01168af080156c2cfe6d3af16f3e1182ae5', 531, 3, 'Api access token', '[]', 0, '2021-04-13 05:11:46', '2021-04-13 05:11:46', '2022-04-13 10:41:46'),
('f050c5fbdf37001d5053db4291aee79f0175e397b73ff1c434f8527d53c99fb83f514bfb23ad3002', 610, 3, 'Api access token', '[]', 0, '2021-05-02 08:06:18', '2021-05-02 08:06:18', '2022-05-02 13:36:18'),
('f077a51eaa370e05572bae3c1e4289a5c107ee9a99cbc669f0bee8dda71d549725a1db7ea5e7feb1', 23, 3, 'Api access token', '[]', 1, '2021-07-27 23:41:37', '2021-07-27 23:41:37', '2022-07-28 05:11:37'),
('f0b460ef7c5f05e9437641a51427e5e6235e50bce225083f1be9208ca847e76f06fd97abfb697ff5', 52, 3, 'Api access token', '[]', 0, '2021-08-05 02:52:27', '2021-08-05 02:52:27', '2022-08-05 08:22:27'),
('f0b64e8efc95414605b2a3c195e097c99428e3fb55e4d89288ea87f91acc1b59c1f641678fc4291b', 626, 3, 'Api access token', '[]', 0, '2021-05-11 06:14:59', '2021-05-11 06:14:59', '2022-05-11 11:44:59'),
('f0e004619e862a2d5b8d38f37fb1b1e703e4fcda996e56ac190df3173bd9ceef0220ccc8327370eb', 13, 3, 'Api access token', '[]', 0, '2021-07-29 04:23:59', '2021-07-29 04:23:59', '2022-07-29 09:53:59'),
('f0e29fe1ffd7dd27e43daa4eccafc061d25c26b4c4d938128e226cfdbb7d95631d04e666a3eb82ed', 715, 3, 'Api access token', '[]', 0, '2021-06-08 09:44:13', '2021-06-08 09:44:13', '2022-06-08 15:14:13'),
('f0e7fd14bb7c2cc430572299908958498f3b7aaafb42578742df067cd84014fcd630d64028a1f464', 28, 3, 'Api access token', '[]', 0, '2021-07-28 07:03:24', '2021-07-28 07:03:24', '2022-07-28 12:33:24'),
('f110a5a3e65637a08c0bf7bd68ab840d7410242913fb0804e542ef89fb7d7c7715eedc1abf2857c9', 674, 3, 'Api access token', '[]', 0, '2021-06-02 07:06:01', '2021-06-02 07:06:01', '2022-06-02 12:36:01'),
('f1218397fc15498893a15b47c10858c861a11f97ea30d0cbcd6f5e7db1a3d987eea22408d0b686b2', 5, 3, 'Api access token', '[]', 1, '2021-08-07 01:42:57', '2021-08-07 01:42:57', '2022-08-07 07:12:57'),
('f12a3fd18ac5b629f9bd0fda7da5de2a6c7710be4e01e4b6ed4cc2cc451ff1bfa0e39afdfe811ad8', 466, 3, 'Api access token', '[]', 0, '2021-03-25 04:11:29', '2021-03-25 04:11:29', '2022-03-25 09:41:29'),
('f14d9cf3050cb90d59da31065beb3cccdd87fc9c835540a8fc523c3d49348c59d8bb1562fd04fda7', 773, 3, 'Api access token', '[]', 0, '2021-07-17 00:38:04', '2021-07-17 00:38:04', '2022-07-17 06:08:04'),
('f15c2ac20929690eb317b93966608dcf7baddadc7374b5011756ef91404fe4a672b1103f07f4b95a', 552, 3, 'Api access token', '[]', 1, '2021-04-15 05:43:24', '2021-04-15 05:43:24', '2022-04-15 11:13:24'),
('f15c309e6ee2cc8407c67ed31da231d50048b5711deeeeb30d83bf8b45fecafde29080434894f960', 668, 3, 'Api access token', '[]', 0, '2021-06-01 08:23:32', '2021-06-01 08:23:32', '2022-06-01 13:53:32'),
('f1dd2b8d14c2b597a050802a1e7adfbbe1b96262b5a809290de51ea9732900948b59cead16ae09a2', 43, 3, 'Api access token', '[]', 0, '2021-08-02 00:12:45', '2021-08-02 00:12:45', '2022-08-02 05:42:45'),
('f1e0f379e8eca3be9643d88db18c9d497657e9968ea2653a3fb6bdf1861bef57620a227a16832b20', 639, 3, 'Api access token', '[]', 0, '2021-05-13 08:45:51', '2021-05-13 08:45:51', '2022-05-13 14:15:51'),
('f1f611fe7a3c1e30193cc152d87405ebe3b932b6e7919f739f1e9235751f55edd00b90d396dad28d', 638, 3, 'Api access token', '[]', 1, '2021-05-12 05:28:44', '2021-05-12 05:28:44', '2022-05-12 10:58:44'),
('f1ff9a1d6cbc761a815363f2d33fb2ebe75cfc026e8b3cfc7cdb97cae0e344beb30abdb51f7a5367', 538, 3, 'Api access token', '[]', 0, '2021-04-05 02:44:48', '2021-04-05 02:44:48', '2022-04-05 08:14:48'),
('f200814ab056b91707b7b0d8dfc84b80ee4f6f12a98156251b3ea3e5fb3186a11472b15632567485', 417, 3, 'Api access token', '[]', 0, '2021-03-19 02:17:18', '2021-03-19 02:17:18', '2022-03-19 07:47:18'),
('f20f7fac4dd8d9969e47b5cf6ab0818e3e95d408f581e72545337b47cd997db09872e7bd7f323ef8', 618, 3, 'Api access token', '[]', 0, '2021-05-08 03:42:14', '2021-05-08 03:42:14', '2022-05-08 09:12:14'),
('f2268a3028c9d897259f1c2b477b2ca146c33f2266bcfbfb2fd4a9a2eeae39032534765750ac90d9', 629, 3, 'Api access token', '[]', 1, '2021-05-05 05:05:00', '2021-05-05 05:05:00', '2022-05-05 10:35:00'),
('f2932ca92f6eb6bfce66d252f437f2223a5e0fc72acf3889cb31672f1a193f749241f90ab919622f', 566, 3, 'Api access token', '[]', 0, '2021-04-15 07:46:54', '2021-04-15 07:46:54', '2022-04-15 13:16:54'),
('f2aa2b44b814055c8e93a400b410bf9cf8779fc1d1405e95f6e1a7de81b1f444fc978a8944f91504', 393, 3, 'Api access token', '[]', 1, '2021-03-18 04:16:36', '2021-03-18 04:16:36', '2022-03-18 09:46:36'),
('f2b70b37a033947475062785583aa8675912920375809226eabe432126d3cd617dda0acd939aaa1f', 455, 3, 'Api access token', '[]', 0, '2021-03-23 22:50:54', '2021-03-23 22:50:54', '2022-03-24 04:20:54'),
('f2f4afe89770a0d7b4aee321c62a28c9032f5b292c0c7ab90307894d9d3283695d89eb79952516f3', 465, 3, 'Api access token', '[]', 0, '2021-03-24 07:58:55', '2021-03-24 07:58:55', '2022-03-24 13:28:55'),
('f30277992416069302d9cabc71d46f6b9dc3f957f4b352c770ecb162c62cd2b5ee39c3785ab5b9f4', 539, 3, 'Api access token', '[]', 0, '2021-04-05 02:59:30', '2021-04-05 02:59:30', '2022-04-05 08:29:30'),
('f31f15bf5b16e27087f2fb5a2ebe4184516f944ec2b5d33f8590a672178f523c3109ddb5052d2048', 408, 3, 'Api access token', '[]', 0, '2021-03-18 04:30:17', '2021-03-18 04:30:17', '2022-03-18 10:00:17'),
('f328bbbb477446f4cb4357d58be2653fdcb250e899fe7f757e618e65e77af2ca79758cf9ca520029', 579, 3, 'Api access token', '[]', 0, '2021-04-20 03:22:10', '2021-04-20 03:22:10', '2022-04-20 08:52:10'),
('f34a9b7f433d89e966f24fd1cf275c7c659f834d8190b89d4c8f91932f78ded3277cd9093766cc27', 639, 3, 'Api access token', '[]', 0, '2021-05-12 07:08:19', '2021-05-12 07:08:19', '2022-05-12 12:38:19'),
('f34c7c32ab54ef38d4a3339d33d03bc5170c1484e9d3dd681b1cbdb9f577b1e065733ac67fe21bf5', 554, 3, 'Api access token', '[]', 0, '2021-04-15 04:18:38', '2021-04-15 04:18:38', '2022-04-15 09:48:38'),
('f36168de49a339fae4331ccedc95f33a02a22e8eb28aa2c535f0deb61836690d10dbfa7fba5f1f7a', 781, 3, 'Api access token', '[]', 1, '2021-07-21 02:17:55', '2021-07-21 02:17:55', '2022-07-21 07:47:55'),
('f39263db49bf68e1c12e1acf6c6186d96d35e437df1ab058867dddf8aa11158bde44164aaaeb386e', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:43:01', '2021-05-12 04:43:01', '2022-05-12 10:13:01'),
('f3964875939c687b3c0de16e88a3f28f62fc7a2ddc61d80f30d042480fa9e4ad3aee45d2cd3f1f55', 555, 3, 'Api access token', '[]', 0, '2021-04-15 05:09:57', '2021-04-15 05:09:57', '2022-04-15 10:39:57'),
('f3a66dd4468fb217baada320add989aafde6a196421f71bf6b2e49f6f0db91f23346eb95ff912e2d', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:49:28', '2021-05-12 04:49:28', '2022-05-12 10:19:28'),
('f3adfa131a5e23e8fd5251d1cb59a476c60a6ae4e89779a8d57ec3d49a7cdd6e0aac79728141b923', 560, 3, 'Api access token', '[]', 0, '2021-04-15 01:12:13', '2021-04-15 01:12:13', '2022-04-15 06:42:13'),
('f3bf30f5c76e449afaf6f3576600db2779706e9b17dff0a08f370b714a6e8c1a75b8f696ce12600c', 19, 3, 'Api access token', '[]', 1, '2021-07-30 01:49:08', '2021-07-30 01:49:08', '2022-07-30 07:19:08'),
('f3e11e9d34cdf83dad2a718fd93dfb9df90832cd70c70c7ac886740618d7d00b9a073c3a065ae591', 52, 3, 'Api access token', '[]', 0, '2021-08-06 05:57:22', '2021-08-06 05:57:22', '2022-08-06 11:27:22'),
('f3eaded637eecf87af8afecfe353658013ea4ba7dfd7bbeff2148fff5a4dcc55b7b6c244c28f8ad5', 773, 3, 'Api access token', '[]', 0, '2021-07-19 00:31:04', '2021-07-19 00:31:04', '2022-07-19 06:01:04'),
('f3f980edcc9267a9e6a2ea6fedaaf43758ddf03886ffee0fe46ee14c9dd61b6ce7ea4b497a9ffbab', 635, 3, 'Api access token', '[]', 0, '2021-05-12 04:55:37', '2021-05-12 04:55:37', '2022-05-12 10:25:37'),
('f455032b1e8fb0d513c3f2d64db3d1475014edf7cd85623c921bee626cc432bea47a711a1c85b08a', 700, 3, 'Api access token', '[]', 0, '2021-06-03 01:01:21', '2021-06-03 01:01:21', '2022-06-03 06:31:21'),
('f483b4ef4f7048cb9e49808486bed5dbd205dd161b61432997bae23bb692b1eb11cab66c76c89bb6', 14, 3, 'Api access token', '[]', 1, '2021-07-27 01:59:34', '2021-07-27 01:59:34', '2022-07-27 07:29:34'),
('f489fb4396b6a249e78d6a315ed81282845aa3331a75de7bce60b764ac4e140c07c4c6b23455f4ba', 398, 3, 'Api access token', '[]', 1, '2021-03-18 00:26:28', '2021-03-18 00:26:28', '2022-03-18 05:56:28'),
('f48acf6d3c61da4da4abba4a701f03f9e2910366440ddec51c897eb977ffcbd209efe6842d73ce63', 713, 3, 'Api access token', '[]', 0, '2021-06-08 09:41:21', '2021-06-08 09:41:21', '2022-06-08 15:11:21'),
('f4a402bd824681f9df5473a7b5a432e5a7c3287c5a9ff9b7e50411f6a509e19790763032197c60d3', 618, 3, 'Api access token', '[]', 0, '2021-05-12 04:55:36', '2021-05-12 04:55:36', '2022-05-12 10:25:36'),
('f4c05373c68d14e6239612e6bf6fbd6953ded8f6f5f6b0458e35e37dfc36a64fa0c0a9a4c598d905', 1, 3, 'Api access token', '[]', 1, '2021-08-18 05:16:22', '2021-08-18 05:16:22', '2022-08-18 10:46:22'),
('f4d111d2f53ef36932b03e6df46f819f3a9c93ff3778fc26dccf2d69a13681f3c30957dfe3842ce7', 52, 3, 'Api access token', '[]', 0, '2021-08-06 04:39:04', '2021-08-06 04:39:04', '2022-08-06 10:09:04'),
('f4d6e620e0280a54c43556fc443e5933463e3bf0dc6e2aa04d3a131362b0381b6aa0df8d07e87214', 773, 3, 'Api access token', '[]', 0, '2021-07-16 07:14:36', '2021-07-16 07:14:36', '2022-07-16 12:44:36'),
('f4db96e28dc0c6db97752ffd5992011ff1d51b02e27c6aedabfb13a96ac192bc74aabe978a6d7180', 588, 3, 'Api access token', '[]', 0, '2021-05-02 07:12:54', '2021-05-02 07:12:54', '2022-05-02 12:42:54'),
('f5372d68707602057f1bb0a861c4d70d3cd70b29cb27bc174ac718aae7fce8dee716898a90fa4c89', 5, 3, 'Api access token', '[]', 0, '2021-07-28 06:04:39', '2021-07-28 06:04:39', '2022-07-28 11:34:39'),
('f550a404c9a7d92354d642d4afdcf581cedb84436581628e4ca50c8a567d5b95da174fc15dbfa654', 764, 3, 'Api access token', '[]', 0, '2021-07-08 07:26:08', '2021-07-08 07:26:08', '2022-07-08 12:56:08'),
('f55c18fed7c2ae7b4323b2d2d15443a355062c3c20847eff838aeefeadf094dddbb59878b55148a8', 618, 3, 'Api access token', '[]', 0, '2021-05-08 02:36:48', '2021-05-08 02:36:48', '2022-05-08 08:06:48'),
('f55dbf9e9bffcc723186196ae74ca6297ae1f176e013a5ddee295e1caa20ddb99d627b86c1c65031', 7, 3, 'Api access token', '[]', 0, '2021-08-04 00:42:39', '2021-08-04 00:42:39', '2022-08-04 06:12:39'),
('f58069a164b50334cc060c626e6d2ef34aead9eb0e455334c23d52bc63c643efcc4f168c5dbd259e', 622, 3, 'Api access token', '[]', 0, '2021-05-04 00:52:07', '2021-05-04 00:52:07', '2022-05-04 06:22:07'),
('f5866e8bd073eb225fa8c50cd061e123615936f1a4862394ba69b99ca114f5d3eba51a39980d025d', 612, 3, 'Api access token', '[]', 0, '2021-05-08 05:58:03', '2021-05-08 05:58:03', '2022-05-08 11:28:03'),
('f5bc9e1723aa5f75ca4d42d771e0c2f205b478000058ebf7a8e04d7a9bef665f6af275eea8640eda', 583, 3, 'Api access token', '[]', 0, '2021-04-21 06:42:16', '2021-04-21 06:42:16', '2022-04-21 12:12:16'),
('f5c0e79c30c1b4fe4f33031fbe51e09e36d80a0817bba439d5b3581e18f7aee47e9ff77d62c89b0b', 693, 3, 'Api access token', '[]', 0, '2021-06-03 00:29:42', '2021-06-03 00:29:42', '2022-06-03 05:59:42'),
('f5c5ccd6085e71650b5af86d3eb9c9f478b75413d88ad68e7c996dd9531e66fdd30e846adac962af', 66, 3, 'Api access token', '[]', 0, '2021-08-17 05:58:13', '2021-08-17 05:58:13', '2022-08-17 11:28:13'),
('f5f873c7c244e6a10c8e4cb835aa3f4790312e043c75ff7ee303487d27d494d3124954fe1e62987c', 400, 3, 'Api access token', '[]', 0, '2021-03-22 05:22:40', '2021-03-22 05:22:40', '2022-03-22 10:52:40'),
('f5fda943059d990bec1f21bb3dce83ad795aaa1fdb522fd9ddd3c6eb57f20ae65813ba80678aa77f', 431, 3, 'Api access token', '[]', 1, '2021-03-23 07:15:17', '2021-03-23 07:15:17', '2022-03-23 12:45:17'),
('f61f372e16458db55e8492b613e100ea9167697cd66c8d63a94dd8bff788b02324ad4a3a72ef0940', 34, 3, 'Api access token', '[]', 0, '2021-07-28 07:27:33', '2021-07-28 07:27:33', '2022-07-28 12:57:33'),
('f6493bc24315678d1883179a61f2e04a4b1d1106f817bb0021b78e7df26975e277de46ff273233f4', 756, 3, 'Api access token', '[]', 0, '2021-06-30 08:53:41', '2021-06-30 08:53:41', '2022-06-30 14:23:41'),
('f650607254f7ea191c9f5c9779ad37b7a28b685f414fcdadccc3b6b293530992ac50b3fe003cfde8', 668, 3, 'Api access token', '[]', 0, '2021-06-14 10:04:40', '2021-06-14 10:04:40', '2022-06-14 15:34:40'),
('f661a4a423e823f23688542d8ad0d7fc1ca99851ff8314bfb41aff83177f56fa5905f9e82c08df14', 477, 3, 'Api access token', '[]', 1, '2021-03-26 03:56:11', '2021-03-26 03:56:11', '2022-03-26 09:26:11'),
('f67aaf66c4aa237bf3c2bca00c79d525590cedb7b819337522d6ef453c5569468270c1ecaa7d0c3f', 542, 3, 'Api access token', '[]', 0, '2021-04-05 05:15:43', '2021-04-05 05:15:43', '2022-04-05 10:45:43'),
('f67b3a0d2ef3645451b8e4c13051029122984092ca0b48cef2d7d813f55e024c1036ab3b46119ff5', 42, 3, 'Api access token', '[]', 0, '2021-07-31 01:04:04', '2021-07-31 01:04:04', '2022-07-31 06:34:04'),
('f6897fccbfa5c33c57464439579ece1cf8fe53b153cf6ab9221a67c73565e33d55e3a66eb091e044', 61, 3, 'Api access token', '[]', 1, '2021-08-13 01:52:42', '2021-08-13 01:52:42', '2022-08-13 07:22:42'),
('f6a350668625c4838b3c31c5ea1c4a28c973cb44361cceee26f6867a808d200b5be2ba357ab0f92e', 397, 1, 'Api access token', '[]', 1, '2021-03-17 06:20:38', '2021-03-17 06:20:38', '2022-03-17 11:50:38'),
('f6a79233f79809af88908e6c8dd1ddc566c70611ae69624a59dc89def3f6bd3306d7710efa134418', 622, 3, 'Api access token', '[]', 0, '2021-05-04 00:46:40', '2021-05-04 00:46:40', '2022-05-04 06:16:40'),
('f6db5baeb5910ebebe5e3c3972b754fd527c71ebb8d6fee5aa32362cab16f86b564239b84acf84f4', 524, 3, 'Api access token', '[]', 1, '2021-04-03 07:18:29', '2021-04-03 07:18:29', '2022-04-03 12:48:29'),
('f6e07fa3ad34598fc8f7976feebdc0f6ea6dc5a3c672484763f0a18660bfe45df745878c1e859a1b', 702, 3, 'Api access token', '[]', 1, '2021-06-15 07:49:53', '2021-06-15 07:49:53', '2022-06-15 13:19:53'),
('f6fe34b47083ad1a4b35daa98facaa2645cd45d47a330091b6e2c7704f3259d735bf2a9c3ec83c0c', 2, 3, 'Api access token', '[]', 1, '2021-07-30 02:23:07', '2021-07-30 02:23:07', '2022-07-30 07:53:07'),
('f738cc3473ef939a63a3a5d9cf7f5d4004c34aa4b565a843ef13179f978afb6214eed640e8151418', 465, 3, 'Api access token', '[]', 1, '2021-03-24 07:58:55', '2021-03-24 07:58:55', '2022-03-24 13:28:55'),
('f74e20e094b2fd9e5477dc9ca2cde1fd9d9bf39b76145cdc1b894c6effa482ca7f5a2028e353f6a6', 14, 3, 'Api access token', '[]', 0, '2021-07-27 07:37:19', '2021-07-27 07:37:19', '2022-07-27 13:07:19'),
('f7550215c34c383fc3f8457a47d4abb450e91e037f0040d23017dea5d8c747b6fbc00f33373c40cd', 464, 3, 'Api access token', '[]', 0, '2021-03-24 06:36:58', '2021-03-24 06:36:58', '2022-03-24 12:06:58'),
('f76ee288fec0b3399d6eebfd57f5833fc826247f5ac8f5bbef258abb117cfdddbf3d66e7afe4010a', 618, 3, 'Api access token', '[]', 0, '2021-05-04 05:10:34', '2021-05-04 05:10:34', '2022-05-04 10:40:34'),
('f7918782c15ae7541e463f4c5986cda530cd507ae6659479e403a48190e4e07b5f25f534af4a9df0', 768, 3, 'Api access token', '[]', 0, '2021-07-14 06:43:20', '2021-07-14 06:43:20', '2022-07-14 12:13:20'),
('f7f741c6ba0cc26f92d7a7ce8434003764141fc60713c43d7a87cac22aec251cb57f99b224756145', 393, 3, 'Api access token', '[]', 0, '2021-03-22 02:24:48', '2021-03-22 02:24:48', '2022-03-22 07:54:48'),
('f8210948f29a2155ffc8cc17b788c0016b25c907d965d77f5206b80d8736da6ef4fd2246cfaedfb7', 432, 3, 'Api access token', '[]', 0, '2021-03-23 22:44:57', '2021-03-23 22:44:57', '2022-03-24 04:14:57'),
('f82724f39935e7e1fba05bb78cf368583cda0e2a9658d0dd316b7d3b873fd77fae055957e21db4bb', 648, 3, 'Api access token', '[]', 0, '2021-05-14 06:08:33', '2021-05-14 06:08:33', '2022-05-14 11:38:33'),
('f82a8e9e7c70ff0167696eb40c47654a9be1afdcec3745f2e55832f6ca2828b9d8ba8d44752fa0ad', 753, 3, 'Api access token', '[]', 1, '2021-07-19 09:19:03', '2021-07-19 09:19:03', '2022-07-19 14:49:03'),
('f831b8a47be1ecc2986013bdd43488431c0940fd6015d93bcfd4a4fd0ee01075c788aacaedaa7e89', 665, 3, 'Api access token', '[]', 0, '2021-06-04 02:59:07', '2021-06-04 02:59:07', '2022-06-04 08:29:07'),
('f8597676710bc6b043f29b9a67039e5653e6b568bfdd7b1421fcb85246f2505acc002843a1c0f991', 406, 3, 'Api access token', '[]', 1, '2021-03-18 02:28:46', '2021-03-18 02:28:46', '2022-03-18 07:58:46'),
('f8628b8185b24378a6b5eec3518d23e47a2ee5b83c52b1115064fd19e19ce6706ebd98464a631535', 19, 3, 'Api access token', '[]', 1, '2021-08-04 00:04:43', '2021-08-04 00:04:43', '2022-08-04 05:34:43'),
('f866983a03760fea78094ff90c3939ba0fef8aba043771c12809ccfa7785ef86575c424ea1c62a5b', 548, 3, 'Api access token', '[]', 0, '2021-04-09 13:30:26', '2021-04-09 13:30:26', '2022-04-09 19:00:26'),
('f8815b00f9956945bdfd6a3b41ce9fe2d90d420be2729c2112951be3a951c413c2ce824f101d617b', 12, 3, 'Api access token', '[]', 1, '2021-07-26 07:22:38', '2021-07-26 07:22:38', '2022-07-26 12:52:38'),
('f8c27800f26f0eea9d2138b4ece5dc877813989379513c2269f44ea5f47cdafe7d706610186f9608', 45, 3, 'Api access token', '[]', 0, '2021-08-03 03:05:50', '2021-08-03 03:05:50', '2022-08-03 08:35:50'),
('f8c7842348c83ddc40dc31315e7a670abd947c08ff4424114614e540c002ccf5324e2ab73436d9f5', 759, 3, 'Api access token', '[]', 0, '2021-07-20 06:42:11', '2021-07-20 06:42:11', '2022-07-20 12:12:11'),
('f8db51da83679c88d6dedc72357d7dba69e07c40f731bfab47c14c2ad4ffe9066a17a0f5cb28ff49', 412, 3, 'Api access token', '[]', 1, '2021-03-18 08:54:51', '2021-03-18 08:54:51', '2022-03-18 14:24:51'),
('f8ea723a0d0a1c89a1b00112acdb3d8c6fcd72575da4740a1a667bfe415adf4223f00eacab47284e', 656, 3, 'Api access token', '[]', 1, '2021-05-26 05:08:07', '2021-05-26 05:08:07', '2022-05-26 10:38:07'),
('f8ef0bfcdc1cceae3a1e86a039fe5a7b6b3b2f215ca9fc27d26366ca4908b3bb30e9dd8f3f05b150', 685, 3, 'Api access token', '[]', 0, '2021-06-02 08:17:15', '2021-06-02 08:17:15', '2022-06-02 13:47:15'),
('f8fd3a4e41c0afbc8a4120cd53b81680ba3eeee8e0e0c9f5160cc5c98447cce7ea149b99a959e5c3', 24, 3, 'Api access token', '[]', 1, '2021-07-28 07:55:20', '2021-07-28 07:55:20', '2022-07-28 13:25:20'),
('f921e3a5212f6194770ffde6ffc24dc402a7edc07f894e7d156ddd03a7493520327bbdef51638075', 566, 3, 'Api access token', '[]', 0, '2021-04-15 07:46:54', '2021-04-15 07:46:54', '2022-04-15 13:16:54'),
('f929fd37fc20aaa93ceaf1a41b9110c9ce8d987b0d08ac45e48f6282e3c7a2d296569c03ec33efb1', 7, 3, 'Api access token', '[]', 1, '2021-08-03 01:54:30', '2021-08-03 01:54:30', '2022-08-03 07:24:30'),
('f94053e771072b2b42377b954a914516daf9824f4c41a0c38b0933fa7f43fb0ff805f9a380603189', 531, 3, 'Api access token', '[]', 0, '2021-04-05 01:20:47', '2021-04-05 01:20:47', '2022-04-05 06:50:47'),
('f955447e10ee8a7e6195525cc2a3eb635c1f8937d073250144404c4751c0c1c6a5f519776a286f2c', 32, 3, 'Api access token', '[]', 0, '2021-07-28 07:26:10', '2021-07-28 07:26:10', '2022-07-28 12:56:10'),
('f967cf6f87646a3adc20d624c237165af7c5efcefc86895413396b7ab59d52a3e7ca1c91f4ee5727', 773, 3, 'Api access token', '[]', 0, '2021-07-19 05:03:16', '2021-07-19 05:03:16', '2022-07-19 10:33:16'),
('f9824ec6aec9eb77932138500f3bcd2c5d12b8242a7e5e0c11b392556b089fdbead0f8e8a1937490', 599, 3, 'Api access token', '[]', 0, '2021-05-02 07:44:59', '2021-05-02 07:44:59', '2022-05-02 13:14:59'),
('f98775f42631644115b88157dc22106069d303633c14df691e1ace597f5672919ff603d1821d7d20', 476, 3, 'Api access token', '[]', 0, '2021-03-26 02:37:31', '2021-03-26 02:37:31', '2022-03-26 08:07:31'),
('f9937669a5fb0018afb5cf9ea18a9c6f4a86eb55485346e70d41748c1e67150ebe0d575c640049df', 17, 3, 'Api access token', '[]', 1, '2021-07-28 07:47:28', '2021-07-28 07:47:28', '2022-07-28 13:17:28'),
('f9d226f4022c538c88326954583101fac29aea139e08febf287ea63cf62e0176f9f52a1f5e3f6eaa', 440, 3, 'Api access token', '[]', 0, '2021-03-23 02:23:17', '2021-03-23 02:23:17', '2022-03-23 07:53:17'),
('f9efce00e3a137fd2dcb666f36e47d06cb507e95b86eae045925b98d2f07205cce0caf7437a99c5f', 644, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('fa05ce86ffdd530dc88d3cfce65805f2cb4a988316e3b50358ee60ae4892f0f61460186db0426f22', 644, 3, 'Api access token', '[]', 0, '2021-05-13 08:41:13', '2021-05-13 08:41:13', '2022-05-13 14:11:13'),
('fa0aa91dacc7b680e9d63eb2357ef0dc1c1eb996af9c4f9751ece56997f23cf589ca807bcdbfa0de', 397, 3, 'Api access token', '[]', 0, '2021-03-22 06:37:09', '2021-03-22 06:37:09', '2022-03-22 12:07:09'),
('fa4115644641d200c24efd61446741b8fdac1d77e648bdb579fd1efdccc9fd26093b5621d6f02442', 486, 3, 'Api access token', '[]', 0, '2021-03-26 06:23:18', '2021-03-26 06:23:18', '2022-03-26 11:53:18'),
('fa4d08a7f0888489878c288aa0a2ef567e7b986c8507a2820a13284df54012f99eca7316ac781cea', 62, 3, 'Api access token', '[]', 0, '2021-08-13 02:19:06', '2021-08-13 02:19:06', '2022-08-13 07:49:06'),
('fa5de4058fe833cfcbe25d4d3e34306d9ae9ab738e2f428d20178e7451f9245266204683b50515bc', 405, 3, 'Api access token', '[]', 0, '2021-03-18 02:01:36', '2021-03-18 02:01:36', '2022-03-18 07:31:36');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('fa8c9775c3513c33acdee268429f45148d50240bbae6c8e9606cb91477454c3f65d93edbab292848', 477, 3, 'Api access token', '[]', 0, '2021-03-26 03:56:11', '2021-03-26 03:56:11', '2022-03-26 09:26:11'),
('fad9317959a4eceb8468f27f61c226c8d01bc96c2dd39dd02e6122b3a6b8d3e1df03eb801a082309', 17, 3, 'Api access token', '[]', 0, '2021-07-28 08:09:55', '2021-07-28 08:09:55', '2022-07-28 13:39:55'),
('fae490b502673ce5d40533084f959d9acf2d582ce689abd55c9d88b90bd0ab236bf4f47f52aeda39', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:26:38', '2021-07-26 07:26:38', '2022-07-26 12:56:38'),
('fb238587ac48bceaf66bfe7709b56b10154b42f8cb4fbc25a61b1e090bcac14363fc8f94aef87b1f', 639, 3, 'Api access token', '[]', 1, '2021-05-12 05:39:18', '2021-05-12 05:39:18', '2022-05-12 11:09:18'),
('fb3b6fc1f569ad8c2e584c87e205d006e62dd245f06579654277dc26853c338f02388eae5bf99614', 6, 3, 'Api access token', '[]', 0, '2021-07-23 08:00:41', '2021-07-23 08:00:41', '2022-07-23 13:30:41'),
('fb7a3758823f8c86c4ec903cb266095d1bded5034a602c43243e147b98f302a30efe5be60e7ee17a', 578, 3, 'Api access token', '[]', 1, '2021-04-20 02:22:30', '2021-04-20 02:22:30', '2022-04-20 07:52:30'),
('fbaf716e3ea0ab621cfc88736e1bab7fe896e8880d00147703b9719eb816223e6e1d29a9562f3e65', 773, 3, 'Api access token', '[]', 0, '2021-07-20 08:15:32', '2021-07-20 08:15:32', '2022-07-20 13:45:32'),
('fbb816a1bcc63fab03a42af6a0d6f88ec5b96b6a49aa5a5ca23b8fd134025a55b8b12b1599c514a7', 611, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:49', '2021-05-02 08:41:49', '2022-05-02 14:11:49'),
('fbd96d63e28616ed46acd70cb1d19a566261f79b7e2bf267184cf7f1afe7eeca75dce5bf4bf61595', 393, 3, 'Api access token', '[]', 0, '2021-03-18 03:46:18', '2021-03-18 03:46:18', '2022-03-18 09:16:18'),
('fbde5a4e40d43a3b34dc3e32e83c214895a73a32f63bcee8e85b7a8642460b35763b3c3373f14eef', 591, 3, 'Api access token', '[]', 0, '2021-05-02 07:31:15', '2021-05-02 07:31:15', '2022-05-02 13:01:15'),
('fbf4b6ffe7033e42db847199ca8ca1360cdfd2e65d845676a0710e3aa857d5f6b4c0c9980e7880e0', 621, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:46', '2021-05-02 08:41:46', '2022-05-02 14:11:46'),
('fc0af0560f85f6a99dab8afddffc159b4ed6f33865696f16951dce7a8b6b0dd9b6db3c56c2671ea9', 533, 3, 'Api access token', '[]', 0, '2021-06-08 08:29:03', '2021-06-08 08:29:03', '2022-06-08 13:59:03'),
('fc17cff619e936a140dd8399068e3a59f6d1c75e7464a28d316d1e8f6a2732985a0ca09704fb95cb', 514, 3, 'Api access token', '[]', 0, '2021-04-02 06:48:34', '2021-04-02 06:48:34', '2022-04-02 12:18:34'),
('fc377ba8e9eca173f5f637ca03f41874a4bf362f585fa15a665c578bb16d1f4fdec08f5ec3c810a4', 23, 3, 'Api access token', '[]', 0, '2021-08-05 07:02:14', '2021-08-05 07:02:14', '2022-08-05 12:32:14'),
('fc402f5a4371f7ae35827ef4da53ed0b31bf182bf52e865acfe09866932cb745331da51229564fff', 3, 3, 'Api access token', '[]', 1, '2021-08-02 01:02:00', '2021-08-02 01:02:00', '2022-08-02 06:32:00'),
('fc726e74ba3aa8692749572da215776116f661aebd05b90e5c25f5547ea2807ece1696d7984c69ed', 533, 3, 'Api access token', '[]', 0, '2021-04-05 01:22:26', '2021-04-05 01:22:26', '2022-04-05 06:52:26'),
('fc86de42c9482ca0c8af5e1f3c3c270b3213197d3447ab223538d27f69c961e7762a3ff2d506c26b', 620, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:25', '2021-05-15 06:22:25', '2022-05-15 11:52:25'),
('fc9b50153c5d4685c29b1f182b78431ac4b451db68809449152e03ee734f746884264cf020a7d673', 626, 3, 'Api access token', '[]', 0, '2021-05-11 07:23:51', '2021-05-11 07:23:51', '2022-05-11 12:53:51'),
('fca21d27937203d75bb13a7864b9dff54461e0ef0f2453cf6edaa73f0b9f6f79008e7e6aea780bf6', 721, 3, 'Api access token', '[]', 0, '2021-06-10 01:41:23', '2021-06-10 01:41:23', '2022-06-10 07:11:23'),
('fcc47388d1732c0e8e6a07645445685275152fdf62fbf6fdbf8fe5b2ccdce481b5ced73dba411e91', 618, 3, 'Api access token', '[]', 0, '2021-05-08 03:24:27', '2021-05-08 03:24:27', '2022-05-08 08:54:27'),
('fcd427c9b978a4da7f7ddc26c03965c664d3c31e90841cc15a19d59290baea00464e78f051e6ba20', 667, 3, 'Api access token', '[]', 1, '2021-06-01 07:40:19', '2021-06-01 07:40:19', '2022-06-01 13:10:19'),
('fcdf0089bfac8d8fd31a5e585d2bca98483de277c71113dea1153304432b93ee6248bf550f878b2e', 759, 3, 'Api access token', '[]', 0, '2021-07-21 05:25:58', '2021-07-21 05:25:58', '2022-07-21 10:55:58'),
('fd09e84bf332ccbd9a42ca4df9c5d621dd0ed8bff5da5d7f61b7fa87c8b3d6b127bb552059c3c5fa', 549, 3, 'Api access token', '[]', 0, '2021-04-21 04:49:33', '2021-04-21 04:49:33', '2022-04-21 10:19:33'),
('fd348567122d880b16f20b922ea8d78abf80af92ffd46eb7a871cb19ebd420259342646b9cb201de', 1, 3, 'Api access token', '[]', 0, '2021-08-13 23:04:29', '2021-08-13 23:04:29', '2022-08-14 04:34:29'),
('fd3d46fb1eba98dacfbb275e6021fe9d55a457eb3d232d202dc9802ed9dc8a6fe80c4c8d539ed558', 659, 3, 'Api access token', '[]', 1, '2021-05-24 05:35:02', '2021-05-24 05:35:02', '2022-05-24 11:05:02'),
('fd3faebbcd6c874a7fbfc0f87749ecf935fbc183df4355c9d033230336eef2d81fa7b7b1fce29473', 655, 3, 'Api access token', '[]', 0, '2021-05-26 04:28:53', '2021-05-26 04:28:53', '2022-05-26 09:58:53'),
('fd4b2d29c0e391eab326f5fdd045d4699630a118f4fa06b27578b8ca4af8d0916d82decdc90b3617', 611, 3, 'Api access token', '[]', 0, '2021-05-04 01:27:43', '2021-05-04 01:27:43', '2022-05-04 06:57:43'),
('fd52d7ecd25092b18fae607fdf7d4f4aecf084ba9d5cf282842d957150a4205f6e80f7f3515f69cc', 1, 3, 'Api access token', '[]', 1, '2021-07-29 08:18:39', '2021-07-29 08:18:39', '2022-07-29 13:48:39'),
('fd72338e4bcf98282b7e9fe94e1dd604d93322b052db1b93191cb44e3027a8a1e37b6d0c4363da5f', 579, 3, 'Api access token', '[]', 0, '2021-04-20 03:14:05', '2021-04-20 03:14:05', '2022-04-20 08:44:05'),
('fdaab26d2d1a7d44c1ff217cfded9cdee3e8340f4a2b08fee801df731f16e3ca21e2179dc2a5c35e', 507, 3, 'Api access token', '[]', 1, '2021-04-01 05:44:42', '2021-04-01 05:44:42', '2022-04-01 11:14:42'),
('fdb185224b49172d7eefd13eed7274dd341f7628946760822893c44d476f8fc13f2754945e683a0a', 534, 3, 'Api access token', '[]', 1, '2021-04-05 01:58:22', '2021-04-05 01:58:22', '2022-04-05 07:28:22'),
('fdca2865b127995005481025c48a156d7d82c9b83a140d6f9624c0b9f3ef2e15caad2ed0da324c5e', 426, 3, 'Api access token', '[]', 0, '2021-03-22 05:46:59', '2021-03-22 05:46:59', '2022-03-22 11:16:59'),
('fde2681f196716be4b625d8e4a4f7e5c5595abde94ca17f206146451832fe4bbbddf5779c0e3f553', 461, 3, 'Api access token', '[]', 1, '2021-03-24 05:28:08', '2021-03-24 05:28:08', '2022-03-24 10:58:08'),
('fdf04f2723b76e3acb4db86a32022e687b51baf5d0d3709dd5e8718f11a0003ee5193f2331076666', 17, 3, 'Api access token', '[]', 1, '2021-07-29 06:54:48', '2021-07-29 06:54:48', '2022-07-29 12:24:48'),
('fe16b124bc2902886a2147f9ab9aff273553b4435b077e20eaced25f749926b03a22376abc7d053a', 636, 3, 'Api access token', '[]', 0, '2021-05-12 04:56:23', '2021-05-12 04:56:23', '2022-05-12 10:26:23'),
('fe230f8953e2db39f2b85cf9531dda236b7adff77c8c13333a17fd23386109bd5b3854c7bb3e74e9', 620, 3, 'Api access token', '[]', 0, '2021-05-02 08:41:49', '2021-05-02 08:41:49', '2022-05-02 14:11:49'),
('fe274f1647b6ca7b0466e4491f4fa45b09187a90870477a0a97964a2169d2e98cc56c1ef26d4dea4', 406, 3, 'Api access token', '[]', 0, '2021-03-18 02:28:46', '2021-03-18 02:28:46', '2022-03-18 07:58:46'),
('fe32d3a9a28db91f2875e1aa685f1c2c22e061fb5200c00300a20addbf372f1552b0e1bbe8118239', 42, 3, 'Api access token', '[]', 0, '2021-07-30 02:27:46', '2021-07-30 02:27:46', '2022-07-30 07:57:46'),
('fe3847ceb8703e8b4c646b3a618c8662cd2ba0628496ffe43a82b0d2c5f8ee10444ffbdb42041e79', 1, 3, 'Api access token', '[]', 0, '2021-07-26 07:30:59', '2021-07-26 07:30:59', '2022-07-26 13:00:59'),
('fe7a6fe55f426f2516c4772c388437c25ad6ea127ebd672e8cb320ae7d515895c83caa1149c943f5', 733, 3, 'Api access token', '[]', 0, '2021-06-10 02:37:19', '2021-06-10 02:37:19', '2022-06-10 08:07:19'),
('fe7f1fbf534a42a08e20c439cb722264bc40e44fd86d80aec6e0ac3b321c60e9fc078da05b9cd64e', 576, 3, 'Api access token', '[]', 0, '2021-04-20 02:08:30', '2021-04-20 02:08:30', '2022-04-20 07:38:30'),
('fe803c3f4ade91f3744fc8e4d47e1d7e2e0bd77a946feece8d342be443570eba49101a3b538ee4f6', 60, 3, 'Api access token', '[]', 0, '2021-08-11 23:12:56', '2021-08-11 23:12:56', '2022-08-12 04:42:56'),
('fe9bab1c3f91b0c4589290536948d88d686d7db0f1e8f8f3b15965089e10530dd1f3a0f352ef4315', 534, 3, 'Api access token', '[]', 1, '2021-04-06 02:14:25', '2021-04-06 02:14:25', '2022-04-06 07:44:25'),
('fec5664cd243e863a48308a0afa3da575fabb3c1bbeefbefd7d9127d7eb0292c67f85e0439973cdf', 754, 3, 'Api access token', '[]', 1, '2021-06-24 07:52:07', '2021-06-24 07:52:07', '2022-06-24 13:22:07'),
('fec5bff21ac391ad0e98385427dd198b24cd510792ecb700701f05cfd857ea5e9dd91795b763eb0f', 593, 3, 'Api access token', '[]', 0, '2021-05-02 07:34:42', '2021-05-02 07:34:42', '2022-05-02 13:04:42'),
('ff0da67cf9f25576ba4c806fbed44c8f198b66026e6adcb6c0650b3cc3e6f7fd36e278e648ce5a5f', 759, 3, 'Api access token', '[]', 0, '2021-07-20 02:37:42', '2021-07-20 02:37:42', '2022-07-20 08:07:42'),
('ff1e9eb05b9db90c55cc0e37c0c0e1c91268986e1a02a46052d5b199a90cdda51b87f0414735457e', 728, 3, 'Api access token', '[]', 0, '2021-06-10 01:47:48', '2021-06-10 01:47:48', '2022-06-10 07:17:48'),
('ff2655ee37f89381e4ea5499747f63391d3b24ffa7ad829246d755585178f430e12fb66ac5c7dd7c', 439, 3, 'Api access token', '[]', 1, '2021-03-23 02:13:09', '2021-03-23 02:13:09', '2022-03-23 07:43:09'),
('ff37b9b27718af4269ae9a7ed37d280ba789ee470a9605aa9b8a73ba355effa12589a2066ebbc297', 1, 3, 'Api access token', '[]', 1, '2021-07-22 06:07:47', '2021-07-22 06:07:47', '2022-07-22 11:37:47'),
('ff3d5ba84e31260d1b247f69fbb971abed2173d6d8bd1d250ebf48218e2780b5884df5ce6b0c5478', 22, 3, 'Api access token', '[]', 1, '2021-07-27 23:37:02', '2021-07-27 23:37:02', '2022-07-28 05:07:02'),
('ff59b14ead8902420b1d634301441f82f35cd0bffc493a272bb55500758dfa161782943aadb15ba0', 432, 3, 'Api access token', '[]', 0, '2021-03-23 00:56:12', '2021-03-23 00:56:12', '2022-03-23 06:26:12'),
('ff9ba34a84eebeebee331b8d5e115fdc50df436dab1dd303a4816e9e98819e6ca209009560ee1a2c', 475, 3, 'Api access token', '[]', 0, '2021-03-26 02:26:59', '2021-03-26 02:26:59', '2022-03-26 07:56:59'),
('ffaeb1ce8d7a354efd5cc6612d4c42a4ba1b501a47eb27cdc789b9204bfbabe9fde4dd9e3b4abde8', 494, 3, 'Api access token', '[]', 0, '2021-03-30 00:37:08', '2021-03-30 00:37:08', '2022-03-30 06:07:08'),
('ffb0620c518bb55485234d607dd1a207accc891d7ee6c728b95e6ed1a1388be365162bb3d05a56d3', 625, 3, 'Api access token', '[]', 0, '2021-05-15 06:22:22', '2021-05-15 06:22:22', '2022-05-15 11:52:22'),
('ffbeae59bf2f7c908da22822cc13f91a3c55dca4f47a1814b2edbbc8f0393475722766d1375ac7b6', 438, 3, 'Api access token', '[]', 0, '2021-03-23 01:42:43', '2021-03-23 01:42:43', '2022-03-23 07:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'Nx7xHT96W3p00sizJI29gbRQhxQbOmY5Z9d0YVp1', NULL, 'http://localhost', 1, 0, 0, '2021-03-17 06:20:01', '2021-03-17 06:20:01'),
(2, NULL, 'Laravel Password Grant Client', 'HdARkf0BYNkm6ULvNZbo6xj5ggLSzvMIWHjkcp2i', 'users', 'http://localhost', 0, 1, 0, '2021-03-17 06:20:01', '2021-03-17 06:20:01'),
(3, NULL, 'Laravel Personal Access Client', 'ybNli3bh2QcRNQvGxvZkXwauWnZtwM6IOO5sX6d2', NULL, 'http://localhost', 1, 0, 0, '2021-03-17 08:03:23', '2021-03-17 08:03:23'),
(4, NULL, 'Laravel Password Grant Client', 'WtOTIdcirJghfi6mfLsU5gBjLhgWWtWyiQiNxzoF', 'users', 'http://localhost', 0, 1, 0, '2021-03-17 08:03:23', '2021-03-17 08:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-03-17 06:20:01', '2021-03-17 06:20:01'),
(2, 3, '2021-03-17 08:03:23', '2021-03-17 08:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bookingId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `gst_tax` decimal(12,2) NOT NULL,
  `total_price` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount` decimal(8,2) NOT NULL,
  `new_price` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `bookingId`, `productId`, `productName`, `productDescription`, `quantity`, `price`, `gst_tax`, `total_price`, `created_at`, `updated_at`, `discount`, `new_price`) VALUES
(1, 1, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-22 06:41:10', '2021-07-22 06:41:10', '10.00', '45.00'),
(2, 1, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-22 06:41:10', '2021-07-22 06:41:10', '10.00', '9.00'),
(3, 2, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-22 06:44:52', '2021-07-22 06:44:52', '10.00', '45.00'),
(4, 3, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-23 06:26:09', '2021-07-23 06:26:09', '5.00', '76.00'),
(5, 4, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-26 07:29:33', '2021-07-26 07:29:33', '10.00', '45.00'),
(6, 5, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-26 07:32:17', '2021-07-26 07:32:17', '10.00', '45.00'),
(7, 6, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 05:21:31', '2021-07-27 05:21:31', '10.00', '45.00'),
(8, 6, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-27 05:21:31', '2021-07-27 05:21:31', '10.00', '9.00'),
(9, 7, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 05:57:19', '2021-07-27 05:57:19', '10.00', '45.00'),
(10, 8, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 06:00:43', '2021-07-27 06:00:43', '10.00', '45.00'),
(11, 9, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 06:01:07', '2021-07-27 06:01:07', '10.00', '45.00'),
(12, 10, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-27 07:23:58', '2021-07-27 07:23:58', '5.00', '76.00'),
(13, 11, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 07:43:34', '2021-07-27 07:43:34', '10.00', '45.00'),
(14, 12, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-27 07:45:33', '2021-07-27 07:45:33', '10.00', '45.00'),
(15, 13, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 00:14:48', '2021-07-28 00:14:48', '10.00', '45.00'),
(16, 14, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 00:15:58', '2021-07-28 00:15:58', '10.00', '45.00'),
(17, 15, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 00:16:30', '2021-07-28 00:16:30', '10.00', '45.00'),
(18, 16, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 00:17:33', '2021-07-28 00:17:33', '10.00', '45.00'),
(19, 17, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 01:05:16', '2021-07-28 01:05:16', '5.00', '76.00'),
(20, 18, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-28 01:15:42', '2021-07-28 01:15:42', '5.00', '76.00'),
(21, 19, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-28 01:16:23', '2021-07-28 01:16:23', '5.00', '76.00'),
(22, 20, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-28 01:17:42', '2021-07-28 01:17:42', '5.00', '76.00'),
(23, 21, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 02:14:01', '2021-07-28 02:14:01', '5.00', '76.00'),
(24, 22, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 02:17:13', '2021-07-28 02:17:13', '5.00', '76.00'),
(25, 23, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 02:35:51', '2021-07-28 02:35:51', '5.00', '76.00'),
(26, 24, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-28 02:46:31', '2021-07-28 02:46:31', '5.00', '76.00'),
(27, 25, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 04:49:30', '2021-07-28 04:49:30', '5.00', '76.00'),
(28, 26, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 05:05:51', '2021-07-28 05:05:51', '5.00', '76.00'),
(29, 27, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 05:07:15', '2021-07-28 05:07:15', '5.00', '76.00'),
(30, 28, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 08:14:04', '2021-07-28 08:14:04', '10.00', '45.00'),
(31, 29, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 08:22:20', '2021-07-28 08:22:20', '10.00', '45.00'),
(32, 30, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 08:24:10', '2021-07-28 08:24:10', '10.00', '45.00'),
(33, 31, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-28 23:25:07', '2021-07-28 23:25:07', '5.00', '76.00'),
(34, 32, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-28 23:53:38', '2021-07-28 23:53:38', '10.00', '45.00'),
(35, 33, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 00:20:58', '2021-07-29 00:20:58', '10.00', '45.00'),
(36, 34, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 00:56:22', '2021-07-29 00:56:22', '10.00', '45.00'),
(37, 35, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 00:57:04', '2021-07-29 00:57:04', '10.00', '45.00'),
(38, 36, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 00:59:48', '2021-07-29 00:59:48', '10.00', '45.00'),
(39, 37, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 01:02:55', '2021-07-29 01:02:55', '10.00', '45.00'),
(40, 38, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 01:07:57', '2021-07-29 01:07:57', '10.00', '45.00'),
(41, 39, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 02:13:43', '2021-07-29 02:13:43', '10.00', '45.00'),
(42, 39, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 02:13:43', '2021-07-29 02:13:43', '10.00', '9.00'),
(43, 40, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 02:41:43', '2021-07-29 02:41:43', '10.00', '45.00'),
(44, 40, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 02:41:43', '2021-07-29 02:41:43', '10.00', '9.00'),
(45, 41, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 04:10:35', '2021-07-29 04:10:35', '10.00', '45.00'),
(46, 41, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 04:10:35', '2021-07-29 04:10:35', '10.00', '9.00'),
(47, 42, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 04:20:35', '2021-07-29 04:20:35', '5.00', '76.00'),
(48, 43, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 04:24:03', '2021-07-29 04:24:03', '10.00', '45.00'),
(49, 43, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 04:24:03', '2021-07-29 04:24:03', '10.00', '9.00'),
(50, 44, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 04:37:38', '2021-07-29 04:37:38', '10.00', '45.00'),
(51, 44, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 04:37:38', '2021-07-29 04:37:38', '10.00', '9.00'),
(52, 45, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-29 04:56:20', '2021-07-29 04:56:20', '5.00', '76.00'),
(53, 46, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 2, '80.00', '7.60', '159.60', '2021-07-29 04:56:58', '2021-07-29 04:56:58', '5.00', '76.00'),
(54, 47, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 05:19:21', '2021-07-29 05:19:21', '5.00', '76.00'),
(55, 48, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 05:37:43', '2021-07-29 05:37:43', '5.00', '76.00'),
(56, 49, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 05:41:36', '2021-07-29 05:41:36', '5.00', '76.00'),
(57, 50, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 05:59:29', '2021-07-29 05:59:29', '5.00', '76.00'),
(58, 51, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 06:01:16', '2021-07-29 06:01:16', '5.00', '76.00'),
(59, 52, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 06:13:52', '2021-07-29 06:13:52', '10.00', '45.00'),
(60, 52, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 06:13:52', '2021-07-29 06:13:52', '10.00', '9.00'),
(61, 53, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 06:27:48', '2021-07-29 06:27:48', '10.00', '45.00'),
(62, 53, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 06:27:48', '2021-07-29 06:27:48', '10.00', '9.00'),
(63, 54, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 06:30:22', '2021-07-29 06:30:22', '5.00', '76.00'),
(64, 55, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-29 06:36:04', '2021-07-29 06:36:04', '5.00', '76.00'),
(65, 56, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 06:58:28', '2021-07-29 06:58:28', '10.00', '45.00'),
(66, 56, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 06:58:29', '2021-07-29 06:58:29', '10.00', '9.00'),
(67, 57, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 07:32:59', '2021-07-29 07:32:59', '10.00', '45.00'),
(68, 57, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 07:32:59', '2021-07-29 07:32:59', '10.00', '9.00'),
(69, 58, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 08:19:04', '2021-07-29 08:19:04', '10.00', '45.00'),
(70, 58, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 08:19:04', '2021-07-29 08:19:04', '10.00', '9.00'),
(71, 59, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 23:02:57', '2021-07-29 23:02:57', '10.00', '45.00'),
(72, 59, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 23:02:57', '2021-07-29 23:02:57', '10.00', '9.00'),
(73, 60, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 23:29:35', '2021-07-29 23:29:35', '10.00', '45.00'),
(74, 60, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 23:29:35', '2021-07-29 23:29:35', '10.00', '9.00'),
(75, 61, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-29 23:56:28', '2021-07-29 23:56:28', '10.00', '45.00'),
(76, 61, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-29 23:56:28', '2021-07-29 23:56:28', '10.00', '9.00'),
(77, 62, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 00:04:02', '2021-07-30 00:04:02', '10.00', '45.00'),
(78, 62, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 00:04:02', '2021-07-30 00:04:02', '10.00', '9.00'),
(79, 63, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 00:12:58', '2021-07-30 00:12:58', '10.00', '45.00'),
(80, 63, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 00:12:58', '2021-07-30 00:12:58', '10.00', '9.00'),
(81, 64, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 00:13:52', '2021-07-30 00:13:52', '10.00', '45.00'),
(82, 64, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 00:13:52', '2021-07-30 00:13:52', '10.00', '9.00'),
(83, 65, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 00:43:50', '2021-07-30 00:43:50', '10.00', '45.00'),
(84, 65, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 00:43:50', '2021-07-30 00:43:50', '10.00', '9.00'),
(85, 66, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-30 01:37:10', '2021-07-30 01:37:10', '5.00', '76.00'),
(86, 67, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-30 01:44:27', '2021-07-30 01:44:27', '5.00', '76.00'),
(87, 68, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-30 01:49:37', '2021-07-30 01:49:37', '5.00', '76.00'),
(88, 69, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-30 02:25:37', '2021-07-30 02:25:37', '5.00', '76.00'),
(89, 70, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 3, '80.00', '11.40', '239.40', '2021-07-30 02:28:13', '2021-07-30 02:28:13', '5.00', '76.00'),
(90, 71, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-30 02:30:28', '2021-07-30 02:30:28', '5.00', '76.00'),
(91, 72, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 04:50:57', '2021-07-30 04:50:57', '10.00', '45.00'),
(92, 72, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 04:50:57', '2021-07-30 04:50:57', '10.00', '9.00'),
(93, 73, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 04:59:16', '2021-07-30 04:59:16', '10.00', '45.00'),
(94, 73, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 04:59:16', '2021-07-30 04:59:16', '10.00', '9.00'),
(95, 74, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 05:29:01', '2021-07-30 05:29:01', '10.00', '45.00'),
(96, 74, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 05:29:01', '2021-07-30 05:29:01', '10.00', '9.00'),
(97, 75, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-07-30 05:31:57', '2021-07-30 05:31:57', '10.00', '45.00'),
(98, 75, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-07-30 05:31:57', '2021-07-30 05:31:57', '10.00', '9.00'),
(99, 76, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-07-31 00:27:14', '2021-07-31 00:27:14', '5.00', '76.00'),
(100, 77, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-02 00:14:10', '2021-08-02 00:14:10', '10.00', '45.00'),
(101, 78, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-02 00:17:23', '2021-08-02 00:17:23', '10.00', '45.00'),
(102, 78, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-02 00:17:23', '2021-08-02 00:17:23', '10.00', '9.00'),
(103, 79, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-02 01:02:31', '2021-08-02 01:02:31', '10.00', '45.00'),
(104, 79, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-02 01:02:31', '2021-08-02 01:02:31', '10.00', '9.00'),
(105, 80, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-02 01:09:58', '2021-08-02 01:09:58', '10.00', '45.00'),
(106, 80, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-02 01:09:58', '2021-08-02 01:09:58', '10.00', '9.00'),
(107, 81, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 00:15:35', '2021-08-03 00:15:35', '10.00', '45.00'),
(108, 81, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 00:15:35', '2021-08-03 00:15:35', '10.00', '9.00'),
(109, 82, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 00:25:56', '2021-08-03 00:25:56', '10.00', '45.00'),
(110, 82, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 00:25:56', '2021-08-03 00:25:56', '10.00', '9.00'),
(111, 83, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:00:09', '2021-08-03 01:00:09', '10.00', '45.00'),
(112, 84, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:36:57', '2021-08-03 01:36:57', '10.00', '45.00'),
(113, 84, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 01:36:57', '2021-08-03 01:36:57', '10.00', '9.00'),
(114, 85, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:45:20', '2021-08-03 01:45:20', '10.00', '45.00'),
(115, 85, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 01:45:20', '2021-08-03 01:45:20', '10.00', '9.00'),
(116, 86, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:49:32', '2021-08-03 01:49:32', '10.00', '45.00'),
(117, 86, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 01:49:32', '2021-08-03 01:49:32', '10.00', '9.00'),
(118, 87, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:55:11', '2021-08-03 01:55:11', '10.00', '45.00'),
(119, 87, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 01:55:11', '2021-08-03 01:55:11', '10.00', '9.00'),
(120, 88, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 01:59:58', '2021-08-03 01:59:58', '10.00', '45.00'),
(121, 88, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 01:59:58', '2021-08-03 01:59:58', '10.00', '9.00'),
(122, 89, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 06:41:34', '2021-08-03 06:41:34', '10.00', '45.00'),
(123, 90, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 06:46:47', '2021-08-03 06:46:47', '10.00', '45.00'),
(124, 91, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 06:50:35', '2021-08-03 06:50:35', '10.00', '45.00'),
(125, 92, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 06:54:41', '2021-08-03 06:54:41', '10.00', '45.00'),
(126, 92, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 06:54:41', '2021-08-03 06:54:41', '10.00', '9.00'),
(127, 93, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 07:09:03', '2021-08-03 07:09:03', '10.00', '45.00'),
(128, 93, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 07:09:03', '2021-08-03 07:09:03', '10.00', '9.00'),
(129, 94, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 07:16:57', '2021-08-03 07:16:57', '10.00', '45.00'),
(130, 94, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 07:16:57', '2021-08-03 07:16:57', '10.00', '9.00'),
(131, 95, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 07:27:19', '2021-08-03 07:27:19', '10.00', '45.00'),
(132, 96, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 07:30:10', '2021-08-03 07:30:10', '10.00', '45.00'),
(133, 96, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-03 07:30:10', '2021-08-03 07:30:10', '10.00', '9.00'),
(134, 97, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-03 07:37:11', '2021-08-03 07:37:11', '10.00', '45.00'),
(135, 98, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-08-04 00:06:19', '2021-08-04 00:06:19', '5.00', '76.00'),
(136, 99, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-08-04 01:08:58', '2021-08-04 01:08:58', '5.00', '76.00'),
(137, 100, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 02:09:20', '2021-08-04 02:09:20', '10.00', '45.00'),
(138, 100, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-04 02:09:20', '2021-08-04 02:09:20', '10.00', '9.00'),
(139, 101, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-08-04 02:43:24', '2021-08-04 02:43:24', '5.00', '76.00'),
(140, 102, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 04:52:03', '2021-08-04 04:52:03', '10.00', '45.00'),
(141, 102, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-04 04:52:03', '2021-08-04 04:52:03', '10.00', '9.00'),
(142, 103, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 05:21:11', '2021-08-04 05:21:11', '10.00', '45.00'),
(143, 103, 2, 'Wine', 'adf', 1, '10.00', '0.00', '9.00', '2021-08-04 05:21:11', '2021-08-04 05:21:11', '10.00', '9.00'),
(144, 104, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 06:09:29', '2021-08-04 06:09:29', '10.00', '45.00'),
(145, 105, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 06:10:41', '2021-08-04 06:10:41', '10.00', '45.00'),
(146, 105, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-04 06:10:41', '2021-08-04 06:10:41', '10.00', '9.00'),
(147, 106, 2, 'Wine', 'adf', 1, '10.00', '0.00', '9.00', '2021-08-04 06:41:47', '2021-08-04 06:41:47', '10.00', '9.00'),
(148, 107, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-04 08:12:25', '2021-08-04 08:12:25', '10.00', '45.00'),
(149, 107, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-04 08:12:25', '2021-08-04 08:12:25', '10.00', '9.00'),
(150, 108, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-05 02:53:07', '2021-08-05 02:53:07', '10.00', '45.00'),
(151, 108, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-05 02:53:07', '2021-08-05 02:53:07', '10.00', '9.00'),
(152, 109, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-05 03:02:42', '2021-08-05 03:02:42', '10.00', '45.00'),
(153, 109, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-05 03:02:42', '2021-08-05 03:02:42', '10.00', '9.00'),
(154, 110, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-05 04:30:50', '2021-08-05 04:30:50', '10.00', '45.00'),
(155, 110, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-05 04:30:50', '2021-08-05 04:30:50', '10.00', '9.00'),
(156, 111, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-05 04:41:34', '2021-08-05 04:41:34', '10.00', '45.00'),
(157, 111, 2, 'Wine', 'adf', 2, '10.00', '0.00', '9.00', '2021-08-05 04:41:34', '2021-08-05 04:41:34', '10.00', '9.00'),
(158, 112, 3, 'Coffeeeee', 'Nexa Tower, F-338, Sector 74 A, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055, India', 1, '80.00', '3.80', '79.80', '2021-08-11 03:56:38', '2021-08-11 03:56:38', '5.00', '76.00'),
(159, 113, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-13 02:23:47', '2021-08-13 02:23:47', '10.00', '45.00'),
(160, 114, 1, 'Coffee', 'asdf', 1, '50.00', '4.50', '49.50', '2021-08-18 05:20:45', '2021-08-18 05:20:45', '10.00', '45.00');

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `phone_number`, `country_code`, `email`, `otp`, `is_verified`, `created_at`, `updated_at`) VALUES
(27, '9501648707', '+1', NULL, '4444', 1, '2021-07-26 01:30:43', '2021-07-26 01:30:47'),
(30, '1627302054', '+91', NULL, '4444', 0, '2021-07-26 06:54:20', '2021-07-26 06:54:20'),
(40, '9501648709', '+1', NULL, '4444', 1, '2021-07-27 04:09:00', '2021-07-27 04:09:04'),
(51, '8182838483', '+1', NULL, '4444', 1, '2021-07-27 04:33:18', '2021-07-27 04:33:31'),
(55, NULL, NULL, 'testvendor@mailinator.com', '4444', 1, '2021-07-27 05:19:39', '2021-07-27 05:19:43'),
(63, '5152535154', '+1', NULL, '4444', 1, '2021-07-27 07:36:27', '2021-07-27 07:36:38'),
(64, '8182838184', '+1', NULL, '4444', 1, '2021-07-27 07:37:10', '2021-07-27 07:37:18'),
(66, '8182838486', '+1', NULL, '4444', 1, '2021-07-27 23:36:53', '2021-07-27 23:37:01'),
(67, '8182838485', '+1', NULL, '4444', 1, '2021-07-27 23:41:31', '2021-07-27 23:41:36'),
(72, '9501648704121', '+1', NULL, '4444', 0, '2021-07-28 02:59:16', '2021-07-28 02:59:16'),
(78, '2587415658', '+91', NULL, '4444', 1, '2021-07-28 05:30:13', '2021-07-28 05:30:21'),
(82, '1627477208', '+1', NULL, '4444', 1, '2021-07-28 07:35:44', '2021-07-28 07:35:48'),
(83, '9192939193', '+91', NULL, '4444', 1, '2021-07-28 08:03:17', '2021-07-28 08:03:22'),
(88, '9698688655', '+1', NULL, '4444', 1, '2021-07-29 00:37:32', '2021-07-29 00:37:37'),
(99, '8182838183', '+1', NULL, '4444', 1, '2021-07-29 01:06:05', '2021-07-29 01:06:11'),
(106, '86868686868', '+1', NULL, '4444', 0, '2021-07-29 02:17:26', '2021-07-29 02:17:26'),
(117, '8686868686', '+1', NULL, '4444', 1, '2021-07-29 05:52:14', '2021-07-29 05:52:31'),
(122, '99696969696', '+1', NULL, '4444', 1, '2021-07-29 07:57:49', '2021-07-29 07:57:54'),
(154, '9501870654', '+1', NULL, '4444', 1, '2021-07-31 01:03:57', '2021-07-31 01:04:03'),
(156, '9569112625', '+1', NULL, '4444', 1, '2021-08-02 00:12:38', '2021-08-02 00:12:45'),
(171, '8989898986', '+1', NULL, '4444', 1, '2021-08-03 01:43:18', '2021-08-03 01:43:22'),
(175, '5152535456', '+1', NULL, '4444', 1, '2021-08-03 01:47:07', '2021-08-03 01:47:11'),
(177, '5152535153', '+1', NULL, '4444', 1, '2021-08-03 01:54:44', '2021-08-03 01:54:50'),
(184, '9501648106', '+1', NULL, '4444', 0, '2021-08-03 04:57:31', '2021-08-03 04:57:31'),
(187, '9696969696', '+1', NULL, '4444', 1, '2021-08-03 07:11:09', '2021-08-03 07:11:12'),
(191, '3535353535', '+1', NULL, '4444', 1, '2021-08-03 07:35:26', '2021-08-03 07:35:31'),
(200, '8989898989', '+1', NULL, '4444', 1, '2021-08-04 02:43:10', '2021-08-04 02:43:14'),
(250, '9575212184', '+1', NULL, '4444', 1, '2021-08-04 07:35:23', '2021-08-05 02:55:12'),
(265, '95752121', '+1', NULL, '4444', 0, '2021-08-05 02:43:41', '2021-08-05 02:43:41'),
(302, '8182838487', '+1', NULL, '4444', 1, '2021-08-06 04:18:58', '2021-08-06 04:19:08'),
(306, '1234567890', '+1', NULL, '4444', 0, '2021-08-06 04:43:22', '2021-08-06 04:43:22'),
(319, '9501648704', '+1', NULL, '4444', 0, '2021-08-06 06:29:10', '2021-08-06 06:29:10'),
(320, '7082559204', '+1', NULL, '4444', 1, '2021-08-06 06:58:25', '2021-08-06 06:58:33'),
(324, '9501648745', '+1', NULL, '4444', 1, '2021-08-06 23:32:04', '2021-08-06 23:32:06'),
(325, '9501648746', '+1', NULL, '4444', 1, '2021-08-06 23:33:32', '2021-08-06 23:33:34'),
(329, '9501648701', '+1', NULL, '4444', 0, '2021-08-06 23:47:40', '2021-08-06 23:47:40'),
(330, '9501648774', '+1', NULL, '4444', 1, '2021-08-06 23:49:32', '2021-08-06 23:49:38'),
(345, '3065197634', '+1', NULL, '4444', 1, '2021-08-08 21:24:48', '2021-08-08 21:25:06'),
(347, '8708882971', '+1', NULL, '4444', 0, '2021-08-10 00:20:41', '2021-08-10 00:20:41'),
(350, '8630901347', '+1', NULL, '4444', 1, '2021-08-10 23:53:40', '2021-08-10 23:53:50'),
(355, '9501648700', '+1', NULL, '4444', 1, '2021-08-11 00:35:29', '2021-08-11 00:35:32'),
(357, '9501648766', '+1', NULL, '4444', 1, '2021-08-11 02:45:50', '2021-08-11 02:45:52'),
(359, '8630901346', '+1', NULL, '4444', 1, '2021-08-11 03:47:16', '2021-08-11 03:47:27'),
(366, '', '', 'adminupmart@yopmail.com', '4444', 1, '2021-08-11 04:05:24', '2021-08-11 04:07:03'),
(380, '8485865153', '+1', NULL, '4444', 1, '2021-08-13 01:52:36', '2021-08-13 01:52:41'),
(382, '20515252', '+1', NULL, '4444', 0, '2021-08-13 01:56:01', '2021-08-13 01:56:01'),
(384, '8485868485', '+1', NULL, '4444', 1, '2021-08-13 01:58:15', '2021-08-13 01:58:21'),
(386, '8485868484', '+1', NULL, '4444', 0, '2021-08-13 02:04:43', '2021-08-13 02:04:43'),
(388, '84858684', '+1', NULL, '4444', 1, '2021-08-13 02:18:59', '2021-08-13 02:19:05'),
(392, '3065266841', '+1', NULL, '4444', 1, '2021-08-15 12:04:23', '2021-08-15 12:04:35'),
(394, '3062068789', '+1', NULL, '4444', 0, '2021-08-15 12:56:09', '2021-08-15 12:56:09'),
(396, '3062098789', '+1', NULL, '4444', 1, '2021-08-15 13:01:32', '2021-08-15 13:01:36'),
(397, '3062060765', '+1', NULL, '4444', 1, '2021-08-16 00:48:20', '2021-08-16 00:48:26'),
(399, '1235289745', '+1', NULL, '4444', 1, '2021-08-17 05:58:09', '2021-08-17 05:58:12'),
(402, '1235289740', '+1', NULL, '4444', 1, '2021-08-17 06:12:15', '2021-08-17 06:12:18'),
(407, '8485868183', '+1', NULL, '4444', 1, '2021-08-18 05:10:13', '2021-08-18 05:10:18'),
(409, '8979908832', '+1', NULL, '4444', 1, '2021-08-18 05:24:23', '2021-08-18 05:24:28'),
(413, '9501648706', '+1', NULL, '4444', 1, '2021-08-18 23:37:21', '2021-08-18 23:37:27'),
(414, '8485868486', '+1', NULL, '4444', 1, '2021-08-19 00:40:26', '2021-08-19 00:40:29'),
(415, '7404123203', '+1', NULL, '4444', 0, '2021-08-19 09:02:40', '2021-08-19 09:02:40'),
(417, '6162166595', '+1', NULL, '4444', 0, '2021-08-19 09:06:53', '2021-08-19 09:06:53'),
(418, '7329624560', '+1', NULL, '4444', 0, '2021-08-19 09:10:01', '2021-08-19 09:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `page_for_terms`
--

CREATE TABLE `page_for_terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `page_for_terms`
--

INSERT INTO `page_for_terms` (`id`, `page_id`, `page_name`, `text`, `version`, `created_at`, `updated_at`) VALUES
(1, 3, 'Privacy And Policy', 'hello', 1, NULL, NULL),
(2, 2, 'Terms And Conditions', 'hello', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(2, 'admin.translation.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(3, 'admin.translation.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(4, 'admin.translation.rescan', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(5, 'admin.admin-user.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(6, 'admin.admin-user.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(7, 'admin.admin-user.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(8, 'admin.admin-user.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(9, 'admin.upload', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(10, 'admin.admin-user.impersonal-login', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(11, 'admin.category', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(12, 'admin.category.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(13, 'admin.category.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(14, 'admin.category.show', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(15, 'admin.category.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(16, 'admin.category.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(17, 'admin.category.bulk-delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(18, 'admin.vendor', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(19, 'admin.vendor.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(20, 'admin.vendor.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(21, 'admin.vendor.show', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(22, 'admin.vendor.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(23, 'admin.vendor.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(24, 'admin.vendor.bulk-delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(25, 'admin.company', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(26, 'admin.company.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(27, 'admin.company.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(28, 'admin.company.show', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(29, 'admin.company.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(30, 'admin.company.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(31, 'admin.company.bulk-delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(32, 'admin.product', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(33, 'admin.product.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(34, 'admin.product.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(35, 'admin.product.show', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(36, 'admin.product.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(37, 'admin.product.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(38, 'admin.product.bulk-delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(39, 'admin.item', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(40, 'admin.item.index', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(41, 'admin.item.create', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(42, 'admin.item.show', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(43, 'admin.item.edit', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(44, 'admin.item.delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(45, 'admin.item.bulk-delete', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(46, 'admin.user', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(47, 'admin.user.index', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(48, 'admin.user.create', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(49, 'admin.user.show', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(50, 'admin.user.edit', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(51, 'admin.user.delete', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(52, 'admin.user.bulk-delete', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(53, 'admin.admin', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(54, 'admin.admin.index', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(55, 'admin.admin.create', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(56, 'admin.admin.show', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(57, 'admin.admin.edit', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(58, 'admin.admin.delete', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(59, 'admin.admin.bulk-delete', 'admin', '2020-12-17 05:10:19', '2020-12-17 05:10:19'),
(60, 'admin.admin-vendor', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(61, 'admin.admin-vendor.index', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(62, 'admin.admin-vendor.create', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(63, 'admin.admin-vendor.show', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(64, 'admin.admin-vendor.edit', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(65, 'admin.admin-vendor.delete', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(66, 'admin.admin-vendor.bulk-delete', 'admin', '2020-12-21 01:43:35', '2020-12-21 01:43:35'),
(67, '', 'admin', '2020-12-25 00:00:53', '2020-12-25 00:00:53'),
(68, 'admin.usre.edit', 'admin', '2020-12-25 00:07:08', '2020-12-25 00:07:08'),
(69, 'admin.item.user-delete', 'admin', '2020-12-25 00:07:41', '2020-12-25 00:07:41'),
(70, 'admin.usre.show', 'admin', '2020-12-25 00:08:25', '2020-12-25 00:08:25'),
(71, 'admin.stripe-connect', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(72, 'admin.stripe-connect.index', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(73, 'admin.stripe-connect.create', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(74, 'admin.stripe-connect.show', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(75, 'admin.stripe-connect.edit', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(76, 'admin.stripe-connect.delete', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(77, 'admin.stripe-connect.bulk-delete', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(78, 'admin.stripe-token', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(79, 'admin.stripe-token.index', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(80, 'admin.stripe-token.create', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(81, 'admin.stripe-token.show', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(82, 'admin.stripe-token.edit', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(83, 'admin.stripe-token.delete', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(84, 'admin.stripe-token.bulk-delete', 'admin', '2021-01-04 00:47:23', '2021-01-04 00:47:23'),
(85, 'admin.coupon', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(86, 'admin.coupon.index', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(87, 'admin.coupon.create', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(88, 'admin.coupon.show', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(89, 'admin.coupon.edit', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(90, 'admin.coupon.delete', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(91, 'admin.coupon.bulk-delete', 'admin', '2021-01-08 02:27:36', '2021-01-08 02:27:36'),
(92, 'admin.charge', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(93, 'admin.charge.index', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(94, 'admin.charge.create', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(95, 'admin.charge.show', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(96, 'admin.charge.edit', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(97, 'admin.charge.delete', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(98, 'admin.charge.bulk-delete', 'admin', '2021-01-30 01:43:08', '2021-01-30 01:43:08'),
(99, 'admin.booking-detail', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(100, 'admin.booking-detail.index', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(101, 'admin.booking-detail.create', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(102, 'admin.booking-detail.show', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(103, 'admin.booking-detail.edit', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(104, 'admin.booking-detail.delete', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(105, 'admin.booking-detail.bulk-delete', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(106, 'admin.various-charge', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(107, 'admin.various-charge.index', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(108, 'admin.various-charge.create', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(109, 'admin.various-charge.show', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(110, 'admin.various-charge.edit', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(111, 'admin.various-charge.delete', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(112, 'admin.various-charge.bulk-delete', 'admin', '2021-02-01 06:31:20', '2021-02-01 06:31:20'),
(113, 'admin.table-banners-location-wise', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(114, 'admin.table-banners-location-wise.index', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(115, 'admin.table-banners-location-wise.create', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(116, 'admin.table-banners-location-wise.show', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(117, 'admin.table-banners-location-wise.edit', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(118, 'admin.table-banners-location-wise.delete', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(119, 'admin.table-banners-location-wise.bulk-delete', 'admin', '2021-02-03 04:26:10', '2021-02-03 04:26:10'),
(120, 'admin.help-and-support', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(121, 'admin.help-and-support.index', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(122, 'admin.help-and-support.create', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(123, 'admin.help-and-support.show', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(124, 'admin.help-and-support.edit', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(125, 'admin.help-and-support.delete', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(126, 'admin.help-and-support.bulk-delete', 'admin', '2021-02-15 00:59:29', '2021-02-15 00:59:29'),
(127, 'admin.profile.edit', 'web', '2021-02-16 07:48:09', '2021-02-16 07:48:09'),
(128, 'admin.profile.edit', 'admin', '2021-02-16 07:48:26', '2021-02-16 07:48:26'),
(129, 'admin.admin-setting', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(130, 'admin.admin-setting.index', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(131, 'admin.admin-setting.create', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(132, 'admin.admin-setting.show', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(133, 'admin.admin-setting.edit', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(134, 'admin.admin-setting.delete', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(135, 'admin.admin-setting.bulk-delete', 'admin', '2021-02-22 06:30:11', '2021-02-22 06:30:11'),
(136, 'admin.faq-category', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(137, 'admin.faq-category.index', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(138, 'admin.faq-category.create', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(139, 'admin.faq-category.show', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(140, 'admin.faq-category.edit', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(141, 'admin.faq-category.delete', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(142, 'admin.faq-category.bulk-delete', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(143, 'admin.faq', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(144, 'admin.faq.index', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(145, 'admin.faq.create', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(146, 'admin.faq.show', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(147, 'admin.faq.edit', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(148, 'admin.faq.delete', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(149, 'admin.faq.bulk-delete', 'admin', '2021-03-11 02:16:37', '2021-03-11 02:16:37'),
(150, 'admin.app-package', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(151, 'admin.app-package.index', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(152, 'admin.app-package.create', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(153, 'admin.app-package.show', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(154, 'admin.app-package.edit', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(155, 'admin.app-package.delete', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(156, 'admin.app-package.bulk-delete', 'admin', '2021-04-27 23:54:02', '2021-04-27 23:54:02'),
(157, 'admin.app-version', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(158, 'admin.app-version.index', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(159, 'admin.app-version.create', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(160, 'admin.app-version.show', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(161, 'admin.app-version.edit', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(162, 'admin.app-version.delete', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(163, 'admin.app-version.bulk-delete', 'admin', '2021-04-27 23:54:36', '2021-04-27 23:54:36'),
(164, 'admin.app-package', 'admin', '2021-04-28 07:00:03', '2021-04-28 07:00:03'),
(165, 'admin.admin-user', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(166, 'admin.admin-user.bulk-delete', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(167, 'admin.admin-user.show', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(168, 'admin.setting', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(169, 'admin.setting.bulk-delete', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(170, 'admin.setting.create', 'admin', '2021-05-24 01:21:27', '2021-05-24 01:21:27'),
(171, 'admin.setting.delete', 'admin', '2021-05-24 01:21:28', '2021-05-24 01:21:28'),
(172, 'admin.setting.edit', 'admin', '2021-05-24 01:21:28', '2021-05-24 01:21:28'),
(173, 'admin.setting.index', 'admin', '2021-05-24 01:21:28', '2021-05-24 01:21:28'),
(174, 'admin.setting.show', 'admin', '2021-05-24 01:21:28', '2021-05-24 01:21:28'),
(175, 'admin.adminadmin.booking-detail', 'admin', '2021-05-24 01:25:19', '2021-05-24 01:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `delivery_boy_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `rating` double(2,1) NOT NULL DEFAULT '0.0',
  `customer_id_by` int(11) DEFAULT NULL,
  `delivery_boy_id_by` int(11) DEFAULT NULL,
  `shop_id_by` int(11) DEFAULT NULL,
  `feedback` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `shop_id`, `delivery_boy_id`, `customer_id`, `rating`, `customer_id_by`, `delivery_boy_id_by`, `shop_id_by`, `feedback`, `created_at`, `updated_at`, `booking_id`) VALUES
(1, 1, NULL, NULL, 4.5, 13, NULL, NULL, NULL, '2021-07-29 00:28:40', '2021-07-29 00:28:40', 30),
(2, 1, NULL, NULL, 2.5, 13, NULL, NULL, 'dndn', '2021-07-29 00:29:04', '2021-07-29 00:29:04', 29),
(3, NULL, 7, NULL, 5.0, 13, NULL, NULL, 'mdmd', '2021-07-29 00:29:49', '2021-07-29 00:29:49', 29),
(4, NULL, 2, NULL, 2.9, 13, NULL, NULL, 'kdd', '2021-07-29 00:29:49', '2021-07-29 00:29:49', 32),
(5, 1, NULL, NULL, 4.5, 13, NULL, NULL, 'ndnnd', '2021-07-29 00:30:51', '2021-07-29 00:30:51', 33),
(6, NULL, 7, NULL, 5.0, 13, NULL, NULL, 'ndnd', '2021-07-29 00:30:51', '2021-07-29 00:30:51', 33),
(7, 1, NULL, NULL, 4.5, 13, NULL, NULL, NULL, '2021-07-29 01:01:50', '2021-07-29 01:01:50', 34),
(8, NULL, 6, NULL, 1.0, 13, NULL, NULL, NULL, '2021-07-29 01:01:50', '2021-07-29 01:01:50', 34);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin', '2020-12-15 00:11:55', '2020-12-15 00:11:55'),
(4, 'vendor', 'admin', '2020-12-22 05:39:24', '2020-12-22 05:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(1, 4),
(5, 4),
(6, 4),
(7, 4),
(8, 4),
(9, 4),
(10, 4),
(11, 4),
(12, 4),
(13, 4),
(14, 4),
(15, 4),
(16, 4),
(17, 4),
(25, 4),
(26, 4),
(27, 4),
(28, 4),
(29, 4),
(30, 4),
(31, 4),
(32, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(39, 4),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 4),
(46, 4),
(47, 4),
(48, 4),
(49, 4),
(50, 4),
(51, 4),
(52, 4),
(53, 4),
(60, 4),
(61, 4),
(62, 4),
(63, 4),
(64, 4),
(65, 4),
(66, 4),
(67, 4),
(68, 4),
(69, 4),
(70, 4),
(71, 4),
(72, 4),
(73, 4),
(74, 4),
(75, 4),
(76, 4),
(77, 4),
(78, 4),
(79, 4),
(80, 4),
(81, 4),
(82, 4),
(83, 4),
(84, 4),
(85, 4),
(86, 4),
(87, 4),
(88, 4),
(89, 4),
(90, 4),
(91, 4),
(99, 4),
(100, 4),
(101, 4),
(102, 4),
(103, 4),
(104, 4),
(105, 4),
(113, 4),
(114, 4),
(115, 4),
(116, 4),
(117, 4),
(118, 4),
(119, 4),
(128, 4),
(165, 4),
(166, 4),
(167, 4),
(168, 4),
(169, 4),
(170, 4),
(171, 4),
(172, 4),
(173, 4),
(174, 4),
(175, 4);

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE `slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shop_id` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `slot_from` time NOT NULL,
  `slot_to` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slots`
--

INSERT INTO `slots` (`id`, `shop_id`, `day`, `slot_from`, `slot_to`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '00:00:00', '23:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(2, 1, 2, '00:00:00', '04:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(3, 1, 3, '00:00:00', '23:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(4, 1, 4, '00:00:00', '23:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(5, 1, 5, '00:00:00', '11:19:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(6, 1, 6, '03:22:00', '23:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(7, 1, 7, '03:19:00', '23:59:00', '2021-07-22 06:20:24', '2021-07-22 06:20:24'),
(8, 2, 1, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(9, 2, 2, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(10, 2, 3, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(11, 2, 4, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(13, 2, 6, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(14, 2, 7, '00:00:00', '23:59:00', '2021-07-23 00:27:30', '2021-07-23 00:27:30'),
(19, 2, 5, '00:00:00', '23:58:00', '2021-07-23 00:36:03', '2021-07-23 00:36:03'),
(20, 3, 1, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(21, 3, 2, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(22, 3, 3, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(23, 3, 4, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(24, 3, 5, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(25, 3, 6, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(26, 3, 7, '00:00:00', '23:59:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(27, 1, 2, '05:00:00', '06:00:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(28, 1, 2, '06:00:00', '07:00:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(29, 1, 2, '07:00:00', '08:00:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(30, 1, 2, '09:00:00', '10:00:00', '2021-07-23 00:49:25', '2021-07-23 00:49:25'),
(31, 4, 1, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(32, 4, 2, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(33, 4, 3, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(34, 4, 4, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(35, 4, 5, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(36, 4, 6, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(37, 4, 7, '00:00:00', '23:59:00', '2021-08-15 11:49:00', '2021-08-15 11:49:00'),
(38, 5, 1, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(39, 5, 2, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(40, 5, 3, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(41, 5, 4, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(42, 5, 5, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(43, 5, 6, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(44, 5, 7, '00:00:00', '23:59:00', '2021-08-15 11:49:31', '2021-08-15 11:49:31'),
(45, 6, 1, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(46, 6, 2, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(47, 6, 3, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(48, 6, 4, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(49, 6, 5, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(50, 6, 6, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(51, 6, 7, '00:00:00', '23:59:00', '2021-08-15 11:49:58', '2021-08-15 11:49:58'),
(52, 7, 1, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(53, 7, 2, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(54, 7, 3, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(55, 7, 4, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(56, 7, 5, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(57, 7, 6, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(58, 7, 7, '00:00:00', '23:59:00', '2021-08-15 11:50:34', '2021-08-15 11:50:34'),
(59, 8, 1, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(60, 8, 2, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(61, 8, 3, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(62, 8, 4, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(63, 8, 5, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(64, 8, 6, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49'),
(65, 8, 7, '00:00:00', '23:59:00', '2021-08-15 11:50:49', '2021-08-15 11:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_payment_records`
--

CREATE TABLE `stripe_payment_records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `payment_intent_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` bigint(20) DEFAULT NULL,
  `booking_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_charge` double NOT NULL,
  `paid_amount` double NOT NULL,
  `payment_status` int(11) NOT NULL COMMENT '1->succeeded,2->refunded',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stripe_payment_records`
--

INSERT INTO `stripe_payment_records` (`id`, `user_id`, `payment_intent_id`, `charge_id`, `payment_id`, `booking_code`, `user_stripe_id`, `currency_code`, `card_id`, `stripe_charge`, `paid_amount`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'pi_1JG0k5BxLRcxFlSDEmwAN9hn', 'ch_1JG0k5BxLRcxFlSDTdSHso2t', NULL, '0S0S20210722121108', 'cus_JtoMZGoE0Iq1Wa', 'USD', 'card_1JG0k1BxLRcxFlSDqMQPyImu', 2.5649, 78.1, 0, '2021-07-22 06:41:10', '2021-07-22 06:41:10'),
(2, 1, 'pi_1JG0neBxLRcxFlSDrg9PP0eq', 'ch_1JG0nfBxLRcxFlSDi5CrReSA', NULL, 'D0SA20210722121450', 'cus_JtoMZGoE0Iq1Wa', 'USD', 'card_1JG0k1BxLRcxFlSDqMQPyImu', 2.2865, 68.5, 0, '2021-07-22 06:44:52', '2021-07-22 06:44:52'),
(3, 5, 'pi_1JGMz6BxLRcxFlSDqMRQ1E65', 'ch_1JGMz6BxLRcxFlSDk4nQVeZz', NULL, 'DASD20210723115607', 'cus_JuBL1o0zeB883g', 'USD', 'card_1JGMyZBxLRcxFlSDsAG6kAWC', 3.48101, 109.69, 0, '2021-07-23 06:26:09', '2021-07-23 06:26:09'),
(4, 12, 'pi_1JHTP6L8INgkSBj9y8SyERGS', 'ch_1JHTP6L8INgkSBj9SSxYanf5', NULL, 'D00D20210726125931', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.48333, 109.77, 2, '2021-07-26 07:29:33', '2021-07-26 07:33:31'),
(5, 12, 'pi_1JHTRjL8INgkSBj9WCfCf0US', 'ch_1JHTRjL8INgkSBj9oXxHoL48', NULL, '0DD020210726130214', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.48333, 109.77, 0, '2021-07-26 07:32:17', '2021-07-26 07:32:17'),
(6, 12, 'pi_1JHnsjL8INgkSBj9PwemOstv', 'ch_1JHnskL8INgkSBj9qC3VZ3nO', NULL, '0DA020210727105129', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.97749, 126.81, 2, '2021-07-27 05:21:31', '2021-07-27 05:37:40'),
(7, 12, 'pi_1JHoROL8INgkSBj90OAhqAOY', 'ch_1JHoROL8INgkSBj9uDNNa308', NULL, 'DSSD20210727112717', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.51233, 110.77, 0, '2021-07-27 05:57:19', '2021-07-27 05:57:19'),
(8, 12, 'pi_1JHoUgL8INgkSBj9ClzYGQZq', 'ch_1JHoUgL8INgkSBj9CXfKo0ze', NULL, 'SASS20210727113041', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.48333, 109.77, 0, '2021-07-27 06:00:43', '2021-07-27 06:00:43'),
(9, 12, 'pi_1JHoV4L8INgkSBj9CzBWaDAF', 'ch_1JHoV4L8INgkSBj9HaQXW6ma', NULL, 'SSS020210727113105', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.48333, 109.77, 0, '2021-07-27 06:01:07', '2021-07-27 06:01:07'),
(10, 19, 'pi_1JHpnEL8INgkSBj936IObecV', 'ch_1JHpnEL8INgkSBj9QnpqFaVT', NULL, 'ASDS20210727125356', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-27 07:23:58', '2021-07-27 07:23:58'),
(11, 13, 'pi_1JHq6CL8INgkSBj947nYPYog', 'ch_1JHq6DL8INgkSBj9JekNUa6H', NULL, 'SDA020210727131332', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.32615, 104.35, 0, '2021-07-27 07:43:34', '2021-07-27 07:43:34'),
(12, 13, 'pi_1JHq88L8INgkSBj9RGGgwTV8', 'ch_1JHq88L8INgkSBj999kTWxHN', NULL, '0S0A20210727131532', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.32615, 104.35, 0, '2021-07-27 07:45:33', '2021-07-27 07:45:33'),
(13, 23, 'pi_1JI5ZSL8INgkSBj96ZZr1Mhq', 'ch_1JI5ZTL8INgkSBj90KdFp4LT', NULL, 'ADAD20210728054446', 'cus_JvxUb3DRtC3ECN', 'USD', 'card_1JI5ZKL8INgkSBj9X3l0ILcd', 3.34152, 104.88, 0, '2021-07-28 00:14:48', '2021-07-28 00:14:48'),
(14, 23, 'pi_1JI5abL8INgkSBj9Ngdqm7Rp', 'ch_1JI5abL8INgkSBj9hqVUToIy', NULL, 'ADDA20210728054556', 'cus_JvxUb3DRtC3ECN', 'USD', 'card_1JI5aTL8INgkSBj9NX9zAvDE', 2.1299, 63.1, 0, '2021-07-28 00:15:58', '2021-07-28 00:15:58'),
(15, 23, 'pi_1JI5b7L8INgkSBj9iHB6YOEo', 'ch_1JI5b7L8INgkSBj9IjIIq3rm', NULL, 'D0SA20210728054628', 'cus_JvxUb3DRtC3ECN', 'USD', 'card_1JI5ZKL8INgkSBj9X3l0ILcd', 2.2865, 68.5, 0, '2021-07-28 00:16:30', '2021-07-28 00:16:30'),
(16, 23, 'pi_1JI5c8L8INgkSBj9nyFmNB20', 'ch_1JI5c8L8INgkSBj96UYNMciU', NULL, '0AAA20210728054731', 'cus_JvxUb3DRtC3ECN', 'USD', 'card_1JI5aTL8INgkSBj9NX9zAvDE', 2.2865, 68.5, 0, '2021-07-28 00:17:33', '2021-07-28 00:17:33'),
(17, 24, 'pi_1JI6MJL8INgkSBj9iAz80j6r', 'ch_1JI6MJL8INgkSBj9veLIZ2n0', NULL, 'A0AS20210728063514', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 3.58106, 113.14, 2, '2021-07-28 01:05:16', '2021-07-28 01:14:29'),
(18, 24, 'pi_1JI6WPL8INgkSBj92n4CtOZy', 'ch_1JI6WPL8INgkSBj9S0bdvS0b', NULL, 'DAA020210728064540', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 6.55646, 215.74, 2, '2021-07-28 01:15:42', '2021-07-28 01:16:05'),
(19, 24, 'pi_1JI6X4L8INgkSBj9tViyjKs8', 'ch_1JI6X4L8INgkSBj9cwqGuFK4', NULL, 'ADAD20210728064622', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 6.55646, 215.74, 2, '2021-07-28 01:16:23', '2021-07-28 01:16:51'),
(20, 24, 'pi_1JI6YKL8INgkSBj9cK6V9QBr', 'ch_1JI6YLL8INgkSBj9TX9sawfe', NULL, 'SDAS20210728064740', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 6.55646, 215.74, 2, '2021-07-28 01:17:42', '2021-07-28 01:18:05'),
(21, 24, 'pi_1JI7QpL8INgkSBj9Xu2n9heI', 'ch_1JI7QqL8INgkSBj9Pv3KreZT', NULL, 'SDAD20210728074359', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 3.58106, 113.14, 2, '2021-07-28 02:14:01', '2021-07-28 02:15:45'),
(22, 24, 'pi_1JI7TwL8INgkSBj9f3ffA65j', 'ch_1JI7TwL8INgkSBj9oYvHYZjM', NULL, 'SA0S20210728074711', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 3.58106, 113.14, 2, '2021-07-28 02:17:13', '2021-07-28 02:17:34'),
(23, 24, 'pi_1JI7lxL8INgkSBj9kkI2iiLz', 'ch_1JI7lxL8INgkSBj9u9fsS7v6', NULL, 'DDD020210728080544', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 3.58106, 113.14, 0, '2021-07-28 02:35:51', '2021-07-28 02:35:51'),
(24, 24, 'pi_1JI7wHL8INgkSBj9xCtchc4A', 'ch_1JI7wIL8INgkSBj9u3V0BF49', NULL, 'AAAS20210728081629', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 6.55646, 215.74, 2, '2021-07-28 02:46:30', '2021-07-28 02:47:10'),
(25, 19, 'pi_1JI9rIL8INgkSBj9iLLllaVI', 'ch_1JI9rJL8INgkSBj9Yyg1rZzD', NULL, 'AS0020210728101928', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 2, '2021-07-28 04:49:30', '2021-07-28 04:50:08'),
(26, 19, 'pi_1JIA77L8INgkSBj9tlHCJyZq', 'ch_1JIA77L8INgkSBj9DM1aHfkC', NULL, 'SDA020210728103547', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 2, '2021-07-28 05:05:51', '2021-07-28 05:06:46'),
(27, 19, 'pi_1JIA8UL8INgkSBj9ZCqiD25F', 'ch_1JIA8UL8INgkSBj9j5Z94r4l', NULL, 'SDDD20210728103713', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 2, '2021-07-28 05:07:15', '2021-07-28 05:07:36'),
(28, 17, 'pi_1JID3HL8INgkSBj9iscFuWzg', 'ch_1JID3HL8INgkSBj9w3t1wwf7', NULL, '00A020210728134402', 'cus_Jw5BlVudZUzBZF', 'USD', 'card_1JID1ML8INgkSBj9qnLTMwZz', 3.29976, 103.44, 0, '2021-07-28 08:14:04', '2021-07-28 08:14:04'),
(29, 13, 'pi_1JIDBGL8INgkSBj9QIIBo9eG', 'ch_1JIDBHL8INgkSBj9AhjP1zSJ', NULL, 'DSDD20210728135218', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.32615, 104.35, 0, '2021-07-28 08:22:20', '2021-07-28 08:22:20'),
(30, 13, 'pi_1JIDD2L8INgkSBj9AA99n14W', 'ch_1JIDD3L8INgkSBj91WjuDADM', NULL, '0ADA20210728135408', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.32615, 104.35, 0, '2021-07-28 08:24:10', '2021-07-28 08:24:10'),
(31, 24, 'pi_1JIRGvL8INgkSBj9ihkvIIau', 'ch_1JIRGwL8INgkSBj9Gqe7sA2L', NULL, 'SA0A20210729045505', 'cus_JvyIxppGdSAiue', 'USD', 'card_1JI6MBL8INgkSBj9B1bJ71wK', 3.58106, 113.14, 0, '2021-07-28 23:25:07', '2021-07-28 23:25:07'),
(32, 13, 'pi_1JIRiXL8INgkSBj9kSCJ8C1x', 'ch_1JIRiXL8INgkSBj9BydBCrXX', NULL, '0DD020210729052337', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.35515, 105.35, 0, '2021-07-28 23:53:38', '2021-07-28 23:53:38'),
(33, 13, 'pi_1JIS8zL8INgkSBj9eTfdN5Sm', 'ch_1JIS8zL8INgkSBj9CwPeWrJF', NULL, '0SAD20210729055057', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 00:20:58', '2021-07-29 00:20:58'),
(34, 13, 'pi_1JIShEL8INgkSBj9b6ElrNOT', 'ch_1JIShEL8INgkSBj9vzmNI474', NULL, 'D00D20210729062620', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 00:56:22', '2021-07-29 00:56:22'),
(35, 13, 'pi_1JIShuL8INgkSBj93upNcUNZ', 'ch_1JIShvL8INgkSBj9TMxQfeOv', NULL, 'AD0D20210729062646', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 00:57:04', '2021-07-29 00:57:04'),
(36, 13, 'pi_1JISkZL8INgkSBj9Bq6fbvSE', 'ch_1JISkZL8INgkSBj9gNJMA7Ga', NULL, '00S020210729062941', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 00:59:48', '2021-07-29 00:59:48'),
(37, 13, 'pi_1JISnZL8INgkSBj9GlNPJr5B', 'ch_1JISnaL8INgkSBj9DGsKVjJK', NULL, '0A0D20210729063253', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 01:02:55', '2021-07-29 01:02:55'),
(38, 13, 'pi_1JISsSL8INgkSBj9YQT7MwlY', 'ch_1JISsSL8INgkSBj9KsYP5Ch2', NULL, 'DS0020210729063749', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 9.85927, 329.63, 0, '2021-07-29 01:07:57', '2021-07-29 01:07:57'),
(39, 13, 'pi_1JITu5L8INgkSBj9F6nnDlGR', 'ch_1JITu6L8INgkSBj9ZVrbpkSX', NULL, '0AAD20210729074341', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 02:13:43', '2021-07-29 02:13:43'),
(40, 13, 'pi_1JIULCL8INgkSBj9KPGnCjvQ', 'ch_1JIULCL8INgkSBj9sl2YJC8y', NULL, 'S0DA20210729081141', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 02:41:43', '2021-07-29 02:41:43'),
(41, 13, 'pi_1JIVjCL8INgkSBj9pulO3R64', 'ch_1JIVjCL8INgkSBj9K3AtvJrZ', NULL, 'DADS20210729094034', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 04:10:35', '2021-07-29 04:10:35'),
(42, 19, 'pi_1JIVssL8INgkSBj9iYUdHmJO', 'ch_1JIVssL8INgkSBj9XuDt1xgb', NULL, 'D00S20210729095033', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 04:20:35', '2021-07-29 04:20:35'),
(43, 13, 'pi_1JIVwEL8INgkSBj92zuze8sE', 'ch_1JIVwEL8INgkSBj9UCSjgC7m', NULL, '00SD20210729095401', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 04:24:03', '2021-07-29 04:24:03'),
(44, 1, 'pi_1JIW9NL8INgkSBj9ndnAxywJ', 'ch_1JIW9NL8INgkSBj9s6R9hsTb', NULL, '00SS20210729100736', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.46738, 109.22, 0, '2021-07-29 04:37:38', '2021-07-29 04:37:38'),
(45, 19, 'pi_1JIWRTL8INgkSBj9hqDTCMn1', 'ch_1JIWRTL8INgkSBj94lbiu41q', NULL, '0D0020210729102618', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 6.55182, 215.58, 0, '2021-07-29 04:56:20', '2021-07-29 04:56:20'),
(46, 19, 'pi_1JIWS5L8INgkSBj9NWDw3FBw', 'ch_1JIWS5L8INgkSBj9eWJUmvrv', NULL, 'DDSA20210729102656', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 6.55182, 215.58, 0, '2021-07-29 04:56:58', '2021-07-29 04:56:58'),
(47, 19, 'pi_1JIWnjL8INgkSBj9Sypx5jCX', 'ch_1JIWnkL8INgkSBj9E7bgmow3', NULL, 'AD0020210729104919', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 05:19:21', '2021-07-29 05:19:21'),
(48, 19, 'pi_1JIX5WL8INgkSBj9bgDJoybp', 'ch_1JIX5WL8INgkSBj9cfUHxz5z', NULL, 'D0AD20210729110741', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 05:37:43', '2021-07-29 05:37:43'),
(49, 19, 'pi_1JIX9HL8INgkSBj9orX24MWz', 'ch_1JIX9HL8INgkSBj9LSjC0XFr', NULL, 'AS0S20210729111134', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 05:41:36', '2021-07-29 05:41:36'),
(50, 19, 'pi_1JIXQZL8INgkSBj9RvS1yGPn', 'ch_1JIXQaL8INgkSBj9KQveM7Jb', NULL, 'A0A020210729112927', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 05:59:29', '2021-07-29 05:59:29'),
(51, 19, 'pi_1JIXSJL8INgkSBj93CnXfhwX', 'ch_1JIXSJL8INgkSBj9uad4WgmT', NULL, 'DS0D20210729113114', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 06:01:16', '2021-07-29 06:01:16'),
(52, 13, 'pi_1JIXeUL8INgkSBj9uYfmm85D', 'ch_1JIXeUL8INgkSBj9ZDFtWHpC', NULL, '0S0D20210729114350', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 06:13:52', '2021-07-29 06:13:52'),
(53, 13, 'pi_1JIXrzL8INgkSBj95yEcod9c', 'ch_1JIXrzL8INgkSBj9d8XIYJ3Y', NULL, 'DSDD20210729115746', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 06:27:48', '2021-07-29 06:27:48'),
(54, 19, 'pi_1JIXuTL8INgkSBj9A11KqdNz', 'ch_1JIXuTL8INgkSBj9ye8CGoQ3', NULL, '0AAS20210729120020', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 06:30:22', '2021-07-29 06:30:22'),
(55, 19, 'pi_1JIXzzL8INgkSBj9vRj9Bm0Z', 'ch_1JIXzzL8INgkSBj9dgWAWSTP', NULL, 'SDAD20210729120557', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-29 06:36:04', '2021-07-29 06:36:04'),
(56, 13, 'pi_1JIYLeL8INgkSBj9uLKry5Yi', 'ch_1JIYLfL8INgkSBj9n5SZ2qqv', NULL, 'DA0S20210729122825', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 06:58:28', '2021-07-29 06:58:28'),
(57, 13, 'pi_1JIYt4L8INgkSBj9Gu0taBXq', 'ch_1JIYt4L8INgkSBj9D3xKaUhb', NULL, 'DD0A20210729130257', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 4.02331, 128.39, 0, '2021-07-29 07:32:59', '2021-07-29 07:32:59'),
(58, 1, 'pi_1JIZbfL8INgkSBj9A8PKfiFg', 'ch_1JIZbfL8INgkSBj9Gk3L7S4i', NULL, 'S0DS20210729134903', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.46738, 109.22, 0, '2021-07-29 08:19:04', '2021-07-29 08:19:04'),
(59, 1, 'pi_1JInP2L8INgkSBj9giT20S9I', 'ch_1JInP2L8INgkSBj9UoeCFnBT', NULL, 'D0SS20210730043244', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-29 23:02:57', '2021-07-29 23:02:57'),
(60, 1, 'pi_1JInooL8INgkSBj9MHsK3Oc4', 'ch_1JInooL8INgkSBj9isKkUXg6', NULL, '0SSS20210730045923', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-29 23:29:35', '2021-07-29 23:29:35'),
(61, 1, 'pi_1JIoEpL8INgkSBj9FhSY22W6', 'ch_1JIoEpL8INgkSBj9uMwKAenV', NULL, '00DS20210730052626', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-29 23:56:28', '2021-07-29 23:56:28'),
(62, 1, 'pi_1JIoM9L8INgkSBj9ZFsvMhRU', 'ch_1JIoM9L8INgkSBj9uss3MEpV', NULL, 'ADD020210730053400', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 00:04:02', '2021-07-30 00:04:02'),
(63, 1, 'pi_1JIoUnL8INgkSBj9DlXTbRjC', 'ch_1JIoUnL8INgkSBj9BtasSzjg', NULL, 'A0AA20210730054256', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 2, '2021-07-30 00:12:58', '2021-07-30 00:13:25'),
(64, 1, 'pi_1JIoVfL8INgkSBj9CfqCtJFJ', 'ch_1JIoVfL8INgkSBj9Uy1Enio1', NULL, 'SDSS20210730054345', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 00:13:52', '2021-07-30 00:13:52'),
(65, 1, 'pi_1JIoyeL8INgkSBj9CzsyarpA', 'ch_1JIoyfL8INgkSBj9LBwmCiet', NULL, 'DA0D20210730061346', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 00:43:50', '2021-07-30 00:43:50'),
(66, 19, 'pi_1JIpoHL8INgkSBj9DOMyH66y', 'ch_1JIpoHL8INgkSBj9WC8AxVBA', NULL, 'SDSS20210730070708', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-30 01:37:10', '2021-07-30 01:37:10'),
(67, 19, 'pi_1JIpvJL8INgkSBj98rItZiKK', 'ch_1JIpvKL8INgkSBj9emhKUTXH', NULL, 'SAAS20210730071423', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-30 01:44:27', '2021-07-30 01:44:27'),
(68, 19, 'pi_1JIq0KL8INgkSBj9sCPj93fs', 'ch_1JIq0KL8INgkSBj90LGytz58', NULL, 'SASD20210730071935', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-30 01:49:37', '2021-07-30 01:49:37'),
(69, 19, 'pi_1JIqZAL8INgkSBj9NvXLE7a4', 'ch_1JIqZBL8INgkSBj9BuLXjqhM', NULL, 'D0SS20210730075536', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-07-30 02:25:37', '2021-07-30 02:25:37'),
(70, 19, 'pi_1JIqbgL8INgkSBj97KODhgTR', 'ch_1JIqbgL8INgkSBj91vNyPhWy', NULL, 'S0SS20210730075811', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 9.61422, 321.18, 0, '2021-07-30 02:28:13', '2021-07-30 02:28:13'),
(71, 42, 'pi_3JIqdqL8INgkSBj91kf4E8kc', 'ch_3JIqdqL8INgkSBj91Oo9R9EY', NULL, '0AAA20210730080025', 'cus_Jwk7ImgafDPHe6', 'USD', 'card_1JIqdaL8INgkSBj9dUQbkfaq', 3.58135, 113.15, 0, '2021-07-30 02:30:28', '2021-07-30 02:30:28'),
(72, 1, 'pi_3JIspoL8INgkSBj91iJJJ7Qw', 'ch_3JIspoL8INgkSBj91EekOJ9d', NULL, '00AS20210730102056', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 04:50:57', '2021-07-30 04:50:57'),
(73, 1, 'pi_1JIsxrL8INgkSBj9L5FoHFFF', 'ch_1JIsxrL8INgkSBj9WDlV14jF', NULL, 'DD0020210730102914', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 04:59:16', '2021-07-30 04:59:16'),
(74, 1, 'pi_1JItQdL8INgkSBj9wxd1qpjn', 'ch_1JItQeL8INgkSBj99Eo437cB', NULL, 'A0SA20210730105859', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 05:29:01', '2021-07-30 05:29:01'),
(75, 1, 'pi_1JItTUL8INgkSBj9XX9bKokv', 'ch_1JItTUL8INgkSBj9lrSSoWu9', NULL, 'SDSS20210730110155', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-07-30 05:31:57', '2021-07-30 05:31:57'),
(76, 42, 'pi_3JJBC9L8INgkSBj90USwwMhR', 'ch_3JJBC9L8INgkSBj90wZfDnJ0', NULL, '0AS020210731055713', 'cus_Jwk7ImgafDPHe6', 'USD', 'card_1JIqdaL8INgkSBj9dUQbkfaq', 3.58135, 113.15, 0, '2021-07-31 00:27:14', '2021-07-31 00:27:14'),
(77, 1, 'pi_3JJtwaL8INgkSBj91hCZva1x', 'ch_3JJtwaL8INgkSBj9193mqTzZ', NULL, 'DADS20210802054402', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.06022, 95.18, 0, '2021-08-02 00:14:10', '2021-08-02 00:14:10'),
(78, 1, 'pi_3JJtziL8INgkSBj91vAJCpx2', 'ch_3JJtziL8INgkSBj91hWjsAna', NULL, '00AS20210802054721', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-02 00:17:23', '2021-08-02 00:17:23'),
(79, 1, 'pi_3JJuhNL8INgkSBj914iAIuXl', 'ch_3JJuhNL8INgkSBj915GmcS1M', NULL, 'SDSA20210802063229', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-02 01:02:31', '2021-08-02 01:02:31'),
(80, 1, 'pi_3JJuobL8INgkSBj9157t1sPi', 'ch_3JJuobL8INgkSBj91fGc5NyY', NULL, '00DS20210802063957', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-02 01:09:58', '2021-08-02 01:09:58'),
(81, 1, 'pi_3JKGRWL8INgkSBj90AyhheFM', 'ch_3JKGRWL8INgkSBj90hdWzFZc', NULL, 'D0DA20210803054532', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 00:15:35', '2021-08-03 00:15:35'),
(82, 1, 'pi_3JKGbWL8INgkSBj90UEGtN8A', 'ch_3JKGbWL8INgkSBj9057ocCNc', NULL, '0SSA20210803055554', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 00:25:56', '2021-08-03 00:25:56'),
(83, 1, 'pi_3JKH8dL8INgkSBj91cP59esN', 'ch_3JKH8dL8INgkSBj91lCS0bIl', NULL, 'DSSS20210803063007', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 2.2865, 68.5, 0, '2021-08-03 01:00:09', '2021-08-03 01:00:09'),
(84, 1, 'pi_3JKHiGL8INgkSBj90mgRGOyV', 'ch_3JKHiGL8INgkSBj90EapgkcL', NULL, 'DA0020210803070656', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 01:36:57', '2021-08-03 01:36:57'),
(85, 1, 'pi_1JKHqNL8INgkSBj9A5SlpTxY', 'ch_1JKHqNL8INgkSBj9HuoN0I4b', NULL, 'DDAD20210803071518', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 01:45:20', '2021-08-03 01:45:20'),
(86, 1, 'pi_3JKHuRL8INgkSBj91fbO2xbm', 'ch_3JKHuRL8INgkSBj91ltyukIE', NULL, 'ADDA20210803071930', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 01:49:32', '2021-08-03 01:49:32'),
(87, 1, 'pi_3JKHzuL8INgkSBj91v5tsfpm', 'ch_3JKHzuL8INgkSBj91QaNmo1J', NULL, 'SSA020210803072509', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 01:55:11', '2021-08-03 01:55:11'),
(88, 1, 'pi_3JKI4XL8INgkSBj90YO0YzPD', 'ch_3JKI4XL8INgkSBj90vuk0N2o', NULL, 'ADAD20210803072956', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JHq69L8INgkSBj9zTAglwhd', 3.75738, 119.22, 0, '2021-08-03 01:59:58', '2021-08-03 01:59:58'),
(89, 1, 'pi_3JKMT2L8INgkSBj91zl1HgEU', 'ch_3JKMT2L8INgkSBj91cZJBtL4', NULL, '0SSS20210803121132', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JKMSUL8INgkSBj9c1eIAjmh', 2.2865, 68.5, 0, '2021-08-03 06:41:34', '2021-08-03 06:41:34'),
(90, 1, 'pi_3JKMY6L8INgkSBj91XlTJPox', 'ch_3JKMY6L8INgkSBj91WaUlbP2', NULL, 'SDD020210803121645', 'cus_JvhVvTuNhLn4n2', 'USD', 'card_1JKMSUL8INgkSBj9c1eIAjmh', 2.2865, 68.5, 0, '2021-08-03 06:46:47', '2021-08-03 06:46:47'),
(91, 1, 'pi_3JKMbmL8INgkSBj91evVBRgB', 'ch_3JKMbmL8INgkSBj91EJe7MBh', NULL, 'SS0D20210803122034', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 2.2865, 68.5, 0, '2021-08-03 06:50:35', '2021-08-03 06:50:35'),
(92, 1, 'pi_3JKMfjL8INgkSBj91YXbIX40', 'ch_3JKMfjL8INgkSBj91UYvBR7W', NULL, '0DA020210803122439', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-03 06:54:40', '2021-08-03 06:54:40'),
(93, 1, 'pi_3JKMteL8INgkSBj91zt8906y', 'ch_3JKMteL8INgkSBj91exgq6ov', NULL, 'DA0D20210803123901', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-03 07:09:03', '2021-08-03 07:09:03'),
(94, 1, 'pi_3JKN1IL8INgkSBj90MLPmyfP', 'ch_3JKN1IL8INgkSBj90Vz1m8ql', NULL, 'ADD020210803124655', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-03 07:16:57', '2021-08-03 07:16:57'),
(95, 1, 'pi_1JKNBKL8INgkSBj9X1u9umQ6', 'ch_1JKNBKL8INgkSBj99HowSm6P', NULL, 'DDDD20210803125717', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.06022, 95.18, 0, '2021-08-03 07:27:19', '2021-08-03 07:27:19'),
(96, 1, 'pi_3JKNE5L8INgkSBj901m6lEbA', 'ch_3JKNE5L8INgkSBj9099F1sme', NULL, 'DAAA20210803130008', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-03 07:30:10', '2021-08-03 07:30:10'),
(97, 1, 'pi_3JKNKsL8INgkSBj90yWnYzAi', 'ch_3JKNKsL8INgkSBj90D7acpkj', NULL, 'S0DS20210803130709', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.06022, 95.18, 0, '2021-08-03 07:37:11', '2021-08-03 07:37:11'),
(98, 19, 'pi_3JKcm5L8INgkSBj91jjYXYlL', 'ch_3JKcm5L8INgkSBj91mWYtTOe', NULL, 'SAAD20210804053617', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-08-04 00:06:19', '2021-08-04 00:06:19'),
(99, 19, 'pi_3JKdkjL8INgkSBj90M723dlO', 'ch_3JKdkjL8INgkSBj908TpXwhj', NULL, 'AD0020210804063856', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-08-04 01:08:58', '2021-08-04 01:08:58'),
(100, 1, 'pi_3JKeh9L8INgkSBj90dZrw0Pl', 'ch_3JKeh9L8INgkSBj90W91YnlX', NULL, 'DA0S20210804073912', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-04 02:09:20', '2021-08-04 02:09:20'),
(101, 19, 'pi_3JKfE7L8INgkSBj90ebz7nul', 'ch_3JKfE7L8INgkSBj90V5fOvEN', NULL, 'D0AD20210804081322', 'cus_JvhAjrNeDljYGf', 'USD', 'card_1JHpmGL8INgkSBj970O4g8yG', 3.57642, 112.98, 0, '2021-08-04 02:43:24', '2021-08-04 02:43:24'),
(102, 1, 'pi_3JKhEcL8INgkSBj90Xdg15Fd', 'ch_3JKhEcL8INgkSBj90oN0fUV1', NULL, 'ASDD20210804102201', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-04 04:52:03', '2021-08-04 04:52:03'),
(103, 1, 'pi_3JKhgoL8INgkSBj91rP5VYlz', 'ch_3JKhgoL8INgkSBj91qSPfR85', NULL, 'A0DS20210804105109', 'cus_JvK0CkAAopHfWA', 'USD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.51842, 110.98, 0, '2021-08-04 05:21:11', '2021-08-04 05:21:11'),
(104, 1, 'pi_3JKiRYL8INgkSBj90dtnRt1H', 'ch_3JKiRYL8INgkSBj90bFujvfl', NULL, 'DADD20210804113921', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 0, 97.18, 0, '2021-08-04 06:09:29', '2021-08-04 06:09:29'),
(105, 1, 'pi_3JKiSiL8INgkSBj90BMbN3rc', 'ch_3JKiSiL8INgkSBj907UpTNYS', NULL, 'D0AA20210804114034', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 0, 119.22, 0, '2021-08-04 06:10:41', '2021-08-04 06:10:41'),
(106, 1, 'pi_3JKiwoL8INgkSBj91wnLz5K4', 'ch_3JKiwoL8INgkSBj91BHIlIgt', NULL, '0DAS20210804121146', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 0, 47.48, 0, '2021-08-04 06:41:47', '2021-08-04 06:41:47'),
(107, 1, 'pi_3JKkMWL8INgkSBj91z1cFi52', 'ch_3JKkMWL8INgkSBj91AVv4She', NULL, 'DA0S20210804134223', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 0, 119.22, 0, '2021-08-04 08:12:25', '2021-08-04 08:12:25'),
(108, 1, 'pi_3JL1r4L8INgkSBj90eqAwMXy', 'ch_3JL1r4L8INgkSBj900DKIqtr', NULL, 'SSD020210805082305', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 0, 119.22, 0, '2021-08-05 02:53:07', '2021-08-05 02:53:07'),
(109, 1, 'pi_3JL20LL8INgkSBj90QUBLgS9', 'ch_3JL20LL8INgkSBj901uKqHC8', NULL, 'A0AD20210805083241', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-05 03:02:42', '2021-08-05 03:02:42'),
(110, 1, 'pi_3JL3NcL8INgkSBj91Tvub3Vl', 'ch_3JL3NcL8INgkSBj91cLcQi5j', NULL, 'D0DS20210805100047', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-05 04:30:49', '2021-08-05 04:30:49'),
(111, 1, 'pi_3JL3Y0L8INgkSBj90vnWlY5d', 'ch_3JL3Y0L8INgkSBj90WyRyNWS', NULL, '0DS020210805101132', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.75738, 119.22, 0, '2021-08-05 04:41:34', '2021-08-05 04:41:34'),
(112, 58, 'pi_3JNDhpL8INgkSBj90pWTTUJa', 'ch_3JNDhpL8INgkSBj90pVihmpq', NULL, 'D0SA20210811092637', 'cus_K1GEuauxTnMP4s', 'CAD', 'card_1JNDheL8INgkSBj9L9fgNu0C', 3.59237, 113.53, 2, '2021-08-11 03:56:38', '2021-08-11 03:58:22'),
(113, 1, 'pi_3JNvD3L8INgkSBj91mQqiuoP', 'ch_3JNvD3L8INgkSBj91ELQJSj9', NULL, 'AS0A20210813075345', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 3.06022, 95.18, 0, '2021-08-13 02:23:47', '2021-08-13 02:23:47'),
(114, 1, 'pi_3JPmM4L8INgkSBj90Jm7Oo7m', 'ch_3JPmM4L8INgkSBj903sBCi2F', NULL, 'DDAD20210818105043', 'cus_JvK0CkAAopHfWA', 'CAD', 'card_1JHTM0L8INgkSBj9fQUVAYq7', 4.11553, 131.57, 0, '2021-08-18 05:20:45', '2021-08-18 05:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_token`
--

CREATE TABLE `stripe_token` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `stripe_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `table_banners_location_wise`
--

CREATE TABLE `table_banners_location_wise` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bannerName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `table_banners_location_wise`
--

INSERT INTO `table_banners_location_wise` (`id`, `bannerName`, `link`, `address`, `latitude`, `longitude`, `created_at`, `updated_at`, `merchant_id`) VALUES
(1, 'Coffee banner', '', 'Mansa, Punjab 151505, India', '29.9995069', '75.3936808', '2021-07-22 06:09:09', '2021-07-22 06:09:09', NULL),
(2, 'Wine', '', NULL, NULL, NULL, '2021-07-22 06:28:05', '2021-07-22 06:28:05', 267);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '*',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `namespace`, `group`, `key`, `text`, `metadata`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'brackets/admin-ui', 'admin', 'operation.succeeded', '{\"en\":\"Operation successful\"}', NULL, '2020-12-18 02:52:10', '2020-12-22 04:31:20', NULL),
(2, 'brackets/admin-ui', 'admin', 'operation.failed', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(3, 'brackets/admin-ui', 'admin', 'operation.not_allowed', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(4, '*', 'admin', 'admin-user.columns.first_name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(5, '*', 'admin', 'admin-user.columns.last_name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(6, '*', 'admin', 'admin-user.columns.email', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(7, '*', 'admin', 'admin-user.columns.Phone_number_with_country_code', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(8, '*', 'admin', 'admin_user.columns.Country_Iso_Code', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(9, '*', 'admin', 'admin-user.columns.Country_Iso_Code', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(10, '*', 'admin', 'admin-user.columns.password', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(11, '*', 'admin', 'admin-user.columns.password_repeat', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(12, '*', 'admin', 'admin-user.columns.activated', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(13, '*', 'admin', 'admin-user.columns.forbidden', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(14, '*', 'admin', 'vendor.columns.Shop_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(15, '*', 'admin', 'vendor.columns.Shop_Address', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(16, '*', 'admin', 'admin-user.columns.language', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(17, 'brackets/admin-ui', 'admin', 'forms.select_an_option', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(18, '*', 'admin', 'admin-user.columns.roles', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(19, 'brackets/admin-ui', 'admin', 'forms.select_options', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(20, '*', 'admin', 'admin-user.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(21, '*', 'admin', 'vendor.columns.Gallery', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(22, 'brackets/admin-ui', 'admin', 'btn.save', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(23, 'brackets/admin-ui', 'admin', 'btn.cancel', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(24, '*', 'admin', 'admin-user.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(25, '*', 'admin', 'admin-user.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(26, 'brackets/admin-ui', 'admin', 'placeholder.search', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(27, 'brackets/admin-ui', 'admin', 'btn.search', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(28, '*', 'admin', 'admin-user.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(29, '*', 'admin', 'admin-user.columns.last_login_at', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(30, 'brackets/admin-ui', 'admin', 'btn.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(31, 'brackets/admin-ui', 'admin', 'btn.delete', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(32, 'brackets/admin-ui', 'admin', 'pagination.overview', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(33, 'brackets/admin-ui', 'admin', 'index.no_items', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(34, 'brackets/admin-ui', 'admin', 'index.try_changing_items', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(35, 'brackets/admin-ui', 'admin', 'btn.new', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(36, '*', 'admin', 'category.columns.Category_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(37, '*', 'admin', 'category.columns.Category_Description', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(38, '*', 'admin', 'category.columns.Profile_Picture', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(39, '*', 'admin', 'category.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(40, '*', 'admin', 'category.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(41, '*', 'admin', 'category.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(42, '*', 'admin', 'category.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(43, 'brackets/admin-ui', 'admin', 'listing.selected_items', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(44, 'brackets/admin-ui', 'admin', 'listing.check_all_items', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(45, 'brackets/admin-ui', 'admin', 'listing.uncheck_all_items', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(46, '*', 'admin', 'company.columns.Company_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(47, '*', 'admin', 'company.columns.Company_Description', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(48, '*', 'admin', 'company.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(49, '*', 'admin', 'company.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(50, '*', 'admin', 'company.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(51, '*', 'admin', 'company.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(52, '*', 'admin', 'product.columns.Category_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(53, '*', 'admin', 'product.columns.Company_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(54, '*', 'admin', 'item.columns.Product_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(55, '*', 'admin', 'item.columns.Product_Description', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(56, '*', 'admin', 'item.columns.Stock_Quantity', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(57, '*', 'admin', 'item.columns.Price', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(58, '*', 'admin', 'item.columns.Product_Images', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(59, '*', 'admin', 'item.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(60, '*', 'admin', 'item.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(61, '*', 'admin', 'item.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(62, '*', 'admin', 'item.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(63, 'brackets/admin-ui', 'admin', 'profile_dropdown.account', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(64, 'brackets/admin-auth', 'admin', 'profile_dropdown.profile', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(65, 'brackets/admin-auth', 'admin', 'profile_dropdown.password', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(66, 'brackets/admin-auth', 'admin', 'profile_dropdown.logout', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(67, 'brackets/admin-ui', 'admin', 'sidebar.content', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(68, '*', 'admin', 'category.title', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(69, '*', 'admin', 'vendor.title', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(70, '*', 'admin', 'company.title', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(71, '*', 'admin', 'product.title', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(72, '*', 'admin', 'item.title', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(73, 'brackets/admin-ui', 'admin', 'sidebar.settings', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(74, '*', 'admin', 'product.columns.company', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(75, '*', 'admin', 'product.columns.Product_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(76, '*', 'admin', 'product.columns.Product_Description', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(77, '*', 'admin', 'product.columns.Stock_Quantity', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(78, '*', 'admin', 'product.columns.Price', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(79, '*', 'admin', 'product.columns.Product_Images', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(80, '*', 'admin', 'product.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(81, '*', 'admin', 'product.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(82, '*', 'admin', 'product.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(83, '*', 'admin', 'product.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(84, '*', 'admin', 'admin-user.actions.edit_password', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(85, '*', 'admin', 'admin-user.actions.edit_profile', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(86, '*', 'admin', 'admin-user.columns.profile_picture', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(87, '*', 'admin', 'vendor.columns.Vendor_Name', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(88, '*', 'admin', 'vendor.columns.Email', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(89, '*', 'admin', 'vendor.columns.Profile_Picture', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(90, '*', 'admin', 'vendor.columns.Phone_number_with_country_code', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(91, '*', 'admin', 'vendor.columns.Password', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(92, '*', 'admin', 'vendor.actions.create', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(93, '*', 'admin', 'vendor.actions.edit', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(94, '*', 'admin', 'vendor.actions.index', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(95, '*', 'admin', 'vendor.columns.id', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(96, 'brackets/admin-auth', 'activations', 'email.line', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(97, 'brackets/admin-auth', 'activations', 'email.action', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(98, 'brackets/admin-auth', 'activations', 'email.notRequested', '[]', NULL, '2020-12-18 02:52:10', '2020-12-18 02:52:10', NULL),
(99, 'brackets/admin-auth', 'admin', 'activations.activated', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(100, 'brackets/admin-auth', 'admin', 'activations.invalid_request', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(101, 'brackets/admin-auth', 'admin', 'activations.disabled', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(102, 'brackets/admin-auth', 'admin', 'activations.sent', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(103, 'brackets/admin-auth', 'admin', 'passwords.sent', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(104, 'brackets/admin-auth', 'admin', 'passwords.reset', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(105, 'brackets/admin-auth', 'admin', 'passwords.invalid_token', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(106, 'brackets/admin-auth', 'admin', 'passwords.invalid_user', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(107, 'brackets/admin-auth', 'admin', 'passwords.invalid_password', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(108, 'brackets/admin-auth', 'resets', 'email.line', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(109, 'brackets/admin-auth', 'resets', 'email.action', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(110, 'brackets/admin-auth', 'resets', 'email.notRequested', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(111, '*', 'auth', 'failed', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(112, '*', 'auth', 'throttle', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(113, 'brackets/admin-auth', 'admin', 'activation_form.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(114, 'brackets/admin-auth', 'admin', 'activation_form.note', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(115, 'brackets/admin-auth', 'admin', 'auth_global.email', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(116, 'brackets/admin-auth', 'admin', 'activation_form.button', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(117, 'brackets/admin-auth', 'admin', 'login.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(118, 'brackets/admin-auth', 'admin', 'login.sign_in_text', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(119, 'brackets/admin-auth', 'admin', 'auth_global.password', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(120, 'brackets/admin-auth', 'admin', 'login.button', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(121, 'brackets/admin-auth', 'admin', 'login.forgot_password', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(122, 'brackets/admin-auth', 'admin', 'forgot_password.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(123, 'brackets/admin-auth', 'admin', 'forgot_password.note', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(124, 'brackets/admin-auth', 'admin', 'forgot_password.button', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(125, 'brackets/admin-auth', 'admin', 'password_reset.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(126, 'brackets/admin-auth', 'admin', 'password_reset.note', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(127, 'brackets/admin-auth', 'admin', 'auth_global.password_confirm', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(128, 'brackets/admin-auth', 'admin', 'password_reset.button', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(129, 'brackets/admin-ui', 'admin', 'media_uploader.max_number_of_files', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(130, 'brackets/admin-ui', 'admin', 'media_uploader.max_size_pre_file', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(131, 'brackets/admin-ui', 'admin', 'media_uploader.private_title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(132, 'brackets/admin-ui', 'admin', 'page_title_suffix', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(133, 'brackets/admin-ui', 'admin', 'footer.powered_by', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(134, 'brackets/admin-translations', 'admin', 'title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(135, 'brackets/admin-translations', 'admin', 'index.all_groups', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(136, 'brackets/admin-translations', 'admin', 'index.edit', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(137, 'brackets/admin-translations', 'admin', 'index.default_text', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(138, 'brackets/admin-translations', 'admin', 'index.translation', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(139, 'brackets/admin-translations', 'admin', 'index.translation_for_language', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(140, 'brackets/admin-translations', 'admin', 'import.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(141, 'brackets/admin-translations', 'admin', 'import.notice', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(142, 'brackets/admin-translations', 'admin', 'import.upload_file', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(143, 'brackets/admin-translations', 'admin', 'import.choose_file', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(144, 'brackets/admin-translations', 'admin', 'import.language_to_import', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(145, 'brackets/admin-translations', 'admin', 'fields.select_language', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(146, 'brackets/admin-translations', 'admin', 'import.do_not_override', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(147, 'brackets/admin-translations', 'admin', 'import.conflict_notice_we_have_found', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(148, 'brackets/admin-translations', 'admin', 'import.conflict_notice_translations_to_be_imported', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(149, 'brackets/admin-translations', 'admin', 'import.conflict_notice_differ', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(150, 'brackets/admin-translations', 'admin', 'fields.group', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(151, 'brackets/admin-translations', 'admin', 'fields.default', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(152, 'brackets/admin-translations', 'admin', 'fields.current_value', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(153, 'brackets/admin-translations', 'admin', 'fields.imported_value', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(154, 'brackets/admin-translations', 'admin', 'import.sucesfully_notice', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(155, 'brackets/admin-translations', 'admin', 'import.sucesfully_notice_update', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(156, 'brackets/admin-translations', 'admin', 'index.export', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(157, 'brackets/admin-translations', 'admin', 'export.notice', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(158, 'brackets/admin-translations', 'admin', 'export.language_to_export', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(159, 'brackets/admin-translations', 'admin', 'btn.export', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(160, 'brackets/admin-translations', 'admin', 'index.title', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(161, 'brackets/admin-translations', 'admin', 'btn.import', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(162, 'brackets/admin-translations', 'admin', 'btn.re_scan', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(163, 'brackets/admin-translations', 'admin', 'fields.created_at', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(164, 'brackets/admin-translations', 'admin', 'index.no_items', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(165, 'brackets/admin-translations', 'admin', 'index.try_changing_items', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(166, '*', '*', 'Vendor', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(167, '*', '*', 'Translations', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(168, '*', '*', 'Configuration', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL),
(169, '*', '*', 'Close', '[]', NULL, '2020-12-18 02:52:11', '2020-12-18 02:52:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile_picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_iso_code` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` int(11) NOT NULL,
  `is_block` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_connect_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `stripe_customer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `availability` int(11) NOT NULL COMMENT '0 > availability is off  ,1> availability is on ',
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referred_by_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title`, `name`, `social_id`, `social_type`, `email`, `email_verified_at`, `profile_picture`, `password`, `country_code`, `phone_number`, `country_iso_code`, `verified`, `remember_token`, `created_at`, `updated_at`, `user_type`, `is_block`, `stripe_connect_id`, `stripe_customer_id`, `availability`, `date_of_birth`, `referral_code`, `referred_by_code`) VALUES
(1, NULL, 'Test Customers', NULL, NULL, 'testshalini7@gmail.com', NULL, NULL, '$2y$10$7MqQuaDaoN51f8ho7Kw/lONwhaWQN6PNJW0Q0dgzVxo7mAfpY3IdW', '+1', '8485868486', 'CA', 1, NULL, '2021-07-22 06:07:46', '2021-08-13 01:58:38', 1, 0, '0', 'cus_JvK0CkAAopHfWA', 0, NULL, '3CD6ZL', NULL),
(2, NULL, 'Test Driver', NULL, NULL, NULL, NULL, NULL, '$2y$10$fJLAhfXIG9oqdCSb1.wnVOXiz4KZLlHhX6SXLk4njyAhzDLsG3g4S', '+1', '5152535153', 'CA', 1, NULL, '2021-07-22 06:35:47', '2021-08-03 01:57:18', 2, 0, '0', 'acct_1JHTimC4rXMgcfeT', 0, NULL, 'UKOFV4', NULL),
(3, NULL, 'Test Driver Two', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1627542011jpg', '$2y$10$NsLrP7NNpYmRAZBgVPIkh.UkIBATkR9hp8NsMr/eDpxXCeZoVy.pa', '+1', '5152535456', 'CA', 1, NULL, '2021-07-22 06:52:37', '2021-08-03 01:54:35', 2, 0, '0', 'acct_1JGOi1BSjZqJK4et', 0, NULL, 'PKFN2A', NULL),
(4, NULL, 'shalini', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1629376933jpg', '$2y$10$o7ZaKyTwMhsh/lOtwQIcre13F6rAPii8gSdgp5hQIrvnpMqRAmTCW', '+1', '8979908832', 'CA', 1, NULL, '2021-07-23 00:24:36', '2021-08-19 07:12:14', 1, 0, '0', 'jhdjkh', 0, NULL, 'XCI10L', NULL),
(5, 'Mr.', 'shivamsa', NULL, NULL, 'adminupmart@yopmail.com', NULL, NULL, '$2y$10$xOX9n9CciZx./uMiSeogfO7pIofkQoSqWk81eafHnT4eAtnzAk8bq', '+1', '9501648766', 'CA', 1, NULL, '2021-07-23 02:29:25', '2021-08-11 04:07:03', 1, 0, '0', 'cus_JuBL1o0zeB883g', 0, NULL, '6IXK34', '50T2S8'),
(6, NULL, 'Fggg', NULL, NULL, NULL, NULL, NULL, '$2y$10$U/pwyeSkBanABXIsmB3AquAomV8dfjryg/Rvjn.9bx3MwKaTUw.iu', '+1', '9696969696', 'CA', 1, NULL, '2021-07-23 08:00:41', '2021-08-03 07:17:26', 2, 0, '0', 'acct_1JGOi1BSjZqJK4et', 0, NULL, 'R1DPHY', NULL),
(7, NULL, 'Gsgaysu', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1627283015jpg', '$2y$10$eZXDjuHV5WkTpOWZTxR3F.q3UYSymSe9Q5KLojqVdrZpZsq7FC3om', '+1', '8989898989', 'CA', 1, NULL, '2021-07-26 00:55:44', '2021-08-04 05:26:06', 2, 0, '0', 'acct_1JJwUPQ1L9hdFDFV', 1, NULL, 'ULSZFW', NULL),
(8, NULL, 'Shivam', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1627282896jpg', '$2y$10$3Xs2t0LYrqLd5SIV/q0r.OivVVOOQQ7Gbzxet1R2kOnF4wAklOH42', '+1', '9501648707', 'CA', 1, NULL, '2021-07-26 01:30:47', '2021-07-26 01:31:38', 2, 0, '0', 'acct_1JJwUPQ1L9hdFDFV1', 0, NULL, 'F92S0P', NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$W//hpu4EjePesRgYQPVAtef.t6ac8vJ.McClxzqRNKQmyFMBZHN1u', '+91', '1626847108', NULL, 1, NULL, '2021-07-26 06:50:06', '2021-07-26 06:50:06', 1, 0, '0', '0', 0, NULL, 'TY75YR', NULL),
(12, NULL, 'Test Custmer', NULL, NULL, NULL, NULL, NULL, '$2y$10$TCr9G3layfqu2.9v9gZoFOl0bJ8NW59Od/o0PLirjKhnErsjj5/VO', '+1', '1626420322', 'CA', 1, NULL, '2021-07-26 07:04:43', '2021-07-26 07:26:22', 1, 0, '0', 'cus_JvK0CkAAopHfWAa', 0, NULL, '6VKLJ1', NULL),
(13, NULL, 'First', NULL, NULL, NULL, NULL, NULL, '$2y$10$qTj2R2z1qr8FWbnri3fQN.K.KLZYW9txiOtVLBnaIJ0FnNcdFa61.', '+1', '8182838183', 'CA', 1, NULL, '2021-07-27 01:58:49', '2021-07-27 07:43:31', 1, 0, '0', 'cus_JvhVvTuNhLn4n2', 0, NULL, '38KPIE', NULL),
(14, NULL, 'Second', NULL, NULL, NULL, NULL, NULL, '$2y$10$PDvr.E/whErwQ4QdMZJiXuTk8dO.btbdcAo6./XIQWa/H3aTaUVxG', '+1', '8182838184', 'CA', 1, NULL, '2021-07-27 01:59:34', '2021-07-27 01:59:41', 1, 0, '0', '0', 0, NULL, '08O0NQ', NULL),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$dm0kUiM4F9Cr7kZGHztoG.Vd3LajqGaDrRSXEZYsWLzfAqNrfG3ge', '+1', '9501648709', 'US', 1, NULL, '2021-07-27 04:09:06', '2021-07-27 04:09:06', 1, 0, '0', '0', 0, NULL, 'GSSBET', NULL),
(16, NULL, 'Dff', NULL, NULL, 'developer@upmart.caa', NULL, NULL, '$2y$10$J9ppr7MPPaGCa6j69n3IieZwbCiCrUbVkS9b0.wV0lVyFDHSj5W32', '+1', '8182838483', 'CA', 1, NULL, '2021-07-27 04:33:31', '2021-07-27 04:33:37', 1, 0, '0', '0', 0, NULL, 'N8HDGR', NULL),
(17, NULL, 'Shalini Test', '114508697748940779539', '2', '', NULL, 'https://lh3.googleusercontent.com/a/AATXAJxXeHppYIKDxcTXlgt82Kw-Kh01YApmVfuNNK2Q=s96-c', '$2y$10$vrZkrLvPyb1oN2QzkWtq9OUeI5m5Az23KJKvQ7Hvq.k0u2zLhNDPC', NULL, NULL, NULL, 1, NULL, '2021-07-27 05:13:57', '2021-07-28 08:12:06', 1, 0, '0', 'cus_Jw5BlVudZUzBZF', 0, NULL, 'T05IUN', NULL),
(19, NULL, 'Shalini', NULL, NULL, 'adminupmart1@yopmail.com', NULL, NULL, '$2y$10$QaYE8QMt456gjPebuTsm4uOEwJihBluoAS6k7TBKNgvamnRlHWmJK', '+1', '8630901347', 'CA', 1, NULL, '2021-07-27 07:21:06', '2021-08-10 23:59:21', 1, 0, '0', 'cus_JvhAjrNeDljYGf', 0, NULL, 'KG6J6Z', NULL),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$lEbE2/tIRofPs3UDC/g7GuYqgdF82WWq8qsS37zyAIGcjRkZbTvlW', '+1', '5152535154', 'US', 1, NULL, '2021-07-27 07:36:38', '2021-07-27 07:36:38', 1, 0, '0', '0', 0, NULL, 'HD3Q87', NULL),
(22, NULL, 'Nens', NULL, NULL, NULL, NULL, NULL, '$2y$10$JS97tzeBrfU0G2cGrEDFl.rfzEm/CaI4GC7ro0oMf2vAUV3PGV.iu', '+1', '8182838486', 'CA', 1, NULL, '2021-07-27 23:37:02', '2021-07-27 23:37:57', 1, 0, '0', '0', 0, NULL, 'V79EL8', 'Kg6j6z'),
(23, NULL, 'Testc', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1627450787jpg', '$2y$10$AJoO1rXemYdtaTaUBCq1Ter4GI.RuGvziJd7SvT3oCqm0gRIoC.GO', '+1', '8182838487', 'CA', 1, NULL, '2021-07-27 23:41:36', '2021-07-28 00:14:44', 1, 0, '0', 'cus_JvxUb3DRtC3ECN', 0, NULL, 'G8SSX1', 'V79EL8'),
(24, NULL, 'Nagarjun', '126149373045228', '2', 'developer@upmart.ca', NULL, 'https://scontent.fdel27-3.fna.fbcdn.net/v/t1.30497-1/p200x200/84628273_176159830277856_972693363922829312_n.jpg?_nc_cat=1&ccb=1-3&_nc_sid=12b3be&_nc_ohc=5kKxotHSP64AX8MX0WH&_nc_oc=AQmrJ2rM0g6WjUNPVK0uxzzy6dESBIlNCn9krx-5N5AI7lqNAu5G2qoZu_e9fctUavg&_nc_ht=', '$2y$10$J76UMRDKkjvEuqvQmCF4f.n9nSnRxlbNpxsVmYfB6RL5a0JNOz4si', '+91', '9192939193', 'IN', 1, NULL, '2021-07-27 23:56:13', '2021-07-28 08:03:22', 1, 0, '0', 'cus_JvyIxppGdSAiue', 0, NULL, 'NLJ7DD', NULL),
(26, NULL, 'shalini mishra', '116424497323267992367', '2', 'shalini.intersoft@gmail.com', NULL, 'https://lh3.googleusercontent.com/a/AATXAJwNJtRjyHHa46WL0Rvouj2kxs7PFoOzp7pBVH8M=s96-c', '$2y$10$1sV5QDg17mJjh.6njpMOSuSA8TpcPB5bTeQtI1/AW.De/sVnTd0D6', NULL, NULL, NULL, 1, NULL, '2021-07-28 06:19:11', '2021-07-28 06:19:11', 1, 0, '0', '0', 0, NULL, 'WRLSUY', NULL),
(27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$3v1ywTgYATaVdDGCugT3yOeJ/iE5bEFQlYWvUL0DyFz9PZG6xL5S6', '+91', '1627475605', NULL, 1, NULL, '2021-07-28 07:03:23', '2021-07-28 07:03:23', 2, 0, '0', '0', 0, NULL, 'G48IYQ', NULL),
(28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$6KpL6xD9kwgLmmRikDS/mO2goEHmHDNbXeIhA1whyuS5j3nCZbbw2', '+91', '1627475606', NULL, 1, NULL, '2021-07-28 07:03:24', '2021-07-28 07:03:25', 2, 0, '0', '0', 0, NULL, 'L94X55', 'G48IYQ'),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$u7lLtC2S1NcEEW1wcXddfO5Y3oQX7CD5aSgF67OxAoSEV9Wnx6Ly2', '+91', '1627476335', NULL, 1, NULL, '2021-07-28 07:15:33', '2021-07-28 07:15:33', 2, 0, '0', '0', 0, NULL, 'O1QKEY', NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$AwGUoeIQHWZ7TXZG.Sxq3.RoAPZUAU1YnRNrXb9AmlKFRyt.t762K', '+91', '1627476336', NULL, 1, NULL, '2021-07-28 07:15:34', '2021-07-28 07:15:34', 2, 0, '0', '0', 0, NULL, 'KQD4Y2', 'O1QKEY'),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Vt5eYKmaIXXB438X3wUEJOkUGm5/JloDBsmPiK5Pz.kPg6kGuUjc6', '+91', '1627476971', NULL, 1, NULL, '2021-07-28 07:26:09', '2021-07-28 07:26:09', 2, 0, '0', '0', 0, NULL, 'DTL16V', NULL),
(32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$4/eVyDtmF2KCgpFJXMh8x.RypRJwbBFEhMG9e.RpNp54B4tAEaBQO', '+91', '1627476973', NULL, 1, NULL, '2021-07-28 07:26:10', '2021-07-28 07:26:11', 2, 0, '0', '0', 0, NULL, 'DD7U3X', 'DTL16V'),
(33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$5.XaL1MDpWeO9vQIXKHHau./OBiF8gvYeGdCnIl/rQX4PJlEA.dOa', '+91', '1627477054', NULL, 1, NULL, '2021-07-28 07:27:32', '2021-07-28 07:27:32', 2, 0, '0', '0', 0, NULL, 'P63T6L', NULL),
(34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$NGVDq7wsbDXWEPtwidrvAOpWpDWnANFzFeeaOkCjUUZOCpjhY3fta', '+91', '1627477056', NULL, 1, NULL, '2021-07-28 07:27:33', '2021-07-28 07:27:34', 2, 0, '0', '0', 0, NULL, 'Q1XFOW', 'P63T6L'),
(35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$u2CAFObLYcuFkIwWscqiZOlacvpfP/Q8wOM.YEqBRDSA/INuRPzP6', '+91', '1627477205', NULL, 1, NULL, '2021-07-28 07:30:04', '2021-07-28 07:30:04', 2, 0, '0', '0', 0, NULL, 'XRCR2A', NULL),
(36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$v5ZDif/io0mWVKVk7CLgBeZuz2Yno2MVEuCa4RXDTqYPpvKipAYy.', '+91', '1627477208', NULL, 1, NULL, '2021-07-28 07:30:06', '2021-07-28 07:30:06', 2, 0, '0', '0', 0, NULL, '54049J', 'XRCR2A'),
(37, NULL, 'Djjd', NULL, NULL, NULL, NULL, NULL, '$2y$10$1SVMUx7l6oV9ztEXFHfdLejIOr6kYcPR4HQCDOYJx/d3NMPJqyvl6', '+1', '1627477208', 'CA', 1, NULL, '2021-07-28 07:35:49', '2021-07-28 07:36:19', 1, 0, '0', '0', 0, NULL, 'GSYTX8', 'XRCR2A'),
(38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$nwsZMkVJKxLV14PuDxMkneTyChzroiPvA6A7Rd5RVh5jn7mGOCLg6', '+1', '1627477812', NULL, 1, NULL, '2021-07-28 07:40:10', '2021-07-28 07:40:10', 2, 0, '0', '0', 0, NULL, 'NC24RM', NULL),
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$ASWhV81s1ysdHNd2ezupc.AMQNR./m/jHRoyyUU91kclNsA/StGuq', '+1', '1627477813', NULL, 1, NULL, '2021-07-28 07:40:11', '2021-07-28 07:40:12', 2, 0, '0', '0', 0, NULL, 'TUEIOZ', 'NC24RM'),
(40, NULL, 'ktest', NULL, NULL, NULL, NULL, NULL, '$2y$10$7jjNSbhzG9r4dddlSjOBtughoWR4TS/kV9yKTMImG31YVqiQekrDy', '+1', '8686868686', 'CA', 1, NULL, '2021-07-29 01:27:58', '2021-07-29 05:37:27', 2, 0, '0', 'acct_1JITGAGMmgh1ZSOP', 1, NULL, '0WJGZC', NULL),
(41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$w/yNiFYEVgAJ4tadCqcg9ONzplSrKo/Dq53sLMstTY6.DOWJjK6tG', '+1', '99696969696', 'CA', 1, NULL, '2021-07-29 07:57:54', '2021-07-29 07:57:54', 2, 0, '0', '0', 0, NULL, 'IEL9O2', NULL),
(42, NULL, 'Shivam', NULL, NULL, NULL, NULL, 'https://limitless-images-test.s3.amazonaws.com/1627719370jpg', '$2y$10$Li/XtSKeZQy83Oyd2Y4bq.o8U.DBAT/gj9OiPZUEymLZsyy8LnZ62', '+1', '9501870654', 'CA', 1, NULL, '2021-07-30 02:27:46', '2021-07-31 02:46:11', 1, 0, '0', 'cus_Jwk7ImgafDPHe6', 0, NULL, 'H10UGE', NULL),
(43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$PX8nghGIPSIqKQBfFx/K2Oh38zwQaXSuwU73kgamQzy/g7l2JZzja', '+1', '9569112625', 'CA', 1, NULL, '2021-08-02 00:12:45', '2021-08-02 00:12:45', 2, 0, '0', '0', 0, NULL, 'FVD2X8', NULL),
(44, NULL, 'Jhhh', NULL, NULL, NULL, NULL, NULL, '$2y$10$SY8PRV3wuKB.j9MutkP95.OinSEhkyNxwHxbM9T..Bze8/iF4Fu.S', '+1', '8989898986', 'CA', 1, NULL, '2021-08-03 01:43:23', '2021-08-03 01:43:32', 2, 0, '0', '0', 0, NULL, '7D8ZHJ', NULL),
(45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$RhXjdjRaAKWAelmlihhgoe0fVDyHzCGXJCV.tQLti3eMKzDxlEkEq', '+91', '1626420322', NULL, 1, NULL, '2021-08-03 03:05:50', '2021-08-03 03:05:50', 1, 0, '0', '0', 0, NULL, '0SB469', NULL),
(46, NULL, 'rahul', NULL, NULL, 'customer2N1@gmail.com', NULL, NULL, '$2y$10$7uppUpn9yUoBzJ7pnIE0v.f.t1qWy5xSD1uPSpg268k7O0L./thRi', '+91', '978152392742', 'IND', 1, NULL, '2021-08-03 04:57:39', '2021-08-03 04:57:39', 1, 0, '0', '0', 0, NULL, 'ORTWSW', NULL),
(47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$AZ95q6NT1LUqD1nKqTjU4OLnp939NUjvCb69ezF/11u58MHTtGiVW', '+1', NULL, NULL, 1, NULL, '2021-08-03 05:01:17', '2021-08-03 05:01:17', 1, 0, '0', '0', 0, NULL, '4BY944', NULL),
(48, NULL, 'rajat', NULL, NULL, NULL, NULL, NULL, '$2y$10$saKEKGIq17cWGTs66pQr..9xMaAy1S6MnTNTtkub2KTXIIEKXBFeO', '+1', NULL, NULL, 1, NULL, '2021-08-03 05:02:08', '2021-08-03 05:02:08', 1, 0, '0', '0', 0, NULL, 'I8FXAN', NULL),
(49, NULL, 'rajat', NULL, NULL, NULL, NULL, NULL, '$2y$10$R2VDPYCDnkECRSqVOtwcL.8Pqf19jLyeOy8diaPX.FutKm.VYExte', '+1', NULL, NULL, 1, NULL, '2021-08-03 05:04:03', '2021-08-03 05:04:03', 1, 0, '0', '0', 0, NULL, 'OTQPTO', NULL),
(50, NULL, 'rajat', NULL, NULL, NULL, NULL, NULL, '$2y$10$r1dQqd6CZd1n/fJaOATJte7qjeyewSy9KACfCsNDBX1su0p0xiey6', '+1', NULL, NULL, 1, NULL, '2021-08-03 05:32:02', '2021-08-03 05:32:02', 1, 0, '0', '0', 0, NULL, 'R4Z2GC', NULL),
(51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$5qeVUranxlx67eSO7eHAlOIQ1gddhxXWfcKBC98RdyDiwJQNbkq9i', '+1', '3535353535', NULL, 1, NULL, '2021-08-03 07:31:35', '2021-08-03 07:31:35', 2, 0, '0', '0', 0, NULL, 'D6D9K8', NULL),
(52, NULL, 'rajat', NULL, NULL, NULL, NULL, NULL, '$2y$10$DGWhznlsCZ2JX/16ySi4HOTFWp7x1q4QimVMif9m7ctn6PISkMmze', '+1', '7082559204', NULL, 1, NULL, '2021-08-04 05:07:31', '2021-08-06 05:09:58', 1, 0, '0', '0', 0, NULL, '5B7RXF', NULL),
(53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$5PyPpaQXhhBMajU1ZWRq5ufJbkwHi5R8LlPepWVQf4CGHgJCB1y2e', '+91', '1626351287', NULL, 1, NULL, '2021-08-05 02:54:24', '2021-08-05 02:54:24', 1, 0, '0', '0', 0, NULL, 'QKPJ2H', NULL),
(54, NULL, 'jatin', NULL, NULL, NULL, NULL, NULL, '$2y$10$r8d/UEg07FMTOxwtMTIbg.9v2dpZ.9MJw.2i2qzEP9wD9bdL5vFdm', '+1', '9501648745', NULL, 1, NULL, '2021-08-06 23:32:06', '2021-08-06 23:32:10', 1, 0, '0', '0', 0, NULL, '81R27C', NULL),
(55, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, '$2y$10$CvrUI7uvpaB4khH9qWq6L.y7hrtv2aEzrPJ3QoCeB4xjJ3z44Gbg.', '+1', '9501648746', NULL, 1, NULL, '2021-08-06 23:33:35', '2021-08-06 23:33:40', 1, 0, '0', '0', 0, NULL, 'VR2CTA', NULL),
(56, NULL, 'shivam', NULL, NULL, NULL, NULL, NULL, '$2y$10$0KNXh7q6b1B8.ch30Cf0muc02PZLHvtbS/QwFCU4P3HofA0gw.Y7O', '+1', '9501648774', NULL, 1, NULL, '2021-08-06 23:49:38', '2021-08-06 23:50:30', 1, 0, '0', '0', 0, NULL, 'F9UEH5', NULL),
(57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$Zyb6RbVZoYE4tQvu/bDzd.M0CjBSkrYDrYlxy6SynGbEWTmfCBHYW', '+1', '3065197634', 'US', 1, NULL, '2021-08-08 21:25:07', '2021-08-08 21:25:07', 1, 0, '0', '0', 0, NULL, 'ONF33R', NULL),
(58, NULL, 'Test', NULL, NULL, NULL, NULL, NULL, '$2y$10$Hi5tE0gGm6hg4tUe4vT3QutDN0.h50BGYzRzudIGciNDW4nbI6mIe', '+1', '8630901346', 'CA', 1, NULL, '2021-08-11 03:47:28', '2021-08-11 03:56:28', 1, 0, '0', 'cus_K1GEuauxTnMP4s', 0, NULL, '1VKF98', NULL),
(59, NULL, 'Nagarjun', NULL, NULL, NULL, NULL, NULL, '$2y$10$Otb3bnCL8SFzjwQzspMy1eXiJzsiA2L2As5nMMO6JBIEF0w.BKbkS', '+1', '3065266841', 'CA', 1, NULL, '2021-08-11 09:24:04', '2021-08-11 09:24:16', 1, 0, '0', '0', 0, NULL, 'S1TYKJ', NULL),
(60, NULL, 'shivams', NULL, NULL, NULL, NULL, NULL, '$2y$10$4ctgIFeM4ESqmDKw.DorP.8gjPuyKFINJNd9XOLiTAdnIyMJNEb7O', '+1', '9501648706', NULL, 1, NULL, '2021-08-11 23:04:55', '2021-08-17 06:11:52', 1, 0, '0', '0', 0, NULL, '97KA7G', NULL),
(61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$29rbvxl2PYnMpOTvnWfaoOP/nFfYXHSJqkmU2eo8OSOiA9vd7H9Ae', '+1', '8485865153', NULL, 1, NULL, '2021-08-13 01:52:42', '2021-08-13 01:52:42', 1, 0, '0', '0', 0, NULL, 'MRI8D3', NULL),
(62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$phaNtsYG1Doo.jRmvonhvOz972aDjsYFQwwTiKuShAHtKowgpu2AC', '+1', '84858684', NULL, 1, NULL, '2021-08-13 02:19:06', '2021-08-13 02:19:06', 1, 0, '0', '0', 0, NULL, '5XWLQN', NULL),
(63, NULL, 'Karan', NULL, NULL, NULL, NULL, NULL, '$2y$10$3yqL6xKKs.64CjN8ZUw/sek3s150xy7kPcsBbjJLoMv95l9YKD3TG', '+1', '3062098789', 'CA', 1, NULL, '2021-08-15 12:15:44', '2021-08-16 00:48:11', 2, 0, '0', 'acct_1JOo7mIulp3hJLgA', 0, NULL, 'G4BRIU', NULL),
(64, NULL, 'Xtz', NULL, NULL, NULL, NULL, NULL, '$2y$10$wixZdKmqnhdLh1oC8866serpXVIUKkc/0y7fDorM2hD/UEbxdWu3K', '+1', '3062060765', 'CA', 1, NULL, '2021-08-16 00:48:27', '2021-08-16 00:48:33', 2, 0, '0', '0', 0, NULL, 'BDJ5TF', NULL),
(65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$od1MQE6NWwvMd4vfYqnnh.NZdMMjzv7hXGw3s63NdSfRp7DO5wJWm', '+91', '5152535153', NULL, 1, NULL, '2021-08-16 03:11:12', '2021-08-16 03:11:12', 1, 0, '0', '0', 0, NULL, 'NF5L1Z', NULL),
(66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$q6yBy4BhvhplmPKe0qnTyO.FkYYINAsZFMRtsaXPeFql4bFdBFzu.', '+1', '1235289745', NULL, 1, NULL, '2021-08-17 05:58:13', '2021-08-17 05:58:13', 1, 0, '0', '0', 0, NULL, '8LUUWF', NULL),
(67, NULL, 'shivam', NULL, NULL, NULL, NULL, NULL, '$2y$10$3sSI74SMOR/e3/Ljav5wI.VCzoAQTQwydUIgLggi11C5h2m8Clk86', '+1', '1235289740', NULL, 1, NULL, '2021-08-17 06:06:27', '2021-08-17 06:06:39', 1, 0, '0', '0', 0, NULL, '04ZU4P', NULL),
(68, NULL, 'Ajay', NULL, NULL, NULL, NULL, NULL, '$2y$10$6PGYXDeX997xV10xJ2R2DeXtGLRSDnut3agbs.fs7HR4VoRGgO/7K', '+1', '8485868183', NULL, 1, NULL, '2021-08-18 05:10:18', '2021-08-18 05:10:26', 1, 0, '0', '0', 0, NULL, 'WFL37D', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_based_location`
--

CREATE TABLE `users_based_location` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `lat` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `accuracy` decimal(10,2) NOT NULL,
  `altitude` decimal(10,2) NOT NULL,
  `speed` decimal(10,2) NOT NULL,
  `speedAccuracy` decimal(10,2) NOT NULL,
  `heading` decimal(10,2) NOT NULL,
  `time` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_based_location`
--

INSERT INTO `users_based_location` (`id`, `user_id`, `lat`, `lng`, `accuracy`, `altitude`, `speed`, `speedAccuracy`, `heading`, `time`, `created_at`, `updated_at`) VALUES
(1, 2, '29.9861336', '75.4050399', '48.90', '238.08', '0.01', '0.00', '117.49', '0.00', '2021-07-22 06:38:53', '2021-08-03 01:54:55'),
(3, 3, '29.9860992', '75.4050559', '56.10', '230.52', '0.00', '0.00', '90.00', '0.00', '2021-07-23 07:36:06', '2021-08-03 01:47:15'),
(4, 6, '29.9861771', '75.4050732', '60.00', '176.44', '0.00', '0.00', '0.00', '0.00', '2021-07-23 08:12:09', '2021-08-03 07:11:16'),
(5, 7, '29.9861562', '75.4050744', '26.40', '176.00', '0.00', '0.00', '0.00', '0.00', '2021-07-26 01:03:21', '2021-08-05 04:07:31'),
(6, 8, '30.7091551', '76.6890939', '800.00', '267.18', '0.00', '0.00', '0.00', '0.00', '2021-07-26 01:32:13', '2021-07-26 01:32:13'),
(7, 40, '30.7052289', '76.7049117', '15.19', '274.32', '0.00', '0.00', '0.00', '0.00', '2021-07-29 01:32:38', '2021-07-29 13:19:35'),
(8, 44, '29.9861534', '75.405064', '48.90', '176.30', '0.00', '0.00', '0.00', '0.00', '2021-08-03 01:43:40', '2021-08-03 01:43:40'),
(9, 63, '17.373826', '78.5003907', '87.60', '425.63', '0.00', '0.00', '0.00', '0.00', '2021-08-15 12:52:32', '2021-08-16 00:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formatted_address` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `additional_info` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double(20,10) NOT NULL,
  `longitude` double(20,10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_primary` tinyint(4) NOT NULL DEFAULT '0',
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `user_id`, `type`, `city`, `country`, `formatted_address`, `additional_info`, `latitude`, `longitude`, `created_at`, `updated_at`, `is_primary`, `pincode`) VALUES
(2, 4, 1, 'Sahibzada Ajit Singh Nagar', 'India', 'E-207, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160071, India', '', 30.7097572000, 76.6895693000, NULL, NULL, 1, '160071'),
(3, 5, 1, 'Sahibzada Ajit Singh Nagar', 'India', '2, Phase-8B, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India', '', 30.7101066000, 76.6915105000, NULL, NULL, 1, '140308'),
(4, 11, 3, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 29.9843753567, 75.4065208510, NULL, NULL, 1, '123'),
(5, 12, 3, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 29.9843753567, 75.4065208510, NULL, NULL, 1, '123'),
(6, 19, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7097570000, 76.6895413000, NULL, '2021-08-11 04:19:12', 0, '160062'),
(7, 13, 1, 'Mansa', 'India', 'Unnamed Road, Sadar, Mansa, Punjab 151505, India', '', 29.9861307000, 75.4051808000, NULL, '2021-07-28 23:59:20', 0, '151505'),
(8, 22, 1, 'Mansa', 'India', 'Unnamed Road, Guru Gobind Singh Nagar, Mansa, Punjab 151505, India', '', 29.9881299106, 75.4109816998, NULL, NULL, 1, '151505'),
(9, 23, 1, 'Mansa', 'India', 'Unnamed Road, Sadar, Mansa, Punjab 151505, India', '', 29.9861892000, 75.4050286000, NULL, NULL, 1, '151505'),
(10, 24, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7096807000, 76.6894805000, NULL, '2021-07-28 08:09:29', 0, '160062'),
(11, 37, 1, 'Mansa', 'India', 'Tagore St, Sadar, Mansa, Punjab 151505, India', '', 29.9861241000, 75.4049221000, NULL, NULL, 1, '151505'),
(12, 24, 1, 'Mansa', 'India', 'Unnamed Road, Sadar, Mansa, Punjab 151505, India', '', 29.9862547000, 75.4051353000, NULL, '2021-07-28 08:09:29', 1, '151505'),
(13, 17, 1, 'Mansa', 'India', 'Tagore St, Sadar, Mansa, Punjab 151505, India', '', 29.9861251000, 75.4048529000, NULL, NULL, 1, '151505'),
(14, 13, 1, 'Mansa', 'India', 'Madhe Moga St, Guru Gobind Singh Nagar, Mansa, Punjab 151505, India', '', 29.9896260008, 75.4021951184, NULL, '2021-07-28 23:59:20', 0, '151505'),
(15, 13, 1, 'Mansa', 'India', 'Shop no 148 149, Near Subhash Dramatic Club, Grain Market, Green Valley Colony, Mansa, Punjab 151505, India', '', 29.9803764221, 75.3942644969, NULL, '2021-07-28 23:59:20', 0, '151505'),
(16, 13, 1, 'Bhaini Bagha', 'India', 'Bhaini Bagha Ring Rd, Bhaini Bagha, Punjab 151508, India', '', 30.0614341491, 75.3598266095, NULL, '2021-07-28 23:59:20', 1, '151508'),
(17, 19, 1, 'Sahibzada Ajit Singh Nagar', 'India', 'PM4J+X8V, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 140308, India', '', 30.7076140476, 76.6802613437, NULL, '2021-08-11 04:19:12', 0, '140308'),
(18, 19, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7098417000, 76.6894521000, NULL, '2021-08-11 04:19:12', 0, '160062'),
(19, 42, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7096780000, 76.6894694000, NULL, NULL, 1, '160062'),
(20, 46, 1, 'repoa', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 30.7015670000, 76.7652730000, NULL, NULL, 1, '123Mnq'),
(21, 46, 2, 'mpaaaga', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 350.7015670000, 76.7652730000, NULL, NULL, 1, '12345'),
(22, 58, 1, 'Mohali', 'India', 'Sector 74 Phase 8B, Mohali, 160055, Punjab, India', '', 30.7095663249, 76.6894931676, NULL, NULL, 1, '160055'),
(23, 19, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7096798000, 76.6894761000, NULL, '2021-08-11 04:19:12', 1, '160062'),
(25, 60, 1, 'Sahibzada Ajit Singh Nagar', 'India', '203, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160062, India', '', 30.7096769000, 76.6894754000, NULL, '2021-08-13 01:35:43', 1, '160062'),
(26, 1, 1, 'Mansa', 'India', '9, Lalluana Rd, Guru Gobind Singh Nagar, Mansa, Punjab 151505, India', '', 29.9919363008, 75.4146187752, NULL, '2021-08-18 00:06:33', 0, '151505'),
(27, 1, 1, 'Mansa', 'India', 'Munshi St, Sadar, Mansa, Punjab 151505, India', '', 29.9857472126, 75.3969668224, NULL, '2021-08-18 00:06:33', 1, '151505'),
(28, 1, 3, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 30.7015670000, 76.7652730000, NULL, NULL, 0, '12'),
(29, 1, 1, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 30.7015670000, 76.7652730000, NULL, NULL, 0, '12'),
(30, 1, 1, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', 'Sector 46, Chandigarh, 160047, India', 30.7015670000, 76.7652730000, NULL, NULL, 0, '12'),
(31, 1, 1, 'repura', 'India', 'Sector 46, Chandigarh, 160047, India', '', 30.7015670000, 76.7652730000, NULL, NULL, 0, '12'),
(32, 1, 1, 'Sahibzada Ajit Singh Nagar', 'India', 'F-337, Industrial Focal Point, Phase 8, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055', 'F-337, Industrial Focal Point, Phase 8, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055', 30.7097110000, 76.6895750000, NULL, NULL, 0, '160055'),
(33, 1, 1, 'Sahibzada Ajit Singh Nagar', 'India', 'F-337, Industrial Focal Point, Phase 8, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055', 'F-337, Industrial Focal Point, Phase 8, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055', 30.7097110000, 76.6895750000, NULL, NULL, 0, '160055'),
(34, 1, 1, 'Sahibzada Ajit Singh Nagar', 'India', 'mansa', 'mansa', 30.7097110000, 76.6895750000, NULL, NULL, 0, '160055');

-- --------------------------------------------------------

--
-- Table structure for table `various_charges`
--

CREATE TABLE `various_charges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `chargeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `value` decimal(12,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `various_charges`
--

INSERT INTO `various_charges` (`id`, `chargeName`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Delivery Charge For Customer', 1, '30.00', '2021-02-01 06:34:06', '2021-07-15 07:58:28'),
(2, 'Delivery Charge For Delivery Boy', 1, NULL, '2021-02-05 08:09:51', '2021-05-11 09:39:17'),
(3, 'Platform Charge', 1, '10.00', '2021-02-05 08:09:51', '2021-07-21 05:19:04'),
(4, 'Commission Charge', 0, '10.00', '2021-02-05 08:09:51', '2021-07-21 05:19:15'),
(5, 'Flat Charge On Order Pick Up', 1, '15.00', '2021-02-05 08:09:51', '2021-07-15 09:05:57'),
(6, 'Flat Charge On Order Delivered', 1, '20.00', '2021-02-05 08:09:51', '2021-07-15 09:06:07');

-- --------------------------------------------------------

--
-- Table structure for table `various_charges_on_bookings`
--

CREATE TABLE `various_charges_on_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bookingId` int(11) NOT NULL,
  `chargeName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chargeAmount` decimal(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `various_charge_relation`
--

CREATE TABLE `various_charge_relation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `various_charge_id` int(11) NOT NULL,
  `startRange` double(12,2) NOT NULL,
  `endRange` double(12,2) NOT NULL,
  `price` double(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `various_charge_relation`
--

INSERT INTO `various_charge_relation` (`id`, `various_charge_id`, `startRange`, `endRange`, `price`, `created_at`, `updated_at`) VALUES
(13, 2, 0.00, 5.00, 10.00, '2021-07-21 05:19:27', '2021-07-21 05:19:27'),
(14, 2, 6.00, 20.00, 25.00, '2021-07-21 05:19:27', '2021-07-21 05:19:27');

-- --------------------------------------------------------

--
-- Table structure for table `wysiwyg_media`
--

CREATE TABLE `wysiwyg_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wysiwygable_id` int(10) UNSIGNED DEFAULT NULL,
  `wysiwygable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD KEY `activations_email_index` (`email`);

--
-- Indexes for table `admin_activations`
--
ALTER TABLE `admin_activations`
  ADD KEY `admin_activations_email_index` (`email`);

--
-- Indexes for table `admin_fcm_tokens`
--
ALTER TABLE `admin_fcm_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`);

--
-- Indexes for table `admin_settings`
--
ALTER TABLE `admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_deleted_at_unique` (`email`,`deleted_at`);

--
-- Indexes for table `admin_vendors`
--
ALTER TABLE `admin_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_packages`
--
ALTER TABLE `app_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_versions`
--
ALTER TABLE `app_versions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_reasons_vendor`
--
ALTER TABLE `block_reasons_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_reason_delivery_boy`
--
ALTER TABLE `block_reason_delivery_boy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_notifications`
--
ALTER TABLE `booking_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_notifications_booking_id_foreign` (`booking_id`),
  ADD KEY `booking_notifications_deliveryboy_id_foreign` (`deliveryboy_id`);

--
-- Indexes for table `booking_reject_reason`
--
ALTER TABLE `booking_reject_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_tracking_history`
--
ALTER TABLE `booking_tracking_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_various_charges`
--
ALTER TABLE `booking_various_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_category_name_unique` (`category_name`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_company_name_unique` (`company_name`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveryboy_main_documents`
--
ALTER TABLE `deliveryboy_main_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deliveryboy_main_documents_user_id_foreign` (`user_id`);

--
-- Indexes for table `deliveryboy_temporary_documents`
--
ALTER TABLE `deliveryboy_temporary_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deliveryboy_temporary_documents_user_id_foreign` (`user_id`);

--
-- Indexes for table `device_details`
--
ALTER TABLE `device_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_details_user_id_foreign` (`user_id`);

--
-- Indexes for table `failed_transfer`
--
ALTER TABLE `failed_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_category_id_foreign` (`category_id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fav_unfac_shops`
--
ALTER TABLE `fav_unfac_shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help_and_supports`
--
ALTER TABLE `help_and_supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_for_terms`
--
ALTER TABLE `page_for_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stripe_payment_records`
--
ALTER TABLE `stripe_payment_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stripe_payment_records_user_id_foreign` (`user_id`);

--
-- Indexes for table `stripe_token`
--
ALTER TABLE `stripe_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_banners_location_wise`
--
ALTER TABLE `table_banners_location_wise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_namespace_index` (`namespace`),
  ADD KEY `translations_group_index` (`group`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `social_id` (`social_id`),
  ADD KEY `phone_number` (`phone_number`);

--
-- Indexes for table `users_based_location`
--
ALTER TABLE `users_based_location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `users_based_location_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `various_charges`
--
ALTER TABLE `various_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `various_charges_on_bookings`
--
ALTER TABLE `various_charges_on_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `various_charge_relation`
--
ALTER TABLE `various_charge_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wysiwyg_media`
--
ALTER TABLE `wysiwyg_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wysiwyg_media_wysiwygable_id_index` (`wysiwygable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_fcm_tokens`
--
ALTER TABLE `admin_fcm_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_settings`
--
ALTER TABLE `admin_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;
--
-- AUTO_INCREMENT for table `admin_vendors`
--
ALTER TABLE `admin_vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `app_packages`
--
ALTER TABLE `app_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `app_versions`
--
ALTER TABLE `app_versions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `block_reasons_vendor`
--
ALTER TABLE `block_reasons_vendor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `block_reason_delivery_boy`
--
ALTER TABLE `block_reason_delivery_boy`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `booking_details`
--
ALTER TABLE `booking_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `booking_notifications`
--
ALTER TABLE `booking_notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `booking_reject_reason`
--
ALTER TABLE `booking_reject_reason`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `booking_tracking_history`
--
ALTER TABLE `booking_tracking_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=509;
--
-- AUTO_INCREMENT for table `booking_various_charges`
--
ALTER TABLE `booking_various_charges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `deliveryboy_main_documents`
--
ALTER TABLE `deliveryboy_main_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `deliveryboy_temporary_documents`
--
ALTER TABLE `deliveryboy_temporary_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `device_details`
--
ALTER TABLE `device_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;
--
-- AUTO_INCREMENT for table `failed_transfer`
--
ALTER TABLE `failed_transfer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `fav_unfac_shops`
--
ALTER TABLE `fav_unfac_shops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `help_and_supports`
--
ALTER TABLE `help_and_supports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1017;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;
--
-- AUTO_INCREMENT for table `page_for_terms`
--
ALTER TABLE `page_for_terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `slots`
--
ALTER TABLE `slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `stripe_payment_records`
--
ALTER TABLE `stripe_payment_records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `stripe_token`
--
ALTER TABLE `stripe_token`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `table_banners_location_wise`
--
ALTER TABLE `table_banners_location_wise`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `users_based_location`
--
ALTER TABLE `users_based_location`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `various_charges`
--
ALTER TABLE `various_charges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `various_charges_on_bookings`
--
ALTER TABLE `various_charges_on_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `various_charge_relation`
--
ALTER TABLE `various_charge_relation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `wysiwyg_media`
--
ALTER TABLE `wysiwyg_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking_notifications`
--
ALTER TABLE `booking_notifications`
  ADD CONSTRAINT `booking_notifications_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `booking_details` (`id`),
  ADD CONSTRAINT `booking_notifications_deliveryboy_id_foreign` FOREIGN KEY (`deliveryboy_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `deliveryboy_temporary_documents`
--
ALTER TABLE `deliveryboy_temporary_documents`
  ADD CONSTRAINT `deliveryboy_temporary_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `device_details`
--
ALTER TABLE `device_details`
  ADD CONSTRAINT `device_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `faq_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `faq_category` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stripe_payment_records`
--
ALTER TABLE `stripe_payment_records`
  ADD CONSTRAINT `stripe_payment_records_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_based_location`
--
ALTER TABLE `users_based_location`
  ADD CONSTRAINT `users_based_location_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
