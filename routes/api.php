<?php
namespace App\Routes;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\AppVersionController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\DeliveryboyDocumentsControllers;
use App\Http\Controllers\PageForTermsAndPolicyController;
use App\Http\Controllers\SlotsController;

Route::prefix('api')->middleware('api')->group(function(){
    Route::post('app_version', [AppVersionController::class,'appVersion']);
    Route::GET('getallitem',[ItemController::class,'getAll']);
    Route::GET('getpolicy', [PageForTermsAndPolicyController::class, 'getpolicy']);
    
    Route::GET('getshop', [ShopController::class, 'getShop']);
    Route::GET('products', [ShopController::class, 'getProductsBasedOnCategory']);
    Route::GET('banners', [BannerController::class, 'getBannersBasedOnLocation']);
    Route::GET('getcategory', [CategoryController::class, 'getCategory']);

    Route::group(['middleware' => ['auth:api','apiDataLogger']], function() {
        Route::group(['middleware' => ['auth:api']], function () {
            Route::POST('deliveryboydoc', [DeliveryboyDocumentsControllers::class, 'registerdocuments']);
            Route::GET('getdocument', [DeliveryboyDocumentsControllers::class, 'getdocuments']);
            Route::GET('distance/calculate', [BookingController::class, 'getDistanceBetweenShopAndLocation']);
            Route::GET('delivery/charges/calculate', [BookingController::class, 'getDeliveryChargeBasedOnShopLocationAndLocation']);
            
            
            Route::GET('shop/products', [ShopController::class, 'getProductsBasedOnShopOrCategory']);
            
            Route::GET('getcoupon', [CouponController::class, 'getCoupon']);
            Route::GET('coupon/apply', [CouponController::class, 'applyCoupon']);
            Route::GET('calculate/charge', [BookingController::class, 'calculateChargeAPI']);
            Route::POST('booking', [BookingController::class, 'createBookingWithPayment']);
            Route::PATCH('booking/update', [BookingController::class, 'updateBookingByDeliveryBoyOrByCustomer']);
            Route::GET('booking/detail', [BookingController::class, 'getParticularBookingDetail']);
            Route::GET('booking/customer', [BookingController::class, 'getCustomerBooking']);
            Route::GET('booking/deliveryboy', [BookingController::class, 'getDeliveryBoyListing']);
            Route::GET('booking/deliveryboy/myearning', [BookingController::class, 'getDeliveryBoyListingMyEarning']);
            Route::POST('customer/rating', [ReviewController::class, 'giveRatingToShopAndDeliveryBoy']);
            Route::GET('support/detail', [SupportController::class, 'getSupportListingForUser']);
            Route::POST('support', [SupportController::class, 'raiseDisputeRegardingBooking']);
            Route::GET('make/payment', [BookingController::class, 'makePaymentForBooking']);
            Route::GET('faqs', [FaqController::class, 'getFaqs']);
            Route::GET('notifications', [NotificationController::class, 'getUserNotifications']);
            Route::GET('notifications/unread/count', [NotificationController::class, 'getUnReadNotificationsCount']);

            Route::GET('favourites/shops', [ShopController::class, 'getFavouriteShops']);
            Route::POST('favourites/shops', [ShopController::class, 'markShopFavourite']);

            // Routes for Testing Purpose
            Route::POST('test/notification', [ShopController::class, 'testPush']);
            Route::get('/AWS/sendSMS/{phone_number}', [SMSController::class, 'sendSMS']);
            Route::GET('getslots',[SlotsController::class,'getSlots']);
        });
    });
});