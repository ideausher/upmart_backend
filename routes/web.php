<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TestBookingRating;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/getcategory', function () {
    return "fdhfuh";
});

Route::get('/terms-and-policy',function(){
    return view('terms-and-policy');
});

Route::get('/testrating',[TestBookingRating::class,'test']);