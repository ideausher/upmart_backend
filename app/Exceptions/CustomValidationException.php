<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class CustomValidationException extends Exception implements Throwable
{
    protected $data;
    public function __construct(string $message, array $data = [])
    {
        $this->data = $data;
        parent::__construct($message);
    }

    public function getData()
    {
        return $this->data;
    }
}
