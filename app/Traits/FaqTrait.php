<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Service\Interfaces\FaqServiceInterface;

trait FaqTrait
{
	private $faqService;

	public function __construct(FaqServiceInterface $faqService)
	{
		$this->faqService = $faqService;
	}
	public function getFaqs(Request $request)
	{
		return $this->faqService->getFaqs($request);
	}
}
