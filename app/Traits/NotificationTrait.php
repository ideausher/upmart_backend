<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\PaginationRequest;
use App\Service\V1\UserNotificationService;

trait NotificationTrait
{
	private $notificationService;

	public function __construct(UserNotificationService $notificationService)
	{
		$this->notificationService = $notificationService;
	}
	public function getUserNotifications(PaginationRequest $request)
	{
		return $this->notificationService->getUserNotifications($request);
	}
	public function getUnReadNotificationsCount(Request $request)
	{
		return $this->notificationService->getUnReadNotificationsCount($request);
	}
	
}
