<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest;
use App\Service\Interfaces\BannerServiceInterface;


trait BannerTrait
{
	private $bannerService;

	public function __construct(BannerServiceInterface $bannerService)
	{
		$this->bannerService = $bannerService;
	}
	public function getBannersBasedOnLocation(BannerRequest $request)
	{
		return $this->bannerService->getBannersBasedOnLocation($request);
	}
}
