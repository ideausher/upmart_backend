<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryBoyRegisterRequest;
use App\Service\Interfaces\DeliveryBoyDocServicesInterface;

trait DeliveryboyDocumentsTrait
{
    private $deliveryService;

    public function __construct(DeliveryBoyDocServicesInterface $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    public function registerdocuments(DeliveryBoyRegisterRequest $request)
    {
        return $this->deliveryService->registerdocuments($request);
    }

    public function getdocuments(DeliveryBoyRegisterRequest  $request)
	{ 
		return $this->deliveryService->getdocuments($request);
    }

}