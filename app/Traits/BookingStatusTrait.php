<?php

namespace App\Traits;

use ReflectionClass;
use App\Exceptions\BadRequestException;
use Illuminate\Contracts\Validation\Validator;

trait BookingStatusTrait
{

    public function generateTextMessageBasedUponBookingStatus($status)
    {
        $bookingStatus = new ReflectionClass('App\Enum\BookingStatus');
        $constants = $bookingStatus->getConstants();
        foreach ($constants as $constantName => $constantValue) {
            if ($constantValue == $status) {
                return $constantName;
            }
        }
        return "UNKNOWN_CONSTANT";
    }

    public function generateTextMessageBasedUponBookingConstantName($statusName)
    {
        $bookingStatus = new ReflectionClass('App\Enum\BookingStatus');
        $constants = $bookingStatus->getConstants();
        foreach ($constants as $constantName => $constantValue) {
            if ($constantName == $statusName . "_TEXT") {
                return $constantValue;
            }
        }
        return "UNKNOWN_CONSTANT_TEXT";
    }

    public function getStatusNameBasedUponStatusValue($statusName)
    {
        $bookingStatus = new ReflectionClass('App\Enum\BookingStatus');
        $constants = $bookingStatus->getConstants();
        foreach ($constants as $constantName => $constantValue) {
            if ($constantName == $statusName . "_TEXT") {
                return $constantValue;
            }
        }
        return "UNKNOWN_CONSTANT_TEXT";
    }
}
