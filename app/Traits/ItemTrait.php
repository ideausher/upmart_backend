<?php
namespace App\Traits;
use App\Models\Item;
use App\Repository\ItemRepository;

trait ItemTrait
{
    private $Item;
    public function __construct(ItemRepository $item){

        $this->item=$item;
    }
    public function getAll()
    {
        return $this->item->getAll();
    }
}