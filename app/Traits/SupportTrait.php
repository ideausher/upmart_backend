<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Http\Requests\SupportRequest;
use App\Http\Requests\GetSupportRequest;
use App\Service\Interfaces\HelpAndSupportServiceInterface;

trait SupportTrait
{
    private $supportService;

    public function __construct(HelpAndSupportServiceInterface $supportService)
    {
        $this->supportService = $supportService;
    }
    public function getSupportListingForUser(GetSupportRequest $request)
    {
        return $this->supportService->getSupportListingForUser($request);
    }
    public function raiseDisputeRegardingBooking(SupportRequest $request)
    {
        return $this->supportService->raiseDisputeRegardingBooking($request);
    }

}
