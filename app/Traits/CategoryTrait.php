<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\GetCategoryRequest;
use App\Service\Interfaces\CategoryServiceInterface;

trait CategoryTrait
{
    private $categoryService;

    public function __construct(CategoryServiceInterface $categoryService){
        $this->categoryService = $categoryService;
    }
    public function getCategory(GetCategoryRequest $request)
	{
		return $this->categoryService->getCategory($request);
	}

}