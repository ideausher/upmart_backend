<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\GetSlotsRequest;
use App\Service\Interfaces\SlotsServiceInterface;

trait SlotsTrait
{
    private $slotsService;

    public function __construct(SlotsServiceInterface $slotsService){
        $this->slotsService = $slotsService;
    }
    public function getSlots(GetSlotsRequest $request)
	{
		return $this->slotsService->getSlots($request);
	}

}