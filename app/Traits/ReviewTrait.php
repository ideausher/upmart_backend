<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\RatingRequest;
use App\Service\Interfaces\ReviewServiceInterface;

trait ReviewTrait
{
	private $reviewService;

	public function __construct(ReviewServiceInterface $reviewService)
	{
		$this->reviewService = $reviewService;
	}
	public function giveRatingToShopAndDeliveryBoy(RatingRequest $request)
	{
		return $this->reviewService->giveRatingToShopAndDeliveryBoy($request);
	}
	
}
