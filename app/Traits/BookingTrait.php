<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\GetBookingDetail;
use App\Http\Requests\DistanceCalculate;
use App\Http\Requests\BookingAfterPayment;
use App\Http\Requests\BookingBeforePayment;
use App\Http\Requests\BookingUpdateRequest;
use App\Http\Requests\CalculateChargeRequest;
use App\Http\Requests\GetBookingsForCustomer;
use App\Http\Requests\DeliveryChargeCalculate;
use App\Http\Requests\GetBookingsForDeliveryBoy;
use App\Service\Interfaces\BookingServiceInterface;
use App\Http\Requests\GetBookingsForDeliveryBoyByDateRange;


trait BookingTrait
{
	private $bookingService;

	public function __construct(BookingServiceInterface $bookingService)
	{
		$this->bookingService = $bookingService;
	}
	public function createBookingWithPayment(BookingBeforePayment $request)
	{
		return $this->bookingService->createBookingWithPayment($request);
	}
	public function updateBookingAfterPayment(BookingAfterPayment $request){
		return $this->bookingService->updateBookingAfterPayment($request);
	}
	public function getParticularBookingDetail(GetBookingDetail $request){
		return $this->bookingService->getParticularBookingDetail($request);
	}
	public function getCustomerBooking(GetBookingsForCustomer $request){
		return $this->bookingService->getCustomerBooking($request);
	}
	public function getDistanceBetweenShopAndLocation(DistanceCalculate $request){
		return $this->bookingService->getDistanceBetweenShopAndLocation($request);
	}
	public function getDeliveryChargeBasedOnShopLocationAndLocation(DeliveryChargeCalculate $request){
		return $this->bookingService->getDeliveryChargeBasedOnShopLocationAndLocation($request);
	}
	public function updateBookingByDeliveryBoyOrByCustomer(BookingUpdateRequest $request){
		return $this->bookingService->updateBookingByDeliveryBoyOrByCustomer($request);
	}
	public function calculateChargeAPI(CalculateChargeRequest $request){
		return $this->bookingService->calculateChargeAPI($request);
	}
	public function getDeliveryBoyListing(GetBookingsForDeliveryBoy $request){
		return $this->bookingService->getDeliveryBoyListing($request);
	}
	public function getDeliveryBoyListingMyEarning(GetBookingsForDeliveryBoyByDateRange $request){
		return $this->bookingService->getDeliveryBoyListingMyEarning($request);
	}
	
}
