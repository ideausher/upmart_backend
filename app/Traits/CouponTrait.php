<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\GetCouponRequest;
use App\Http\Requests\PaginationRequest;
use App\Http\Requests\ApplyCouponRequest;
use App\Service\Interfaces\CouponServiceInterface;

trait CouponTrait
{
	private $couponService;
	
	public function __construct(CouponServiceInterface $couponService)
	{
        $this->couponService = $couponService;
	}
    public function getCoupon(GetCouponRequest $request)
	{
        return $this->couponService->getcoupon($request);
	}
	public function applyCoupon(ApplyCouponRequest $request)
	{
        return $this->couponService->applyCoupon($request);
	}
	
	public function getAllCoupons(PaginationRequest $request)
	{
        return $this->couponService->getAllCoupons($request);
	}
}