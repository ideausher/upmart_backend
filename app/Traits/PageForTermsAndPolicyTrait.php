<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\Service\Interfaces\PageForTermsAndPolicyServiceInterface;

trait PageForTermsAndPolicyTrait
{
    private $pageService;
    
    public function __construct(PageForTermsAndPolicyServiceInterface $pageService){
        $this->pageService = $pageService;
    }
    
    public function getpolicy(V7Request\PageRequest $request)
	{
		return $this->pageService->getpolicy($request);
	}

}