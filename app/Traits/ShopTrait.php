<?php
namespace App\Traits;
use Illuminate\Http\Request;
use App\Http\Requests\FavShopRequest;
use App\Http\Requests\GetShopRequest;
use App\Http\Requests\PaginationRequest;
use App\Http\Requests\ProductListingRequest;
use App\Service\Interfaces\ShopServiceInterface;


trait ShopTrait
{
    private $shopService;
	
	public function __construct(ShopServiceInterface $shopService)
	{
		$this->shopService = $shopService;
	}
    public function getShop(GetShopRequest $request)
	{
        return $this->shopService->getShop($request);
	}
	public function getProductsBasedOnShopOrCategory(Request $request)
	{
        return $this->shopService->getProductsBasedOnShopOrCategory($request);
	}
	public function getProductsBasedOnCategory(ProductListingRequest $request)
	{
        return $this->shopService->getProductsBasedOnCategory($request);
	}
	public function getFavouriteShops(PaginationRequest $request)
	{
        return $this->shopService->getFavouriteShops($request);
	}
	public function markShopFavourite(FavShopRequest $request)
	{
        return $this->shopService->markShopFavourite($request);
	}
	public function testPush(Request $request)
	{
        return $this->shopService->testPush($request);
	}
	

}