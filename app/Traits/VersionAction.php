<?php

namespace App\Traits;

use App\Http\Requests\GetAppVersionRequest;
use App\Service\Interfaces\IVersionService;


trait VersionAction
{
	private $versionService;

	public function __construct(IVersionService  $versionService)
	{
		$this->versionService = $versionService;
	}
	public function appVersion(GetAppVersionRequest $request)
	{
		return 	$this->versionService->appVersion($request);
	}
}
