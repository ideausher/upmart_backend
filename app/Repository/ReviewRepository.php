<?php

namespace App\Repository;

use App\Repository\Interfaces\IReviewRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class ReviewRepository extends GenericRepository implements IReviewRepository
{
    public function model()
    {
        return 'App\Models\Review';
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }

    /**
     * Create Review by Customer For Delivery Boy
     *
     * @param [type] $data
     * @return void
     */
    public function createReviewByCustomerForDeliveryBoy($bookingId,$deliveryBoy, $rating, $feedback, $userId){
        $data = [
            'delivery_boy_id' => $deliveryBoy,
            'booking_id' => $bookingId,
            'rating' => $rating,
            'feedback' => $feedback,
            'customer_id_by' => $userId,
        ];
        return $this->model->create($data);
    }
    /**
     * Create Review by Customer For Shop
     *
     * @param [type] $data
     * @return void
     */
    public function createReviewByCustomerForShop($bookingId,$shopId, $rating, $feedback, $userId){
        $data = [
            'shop_id' => $shopId,
            'booking_id' => $bookingId,
            'rating' => $rating,
            'feedback' => $feedback,
            'customer_id_by' => $userId,
        ];
        return $this->model->create($data);
    }

}
