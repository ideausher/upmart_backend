<?php

namespace App\Repository;

use Illuminate\Support\Facades\Auth;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IHelpAndSupportRepository;

class HelpAndSupportRepository extends GenericRepository implements IHelpAndSupportRepository
{
    public function model()
    {
        return 'App\Models\HelpAndSupport';
    }
    
    /**
     * Get Help and support details for user
     *
     * @param [Request] $request
     * @return collection
     */
    public function getUserHelpAndSupportByUserId($userId,$limit,$page,$bookingId = null){
        $data = $this->model->where('user_id',$userId);
        if($bookingId){
            $data = $data->where('booking_id', $bookingId);
        }
        $data = $data->orderBy('created_at','DESC')->limit($limit)
        ->offset(
            ($page-1) * $limit
        )->get();
        return $data;
    }

    /**
     * Get Help and support details for user by shop id
     *
     * @param [integer] $userId
     * @param [integer] $shopId
     * @return collection
     */
    public function getUserHelpAndSupportByUserIdAndShopId($userId, $shopId){
        $data = $this->model->where(['user_id' => $userId, 'shop_id' => $shopId])->get();
        return $data;
    }

    /**
     * Generate Unique Booking Code
     *
     * @return string
     */
    public function generateUniqueTicektCode(){
        $code = 'AS0DF1GH3JK2L4MN6BVC5X7ZQ9WERT8YUIOP';
        $randomString = '';
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $code[rand(0, 4 - 1)];
        }
        $randomString .= date('is');
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $code[rand(0, 4 - 1)];
        }

        return $randomString;
    }
    /**
     * Create Dispute Entry into the database
     *
     * @param [BookingDetail] $booking
     * @param [Integer] $userId
     * @param [text] $feedbackTitle
     * @param [text] $feedbackDescription
     * @param [array] $images
     * @return void
     */
    public function createDisputeEntry($booking,$userId,$feedbackTitle,$feedbackDescription,$images){
        $bookingId = $booking->id;
        $shopId = $booking->shopId;
        return $this->model->create([
            'booking_id' => $bookingId,
            'ticket_id' => $this->generateUniqueTicektCode(),
            'user_id' => $userId,
            'shop_id' => $shopId,
            'feedback_title' => $feedbackTitle,
            'feedback_description' => $feedbackDescription,
            'image' => json_encode($images),
            'query_status' => 0
        ]);
    }
}
