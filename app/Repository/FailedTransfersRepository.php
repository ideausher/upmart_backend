<?php

namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IFailedTransfersRepository;

class FailedTransfersRepository extends GenericRepository implements IFailedTransfersRepository
{
    public function model()
    {
        return 'App\Models\FailedTransfer';
    }

    public function failedTransfersCreate($bookingId, $deliveryBoyId, $vendorId){
        return $this->model->create([
            'booking_id' => $bookingId,
            'delivery_boy_id' => $deliveryBoyId,
            'vendor_id' => $vendorId,
            'status'
        ]);
    }

    public function getFailedTransfers(){
        $data = $this->model->with(['bookingDetail'])->where('status',0)->get();
        // dd($data);
        return $data;
    }

}
