<?php
namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IOrderRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class OrderRepository extends GenericRepository implements IOrderRepository
{
    public function model()
    {
        return 'App\Models\OrderDetail';
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }
    public function getOrdersByOrderId($request, $orderId)
    {
        $shop = $this->model
        ->where('id', $orderId)
        ->limit($request->limit)
        ->offset(
            ($request->page-1) * $request->limit
        )
        ->first();
        
        return $this->setData($shop);
    }
    public function getOrderDetailsByBookingId($bookingId)
    {
        return $this->model
        ->where('bookingId', $bookingId)
        ->get();
    }

    /**
     * Create Order Data(Products information) Based on Booking
     *
     * @param [array] $allOrders
     * @return void
     */
    public function createOrdersBasedOnBooking($allOrders){
        foreach($allOrders as $order){
            $this->model->create($order);
        }
        return true;
    }
}
