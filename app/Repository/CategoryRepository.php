<?php

namespace App\Repository;

use DB;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\ICategoryRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class CategoryRepository extends GenericRepository implements ICategoryRepository
{
    public function model()
    {
        return 'App\Models\Category';
    }
    /**
     * Get Categories
     *
     * @param [type] $request
     * @return void
     */
    public function getCategory($request)
    {
        if ($request->shop_id) { 
            $categorybyShop=Item::where('shop_id', $request->shop_id)->with('categories')->get();
            $category = array ();
            foreach($categorybyShop as $cat){                
                array_push($category, $cat['categories']);
            }
            return $category;
        } elseif (!$request->shop_id) {
            $category=$this->model->get();
            return $category;
        }
    }
    
    /**
     * Get Categories By Shop Id
     *
     * @param [type] $shopId
     * @return void
     */
    public function getCategoriesByShopId($shopId)
    {
            $categorybyShop=Item::where('shop_id', $shopId)->groupBy('category_id')->with('categories')->get();
            $category = array ();
            foreach($categorybyShop as $cat){
                
                if(! isset($cat['categories']['id'])){
                    // this means if category does not exists then skip that one
                    continue;
                }
                $s3 = Storage::disk('s3')->getAdapter()->getClient();
                $customData = [
                    'id' => $cat['categories']['id'],
                    'category_name' => $cat['categories']['category_name'],
                    'category_description' => $cat['categories']['category_description'],
                    'image' => isset($cat['categories']['media'][0]) ? $s3->getObjectUrl(env('AWS_BUCKET'),$cat['categories']['media'][0]['file_name']) : "",
                    
                    
                ];          
                array_push($category,$customData );
            }
            
            return $category;
        
    }
}
