<?php

namespace App\Repository;

use App\Enum\BookingStatus;
use App\Models\Notification;
use DateTime;
use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IBookingNotificationRepository;

class BookingNotificationRepository extends GenericRepository implements IBookingNotificationRepository{

    const BookingNotificationRejected = 0;
    const BookingNotificationAccepted = 1;
    public function model()
    {
        return 'App\Models\BookingNotification';
    }
    
    public function createBookingNotification($deliveryBoysforBookings)
    {
        $notificationData = array();
        $deliveryBoysforBookings =array_map("unserialize", array_unique(array_map("serialize", $deliveryBoysforBookings)));;
        foreach($deliveryBoysforBookings as $data){
            $bookingId = $data['booking']->id;
            $bookingCode = $data['booking']->booking_code;
            foreach($data['deliveryBoys'] as $deliveryBoys){
                $notificationSent = $this->getAlreadySentNotification($bookingId, $deliveryBoys->id);
                if (!$notificationSent) {
                    array_push($notificationData, [
                        'booking_id' => $bookingId ,
                        'deliveryboy_id' => $deliveryBoys->id,
                        'deliveryboy_rating' => $deliveryBoys->avgReviews->avgRating ?? 0,
                        'notification_message'=> "You got the order request for the booking code #$bookingCode ",
                        'deliveryboy_accept_status'=> 0,
                        'expiration_time' => ((new DateTime())->modify("+10 minutes"))->format('Y-m-d H:i:s'),
                        'created_at' => ((new DateTime()))->format('Y-m-d H:i:s'),
                        'updated_at' => ((new DateTime()))->format('Y-m-d H:i:s')
                    ]);
                    Notification::createNotificationNew(0, $deliveryBoys->id, "Hurray! You got a new Order request.", "Hurray! You got a new order request for order #".$data['booking']->booking_code .".", BookingStatus::BOOKING_NOTIFICATION_TO_DELIVERY_BOY);
                }
            }
        }
        $noti = $this->model->insert($notificationData);
        return $noti;
    }
    public function getAlreadySentNotification($bookingId, $deliveryBoyId)
    {
        return $this->model->where([['booking_id', '=', $bookingId], ["deliveryboy_id", '=', $deliveryBoyId]])->first();
    }
    public function getRequestBookingsForDeliveryBoy($status,$user_id,$limit,$page){
        return $this->model->with('bookingDetail','rejectedbookings')->where([
            'deliveryboy_id' => $user_id,
            'deliveryboy_accept_status' => 0
        ])
        ->whereHas('bookingDetail',function($query) use($status){
            $query->whereIn('status', $status);
        })
        ->limit($limit)
        ->offset(
            ($page-1) * $limit
        )
        ->get()
        ->reject(function($value){
            if($value->expiration_time < date('Y-m-d H:i:s'))
                return true;
        })->values();
    }
    public function updateAcceptStatus($bookingId, $deliveryBoyId){
        $row = $this->model->where([
                'booking_id' => $bookingId,
                'deliveryboy_id' => $deliveryBoyId,
                'deliveryboy_accept_status' => 0
            ])->first();
            
        return $row->update([
            'deliveryboy_accept_status' => 1
        ]);
    }
    public function updateExpirationTime($bookingId, $deliveryBoyId, $expiration_time){
        $row = $this->model->where([
                'booking_id' => $bookingId,
                'deliveryboy_id' => $deliveryBoyId
            ])->first();
            
        return $row->update([
            'expiration_time' => $expiration_time
        ]);
    }
    public function sendNotificationToAllDBExceptCurrDB($bookingid, $userid){
        return $this->model->where('booking_id',$bookingid)->get()->reject(function($value) use($userid){
            if($value->deliveryboy_id == $userid){
                return true;
            }
            if($value->expiration_time >= date('Y-m-d H:i:s')){
                return true;
            }
        })->values();
        
    }
    public function sendNotificationToAllDBForParticularBooking($bookingid){
        return $this->model->with('userDetail')->where('booking_id',$bookingid)->get();
        
    }
}
