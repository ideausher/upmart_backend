<?php

namespace App\Repository\Interfaces;

interface IVariousChargesOnBookingRepository
{
    /**
     * Get Charge By Booking ID
     *
     * @param Integer $bookingId
     * @return void
     */
	function getChargeByBookingId($bookingId);

    /**
     * Create Charge Based on booking
     *
     * @param [type] $allCharges
     * @return void
     */
    function createChargeBasedOnBooking($allCharges);
}
