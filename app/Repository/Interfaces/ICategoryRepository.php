<?php

namespace App\Repository\Interfaces;
use Illuminate\Support\Facades\Request;
use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;

interface ICategoryRepository
{
    /**
     * Get Category
     *
     * @param Request $request
     * @return void
     */
	function getCategory(Request $request);

    /**
     * Get Categories By Shop ID
     *
     * @param Integer $shopId
     * @return void
     */
    function getCategoriesByShopId($shopId);
}
