<?php

namespace App\Repository\Interfaces;
use Illuminate\Support\Facades\Request;

interface IDeliveryBoyRegisterRepository
{
    /**
     * Modify Approved to Temp
     *
     * @param Request $request
     * @return void
     */
	static function modifyApprovedToTemp(Request $request);

    /**
     * Register Delivery Boy
     *
     * @param array $data
     * @return void
     */
    function registerdeliveryboy(array $data);

    /**
     * Get Documents
     *
     * @param Integer $user_id
     * @return void
     */
    function getdocuments($user_id);
}
