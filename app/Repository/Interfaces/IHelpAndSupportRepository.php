<?php

namespace App\Repository\Interfaces;

interface IHelpAndSupportRepository
{
    /**
     * Get User Help and support by User ID
     *
     * @param Integer $userId
     * @param Integer $limit
     * @param Integer $page
     * @param Integer $bookingId
     * @return void
     */
	function getUserHelpAndSupportByUserId($userId,$limit,$page,$bookingId = null);

    /**
     * Get User Help and Support By User ID and Shop ID
     *
     * @param Integer $userId
     * @param Integer $shopId
     * @return void
     */
    function getUserHelpAndSupportByUserIdAndShopId($userId, $shopId);

    /**
     * Generate Unique Ticket Code
     *
     * @return void
     */
    function generateUniqueTicektCode();

    /**
     * Create Dispute Entry
     *
     * @param Booking $booking
     * @param Integer $userId
     * @param String $feedbackTitle
     * @param String $feedbackDescription
     * @param array $images
     * @return void
     */
    function createDisputeEntry($booking,$userId,$feedbackTitle,$feedbackDescription,$images);
}
