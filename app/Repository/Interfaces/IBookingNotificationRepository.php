<?php

namespace App\Repository\Interfaces;
interface IBookingNotificationRepository
{
    /**
     * Create Booking Notification
     *
     * @param array $deliveryBoysforBookings
     * @return void
     */
	function createBookingNotification($deliveryBoysforBookings);

    /**
     * Get Request Bookings For Delivery Boy
     *
     * @param Integer $status
     * @param Integer $user_id
     * @param Integer $limit
     * @param Integer $page
     * @return void
     */
    function getRequestBookingsForDeliveryBoy($status,$user_id,$limit,$page);

    /**
     * Update Accept Status
     *
     * @param Integer $bookingId
     * @param Integer $deliveryBoyId
     * @return void
     */
    function updateAcceptStatus($bookingId, $deliveryBoyId);

    /**
     * Update Expiration Time
     *
     * @param Integer $bookingId
     * @param Integer $deliveryBoyId
     * @param Time $expiration_time
     * @return void
     */
    function updateExpirationTime($bookingId, $deliveryBoyId, $expiration_time);

    /**
     * Send Notification to all Delivery boy except Current Delivery Boy
     *
     * @param Integer $bookingid
     * @param Integer $userid
     * @return void
     */
    function sendNotificationToAllDBExceptCurrDB($bookingid, $userid);

    /**
     * Send Notification to all Delivery boy
     *
     * @param [type] $bookingid
     * @return void
     */
    function sendNotificationToAllDBForParticularBooking($bookingid);

    function getAlreadySentNotification($bookingid, $deliveryBoyId);
}
