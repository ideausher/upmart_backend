<?php

namespace App\Repository\Interfaces;
use Illuminate\Support\Facades\Request;

interface IBannerRepository
{
	/**
	 * Get All Banners By Location
	 *
	 * @param Request $request
	 * @return void
	 */
	function getAllBannersByLocation(Request $request);
}
