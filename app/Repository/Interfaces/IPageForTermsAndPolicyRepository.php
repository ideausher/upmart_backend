<?php

namespace App\Repository\Interfaces;

use Illuminate\Support\Facades\Request;

interface IPageForTermsAndPolicyRepository
{
	/**
	 * Get Policy
	 *
	 * @param Request $request
	 * @return void
	 */
	function getpolicy(Request $request);
}
