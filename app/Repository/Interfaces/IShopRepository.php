<?php

namespace App\Repository\Interfaces;

interface IShopRepository
{
    /**
     * Get Shop Details By ID 
     *
     * @param Integer $shopId
     * @return void
     */
	function getShopDetailById($shopId);

    /**
     * Get Shops
     *
     * @param String $latitude
     * @param String $longitude
     * @param String $searchshopname
     * @param array $category_ids
     * @param array $searchshop_id
     * @param Integer $shop_type
     * @param Integer $limit
     * @param Integer $page
     * @param Integer $request
     * @return void
     */
    function getShops($latitude, $longitude,$searchshopname,$category_ids,$searchshop_id=[],$shop_type,$limit,$page,$request);

    /**
     * Get Distance from shop to passed address
     *
     * @param Shop $shopDetails
     * @param AddressDetail $addressDetails
     * @return void
     */
    function getDistanceFromShopToPassedAddress($shopDetails, $addressDetails);

    /**
     * Get Distance from Shop to Passed latitude and longitude
     *
     * @param Shop $shopDetails
     * @param String $lat
     * @param String $lng
     * @return void
     */
    function getDistanceFromShopToPassedLatLong($shopDetails, $lat, $lng);

    function getFavouriteShops($limit,$page,$request);

    function getShopDetailByShopId($shopid);
}
