<?php

namespace App\Repository\Interfaces;

interface IFavUnfavRepository
{
    /**
     * Get FAQs
     *
     * @param Integer $shopid
     * @return void
     */
    function markShopFavourite($shopid,$isFavourite);
}
