<?php

namespace App\Repository\Interfaces;

interface ISettingRepository
{
	/**
	 * Get Settings
	 *
	 * @return void
	 */
	function getSettings();
}
