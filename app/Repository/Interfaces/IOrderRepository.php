<?php
namespace App\Repository\Interfaces;

interface IOrderRepository
{
    /**
     * Get Orders By Order ID
     *
     * @param Request $request
     * @param Integer $orderId
     * @return void
     */
	function getOrdersByOrderId($request, $orderId);

    /**
     * Get Order Details By Booking ID
     *
     * @param Integer $bookingId
     * @return void
     */
    function getOrderDetailsByBookingId($bookingId);

    /**
     * Create Orders Based on Booking
     *
     * @param array $allOrders
     * @return void
     */
    function createOrdersBasedOnBooking($allOrders);
}
