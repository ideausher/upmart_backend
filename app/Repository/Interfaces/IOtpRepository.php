<?php

namespace App\Repository\Interfaces;

use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;

interface IOtpRepository extends IGenericRepository
{
    /**
     * Update OTP Data
     *
     * @param Integer $otp
     * @param array $data
     * @return void
     */
	function updateOtpData($otp,$data);

    /**
     * Save OTP
     *
     * @param Integer $otp
     * @param String $phone_number
     * @param String $country_code
     * @param String $email
     * @return void
     */
    function saveOTP($otp,$phone_number=null,$country_code=null,$email=null);

    /**
     * Check OTP on Phone
     *
     * @param String $phone_number
     * @param String $country_code
     * @param Integer $otp
     * @return void
     */
    function checkOtpOnPhone($phone_number,$country_code,$otp);

    /**
     * Check OTP on Email
     *
     * @param String $email
     * @param Integer $otp
     * @return void
     */
    function checkOtpOnEmail($email,$otp);

    /**
     * Check OTP on both
     *
     * @param String $email
     * @param String $phone_number
     * @param String $country_code
     * @param Integer $otp
     * @return void
     */
    function checkOtpOnBoth($email,$phone_number,$country_code,$otp);
    
    /**
     * Get OTP of Phone
     *
     * @param String $phone_number
     * @param String $country_code
     * @return void
     */
    function getOtpOfPhone($phone_number,$country_code);

    /**
     * Get OTP of Email
     *
     * @param String $email
     * @return void
     */
    function getOtpOfEmail($email);

    /**
     * Get OTP of Both
     *
     * @param String $email
     * @param String $phone_number
     * @param String $country_code
     * @return void
     */
    function getOtpOfBoth($email,$phone_number,$country_code);
}
