<?php

namespace App\Repository\Interfaces;
use Illuminate\Support\Facades\Request;

interface IBookingRepository
{
    /**
     * Get Bookings By Booking ID
     *
     * @param Integer $bookingId
     * @return void
     */
	function getBookingByBookingId($bookingId);

    /**
     * Get Bookings By User ID
     *
     * @param Request $request
     * @param Integer $userId
     * @return void
     */
    function getBookingsByUserId(Request $request, $userId);

    /**
     * Get Bookings By User ID and Booking Status
     *
     * @param Integer $status
     * @param Integer $userId
     * @param Integer $limit
     * @param Integer $page
     * @return void
     */
    function getBookingsByUserIdAndBookingStatus($status, $userId, $limit,$page);

    /**
     * Get Bookings by Shop ID
     *
     * @param Request $request
     * @param Integer $shopId
     * @return void
     */
    function getBookingsByShopId($request, $shopId);

    /**
     * Create Booking With Payment
     *
     * @param array $data
     * @return void
     */
    function createBookingWithPayment($data);

    /**
     * Check User used coupon times
     *
     * @param User $user
     * @param Integer $couponId
     * @return void
     */
    function checkUserUsedCouponTimes($user, $couponId);

    /**
     * Check Total Used Coupon Times
     *
     * @param Integer $couponId
     * @return void
     */
    function checkTotalUsedCouponTimes($couponId);

    /**
     * Get Active Bookings with Notification Average
     *
     * @return void
     */
    function getActiveBookingsWithNotificationsAverage();

    /**
     * Get Active Bookings for the delivery boy
     *
     * @param Integer $status
     * @param Integer $user_id
     * @param Integer $limit
     * @param Integer $page
     * @return void
     */
    function getActiveBookingsForDeliveryBoy($status,$user_id,$limit,$page);

    /**
     * Get Delivered Bookings and Booking Status
     *
     * @param Integer $status
     * @param Integer $user_id
     * @param Integer $limit
     * @param Integer $page
     * @return void
     */
    function getDeliveredBookingsAndBookingStatus($status,$user_id,$limit,$page);

    /**
     * Get Delivered Booking With Date Range
     *
     * @param Integer $status
     * @param Integer $user_id
     * @param Date $startrange
     * @param Date $endrange
     * @return void
     */
    function getDeliveredBookingWithDateRange($status,$user_id,$startrange,$endrange);

    /**
     * Get Bookings By Delivery Boy ID
     *
     * @param Integer $deliveryboyId
     * @return void
     */
    function getBookingsByDeliveryBoyId($deliveryboyId);

    /**
     * Get Active Bookings
     *
     * @return void
     */
    function getActiveBookings();

}
