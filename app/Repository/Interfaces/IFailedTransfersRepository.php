<?php

namespace App\Repository\Interfaces;

interface IFailedTransfersRepository
{
    /**
     * Create a record for failed transfers
     *
     * @param Integer $bookingId
     * @param Integer $deliveryBoyId
     * @param Integer $vendorId
     * @return void
     */
	function failedTransfersCreate($bookingId, $deliveryBoyId, $vendorId);

    /**
     * Get Failed Transfers
     *
     * @return void
     */
    function getFailedTransfers();
}
