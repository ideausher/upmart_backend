<?php

namespace App\Repository\Interfaces;

interface IFaqCategoryRepository
{
    /**
     * Get FAQ Category
     *
     * @return void
     */
    function getFaqCategory();
}
