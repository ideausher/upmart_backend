<?php
namespace App\Repository\Interfaces;

interface IVariousChargesRepository
{
    /**
     * Get All Charges
     *
     * @return void
     */
	function getAllCharges();

    /**
     * Get Charge by ID
     *
     * @param Integer $chargeId
     * @return void
     */
    function getChargeById($chargeId);
}
