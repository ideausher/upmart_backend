<?php

namespace App\Repository\Interfaces;
use Illuminate\Support\Facades\Request;
use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;

interface ISlotsRepository
{
    /**
     * Get Slots
     *
     * @param Request $request
     * @return void
     */
	function getSlots(Request $request);

}
