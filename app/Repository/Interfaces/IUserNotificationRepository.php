<?php

namespace App\Repository\Interfaces;

interface IUserNotificationRepository
{
    /**
     * Get User Notification
     *
     * @param Integer $userId
     * @param Integer $page
     * @param Integer $limit
     * @return void
     */
	function getUserNotifications($userId,$page,$limit);

    /**
     * Get Unread Notification Count
     *
     * @param Integer $userId
     * @return void
     */
    function getUnReadNotificationsCount($userId);
}
