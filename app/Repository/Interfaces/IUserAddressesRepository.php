<?php
namespace App\Repository\Interfaces;

interface IUserAddressesRepository
{
    /**
     * Get User Address
     *
     * @param Integer $user_id
     * @return void
     */
    function getUserAddress($user_id);

    /**
     * Save User Address
     *
     * @param array $insertData
     * @return void
     */
    function saveUserAddress($insertData);

    /**
     * Delete User Address
     *
     * @param array $deleteData
     * @return void
     */
    function deleteUserAddress($deleteData);

    /**
     * Edit User Address
     *
     * @param array $editData
     * @return void
     */
    function editUserAddress(array $editData);

    /**
     * Make All User Address Non Primary
     *
     * @param Integer $user_id
     * @return void
     */
    function makeAllUseraaaadresNonPrimarry($user_id);

    /**
     * Get Address Details By ID
     *
     * @param Integer $id
     * @return void
     */
    function getAddressDetailsById($id);
}
