<?php

namespace App\Repository\Interfaces;

use Illuminate\Support\Facades\Request;

interface IUserBasedlocationRepository
{
	/**
     * Update Location
     *
     * @param Request $request
     * @return void
     */
    function updatelocation(Request $request);

    /**
     * Get Current Location
     *
     * @param Request $request
     * @return void
     */
    function getcurrentlocation($request);
}
