<?php

namespace App\Repository\Interfaces;

interface ICouponRepository
{
    /**
     * Get Coupon
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
	function getCoupon($request, $user);

    /**
     * Get All Coupons
     *
     * @param Request $request
     * @return void
     */
    function getAllCoupons($request);

    /**
     * Get Coupon Details By ID
     *
     * @param Integer $couponId
     * @return void
     */
    function getCouponDetailById($couponId);

    /**
     * Get Coupon Details By Coupon Code
     *
     * @param String $couponCode
     * @return void
     */
    function getCouponDetailByCouponCode($couponCode);

    /**
     * Get Coupon Details By Coupon ID
     *
     * @param Integer $couponId
     * @return void
     */
    function getCouponDetailByCouponId($couponId);
}
