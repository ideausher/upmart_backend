<?php

namespace App\Repository\Interfaces;

interface IReviewRepository
{
    /**
     * Create review by customer for delivery boy
     *
     * @param Integer $bookingId
     * @param Integer $deliveryBoy
     * @param Integer $rating
     * @param String $feedback
     * @param Integer $userId
     * @return void
     */
	function createReviewByCustomerForDeliveryBoy($bookingId,$deliveryBoy, $rating, $feedback, $userId);

    /**
     * Create Review by Customer for Shop
     *
     * @param Integer $bookingId
     * @param Integer $shopId
     * @param Integer $rating
     * @param String $feedback
     * @param Integer $userId
     * @return void
     */
    function createReviewByCustomerForShop($bookingId,$shopId, $rating, $feedback, $userId);
}
