<?php

namespace App\Repository\Interfaces;

interface IItemInterface 
{
    /**
     * Get All Items
     *
     * @return void
     */
	function getAll();

    /**
     * Get Items By Shop and Category ID
     *
     * @param Integer $shopId
     * @param Integer $categoryId
     * @param String $searchText
     * @param Integer $limit
     * @param Integer $page
     * @return void
     */
    function getItemsByShopIdAndCategoryId($shopId,$categoryId,$searchText,$limit,$page);

    /**
     * Get Items By Category ID
     *
     * @param Request $request
     * @param Integer $categoryId
     * @param String $searchText
     * @param Integer $shop_id
     * @param Limit $limit
     * @param Integer $page
     * @param Integer $shop_type
     * @return void
     */
    function getItemsByCategoryId($request,$categoryId,$searchText,$shop_id,$limit,$page,$shop_type);

    /**
     * Get Unique Categories By Shop ID
     *
     * @param Integer $shopId
     * @return void
     */
    function getUninqueCategoriesByShopId($shopId);

    /**
     * Get Product Detail by ID
     *
     * @param Integer $productId
     * @return void
     */
    function getProductDetailById($productId);

    /**
     * Update Stock Value
     *
     * @param Integer $productId
     * @param Integer $quantity
     * @return void
     */
    function updateStockValue($productId, $quantity);
}
