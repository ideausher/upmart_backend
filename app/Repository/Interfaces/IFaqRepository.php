<?php

namespace App\Repository\Interfaces;

interface IFaqRepository
{
    /**
     * Get FAQs
     *
     * @param Integer $categoryId
     * @return void
     */
    function getFaqs($categoryId);
}
