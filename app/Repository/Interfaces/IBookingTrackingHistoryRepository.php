<?php

namespace App\Repository\Interfaces;
use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;

interface IBookingTrackingHistoryRepository
{
	/**
	 * Create Booking History
	 *
	 * @param Integer $bookingId
	 * @param Integer $userId
	 * @param String $reason
	 * @param Integer $value
	 * @param Integer $type
	 * @param Integer $status
	 * @return void
	 */
	function createBookingHistory($bookingId, $userId, $reason,$value , $type,$status);
}
