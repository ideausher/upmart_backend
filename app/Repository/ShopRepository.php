<?php

namespace App\Repository;

use App\Models\Shop;
use App\Enum\ShopType;
use App\Enum\ProductHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IShopRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class ShopRepository extends GenericRepository implements IShopRepository
{
    public function model()
    {
        return 'App\Models\Shop';
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }

    /**
     * Get Shop details by shop id
     *
     * @param [integer] $shopId
     * @return void
     */
    public function getShopDetailById($Id){
        return $this->model->where('id',$Id)->with(['media','image','primaryCoupon','showOwner'])->first();
    }

    /**
     * Get Shop details by shop id
     *
     * @param [integer] $shopId
     * @return void
     */
    public function getShopDetailByShopId($shopId){
        return $this->model->where('user_id',$shopId)->with(['media','image','primaryCoupon','showOwner'])->first();
    }
    /**
     * Get All Shops based on the filters
     * Conditions :
     * -> Only Shops that lie in the 20 km of passed lat long
     * -> If category ids sent in the parameter then only those shops should be listed those serve in category_ids
     * -> Filter by shop name if parameter passed : searchshopname
     * -> Order by : firstly by distance then by rating and finally by their id's 
     * -> Stripe should be connected
     * -> Shop/Vendor Should not be blocked
     * -> Listing should be based on shop_type parameter passed
     * -> Pagination
     * @param [Request] $request
     * @param array $searchshop_id
     * @return void
     */
    public function getShops($latitude, $longitude,$searchshopname,$category_ids,$searchshop_id=[],$shop_type,$limit,$page,$request)
    {
        
        $shop = $this->model->selectRaw(" admin_vendors.*, avg(review.rating) as rating , ( 6371 * acos( cos( radians(' $latitude ') ) * cos( radians( lat ) ) * cos( radians( lng)
                                - radians('$longitude') ) + sin( radians(' $latitude ') ) * sin( radians( lat ) ) ) ) AS distance ")
                                ->join('items', 'admin_vendors.id', '=', 'items.shop_id')
                                ->with(['media','image','primaryCoupon','showOwner','showBanners.bannerImage']);
        
        if(isset($latitude)){
            $shop = $shop->havingRaw('distance <= ' . env("CUS_DISTANCE_VALUE", "20"));
        }
        
        if ($searchshopname) {
            $shop=$shop->where(
                'admin_vendors.shop_name',
                'LIKE',
                "%". $searchshopname ."%"
            );
        }

        if($category_ids){
            $shop=$shop
            // ->join('items', 'admin_vendors.id', '=', 'items.shop_id') // here admin_vendors is shop
            ->whereIn(
                'items.category_id',
                $category_ids
            )->where('is_disabled', ProductHelper::EnabledProduct);
        }
        if (!empty($searchshop_id)) {
            $shop= $shop->whereIn('admin_vendors.id', $searchshop_id);
        }
        
        $shop = $shop->leftjoin('review', 'admin_vendors.id', '=', 'review.shop_id')
                    ->groupBy('review.shop_id');
        
        
        if(isset($shop_type)){
            $shop = $shop->whereIn('admin_vendors.shop_type',[$shop_type,ShopType::DeliveryAndTakeawayShop]);
        }
        
        $shop = $shop->whereNotNull('admin_vendors.stripe_user_id')
        ->where('forbidden', 0)
        ->orderBy('distance','DESC')
        ->orderBy('rating','DESC')
        ->orderBy('admin_vendors.id','asc')
        ->groupBy('admin_vendors.id')
        ->limit($limit)
        ->offset(
            ($page-1) * $limit
        )
        ->get();

        return $shop;
    }

    /**
     * Get Distance From Shop Address to the passed Address(Latitude and Longitude)
     *
     * @param [admin_vendors] $shopDetails
     * @param [user_address] $addressDetails
     * @return void
     */
    public function getDistanceFromShopToPassedAddress($shopDetails, $addressDetails){
        return $this->model->selectRaw(" 
            *,
            ( 6371 * acos( cos( radians(' $addressDetails->latitude ') ) * cos( radians( lat ) ) * cos( radians( lng) - 
                radians('  $addressDetails->longitude ') ) + sin( radians('  $addressDetails->latitude ') ) * sin( radians( lat ) ) ) 
            ) AS distance "
        )
        ->where('id',$shopDetails->id)
        ->first();
    }
    public function getDistanceFromShopToPassedLatLong($shopDetails, $lat, $lng){
        return $this->model->selectRaw(" 
            *,
            ( 6371 * acos( cos( radians(' $lat ') ) * cos( radians( lat ) ) * cos( radians( lng) - 
                radians('  $lng ') ) + sin( radians('  $lat ') ) * sin( radians( lat ) ) ) 
            ) AS distance "
        )
        ->where('id',$shopDetails->id)
        ->first();
    }

    public function getFavouriteShops($limit,$page,$request)
    {
        $shop = $this->model->select('admin_vendors.*','fav_unfac_shops.isFavourite as fav')->join('items', 'admin_vendors.id', '=', 'items.shop_id')
                            ->with(['media','image','primaryCoupon','showOwner'])
                            ->join('fav_unfac_shops','admin_vendors.id','=', 'fav_unfac_shops.shop_id');

        $shop = $shop->leftjoin('review', 'admin_vendors.id', '=', 'review.shop_id')
                    ->groupBy('review.shop_id');
        if(Auth::guard('api')->check()){
            $shop =$shop->where('fav_unfac_shops.user_id',Auth::guard('api')->user()->id);
        }
        $shop = $shop->whereNotNull('admin_vendors.stripe_user_id')
        ->where('forbidden', 0)
        ->where('fav_unfac_shops.isFavourite', 1)
        
        ->groupBy('admin_vendors.id')
        ->limit($limit)
        ->offset(
            ($page-1) * $limit
        )
        ->get();
        return $shop;
    }
}
