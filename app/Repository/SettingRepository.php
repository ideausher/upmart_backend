<?php

namespace App\Repository;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\ISettingRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class SettingRepository extends GenericRepository implements ISettingRepository
{
    public function model()
    {
        return 'App\Models\Setting';
    }
    public function getSettings(){
        return $this->model->first();
    }
}
