<?php

namespace App\Repository;

use App\Repository\Interfaces\IOtpRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class OtpRepository extends GenericRepository implements IOtpRepository{

    public function model(){
        return 'App\Models\Otp';
    }
    public function updateOtpData($otp,$data){
        return $otp->update($data);
    }
    
    //for saving Otp
    public function saveOTP($otp,$phone_number=null,$country_code=null,$email=null)
    {
        if(isset($phone_number) && isset($country_code) && isset($email))
        {
            $otpcreated= $this->model->where([
            'phone_number' => $phone_number, 
            'country_code' => $country_code, 
            'otp' => $otp,
            'email' => $email])->delete();


            $otpcreated = $this->create([
                'phone_number' => $phone_number, 
                'country_code' => $country_code, 
                'otp' => $otp,
                'email' => $email
            ]); 
            return true;
        }

        elseif(isset($email) && !isset($phone_number)){

            $otpcreated= $this->model->where([
                'email' => $email])->delete();

            $otpcreated = $this->otpRepo->create([
                'email' => $email,
                'otp' => $otp
            ]);
            return true;
        }elseif(isset($phone_number) && isset($country_code) && !isset($email)){

            $otpcreated= $this->model->where([
                'phone_number' => $phone_number, 
                'country_code' => $country_code                 
               ])->delete();

            $otpcreated = $this->create([
                'phone_number' => $phone_number, 
                'country_code' => $country_code, 
                'otp' => $otp
            ]);
            return true;
        }
    }

    //for check otp on phone
    public function checkOtpOnPhone($phone_number,$country_code,$otp){
        
      return  $this->where([
                'phone_number' => $phone_number,
                'country_code' => $country_code, 
                'otp' => $otp,
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first();
    }

    //for check otp on email
    public function checkOtpOnEmail($email,$otp){
        return $this->where([
                'email' => $email,
                'otp' => $otp,
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first(); 
    }

    //for check otp on email
    public function checkOtpOnBoth($email,$phone_number,$country_code,$otp){
        return $this->where([
                'email' => $email,
                'phone_number' => $phone_number,
                'country_code' => $country_code, 
                'otp' => $otp,
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first();
    }



    public function getOtpOfPhone($phone_number,$country_code){
        return $this->where([
                'phone_number' => $phone_number,
                'country_code' => $country_code, 
                'email'=>null,
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first();
    }

    //for check otp on email
    public function getOtpOfEmail($email){
        return $this->where([
                'email' => $email,
                'phone_number'=>'',
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first();
    }

    //for check otp on email
    public function getOtpOfBoth($email,$phone_number,$country_code){
        return $this->where([
                'email' => $email,
                'phone_number' => $phone_number,
                'country_code' => $country_code, 
                'is_verified' => '0'
            ])->orderBy('id','DESC')
            ->limit(1)
            ->first();
    }
}






