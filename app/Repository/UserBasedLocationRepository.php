<?php

namespace App\Repository;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IUserBasedlocationRepository;

class UserBasedLocationRepository extends GenericRepository implements IUserBasedlocationRepository
{
    public function model()
    {
        return 'App\Models\UserBasedLocation';
    }

    /**
     * Update Location of the user, if user doesn't have any location then create a location for him
     *
     * @param [Request] $request
     * @return void
     */
    public function updatelocation($request) 
    {
        $currentUser=Auth::user();
        $userupdatelocation = $this->model->updateOrCreate(
            [
                'user_id'=>$currentUser->id
            ],
            [
                'lat' => $request['latitude'],
                'lng' => $request['longitude'],
                'accuracy' => $request['accuracy'] ?? 0.00,
                'altitude' => $request['altitude'] ?? 0.00,
                'speed' => $request['speed'] ?? 0.00,
                'speedAccuracy' => $request['speedAccuracy'] ?? 0.00,
                'heading' => $request['heading'] ?? 0.00,
                'time' => $request['time'] ?? 0.00,
            ]
        );
        return $userupdatelocation;
    }

    /**
     * Get Current Location of the user
     *
     * @param [type] $request
     * @return void
     */
    public function getcurrentlocation($request)
    {
        
        $currentlocation =$this->model->where('user_id',$request->user_id)->first();
        return $currentlocation;
    }
}
