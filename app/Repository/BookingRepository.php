<?php

namespace App\Repository;

use DateTime;
use App\Models\Shop;
use App\Enum\BookingStatus;
use App\Service\V1\BookingService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IBookingRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class BookingRepository extends GenericRepository implements IBookingRepository
{
    public function model()
    {
        return 'App\Models\BookingDetail';
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }

    /**
     * Get Booking By Booking Id
     *
     * @param [Request] $request
     * @param [integer] $bookingId
     * @return void
     */
    public function getBookingByBookingId($bookingId)
    {
        $shop = $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings','rejectedbookings'])
        ->where('id', $bookingId)
        ->first();
        
        return $shop;
    }
    /**
     * Get Bookings by User ID
     *
     * @param [Request] $request
     * @param [integer] $userId
     * @return void
     */
    public function getBookingsByUserId($request, $userId)
    {
        $shop = $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings'])
        ->where('userId', $userId)
        ->limit($request->limit)
        ->offset(
            ($request->page-1) * $request->limit
        )
        ->get();
        
        return $shop;
    }
    public function getBookingsByUserIdAndBookingStatus($status, $userId, $limit,$page)
    {
        $shop = $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings','rejectedbookings'])
        ->where('userId', $userId)
        ->whereIn('status', $status)
        ->limit($limit)
        ->orderBy('id','DESC')
        ->offset(
            ($page-1) * $limit
        )
        ->get();
        
        return $shop;
    }
    
    /**
     * Get Bookings by Shop Id
     *
     * @param [Request] $request
     * @param [integer] $shopId
     * @return void
     */
    public function getBookingsByShopId($request, $shopId)
    {
        $shop = $this->model->with(['ordersDetails'])
        ->where('shopId', $shopId)
        ->limit($request->limit)
        ->offset(
            ($request->page-1) * $request->limit
        )
        ->get();
        return $this->setData($shop);
    }

    /**
     * Create Booking Before Payment
     *
     * @param [type] $data
     * @return void
     */
    public function createBookingWithPayment($data){
        
        return $this->model->create($data);
    }
    
    /**
     * Check how many times a user has used a coupon
     *
     * @param [User] $user
     * @param [integer] $couponId
     * @return void
     */
    public function checkUserUsedCouponTimes($user, $couponId){
        return $this->model->where([
            'userId' => $user->id ,
            'couponId' => $couponId])
            ->count();
    }

    /**
     * Check how many times a coupon is used
     *
     * @param [integer] $couponId
     * @return void
     */
    public function checkTotalUsedCouponTimes($couponId){
        return $this->model->where([
            'couponId' => $couponId
            ])
            ->count();
    }

    public function getActiveBookingsWithNotificationsAverage(){
        return $this->model->with(['bookingNotificationsMinRatingByBooking','shopDetails','addressDetails','ordersDetails'])->where(function($query){
                    $query = $query->where('bookingDateTime','>=' , (new DateTime())->format(("Y-m-d H:i:s")))
                                    ->orWhereNull('bookingDateTime');
                })
                ->where([
                    'active' => 1,
                    'bookingType' => BookingService::Delivery
                ])
                ->whereNull('deliveryBoyId')
                ->whereIn('status', [BookingStatus::ORDER_ACCEPTED_BY_VENDOR,BookingStatus::ORDER_IN_PROGRESS])
                ->groupBy('id')
                ->get();
    }

    public function getActiveBookingsForDeliveryBoy($status,$user_id,$limit,$page){
        return $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings','rejectedbookings'])
            ->where([
                'deliveryBoyId' => $user_id,
                'bookingType' => BookingService::Delivery
            ])
            ->whereIn('status', $status)
            ->limit($limit)
            ->offset(
                ($page-1) * $limit
            )
            ->get();
    }
    public function getDeliveredBookingsAndBookingStatus($status,$user_id,$limit,$page){
        return $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings','rejectedbookings'])
            ->where([
                'deliveryBoyId' => $user_id
            ])
            ->whereIn('status', $status)
            ->limit($limit)
            ->offset(
                ($page-1) * $limit
            )
            ->get();
    }
    public function getDeliveredBookingWithDateRange($status,$user_id,$startrange,$endrange){
        return $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings'])
            ->where([
                'deliveryBoyId' => $user_id
            ])
            ->whereIn('status', $status)
            ->whereDate('created_at','>=',$startrange)
            ->whereDate('created_at','<=',$endrange )
            ->get();
    }
    
    /**
     * Get Active Bookings for the delivery boy
     *
     * @param [type] $request
     * @param [type] $userId
     * @return void
     */
    public function getBookingsByDeliveryBoyId($deliveryboyId)
    {
        return $this->model->with(['ordersDetails','userDetails','shopDetails','couponDetails','addressDetails','chargeDetails','trackingHistory','ratings'])
        ->where('deliveryBoyId', $deliveryboyId)
        ->whereIn('status',[
            BookingStatus::DELIVERY_BOY_ASSIGNED,
            BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY,
            BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR,
            BookingStatus::DELIVERY_BOY_PICKED_ORDER,
            BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,
            ])
        ->get();
    }
    public function getActiveBookings(){
        return $this->model->with(['ordersDetails','userDetails','shopDetails','addressDetails','rejectedBookingNotifications','totalBookingNotifications'])
                    ->whereIn('status',[
                            BookingStatus::ORDER_PLACED,
                            BookingStatus::ORDER_ACCEPTED_BY_VENDOR,
                            BookingStatus::ORDER_IN_PROGRESS
                        ])
                    ->whereNull('deliveryBoyId')
                    ->get();
    }
}
