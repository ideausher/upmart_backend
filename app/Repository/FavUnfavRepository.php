<?php

namespace App\Repository;

use App\Models\Shop;
use App\Enum\ShopType;
use App\Enum\ProductHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IFavUnfavRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class FavUnfavRepository extends GenericRepository implements IFavUnfavRepository
{
    public function model()
    {
        return 'App\Models\FavUnfavShop';
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }

    /**
     * Mark Shopfav
     *
     * @param [integer] $shopId
     * @return void
     */
    public function markShopFavourite($shopId,$isFavourite){
        return $this->model->updateOrCreate(
            [
                'shop_id' => $shopId,
                'user_id' => Auth::user()->id,
            ],
            [
                'shop_id' => $shopId,
                'user_id' => Auth::user()->id,
                'isFavourite' => $isFavourite
            ]
            );
    }

}
