<?php

namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IBannerRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class BannerRepository extends GenericRepository implements IBannerRepository
{
    public function model()
    {
        return 'App\Models\Banner';
    }
    
    public function getAllBannersByLocation($request)
    {
        $banner = $this->model->with(['bannerImage'])->selectRaw("
                *,
                ( 6371 * acos( cos( radians(' $request->latitude ') ) * cos( radians( latitude ) ) * cos( radians( longitude) - 
                    radians('  $request->longitude ') ) + sin( radians('  $request->latitude ') ) * sin( radians( latitude ) ) ) 
                ) AS distance "
            )
            ->havingRaw('distance <= ' . env("CUS_DISTANCE_VALUE", "20"))
            ->get();
            
        return $banner;
    }
}
