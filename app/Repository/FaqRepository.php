<?php

namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\IFaqRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class FaqRepository extends GenericRepository implements IFaqRepository
{
    public function model()
    {
        return 'App\Models\Faq';
    }
    /**
     * Get FAQs By Category Id
     *
     * @param [integer] $categoryId
     * @return void
     */
    public function getFaqs($categoryId)
    {
        return $this->model->with('categoryDetail')->where('category_id',$categoryId)->get();
    }
}
