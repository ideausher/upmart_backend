<?php

namespace App\Repository;

use DB;
use App\Models\Item;
use App\Models\Slot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\ISlotsRepository;
use Intersoft\Auth\App\Repository\GenericRepository;

class SlotsRepository extends GenericRepository implements ISlotsRepository
{
    public function model()
    {
        return 'App\Models\Slot';
    }
    /**
     * Get Categories
     *
     * @param [type] $request
     * @return void
     */
    public function getSlots($request)
    {
        $slots = Slot::where('shop_id',$request->shop_id)->get();

        return $slots;
    }
}
