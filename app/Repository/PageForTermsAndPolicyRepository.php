<?php

namespace App\Repository;

use Illuminate\Support\Facades\Auth;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IPageForTermsAndPolicyRepository;

class PageForTermsAndPolicyRepository extends GenericRepository implements IPageForTermsAndPolicyRepository
{
    public function model()
    {
        return 'App\Models\PageForTermsAndPolicy';
    }
    /**
     * Get Policy by Page Id
     *
     * @param [Request] $request
     * @return void
     */
    public function getpolicy($request){
        $page= $this->model->where('page_id',$request->page_id)->first();
        return($page);
    }

    
}
