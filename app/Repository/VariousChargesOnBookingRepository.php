<?php

namespace App\Repository;


use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IVariousChargesOnBookingRepository;

class VariousChargesOnBookingRepository extends GenericRepository implements IVariousChargesOnBookingRepository
{
    public function model()
    {
        return 'App\Models\VariousChargesOnBooking';
    }

    /**
     * Get Charge details of a booking
     *
     * @param [integer] $bookingId
     * @return void
     */
    public function getChargeByBookingId($bookingId)
    {
        return $this->model->where('bookingId', $bookingId)->first();
    }

    /**
     * Create all charges charged for a booking
     *
     * @param [array] $allCharges
     * @return void
     */
    public function createChargeBasedOnBooking($allCharges)
    {
        foreach($allCharges as $charge){
            $this->model->create($charge);
        }
        return true;
    }
}
