<?php
namespace App\Repository;

use App\Models\Item;
use App\Enum\ShopType;
use App\Enum\ProductHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Repository\Interfaces\IItemInterface;

class ItemRepository  implements IItemInterface
{
    public function __construct(Item $model)
    {
        $this->model=$model;
    }

    /**
     * Remove null from the json and replace it with empty string
     *
     * @param [json object] $complexObject
     * @return void
     */
    protected function setData($complexObject){
        $json = json_encode($complexObject);
        $encodedString = preg_replace('/null/', '""' , $json);
        return json_decode($encodedString);;
    }

    /**
     * Get All Items/Products Available
     *
     * @return void
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get Items based on Shop ID, Category IDs (Array), Search Text
     *
     * @param [integer] $shopId
     * @param [array] $categoryId
     * @param [string] $searchText
     * @param [integer] $limit
     * @param [integer] $page
     * @return void
     */
    public function getItemsByShopIdAndCategoryId($shopId,$categoryId,$searchText,$limit,$page){
        $query = $this->model->where([
                'shop_id' => $shopId             
            ])->with('shop');

        if( isset($searchText) && trim($searchText) != "" ){
            $query = $query->where(
                'product_Name', 'like', "%'" . $searchText . "'%"
            );
        }

        if(!empty($categoryId)){
            $query = $query->whereIn(
                'category_id',$categoryId
            );
        }
            
        $data = $query->where('is_disabled', ProductHelper::EnabledProduct)
            ->limit($limit)
            ->offset(
                ($page-1) * $limit
            )
            ->get();
            
        return $this->setData($data);
    }

    /**
     * Get Items based on Category IDs and Search Text
     *
     * @param [integer] $categoryId
     * @param [integer] $searchText
     * @param [integer] $limit
     * @param [integer] $page
     * @return void
     */
    public function getItemsByCategoryId($request,$categoryId,$searchText,$shop_id,$limit,$page,$shop_type){
        $query = $this->model->selectRaw(" items.*,( 6371 * acos( cos( radians(' $request->lat ') ) * cos( radians( lat ) ) * cos( radians( lng) - radians('$request->lng') ) + sin( radians(' $request->lat ') ) * sin( radians( lat ) ) ) ) AS distance ")
                ->with(['productImage','shop','shop.media','shop.image','shop.showBanners'])
                ->join('admin_vendors', 'admin_vendors.id', '=', 'items.shop_id')
                ->where('admin_vendors.forbidden', 0)         
                ->whereNotNull('admin_vendors.stripe_user_id')       
                ->orderBy('admin_vendors.id','asc')
                ->groupBy('items.id')
                ->groupBy('admin_vendors.id');
        if(isset($request->lat )){
            $query = $query->havingRaw('distance <= ' . env("CUS_DISTANCE_VALUE", "20"));
        }
        if( isset($searchText) && trim($searchText) != "" ){
            $query = $query->where(function($query) use($searchText){
                $query->where('items.product_name','LIKE', "%{$searchText}%");
                // ->orWhere('admin_vendors.shop_name', 'like',"%{$searchText}%");
            });
        }

        if(!empty($categoryId)){
            $query = $query->whereIn(
                'category_id',$categoryId
            );
        }
            
        if(isset($shop_type)){
            $query = $query->whereIn('admin_vendors.shop_type',[$shop_type,ShopType::DeliveryAndTakeawayShop]);
            // $query = $query->where(
            //     'admin_vendors.shop_type',$shop_type
            // );
        }

        if(isset($shop_id)){
            $query = $query->where(
                'shop_id',$shop_id
            );
        }

        $data = $query->where('is_disabled', ProductHelper::EnabledProduct)
            ->limit($limit)
            ->offset(
                ($page-1) * $limit
            )
            ->get();

        return $data;
    }

    /**
     * Get Categories of a shop in which shop has listed the products
     *
     * @param [integer] $shopId
     * @return void
     */
    public function getUninqueCategoriesByShopId($shopId){
        return $this->model->with(['categories'])
        ->select('category_id')
        ->distinct('category_id')->where([
            'shop_id' => $shopId
        ])->get();
    }
    /**
     * Get Product Details By Id
     *
     * @param [integer] $productId
     * @return void
     */
    public function getProductDetailById($productId){
        return $this->model->where('id', $productId)->first();
    }

    public function updateStockValue($productId, $quantity){
        return $this->model->where('id', $productId)
                            ->update([
                                'stock_quantity' => DB::raw("stock_quantity + $quantity")
                            ]);
    }
}