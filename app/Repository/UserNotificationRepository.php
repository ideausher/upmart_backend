<?php

namespace App\Repository;


use DateTime;
use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IUserNotificationRepository;

class UserNotificationRepository extends GenericRepository implements IUserNotificationRepository{
    public function model()
    {
        return 'App\Models\Notification';
    }
    
    public function getUserNotifications($userId,$page,$limit){
        $notifications = $this->model->with(['sendBy','sendTo'])->where([
            'send_to' => $userId
        ])
        ->limit($limit)
        ->offset(
            ($page-1) * $limit
        )
        ->orderBy('created_at', 'DESC')
        ->get();
        foreach($notifications as $noti){
            $noti->is_read = 1;
            $noti->save();
        }
        return $notifications;
    }
    public function getUnReadNotificationsCount($userId){
        return $this->model->where([
                'send_to' => $userId,
                'is_read' => 0
            ])
            ->get()->count();
    }
}
