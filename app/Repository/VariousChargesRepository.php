<?php

namespace App\Repository;


use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IVariousChargesRepository;

class VariousChargesRepository extends GenericRepository implements IVariousChargesRepository
{
    public function model()
    {
        return 'App\Models\VariousCharge';
    }

    /**
     * Return All the charges from various charges table
     *
     * @return void
     */
    public function getAllCharges()
    {
        return $this->model->with('variouschargerelation')->get();
    }

    /**
     * Get Charge Details by Id
     *
     * @param [type] $chargeId
     * @return void
     */
    public function getChargeById($chargeId)
    {
        return $this->model->where('id', $chargeId)->first();
    }
}
