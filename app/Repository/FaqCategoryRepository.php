<?php

namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IFaqCategoryRepository;

class FaqCategoryRepository extends GenericRepository implements IFaqCategoryRepository
{
    public function model()
    {
        return 'App\Models\FaqCategory';
    }
    public function getFaqCategory()
    {
        return $this->model->select('id','category')->get();
    }
}
