<?php

namespace App\Repository;
use Auth;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IUserAddressesRepository;

class UserAddressesRepository extends GenericRepository implements IUserAddressesRepository
{
    public function model()
    {
        return 'App\Models\UserAddress';
    }

    /**
     * Get User Adress By User Id
     *
     * @param [integer] $user_id
     * @return void
     */
    public function getUserAddress($user_id)
    {
        $user_address = $this->model->where('user_id', $user_id)->get();
        if ($user_address->count() > 0) {
            return $user_address;
        }
        return false;
    }

    /**
     * Create User Adress
     * 
     * @param [array] $insertData
     * @return void
     */
    public function saveUserAddress($insertData)
    {
        return $this->model->insert($insertData);
    }

    /**
     * Delete User Address
     *
     * @param [array] $deleteData
     * @return void
     */
    public function deleteUserAddress($deleteData)
    {
        return $this->model->where($deleteData)->delete();
    }
    
    /**
     * Edit User Address : we will update user adress if it exists else we will create new user address
     *
     * @param array $editData
     * @return void
     */
    public function editUserAddress(array $editData)
    {
        return $this->model->updateOrCreate(
                [
                    'id'=>$editData['id']
                ],
                [
                    'type'=>$editData['type'],
                    'pincode'=>$editData['pincode'],
                    'city' => $editData['city'],
                    'country' => $editData['country'],
                    'formatted_address' => $editData['formatted_address'],
                    'additional_info' => $editData['additional_info'] ?? "",
                    'latitude' => $editData['latitude'],
                    'longitude' => $editData['longitude'],
                ]
            );
    }
    

    /**
     * Make all User Adress Non Primary
     *
     * @param [integer] $user_id
     * @return void
     */
	public function makeAllUseraaaadresNonPrimarry($user_id)
	{
        return $this->model->where('user_id',$user_id)
                            ->update(['is_primary' => 0]);
    }
    
    /**
     * Get Adress Details by id
     *
     * @param [integer] $id
     * @return void
     */
    public function getAddressDetailsById($id)
    {
        return $this->model->where('id', $id)->first();
    }
}