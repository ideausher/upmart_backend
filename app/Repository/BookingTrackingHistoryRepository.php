<?php

namespace App\Repository;

use App\Models\Shop;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IBookingTrackingHistoryRepository;

class BookingTrackingHistoryRepository extends GenericRepository implements IBookingTrackingHistoryRepository
{
    public function model()
    {
        return 'App\Models\BookingTrackingHistory';
    }
    public function createBookingHistory($bookingId, $userId, $reason,$value , $type,$status){
        $data = [
            'bookingId' => $bookingId,
            'userId' => $userId,
            'reason' => $reason,
            'value' => $value,
            'type' => $type,
            'status_value' => $status
        ];
        return $this->model->create($data);
    }
}
