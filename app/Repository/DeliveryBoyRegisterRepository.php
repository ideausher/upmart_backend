<?php

namespace App\Repository;

use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\DeliveryboyMainDocument;
use App\Models\DeliveryboyTemporaryDocument;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IDeliveryBoyRegisterRepository;

class DeliveryBoyRegisterRepository extends GenericRepository implements IDeliveryBoyRegisterRepository
{
    public function model()
    {
        return 'App\Models\DeliveryboyTemporaryDocument';
    }
    public static function modifyApprovedToTemp($request)
    {
        
        return [
            'user_id' =>  isset($request->user_id) ? $request->user_id : null,
            'registeration_number' => isset($request->registeration_number) ? $request->registeration_number : null,
            'vehicle_picture'=>isset($request->vehicle_picture) ? $request->vehicle_picture : null,
            'vehicle_company'=>isset($request->vehicle_company) ? $request->vehicle_company : null,
            'model' => isset($request->model) ? $request->model : null,
            'color' => isset($request->color) ? $request->color : null,
            'valid_upto' => isset($request->valid_upto) ? $request->valid_upto : null,
            
            'drivinglicence_images'=>isset($request->drivinglicence_images) ? $request->drivinglicence_images : null,
            'drivinglicence_no' =>  isset($request->drivinglicence_no) ? $request->drivinglicence_no : null,

            'drivinglicence_expiry_date' => isset($request->drivinglicence_expiry_date) ? $request->drivinglicence_expiry_date : null,
            'licence_no_plate_images'=>isset($request->licence_no_plate_images) ? $request->licence_no_plate_images : null,
            'no_plate' => isset($request->no_plate) ? $request->no_plate : null,
            'vehicle_insurence_image'=>isset($request->vehicle_insurence_image)  ?  $request->vehicle_insurence_image :null,
            'policy_number' => isset($request->policy_number) ? $request->policy_number : null,
            'vehicle_insurence_valid_upto' => isset($request->vehicle_insurence_valid_upto) ? $request->vehicle_insurence_valid_upto : null,
            'vehicle_insurence_valid_from' => isset($request->vehicle_insurence_valid_from) ? $request->vehicle_insurence_valid_from : null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
    }
    /**
     * Register/Edit Delivery Boy
     *
     * @param array $data
     * @return void
     */
    public function registerdeliveryboy(array $data)
    {
        
        $currentUser=Auth::user();
        $checkIfAprooved = DeliveryboyMainDocument::where('user_id',$currentUser->id)->first();
        
        if($checkIfAprooved){
            
            $dataToBeInserted = $this->modifyApprovedToTemp($checkIfAprooved);
            $insertedData = $this->model->create($dataToBeInserted);
            
            $insertedData = $this->model->find($insertedData->id);
            
            $insertedData->update(
            [
                'registeration_number' => $data['registeration_number'] ?? $checkIfAprooved->registeration_number,
                'model' => $data['model'] ?? $checkIfAprooved->model,
                'vehicle_picture'=> $data['vehicle_picture'] ?? $checkIfAprooved->vehicle_picture,
                'vehicle_company'=> $data['vehicle_company'] ?? $checkIfAprooved->vehicle_company,
                'vehicle_status' => isset($data['registeration_number']) ? 1 : 2,
                'vehicle_reason' => isset($data['registeration_number']) ? null : null,
                'color' => $data['color'] ?? $checkIfAprooved->color,
                'valid_upto' => $data['valid_upto'] ?? $checkIfAprooved->valid_upto,
                
                'drivinglicence_images'=>$data['drivinglicence_images'] ?? $checkIfAprooved->drivinglicence_images,
                'drivinglicence_no' => $data['drivinglicence_no'] ?? $checkIfAprooved->drivinglicence_no,
                'drivinglicence_expiry_date' => $data['drivinglicence_expiry_date'] ?? $checkIfAprooved->drivinglicence_expiry_date,
                'drivinglicence_status'  => isset($data['drivinglicence_no']) ? 1 : 2,
                'drivinglicence_reason' => isset($data['drivinglicence_no']) ? null : null,

                'licence_no_plate_images'=>$data['licence_no_plate_images'] ?? $checkIfAprooved->licence_no_plate_images,
                'no_plate' => $data['no_plate'] ?? $checkIfAprooved->no_plate,
                'licence_no_plate_status'  => isset($data['no_plate']) ? 1 : 2,
                'licence_no_plate_reason' => isset($data['no_plate']) ? null : null,

                'vehicle_insurence_image'=>$data['vehicle_insurence_image'] ?? $checkIfAprooved->vehicle_insurence_image,
                'policy_number'=>$data['policy_number'] ?? $checkIfAprooved->policy_number,
                'vehicle_insurence_valid_upto'=>$data['vehicle_insurence_valid_upto'] ?? $checkIfAprooved->vehicle_insurence_valid_upto,
                'vehicle_insurence_valid_from'=>$data['vehicle_insurence_valid_from'] ?? $checkIfAprooved->vehicle_insurence_valid_from,
                'vehicle_insurence_status'  => isset($data['policy_number']) ? 1 : 2,
                'vehicle_insurence_reason' => isset($data['policy_number']) ? null : null,
            ]);
            $currentUser->is_block = 0;
            $currentUser->save();
            $insertedData->status = 1;
            $checkIfAprooved->delete();
            return $insertedData;
        }
        else{
            $alreadyData = $this->model->where('user_id',$currentUser->id)->first();
            
            $insertedData = $this->model->updateOrCreate(
                [
                    'user_id'=>$currentUser->id
                ],
                [
                    'registeration_number' => $data['registeration_number'] ?? ($alreadyData->registeration_number ?? ""),
                    'model' => $data['model'] ??  ($alreadyData->model ?? ""),
                    'vehicle_picture'=>$data['vehicle_picture'] ??  ($alreadyData->vehicle_picture ?? ""),
                    'vehicle_company'=>$data['vehicle_company'] ??  ($alreadyData->vehicle_company ?? ""),
                    'color' => $data['color'] ??  ($alreadyData->color ?? ""),
                    'valid_upto' => $data['valid_upto'] ??  ($alreadyData->valid_upto ?? ""),
                    'vehicle_status' => $data['registeration_number'] ? 1 :  ($alreadyData->vehicle_status ?? 0),

                    'drivinglicence_images'=>$data['drivinglicence_images'] ??  ($alreadyData->drivinglicence_images ?? ""),
                    'drivinglicence_no' => $data['drivinglicence_no'] ??  ($alreadyData->drivinglicence_no ?? ""),
                    'drivinglicence_expiry_date' => $data['drivinglicence_expiry_date'] ??  ($alreadyData->drivinglicence_expiry_date ?? ""),
                    'drivinglicence_status' => $data['drivinglicence_no'] ? 1 :  ($alreadyData->drivinglicence_status ?? 0),

                    'licence_no_plate_images'=>$data['licence_no_plate_images'] ??  ($alreadyData->licence_no_plate_images ?? ""),
                    'no_plate' => $data['no_plate'] ??  ($alreadyData->no_plate ?? ""),
                    'licence_no_plate_status'  => $data['no_plate'] ? 1 :  ($alreadyData->licence_no_plate_status ?? 0),

                    'vehicle_insurence_image'=>$data['vehicle_insurence_image'] ??  ($alreadyData->vehicle_insurence_image ?? ""),
                    'vehicle_insurence_valid_upto'=>$data['vehicle_insurence_valid_upto'] ?? ($alreadyData->vehicle_insurence_valid_upto ?? ""),
                    'vehicle_insurence_valid_from'=>$data['vehicle_insurence_valid_from'] ?? ($alreadyData->vehicle_insurence_valid_from ?? ""),
                    'policy_number' => $data['policy_number'] ??  ($alreadyData->policy_number ?? ""),
                    'vehicle_insurence_status'  => $data['policy_number'] ? 1 :  ($alreadyData->vehicle_insurence_status ?? 0),
                ]
            );
            
            $currentUser->is_block = 0;
            $currentUser->save();
            $insertedData->status = 1;
            return $insertedData;
        }
        
        
    }
    
    /**
     * Get delivery boy latest Data(documents) with their reasons
     *
     * @param [integer] $user_id
     * @return void
     */
    public function getdocuments($user_id)  
    {
        $currentUser=Auth::user();
        $temp= DeliveryboyMainDocument::where('user_id',$user_id)->first(); 
        if ($temp) {
            $status=DB::table('users')->where('id', $user_id)->first();
            $temp->status=$status->is_block;
            $temp->vehicle_status=2;
            $temp->drivinglicence_status=2;
            $temp->licence_no_plate_status=2;
            $temp->vehicle_insurence_status=2;
            
            return $temp;
        }else{
            $temp= DeliveryboyTemporaryDocument::where('user_id',$user_id)->first(); 
            if ($temp) {
                $status=DB::table('users')->where('id', $user_id)->first();
                $temp->status=$status->is_block;
                
                return $temp;
            }
            
        }
    }
}
