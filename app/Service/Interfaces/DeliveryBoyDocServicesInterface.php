<?php

namespace App\Service\Interfaces;

interface DeliveryBoyDocServicesInterface
{
    /**
     * Register Documents
     *
     * @param [Request] $request
     * @return void
     */
    function registerdocuments($request);

    /**
     * Get Documents
     *
     * @param [Request] $request
     * @return void
     */
    function getdocuments($request);
}