<?php

namespace App\Service\Interfaces;
use Illuminate\Support\Facades\Request;

interface UserNotificationInterface
{
    /**
     * Get User Notifications 
     *
     * @param [Request] $request
     * @return void
     */
    function getUserNotifications(Request $request);

    /**
     * Get Unread Notifications Count
     *
     * @return void
     */
    function getUnReadNotificationsCount();
}