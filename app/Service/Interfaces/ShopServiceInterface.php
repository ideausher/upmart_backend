<?php

namespace App\Service\Interfaces;
use Illuminate\Support\Facades\Request;

interface ShopServiceInterface
{
    /**
     * Get Shop Listing
     *
     * @param Request $request
     * @return void
     */
    function getshop(Request $request);



    /**
     * Get Products Based on Category
     *
     * @param [Request] $request
     * @return void
     */
    function getProductsBasedOnCategory(Request $request);

    /**
     * Get Products Based on Shop or Category
     *
     * @param [Request] $request
     * @return void
     */
    function getProductsBasedOnShopOrCategory(Request $request);

    function getFavouriteShops(Request $request);

    function markShopFavourite(Request $request);

    function testPush($request);
}
