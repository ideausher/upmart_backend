<?php

namespace App\Service\Interfaces;

interface FaqServiceInterface
{
    /**
     * Get FAQs
     *
     * @param [Request] $request
     * @return void
     */
    function getFaqs($request);
}