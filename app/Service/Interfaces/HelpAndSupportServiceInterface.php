<?php

namespace App\Service\Interfaces;

interface HelpAndSupportServiceInterface
{
    /**
     * Get Support Listing for user
     *
     * @param [Request] $request
     * @return void
     */
    function getSupportListingForUser($request);

    /**
     * Raise Dispute Regarding Booking
     *
     * @param [Request] $request
     * @return void
     */
    function raiseDisputeRegardingBooking($request);
}
