<?php

namespace App\Service\Interfaces;

use Illuminate\Support\Facades\Request;

interface SlotsServiceInterface
{
    /**
     * Get Slots Listing
     *
     * @param Request $request
     * @return void
     */
    function getSlots(Request $request);
}