<?php

namespace App\Service\Interfaces;
use Illuminate\Support\Facades\Request;

interface ReviewServiceInterface
{
    /**
     * Give Rating To Shop and Delivery Boy
     *
     * @param [Request] $request
     * @return void
     */
    function giveRatingToShopAndDeliveryBoy(Request $request);
}