<?php

namespace App\Service\Interfaces;

interface CouponServiceInterface
{
    /**
     * Get Coupon
     *
     * @param [Request] $request
     * @return void
     */
    function getCoupon($request);

    /**
     * Apply Coupon
     *
     * @param [Request] $request
     * @return void
     */
    function applyCoupon($request);

    /**
     * Validate Coupon
     *
     * @param [Coupon] $coupon
     * @param [float] $originalPrice
     * @param [User] $user
     * @return void
     */
    function validateCoupon($coupon, $originalPrice,$user,$timezone);

    /**
     * Get All Coupons
     *
     * @param [type] $request
     * @return void
     */
    function getAllCoupons($request);
}