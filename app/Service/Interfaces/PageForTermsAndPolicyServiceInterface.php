<?php

namespace App\Service\Interfaces;
use Illuminate\Support\Facades\Request;

interface PageForTermsAndPolicyServiceInterface
{
    /**
     * Get Policy
     *
     * @param Request $request
     * @return void
     */
    function getpolicy(Request $request);
}
