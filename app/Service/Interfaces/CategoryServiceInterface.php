<?php

namespace App\Service\Interfaces;

use Illuminate\Support\Facades\Request;

interface CategoryServiceInterface
{
    /**
     * Get Category Listing
     *
     * @param Request $request
     * @return void
     */
    function getCategory(Request $request);
}