<?php

namespace App\Service\Interfaces;

use Illuminate\Support\Facades\Request;

interface BannerServiceInterface
{
    /**
     * Get Banners Based on Location
     *
     * @param Request $request
     * @return void
     */
    function getBannersBasedOnLocation(Request $request);
}