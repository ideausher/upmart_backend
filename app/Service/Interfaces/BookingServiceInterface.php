<?php

namespace App\Service\Interfaces;

use Illuminate\Support\Facades\Request;

interface BookingServiceInterface
{
    /**
     * Generate Unique Booking Code
     *
     * @return void
     */
    function generateUniqueBookingCode();
    
    /**
     * Create booking with Payment
     *
     * @param Request $request
     * @return void
     */
    function createBookingWithPayment(Request $request);

    /**
     * Update Booking After Payment
     *
     * @param Request $request
     * @return void
     */
    function updateBookingAfterPayment(Request $request);

    /**
     * Get Particulare Booking Detail
     *
     * @param Request $request
     * @return void
     */
    function getParticularBookingDetail(Request $request);

    /**
     * Get Customer Booking
     *
     * @param Request $request
     * @return void
     */
    function getCustomerBooking(Request $request);

    /**
     * Validate Coupon
     *
     * @param [Coupon] $coupon
     * @param [integer] $originalPrice
     * @param [User] $user
     * @return void
     */
    function validateCoupon($coupon, $originalPrice, $user = null,$timezone);

    /**
     * Get Distance Between Shop and Location
     *
     * @param Request $request
     * @return void
     */
    function getDistanceBetweenShopAndLocation(Request $request);

    /**
     * Get Delivery Charge based on shop location and location provided
     *
     * @param Request $request
     * @return void
     */
    function getDeliveryChargeBasedOnShopLocationAndLocation(Request $request);

    /**
     * Calculate Distance Between Shop and Location
     *
     * @param [integer] $shopId
     * @param [integer] $addressId
     * @param [string] $lat
     * @param [string] $lng
     * @return void
     */
    function calculateDistanceBetweenShopAndLocation($shopId, $addressId=null, $lat = null, $lng=null);

    /**
     * Send Notification After Booking Updation
     *
     * @param [integer] $status
     * @param [integer] $userId
     * @param [BookingDetail] $booking
     * @return void
     */
    function sendNotificationAfterBookingUpdation($status,$userId, $booking);

    /**
     * Update Booking By Delivery Boy or By Customer
     *
     * @param Request $request
     * @return void
     */
    function updateBookingByDeliveryBoyOrByCustomer(Request $request);

    /**
     * Calculate Charge API
     *
     * @param Request $request
     * @return void
     */
    function calculateChargeAPI(Request $request);

    /**
     * Calculate Charge
     *
     * @return void
     */
    function calculateCharge($couponId,$amountGst,$bookingVariousCharges,$totalAmount,$addressId,$shopId,$currentUser,$bookingType,$latInCaseNotAddressId,$lngInCaseNotAddressId,$request);

    /**
     * Make Payment for booking
     *
     * @return void
     */
    function makePaymentForBooking();

    /**
     * Refund Booking by booking
     *
     * @param [type] $booking
     * @return void
     */
    function refundPaymentByBooking($booking);

    /**
     * Refund Stock of booking
     *
     * @param [type] $booking
     * @return void
     */
    function refundStockOfBooking($booking);

    /**
     * Transfer money to vendor and delivery boy
     *
     * @param [type] $booking
     * @return void
     */
    function transferMoneyToVendorAndDeliveryBoy($booking);

    /**
     * Delivery Boy Listing
     *
     * @param Request $request
     * @return void
     */
    function getDeliveryBoyListing(Request $request);

    /**
     * Delivery Boy Listing My earning
     *
     * @param Request $request
     * @return void
     */
    function getDeliveryBoyListingMyEarning(Request $request);
}