<?php

namespace App\Service\Interfaces;
use Illuminate\Support\Facades\Request;

interface UserBasedLocationServiceInterface
{
    /**
     * Update Location
     *
     * @param [Request] $request
     * @return void
     */
	function updatelocation(Request $request);

    /**
     * Get Current Location
     *
     * @param [Request] $request
     * @return void
     */
    function getcurrentlocation(Request $request);
}