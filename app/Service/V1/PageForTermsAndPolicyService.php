<?php

namespace App\Service\V1;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use App\Repository\Interfaces\IPageForTermsAndPolicyRepository;
use App\Service\Interfaces\PageForTermsAndPolicyServiceInterface;

class PageForTermsAndPolicyService implements PageForTermsAndPolicyServiceInterface
{
    private $pageRepo;
    
    public function __construct(IPageForTermsAndPolicyRepository $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }
    /**
     * Get Policy
     *
     * @param [Request] $request
     * @return void
     */
    public function getpolicy($request) //get category listing
    {
        $page = $this->pageRepo->getpolicy(
            $request
        );
        return $page;

    }
}
