<?php

namespace App\Service\V1;

use App\Enum\BookingStatus;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use App\Repository\BookingRepository;
use App\Repository\HelpAndSupportRepository;
use App\Http\Resources\HelpAndSupportResource;
use App\Service\Interfaces\HelpAndSupportServiceInterface;


class HelpAndSupportService implements HelpAndSupportServiceInterface
{
    private $helpAndSupportRepo;
    private $bookingRepo;
    public function __construct(
        HelpAndSupportRepository $helpAndSupportRepo,
        BookingRepository $bookingRepo
        )
    {
        $this->helpAndSupportRepo = $helpAndSupportRepo;
        $this->bookingRepo = $bookingRepo;
    }
    /**
     * Get Support Listing For User
     *
     * @param [Request] $request
     * @return void
     */
    public function getSupportListingForUser($request) //get category listing
    {
        $user = Auth::user();
        $data = $this->helpAndSupportRepo->getUserHelpAndSupportByUserId(
            $user->id,
            $request->limit,
            $request->page,
            $request->booking_id
            
        );
        return HelpAndSupportResource::collection($data);
    }

    /**
     * Raise Request For Booking
     *
     * @param [Request] $request
     * @return HelpAndSupportResource
     */
    public function raiseDisputeRegardingBooking($request){
        $user = Auth::user();
        $booking = $this->bookingRepo->getBookingByBookingId($request->booking_id);
        
        $data = $this->helpAndSupportRepo->createDisputeEntry(
                $booking,
                $user->id,
                $request->feedback_title,
                $request->feedback_description ?? "",
                $request->images ?? []
            );

        Notification::createNotificationNew($booking->userId,$booking->userId,"Dispute feedback!","We have open the ticket id #".$data->ticket_id.".",BookingStatus::TICKET_RAISED);

        $data->responseMessage = "Thanks for your query! Admin will contact you soon.";
        return new HelpAndSupportResource($data);
    }
}
