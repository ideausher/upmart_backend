<?php

namespace App\Service\V1;

use ReflectionClass;
use App\Models\Notification;
use App\Exceptions as Exceptions;
use App\Traits\BookingStatusTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log; 
use Intersoft\Auth\App\Enum\UserEnum;
use App\Http\Resources\UserNotificationResource;
use App\Service\Interfaces\UserNotificationInterface;
use App\Repository\Interfaces\IUserNotificationRepository;

class UserNotificationService implements UserNotificationInterface
{
    use BookingStatusTrait;
    
    private $userNotification;
    public function __construct(
        IUserNotificationRepository $userNotification
    ) {
        $this->userNotification = $userNotification;
    }

    public function getUserNotifications($request){
        $user = Auth::user()->id;
        return UserNotificationResource::collection($this->userNotification->getUserNotifications(Auth::user()->id, $request->page, $request->limit));
    }
    public function getUnReadNotificationsCount(){
        $data = [
            'unreadCount' => $this->userNotification->getUnReadNotificationsCount(Auth::user()->id)
        ];
        return $data;
    }
}
