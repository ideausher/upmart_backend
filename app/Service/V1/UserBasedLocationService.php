<?php

namespace App\Service\V1;

use App\Enum\BookingStatus;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Repository\Interfaces\IBookingRepository;
use App\Repository\Interfaces\IUserBasedlocationRepository;
use App\Service\Interfaces\UserBasedLocationServiceInterface;

class UserBasedLocationService implements UserBasedLocationServiceInterface
{
    private $locationRepo;
    private $bookingRepo;

    public function __construct(IUserBasedlocationRepository $locationRepo,IBookingRepository $bookingRepo)
    {
        $this->locationRepo = $locationRepo;
        $this->bookingRepo = $bookingRepo;
    }
    
    /**
     * Update Location
     *
     * @param [Request] $request
     * @return lat-long
     */
    public function updatelocation($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $updatelocation = $this->locationRepo->updatelocation($request);
            $status= array();
            array_push($status,BookingStatus::DELIVERY_BOY_ASSIGNED);
            array_push($status,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY);
            array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR);
            array_push($status,BookingStatus::DELIVERY_BOY_PICKED_ORDER);
            array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER);
            $bookingDetails = $this->bookingRepo->getActiveBookingsForDeliveryBoy($status,$currentUser->id,100,0);
            foreach($bookingDetails as $booking){
                Notification::createLocationSilentNotification($booking->userId,"Delivery Boy reached to a point","",$request);
            }
            $updatelocation->responseMessage = 'Update successfully ';
            return ($updatelocation);
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    /**
     * Get Current Location
     *
     * @param [Request] $request
     * @return lat-lng
     */
    public function getcurrentlocation($request){
        if ($request->header('Authorization')) {
            if( !isset($request->user_id))
                throw new Exceptions\BadRequestException("Please pass a valid user");
            $location = $this->locationRepo->getcurrentlocation($request);
            if(!isset($location))
                throw new Exceptions\BadRequestException("No Location Found");
            return $location;

        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
}