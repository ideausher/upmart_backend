<?php

namespace App\Service\V1;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\ISlotsRepository;
use App\Service\Interfaces\SlotsServiceInterface;

class SlotsService implements SlotsServiceInterface
{
    private $slotsRepo;
    
    public function __construct(ISlotsRepository $slotsRepo)
    {
        $this->slotsRepo = $slotsRepo;
    }
    /**
     * Get Category Listing
     *
     * @param [Request] $request
     * @return Slots
     */
    public function getSlots($request) //get category listing
    {
        $slots = $this->slotsRepo->getSlots($request);

        return $slots;
    }
}
