<?php

namespace App\Service\V1;

use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\CouponResource;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Repository\Interfaces\ICouponRepository;
use App\Service\Interfaces\CouponServiceInterface;

class CouponService implements CouponServiceInterface
{
    private $couponRepo;

    public function __construct(ICouponRepository $couponRepo)
    {
        $this->couponRepo = $couponRepo;   
    }

    /**
     * Get Coupons
     *
     * @param [type] $request
     * @return CouponResource
     */
    public function getCoupon($request)
    {
        if ($request->header('Authorization')) {
            $user = Auth::user();
            $coupon = $this->couponRepo->getCoupon($request, $user);
            // return $coupon;
            if ($coupon->count()>0) {
                $coupon->responseMessage = "";
                return CouponResource::collection($coupon);
            }
            else{
                $coupon->responseMessage = "Coupon Not Found";
                return $coupon;
            }
        }   else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    /**
     * Apply Coupon
     *
     * @param [Request] $request
     * @return CouponResource
     */
    public function applyCoupon($request)
    {
        if ($request->header('Authorization')) {
            $user = Auth::user();
            $coupon = $this->couponRepo->getCouponDetailByCouponCode($request->coupon_code);
            
            if( ($request->shop_id == null &&  $coupon->shopId != null))
                throw new Exceptions\BadRequestException("Coupon is not a global coupon");
            if( $coupon->shopId != null && $request->shop_id != null &&  ($coupon->shopId != $request->shop_id))
                throw new Exceptions\BadRequestException("Coupon is not a global coupon");
            if (isset($coupon->user_id) && $coupon->user_id != Auth::User()->id) {
                throw new Exceptions\BadRequestException("Coupon is not available for you!");
            }
            $couponData = $this->validateCoupon($coupon, $request->amount, $user,$request->header('timezone'));

            return new CouponResource($coupon);
        }   else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    /**
     * Validate Coupon
     *
     * @param [Coupon] $coupon
     * @param [float] $originalPrice
     * @param [User] $user
     * @return void
     */
    public function validateCoupon($coupon, $originalPrice,$user,$timezone){
        $failed = false;
        $failedMessage = "";
        $couponId = $coupon->id;
        
        if( $user){
            $totalUsedByLoggedInUser = $coupon->couponHistory()->where('userId', $user->id)->count();
            if ($coupon->maximum_per_customer_use <= $totalUsedByLoggedInUser) {
                $failed = true;
                $failedMessage = "You have already used coupon many Times";
            }
            
        }
        $totalUsed = $coupon->couponHistory()->count();
        if ($coupon->maximum_total_use <= $totalUsed){
            $failed = true;
            $failedMessage = "Coupon is already used many times";
        }

        if($originalPrice <  $coupon->coupon_min_amount )
        {
            $failed = true;
            $failedMessage = "Original Amount is less then Min Amount";
        }
        if($originalPrice >  $coupon->coupon_max_amount )
        {
            $failed = true;
            $failedMessage = "Original Amount is greater then Max Amount";
        }
        $currentDate = Carbon::parse(date('Y-m-d H:i:s'))->timezone($timezone)->format("Y-m-d H:i:s");
        if($currentDate <  $coupon->start_date ){
            $failed = true;
            $failedMessage = "Coupon not  yet available to be applied ";
        }
        if($currentDate >  $coupon->end_date ){
            $failed = true;
            $failedMessage = "Coupon expired";
        }
        if($coupon->coupon_type == 1){
            $discountedAmount = $coupon->coupon_discount;
            if($discountedAmount > $originalPrice){
                $failed = true;
                $failedMessage = "Original Amount is less then Coupon Amount";
            }
        }

        $discountedAmount = 0.00;
        $finalAmount = 0.00;

        if(  ! $failed){
            if($coupon->coupon_type == 1){
                $discountedAmount = $coupon->coupon_discount;
                $finalAmount = $originalPrice - $discountedAmount;
            }
            if($coupon->coupon_type == 0){
                $discountedAmount = $originalPrice * ($coupon->coupon_discount/100);
                $finalAmount = $originalPrice - $discountedAmount;
            }
            $data = [
                'couponId' => $coupon->id,
                'code' => $coupon->coupon_code,
                'original_amount' =>  $originalPrice,
                'discounted_amount' => $discountedAmount,
                'final_amount' => $finalAmount 
            ];
            
            return $data;
        }
        throw new Exceptions\BadRequestException($failedMessage);
    }

    /**
     * Get All Coupons
     *
     * @param [Request] $request
     * @return CouponResource
     */
    public function getAllCoupons($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $coupon = $this->couponRepo->getAllCoupons($request);
            // return $coupon;
            if ($coupon->count()>0) {
                $coupon->responseMessage = "";
                return CouponResource::collection($coupon);
            }
            else{
                $coupon->responseMessage = "Coupon Not Found";
                return $coupon;
            }
        }   else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    
}