<?php

namespace App\Service\V1;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Repository\Interfaces\ICategoryRepository;
use App\Service\Interfaces\CategoryServiceInterface;

class CategoryService implements CategoryServiceInterface
{
    private $categoryRepo;
    
    public function __construct(ICategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }
    /**
     * Get Category Listing
     *
     * @param [Request] $request
     * @return Category
     */
    public function getCategory($request) //get category listing
    {
        $category= $this->categoryRepo->getCategory($request);
        
        $s3 = Storage::disk('s3')->getAdapter()->getClient();
        foreach($category as $cat) {
            if(!empty($cat->media[0])) {
                $image = $s3->getObjectUrl(env('AWS_BUCKET'), $cat->media[0]->file_name );
                $cat->image = $image;
            }   
        }
        return $category;
    }
}
