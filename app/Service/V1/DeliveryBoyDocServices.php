<?php

namespace App\Service\V1;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Http\Resources\DeliveryBoyResource;
use App\Service\Interfaces\DeliveryBoyDocServicesInterface;
use Intersoft\Auth\App\Helpers\V1\DeliveryboyRegisterHelper;
use App\Repository\Interfaces\IDeliveryBoyRegisterRepository;

class DeliveryBoyDocServices implements DeliveryBoyDocServicesInterface
{
    private $deliveryRepo;
    private $deliveryHelper;

    public function __construct(
        IDeliveryBoyRegisterRepository $deliveryRepo,
        DeliveryboyRegisterHelper $deliveryHelper
    
    ){
        $this->deliveryRepo = $deliveryRepo;
        $this->deliveryHelper=$deliveryHelper;
    }


    /**
     * Register Document
     *
     * @param [Request] $request
     * @return DeliveryBoyResource
     */
    public function registerdocuments($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            
            $data = $this->deliveryHelper->register($request); //Check parameters exist or not and return array
            $deliveryboyregister = $this->deliveryRepo->registerdeliveryboy($data);
            // if($deliveryboyregister){
            //     $currentUser->update(['document_status'=>1]);
            // }
            $deliveryboyregister->responseMessage="Document Uploaded";
            return new DeliveryBoyResource($deliveryboyregister);
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    /**
     * Get Documents
     *
     * @param [Request] $request
     * @return DeliveryBoyResource
     */
    public function getdocuments($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $deliveryboydocs = $this->deliveryRepo->getdocuments($currentUser->id);
            
            if (!empty($deliveryboydocs)) {
                $deliveryboydocs->responseMessage="";
                return new DeliveryBoyResource($deliveryboydocs);
            }
            else{
                throw new RecordNotFoundException('Document not found');
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
}
