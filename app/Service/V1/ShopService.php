<?php

namespace App\Service\V1;

use DB;
use App\Models\Shop;
use App\Enum\BookingStatus;
use App\Models\AdminVendor;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use App\Http\Resources\ShopResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Repository\FavUnfavRepository;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Http\Resources\ItemResource;
use App\Repository\Interfaces\IItemInterface;
use App\Repository\Interfaces\IShopRepository;
use App\Service\Interfaces\ShopServiceInterface;
use App\Repository\Interfaces\ICategoryRepository;
use App\Repository\Interfaces\IFavUnfavRepository;

class ShopService implements ShopServiceInterface
{
    private $shopRepo;
    private $categoryRepo;
    private $itemRepo;
    private $favUnfavRepo;
    public function __construct(IShopRepository $shopRepo, IItemInterface $itemRepo, ICategoryRepository $categoryRepo, FavUnfavRepository $favUnfavRepo)
    {
        $this->shopRepo = $shopRepo;
        $this->itemRepo = $itemRepo;
        $this->categoryRepo = $categoryRepo;
        $this->favUnfavRepo = $favUnfavRepo;
    }

    /**
     * Get Shop Listing 
     * Conditions : 
     * -> User Within 21 radius of passed latitude and longitude
     * -> 
     *  
     * @param [type] $request
     * @return void
     */
    public function getshop($request)
    {
        $searchshop_id=[];
        if ($request->lng and $request->lat) {
            $searchshop = $this->getUserWithinRadius($request->lat, $request->lng, env("CUS_DISTANCE_VALUE", "20"));
            if (!$searchshop) {
                throw new RecordNotFoundException($request['searchshopname']. ' shop not found.');
            }
            $searchshop_id=$searchshop->pluck('id')->toArray();
        } 

        $searchshop= $this->shopRepo->getShops(
            $request->lat, 
            $request->lng,
            $request->searchshopname,
            $request->category_ids,
            $searchshop_id,
            $request->shop_type,
            $request->limit,
            $request->page,
            $request
        );
        
        if(empty($searchshop) || count($searchshop) == 0){
            throw new RecordNotFoundException('Sorry! Shop not found.');
        }
        return ShopResource::collection($searchshop);
        
    }
    /**
     * Get User Within Radius
     *
     * @param [string] $latitude
     * @param [string] $longitude
     * @param [integer] $finding_radius_in_km
     * @return void
     */
    private function getUserWithinRadius($latitude, $longitude, $finding_radius_in_km)
    {
        $shops = Shop::select(
            DB::raw(" *,( 6371 * acos( cos( radians(' $latitude ') ) * cos( radians( lat ) ) * cos( radians( lng) - radians(' $longitude ') ) + sin( radians(' $latitude ') ) * sin( radians( lat ) ) ) ) AS distance ")
        )
            ->havingRaw('distance <= ' . $finding_radius_in_km)
            ->get();
            
        if ($shops->count()>0) {
            return $shops;
        } else {
            return false;
        }
    }

    /**
     * Get Products Based on Category
     *
     * @param [Request] $request
     * @return ProductResource
     */
    public function getProductsBasedOnCategory($request){
        $request->page = isset($request->page) ? $request->page : 1;
        $request->limit = isset($request->limit) ? $request->limit : 1000;
        $searchshop_id=[];
        if ($request->lng and $request->lat) {
            $searchshop = $this->getUserWithinRadius($request->lat, $request->lng, env("CUS_DISTANCE_VALUE", "20"));
            if (!$searchshop) {
                throw new RecordNotFoundException($request['search_text']. ' shop not found.');
            }
            $searchshop_id=$searchshop->pluck('id')->toArray();
        } 
        $searchshop = array();
        if( isset($request->search_text) && trim($request->search_text) != "" ){
            $searchshop= $this->shopRepo->getShops(
                $request->lat, 
                $request->lng,
                $request->search_text,
                $request->categoryId,
                $searchshop_id,
                $request->shop_type,
                100,
                0,
                $request
            );
        }
        

        $items = $this->itemRepo->getItemsByCategoryId(
            $request,
            $request->category_id,
            $request->search_text,
            $request->shop_id,
            $request->limit,
            $request->page,
            $request->shop_type
        );
        
        $categories = $this->categoryRepo->getCategoriesByShopId(
            $request->shop_id
        );
        $data['categoryServed'] = $categories;

        if($items->count() == 0){
            $data['categoryServed'] = [];
        }
        // $data['shops'] = ShopResource::collection($searchshop);
        $data['items'] = ProductResource::collection($items);

        return $data;
    }

    /**
     * Get Products Based on Shop or Category
     *
     * @param [Request] $request
     * @return Items
     */
    public function getProductsBasedOnShopOrCategory($request){
        $categories = $this->categoryRepo->getCategoriesByShopId(
            $request->shopId
        );
        $data['categoryServed'] = $categories;
        $items = $this->itemRepo->getItemsByShopIdAndCategoryId(
            $request->shopId,
            $request->categoryId,
            $request->searchText,
            $request->limit,
            $request->page
        );
        if(empty($data['items']) && empty($data['categoryServed']) ){
            throw new RecordNotFoundException('No products found');
        }
        $data['items'] = ItemResource::collection($items);
        return $data;
    }
    public function getFavouriteShops($request){

        $searchshop= $this->shopRepo->getFavouriteShops(
            $request->limit,
            $request->page,
            $request
        );
        
        if(empty($searchshop) || count($searchshop) == 0){
            throw new RecordNotFoundException('Sorry! No Favourite Shops not found.');
        }
        return ShopResource::collection($searchshop);
    }

    public function markShopFavourite($request){
        $this->favUnfavRepo->markShopFavourite(
            $request->shop_id,
            $request->isFavourite
        );
        $shop=$this->shopRepo->getShopDetailById($request->shop_id);
        $shop->fav = $request->isFavourite;
        return new ShopResource($shop);
    }

    public function testPush($request){
        Notification::createNotificationNew(Auth::user()->id,Auth::user()->id,"Testing Done Successfully!","Testing Done",BookingStatus::ORDER_PLACED);
    }
}
