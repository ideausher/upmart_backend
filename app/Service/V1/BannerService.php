<?php

namespace App\Service\V1;

use DB;
use App\Models\Shop;
use App\Models\AdminVendor;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\BannerResource;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Repository\Interfaces\IBannerRepository;
use App\Service\Interfaces\BannerServiceInterface;

class BannerService implements BannerServiceInterface
{
    private $bannerRepo;
    public function __construct(
        IBannerRepository $bannerRepo
    ){
        $this->bannerRepo = $bannerRepo;
    }

    /**
     * Get Banners Location
     *
     * @param [Request] $request
     * @return BannerResource
     */
    public function getBannersBasedOnLocation($request)
    {
        $bannerDetails = $this->bannerRepo->getAllBannersByLocation($request);
        if(count($bannerDetails) == 0)
            throw new RecordNotFoundException('No Banner Found in your location');
        return BannerResource::collection($bannerDetails);
    }
}
