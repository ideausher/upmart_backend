<?php

namespace App\Service\V1;

use DB;
use App\Models\Shop;
use App\Models\AdminVendor;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use App\Repository\ShopRepository;
use App\Repository\ReviewRepository;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ReviewResource;
use App\Http\Resources\BookingResource;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Repository\UserRepository;
use App\Service\Interfaces\ReviewServiceInterface;

class ReviewService implements ReviewServiceInterface
{
    private $reviewRepo;
    private $userRepo;
    private $shopRepo;
    public function __construct(
        ReviewRepository $reviewRepo,
        UserRepository $userRepo,
        ShopRepository $shopRepo
    )
    {
        $this->reviewRepo = $reviewRepo;
        $this->userRepo = $userRepo;
        $this->shopRepo = $shopRepo;
    }
    /**
     * Give Rating to shop and delivery boy
     *
     * @param [Request] $request
     * @return ReviewResource
     */
    public function giveRatingToShopAndDeliveryBoy($request)
    {
        if ($request->header('Authorization')) {
            $user = Auth::user();

            $ifAnyKeyExists = false;
            foreach($request->ratings as $rating){
                if(isset($rating['shop_id'])){
                    $ifAnyKeyExists = true;
                    $checkIfExists = $this->shopRepo->getShopDetailById($rating['shop_id']);
                    if(!$checkIfExists ){
                        throw new Exceptions\BadRequestException("Shop does not exists!");
                    }
                }
                if(isset($rating['delivery_boy_id'])){
                    $ifAnyKeyExists = true;
                    $checkIfExists = $this->userRepo->findDeliveryBoyWithId($rating['delivery_boy_id']);
                    if(!$checkIfExists ){
                        throw new Exceptions\BadRequestException("Delivery boy does not exists!");
                    }
                }
            }

            if(!$ifAnyKeyExists)
                throw new Exceptions\BadRequestException("Please pass atleast a valid rating array!");
            
            foreach($request->ratings as $rating){
                if(isset($rating['shop_id'])){
                    $reviewForShop = $this->reviewRepo->createReviewByCustomerForShop(
                        $request['booking_id'],
                        $rating['shop_id'],
                        $rating['rating'],
                        $rating['feedback'],
                        $user->id
                    );
                    continue;
                }
                if(isset($rating['delivery_boy_id'])){

                    $reviewForDeliveryBoy = $this->reviewRepo->createReviewByCustomerForDeliveryBoy(
                        $request['booking_id'],
                        $rating['delivery_boy_id'],
                        $rating['rating'],
                        $rating['feedback'],
                        $user->id
                    );
                    continue;
                }
            }
            
            return [
                'review_for_shop' => isset($reviewForShop) ? new ReviewResource($reviewForShop) : new \stdClass(),
                'review_for_delivery_boy' => isset($reviewForDeliveryBoy) ?  new ReviewResource($reviewForDeliveryBoy) : new \stdClass()
            ];
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
}
