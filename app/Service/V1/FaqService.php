<?php

namespace App\Service\V1;

use DB;
use App\Models\Shop;
use App\Models\AdminVendor;
use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use App\Http\Resources\FaqResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\BannerResource;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\RecordNotFoundException;
use App\Repository\Interfaces\IFaqRepository;
use App\Service\Interfaces\FaqServiceInterface;
use App\Repository\Interfaces\IFaqCategoryRepository;

class FaqService implements FaqServiceInterface
{
    private $faqRepo;
    private $faqcatRepo;
    public function __construct(
        IFaqRepository $faqRepo,
        IFaqCategoryRepository $faqcatRepo
    ){
        $this->faqRepo = $faqRepo;
        $this->faqcatRepo = $faqcatRepo;
    }

    /**
     * Get Faqs
     *
     * @param [Request] $request
     * @return FaqResource
     */
    public function getFaqs($request)
    {
        if ($request->header('Authorization')) {
            $faqcat = $this->faqcatRepo->getFaqCategory();
            $data = array();
            foreach($faqcat as $faq){
                
                $faqDetails = $this->faqRepo->getFaqs($faq->id);
                if(isset($faqDetails) && !empty($faqDetails) && count($faqDetails)!=0)
                {
                    array_push($data,[
                        "category_id" => $faq->id ,
                        "category_name" => $faq->category,
                        "questions" => FaqResource::collection($faqDetails)
                    ]);
                }
            }

            if(count($data) == 0)
                throw new RecordNotFoundException('No FAQs Found');
            return $data;
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
}
