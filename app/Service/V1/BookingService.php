<?php

namespace App\Service\V1;

use Carbon\Carbon;
use ReflectionClass;
use App\Enum\ChargeType;
use App\Enum\BookingStatus;
use App\Models\Notification;
use App\Exceptions as Exceptions;
use App\Service\V1\BookingService;
use App\Traits\BookingStatusTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log; 
use Intersoft\Auth\App\Enum\UserEnum;
use App\Http\Resources\BookingResource;
use App\Repository\Interfaces\IItemInterface;
use App\Repository\Interfaces\IShopRepository;
use App\Http\Resources\CalculateChargeResponse;
use App\Repository\Interfaces\IOrderRepository;
use App\Repository\Interfaces\ICouponRepository;
use App\Repository\Interfaces\IBookingRepository;
use App\Repository\Interfaces\ISettingRepository;
use App\Service\Interfaces\BookingServiceInterface;
use App\Repository\Interfaces\IUserAddressesRepository;
use IntersoftStripe\Http\Services\StripePaymentProcess;
use App\Repository\Interfaces\IVariousChargesRepository;
use IntersoftStripe\Exceptions\StripeSomeThingWentWrong;
use App\Repository\Interfaces\IFailedTransfersRepository;
use App\Repository\Interfaces\IBookingNotificationRepository;
use App\Http\Resources\BookingResourceWithBookingNotification;
use App\Models\AdminVendor;
use App\Models\BookingRejectReason;
use App\Models\VariousCharge;
use Intersoft\Auth\App\Helpers\V1\PushNotification;
use App\Repository\Interfaces\IBookingTrackingHistoryRepository;
use App\Repository\Interfaces\IVariousChargesOnBookingRepository;
use App\Models\AdminUser;

class BookingService implements BookingServiceInterface
{
    use BookingStatusTrait;
    const Delivery = 2;
    const Takeaway = 1;
    private $deliveryBoyAllowedStatus = [
        BookingStatus::DELIVERY_BOY_ASSIGNED,
        BookingStatus::CANCELLED_BY_DELIVERY_BOY,
        BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY,
        BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR,
        BookingStatus::DELIVERY_BOY_PICKED_ORDER,
        BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,
        BookingStatus::DELIVERED_PRODUCT
    ];

    private $customerAllowedStatus = [
        BookingStatus::CANCELLED_BY_CUSTOMER,
    ];

    private $bookingRepo;
    private $itemRepo;
    private $couponRepo;
    private $orderRepo;
    private $variousChargeRepo;
    private $shopRepo;
    private $userAddressRepo;
    private $variousChargeOnBookingRepo;
    private $bookingHistoryRepo;
    private $settingRepo;
    private $bookingNotificationRepo;
    private $failedTranRepo;

    public function __construct(
        IBookingRepository $bookingRepo,
        IItemInterface $itemRepo,
        ICouponRepository $couponRepo,
        IOrderRepository $orderRepo,
        IVariousChargesRepository $variousChargeRepo,
        IShopRepository $shopRepo,
        IUserAddressesRepository $userAddressRepo,
        IVariousChargesOnBookingRepository $variousChargeOnBookingRepo,
        IBookingTrackingHistoryRepository $bookingHistoryRepo,
        ISettingRepository $settingRepo,
        IBookingNotificationRepository $bookingNotificationRepo,
        IFailedTransfersRepository $failedTranRepo
    ) {
        $this->bookingRepo = $bookingRepo;
        $this->itemRepo = $itemRepo;
        $this->couponRepo = $couponRepo;
        $this->orderRepo = $orderRepo;
        $this->variousChargeRepo = $variousChargeRepo;
        $this->shopRepo = $shopRepo;
        $this->userAddressRepo = $userAddressRepo;
        $this->variousChargeOnBookingRepo = $variousChargeOnBookingRepo;
        $this->bookingHistoryRepo = $bookingHistoryRepo;
        $this->settingRepo = $settingRepo;
        $this->bookingNotificationRepo = $bookingNotificationRepo;
        $this->failedTranRepo = $failedTranRepo;
    }

    /**
     * Generate Unique Booking Code
     *
     * @return string
     */
    public function generateUniqueBookingCode(){
        $code = 'AS0DF1GH3JK2L4MN6BVC5X7ZQ9WERT8YUIOP';
        $randomString = '';
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $code[rand(0, 4 - 1)];
        }
        $randomString .= date('YmdHis');
        
        return $randomString;
    }

    /**
     * Create Booking
     *
     * @param [Request] $request
     * @return BookingResource
     */
    public function createBookingWithPayment($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            if($currentUser->user_type == UserEnum::DeliveryBoyUser){
                throw new Exceptions\BadRequestException("Sorry! you are not allowed to make booking");
            }
            
            // check if products passed are from the same shop, that is passed
            // check available stock based on the quantity
            
            $settings = $this->settingRepo->getSettings();
            $totalProductsPrice = 0;
            $amountGst = 0;
            $totalProductsOriPrice = 0;
            $bookingOrderData = [];
            $bookingVariousCharges = [];
            $outOfStockError = [];
            foreach ($request->products as $product) {
                $productDetail = $this->itemRepo->getProductDetailById($product['id']);
                if (!$productDetail) {
                    throw new Exceptions\BadRequestException("Product " . $product['id'] . " not found");
                }
                if($productDetail->shop_id != $request->shopId){
                    throw new Exceptions\BadRequestException("Product shop does not belong to passed shop");
                }
                if ($product['quantity'] > $productDetail->stock_quantity) {
                    
                    $data = [
                        'productName' => $productDetail->product_name,
                        'productDescription' => $productDetail->product_description,
                        'quantity_available' => $productDetail->stock_quantity,
                        'quantity_requested' => $product['quantity'],
                        'msg' => "Product Required Quantity is less then available quantity",
                    ];
                    array_push($outOfStockError, $data);
                }
                $totalProductsOriPrice = $productDetail->new_price * $product['quantity'];
                $totalProductsPrice += $productDetail->new_price * $product['quantity'];
                $amountGst += ($productDetail->gst_enable==1) ? $totalProductsOriPrice * $productDetail->shop->gst /100 : 0.00;
                array_push($bookingOrderData, [
                    'bookingId' => "",
                    'productId' => $productDetail->id,
                    'productName' => $productDetail->product_name,
                    'productDescription' => $productDetail->product_description,
                    'quantity' => $product['quantity'],
                    'price' => $productDetail->price,
                    'discount' => $productDetail->discount,
                    'new_price' => $productDetail->new_price,
                    'gst_tax' =>($productDetail->gst_enable==1) ? $totalProductsOriPrice * $productDetail->shop->gst /100 : 0.00,
                    'total_price' => ($productDetail->gst_enable==1) ? $totalProductsOriPrice + $totalProductsOriPrice * $productDetail->shop->gst /100 : $productDetail->new_price
                ]);
            }

            if(!empty($outOfStockError)){
                throw new Exceptions\CustomValidationException("Product out of stock error!",$outOfStockError);
            }
            // dd($outOfStockError);

            $totalAmount = $totalProductsPrice;
            $totalAmountAfExtraCharge = $totalAmount;
            $allCharges = $this->calculateCharge(
                $request->couponId,
                $amountGst,
                $bookingVariousCharges,
                $totalAmount,
                $request->addressId,
                $request->shopId,
                $currentUser,
                $request->booking_type,
                null,
                null,
                $request
            );


            $couponDetail = null;
            if (isset($request->couponId)) {
                $couponDetail = $this->couponRepo->getCouponDetailById($request->couponId);
                if( ($request->shopId == null &&  $couponDetail->shopId != null))
                    throw new Exceptions\BadRequestException("Coupon is not a global coupon");
                if( $couponDetail->shopId != null && $request->shopId != null &&  ($couponDetail->shopId != $request->shopId))
                    throw new Exceptions\BadRequestException("Coupon is not a global coupon");
            }

            
            // sending whole payment to the admin only if payment is to be given at the time of booking
            $bookingCode = $this->generateUniqueBookingCode();
            $setting = $this->settingRepo->getSettings();
            if($setting && $setting->charge_payment_from_customer_booking_accept == 2){
                $stripe_secret_key = env('STRIPE_SECRET_KEY');
                $payment = new StripePaymentProcess($stripe_secret_key);
                try{
                    $tip = isset($request->addressId) ? (isset($request->tip_to_delivery_boy )? $request->tip_to_delivery_boy :  0) : 0;
                    $payment = $payment->IntentPayment($allCharges['after_charge_amount'] + $tip, $request->card_id, "CAD", $currentUser,$bookingCode);
                }
                catch(\Exception $ex){
                    
                }
                if( ! isset($payment->payment_intent_id)){
                    throw new Exceptions\BadRequestException("Sorry Something went wrong! Payment Failed!");
                }
            }
            $bs_amount_to_vendor_before_commi = isset($couponDetail) ? (isset($couponDetail->shopId) ? $allCharges['amount_after_discount'] : $allCharges['amount_before_discount']) : $allCharges['amount_before_discount'];

            $amount_paid_to_delivery_boy_on_order_delivered = "";
            $amount_paid_to_delivery_boy_on_order_pickup = "";

            $flatChargeOnOrderPickup = $this->variousChargeRepo->getChargeById(ChargeType::FlatChargeOrderPickDeliveryBoy);
            $flatChargeOnOrderDelivered = $this->variousChargeRepo->getChargeById(ChargeType::FlatChargeOrderDeliveredDeliveryBoy);
            $amount_paid_to_delivery_boy_on_order_delivered = ($request->booking_type==2) ? ($flatChargeOnOrderDelivered->value ? $flatChargeOnOrderDelivered->value : 0.00) : 0.00;
            $amount_paid_to_delivery_boy_on_order_pickup = ($request->booking_type==2) ? ($flatChargeOnOrderPickup->value ? $flatChargeOnOrderPickup->value : 0.00) : 0.00;
            
            $data = [
                "userId" => $currentUser->id,
                "booking_code" => $bookingCode,
                "shopId" => $request->shopId,
                "addressId" => $request->addressId,
                'after_charge_amount' => $allCharges['after_charge_amount'],
                'commission_charge' => $allCharges['commission_charge'],
                'platform_charge' => $allCharges['platform_charge'],
                'delivery_charge_to_delivery_boy' => $allCharges['delivery_charge_to_delivery_boy'],
                'delivery_charge_for_customer' => $allCharges['delivery_charge_for_customer'],
                'amount_after_discount' => $allCharges['amount_after_discount'],
                'discount_amount' => $allCharges['discount_amount'],
                'amount_before_discount' => $allCharges['amount_before_discount'],
                'stripe_charges' => $allCharges['stripe_charges'],
                'amount_after_stripe_charges' => $allCharges['amount_after_stripe_charges'] + $request->tip_to_delivery_boy,
                'instruction' => $request->instruction,
                'tip_to_delivery_boy' => $request->tip_to_delivery_boy,
                "bookingType" => $request->booking_type,
                "bookingDateTime" => isset($request->scheduled_time) ? $request->scheduled_time : null,
                "couponId" => isset($request->couponId) ? $request->couponId : null,
                "status" => BookingStatus::ORDER_PLACED,

                "bs_amount_to_vendor_before_commi" => $bs_amount_to_vendor_before_commi,
                "bs_comm_charged_to_vendor" => $allCharges['commission_charge'],
                "bs_amount_to_vendor_after_commi" => $bs_amount_to_vendor_before_commi - $allCharges['commission_charge'] + $allCharges['gst_tax'] + $allCharges['pst_tax'] + $allCharges['hst_tax'],
                "bs_total_paid_to_admin" => $allCharges['amount_after_stripe_charges']+$request->tip_to_delivery_boy,
                "ws_deliverycharge_amount_to_db" => $allCharges['delivery_charge_to_delivery_boy'],
                "ws_tip_to_db" => $request->tip_to_delivery_boy,
                "ws_total_amount_to_db" => $allCharges['delivery_charge_to_delivery_boy'] + $request->tip_to_delivery_boy + $amount_paid_to_delivery_boy_on_order_pickup + $amount_paid_to_delivery_boy_on_order_delivered,
                "as_total_amount_to_admin" => $allCharges['amount_after_stripe_charges'] - ($allCharges['delivery_charge_to_delivery_boy'] + $amount_paid_to_delivery_boy_on_order_pickup + $amount_paid_to_delivery_boy_on_order_delivered) - ($bs_amount_to_vendor_before_commi - $allCharges['commission_charge']) - ($allCharges['gst_tax'] + $allCharges['pst_tax'] + $allCharges['hst_tax']),
                "timezone" => $request->header('timezone'),
                // if payment is made now then active will be 1 else it will be 0
                "active" => isset($setting) ? ($setting->charge_payment_from_customer_booking_accept == 2 ? 1 : 0) : 0,
                "amount_paid_to_delivery_boy_on_order_pickup" => $amount_paid_to_delivery_boy_on_order_pickup,
                "amount_paid_to_delivery_boy_on_order_delivered" => $amount_paid_to_delivery_boy_on_order_delivered,
                "hst"=>$allCharges['hst_tax'],
                "pst"=>$allCharges['pst_tax'],
            ];
            // insert data in booking_detail table
            $booking = $this->bookingRepo->createBookingWithPayment($data);
            foreach ($request->products as $product) {
                $productDetail = $this->itemRepo->getProductDetailById($product['id']);
                $productDetail->stock_quantity = $productDetail->stock_quantity - $product['quantity'];
                $productDetail->save();
            }
            // update all booking keys in all orders
            foreach ($bookingOrderData as $key => $order) {
                $bookingOrderData[$key]['bookingId'] = $booking->id;
            }
            // insert data in order_detail table
            $order = $this->orderRepo->createOrdersBasedOnBooking($bookingOrderData);
            foreach ($bookingVariousCharges as $key => $order) {
                $bookingVariousCharges[$key]['bookingId'] = $booking->id;
            }
            $this->variousChargeOnBookingRepo->createChargeBasedOnBooking($bookingVariousCharges);
            $this->bookingHistoryRepo->createBookingHistory(
                $booking->id,
                $currentUser->id,
                $this->generateTextMessageBasedUponBookingStatus(BookingStatus::ORDER_PLACED),
                $this->generateTextMessageBasedUponBookingConstantName($this->generateTextMessageBasedUponBookingStatus(BookingStatus::ORDER_PLACED)),
                1,
                1
            );
            $bookingDetails = $this->bookingRepo->getBookingByBookingId($booking->id);
            // if($bookingDetails->active) // means payment is done
                Notification::createNotificationNew(Auth::user()->id,$currentUser->id,"Order Done Successfully!","Dear ". Auth::user()->name.", Your order #". $bookingCode ." placed successfully.",BookingStatus::ORDER_PLACED);
            // else // means payment is done
            //     Notification::createNotificationNew(Auth::user()->id,$currentUser->id,"Order Done Successfully!","Dear Customer, Your order is done but your payment is still pending.",BookingStatus::ORDER_PLACED);
            
            return new BookingResource($bookingDetails);
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    public function updateBookingAfterPayment($request)
    {

    }
    /**
     * Returns Booking Details of a particular booking
     *
     * @param [Request] $request
     * @return BookingResource
     */
    public function getParticularBookingDetail($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $bookingDetails = $this->bookingRepo->getBookingByBookingId($request->id);
            return $bookingDetails;
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    /**
     * Returns Customer bookings based upon the status of the booking
     *
     * @param [Request] $request
     * @return BookingCollection
     */
    public function getCustomerBooking($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $status = array();
            // status : 1 => ORDER_ACCEPTED, 2 => ORDER_REJECTED, 3 => ORDER_COMPLETED, 4 => ORDER_INPROGRESS, 5 => PENDING_ORDERS
            foreach($request->status as $stat){
                if($stat == 1){
                    array_push($status,BookingStatus::ORDER_ACCEPTED_BY_VENDOR);
                    array_push($status,BookingStatus::ORDER_IN_PROGRESS);
                    array_push($status,BookingStatus::DELIVERY_BOY_ASSIGNED);
                    array_push($status,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY);
                    array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR);
                    array_push($status,BookingStatus::DELIVERY_BOY_PICKED_ORDER);
                    array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER);
                }
                elseif($stat == 2){
                    array_push($status,BookingStatus::ORDER_REJECTED_BY_VENDOR);
                    array_push($status,BookingStatus::CANCELLED_BY_DELIVERY_BOY);
                    array_push($status,BookingStatus::CANCELLED_BY_CUSTOMER);
                    array_push($status,BookingStatus::AUTO_CANCELLED);
                    
                }
                elseif($stat == 3){
                    array_push($status,BookingStatus::DELIVERED_PRODUCT);
                }
                elseif($stat == 4){
                    array_push($status,BookingStatus::ORDER_IN_PROGRESS);
                    array_push($status,BookingStatus::DELIVERY_BOY_ASSIGNED);
                    array_push($status,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY);
                    array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR);
                    array_push($status,BookingStatus::DELIVERY_BOY_PICKED_ORDER);
                    array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER);
                }
                elseif($stat == 5){
                    array_push($status,BookingStatus::ORDER_PLACED);
                }
            }
            $status = array_unique($status);
            $bookingDetails = $this->bookingRepo->getBookingsByUserIdAndBookingStatus($status,$currentUser->id,$request->limit,$request->page);
            return $bookingDetails;
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    /**
     * Responsible for validating coupon
     *
     * @param [Coupon] $coupon
     * @param [float] $originalPrice
     * @param [User] $user
     * @param [Timezone] $timezone
     * @return void
     */
    public function validateCoupon($coupon, $originalPrice, $user = null,$timezone)
    {
        $failed = false;
        $failedMessage = "";
        $couponId = $coupon->id;

        if (!$user) {
            $userUsedCouponTimes = $this->bookingRepo->checkUserUsedCouponTimes($user, $coupon->id);
            if ($userUsedCouponTimes >= $coupon->maximum_per_customer_use) {
                $failed = true;
                $failedMessage = "You have already used coupon many Times";
            }
        }

        $totalUsedCouponTimes = $this->bookingRepo->checkTotalUsedCouponTimes($coupon->id);
        if ($totalUsedCouponTimes >= $coupon->maximum_total_use) {
            $failed = true;
            $failedMessage = "Coupon Already Used Many Times";
        }

        if ($originalPrice < $coupon->coupon_min_amount) {
            $failed = true;
            $failedMessage = "Original Amount is less then Min Amount";
        }
        if ($originalPrice > $coupon->coupon_max_amount) {
            $failed = true;
            $failedMessage = "Original Amount is greater then Max Amount";
        }
        $currentDate = Carbon::parse(date('Y-m-d H:i:s'))->timezone($timezone)->format("Y-m-d H:i:s");
        if ($currentDate < $coupon->start_date) {
            $failed = true;
            $failedMessage = "Coupon not  yet available to be applied ";
        }
        if ($currentDate > $coupon->end_date) {
            $failed = true;
            $failedMessage = "Coupon expired";
        }
        if($coupon->coupon_type == 1){
            $discountedAmount = $coupon->coupon_discount;
            if($discountedAmount > $originalPrice){
                $failed = true;
                $failedMessage = "Original Amount is less then Coupon Amount";
            }
        }

        $discountedAmount = 0.00;
        $finalAmount = 0.00;

        if (!$failed) {
            if ($coupon->coupon_type == 1) {
                $discountedAmount = $coupon->coupon_discount;
                $finalAmount = $originalPrice - $discountedAmount;
            }
            if ($coupon->coupon_type == 0) {
                $discountedAmount = $originalPrice * ($coupon->coupon_discount / 100);
                $finalAmount = $originalPrice - $discountedAmount;
            }
            $data = [
                'couponId' => $coupon->id,
                'code' => $coupon->coupon_code,
                'original_amount' => $originalPrice,
                'discounted_amount' => $discountedAmount,
                'final_amount' => $finalAmount,
            ];

            return $data;
        }
        throw new Exceptions\BadRequestException($failedMessage);
    }

    /**
     * Calculates distance Between Shop and Location
     *
     * @param [Request] $request
     * @return integer
     */
    public function getDistanceBetweenShopAndLocation($request)
    {
        return $this->calculateDistanceBetweenShopAndLocation($request->shopId, $request->addressId);
    }

    /**
     * Returns Delivery Charges based upon shop location and user location
     *
     * @param [type] $request
     * @return void
     */
    public function getDeliveryChargeBasedOnShopLocationAndLocation($request)
    {
        $deliveryChargeAmount = 0;
        $deliveryCharge = $this->variousChargeRepo->getChargeById(1); // 1 means Delivery Charge
        if ($deliveryCharge->id == 1) { // 1 means delivery Charge
            if (isset($request->addressId) || (isset($request->lat) && isset($request->lng))) {
                // dd($request);
                if(isset($request->addressId))
                    $distance = $this->calculateDistanceBetweenShopAndLocation($request->shopId, $request->addressId)['distance'];
                if(isset($request->lat) && isset($request->lng))
                    $distance = $this->calculateDistanceBetweenShopAndLocation($request->shopId, null,$request->lat, $request->lng)['distance'];

                if ($deliveryCharge->type == 1) {
                    $deliveryChargeAmount += $distance * $deliveryCharge->value;
                } elseif ($deliveryCharge->type == 0) {
                    throw new Exceptions\BadRequestException("Delivery Charge is set to percentage. Please set to Fixed Value");
                }

            }
        }

        $data = [
            "deliveryCharges" => $deliveryChargeAmount,
        ];
        return $data;
    }
    /**
     * Calculates distance between the shop and location
     *
     * @param [integer] $shopId
     * @param [integer] $addressId
     * @param [string] $lat
     * @param [string] $lng
     * @return integer
     */
    public function calculateDistanceBetweenShopAndLocation($shopId, $addressId=null, $lat = null, $lng=null)
    {
        $shopDetails = $this->shopRepo->getShopDetailById($shopId);
        // dd($addressId);
        if(isset($addressId)){
            $addressDetails = $this->userAddressRepo->getAddressDetailsById($addressId);
            $shopDistance = $this->shopRepo->getDistanceFromShopToPassedAddress($shopDetails, $addressDetails);
        }
        else{
            $shopDistance = $this->shopRepo->getDistanceFromShopToPassedLatLong($shopDetails,$lat, $lng );
        }
        $data = [
            'shopId' => $shopDistance->id,
            'distance' => $shopDistance->distance,
        ];
        return $data;
    }
    public function sendNotificationAfterBookingUpdation($status,$userId, $booking){
        $username = "";
        if(Auth::user()->user_type == 1){
            $username = $booking->userDetails->name ?? "";
        }
        else{
            $username = $booking->deliveryBoyDetails->name ?? "";
        }
        if($status == BookingStatus::DELIVERY_BOY_ASSIGNED){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Delivery Boy Assigned.","Dear Customer, ". ($booking->deliveryBoyDetails->name ?? "") ." Assigned for your order #".$booking->booking_code.".",BookingStatus::DELIVERY_BOY_ASSIGNED);
        }
        if($status == BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Delivery Boy Started the journey.","Dear Customer, ". ($booking->deliveryBoyDetails->name ?? "") ." on his way to pick the order #".$booking->booking_code.".",BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY);
        }
        if($status == BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Delivery Boy Reached to Vendor".".","Dear Customer,  ". ($booking->deliveryBoyDetails->name ?? "") ." reached to  ". ($booking->shopDetails->shop_name ?? "") ." to pick order #".$booking->booking_code.".",BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR);
        }
        if($status == BookingStatus::DELIVERY_BOY_PICKED_ORDER){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Delivery Boy Picked up order.","Dear Customer, ". ($booking->deliveryBoyDetails->name ?? "") ." Picked the order #".$booking->booking_code.".",BookingStatus::DELIVERY_BOY_PICKED_ORDER);
        }
        if($status == BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Delivery Boy is almost their!","Dear Customer, ". ($booking->deliveryBoyDetails->name ?? "") ." on his way to deliver the order #".$booking->booking_code.".",BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER);
        }
        if($status == BookingStatus::DELIVERED_PRODUCT){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"Congratulations! Your order delivered successfully.","Dear Customer, Your order #".$booking->booking_code." delivered successfully!",BookingStatus::DELIVERED_PRODUCT);
        }
        if($status == BookingStatus::CANCELLED_BY_CUSTOMER){
            if(isset($booking->deliveryBoyId)){
                Notification::createNotificationNew(Auth::user()->id,$booking->deliveryBoyId,"The order cancelled by customer!","Dear ". ($booking->deliveryBoyDetails->name ?? "") .", Your order #".$booking->booking_code." cancelled by customer!",BookingStatus::CANCELLED_BY_CUSTOMER);
            }
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"The order cancelled successfully!","Dear Customer, Your order #".$booking->booking_code." cancelled!",BookingStatus::CANCELLED_BY_CUSTOMER);
            
        }
        if($status == BookingStatus::CANCELLED_BY_DELIVERY_BOY){
            Notification::createNotificationNew(Auth::user()->id,$booking->userId,"The order cancelled by delivery boy.","Dear Customer, Your order #".$booking->booking_code." cancelled by ". ($booking->deliveryBoyDetails->name ?? "") .".",BookingStatus::CANCELLED_BY_DELIVERY_BOY);
        }
    }
    /**
     * Update Booking Status
     *
     * @param [Request] $request
     * @return BookingResource
     */
    public function updateBookingByDeliveryBoyOrByCustomer($request)
    {
        $user = Auth::user();
        
        $bookingDetails = $this->bookingRepo->getBookingByBookingId($request->booking_id);
        
        if (in_array($bookingDetails->status, [BookingStatus::ORDER_REJECTED_BY_VENDOR, BookingStatus::CANCELLED_BY_CUSTOMER, BookingStatus::CANCELLED_BY_DELIVERY_BOY, BookingStatus::AUTO_CANCELLED])) {
            throw new Exceptions\BadRequestException("Action not allowed");
        }
        if (in_array($bookingDetails->status, [BookingStatus::DELIVERY_BOY_PICKED_ORDER,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,BookingStatus::DELIVERED_PRODUCT])) {
            if (in_array($request->status, [BookingStatus::CANCELLED_BY_CUSTOMER,BookingStatus::CANCELLED_BY_DELIVERY_BOY])) {
                throw new Exceptions\BadRequestException("Sorry! You can't cancel the order now. Delivery Boy already picked up the order");
            }
        }
        if ($user->user_type == UserEnum::DeliveryBoyUser) {
            if($bookingDetails->bookingType == BookingService::Takeaway){
                throw new Exceptions\BadRequestException("Sorry! You can't accept this order as this is Pickup order");
            }
            if (in_array($request->status, $this->deliveryBoyAllowedStatus)) {
                if($request->status == BookingStatus::DELIVERY_BOY_ASSIGNED){
                    $bookingInHand = $this->bookingRepo->getBookingsByDeliveryBoyId($user->id)->count();
                    $setting = $this->settingRepo->getSettings();
                    $maxAcceptanceLimit = $setting->max_accepting_limit ?? 1;
                    if($bookingInHand >= $maxAcceptanceLimit){
                        throw new Exceptions\BadRequestException("Sorry! You can't accept this order as you already have reached maximum booking limit");
                    }
                    
                    $this->bookingNotificationRepo->updateAcceptStatus($bookingDetails->id,$user->id);
                    
                    $bookingDetails->update([
                        'deliveryBoyId' => $user->id
                        
                    ]);
                    $admin_user_id = AdminUser::where('email', env('SUPER_ADMIN_EMAIL'))->first();
                    $vendor_user_data = AdminVendor::where('id', $bookingDetails->shopId)->first();
                    
                    PushNotification::sendAdminPushNotification($admin_user_id->id, 'Delivery Boy Assigned', "Delivery Boy Assigned for Booking with Booking code #$bookingDetails->booking_code ", 1);
                    $vendor_user_id = AdminUser::where('id', $vendor_user_data->user_id)->first();
                    PushNotification::sendAdminPushNotification($vendor_user_id->id, 'Delivery Boy Assigned', "Delivery Boy Assigned for Booking with Booking code #$bookingDetails->booking_code ", 1);
                    // $notificationsToSent = $this->bookingNotificationRepo->sendNotificationToAllDBAcceptCurrDB($bookingDetails->id,$user->id);

                    // Sending Notification to all the delivery boy that another delivery boy has accepted the order
                    $notificationsToSent = $this->bookingNotificationRepo->sendNotificationToAllDBExceptCurrDB($bookingDetails->id,$user->id);
                    
                    foreach($notificationsToSent as $notificationRow){
                        $notificationRow->expiration_time = date("Y-m-d H:i:s");
                        $notificationRow->save();
                        $username = "";
                        if(Auth::user()->user_type == 1){
                            $username = $booking->userDetails->name ?? "";
                        }
                        else{
                            $username = $booking->deliveryBoyDetails->name ?? "";
                        }
                        Notification::createNotificationNew(Auth::user()->id,$notificationRow->deliveryboy_id,"Another Delivery Boy Assigned!","Dear Delivery Boy, ". Auth::user()->name ." assigned for the order #" .$bookingDetails->booking_code.".",BookingStatus::DELIVERY_BOY_ASSIGNED);
                    }
                }
                if($request->status == BookingStatus::CANCELLED_BY_DELIVERY_BOY){
                    if( ! isset($bookingDetails->deliveryBoyId)){
                        // then it means we have to just expire the time for that delivery boy
                        $this->bookingNotificationRepo->updateExpirationTime($bookingDetails->id, $user->id, date("Y-m-d H:i:s"));
                    }
                }
                
                if($request->status != BookingStatus::CANCELLED_BY_DELIVERY_BOY){
                    $bookingDetails->update([
                        'status' => $request->status,
                    ]);

                    // refund full amount
                    if (in_array($request->status, [BookingStatus::CANCELLED_BY_DELIVERY_BOY])) {
                        $this->refundPaymentByBooking($bookingDetails);
                        $this->refundStockOfBooking($bookingDetails);
                    }
                    if($request->status == BookingStatus::DELIVERED_PRODUCT){
                        $this->transferMoneyToVendorAndDeliveryBoy($bookingDetails);
                    }
                    $this->sendNotificationAfterBookingUpdation($request->status , $user->id, $bookingDetails);
                    
                    $this->bookingHistoryRepo->createBookingHistory(
                        $bookingDetails->id,
                        $user->id,
                        $this->generateTextMessageBasedUponBookingStatus($request->status),
                        $this->generateTextMessageBasedUponBookingConstantName($this->generateTextMessageBasedUponBookingStatus($request->status)),
                        1,
                        $request->status
                    );
                }
            } else {
                
                throw new Exceptions\BadRequestException("Action not allowed");
            }
        } else {
            if (in_array($request->status, $this->customerAllowedStatus)) {
                if (in_array($bookingDetails->status, [BookingStatus::DELIVERY_BOY_PICKED_ORDER,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,BookingStatus::DELIVERED_PRODUCT])) {
                    if (in_array($request->status, [BookingStatus::CANCELLED_BY_CUSTOMER,BookingStatus::CANCELLED_BY_DELIVERY_BOY])) {
                        throw new Exceptions\BadRequestException("Sorry! You can't cancel the order now. Delivery Boy already picked up the order");
                    }
                }
                $previousStatus = $bookingDetails->status;
                $bookingDetails->update([
                    'status' => $request->status,
                ]);

                if($request->status==BookingStatus::CANCELLED_BY_CUSTOMER && $request->reject_reason==null)
                {
                    throw new Exceptions\BadRequestException("Please enter reason for cancellation of order.");
                }
                else
                {
                    BookingRejectReason::create([
                        'booking_id'=>$request->booking_id,
                        'reason'=>$request->reject_reason
                    ]);
                }


                $admin_user_id = AdminUser::where('email', env('SUPER_ADMIN_EMAIL'))->first();
                $vendor_user_data = AdminVendor::where('id', $bookingDetails->shopId)->first();
                PushNotification::sendAdminPushNotification($admin_user_id->id, 'Customer Booking Cancel', "Booking with Booking code #$bookingDetails->booking_code has been cancelled by Customer", 1);
                $vendor_user_id = AdminUser::where('id', $vendor_user_data->user_id)->first();
                PushNotification::sendAdminPushNotification($vendor_user_id->id, 'Customer Booking Cancel', "Booking with Booking code #$bookingDetails->booking_code has been cancelled by Customer", 1);


                $this->sendNotificationAfterBookingUpdation($request->status , $user->id, $bookingDetails);
                // refund full amount
                if (in_array($request->status, [BookingStatus::CANCELLED_BY_CUSTOMER])) {

                    // If booking is cancelled by customer and no delivery boy has accepted the order then send notification to all of the db to whom notification was sent
                    
                    if (in_array($previousStatus, [BookingStatus::ORDER_IN_PROGRESS,BookingStatus::ORDER_ACCEPTED_BY_VENDOR])) {
                        $notificationsToSent = $this->bookingNotificationRepo->sendNotificationToAllDBForParticularBooking($bookingDetails->id);
                        
                        foreach($notificationsToSent as $notificationRow){
                            $notificationRow->expiration_time = date("Y-m-d H:i:s");
                            $notificationRow->save();
                            Notification::createNotificationNew(Auth::user()->id,$notificationRow->deliveryboy_id,"The order cancelled by customer!","Dear ". ($notificationRow->userDetail->name ?? "") .", Your order #".$bookingDetails->booking_code." cancelled by customer!",BookingStatus::CANCELLED_BY_CUSTOMER);
                        }
                    }
                    $this->refundPaymentByBooking($bookingDetails);
                    $this->refundStockOfBooking($bookingDetails);
                }
                $this->bookingHistoryRepo->createBookingHistory(
                    $bookingDetails->id,
                    $user->id,
                    $this->generateTextMessageBasedUponBookingStatus($request->status),
                    $this->generateTextMessageBasedUponBookingConstantName($this->generateTextMessageBasedUponBookingStatus($request->status)),
                    1,
                    $request->status
                );
            } else {
                throw new Exceptions\BadRequestException("Action not allowed");
            }
        }

        $bookingDetails = $this->bookingRepo->getBookingByBookingId($request->booking_id);
        return $bookingDetails;
    }
    /**
     * Calculate Charge
     *
     * @param [Request] $request
     * @return CalculateChargeResponse
     */
    public function calculateChargeAPI($request)
    {
        $currentUser = Auth::user();
        // check if products passed are from the same shop, that is passed
        // check available stock based on the quantity
        $settings = $this->settingRepo->getSettings();

        $coupon = $this->couponRepo->getCouponDetailByCouponId($request->coupon_id);
        if( ($request->shop_id == null &&  $coupon->shopId != null))
            throw new Exceptions\BadRequestException("Coupon is not a global coupon");
        if( isset($coupon) && $coupon->shopId != null && $request->shop_id != null &&  ($coupon->shopId != $request->shop_id))
            throw new Exceptions\BadRequestException("Coupon is not a global coupon");

        $outOfStockError = [];
        $totalProductsPrice = 0;
        $amountGst = 0;
        $totalProductsOriPrice = 0;
        $bookingOrderData = [];
        $bookingVariousCharges = [];
        foreach ($request->products as $product) {
            $productDetail = $this->itemRepo->getProductDetailById($product['id']);
            if (!$productDetail) {
                throw new Exceptions\BadRequestException("Product " . $product['id'] . " not found");
            }
            if($productDetail->shop_id != $request->shop_id){
                throw new Exceptions\BadRequestException("Product shop does not belong to passed shop");
            }
            if ($product['quantity'] > $productDetail->stock_quantity) {
                $data = [
                    'productName' => $productDetail->product_name,
                    'productDescription' => $productDetail->product_description,
                    'quantity_available' => $productDetail->stock_quantity,
                    'quantity_requested' => $product['quantity'],
                    'msg' => "Product Required Quantity is less then available quantity",
                ];
                array_push($outOfStockError, $data);
            }
            $totalProductsOriPrice = $productDetail->new_price * $product['quantity'];
            $totalProductsPrice += $productDetail->new_price * $product['quantity'];
            $amountGst += ($productDetail->gst_enable==1) ? $totalProductsOriPrice * $productDetail->shop->gst /100 : 0.00;
            array_push($bookingOrderData, [
                'bookingId' => "",
                'productId' => $productDetail->id,
                'productName' => $productDetail->product_name,
                'productDescription' => $productDetail->product_description,
                'quantity' => $product['quantity'],
                'price' => $productDetail->price,
                'gst_tax' =>($productDetail->gst_enable==1) ? $totalProductsOriPrice * $productDetail->shop->gst /100 : 0.00,
                'total_price' => ($productDetail->gst_enable==1) ? $totalProductsOriPrice + $totalProductsOriPrice * $productDetail->shop->gst /100 : $productDetail->new_price
            ]);
        }
        
        if(!empty($outOfStockError)){
            throw new Exceptions\CustomValidationException("Product out of stock error!",$outOfStockError);
        }
        $totalAmount = $totalProductsPrice;
        $totalAmountAfExtraCharge = $totalAmount;

        $couponId = $request->coupon_id;
        $addressId = $request->address_id;
        $shopId = $request->shop_id;
        $currentUser = Auth::user()->id;

        return new CalculateChargeResponse($this->calculateCharge(
            $couponId,
            $amountGst,
            $bookingVariousCharges,
            $totalAmount,
            $addressId,
            $shopId,
            $currentUser,
            $request->booking_type,
            $request->lat,
            $request->lng,
            $request
        ));
    }
    /**
     * Calculate Charge
     *
     * @param [integer] $couponId
     * @param [array] $bookingVariousCharges
     * @param [float] $totalAmount
     * @param [integer] $addressId
     * @param [integer] $shopId
     * @param [User] $currentUser
     * @param [integer] $bookingType
     * @param [string] $latInCaseNotAddressId
     * @param [string] $lngInCaseNotAddressId
     * @return void
     */
    public function calculateCharge(
        $couponId,
        $amountGst,
        $bookingVariousCharges,
        $totalAmount,
        $addressId,
        $shopId,
        $currentUser,
        $bookingType,
        $latInCaseNotAddressId,
        $lngInCaseNotAddressId,
        $request
    ) {
        $bookingType = $bookingType ?? BookingService::Takeaway;
        $totalAmountAfExtraCharge = $totalAmount;
        $totalAmountBeforeDiscount = $totalAmount;
        $discountAmount = 0;
        if (isset($couponId)) {
            $couponDetail = $this->couponRepo->getCouponDetailById($couponId);
            if ($couponData = $this->validateCoupon($couponDetail, $totalAmountBeforeDiscount, $currentUser,$request->header('timezone') ?? "Asia/Kolkata" )) { //validate coupon based on date time, max use, product price
                $discountAmount = $couponData['discounted_amount'];
            }
        }
        $totalAmount = ($totalAmountBeforeDiscount - $discountAmount) > 0 ? ($totalAmountBeforeDiscount - $discountAmount) : 0; // total price before adding any extra charges
        $totalAmountAfterDiscount = $totalAmount;
        $settings = $this->settingRepo->getSettings();
        $shop = AdminVendor::find($shopId);
        $taxtotal = '';
        if($shop->pst || $shop->hst)
        {
            $taxtotal = $shop->pst+$shop->hst;
        }
        $allCharges = [
            'amount_before_discount' => $totalAmountBeforeDiscount,
            'discount_amount' => round($discountAmount, 2),
            'amount_after_discount' => $totalAmountAfterDiscount,
            'delivery_charge_for_customer' => 0,
            'delivery_charge_to_delivery_boy' => 0,
            'platform_charge' => 0,
            'commission_charge' => 0,
            'after_charge_amount' => $totalAmountAfExtraCharge,
            'stripe_charges' => 0,
            'amount_after_stripe_charges' => $totalAmountAfExtraCharge,
            'payment_after_before' => isset($settings) ? $settings->charge_payment_from_customer_booking_accept : 1
        ];

        $totalAmountAfExtraCharge = $totalAmount;

        $variousCharges = $this->variousChargeRepo->getAllCharges();
        foreach ($variousCharges as $extraCharges) {
            if ($extraCharges->id == ChargeType::DeliveryChargeForCustomer) {
                if($bookingType == BookingService::Takeaway){
                    $allCharges['delivery_charge_for_customer'] = 0;
                    continue;
                }
                
                if (isset($addressId) || isset($latInCaseNotAddressId) || isset($lngInCaseNotAddressId)) {
                    if(isset($addressId))
                        $distance = $this->calculateDistanceBetweenShopAndLocation($shopId, $addressId)['distance'];
                    if(isset($latInCaseNotAddressId) && isset($lngInCaseNotAddressId) )
                        $distance = $this->calculateDistanceBetweenShopAndLocation($shopId, null,$latInCaseNotAddressId, $lngInCaseNotAddressId)['distance'];
                    $extraChargeAmount = 0;
                    if ($extraCharges->type == 1) {
                        $extraChargeAmount = round($distance * $extraCharges->value,2);
                    } elseif ($extraCharges->type == 0) {
                        $extraChargeAmount = round( $distance * ($totalAmount * $extraCharges->value) / 100 ,2);
                    }

                    $totalAmountAfExtraCharge += $extraChargeAmount;
                    array_push($bookingVariousCharges, [
                        'bookingId' => "",
                        'chargeName' => $extraCharges->chargeName,
                        'chargeAmount' => $extraChargeAmount,
                    ]);
                }
                $allCharges['delivery_charge_for_customer'] = $extraChargeAmount;
                continue;
            }
            if ($extraCharges->id == ChargeType::DeliveryChargeToDeliveryBoy) {
                if($bookingType ==  BookingService::Takeaway){
                    $allCharges['delivery_charge_to_delivery_boy'] = 0;
                    continue;
                }
                if (isset($addressId)) {
                    $distance = $this->calculateDistanceBetweenShopAndLocation($shopId, $addressId)['distance'];
                    $extraChargeAmount = 0;
                    if ($extraCharges->type == 1) {
                        $i=0;
                        $currentDistance=0;
                        $sum =0;
                        $totalDistance=$distance;
                        $calculatedDistance=$distance;
                        $data=$extraCharges->variouschargerelation;

                        foreach($data as $element){

                            $currentDistance = ($i==0 ? ($element->endRange > $totalDistance ? $totalDistance :
                            $element->endRange) : $data[$i-1]->endRange);

                            if($calculatedDistance < 0){
                                break;
                            }
                            if($i==0 ){
                                if(count($data) == ($i + 1) || $calculatedDistance < $element->endRange) {
                                    $sum += ($calculatedDistance )  * $data[$i]->price;
                                break;
                                }
                                $sum += $currentDistance* $data[$i]->price;

                                    $calculatedDistance -=  $data[$i]->endRange ;

                            }else{
                                $calculatedDistance -=  $data[$i]->endRange ;
                                if($calculatedDistance > 0 &&count($data) == ($i + 1)){
                                    $sum += ( $totalDistance - $currentDistance)  * $data[$i]->price;
                                }elseif($calculatedDistance < 0){
                                    $sum += ($calculatedDistance+$data[$i]->endRange )  * $data[$i]->price;
                                }else{
                                    $sum += ($data[$i]->endRange -$data[$i-1]->endRange )  * $data[$i]->price;
                                }
                            }
                            $i++;

                            }

                        $extraChargeAmount = round($sum, 2);
                    } elseif ($extraCharges->type == 0) {
                        $extraChargeAmount = round($distance * ($totalAmount * $extraCharges->value) / 100, 2);
                    }

                    // $totalAmountAfExtraCharge += $extraChargeAmount;
                    // array_push($bookingVariousCharges, [
                    //     'bookingId' => "",
                    //     'chargeName' => $extraCharges->chargeName,
                    //     'chargeAmount' => $extraChargeAmount,
                    // ]);
                }
                
                $allCharges['delivery_charge_to_delivery_boy'] = $extraChargeAmount;
                continue;
            }
            if ($extraCharges->id == ChargeType::PlatformCharge) {
                
                $shopPlatformCharges = $this->shopRepo->getShopDetailById($shopId)->platform_charges;
                $extraChargeAmount = 0;
                $extraChargeAmount = (float) number_format(isset($shopPlatformCharges) && $shopPlatformCharges != 0 ? $shopPlatformCharges : $extraCharges->value,2); // give priority to shop specific charges
                $totalAmountAfExtraCharge += $extraChargeAmount;
                array_push($bookingVariousCharges, [
                    'bookingId' => "",
                    'chargeName' => $extraCharges->chargeName,
                    'chargeAmount' => $extraChargeAmount,
                ]);
                $allCharges['platform_charge'] = $extraChargeAmount;
                continue;
            }
            if ($extraCharges->id == ChargeType::CommissionCharge) {
                $shopCommissionCharges = $this->shopRepo->getShopDetailById($shopId)->commission_charges;
                $extraChargeAmount = 0;
                // Please note that commission charges is always applied on cart value if coupon is not applied i.e. before discount
                // if coupon is vendor specific then we will calculate commission on discuonted price
                // if coupon is admin specific then we will calculate on cart price
                
                if (isset($couponId)) {
                    $couponDetail = $this->couponRepo->getCouponDetailById($couponId);
                    if(isset($couponDetail->shopId)){
                        $extraChargeAmount = (float) number_format( isset($shopCommissionCharges) && $shopCommissionCharges != 0 ? ($totalAmountAfterDiscount * $shopCommissionCharges) / 100 : ($totalAmountAfterDiscount * $extraCharges->value) / 100,2); // give priority to shop specific charges
                    }
                    else{
                        $extraChargeAmount = (float) number_format( isset($shopCommissionCharges) && $shopCommissionCharges != 0 ? ($totalAmountBeforeDiscount * $shopCommissionCharges) / 100 : ($totalAmountBeforeDiscount * $extraCharges->value) / 100,2); // give priority to shop specific charges
                    }
                }
                else{
                        $extraChargeAmount = (float) number_format( isset($shopCommissionCharges) && $shopCommissionCharges != 0 ? ($totalAmountBeforeDiscount * $shopCommissionCharges) / 100 : ($totalAmountBeforeDiscount * $extraCharges->value) / 100,2); // give priority to shop specific charges
                }
                
                // $totalAmountAfExtraCharge += $extraChargeAmount;
                // array_push($bookingVariousCharges, [
                //     'bookingId' => "",
                //     'chargeName' => $extraCharges->chargeName,
                //     'chargeAmount' => $extraChargeAmount,
                // ]);
                $allCharges['commission_charge'] = $extraChargeAmount;
                continue;
            }
        }
        $aftertaxamount = '';
        if($taxtotal)
        {
            $aftertaxamount = $amountGst + $totalAmountAfExtraCharge + $allCharges['amount_after_discount'] * $taxtotal /100;
        }
        
        $allCharges['after_charge_amount'] = round($aftertaxamount, 2);
        $stripe_secret_key = env('STRIPE_SECRET_KEY');
        $payment = new StripePaymentProcess($stripe_secret_key);
        $stripeCharges = $payment->get_stripe_fees("CAD", $allCharges['after_charge_amount']);
        $allCharges['stripe_charges'] = round($stripeCharges, 2);
        // Amount after stripe charge is the amount that goes to admin
        $allCharges['amount_after_stripe_charges'] = round($allCharges['after_charge_amount'] - $allCharges['stripe_charges'], 2);
        
        $allCharges['gst_tax'] = ($shop->gst) ? $amountGst : 0.00;
        $allCharges['pst_tax'] = ($shop->pst) ? $allCharges['amount_after_discount'] * $shop->pst /100 : 0.00;
        $allCharges['hst_tax'] = ($shop->hst) ? $allCharges['amount_after_discount'] * $shop->hst /100 : 0.00;
        
        return $allCharges;
    }

    public function makePaymentForBooking(){

    }
    /**
     * Refund Payment
     *
     * @param [Booking] $booking
     * @return void
     */
    public function refundPaymentByBooking($booking){
        $stripe_secret_key = env('STRIPE_SECRET_KEY');
        $payment = new StripePaymentProcess($stripe_secret_key);
        $refund = $payment->refund_payment_with_bookingcode($booking->booking_code);
        $username = "";
        if(Auth::user()->user_type == 1){
            $username = $booking->userDetails->name ?? "";
        }
        else{
            $username = $booking->deliveryBoyDetails->name ?? "";
        }
        Notification::createNotificationNew($booking->userId,Auth::user()->id,"Order Refund Initiated!","Dear $username, Your order refund is initiated for the order #".$booking->booking_code .".",BookingStatus::BOOKING_REFUND);
    }
    /**
     * Refund Stock of Booking
     *
     * @param [Booking] $booking
     * @return void
     */
    public function refundStockOfBooking($booking){
        $orderDetails = $this->orderRepo->getOrderDetailsByBookingId($booking->id);
        if($orderDetails->count() > 0){
            foreach($orderDetails as $order){
                $this->itemRepo->updateStockValue($order->productId, $order->quantity);
            }
        }
    }
    public function transferMoneyToVendorAndDeliveryBoy($booking){
        $stripe_secret_key = env('STRIPE_SECRET_KEY');
        $payment = new StripePaymentProcess($stripe_secret_key);

        // Transfer Money to Vendor
        if( !  $payment->transferCharges($booking->bs_amount_to_vendor_after_commi,$booking->shopDetails->stripe_user_id)){
            
            $this->failedTranRepo->failedTransfersCreate($booking->id,null, $booking->shopId);
        }

        // Transfer Money to Delivery Boy
        if( !  $payment->transferCharges($booking->ws_total_amount_to_db,$booking->deliveryBoyDetails->stripe_customer_id)){
            
            $this->failedTranRepo->failedTransfersCreate($booking->id,$booking->deliveryBoyId,null );
        }
    }
    
    /**
     * Delivery boy listing
     *
     * @param [Request] $request
     * @return BookingResource
     */
    public function getDeliveryBoyListing($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $status = array();
            // status : 1 => ACTIVE , 2 => INCOMING, 3 => COMPLETED
            if($request->status == 1){
                array_push($status,BookingStatus::DELIVERY_BOY_ASSIGNED);
                array_push($status,BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY);
                array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR);
                array_push($status,BookingStatus::DELIVERY_BOY_PICKED_ORDER);
                array_push($status,BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER);
                $bookingDetails = $this->bookingRepo->getActiveBookingsForDeliveryBoy($status,$currentUser->id,$request->limit,$request->page);
                return BookingResource::collection($bookingDetails);
            }
            elseif($request->status == 2){
                array_push($status,BookingStatus::ORDER_ACCEPTED_BY_VENDOR);
                array_push($status,BookingStatus::ORDER_IN_PROGRESS);
                $bookingDetails = $this->bookingNotificationRepo->getRequestBookingsForDeliveryBoy($status,$currentUser->id,$request->limit,$request->page);
                return BookingResourceWithBookingNotification::collection($bookingDetails);
                
            }
            elseif($request->status == 3){
                array_push($status,BookingStatus::DELIVERED_PRODUCT);
                array_push($status,BookingStatus::CANCELLED_BY_CUSTOMER);
                array_push($status,BookingStatus::CANCELLED_BY_DELIVERY_BOY);
                $bookingDetails = $this->bookingRepo->getDeliveredBookingsAndBookingStatus($status,$currentUser->id,$request->limit,$request->page);
                return BookingResource::collection($bookingDetails);
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    public function getDeliveryBoyListingMyEarning($request){
        $currentUser = Auth::user();
        $bookingDetails = $this->bookingRepo->getDeliveredBookingWithDateRange([BookingStatus::DELIVERED_PRODUCT],$currentUser->id,$request->start_date,$request->end_date);
        return BookingResource::collection($bookingDetails);
    }
}
