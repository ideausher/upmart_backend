<?php
namespace App\Models;

use App\Models\Shop;
use App\Models\User;
use App\Models\Coupon;
use App\Models\Review;
use App\Models\OrderDetail;
use App\Models\UserAddress;
use App\Models\BookingNotification;
use App\Models\BookingTrackingHistory;
use App\Http\Resources\BookingResource;
use App\Models\VariousChargesOnBooking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookingDetail extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'orderId',
        'userId',
        'shopId',
        'addressId',
        'cardId',
        'couponId',
        'status',
        'active',
        'bookingDateTime',
        'bookingType',
        'after_charge_amount',
        'commission_charge',
        'platform_charge',
        'delivery_charge_to_delivery_boy',
        'delivery_charge_for_customer',
        'amount_after_discount',
        'discount_amount',
        'amount_before_discount',
        'tip_to_delivery_boy',
        'instruction',
        'amount_after_stripe_charges',
        'stripe_charges',
        'booking_code',
        'bs_amount_to_vendor_before_commi',
        'deliveryBoyId',
        'bs_comm_charged_to_vendor',
        'bs_amount_to_vendor_after_commi',
        'bs_total_paid_to_admin',
        'ws_deliverycharge_amount_to_db',
        'ws_tip_to_db',
        'ws_total_amount_to_db',
        'as_total_amount_to_admin',
        'amount_paid_to_delivery_boy_on_order_pickup',
        'amount_paid_to_delivery_boy_on_order_delivered',
        'hst',
        'pst',
        'timezone'
    ];
        
    protected $dates = ['created_at','updated_at'];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function ordersDetails(){
        return $this->hasMany(OrderDetail::class, 'bookingId' ,'id')
                    ->with('productImage')
                    ->select('id','bookingId','productId','productName','productDescription','price','discount','new_price','quantity','gst_tax','total_price');
    }

    public function userDetails(){
        return $this->hasOne(User::class, 'id' ,'userId')
                    ->select('id','name','email','country_code','phone_number','country_iso_code','profile_picture');
    }

    public function deliveryBoyDetails(){
        return $this->hasOne(User::class, 'id' ,'deliveryBoyId')
                    ->select('id','name','email','country_code','phone_number','country_iso_code','profile_picture','stripe_customer_id');
    }
    
    public function shopDetails(){
        return $this->hasOne(Shop::class, 'id' ,'shopId')
                    ->select('id','user_id','phonenumber_with_countrycode','shop_name','shop_address','additional_address','description','lat','lng','shop_type','availability','stripe_user_id');
    }

    public function addressDetails(){
        return $this->hasOne(UserAddress::class, 'id' ,'addressId')
                    ->select('id','user_id','city','country','formatted_address','latitude','longitude','additional_info','pincode');
    }

    public function couponDetails(){
        return $this->hasOne(Coupon::class, 'id' ,'couponId')
                    ->select('id','coupon_name','coupon_code','coupon_description','isPrimary','coupon_type','coupon_discount');
    }

    public function chargeDetails(){
        return $this->hasMany(VariousChargesOnBooking::class, 'bookingId' ,'id')
                    ->select('id','bookingId','chargeName','chargeAmount');
    }

    public function trackingHistory(){
        return $this->hasMany(BookingTrackingHistory::class, 'bookingId' ,'id')
                    ->selectRaw('id,bookingId,reason,value,cast(COALESCE(status_value,"") as UNSIGNED) as status_value,cast(created_at as datetime) as action_time');
    }

    public function bookingNotificationsMinRatingByBooking(){
        return $this->hasMany(BookingNotification::class, 'booking_id' ,'id')
                    ->selectRaw('id,booking_id,deliveryboy_id,deliveryboy_rating,notification_message,deliveryboy_accept_status,expiration_time,MIN(deliveryboy_rating) as minRating')
                    ->groupBy('booking_id');
    }

    public function rejectedBookingNotifications(){
        return $this->hasMany(BookingNotification::class, 'booking_id' ,'id')
                    ->selectRaw('id,booking_id,deliveryboy_id,deliveryboy_rating,notification_message,deliveryboy_accept_status,expiration_time');
    }

    public function totalBookingNotifications(){
        return $this->hasMany(BookingNotification::class, 'booking_id' ,'id')
                    ->selectRaw('id,booking_id,deliveryboy_id,deliveryboy_rating,notification_message,deliveryboy_accept_status,expiration_time');
    }

    public function ratings(){
        return $this->hasMany(Review::class, 'booking_id' ,'id')
                    ->selectRaw('id,booking_id,shop_id,COALESCE(delivery_boy_id,"") as delivery_boy_id,COALESCE(customer_id,"") as customer_id,COALESCE(rating,"") as rating,COALESCE(customer_id_by,"") as customer_id_by,COALESCE(delivery_boy_id_by,"") as delivery_boy_id_by,COALESCE(shop_id_by,"") as shop_id_by , feedback');
    }

    public function rejectedbookings()
    {
        return $this->hasOne(BookingRejectReason::class,'booking_id','id');
    }
    
    public function setResource($data){
        return new BookingResource($data);
    }
}
