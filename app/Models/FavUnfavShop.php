<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavUnfavShop extends Model
{
    protected $table='fav_unfac_shops';
    
    protected $fillable = [
        'shop_id',
        'user_id',
        'isFavourite',
    ];
}
