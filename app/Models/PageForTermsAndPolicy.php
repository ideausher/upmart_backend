<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\PageForTermsAndPolicyResource;

class PageForTermsAndPolicy extends Model 
{
    protected $table = 'page_for_terms';
    
    protected $fillable = [ 
        'id',
        'page_id',
        'page_name',
        'text',
        'version',
    ];

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];

    public function setResource($data)
    {
        return new PageForTermsAndPolicyResource($data);
    }
}
