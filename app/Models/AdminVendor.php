<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;



class AdminVendor extends Model
{

    protected $table = 'admin_vendors';

    protected $fillable = [
        
        'country_code',
        'phonenumber_with_countrycode',
        'shop_name',
        'lat',
        'lng',
        'shop_address',
        'additional_address',
        'description',
        'shop_type',
        'platform_charges',
        'commission_charges'
    
    ];
    
    
    protected $dates = [
    
    ];
    public $timestamps = false;
    
    public function vendor()
    {
        return $this->belongsTo(AdminUsers::class,'user_id','id');
    }

    public function items()
    {
        return $this->hasMany(Item::class,'shop_id','id');
    }
}
