<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Item extends Model
{
    protected $fillable = [
        'Product_Name',
        'Product_Description',
        'Stock_Quantity',
        'Price',
        'Product_Images',
        'Category_Name',
        'Company_Name',
        'category_id',
        'shop_name',
        'shop_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function shop(){
        return $this->hasOne('App\Models\Shop', 'id', 'shop_id');
    }
    
    public function productImage(){
        return $this->hasOne('App\Models\Media', 'model_id', 'id')
                    ->where('model_type','App\Models\Item')
                    ->where('collection_name','product_images')
                    ->select('id','model_id','model_type','file_name','name');
    }
}
