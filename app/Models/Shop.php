<?php
namespace App\Models;

use App\Enum\CouponType;
use App\Http\Resources\ShopResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'admin_vendors';

    protected $fillable = [
        'shop_name',
        'shop_address',
        'description',
        'additional_address',
        'shop_rating',
    ];

    protected $dates = [
            'created_at',
            'updated_at',
        
        ];
    protected $attributes = ['rating','fav'];

    protected $appends = ['fav'];

    public function setShopRatingAttribute(){
        return $this->hasMany('App\Models\Review','shop_id','id');
    }
    public function getFavAttribute(){
        $favourite = $this->hasOne('App\Models\FavUnfavShop','shop_id','id')
        ->select('id','shop_id','user_id', 'isFavourite');
        
        if(Auth::guard('api')->check()){
            $favourite =$favourite->where('user_id',Auth::guard('api')->user()->id)->first();
            return isset($favourite) ? $favourite->isFavourite : 0;
        }
        else{
            return 0;
        }
        
    }
    public function setResource($data)
    {
        return new ShopResource($data);
    }
    public function media(){
        return $this->hasMany('App\Models\Media', 'model_id', 'user_id')
                    ->where('model_type','App\Models\AdminUsers')
                    ->where('collection_name','shop_image')
                    ->select('id','model_id','model_type','file_name','name');
    }
    public function image(){
        return $this->hasOne('App\Models\Media', 'model_id', 'user_id')
                    ->where('model_type','App\Models\AdminUsers')
                    ->where('collection_name','Gallery')
                    ->select('id','model_id','model_type','file_name','name');
    }
    public function primaryCoupon(){
        return $this->hasOne('App\Models\Coupon','shopId','id')
                    ->select("id","shopId","coupon_name as name","coupon_code as code","coupon_description as description","isPrimary as is_primary","coupon_type as type","coupon_discount as discount")
                    ->where('isPrimary', CouponType::PrimayCoupon)
                    ->where('end_date','>', date('Y-m-d'));
    }
    public function showOwner(){
        return $this->hasOne('App\Models\ShowOwner','id','user_id')
                    ->select("id","first_name","last_name","email","phonenumber_with_countrycode as phone_number");
    }

    public function showBanners(){
        return $this->hasMany('App\Models\Banner','merchant_id','user_id')
                    ->selectRaw("id,
                            merchant_id,
                            COALESCE(bannerName,'') as bannerName,
                            COALESCE(link,'') as link,
                            COALESCE(address,'') as address,
                            COALESCE(latitude,'') as latitude,
                            COALESCE(longitude,'') as longitude"
                        );
    }
}
