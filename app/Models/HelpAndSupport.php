<?php
namespace App\Models;

use App\Models\BookingDetail;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\HelpAndSupportResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HelpAndSupport extends Model
{
    use HasFactory;

    protected $fillable = [
        'booking_id',
        'user_id',
        'shop_id',
        'feedback_title',
        'feedback_description',
        'image',
        'ticket_id',
        'admin_reply',
        'query_status'
    ];

    public function setResource($data)
    {
        return new HelpAndSupportResource($data);
    }
    
    public function bookingDetail(){
        return $this->hasOne(BookingDetail::class, 'id', 'booking_id');
    }
}
