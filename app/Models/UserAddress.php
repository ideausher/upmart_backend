<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'user_id',
        'type',
        'city',
        'country',
        'formatted_address',
        'additional_info',
        'latitude',
        'longitude',
        'is_primary' ,
        'pincode',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
}
