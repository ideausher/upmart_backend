<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\DeliveryBoyResource;

class DeliveryboyTemporaryDocument extends Model 
{
    protected $table = 'deliveryboy_temporary_documents';
    
    protected $fillable = [
            'id',
            'user_id',
            'registeration_number',
            'vehicle_company',
            'vehicle_picture',
            'model',
            'color',
            'valid_upto',
            'vehicle_status',
            'vehicle_reason',
            'drivinglicence_images',
            'drivinglicence_no',
            'drivinglicence_expiry_date',
            'drivinglicence_status',
            'drivinglicence_reason',

            'licence_no_plate_images',
            'no_plate',
            'licence_no_plate_status',
            'licence_no_plate_reason',
            
            'vehicle_insurence_image',
            'policy_number',
            'vehicle_insurence_status',
            'vehicle_insurence_reason',
            'vehicle_insurence_valid_upto',
            'vehicle_insurence_valid_from'
        ];

    public $timestamps = false;

    protected $dates = [
            'deleted_at',
            'created_at',
            'updated_at',
        ];

    public function setResource($data)
    {
        return new DeliveryBoyResource($data);
    }
}
