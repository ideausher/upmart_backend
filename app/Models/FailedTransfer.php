<?php

namespace App\Models;

use App\Models\Shop;
use App\Models\User;
use App\Models\BookingDetail;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\FailedTransferResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FailedTransfer extends Model
{
    use HasFactory;
    
    protected $table = "failed_transfer";

    protected $fillable = [
        'booking_id',
        'delivery_boy_id',
        'vendor_id',
        'status '
    ];
        
    protected $dates = ['created_at','updated_at'];

    protected $casts = [
        'created_at' => 'date',
    ];
    
    public function bookingDetail(){
        return $this->hasOne(BookingDetail::class, 'id', 'booking_id');
    }
    public function setResource($data)
    {
        return new FailedTransferResource($data);
    }

}
