<?php
namespace App\Models;

use App\Models\FaqCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Faq extends Model
{
    use HasFactory;

    protected $table='faq';

    public function categoryDetail(){
        return $this->belongsTo(FaqCategory::class, 'category_id','id');
    }
}
