<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\DeliveryBoyResource;

class DeliveryboyMainDocument extends Model 
{
    protected $table = 'deliveryboy_main_documents';
    
    protected $fillable = [
            'id',
            'user_id',
            'registeration_number',
            'vehicle_company',
            'vehicle_picture',
            'model',
            'color',
            'valid_upto',

            'drivinglicence_images',
            'drivinglicence_no',
            'drivinglicence_expiry_date',

            'licence_no_plate_images',
            'no_plate',
            
            'vehicle_insurence_image',
            'policy_number'   
        ];

    public $timestamps = false;
        
    protected $dates = [
            'deleted_at',
            'created_at',
            'updated_at',
        
        ];
    public function setResource($data)
    {
        return new DeliveryBoyResource($data);
    }
    
    
    

}
