<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $table='table_banners_location_wise';

    public function bannerImage(){
        return $this->hasOne('App\Models\Media', 'model_id', 'id')
                    ->where('model_type','App\Models\TableBannersLocationWise')
                    ->where('collection_name','banner_image')
                    ->select('id','model_id','model_type','file_name','name');
    }
}
