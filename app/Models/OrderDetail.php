<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\OrderDetailResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OrderDetail extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'bookingId',
        'productId',
        'productName',
        'productDescription',
        'quantity',
        'price',
        'new_price',
        'discount',
        'gst_tax',
        'total_price'
    ];

    protected $casts = [
        'quantity' => 'int',
        'price' => 'float',
        'new_price' => 'float',
        'discount' => 'float',
        'gst_tax' => 'float',
        'total_price' => 'float',
    ];

    public function productImage(){
        return $this->hasOne('App\Models\Media', 'model_id', 'productId')
                    ->where('model_type','App\Models\Item')
                    ->where('collection_name','product_images')
                    ->select('id','model_id','model_type','file_name','name');
    }
    
    public function setResource($data)
    {
        return new OrderDetailResource($data);
    }
}
