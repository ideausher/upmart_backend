<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\UserBasedLocationResource;

class UserBasedLocation extends Model 
{
    protected $table = 'users_based_location';
    
    protected $fillable = [ 
        'id',
        'user_id',
        'lat',
        'lng',
        'accuracy',
        'altitude',
        'speed',
        'speedAccuracy',
        'heading',
        'time'
    ];
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at',
    ];
    
    public function setResource($data)
    {
        return new UserBasedLocationResource($data);
    }
}
