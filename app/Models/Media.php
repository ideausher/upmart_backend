<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Media extends Model
{
    use HasFactory;
    protected $table ='media';
    
    public function getFileNameAttribute(){
        $s3= Storage::disk('s3')->getAdapter()->getClient();
        return $s3->getObjectUrl(env('AWS_BUCKET'),$this->attributes['file_name']);
        
    }
}
