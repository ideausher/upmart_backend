<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariousChargesOnBooking extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'bookingId',
        'chargeName',
        'chargeAmount'
    ];
}
