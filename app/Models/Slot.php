<?php

namespace App\Models;

use App\Http\Resources\SlotsResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    use HasFactory;

    protected $table = 'slots';

    protected $fillable = ['shop_id','day','slot_from','slot_to'];

    public function setResource($data)
    {
        return new SlotsResource($data);
    }
}
