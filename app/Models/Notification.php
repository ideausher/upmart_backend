<?php

namespace App\Models;

use App\Models\Notification;
use Illuminate\Support\Facades\Log;
use Intersoft\Auth\App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Intersoft\Auth\App\Helpers\V1\PushNotification;

class Notification extends Model
{
    protected $table = 'notifications';
    
    protected $fillable = ['send_by','send_to','notification_type','title','description','is_read'];
    public static function createNotificationNew($send_by,$userId, $title,$message,$type)
    {
        
        if(!isset($userId)){
            $userId = $send_by;
        }
        $notificationData["data"]["noti_title"] = $title;
        $notificationData["message"] = $message;
        $notificationData["title"] = $title;
        $notification = new Notification;
        $notification->send_by = $send_by;
        $notification->send_to = $userId ;
        $notification->notification_type = $type;
        $notification->title = $title;
        $notification->description =  $message;
        $notification->is_read = 0;
        $notification->save();
        $pushNotification = new PushNotification();
        $status = $pushNotification->sendNotification($notificationData, $userId);
        return $status;
    }
    // public static function createNotificationNew($userId,$sendBy, $title,$message,$status)
    // {
    //     $notificationData["data"]["noti_title"] = $title;
    //     $notificationData["message"] = $message;
    //     $notificationData["title"] = $title;
    //     $pushNotification = new PushNotification();
    //     $status = $pushNotification->sendNotification($notificationData, $userId);
    //     return $status;
    // }

    public static function createLocationSilentNotification($userId, $title,$message,$data)
    {
        $notificationData["data"]["noti_title"] = "Welcome";
        $notificationData["data"]["type"] = 100;
        $notificationData["message"] = $message;
        $notificationData["title"] = $title;
        $pushNotification = new PushNotification();
        $status = $pushNotification->sendNotification($notificationData, $userId);
        return $status;
    }
    protected $dates = ['created_at','updated_at'];
    public function sendBy(){
        return $this->hasOne(User::class, 'id','send_by')
                    ->select('id','title','name','email','profile_picture','country_code','phone_number','country_iso_code','availability');
    }
    public function sendTo(){
        return $this->hasOne(User::class, 'id','send_to')
                    ->select('id','title','name','email','profile_picture','country_code','phone_number','country_iso_code','availability');
    }
}