<?php
namespace App\Models;

use App\Enum\BookingStatus;
use App\Models\BookingDetail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Intersoft\Auth\App\Models\DeliveryboyMainDocument;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intersoft\Auth\App\Http\Resources\UserLoginResource;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;
    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password','login_type','social_id','country_code','profile_picture','phone_number','country_iso_code','verified'
    // ];
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','updated_at','created_at','email_verified_at'
    ];
    protected $appends = ['resource_url'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getResourceUrlAttribute()
    {
        return url('/admin/users/'.$this->getKey());
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function AuthAccessToken()
    {
        return $this->hasMany('Intersoft\Auth\App\Models\OauthAccessToken');
    }

    public function setResource($user)
    {
        return new UserLoginResource($user);
    }
    public function userAddresses()
    {
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id');
    }

    public function avgReviews()    {
        return $this->hasOne('App\Models\Review', 'delivery_boy_id', 'id')
                    ->whereNotNull('delivery_boy_id')
                    ->selectRaw('COALESCE(avg(rating),0) as avgRating, delivery_boy_id')
                    ->groupBy('delivery_boy_id');
    }
    public function currentBookings()    {
        return $this->hasOne(BookingDetail::class, 'deliveryBoyId', 'id')
                    ->whereIn('status', [
                                            BookingStatus::DELIVERY_BOY_ASSIGNED,
                                            BookingStatus::DELIVERY_BOY_STARTED_THE_JOURNEY,
                                            BookingStatus::DELIVERY_BOY_REACHED_TO_VENDOR,
                                            BookingStatus::DELIVERY_BOY_PICKED_ORDER,
                                            BookingStatus::DELIVERY_BOY_REACHED_TO_CUSTOMER,
                                        ]); 
    }
    public function deliveryBoyDocuments()    {
        return $this->hasOne(DeliveryboyMainDocument::class, 'user_id', 'id');
    }
    
}
