<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phonenumber_with_countrycode',
        'country_code',
        'country_iso_code',
        'password',
        'remember_token',
        'activated',
        'forbidden',
        'platform_charges',
        'commission_charges',
        'availability',
        'merchant_type',
        'platform_charges',
        'commission_charges'
    ];

}