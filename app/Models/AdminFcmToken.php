<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminFcmToken extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'userid',
        'token',
        'adminType'
    ];
}
