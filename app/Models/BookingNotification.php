<?php
namespace App\Models;

use App\Models\Shop;
use App\Models\User;
use App\Models\BookingDetail;
use App\Http\Resources\BookingResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookingNotification extends Model
{
    use HasFactory;
    protected $table = "booking_notifications";

    protected $fillable = [
        'booking_id',
        'deliveryboy_id',
        'deliveryboy_rating',
        'notification_message',
        'deliveryboy_accept_status',
        'expiration_time'
    ];
        
    protected $dates = [
        'expiration_time ',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'date',
    ];

    public function bookingDetail(){
        return $this->hasOne(BookingDetail::class, 'id', 'booking_id');
    }
    public function userDetail(){
        return $this->hasOne(User::class, 'id', 'deliveryboy_id');
    }
    public function rejectedbookings(){
        return $this->hasOne(BookingRejectReason::class, 'id', 'booking_id');
    }
}
