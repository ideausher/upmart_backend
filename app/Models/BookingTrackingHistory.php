<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingTrackingHistory extends Model
{
    use HasFactory;

    protected $table = 'booking_tracking_history';

    protected $fillable = [
        'bookingId',
        'userId',
        'reason',
        'value',
        'type',
        'status_value'
    ];
    protected $date = [
        'created_at'
    
    ];
}
