<?php
namespace App\Models;

use App\Models\BookingDetail;
use App\Http\Resources\CouponResource;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model 
{
    protected $table = 'coupons';
    
    protected $fillable = [ 
            'id',
            'coupon_name',
            'shopId',
            'isPrimary',
            'coupon_code',
            'coupon_description',
            'coupon_type',
            'coupon_discount',
            'coupon_min_amount',
            'coupon_max_amount',
            'maximum_total_use',
            'maximum_per_customer_use',
            'start_date',
            'end_date',
            'user_id'
        ];
    protected $dates = [
            'deleted_at',
            'created_at',
            'updated_at',
        ];

    public function couponImage(){
        return $this->hasOne('App\Models\Media', 'model_id', 'id')
                    ->where('model_type','App\Models\Coupon')
                    ->where('collection_name','coupon_image')
                    ->select('id','model_id','model_type','file_name','name');
    }
    
    public function setResource($data)
    {
        return new CouponResource($data);
    }

    public function couponHistory()
    {
        return $this->hasMany(BookingDetail::class,'couponId','id');
    }
}
