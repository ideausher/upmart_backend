<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingRejectReason extends Model
{
    use HasFactory;

    protected $table = 'booking_reject_reason';

    protected $fillable = ['booking_id','reason'];
}
