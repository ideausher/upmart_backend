<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = [
        'is_verified',
        'email',
        'phone_number',
        'country_code',
        'otp'
    ];
}
