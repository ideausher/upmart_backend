<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VariousCharge extends Model
{
    use HasFactory;

    public function variouschargerelation()
    {
        return $this->hasMany(VariousChargeRelation::class,'various_charge_id','id');
    }
}
