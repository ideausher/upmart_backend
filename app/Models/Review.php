<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    protected $table = 'review';
    
    protected $fillable = [
        'shop_id',
        'delivery_boy_id',
        'customer_id',
        'shop_id_by',
        'delivery_boy_id_by',
        'customer_id_by',
        'rating',
        'feedback',
        'booking_id'
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
