<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\CategoryResource;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $table ='categories';
    
    protected $fillable = [
            'category_Name',
            'category_Description',
            'profile_Picture',
        
        ];
        
        
    protected $dates = [
            'created_at',
            'updated_at',
        
        ];
    public function setResource($data)
    {
        return new CategoryResource($data);
    }
}
