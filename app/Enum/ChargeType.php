<?php

namespace App\Enum;

class ChargeType{
    const DeliveryChargeForCustomer = 1;
    const DeliveryChargeToDeliveryBoy = 2;
    const PlatformCharge = 3;
    const CommissionCharge = 4;
    const FlatChargeOrderPickDeliveryBoy = 5;
    const FlatChargeOrderDeliveredDeliveryBoy = 6;

}