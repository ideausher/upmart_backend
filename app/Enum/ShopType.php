<?php

namespace App\Enum;

class ShopType
{
	const TakeawayShop = 1;
	const DeliveryShop = 2;
	const DeliveryAndTakeawayShop = 3;
}
