<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        DB::listen(function ($query) {
            $sqlQuery = [
                $query->sql,
                $query->bindings,
                $query->time
            ];
            \File::append(storage_path('logs' . DIRECTORY_SEPARATOR . "QueryLogger"), "****\n" . json_encode($sqlQuery)."\n");
        });
        Schema::defaultStringLength(191);
    }
}
