<?php

namespace App\Console\Commands;

use App\Enum\BookingStatus;
use App\Models\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use IntersoftStripe\Http\Services\StripePaymentProcess;
use App\Repository\Interfaces\IFailedTransfersRepository;

class FailedTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FailedTransfer:repay';

    private $failedTranRepo;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will reapy to the vendor and delivery boy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        IFailedTransfersRepository $failedTranRepo
    )
    {
        $this->failedTranRepo = $failedTranRepo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::error("**** Failed Transfers Cron Started");
        $stripe_secret_key = env('STRIPE_SECRET_KEY');
        $failedTransfers = $this->failedTranRepo->getFailedTransfers();
        Log::error("**** Pending Payments : " . json_encode($failedTransfers));
        foreach($failedTransfers as $ft){
            Log::error("**** Booking Detail : " . json_encode($ft->bookingDetail));

            $payment = new StripePaymentProcess($stripe_secret_key);
            if(isset($ft->bookingDetail)){
                // Transfer Money to Vendor
                if(isset($ft->vendor_id) && $payment->transferCharges($ft->bookingDetail->bs_amount_to_vendor_after_commi,$ft->bookingDetail->shopDetails->stripe_user_id)){
                    Log::error("Vendor Transfer failed for booking : ".$ft->bookingDetail->shopDetails->stripe_user_id );
                    $ft->status = 1;
                    $ft->save();
                }

                // Transfer Money to Delivery Boy
                if(isset($ft->delivery_boy_id) && $payment->transferCharges($ft->bookingDetail->ws_total_amount_to_db,$ft->bookingDetail->deliveryBoyDetails->stripe_customer_id)){
                    Log::error("Vendor Transfer failed for booking : ".$ft->bookingDetail->deliveryBoyDetails->stripe_customer_id );
                    $ft->status = 1;
                    $ft->save();
                }
            }
            
        }
        Log::error("****  Cron Completed");
    }
}
