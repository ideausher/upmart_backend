<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Enum\BookingStatus;
use App\Models\Notification;
use Illuminate\Console\Command;
use Carbon\Exceptions\Exception;
use App\Service\V1\BookingService;
use App\Traits\BookingStatusTrait;
use Illuminate\Support\Facades\Log;
use App\Repository\Interfaces\IItemInterface;
use App\Repository\Interfaces\IOrderRepository;
use App\Repository\Interfaces\IBookingRepository;
use App\Repository\Interfaces\ISettingRepository;
use IntersoftStripe\Http\Services\StripePaymentProcess;
use App\Repository\Interfaces\IBookingNotificationRepository;
use Intersoft\Auth\App\Repository\Interfaces\IUserRepository;
use App\Repository\Interfaces\IBookingTrackingHistoryRepository;

class AutoCancelOrder extends Command
{
    use BookingStatusTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutoCancelOrder:autocancel';

    private $bookingRepo;
    private $userRepo;
    private $bookingNotiRepo;
    private $settingRepo;
    private $orderRepo;
    private $itemRepo;
    private $bookingHistoryRepo;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will auto cancel the various delivery boy.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IBookingRepository $bookingRepo, IUserRepository $userRepo,IBookingNotificationRepository $bookingNotiRepo,ISettingRepository $settingRepo,IOrderRepository $orderRepo,IItemInterface $itemRepo,IBookingTrackingHistoryRepository $bookingHistoryRepo)
    {
        $this->bookingRepo = $bookingRepo;
        $this->userRepo = $userRepo;
        $this->bookingNotiRepo = $bookingNotiRepo;
        $this->settingRepo = $settingRepo;
        $this->orderRepo = $orderRepo;
        $this->itemRepo = $itemRepo;
        $this->bookingHistoryRepo = $bookingHistoryRepo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $setting = $this->settingRepo->getSettings();
        $scheduleCancelTime = env('Schedule_Booking_Cancel_Time', 5*60);
        $nonscheduleCancelTime = env('Nonschedule_Booking_Cancel_Time', 10*60);
        $dbScheduledSecondsBefore = $this->convertTimeToSeconds($setting->send_db_noti_before_schedule_time);
        Log::error("**** AUTO CANCEL CRON RUNNING ******");
        $getActiveBookings = $this->bookingRepo->getActiveBookings()->reject(function($value) use($dbScheduledSecondsBefore, $scheduleCancelTime ,$nonscheduleCancelTime){
            
            
            // BOOKING TYPE === TAKEAWAY
            if( $value->bookingType == BookingService::Takeaway && $value->status == BookingStatus::ORDER_PLACED ){
                
                // Scheduled Booking
                if( isset($value->bookingDateTime) && Carbon::now()->timezone($value->timezone)->gte((new Carbon($value->bookingDateTime, $value->timezone))->addSeconds($scheduleCancelTime))){
                    
                    Log::error("**** Takeaway | Scheduled | Auto Cancel : Current Time :".Carbon::now()." Compare with " . Carbon::parse($value->bookingDateTime)->addSeconds($scheduleCancelTime) );
                    $value->status = BookingStatus::AUTO_CANCELLED;
                    $this->generateBookingHistory($value,BookingStatus::AUTO_CANCELLED);
                    $this->refundPaymentByBooking($value);
                    $this->refundStockOfBooking($value);
                    Notification::createNotificationNew(0,$value->userId,"Your Order #$value->booking_code got cancelled by system!","Your Order #$value->booking_code got cancelled!",BookingStatus::AUTO_CANCELLED);
                    $value->save();
                }
                // Non Scheduled Booking
                if( (!isset($value->bookingDateTime)) && Carbon::now()->gte(Carbon::parse($value->created_at)->addSeconds($nonscheduleCancelTime))){
                    
                    Log::error("**** Takeaway | Non Schedule | Auto Cancel : Current Time :".Carbon::now()." Compare with " .Carbon::parse($value->created_at)->addSeconds($nonscheduleCancelTime)->timezone($value->timezone) );
                    $value->status = BookingStatus::AUTO_CANCELLED;
                    $this->generateBookingHistory($value,BookingStatus::AUTO_CANCELLED);
                    $this->refundPaymentByBooking($value);
                    $this->refundStockOfBooking($value);
                    Notification::createNotificationNew(0,$value->userId,"Your Order #$value->booking_code got cancelled by system!","Your Order #$value->booking_code got cancelled!",BookingStatus::AUTO_CANCELLED);
                    $value->save();
                }
            }

            // BOOKING TYPE === DELIVERY
            if( $value->bookingType == BookingService::Delivery ){
                Log::error(json_encode($value));
                // Scheduled Booking
                Log::error("**** Delivery | Scheduled | Auto Cancel : Current Time :".Carbon::now()." Compare with " . Carbon::parse($value->bookingDateTime)->addSeconds($scheduleCancelTime) );
                if( isset($value->bookingDateTime) && Carbon::now()->timezone($value->timezone)->gte((new Carbon($value->bookingDateTime, $value->timezone))->addSeconds($scheduleCancelTime))){                    
                    // if delivery boy not assigned
                    if( ! isset($value->deliveryBoyId)){
                        
                        $value->status = BookingStatus::AUTO_CANCELLED;
                        $this->generateBookingHistory($value,BookingStatus::AUTO_CANCELLED);
                        $this->refundPaymentByBooking($value);
                        $this->refundStockOfBooking($value);
                        Notification::createNotificationNew(0,$value->userId,"Your Order #$value->booking_code got cancelled by system!","Your Order #$value->booking_code got cancelled!",BookingStatus::AUTO_CANCELLED);
                        $value->save();
                    }
                    
                }
                // Non Scheduled Booking
                Log::error("**** Delivery | Non Schedule | Auto Cancel : Current Time :".Carbon::now()." Compare with " .Carbon::parse($value->created_at)->addSeconds($nonscheduleCancelTime)->timezone($value->timezone) );
                if( (!isset($value->bookingDateTime)) && Carbon::now()->timezone($value->timezone)->gte(Carbon::parse($value->created_at)->addSeconds($nonscheduleCancelTime)->timezone($value->timezone))){
                    
                    // if delivery boy not assigned
                    if( ! isset($value->deliveryBoyId)){
                        
                        $value->status = BookingStatus::AUTO_CANCELLED;
                        $this->generateBookingHistory($value,BookingStatus::AUTO_CANCELLED);
                        $this->refundPaymentByBooking($value);
                        $this->refundStockOfBooking($value);
                        Notification::createNotificationNew(0,$value->userId,"Your Order #$value->booking_code got cancelled by system!","Your Order #$value->booking_code got cancelled!",BookingStatus::AUTO_CANCELLED);
                        $value->save();
                    }
                }
            }
        });
    }
    /**
     * Convert Time to seconds
     *
     * @param [TIME] $time
     * @return void
     */
    public function convertTimeToSeconds($time){
        sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
        
        return $time_seconds;
    }
    /**
     * Cancel Booking
     *
     * @param [BookingDetail] $booking
     * @return void
     */
    public function cancelBooking($booking){
        $this->refundPaymentByBooking($booking);
        $this->refundStockOfBooking($booking);
        $booking->status = BookingStatus::AUTO_CANCELLED;
        $booking->save();
    }

    /**
     * Refund Payment
     *
     * @param [Booking] $booking
     * @return void
     */
    public function refundPaymentByBooking($booking){
        $stripe_secret_key = env('STRIPE_SECRET_KEY');
        $payment = new StripePaymentProcess($stripe_secret_key);
        try{
            $refund = $payment->refund_payment_with_bookingcode($booking->booking_code);
            Notification::createNotificationNew(0,$booking->userId,"Order Refund Initiated!","Dear Customer, Your Order refund is initiated for the Order #".$booking->booking_code .".",BookingStatus::BOOKING_REFUND);
        }
        catch(\Exception $ex){
            Log::error($ex);
        }
    }
    /**
     * Refund Stock of Booking
     *
     * @param [Booking] $booking
     * @return void
     */
    public function refundStockOfBooking($booking){
        $orderDetails = $this->orderRepo->getOrderDetailsByBookingId($booking->id);
        if($orderDetails->count() > 0){
            foreach($orderDetails as $order){
                $this->itemRepo->updateStockValue($order->productId, $order->quantity);
            }
        }
    }

    /**
     * Generate Booking History By Booking Status
     *
     * @param [type] $bookingDetails
     * @param [type] $status
     * @return void
     */
    public function generateBookingHistory($bookingDetails,$status){
        return $this->bookingHistoryRepo->createBookingHistory(
            $bookingDetails->id,
            $bookingDetails->userId,
            $this->generateTextMessageBasedUponBookingStatus($status),
            $this->generateTextMessageBasedUponBookingConstantName($this->generateTextMessageBasedUponBookingStatus($status)),
            1,
            $status
        );
    }
}
