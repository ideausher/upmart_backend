<?php

namespace App\Http\Controllers;
use App\Traits\BannerTrait;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{ 
    use BannerTrait;
}      