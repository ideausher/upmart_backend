<?php

namespace App\Http\Controllers;
use App\Traits\SupportTrait;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{ 
    use SupportTrait;
}      