<?php

namespace App\Http\Controllers;
use App\Traits\ShopTrait;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
  use ShopTrait;
}
