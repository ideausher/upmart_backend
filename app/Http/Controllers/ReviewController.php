<?php

namespace App\Http\Controllers;
use App\Traits\ReviewTrait;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{ 
    use ReviewTrait;
}      