<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enum\BookingStatus;
use App\Models\Notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Repository\Interfaces\IBookingRepository;
use App\Repository\Interfaces\ISettingRepository;
use App\Repository\Interfaces\IBookingNotificationRepository;
use Intersoft\Auth\App\Repository\Interfaces\IUserRepository;

class TestBookingRating extends Controller
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckDeliveryBoyAndSendBookingNotification:sendnotification';

    private $bookingRepo;
    private $userRepo;
    private $bookingNotiRepo;
    private $settingRepo;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will check that to how many delivery boy the notification is sent for particular booking and then send notification to rest of other delivery boy, based upon ratings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IBookingRepository $bookingRepo, IUserRepository $userRepo,IBookingNotificationRepository $bookingNotiRepo,ISettingRepository $settingRepo)
    {
        $this->bookingRepo = $bookingRepo;
        $this->userRepo = $userRepo;
        $this->bookingNotiRepo = $bookingNotiRepo;
        $this->settingRepo = $settingRepo;
        // parent::__construct();
    }

    public function test()
    {
        Log::error("**** DELIVERY BOY CRON RUNNING ******");
        $setting = $this->settingRepo->getSettings();
        $maxAcceptingLimit = $setting->max_accepting_limit;
        $getActiveBookingsWithNotificationsAverage = $this->bookingRepo->getActiveBookingsWithNotificationsAverage();
        // print_r($getActiveBookingsWithNotificationsAverage);die;
        // echo count($getActiveBookingsWithNotificationsAverage);die;
        // looping over the active booking
        Log::error("****  CRON ACTIVE BOOKINGS :" . json_encode($getActiveBookingsWithNotificationsAverage));
        $deliveryBoysforBookings = array();
        
        foreach($getActiveBookingsWithNotificationsAverage as $booking){
            // Log::error("**** BOOKING ID $booking->id");
            $check = $booking->bookingNotificationsMinRatingByBooking;
            echo $minRatingAlloted = (int) (isset($check[0]) ?  $check[0]->minRating : 6);
            Log::error("**** MIN RATING ALLOTED $minRatingAlloted");
            if($minRatingAlloted == 0)
                continue; // continue if all notification sent to the delivery boys even having 0
            if($minRatingAlloted == 6 ){ 
                Log::error("In else if : 6");
                // if min rating is 5 then it means we have to send the rating to delivery boy having the ratings between 5 and 3
                $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(6,6,3,0, 1, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                // dd($deliveryBoys);
                if(count($deliveryBoys) == 0){
                    
                    // if min rating is between 3 and 5 and no delivery boys are available between the rating of 2 to 1  then it means we have to send the rating to delivery boy having the rating 0
                    $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(3,5,0,0, 1, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                    
                    if(count($deliveryBoys) == 0){
                        $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(3,5,null,null,1, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                        // dd($deliveryBoys);
                        if(count($deliveryBoys) == 0){
                            $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(2,1,0,0,0, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                            // dd($deliveryBoys);
                        }
                    }
                    
                }
            }
            if($minRatingAlloted >= 3 && $minRatingAlloted <= 5 ){
                Log::error("In else if : 3 and 5");
                // if min rating is between 3 and 5 then it means we have to send the rating to delivery boy having the ratings between 2 to 1
                $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(3,5,0,0, 1, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                if(count($deliveryBoys) == 0){
                    // if min rating is between 3 and 5 and no delivery boys are available between the rating of 2 to 1  then it means we have to send the rating to delivery boy having the rating 0
                    $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(3,5,null,null,1, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                    if(count($deliveryBoys) == 0){
                        $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(2,1,0,0,0, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
                    }
                }
            }
            elseif($minRatingAlloted >= 1 && $minRatingAlloted <= 2){
                Log::error("In else if : 1 and 2");
                // if min rating is between 1 and 2 then we have to send notification to  delivery boys having rating zero
                $deliveryBoys = $this->userRepo->getDeliveryBoysWithReviews(2,1,0,0,0, $booking->addressDetails->latitude, $booking->addressDetails->longitude,$maxAcceptingLimit);
            }
            
            array_push($deliveryBoysforBookings, [
                'booking'=> $booking,
                'deliveryBoys' => $deliveryBoys
            ]);
            Log::error("****  Sending Notifications for Booking ID $booking->id to :" . json_encode($deliveryBoys));
        }
// die;
        // Log::error("delivery boys rating check: ");
        // Log::error($deliveryBoysforBookings);

        $this->bookingNotiRepo->createBookingNotification($deliveryBoysforBookings);
        // $this->sendBookingNotification($deliveryBoysforBookings);
        Log::error("****  Cron Completed");
    }
}
