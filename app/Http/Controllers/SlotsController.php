<?php

namespace App\Http\Controllers;

use App\Traits\SlotsTrait;
use App\Http\Controllers\Controller;

class SlotsController extends Controller
{
    use SlotsTrait;
}
