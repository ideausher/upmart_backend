<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Traits\VersionAction;

class AppVersionController extends Controller
{
    use VersionAction;
}
