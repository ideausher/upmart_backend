<?php

namespace App\Http\Controllers;
use App\Traits\NotificationTrait;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{ 
    use NotificationTrait;
}      