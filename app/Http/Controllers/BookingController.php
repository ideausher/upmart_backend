<?php

namespace App\Http\Controllers;
use App\Traits\BookingTrait;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{ 
    use BookingTrait;
}      