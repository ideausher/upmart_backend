<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Traits\PageForTermsAndPolicyTrait;

class PageForTermsAndPolicyController extends Controller
{
       use PageForTermsAndPolicyTrait;
}