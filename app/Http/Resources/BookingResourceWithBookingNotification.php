<?php

namespace App\Http\Resources;
use App\Http\Resources\ProductShopResource;
use App\Http\Resources\NotificationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResourceWithBookingNotification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $trackingsObject ;
        if(isset($this->bookingDetail->trackingHistory)){
            foreach ($this->bookingDetail->trackingHistory as $key => $trackings)
                $trackingsObject[$trackings->status_value] = $trackings;
            $historyCollect = (object)$trackingsObject;
        }
        $totalGST = 0;
        if($this->bookingDetail->ordersDetails)
        {
            foreach($this->bookingDetail->ordersDetails as $orderdetails)
            {
                $totalGST += $orderdetails->gst_tax;
            }
        }
        return [
            "id" => $this->bookingDetail->id,
            // "user_id" => $this->userId ?? "",
            // "shop_id" => $this->shopId ?? "",
            // "address_id" => $this->addressId ?? "",
            // "card_id" => $this->cardId ?? "",
            // "coupon_id" => $this->couponId ?? "",
            "booking_code" => $this->bookingDetail->booking_code ?? "",
            "delivery_boy_id" => ((int) $this->bookingDetail->deliveryBoyId) ?? "",
            "booking_type" => (int) $this->bookingDetail->bookingType ?? "",
            "booking_date_time" => $this->bookingDetail->bookingDateTime ?? "",

            'amount_before_discount' => (float) $this->bookingDetail->amount_before_discount ?? "",
            'discount_amount' => (float) $this->bookingDetail->discount_amount ?? "",
            'amount_after_discount' => (float) $this->bookingDetail->amount_after_discount ?? "",
            'delivery_charge_to_delivery_boy' => (float) $this->bookingDetail->delivery_charge_to_delivery_boy ?? "",
            'delivery_charge_for_customer' => (float) $this->bookingDetail->delivery_charge_for_customer ?? "",
            'commission_charge' => (float) $this->bookingDetail->commission_charge ?? "",
            'platform_charge' => (float) $this->bookingDetail->platform_charge ?? "",
            'after_charge_amount' => (float) $this->bookingDetail->after_charge_amount ?? "",
            'stripe_charges' => (float) $this->bookingDetail->stripe_charges ?? "",
            'amount_after_stripe_charges' => (float) $this->bookingDetail->amount_after_stripe_charges ?? "",
            'tip_to_delivery_boy' => (float) $this->bookingDetail->tip_to_delivery_boy ?? "",
            'instruction' => $this->bookingDetail->instruction ?? "",
            
            "status" => (int) $this->bookingDetail->status ?? "",
            "reject_reason"=>($this->rejectedbookings) ? $this->rejectedbookings->reason : null,
            "order_details" => $this->bookingDetail->ordersDetails ?? new \stdClass(),
            'user_details' => $this->bookingDetail->userDetails ?? new \stdClass(),
            'shop_details' => new ProductShopResource($this->bookingDetail->shopDetails) ?? new \stdClass(),
            'coupon_details' => $this->bookingDetail->couponDetails ?? new \stdClass(),
            'delivery_address' => $this->bookingDetail->addressDetails ?? new \stdClass(),
            'delivery_boy_details' => $this->bookingDetail->deliveryBoyDetails ?? new \stdClass(),
            'booking_status' => isset($historyCollect) ? $historyCollect : new \stdClass(),
            'ratings' =>  $this->bookingDetail->ratings ?? new \stdClass(),
            "gst_tax"=>(float) $totalGST,
            "pst_tax"=>(float) $this->bookingDetail->pst,
            "hst_tax"=>(float) $this->bookingDetail->hst,
            "amount_paid_to_delivery_boy_on_order_pickup"=>(float) $this->bookingDetail->amount_paid_to_delivery_boy_on_order_pickup,
            "amount_paid_to_delivery_boy_on_order_delivery"=>(float) $this->bookingDetail->amount_paid_to_delivery_boy_on_order_delivered,
            // 'notification_details' =>  new NotificationResource($this) ?? new \stdClass(),
            "created_at" => $this->bookingDetail->created_at->format('Y-m-d H:i:s') ?? "",
        ];
    }
}
