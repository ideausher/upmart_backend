<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'booking_id' => $this->booking_id ?? "",
            'deliveryboy_id' => $this->deliveryboy_id ?? "",
            'deliveryboy_rating' => $this->deliveryboy_rating ?? "",
            'notification_message' => $this->notification_message ?? "",
            "deliveryboy_accept_status" => $this->deliveryboy_accept_status ?? 0,
            'expiration_time' => $this->expiration_time ?? "",
        ];
    }
}
