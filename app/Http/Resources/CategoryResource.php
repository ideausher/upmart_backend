<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'category_name'=>$this->category_name,
                'category_description'=>$this->category_description,
                'image'=>$this->image ?? "",
        ];
    } 
}
