<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class UserBasedLocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'lng'=>$this->lng ?? 0.00,
                'lat'=>$this->lat ?? 0.00,
                'accuracy'=>(float)number_format($this->accuracy,2) ?? 0.00,
                'altitude'=>(float)number_format($this->altitude,2) ?? 0.00,
                'speed'=>(float)number_format($this->speed,2) ?? 0.00,
                'speedAccuracy'=>(float)number_format($this->speedAccuracy,2) ?? 0.00,
                'heading'=>(float)number_format($this->heading,2) ?? 0.00,
                'time'=>(float)number_format($this->time,2) ?? 0.00
        ];
    } 
}