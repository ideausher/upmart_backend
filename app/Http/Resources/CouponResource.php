<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->coupon_name,
            'code' => $this->coupon_code,
            "coupon_image" => $this->couponImage ?? new \stdClass(),
            'is_primary' => $this->isPrimary == 2 ? 1 : 0,
            'is_global' => isset($this->shopId) ? 0 : 1 ,
            'type' => $this->coupon_type,
            'discount' => $this->coupon_discount ?? 0,
            'description' => $this->coupon_description ?? "",
            'min_amount' => $this->coupon_min_amount ?? 0,
            'max_amount' => $this->coupon_max_amount ?? 0,
            'maximum_customer_use' =>$this->maximum_per_customer_use ?? 0,
            'total_use' =>$this->maximum_total_use ?? 0,
            'start_date' => $this->start_date ?? "",
            'end_date' => $this->end_date ?? "",
        ];
        
    }
}
