<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    function lz($num)
    {
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }
    function convertToTime($dec)
    {
        $seconds = ($dec * 3600);
        $hours = floor($dec);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;
        return $this->lz($hours).":".$this->lz($minutes).":".substr($this->lz($seconds),0,2);
    }
    public function toArray($request)
    {
        return [
                'id'=>$this->id,
                'name'=>$this->shop_name,
                "banners"=> ShopBannerResource::collection($this->showBanners),
                'show_owner'=>$this->showOwner  ??  new \stdClass(),
                'description'=>$this->description ?? "",
                'profile_image'=> $this->image ??  new \stdClass(),
                'address'=>$this->shop_address,
                'additional_address'=>$this->additional_address,
                'latitude'=>$this->lat,
                'longitude'=>$this->lng,
                'rating'=> (float)($this->shop_rating ? $this->shop_rating : ($this->rating ?? 0)),
                'isFavourite'=> (int) ($this->fav ? $this->fav : 0),
                'distance'=> (float)round(($this->distance ?? ""),2),
                'exp_time'=> $this->convertToTime( ($this->distance ?? 0) / 40),
                "shop_type" => $this->shop_type ?? "",
                'media' => $this->media ?? "",
                'primary_coupon' => $this->primaryCoupon ??  new \stdClass(),
                'availability' => $this->availability ??  0,
                'gst'=>(float) $this->gst,
                'hst'=>(float) $this->hst,
                'pst'=>(float) $this->pst,
        ];
    }
}
