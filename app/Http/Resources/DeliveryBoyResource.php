<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class DeliveryBoyResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $response_data = [
            'vehicle_registeration'=> [
                "registeration_number"=> $this->registeration_number ?? "",
                "vehicle_picture"=>$this->vehicle_picture ?? "",
                "vehicle_company"=>$this->vehicle_company ?? "",
                "vehicle_status" => $this->vehicle_status ?? "",
                "vehicle_reject_reason" => $this->vehicle_reason  ?? "",
                "model"=>$this->model ?? "",
                "color" => $this->color ?? "",
                "valid_upto" => $this->valid_upto ?? "" 
            ],
            'driving_licence' => [
                "driving_licence_images" => $this->drivinglicence_images ?? "",
                "driving_licence_no"=> $this->drivinglicence_no ?? "",
                "driving_licence_expiry_date"=> $this->drivinglicence_expiry_date ?? "" ,
                "driving_licence_status" => $this->drivinglicence_status ?? "",
                "driving_licence_reason" => $this->drivinglicence_reason ?? "",
            ],
            
            'licence_no_plate'=>[
                "licence_no_plate_images"=>$this->licence_no_plate_images ?? "",
                "no_plate"=>$this->no_plate ?? "",
                "licence_no_plate_status" => $this->licence_no_plate_status ?? "",
                "licence_no_plate_reason" => $this->licence_no_plate_reason ?? ""
            ],

            'vehicle_insurance'=> [
                "vehicle_insurence_image" => $this->vehicle_insurence_image ?? "", 
                "vehicle_insurence_status" => $this->vehicle_insurence_status ?? "",
                "vehicle_insurence_reason" => $this->vehicle_insurence_reason  ?? "",
                "vehicle_insurence_valid_upto" => $this->vehicle_insurence_valid_upto  ?? "",
                "vehicle_insurence_valid_from" => $this->vehicle_insurence_valid_from  ?? "",
                "policy_number" =>$this->policy_number ?? ""
            ],

            'action'=> [ 
                "status"=> $this->status ,
            ],
            

        ];
        if ($this->status ==3){ 
            $response_data["action"]["reason"]=$this->reason;
        }

        return $response_data;
    }

}
