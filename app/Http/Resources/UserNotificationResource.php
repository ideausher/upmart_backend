<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class UserNotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "notification_type" => ((int) $this->notification_type) ?? 0,
            "title" => $this->title ?? "",
            "description" => $this->description ?? "",
            "is_read" => $this->is_read ?? "",
            "send_to" => $this->sendTo ?? new \stdClass(),
            "send_by" => $this->sendBy ?? new \stdClass(),
            "created_at" => $this->created_at->format('Y-m-d H:i:s') ?? "",
            "updated_at" => $this->updated_at->format('Y-m-d H:i:s') ?? "",
        ];
    }
}
