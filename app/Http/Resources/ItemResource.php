<?php

namespace App\Http\Resources;
use stdClass;
use App\Http\Resources\ProductShopResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    function lz($num)
    {
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }
    function convertToTime($dec)
    {
        $seconds = ($dec * 3600);
        $hours = floor($dec);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;
        return $this->lz($hours).":".$this->lz($minutes).":".substr($this->lz($seconds),0,2);
    }
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'product_description' => $this->product_description,
            "stock_quantity"=> $this->stock_quantity,
            "mrp_price" => $this->price,
            "product_image" => $this->product_image,    
            "category_name" => $this->category_name,
            "company_name" => $this->company_name,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at,
            "category_id" => $this->category_id,
            "is_disabled" => $this->is_disabled,
            "discount_percentage" => $this->discount,
            "discounted_price" => $this->new_price,
            "gst_price" => ($this->gst_enable==1) ? (float) $this->shop->gst : (float) 0.00,
            "gst_enable"  =>(int) $this->gst_enable
        ];
    }
}
