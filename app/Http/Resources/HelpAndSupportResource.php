<?php

namespace App\Http\Resources;
use App\Http\Resources\BookingResource;
use Illuminate\Http\Resources\Json\JsonResource;

class HelpAndSupportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "ticket_id" => $this->ticket_id ?? "", 
            "booking_id" => $this->booking_id ?? "", 
            "booking_detail" =>  new BookingResource($this->bookingDetail) ?? "",
            "user_id" => $this->user_id ?? "",
            "shop_id" => $this->shop_id ?? "",
            "feedback_title" => $this->feedback_title ?? "",
            "feedback_description" => $this->feedback_description ?? "",
            "image" => json_decode($this->image) ?? [],
            "query_status" => $this->query_status ?? 0,
            "admin_reply" => $this->admin_reply ?? "",
            "updated_at" => $this->updated_at->format('Y-m-d H:i:s') ?? "",
            "created_at" => $this->created_at->format('Y-m-d H:i:s') ?? "",
        ];
    }
}
