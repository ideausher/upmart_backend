<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id ,
            "shop_id" => $this->shop_id,
            'booking_id' => $this->booking_id ,
            "delivery_boy_id" => $this->delivery_boy_id,
            "rating" => $this->rating ,
            "feedback" => $this->feedback ?? "",
            "customer_id_by" => $this->customer_id_by ,
            
        ];
    }
}
