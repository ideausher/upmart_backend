<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "bookingId" => $this->bookingId,
            "productId" => $this->productId,
            "productName" => $this->productName,
            "productDescription" => $this->productDescription,
            "price" => $this->price,
            "discount" => $this->discount,
            "new_price" => $this->new_price,
            "gst_tax" => (float) $this->gst_tax,
            "total_price" => (float) $this->total_price,
            "quantity" => $this->quantity,
            "product_image" => $this->product_image ?? new \stdClass()
        ];
    }
}
