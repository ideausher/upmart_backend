<?php

namespace App\Http\Resources;
use App\Http\Resources\ProductShopResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $trackingsObject = array();
        if(isset($this->trackingHistory)){
            foreach ($this->trackingHistory as $key => $trackings)
                $trackingsObject[$trackings->status_value] = $trackings;
            $historyCollect = (object)$trackingsObject;
        }
        $totalGST = 0;
        if($this->ordersDetails)
        {
            foreach($this->ordersDetails as $orderdetails)
            {
                $totalGST += $orderdetails->gst_tax;
            }
        }
        return [
            "id" => $this->id,
            // "user_id" => $this->userId ?? "",
            // "shop_id" => $this->shopId ?? "",
            // "address_id" => $this->addressId ?? "",
            // "card_id" => $this->cardId ?? "",
            // "coupon_id" => $this->couponId ?? "",
            "booking_code" => $this->booking_code ?? "",
            "delivery_boy_id" => ((int) $this->deliveryBoyId) ?? "",
            "booking_type" => (int) $this->bookingType ?? "",
            "booking_date_time" => $this->bookingDateTime ?? "",

            'amount_before_discount' => (float) $this->amount_before_discount ?? "",
            'discount_amount' => (float) $this->discount_amount ?? "",
            'amount_after_discount' => (float) $this->amount_after_discount ?? "",
            'delivery_charge_to_delivery_boy' => (float) $this->delivery_charge_to_delivery_boy ?? "",
            'delivery_charge_for_customer' => (float) $this->delivery_charge_for_customer ?? "",
            'commission_charge' => (float) $this->commission_charge ?? "",
            'platform_charge' => (float) $this->platform_charge ?? "",
            'after_charge_amount' => (float) $this->after_charge_amount ?? "",
            'stripe_charges' => (float) $this->stripe_charges ?? "",
            'amount_after_stripe_charges' => (float) $this->amount_after_stripe_charges ?? "",
            'tip_to_delivery_boy' => (float) $this->tip_to_delivery_boy ?? "",
            'instruction' => $this->instruction ?? "",
            
            "status" => (int) $this->status ?? "",
            "reject_reason"=>($this->rejectedbookings) ? $this->rejectedbookings->reason : null,
            "order_details" => $this->ordersDetails ?? new \stdClass(),
            'user_details' => $this->userDetails ?? new \stdClass(),
            'shop_details' => new ProductShopResource($this->shopDetails) ?? new \stdClass(),
            'coupon_details' => $this->couponDetails ?? new \stdClass(),
            'delivery_address' => $this->addressDetails ?? new \stdClass(),
            'delivery_boy_details' => $this->deliveryBoyDetails ?? new \stdClass(),
            'booking_status' => isset($historyCollect) ? $historyCollect : new \stdClass(),
            'ratings' =>  $this->ratings ?? new \stdClass(),
            "gst_tax"=>(float) $totalGST,
            "pst_tax"=>(float) $this->pst,
            "hst_tax"=>(float) $this->hst,
            "amount_paid_to_delivery_boy_on_order_pickup"=>(float) $this->amount_paid_to_delivery_boy_on_order_pickup,
            "amount_paid_to_delivery_boy_on_order_delivery"=>(float) $this->amount_paid_to_delivery_boy_on_order_delivered,
            "created_at" => $this->created_at->format('Y-m-d H:i:s') ?? "",
        ];
    }
}
