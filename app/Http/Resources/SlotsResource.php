<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class SlotsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'shop_id'=>$this->shop_id,
            'day'=>$this->day,
            "slot_from"=>$this->slot_from,
            "slot_to"=>$this->slot_to,
            "created_at"=>$this->created_at,
            "updated_at"=>$this->updated_at
        ];
    } 
}
