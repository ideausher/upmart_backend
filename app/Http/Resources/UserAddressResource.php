<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class UserAddressResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id ,
            "title"=>$this->title ?? "",
            "user_id"=>$this->user_id,
            "address_type"=>$this->type,
            "city"=> $this->city ?? "",
            "country"=> $this->country ?? "",
            // "address_type"=> $this->address_type ?? "",
            "formatted_address"=>$this->formatted_address ?? "" ,
            "additional_info"=> $this->additional_info ?? "",
            "latitude"=> $this->latitude ?? "" ,
            "longitude"=> $this->longitude ?? "", 
            "Primary-address"=>$this->is_primary ,
            "pincode"=>$this->pincode ?? "",
        ];
    }   
}
