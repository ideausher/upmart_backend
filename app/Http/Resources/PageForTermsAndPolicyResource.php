<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class PageForTermsAndPolicyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'page_id'=>$this->page_id,
                'page_name'=>$this->page_name,
                'text'=>$this->text,
                'version'=>$this->version ?? "",
        ];
    } 
}
