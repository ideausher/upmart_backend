<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'banner_name' => $this->bannerName ?? "",
            'address' => $this->address ?? "",
            'latitude' => $this->latitude ?? "",
            'longitude' => $this->longitude ?? "",
            "distance" => $this->distance ?? 0,
            'banner_image' => $this->bannerImage ?? new \stdClass(),
        ];
    }
}
