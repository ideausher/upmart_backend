<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
class ShopBannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "merchant_id"=> $this->merchant_id,
            "bannerName" => $this->bannerName,
            'link' => $this->bannerImage->file_name ?? "",
            // 'longitude' => $this->longitude ?? "",
            // "distance" => $this->distance ?? 0,
            // 'banner_image' => $this->bannerImage ?? new \stdClass(),
        ];
    }
}
