<?php
namespace App\Http\Middleware;

use Exception;
use Hamcrest\Type\IsString;
use App\Http\Resources\Handler;
use App\Exceptions\BlockedException;
// use App\Exceptions\ValidationException;
use App\Exceptions\SuccessException;
use PhpParser\Node\Expr\Cast\String_;
use App\Exceptions\BadRequestException;
use App\Exceptions\BlockedUserException;
use Intersoft\Auth\App\Traits\APIResponse;
use App\Exceptions\RecordNotFoundException;
use App\Http\Resources\Interfaces\IHandler;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\CustomValidationException;

/**
 *
 * Middleware that tries to automatically transform the data provided
 * that is returned in the form of direct model
 */
class ResourceHandler
{
    use APIResponse;
    private $handler;
    public function __construct(Handler $handler) //)
    {
        $this->handler = $handler;
    }

    public function handle($request, \Closure $next, $guard = null)
    {
        try {
            $response = $next($request);
            // Having the `original` property means that we have the models and
            // the response can be tried to transform
            if (property_exists($response, 'original')) {
                $message='Response Successfully.';

                if (isset($response->original->responseMessage)) {
                    $message=$response->original->responseMessage;
                }
                // Transform based on model and reset the content
                if (is_string($response->original)) {
                    return $this->reponseSuccess([], $response->original);
                } else {
                    return $this->reponseSuccess($this->handler->transformModel($response->original), $message);
                }
            }
        } catch (SuccessException $ex) {
            return $this->reponseSuccess($ex->getData(), $ex->getMessage(), $ex->getStatusCode());
        } catch (BlockedUserException $ex) {
            return $this->respondUnauthorized($ex->getMessage());
        } catch (AuthenticationException $ex) {
            return $this->respondUnauthorized();
        } catch (BadRequestException $ex) {
            return $this->respondInvalidParameters($ex->getMessage());
        } catch (CustomValidationException $ex) {
            return $this->respondCustomInvalidParameters($ex->getMessage(),$ex->getData());
        }catch (RecordNotFoundException $ex) {
            return $this->respondNotFound($ex->getMessage());
        } catch (StripeSomeThingWentWrong $ex) {
            return $this->respondNotFound($ex->getMessage());
        } catch (BlockedException $ex) {
            return $this->blockedUser($ex->getMessage());
        } catch (Exception $ex) {
            dd($ex);
            return $this->respondInternalError($ex->getMessage());
        }
    }

    public function terminate($request, $response)
    {
    }
}
