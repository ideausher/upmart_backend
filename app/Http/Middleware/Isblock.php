<?php

namespace App\Http\Middleware;
use Closure;
use App\Exceptions\BlockedException;
use Illuminate\Support\Facades\Auth;

class Isblock {
    const Blocked = 1;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(Auth::user() && Auth::user()->is_block == Isblock::Blocked ){
            throw new BlockedException("Blocked User");
        }
        return $next($request);
    }

}
