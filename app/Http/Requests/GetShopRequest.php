<?php

namespace App\Http\Requests;

use App\Enum\ShopType;
use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class GetShopRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_ids"=>'sometimes|array',
            "searchshopname"=>'sometimes|string|nullable',
            "shop_type" => 'required|nullable|in:'. ShopType::TakeawayShop .','. ShopType::DeliveryShop . ',' . ShopType::DeliveryAndTakeawayShop,
            "lat"=>'required|string|nullable',
            "lng"=>'required|string|nullable',
            "page"=>'required|integer|nullable',
            "limit"=>'required|integer|nullable',
        ];
    }
}
