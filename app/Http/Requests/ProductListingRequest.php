<?php

namespace App\Http\Requests;

use App\Enum\ShopType;
use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class ProductListingRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'limit' => 'required_with:search_text|integer|nullable',
            'page' => 'required_with:search_text|integer|nullable',
            'lat' => 'required_with:search_text|string|nullable',
            'lng' => 'required_with:search_text|string|nullable',
            'category_id' => 'array',
            'search_text' => 'sometimes|string|nullable',
            'shop_id' => 'sometimes|nullable',
            "shop_type" => 'sometimes|nullable|in:'. ShopType::TakeawayShop .','. ShopType::DeliveryShop . ',' . ShopType::DeliveryAndTakeawayShop,
        ];
    }
}
