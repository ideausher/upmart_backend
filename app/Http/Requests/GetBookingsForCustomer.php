<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class GetBookingsForCustomer extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // status : 1 => ORDER_ACCEPTED, 2 => ORDER_REJECTED, 3 => ORDER_COMPLETED, 4 => ORDER_INPROGRESS, 5 => PENDING_ORDERS
        return [
            'status' => 'required|array',
            'status.*' => 'required|integer|in:1,2,3,4,5',
            'limit' => 'required|integer',
            'page' => 'required|integer'
        ];
    }
}
