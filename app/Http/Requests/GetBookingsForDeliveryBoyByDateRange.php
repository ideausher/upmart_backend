<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class GetBookingsForDeliveryBoyByDateRange extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // status : 1 => ORDER_ACCEPTED, 2 => ORDER_REJECTED, 3 => ORDER_COMPLETED, 4 => ORDER_INPROGRESS, 5 => PENDING_ORDERS
        return [
            'start_date' => 'required|date|date_format:Y-m-d',
            'end_date' => 'required|date|date_format:Y-m-d',
        ];
    }
}
