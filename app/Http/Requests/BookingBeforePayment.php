<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;
use Intersoft\Auth\App\Http\Rules\CheckProductStockAvailability;

class BookingBeforePayment extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "addressId" => 'required_if:booking_type,1|exists:user_addresses,id',
            "shopId" => "required|exists:admin_vendors,id",
            "couponId" => 'sometimes|nullable|exists:coupons,id',
            "booking_type" => 'required|in:1,2',
            "products" => [
                "sometimes",
                "array"
            ],
            "products.*.id"=> [
                "required"
            ],
            "products.*.quantity"=> [
                "required"
            ],
            "scheduled_time" => [
                "nullable",
                "date_format:Y-m-d H:i:s",
                // "after_or_equal:now"
            ]
        ];
    }
}
