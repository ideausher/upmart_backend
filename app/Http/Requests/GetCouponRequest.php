<?php

namespace App\Http\Requests;

use App\Enum\CouponType;
use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class GetCouponRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "page" => 'required|integer',
            "limit" => 'required|integer|integer',
            "is_global" => 'required|integer|in:'. CouponType::GlobalCoupon . ',' .CouponType::BothGlobalAndNonGlobalCoupon, 
            "shop_id" => 'required_if:is_global,'. CouponType::BothGlobalAndNonGlobalCoupon.'|integer|exists:admin_vendors,id',
            "only_primary" => 'required_with:shop_id|integer|in:0,1',

        ];
    }
}
