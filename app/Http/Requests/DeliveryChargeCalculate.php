<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class DeliveryChargeCalculate extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "addressId" => 'required|exists:user_addresses,id',
            "shopId" => "required|exists:admin_vendors,id"
        ];
    }
}
