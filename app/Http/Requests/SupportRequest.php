<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class SupportRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_id' => 'required|integer|exists:booking_details,id',
            'feedback_title' => 'required|string',
            'feedback_description' => 'required|string',
            'images' => 'sometimes|array',
        ];
    }
}
