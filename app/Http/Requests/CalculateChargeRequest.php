<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class CalculateChargeRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "address_id" => 'required_without:lat|exists:user_addresses,id',
            "lat" => 'required_without:address_id|string',
            "lng" => 'required_without:address_id|string',
            "shop_id" => "required|exists:admin_vendors,id",
            "coupon_id" => 'sometimes|nullable|exists:coupons,id',
            "products" => [
                "sometimes",
                "array"
            ],
            "products.*.id"=> [
                "required"
            ],
            "products.*.quantity"=> [
                "required"
            ],
            "booking_type" => 'required|in:1,2'
        ];
    }
}
