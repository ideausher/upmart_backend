<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class GetSlotsRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "shop_id"=>'required|numeric|exists:slots,shop_id',
        ];
    }

    public function messages()
    {
        return [
            'shop_id.exists'=>'Slot is not available for this shop'
        ];
    }
}
