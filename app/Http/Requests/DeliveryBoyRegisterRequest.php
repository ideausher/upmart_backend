<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class   DeliveryBoyRegisterRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "vehicle_registeration.*.registeration_number" => 'string|sometimes|nullable',
            "vehicle_registeration.*.vehicle_picture"=>'string|sometimes',
            "vehicle_registeration.*.vehicle_company"=>'string|sometimes',
            "vehicle_registeration.*.model" => 'sometimes|string|',
            "vehicle_registeration.*.color" => 'string|sometimes',
            "vehicle_registeration.*.valid_upto" => 'sometimes|string',
            "driving_licence.*.drivinglicence_images"=>'sometimes|string',
            "driving_licence.*.drivinglicence_no" => 'sometimes|string',
            "driving_licence.*.drivinglicence_expiry_date" => 'sometimes|string',
            "licence_no_plate.*.licence_no_plate_images"=>'sometimes|string',
            "licence_no_plate.*.no_plate"=>'sometimes|string',
            "vehicle_insurance.*.vehicle_insurence_image"=>'sometimes|string',
            "vehicle_insurance.*.policy_number" =>'sometimes|string'
           
                ];
            
    }
    public function messages()
    {
        return [
            'vehicle_registeration.*.registeration_number.string'=>"registeration_number must be string",
            'vehicle_registeration.*.vehicle_picture.string'=>"vehicle_picture must be string",
            'vehicle_registeration.*.vehicle_company.string'=>"vehicle_company must be string",
            'vehicle_registeration.*.model.string'=>"model must be string",
            'vehicle_registeration.*.color.string'=>"color must be string",
            'vehicle_registeration.*.valid_upto.string'=>"valid_upto must be string",
            'driving_licence.*.drivinglicence_images.string'=>"drivinglicence_images must be string",
            'driving_licence.*.drivinglicence_no.string'=>"drivinglicence_no must be string",
            'driving_licence.*.drivinglicence_expiry_date.string'=>"drivinglicence_expiry_date must be string",
            'licence_no_plate.*.licence_no_plate_images.string'=>"licence_no_plate_images must be string",
            'licence_no_plate.*.no_plate.string'=>"no_plate must be string",
            'vehicle_insurance.*.vehicle_insurence_image.string'=>"vehicle_insurence_image must be string",
            'vehicle_insurance.*.policy_number.string'=>"policy_number must be string"

            

        ];
    }
}
