<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  IntersoftStripe\Http\Controllers\StripeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('api')->middleware('api')->group(function(){
    Route::group(['prefix' => 'v1', 'middleware' =>  ['auth:api','apiDataLogger']], function () {
        Route::get('accountLinked',[StripeController::class, 'accountLinked']);
        Route::get('customerExists', [StripeController::class,'customerExists']);
        Route::post('linkAccount', [StripeController::class,'linkAccount']);
        Route::post('createCustomer',[StripeController::class, 'createCustomer']);

    });
});
