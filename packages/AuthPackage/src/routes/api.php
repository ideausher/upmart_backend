<?php

use Illuminate\Support\Facades\Route;
use Intersoft\Auth\App\Http\Controllers\UserController;
use Intersoft\Auth\App\Http\Controllers\UserBasedLocationController;

Route::prefix('api')->middleware('api')->group(function(){
    Route::POST('register', [UserController::class, 'register']);
    Route::POST('login', [UserController::class, 'login']);
    Route::POST('social-login', [UserController::class, 'socialLogin']);
    Route::POST('send-otp', [UserController::class, 'sendOtp']);
    Route::POST('verify-otp', [UserController::class, 'verifyOtp']);
    Route::POST('check-user', [UserController::class, 'checkUserExistence']);
    Route::POST('forget-password', [UserController::class, 'forgetChangePassword']);
    Route::POST('verify-email-or-phone', [UserController::class, 'verifyEmailOrPhone']);
    Route::POST('get-otp', [UserController::class, 'getOtp']);
    Route::POST('isblock', [UserController::class, 'isblock']);
    Route::group(['middleware' => ['auth:api','apiDataLogger']], function() {
        Route::group(['middleware' => ['auth:api']], function () {
            Route::POST('updatelocation', [UserBasedLocationController::class, 'updatelocation']);
            Route::GET('getcurrentlocation', [UserBasedLocationController::class, 'getcurrentlocation']);
            Route::POST('forget-password-login', [UserController::class, 'forgetChangePassword']);
            Route::POST('upload-image', [UserController::class, 'uploadImage']);
            Route::POST('imageUpload', [UserController::class, 'imageUpload']);
            Route::POST('delete-image', [UserController::class, 'deleteImage']);
            Route::POST('update-profile', [UserController::class, 'updateProfile']);
            Route::GET('get-profile', [UserController::class, 'getProfile']);
            Route::POST('edit-address', [UserController::class, 'editAddress']);
            Route::POST('add-address', [UserController::class, 'addAddress']);
            Route::POST('delete-address', [UserController::class, 'deleteAddress']);
            Route::POST('logout', [UserController::class, 'logout']);
            Route::POST('availability', [UserController::class, 'updateavailability']);
            Route::GET('availability', [UserController::class, 'getavailability']);
            Route::POST('get-address', [UserController::class, 'getAddress']);
            Route::POST('verify-email-or-phone', [UserController::class, 'verifyEmailOrPhone']);
            Route::GET('code/referral', [UserController::class, 'myReferralCode']);
        });
    });
});
