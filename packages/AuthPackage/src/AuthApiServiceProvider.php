<?php
namespace Intersoft\Auth;

use App\Service\V1\FaqService;
use App\Service\V1\ShopService;
use App\Repository\FaqRepository;
use App\Repository\OtpRepository;
use App\Service\V1\BannerService;
use App\Service\V1\CouponService;
use App\Service\V1\ReviewService;
use App\Repository\ItemRepository;
use App\Repository\ShopRepository;
use App\Service\V1\BookingService;
use App\Repository\OrderRepository;
use App\Service\V1\CategoryService;
use App\Repository\BannerRepository;
use App\Repository\CouponRepository;
use App\Repository\ReviewRepository;
use App\Repository\BookingRepository;
use App\Repository\SettingRepository;
use App\Repository\CategoryRepository;
use Illuminate\Support\ServiceProvider;
use App\Repository\FaqCategoryRepository;
use App\Service\V1\HelpAndSupportService;
use App\Service\V1\DeliveryBoyDocServices;
use App\Repository\UserAddressesRepository;
use App\Service\V1\UserNotificationService;
use Intersoft\Auth\App\Repository\Criteria;
use App\Repository\HelpAndSupportRepository;
use App\Service\V1\UserBasedLocationService;
use App\Repository\FailedTransfersRepository;
use App\Repository\Interfaces\IFaqRepository;
use App\Repository\Interfaces\IItemInterface;
use App\Repository\Interfaces\IOtpRepository;
use App\Repository\Interfaces\IShopRepository;
use Intersoft\Auth\App\Service\V1\UserService;
use App\Repository\Interfaces\IOrderRepository;
use App\Repository\UserBasedLocationRepository;
use App\Service\Interfaces\FaqServiceInterface;
use App\Repository\Interfaces\IBannerRepository;
use App\Repository\Interfaces\ICouponRepository;
use App\Repository\Interfaces\IReviewRepository;
use App\Service\Interfaces\ShopServiceInterface;
use App\Service\V1\PageForTermsAndPolicyService;
use App\Repository\BookingNotificationRepository;
use App\Repository\DeliveryBoyRegisterRepository;
use App\Repository\Interfaces\IBookingRepository;
use App\Repository\Interfaces\ISettingRepository;
use Intersoft\Auth\App\Repository\UserRepository;
use App\Repository\Interfaces\ICategoryRepository;
use App\Service\Interfaces\BannerServiceInterface;
use App\Service\Interfaces\CouponServiceInterface;
use App\Service\Interfaces\ReviewServiceInterface;
use App\Repository\PageForTermsAndPolicyRepository;
use App\Service\Interfaces\BookingServiceInterface;
use App\Repository\BookingTrackingHistoryRepository;
use App\Service\Interfaces\CategoryServiceInterface;
use Intersoft\Auth\App\Repository\GenericRepository;
use App\Repository\Interfaces\IFaqCategoryRepository;
use App\Service\Interfaces\UserNotificationInterface;
use App\Repository\Interfaces\IUserAddressesRepository;
use App\Repository\Interfaces\IHelpAndSupportRepository;
use App\Repository\Interfaces\IVariousChargesRepository;
use App\Repository\Interfaces\IFailedTransfersRepository;
use App\Repository\Interfaces\IUserNotificationRepository;
use App\Service\Interfaces\HelpAndSupportServiceInterface;
use App\Repository\Interfaces\IUserBasedlocationRepository;
use App\Service\Interfaces\DeliveryBoyDocServicesInterface;
use Intersoft\Auth\App\Repository\VariousChargesRepository;
use App\Repository\Interfaces\IBookingNotificationRepository;
use App\Repository\Interfaces\IDeliveryBoyRegisterRepository;
use App\Service\Interfaces\UserBasedLocationServiceInterface;
use Intersoft\Auth\App\Repository\Interfaces\IUserRepository;
use Intersoft\Auth\App\Repository\UserNotificationRepository;
use App\Repository\Interfaces\IPageForTermsAndPolicyRepository;
use Intersoft\Auth\App\Service\Interfaces\UserServiceInterface;
use App\Repository\Interfaces\IBookingTrackingHistoryRepository;
use Intersoft\Auth\App\Repository\Interfaces\ICriteriaInterface;
use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;
use App\Repository\Interfaces\IVariousChargesOnBookingRepository;
use App\Service\Interfaces\PageForTermsAndPolicyServiceInterface;
use Intersoft\Auth\App\Repository\VariousChargesOnBookingRepository;

class AuthApiServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__ . '/routes/api.php');
	}

	public function register()
	{
		$this->app->bind(UserServiceInterface::class,UserService::class);
		$this->app->bind(IUserRepository::class,UserRepository::class);
		$this->app->bind(IOtpRepository::class,	OtpRepository::class);
		$this->app->bind(IUserAddressesRepository::class,UserAddressesRepository::class);
		$this->app->bind(IGenericRepository::class,GenericRepository::class);
		$this->app->bind(ICriteriaInterface::class,Criteria::class);
		$this->app->bind(ICategoryRepository::class,CategoryRepository::class);
		$this->app->bind(CategoryServiceInterface::class,CategoryService::class);
		$this->app->bind(IShopRepository::class,ShopRepository::class);
		$this->app->bind(ShopServiceInterface::class,ShopService::class);
		$this->app->bind(ICouponRepository::class,CouponRepository::class);
		$this->app->bind(CouponServiceInterface::class,CouponService::class);
		$this->app->bind(DeliveryBoyDocServicesInterface::class,DeliveryBoyDocServices::class);
		$this->app->bind(IDeliveryBoyRegisterRepository::class,DeliveryBoyRegisterRepository::class);
		$this->app->bind(UserBasedLocationServiceInterface::class,UserBasedLocationService::class);
		$this->app->bind(IUserBasedlocationRepository::class,UserBasedLocationRepository::class);
		$this->app->bind(IItemInterface::class,ItemRepository::class);
		$this->app->bind(IBookingRepository::class,BookingRepository::class);
		$this->app->bind(BookingServiceInterface::class,BookingService::class);
		$this->app->bind(IOrderRepository::class,OrderRepository::class);
		$this->app->bind(IVariousChargesRepository::class,VariousChargesRepository::class);
		$this->app->bind(IVariousChargesOnBookingRepository::class,VariousChargesOnBookingRepository::class);
		$this->app->bind(PageForTermsAndPolicyServiceInterface::class,PageForTermsAndPolicyService::class);
		$this->app->bind(IPageForTermsAndPolicyRepository::class,PageForTermsAndPolicyRepository::class);
		$this->app->bind(IBannerRepository::class,BannerRepository::class);
		$this->app->bind(BannerServiceInterface::class,BannerService::class);
		$this->app->bind(IBookingTrackingHistoryRepository::class,BookingTrackingHistoryRepository::class);
		$this->app->bind(ReviewServiceInterface::class,ReviewService::class);
		$this->app->bind(IReviewRepository::class,ReviewRepository::class);
		$this->app->bind(IHelpAndSupportRepository::class,HelpAndSupportRepository::class);
		$this->app->bind(HelpAndSupportServiceInterface::class,HelpAndSupportService::class);
		$this->app->bind(ISettingRepository::class, SettingRepository::class);
		$this->app->bind(IBookingNotificationRepository::class, BookingNotificationRepository::class);
		$this->app->bind(FaqServiceInterface::class,FaqService::class);
		$this->app->bind(IFaqRepository::class,FaqRepository::class);
		$this->app->bind(IFaqCategoryRepository::class,FaqCategoryRepository::class);
		$this->app->bind(IUserNotificationRepository::class,UserNotificationRepository::class);
		$this->app->bind(UserNotificationInterface::class,UserNotificationService::class);
		$this->app->bind(IFailedTransfersRepository::class,FailedTransfersRepository::class);
		$this->app->bind(IFavUnfavRepository::class,FavUnfavRepository::class);
		
		
	}
}
