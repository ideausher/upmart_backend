<?php
namespace Intersoft\Auth\App\Exceptions;
use Exception;
use Throwable;
class RecordNotFoundException extends Exception implements Throwable
{

}
?>