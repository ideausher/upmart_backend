<?php

namespace Intersoft\Auth\App\Exceptions;

use Exception;
use Throwable;

class BadRequestException extends Exception implements Throwable
{
}
