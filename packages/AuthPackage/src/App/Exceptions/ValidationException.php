<?php

namespace Intersoft\Auth\App\Exceptions;

use Exception;
use Throwable;

class ValidationException extends Exception implements Throwable
{
    //
}
