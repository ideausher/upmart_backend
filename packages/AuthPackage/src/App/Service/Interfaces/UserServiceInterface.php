<?php

namespace Intersoft\Auth\App\Service\Interfaces;
use Illuminate\Support\Facades\Request;
interface UserServiceInterface
{
	/**
	 * Send OTP
	 *
	 * @param Request $request
	 * @return void
	 */
	function sendOtp(Request $request);

	/**
	 * Verify OTP
	 *
	 * @param Request $request
	 * @return void
	 */
	function verifyOtp(Request $request);

	/**
	 * Register
	 *
	 * @param Request $request
	 * @return void
	 */
	function register(Request $request);
}