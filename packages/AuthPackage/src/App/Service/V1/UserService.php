<?php

namespace Intersoft\Auth\App\Service\V1;

use Illuminate\Http\Request;
use App\Exceptions as Exceptions;
use App\Models\DeviceDetails;
use Illuminate\Support\Facades\Auth;
use Intersoft\Auth\App\Enum\OtpEnum;
use Intersoft\Auth\App\Enum\LoginEnum;
use Illuminate\Support\Facades\Storage;
use Intersoft\Auth\App\Enum\IsBlockEnum;
use Intersoft\Auth\App\Traits\ApiUserTrait;
use Intersoft\Auth\App\Helpers\V1\OtpHelper;
use App\Repository\Interfaces\IOtpRepository;
use Intersoft\Auth\App\Helpers\V1\UserHelper;
use App\Repository\Interfaces\ICouponRepository;
use App\Repository\Interfaces\IUserAddressesRepository;
use Intersoft\Auth\App\Repository\Interfaces\IUserRepository;
use Intersoft\Auth\App\Service\Interfaces\UserServiceInterface as InterfacesUserServiceInterface;

class UserService implements InterfacesUserServiceInterface
{

    use ApiUserTrait;
    // use APIResponse;
    private $userRepo;
    private $otpHelper;
    private $otpRepo;
    private $userHelper;
    private $couponRepo;

    public function __construct(
        IUserRepository $userRepo,
        IUserAddressesRepository $userAddressRepo,
        IOtpRepository $otpRepo,
        ICouponRepository $couponRepo,
        OtpHelper $otpHelper,
        UserHelper $userHelper
    ) {
        $this->userRepo = $userRepo;
        $this->otpRepo = $otpRepo;
        $this->otpHelper = $otpHelper;
        $this->userHelper = $userHelper;
        $this->userAddressRepo = $userAddressRepo;
        $this->couponRepo = $couponRepo;
    }
    //For register user
    public function register($request)
    {
        $data = $this->userHelper->User($request); //Check parameters exist or not and return array

        $registerUser = $this->userRepo->createUser($data);
        
        if ($request->address) { //for save address
            $save_address_status = $this->userAddressRepo->saveUserAddress($this->userHelper->getInsertUserAddressData($request->address, $registerUser->id));
            

            if (!$save_address_status) {
                throw new Exceptions\BadRequestException("User address not store");
            }
           
            return  $registerUser;
        }
        return $registerUser;
    }

    public function login($request)
    {
        if (isset($request['type']) && isset($request['country_code']) && isset($request['phone_number'])
        && isset($request['password']) && $request['type'] == LoginEnum::phone && isset($request['token'])) {
            // $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
            
            $user = $this->userRepo->attemptAuthPhoneNo($request['phone_number'], $request['country_code'], $request['password']);
    
            if (!$user) {
                throw new Exceptions\RecordNotFoundException("invalid details");
            }
            if ($request['token'] == LoginEnum::tokenNeeded) {
                $user->auth_token = $user->createToken('Api access token')->accessToken;
                $this->insertDeviceDetails($user->auth_token, $user->id);
                $user->responseMessage = 'login successfully ';
                return $user;
            }
            $user->responseMessage = 'login successfully ';
            return $user;
        } elseif (isset($request['type']) && isset($request['email']) && isset($request['password']) && $request['type'] == LoginEnum::email && isset($request['token'])) {
            // $user = $this->userRepo->findUserWithEmailId($request['email']);
            $user = $this->userRepo->attemptAuthEmailId($request['email'], $request['password']);
            if (!$user) {
                throw new Exceptions\RecordNotFoundException("invalid details");
            }
            if ($request['token'] == LoginEnum::tokenNeeded) {
                $user->auth_token = $user->createToken('Api access token')->accessToken;
                $this->insertDeviceDetails($user->auth_token, $user->id);
                $user->responseMessage = 'login successfully ';
                return $user;
            }
            $user->responseMessage = 'login successfully ';
            return $user;
        } elseif (isset($request['phone_number']) && isset($request['country_code']) && $request['type'] == LoginEnum::phoneWithOtp && isset($request['token'])) {
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
        
            if (!$user && !empty($request['registration'])  && $request['registration'] == LoginEnum::registration) {
                $data = $this->userHelper->User($request);
                $user = $this->userRepo->createUser($data);
            }
            if ($request['token'] == LoginEnum::tokenNeeded) {
                if ($user) {
                    $user->auth_token = $user->createToken('Api access token')->accessToken;
                    $this->insertDeviceDetails($user->auth_token, $user->id);
                    $user->responseMessage = 'login successfully ';
                    return $user;
                } else {
                    throw new Exceptions\RecordNotFoundException("user not exist");
                }
            } else {
                
                $user->responseMessage = 'login successfully ';
                return $user;
            }
        } else {
            throw new Exceptions\BadRequestException("invalid details");
        }
    }

    public function socialLogin($request)
    {
        $user = $this->checkSocialUserExistence($request);
        if ($user) {
            $attempt = $this->userRepo->attemptAuthSocialId($request['social_id'], $request['social_type']);
            $attempt->responseMessage = 'login successfully ';
            $attempt->auth_token = $attempt->createToken('Api access token')->accessToken;
            return $attempt;
        } else {
            $user = $this->checkEmailExistence($request);
            if($user){
                throw new Exceptions\BadRequestException("Email Already Exists.");
            }
            $user = $this->checkPhoneNoExistence($request);
            if($user){
                throw new Exceptions\BadRequestException("Phone Number Already Exists.");
            }
            $user = $this->userRepo->createSocialUser($request);
            $attempt = $this->userRepo->attemptAuthSocialId($request['social_id'], $request['social_type']);
            $attempt->auth_token = $attempt->createToken('Api access token')->accessToken;
            return $attempt;
        }
    }

    public function checkEmailExistence($request)
    {
        if (isset($request['email'])) {
            $user = $this->userRepo->findUserWithEmailId($request['email']);
            if (isset($user)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function checkPhoneNoExistence($request)
    {
        if (isset($request['phone_number'])) {
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'],$request['country_code'] );
            if (isset($user)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function sendOtp($request)
    {
        if (isset($request['type']) && isset($request['country_code']) && isset($request['phone_number']) && $request['type'] == OtpEnum::onPhone) {
            $phoneno = $request['country_code'] . $request['phone_number'];
            $DEFAULT_OTP_TO_PHONE_NUMBER = env('DEFAULT_OTP_TO_PHONE_NUMBER');
            if(in_array($request['phone_number'],explode(',',env('DEFAULT_OTP_TO_PHONE_NUMBER')))){
                $get_otp = $this->otpHelper->get_static_otp();
                
            }
            else{
                $get_otp = $this->otpHelper->get_otp();
            }                   
            $send_otp = $this->otpHelper->send_otp_phone($phoneno, $get_otp);
            if (!$send_otp) {
                throw new Exceptions\BadRequestException("Otp not send");
            } else {
                $this->otpRepo->saveOTP($get_otp, $request['phone_number'], $request['country_code']);
                throw new Exceptions\SuccessException('Otp send on phone number successfully');
            };
        }
        if (isset($request['type']) && isset($request['email']) && $request['type'] == OtpEnum::onEmail) {
            $email = $request['email'];
            if(in_array($request['email'],explode(',',env('DEFAULT_OTP_TO_EMAIL')))){
                $get_otp = $this->otpHelper->get_static_otp();
            }
            else{
                $get_otp = $this->otpHelper->get_otp();
            }
            $message = "OTP for the Upmart App is : " . $get_otp . ". Please don't share it with anybody.";
            $send_otp = $this->otpHelper->send_otp_email($email, $message);
            if (!$send_otp) {
                throw new Exceptions\BadRequestException("Otp not send");
            } else {
                $this->otpRepo->saveOTP($get_otp, '', '', $request['email']);
                throw new Exceptions\SuccessException('Otp send on email successfully');
            };
            //in case of forget passsword or register tym otp send on both email & ph
        }
        if (isset($request['type']) && isset($request['email']) && isset($request['phone_number']) && $request['type'] == OtpEnum::onEmailAndPhone) {
            if(in_array($request['email'],explode(',',env('DEFAULT_OTP_TO_EMAIL')))){
                $get_otp = $this->otpHelper->get_static_otp();
            }
            else{
                $get_otp = $this->otpHelper->get_otp();
            }
            $get_otp = $this->otpHelper->get_otp();
            $phoneno = $request->country_code . $request->phone_number;
            $email = $request->email;
            $message = "OTP for the Upmart App is : " . $get_otp . ". Please don't share it with anybody.";
            $send_otp_phone = $this->otpHelper->send_otp_phone($phoneno, $get_otp);
            $send_otp_email = $this->otpHelper->send_otp_email($email, $message);
            if ($send_otp_phone & $send_otp_email) {
                $this->otpRepo->saveOTP($get_otp, $request['phone_number'], $request['country_code'], $request['email']);
                throw new Exceptions\SuccessException('Otp send on email & phone Number successfully');
            } else {
                throw new Exceptions\BadRequestException("Otp not send");
            }
        } else {
            throw new Exceptions\RecordNotFoundException("Invalid details");
        }
    }

    public function verifyOtp($request)
    {
        if ($request['type'] == OtpEnum::onPhone && isset($request['phone_number']) && isset($request['country_code'])) {
            $validOtp = $this->otpRepo->checkOtpOnPhone($request['phone_number'], $request['country_code'], $request['otp']);
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
        }
        if ($request['type'] == OtpEnum::onEmail && isset($request['email'])) {
            $validOtp = $this->otpRepo->checkOtpOnEmail($request['email'], $request['otp']); 
            $user = $this->userRepo->findUserWithEmailId($request['email']);
        }
        if ($request['type'] == OtpEnum::onEmailAndPhone && isset($request['email']) && isset($request['phone_number']) && isset($request['country_code'])) {
            $validOtp = $this->otpRepo->checkOtpOnBoth($request['email'], $request['phone_number'], $request['country_code'], $request['otp']);
            $user = $this->userRepo->findUserWithPhoneNumberAndEmailId($request['email'], $request['phone_number'], $request['country_code']);
        }

        if (isset($validOtp)) {
            //update otp
            $this->otpRepo->updateOtpData($validOtp, ['is_verified' => '1']);

            if ($request['action'] == OtpEnum::userVerify) { // if action is 1 then verify user also
                $user = $this->userVerified($request);
                unset($user['email_verified_at']);
                throw new Exceptions\SuccessException('Otp and User verified successfully');
            }
            throw new Exceptions\SuccessException('Otp verified successfully');
        } else {
            throw new Exceptions\BadRequestException("Invalid Otp");
        }
    }

    public function userVerified($request)
    {
        if ($request['type'] == OtpEnum::onPhone) {
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
            if (isset($user)) {
                $userVerified = $this->userRepo->updateUser($user, ['verified' => '1']);
                
                return $userVerified;
            } else {
                throw new Exceptions\RecordNotFoundException("Otp Verified but User Not found");
            }
        }
        if ($request['type'] == OtpEnum::onEmail) {
            $user = $this->userRepo->findUserWithEmailId($request['email']);
            if (isset($user)) {
                $userVerified = $this->userRepo->updateUser($user, ['verified' => '1']);
                return $userVerified;
            } else {
                throw new Exceptions\RecordNotFoundException("Otp Verified but User Not found");
            }
        }
        if ($request['type'] == OtpEnum::onEmailAndPhone) {
            $user = $this->userRepo->findUserWithPhoneNumberAndEmailId($request['phone_number'], $request['country_code'], $request['email']);
            if (isset($user)) {
                $userVerified = $this->userRepo->updateUser($user, ['verified' => '1']);
                return $userVerified;
            } else {
                throw new Exceptions\RecordNotFoundException("Otp Verified but User Not found");
            }
        }
        return false;
    }
    public function checkSocialUserExistence($request)
    {
        if (isset($request['social_id']) && isset($request['social_type'])) {
            $user = $this->userRepo->findUserWithSocialId($request['social_id']);
            if (isset($user)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function checkUserExistence($request)
    {
        if (isset($request['phone_number']) && isset($request['country_code']) && isset($request['email']) && !isset($request['social_id'])) {
            $user = $this->userRepo->findUserWithPhoneNumberAndEmailId($request['phone_number'], $request['country_code'], $request['email']);
            if (isset($user)) {
                throw new Exceptions\SuccessException('user already exist');
            }
        } elseif (isset($request['phone_number']) && isset($request['country_code']) && !isset($request['social_id'])) {
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
            if (isset($user)) {
                throw new Exceptions\SuccessException('user already exist');
            }
        } elseif (isset($request['email']) && !isset($request['social_id'])) {
            $user = $this->userRepo->findUserWithEmailId($request['email']);
            if (isset($user)) {
                throw new Exceptions\SuccessException('user already exist');
            }
        } elseif (isset($request['social_id']) && isset($request['social_type'])) {
            $user = $this->userRepo->findUserWithSocialId($request['social_id']);
            if (isset($user)) {
                throw new Exceptions\SuccessException('user already exist');
            }
        }
        throw new Exceptions\RecordNotFoundException("user not found");
    }

    public function forgetChangePassword($request)
    {
        if ($request['authorization'] == LoginEnum::loginUser && isset($request['new_password'])) {
            $currentUser = Auth::user();
            if (!$currentUser) {
                throw new Exceptions\RecordNotFoundException("User not login");
            }
            $currentUser->update(['password' => $request['new_password']]);
            throw new Exceptions\SuccessException('Update successfully');
        } elseif ($request['authorization'] == LoginEnum::notLogin && isset($request['type'])) {
            if ($request['type'] == LoginEnum::phone) {
                $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
                if ($user) {
                    $user->update(['password' => $request['new_password']]);
                    throw new Exceptions\SuccessException('Update successfully');
                } else {
                    throw new Exceptions\RecordNotFoundException("User not find");
                }
            } elseif ($request['type'] == LoginEnum::email) {
                $user = $this->userRepo->findUserWithEmailId($request['email']);
                if ($user) {
                    $user->update(['password' => $request['new_password']]);
                    throw new Exceptions\SuccessException('Update successfully');
                } else {
                    throw new Exceptions\RecordNotFoundException("User not find");
                }
            } elseif ($request['type'] == LoginEnum::phoneWithOtp) {
                $user = $this->userRepo->findUserWithPhoneNumberAndEmailId($request['phone_number'], $request['country_code'], $request['email']);
                if ($user) {
                    $user->update(['password' => $request['new_password']]);
                    throw new Exceptions\SuccessException('Update successfully');
                } else {
                    throw new Exceptions\RecordNotFoundException("User not find");
                }
            }
        }
    }
    public function uploadImage($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $filename = time() . $file->extension();
                //store url
                $storagePath = Storage::disk('s3')->put($filename, file_get_contents($file));
                $path = Storage::disk('s3')->url($filename);
                if ($path) {
                    $currentUser->update(['profile_picture' => $path]); 
                }
                $currentUser->responseMessage = 'image upload successfully ';
                return $currentUser;
                throw new Exceptions\SuccessException('image upload successfully');
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    public function imageUpload($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            if ($request->hasfile('image')) {
                $file = $request->file('image');
                $filename = time() . str_replace(" ","",$file->getClientOriginalName());
                //store url
                if (env('AWS_BUCKET') !== null && env('AWS_ACCESS_KEY_ID') !== null && env('AWS_SECRET_ACCESS_KEY') !== null && env('AWS_DEFAULT_REGION') !== null) {
                    $storagePath = Storage::disk('s3')->put($filename, file_get_contents($file), 'public');
                    $path = Storage::disk('s3')->url($filename);
                    $currentUser->responseMessage = 'image upload successfully ';
                    return ['image_path' => $path];
                } else {
                    $request->image->move(public_path('images'), $filename);
                    // $path = URL::to("/images/$filename");
                    $path = "/public/images/$filename";
                    $currentUser->responseMessage = 'image upload successfully ';
                    return ['image_path' => $path];
                }
            }
        }else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    public function deleteImage($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $file = $request->url;
            $filename = $file;
            $filename = basename($file);
            if (!Storage::disk('s3')->exists($filename)) {
                throw new Exceptions\RecordNotFoundException('not found');
            }
            Storage::disk('s3')->delete($filename);
            $currentUser->update(['profile_picture' => null]);
            throw new Exceptions\SuccessException('delete successfully');
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }


    public function verifyEmailOrPhone($request)
    {
        if ($request->header('Authorization')) {
            if ($request['type'] == OtpEnum::onPhone && isset($request['phone_number']) && isset($request['country_code'])) {
                $validOtp = $this->otpRepo->checkOtpOnPhone($request['phone_number'], $request['country_code'], $request['otp']);
            }
            if ($request['type'] == OtpEnum::onEmail && isset($request['email'])) {
                $validOtp = $this->otpRepo->checkOtpOnEmail($request['email'], $request['otp']);
            }
            if ($request['type'] == OtpEnum::onEmailAndPhone && isset($request['email']) && isset($request['phone_number']) && isset($request['country_code'])) {
                $validOtp = $this->otpRepo->checkOtpOnBoth($request['email'], $request['phone_number'], $request['country_code'], $request['otp']);
            }

            if (isset($validOtp)) {
                //update otp
                  
                $this->otpRepo->updateOtpData($validOtp, ['is_verified' => '1']);
                $user = Auth::user();
                if (isset($request['title'])) {
                     $this->userRepo->updateUser($user, ['title' => $request['title']]);
                }
                if (isset($request['name'])) {
                    $this->userRepo->updateUser($user, ['name' => $request['name']]);
                }

                if (isset($request['email'])) {
                    $this->userRepo->updateUser($user, ['email' => $request['email']]);
                }

                if (isset($request['phone_number'])) {
                    $this->userRepo->updateUser($user, ['phone_number' => $request['phone_number']]);
                }

                if (isset($request['country_code'])) {
                    $this->userRepo->updateUser($user, ['country_code' => $request['country_code']]);
                }

                if (isset($request['country_iso_code'])) {
                    $this->userRepo->updateUser($user, ['country_iso_code' => $request['country_iso_code']]);
                }  
                $user->responseMessage = 'Update successfully ';
                return $user;
               
            } else {
                throw new Exceptions\BadRequestException("Invalid Otp");
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    public function updateProfile($request)
    {
        $user = Auth::user();
        if (isset($request['title'])) {
            $this->userRepo->updateUser($user, ['title' => $request['title']]);
        }
        if (isset($request['name'])) {
            $this->userRepo->updateUser($user, ['name' => $request['name']]);
        }

        if (isset($request['email'])) {
            $this->userRepo->updateUser($user, ['email' => $request['email']]);
        }

        if (isset($request['phone_number'])) {
            $this->userRepo->updateUser($user, ['phone_number' => $request['phone_number']]);
        }

        if (isset($request['country_code'])) {
            $this->userRepo->updateUser($user, ['country_code' => $request['country_code']]);
        }

        if (isset($request['country_iso_code'])) {
            $this->userRepo->updateUser($user, ['country_iso_code' => $request['country_iso_code']]);
        }
        if (isset($request['date_of_birth'])) {
            $this->userRepo->updateUser($user, ['date_of_birth' => $request['date_of_birth']]);
        }
        if (isset($request['referred_by_code']) && ! empty($request['referred_by_code'])) {

            if($request['referred_by_code'] == $user->referral_code){
                throw new Exceptions\BadRequestException("Sorry! You can not use your own referral code");
            }
            if($user->referred_by_code){
                throw new Exceptions\BadRequestException("Sorry! You have already used a referral codes");
            }
            
            $this->userRepo->updateUser($user, ['referred_by_code' => $request['referred_by_code']]);
            $referrerUser = $this->userRepo->getUserByReferralCode($request['referred_by_code']);
            $this->couponRepo->createCouponOnReferral(Auth::user()->id);
            $this->couponRepo->createCouponOnReferral($referrerUser->id);
        }
        
        $user->responseMessage = 'Update successfully ';
        return $user;
    }
    public function getProfile($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $userProfile = $this->userRepo->get_user(['id' => $currentUser->id]);
            $userAddress = $this->userAddressRepo->getUserAddress($currentUser->id);
            $userProfile->token = false;
            $userProfile->responseMessage = '';
            return $userProfile;
        // throw new Exceptions\SuccessException('Add successfully', $userProfile->toArray());
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }

    public function addAddress($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
          
            if ($request->address) { //for save address
            
                $save_address_status = $this->userAddressRepo->saveUserAddress($this->userHelper->getInsertUserAddressData($request->address, $currentUser->id));
              
                if (!$save_address_status) {
                    throw new Exceptions\BadRequestException("User address not store");
                } else {
                    throw new Exceptions\SuccessException('Add successfully');
                }
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    public function deleteAddress($request)
    {
        $delete_address_status = $this->userAddressRepo->deleteUserAddress(['id' => $request->address_id]);

        if (!$delete_address_status) {
            throw new Exceptions\BadRequestException("address not found");
        } else {
            throw new Exceptions\SuccessException('delete successfully');
        }
    }
    public function editAddress($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
           
            if ($request->is_primary==1) {
                $this->userAddressRepo->makeAllUseraaaadresNonPrimarry($currentUser->id);
                $edit_address_status = $this->userAddressRepo->editUserAddress($this->userHelper->getAddress($request, $currentUser->id));
           
                $user_id=$currentUser->id;
                $address_id=$edit_address_status->id;
               
           
                if ($edit_address_status) {
                    $edit= $edit_address_status->where('user_id', $user_id)->where('id', $address_id)->update(['is_primary'=> $request['is_primary']]);
                    $currentUser->responseMessage = 'Update successfully';
                    return $currentUser;
                   
                } else {
                    throw new Exceptions\BadRequestException("address not found");
                }
            } else {
                $edit_address_status = $this->userAddressRepo->editUserAddress($this->userHelper->getAddress($request, $currentUser->id));
           
                $user_id=$currentUser->id;
                $address_id=$edit_address_status->id;
               
               
           
                if ($edit_address_status) {
                    $edit= $edit_address_status->where('user_id', $user_id)->where('id', $address_id)->update(['is_primary'=> $request['is_primary']]);
                    
                    $currentUser->responseMessage='Edit address succesfully';
                    return $currentUser;
                   
                } else {
                    throw new Exceptions\BadRequestException("address not found");
                }
            }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    public function getOtp($request)
    {
        if ($request['type'] == OtpEnum::onPhone && isset($request['phone_number']) && isset($request['country_code'])) {
            $validOtp = $this->otpRepo->getOtpOfPhone($request['phone_number'], $request['country_code']);
        }
        if ($request['type'] == OtpEnum::onEmail && isset($request['email'])) {
            $validOtp = $this->otpRepo->getOtpOfEmail($request['email']);
        }
        if ($request['type'] == OtpEnum::onEmailAndPhone && isset($request['email']) && isset($request['phone_number']) && isset($request['country_code'])) {
            $validOtp = $this->otpRepo->getOtpOfBoth($request['email'], $request['phone_number'], $request['country_code']);
        }
        if (isset($validOtp)) {
            return ['otp'=>$validOtp->otp];
        } else {
            throw new Exceptions\BadRequestException("Otp not found");
        }
    }
    public function logout(Request $request)
    {
        $user = Auth::user();
        $user->availability = 0;
        $user->save();
        $accessToken = $user->token();
        $token = $request->user()->tokens->find($accessToken);
        $deviceTokens = DeviceDetails::where('user_id', $user->id)->delete();
        if ($token->revoke()) {
            return ("logout");
        }
        return false;

    }
    public function isblock($request)
    {
        if (isset($request['phone_number']) && isset($request['country_code']) && isset($request['email']) && !isset($request['social_id'])) {
            $user = $this->userRepo->findUserWithPhoneNumberAndEmailId($request['phone_number'], $request['country_code'], $request['email']);
            if (isset($user)) {
                return ['user_type'=>$user->user_type,'is_blocked'=>$user->is_block];
            }
        } elseif ($request['type'] == IsBlockEnum::Phone && isset($request['phone_number']) && isset($request['country_code'])) {
            $user = $this->userRepo->findUserWithPhoneNumber($request['phone_number'], $request['country_code']);
            if (isset($user)) {
                // return $user;
                return ['user_type'=>$user->user_type,'is_blocked'=>$user->is_block];
            }
        } elseif (isset($request['email']) && !isset($request['social_id'])) {
            $user = $this->userRepo->findUserWithEmailId($request['email']);
            if (isset($user)) {
                return ['user_type'=>$user->user_type,'is_blocked'=>$user->is_block];
            }
        }
        
        throw new Exceptions\RecordNotFoundException("user not found");
    }
    public function getAddress($request)
    {
        if ($request->header('Authorization')) {
            $currentUser = Auth::user();
            $userProfile = $this->userRepo->get_user(['id' => $currentUser->id]);
        
        $userAddress = $this->userAddressRepo->getUserAddress($currentUser->id);
        
        if($userAddress)
            {
            $userProfile->token = false;
            $userAddress->responseMessage = 'Get Address successfully';
            return $userAddress;
        }else{
            return (" no address found");
            
        }
        } else {
            throw new Exceptions\BadRequestException("User not login");
        }
    }
    public function updateavailability($request){
        $user = Auth::user();
        $availablity=  $this->userRepo->updateUser($user, ['availability' => $request['availability']]);
        return ["availability" => $user->availability];
    }
    public function getavailability($request){
        $user = Auth::user();
        return ["availability" => $user->availability];
    }
    public function myReferralCode($request){
        $user = Auth::user();
        return ["referral_code" => $user->referral_code];
    }
    
    
}
