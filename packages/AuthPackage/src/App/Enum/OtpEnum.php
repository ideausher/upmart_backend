<?php

namespace Intersoft\Auth\App\Enum;

class OtpEnum
{
	const onPhone = 1;
	const onEmail = 2;
	const onEmailAndPhone = 3;	
	const userVerify =1;
}
