<?php

namespace Intersoft\Auth\App\Enum;

class LoginEnum
{
	const phone = 1;
	const email = 2;
	const phoneWithOtp = 3;
	const social = 4;

	const tokenNeeded =1;
	const tokenNotNeeded =0;

	const loginUser = 1;
	const notLogin= 0;

	const registration = 1;

	
}