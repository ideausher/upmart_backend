<?php

namespace Intersoft\Auth\App\Enum;

final class DeviceTypeEnum
{
	const android = 0;
	const ios = 1;
}
