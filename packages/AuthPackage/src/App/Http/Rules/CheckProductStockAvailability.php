<?php

namespace Intersoft\Auth\App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckProductStockAvailability implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        dd("sdf");
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The passed quantity of product is greater then available stock';
    }
}
