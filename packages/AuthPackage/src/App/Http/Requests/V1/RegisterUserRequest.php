<?php

namespace Intersoft\Auth\App\Http\Requests\V1;
use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class RegisterUserRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool 
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'nullable|email|unique:users,email',
            'name' => 'nullable|string|regex:/^([a-z A-Z]+)$/',
            'phone_number' => 'nullable|string|regex:/^([0-9]+)$/|unique:users,phone_number|min:8|max:14',
            'password' => 'sometimes|required|string|min:8|max:12|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%#*?&])[A-Za-z\d@$!%#*?&]{8,}$/',
            'country_code' => 'sometimes|required_with:phone_number|string|regex:/^(\+[0-9]+)$/|max:5',
            'country_iso_code' => 'sometimes|required_with:phone_number|string|regex:/^([a-zA-Z]+)$/|min:0|max:3',
            'user_type'=>['required',
            'numeric',
             Rule::in([1,2]),
                 ],
            "address.*.is_primary"=>['required','numeric',Rule::in([0,1]),],
            "address.*.city"=>'required|string',
            "address.*.country"=>'required|string',
            "address.*.formatted_address"=>'required|string',
            "address.*.additional_info"=>'nullable|string',
            "address.*.latitude"=>'required|numeric',
            "address.*.longitude"=>'required|numeric',
            "address.*.pincode"=>'required|string|regex:/^([a-z A-Z0-9]+)$/|min:2|max:15',
            "date_of_birth"=>'nullable|date|date_format:Y-m-d'
                ];
            
    }
    public function messages()
    {
        return [
            // 'address.*.city.string' => trans('Api/v1/auth.must_be_a_string', ['name' => 'city']),
            // 'address.*.country.string' => trans('Api/v1/auth.must_be_a_string', ['name' => 'country']),
            // 'address.*.formatted_address.string' => trans('Api/v1/auth.must_be_a_string', ['name' => 'formatted_address']),
            // 'address.*.additional_info.string' => trans('Api/v1/auth.must_be_a_string', ['name' => 'additional_info']),
            // 'address.*.latitude.numeric' => trans('Api/v1/auth.must_be_a_string', ['name' => 'latitude']),
            // 'address.*.longitude.numeric' => trans('Api/v1/auth.must_be_a_string', ['name' => 'longitude']),

            'address.*.city.string' => 'City must be string',
            'address.*.country.string' => 'Country must be string',
            'address.*.formatted_address.string' => 'Formatted_address must be string',
            'address.*.additional_info.string' => 'Additional_info must be string',
            'address.*.latitude.numeric' => 'Latitude must be numeric',
            'address.*.longitude.numeric' => 'Longitude must be numeric',
            'address.*.is_primary.numeric' =>'Primary address must be numeric',
            'address.*.pincode.numeric'=>'pincode must be numeric',

            'address.*.city.required' => 'City must be required',
            'address.*.country.required' => 'Country must be required',
            'address.*.formatted_address.required' => 'Formatted_address must be required',
            'address.*.latitude.required' => 'Latitude must be required',
            'address.*.longitude.required' => 'Longitude must be required',
            'address.*.is_primary.required' => 'Primary address must be requird',
            'address.*.pincode.required' => 'Pincode must be required',

        ];
    }
}
