<?php

namespace Intersoft\Auth\App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class ForgetChangePasswordRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'required_if:type,==,1|nullable|string|regex:/^([0-9]+)$/|exists:users,phone_number|min:8|max:14',
            'country_code' => 'nullable|required_with:phone_number|string|regex:/^(\+[0-9]+)$/ |max:5',
            'type' => 'nullable|integer|min:1|max:3',
            'email' => 'nullable|required_if:type,==,2|email',
            'new_password'=>'required|string|min:8|max:12|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%#*?&])[A-Za-z\d@$!%#*?&]{8,}$/',
            'authorization' => 'required|integer|min:0|max:1',
            'country_iso_code' => 'sometimes|required_with:phone_number|string|regex:/^([a-zA-Z]+)$/|min:0|max:3'
        ];
    }
}
