<?php

namespace Intersoft\Auth\App\Http\Requests\V1;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class EditAddressRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ( [
            "is_primary"=>['required',
            'numeric',
            Rule::in([0,1]),
                ],
            "city"=>'required|string|nullable',
            "country"=>'required|string',
            "formatted_address"=>'required|string',
            "additional_info"=>'sometimes',
            "latitude"=>'required|numeric',
            "longitude"=>'required|numeric',
            "pincode"=>'required|string|regex:/^([a-z A-Z0-9]+)$/|min:2|max:15',
            
        ]);
    }
}
