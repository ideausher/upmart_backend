<?php

namespace Intersoft\Auth\App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;
class SendOtpRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required|integer|min:1|max:3',
        	'email'=>'sometimes|required_if:type,==,2|required_if:type,==,3|email',
            'phone_number' => 'sometimes|required_if:type,==,1|required_if:type,==,3|regex:/^([0-9]+)$/|string|min:8|max:14',
            'country_code' => 'required_with:phone_number|string|regex:/^(\+[0-9]+)$/ |max:5',
            'country_iso_code' => 'sometimes|required_with:phone_number|string|regex:/^([a-zA-Z]+)$/|min:0|max:3'
        ];
    }
}
 