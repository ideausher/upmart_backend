<?php

namespace Intersoft\Auth\App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class SocialUserRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'social_type' => 'required|integer|min:1|max:3',
            "social_id"=>'required|string',
            'name' => 'nullable|string',
            'email' => 'nullable|email',
            'phone_number' => 'nullable|string|regex:/^([0-9]+)$/|unique:users,phone_number|min:8|max:14',
            'country_code' => 'nullable|string|regex:/^(\+[0-9]+)$/|max:5',
            'country_iso_code' => 'nullable|string',
            "date_of_birth"=>'nullable|date|date_format:Y-m-d',
            "user_type" => 'required|integer'
        ];
    }
    public function messages()
    {
        return [
            
        ];
    }
}
