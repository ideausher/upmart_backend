<?php

namespace Intersoft\Auth\App\Http\Requests\V1;
use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class VerifyOtpRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'sometimes|required_if:type,==,1|nullable|string|regex:/^([0-9]+)$/|min:8|max:14|exists:otps,phone_number',
            'country_code' => 'sometimes|nullable|required_with:phone_number|string|regex:/^(\+[0-9]+)$/|max:5',
            'email' => 'sometimes|required_if:type,==,2|nullable|email|exists:otps,email',
            'otp' => 'required|string|min:4|max:4',
            'type' => 'required|integer|min:1|max:3',
            'action' => 'required|integer|min:0|max:1'
        ];
    }
}
