<?php

namespace Intersoft\Auth\App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;
use Illuminate\Validation\Rule;
class LoginUserRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes|required_if:type,==,2|email|exists:users,email',
            'phone_number' => 'nullable|required_if:type,==,1|required_if:type,==,3|regex:/^([0-9]+)$/|string|min:8|max:14',
            'country_code' => 'nullable|required_with:phone_number|regex:/^(\+[0-9]+)$/|max:5',
            'password' => 'nullable|required_if:type,==,2|required_if:type,==,1|string|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[#?!@$%^&*-])/|min:8|max:12',
            'type' => 'required|integer|min:1|max:3',
            'token'=> 'required_if:type,==,3|integer|min:0|max:1 ',
            'registration'=> [
                'nullable',
                'numeric',
                Rule::in([0, 1]),
            ],
            'user_type'=>['required_if:registration,==,1',
            'numeric',
             Rule::in([1,2]),
                 ],
            'country_iso_code' => 'sometimes|required_with:phone_number|string|regex:/^([a-zA-Z]+)$/|min:0|max:3'
        ];
    }
}
