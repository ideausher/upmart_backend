<?php

namespace Intersoft\Auth\App\Http\Requests\V1;
use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class UpdateProfileRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title'=> 'sometimes|string|regex:/^([a-z A-Z]+\.)$/|min:2|max:4',
            'title'=> 'sometimes|nullable|string|min:2|max:4',

            'email' => 'sometimes|email',
            'name' => 'sometimes|string|regex:/^([a-z A-Z]+)$/',
            'phone_number' => 'sometimes|string|regex:/^([0-9]+)$/|unique:users,phone_number|min:8|max:14',
            'country_code' => 'sometimes|string|regex:/^(\+[0-9]+)$/|min:1|max:5',
            'country_iso_code' => 'sometimes|string|regex:/^([a-zA-Z]+)$/|min:0|max:3',
            "date_of_birth"=>'nullable|date|date_format:Y-m-d',
            "referred_by_code"=>'nullable|exists:users,referral_code'
        ];
    }
}
      