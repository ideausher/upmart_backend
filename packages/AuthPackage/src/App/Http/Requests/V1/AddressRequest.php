<?php

namespace Intersoft\Auth\App\Http\Requests\V1;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;


class AddressRequest extends FormRequest
{
  
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() 
    {       
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "address.*.city"=>'required|string|nullable',
            "address.*.country"=>'required|string',
            "address.*.pincode"=>'required|string|regex:/^([a-z A-Z0-9]+)$/|min:2|max:15',
            "address.*.formatted_address"=>'required|string',
            "address.*.additional_info"=>'sometimes',
            "address.*.latitude"=>'required|numeric',
            "address.*.longitude"=>'required|numeric',
            "address.*.is_primary"=>'required|numeric|max:1|min:0',
          
        ];
    }
}
