<?php

namespace Intersoft\Auth\App\Http\Requests\V6;

use Illuminate\Foundation\Http\FormRequest;
use Intersoft\Auth\App\Traits\RequestFailedValidation;

class    UserBasedLocationRequest extends FormRequest
{
    use RequestFailedValidation;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "latitude"=>'sometimes|string|nullable',
            "longitude"=>'sometimes|string|nullable',
            "accuracy"=> 'sometimes|nullable',
            "altitude"=> 'sometimes|nullable',
            "speed"=> 'sometimes|nullable',
            "speedAccuracy"=> 'sometimes|nullable',
            "heading"=> 'sometimes|nullable',
            "time"=> 'sometimes|nullable',
        ];
    }
}
