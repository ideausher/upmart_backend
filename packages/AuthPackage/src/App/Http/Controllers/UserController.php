<?php

namespace Intersoft\Auth\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Intersoft\Auth\App\Traits\V1\UserTrait;

class UserController extends Controller
{
	use UserTrait;
}