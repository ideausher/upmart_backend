<?php

namespace Intersoft\Auth\App\Http\Controllers;
use App\Http\Controllers\Controller;
use Intersoft\Auth\App\Traits\V1\UserBasedLocationTrait;

class UserBasedLocationController extends Controller
{
       use UserBasedLocationTrait;
}