<?php

namespace Intersoft\Auth\App\Http\Resources;

use stdClass;
use App\Http\Resources\UserAddressResource;
use Illuminate\Http\Resources\Json\JsonResource;
 use Intersoft\Auth\App\Models\UserAddress;

class UserLoginResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    
       
        $response_data = [
            
            "id"=> $this->id ?? "",
            "title"=>$this->title ?? "",
            'name' => $this->name ?? "",
            'email' => $this->email ?? "",
            'phone_number' => $this->phone_number ?? "",
            "country_iso_code" => $this->country_iso_code ?? "",
            "country_code"=> $this->country_code ?? "",
            "verified"=> $this->verified ? 1 : 0,
            "user_type"=>$this->user_type ,
            "profile_picture"=>$this->profile_picture,
            "date_of_birth" =>$this->date_of_birth ?? "",
            "address" => $this->userAddresses ? UserAddressResource::collection($this->userAddresses) : []

            

        ];
      

        if (isset($this->auth_token) and $this->auth_token != null) {
            $response_data['access_token'] = $this->auth_token;
        }
        elseif ($this->user_type ==2 ){
            $response_data["availablity"]=$this->availability;
        }

        return $response_data;
    }

}
