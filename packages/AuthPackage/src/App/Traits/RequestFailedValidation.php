<?php

namespace Intersoft\Auth\App\Traits;

use Intersoft\Auth\App\Traits\APIResponse;
use Illuminate\Contracts\Validation\Validator;
//use Illuminate\Http\Exceptions\HttpResponseException;
use App\Exceptions\BadRequestException;

trait RequestFailedValidation
{

	use APIResponse;

	protected function failedValidation(Validator $validator)
	{
		throw new BadRequestException($validator->errors()->first());
	}
}
