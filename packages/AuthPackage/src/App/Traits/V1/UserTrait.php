<?php

namespace Intersoft\Auth\App\Traits\V1;
use Illuminate\Http\Request;

use Intersoft\Auth\App\Helpers\V1\UserHelper;
use Intersoft\Auth\App\Service\Interfaces\UserServiceInterface;

use Intersoft\Auth\App\Http\Requests\V1 as V1Request;

trait UserTrait
{
	private $userService;
	private $userHelper; 

	public function __construct(UserServiceInterface $userService, UserHelper $userHelper)
	{
		$this->userService = $userService;
		$this->userHelper = $userHelper;
	}
	public function register(V1Request\RegisterUserRequest $request)
	{
		return $this->userService->register($request);
	}
	public function login(V1Request\LoginUserRequest $request)
	{
		return $this->userService->login($request);
	}
	public function socialLogin(V1Request\SocialUserRequest $request)
	{
		$req = $this->userHelper->SocialUser($request);
		return $this->userService->socialLogin($req);
	}
	public function sendOtp(V1Request\SendOtpRequest $request)
	{
		return $this->userService->sendOtp($request);   
	}
	public function verifyOtp(V1Request\VerifyOtpRequest $request)
	{
		return $this->userService->verifyOtp($request);
	}
	public function forgetChangePassword(V1Request\ForgetChangePasswordRequest $request)
	{
		return $this->userService->forgetChangePassword($request);
	}
	public function uploadImage(V1Request\UploadImageRequest $request)
	{
		return $this->userService->uploadImage($request);
	}
	public function imageUpload(V1Request\UploadImageRequest $request)
	{
		return $this->userService->imageUpload($request);
	}
	public function deleteImage(V1Request\DeleteImageRequest $request)
	{
		return $this->userService->deleteImage($request);
	}
	public function verifyEmailOrPhone(V1Request\VerifyEmailOrPhoneRequest $request)
	{
		return $this->userService->verifyEmailOrPhone($request);
	}
	public function updateProfile(V1Request\UpdateProfileRequest $request)
	{
		return $this->userService->updateProfile($request);
	}
	public function getProfile(V1Request\GetProfileRequest $request)
	{
		
		return $this->userService->getProfile($request);
	}


	public function checkUserExistence(V1Request\CheckUserExistenceRequest $request)
	{
		return $this->userService->checkUserExistence($request);
	}


	public function editAddress(V1Request\EditAddressRequest $request)
	{
		return $this->userService->editAddress($request);
	}


	public function addAddress(V1Request\AddressRequest $request)
	{
		
		return $this->userService->addAddress($request);
	}


	public function deleteAddress(V1Request\DeleteAddressRequest $request)
	{
		return $this->userService->deleteAddress($request);
	}
	public function getOtp(V1Request\GetOtpRequest $request)
	{
		return $this->userService->getOtp($request);
	}
	public function logout(Request $request){
		return $this->userService->logout($request);
	}
	public function isblock(V1Request\CheckUserExistenceRequest $request)
	{
		return $this->userService->isblock($request);
	}
	public function getAddress(V1Request\GetProfileRequest $request)
	{
	
		return $this->userService->getAddress($request);
	}
	public function updateavailability(V1Request\UpdateAvailabilityRequest $request)
	{
		return $this->userService->updateavailability($request);
	}
	public function getavailability(Request $request)
	{
		return $this->userService->getavailability($request);
	}

	public function myReferralCode(Request $request)
	{
		return $this->userService->myReferralCode($request);
	}
	
}
