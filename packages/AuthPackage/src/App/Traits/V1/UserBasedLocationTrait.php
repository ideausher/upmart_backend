<?php
namespace Intersoft\Auth\App\Traits\V1;
use Illuminate\Http\Request;
use Intersoft\Auth\App\Http\Requests\V6 as V6Request;
use App\Service\Interfaces\UserBasedLocationServiceInterface;

trait UserBasedLocationTrait
{
    private $locationService;

 

	public function __construct(UserBasedLocationServiceInterface $locationService)
	{

        $this->locationService = $locationService;
  
	}
    
    public function updatelocation(V6Request\UserBasedLocationRequest $request)
	{   
    
        return $this->locationService->updatelocation($request);
        
    }
    
    public function getcurrentlocation(V6Request\UserBasedLocationRequest $request){
        return $this->locationService->getcurrentlocation($request);
    }

}