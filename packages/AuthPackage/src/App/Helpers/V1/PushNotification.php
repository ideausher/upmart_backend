<?php

namespace Intersoft\Auth\App\Helpers\V1;

use App\Models\AdminFcmToken;
use App\Models\DeviceDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PushNotification
{

    public function __construct()
    {
    }

    private function androidPushNotifiction($message, $title, $deviceToken, $moreData = [],$notificationToSent = null)
    {
        
        $registrationIds = (is_array($deviceToken)) ? $deviceToken : ["$deviceToken"];

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $data = [
            'title' => $title,
            'body' => $message,
        ];

        if(! isset($notificationToSent)){
            $fcmNotification = [
                'registration_ids' => $registrationIds, //multple token array
                "data" => $moreData,
                "notification" => $data,
            ];
        }
        else{
            $fcmNotification = [
                'registration_ids' => $registrationIds, //multple token array
                "data" => $moreData,
            ];
        }
        


        $headers = [
            'Authorization: key=' . config('services.fcm_token'),
            'Content-Type: application/json',
            'Accept: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        Log::info("Notification Result".$result);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function iphonePushNotifiction($message, $title, $deviceToken, $moreData = [],$notificationToSent = null)
    {
        $registrationIds = (is_array($deviceToken)) ? $deviceToken : ["$deviceToken"];

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $data = [
            'title' => $title,
            'body' => $message,
            'priority' => '"high"',
        ];

        
        if(! isset($notificationToSent)){
            $fcmNotification = [
                'registration_ids' => $registrationIds, //multple token array
                "data" => $moreData,
                "notification" => $data,
            ];
        }
        else{
            $fcmNotification = [
                'registration_ids' => $registrationIds, //multple token array
                "data" => $moreData,
            ];
        }

        $headers = [
            'Authorization: key=' . config('services.fcm_token'),
            'Content-Type: application/json',
            'Accept: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function sendNotification($data, $userId)
    {

        $androidDeviceTokens = DeviceDetails::where('user_id', $userId)->AndroidTokens()->get()->toArray();
        $iphoneTokens = DeviceDetails::where('user_id', $userId)->IosToken()->get()->toArray();
        
        $status = '';
        Log::error($userId);
        Log::error($androidDeviceTokens);
        Log::error($iphoneTokens);
        Log::error("*** Android Device token of the user ***");
        if (!empty($androidDeviceTokens)) {
            if(isset($data["data"]["type"]) && $data["data"]["type"] == 100){
                $status = $this->androidPushNotifiction($data['message'], $data['title'], array_column($androidDeviceTokens, 'device_token'), $data['data'], 1);
            }
            else{
                $status = $this->androidPushNotifiction($data['message'], $data['title'], array_column($androidDeviceTokens, 'device_token'), $data['data']);
            }
            Log::error("*** Android Device token of the user *** : ". $status);
        }
        if (!empty($iphoneTokens)) {
            
            if(isset($data["data"]["type"]) && $data["data"]["type"] == 100){
                $status = $this->iphonePushNotifiction($data['message'], $data['title'], array_column($iphoneTokens, 'device_token'), $data['data'], 1);
            }
            else{
                $status = $this->iphonePushNotifiction($data['message'], $data['title'], array_column($iphoneTokens, 'device_token'), $data['data']);
            }
        }

        return $status;
    }

    private static function send_request_to_fcm_server(array $fcm_notification_data, $fcm_server_key)
	{
		$fcmUrl = 'https://fcm.googleapis.com/fcm/send';

		$headers = [
			'Authorization: key=' . $fcm_server_key,
			'Content-Type: application/json',
			'Accept: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $fcmUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcm_notification_data));
		$result = curl_exec($ch);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    // sed push notification to admin panel
	public static function sendAdminPushNotification($user_id,$title, $message, $booking_type)
	{
		$admin_fcm_token = AdminFcmToken::where('userid', $user_id)->get();
		if (!$admin_fcm_token->count() > 0) {
			return false;
		}
		$admin_fcm_server_key = env('ADMIN_FCM_SERVER_CLIENT_SIDE_KEY', "");
		$fcm_notification = [
			'registration_ids' => $admin_fcm_token->pluck('token')->toArray(), //multiple token array
			"notification" => [
				"body" => $message,
				"title" => $title,
				"icon" => "https://limitless-images-test.s3.amazonaws.com/1621597916ic_launcher.png"
			],
			'data' => [
				'booking_type' => $booking_type
			]
		];

		return self::send_request_to_fcm_server($fcm_notification, $admin_fcm_server_key);
	}

}