<?php

namespace Intersoft\Auth\App\Helpers\V1;
use Illuminate\Validation\Rule;


class UserHelper
{
    public function User($request)
    {
        return [    
            'title' => isset($request->title) ? $request->title : null,
            'type' => isset($request->type) ? $request->type : null,
            'email' => isset($request->email) ? $request->email : null,
            'name' => isset($request->name) ? $request->name : null,
            'phone_number' =>  isset($request->phone_number) ? $request->phone_number : null,
            'password' => isset($request->password) ? $request->password : null,
            'country_code' => isset($request->country_code) ? $request->country_code : null,
            'country_iso_code' => isset($request->country_iso_code) ? $request->country_iso_code : null,
            'verified' =>  0,
            'user_type'=>$request->user_type,
            'date_of_birth'=>$request->date_of_birth ?? null,
        ];
    }
    public function SocialUser($request)
    {
        return [
            'social_type' => $request->social_type,
            'social_id' => $request->social_id,
            'email' => isset($request->email) ? $request->email : null,
            'name' => isset($request->name) ? $request->name : null,
            'profile_picture' => isset($request->profile_picture) ? $request->profile_picture : null,
            'phone_number' =>  isset($request->phone_number) ? $request->phone_number : null,
            'profile_picture' => isset($request->profile_picture) ? $request->profile_picture : null,
            'password' => isset($request->password) ? $request->password : null,
            'country_code' => isset($request->country_code) ? $request->country_code : null,
            'country_iso_code' => isset($request->country_iso_code) ? $request->country_iso_code : null,
            'verified' => 1,
            'date_of_birth'=>$request->date_of_birth ?? null,
            'user_type'=>$request->user_type ?? null,
        ];
    }
    public function getInsertUserAddressData($address_data, $user_id)
    {
        $returnData = [];
        foreach ($address_data as $singleAddress) {
            $returnData[] = [
                'user_id' => $user_id,
                'type' => $singleAddress['address_type'],
                'is_primary' =>$singleAddress['is_primary'] ,
                'latitude' => $singleAddress['latitude'],
                'longitude' => $singleAddress['longitude'],
                'city' => $singleAddress['city'] ?? "",
                'country' => $singleAddress['country'] ?? "",
                'formatted_address' => $singleAddress['formatted_address'] ?? "",
                'additional_info' => $singleAddress['additional_info'] ?? "",
                'pincode'=>$singleAddress['pincode'],
            ];
        }
        return $returnData;
    }
    public static function getUpdateAddress($address)
	{
		return $returnData[] = [
            
            'type' => $address['address_type'],
            
            'is_primary' =>$address['is_primary'] ,
			'latitude' => $address['latitude'],
			'longitude' => $address['longitude'],
			'city' => $address['city'] ?? "",
			'country' => $address['country'] ?? "",
			'formatted_address' => $address['formatted_address'] ?? "",
            'additional_info' => $address['additional_info'] ?? "",
            'pincode'=>$address['pincode'],
		];
    }
    public static function getAddress($address,$user_id)
	{
        
		return $returnData[] = [
            'id'=>$address['address_id'],
            'user_id'=>$user_id,
            'is_primary' =>$address['primary_address'] ,
			'type' => $address['address_type'],
			'latitude' => $address['latitude'],
			'longitude' => $address['longitude'],
			'city' => $address['city'] ?? "",
			'country' => $address['country'] ?? "",
			'formatted_address' => $address['formatted_address'] ?? "",
            'additional_info' => $address['additional_info'] ?? "",
            'pincode'=>$address['pincode'],
        ];
       
    }
   
    
   
}
