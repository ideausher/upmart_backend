<?php

namespace Intersoft\Auth\App\Helpers\V1;

use Illuminate\Validation\Rule;

class DeliveryboyRegisterHelper
{
    public function register($request)
    {
        
        return [
            'registeration_number' => isset($request->vehicle_registeration['registeration_number']) ? $request->vehicle_registeration['registeration_number'] :null,
            'vehicle_picture'=>isset($request->vehicle_registeration['vehicle_picture']) ? $request->vehicle_registeration['vehicle_picture'] : null,
            'vehicle_company'=>isset($request->vehicle_registeration['vehicle_company']) ? $request->vehicle_registeration['vehicle_company'] : null,
            'model' => isset($request->vehicle_registeration['model']) ? $request->vehicle_registeration['model'] : null,
            'color' => isset($request->vehicle_registeration['color']) ? $request->vehicle_registeration['color'] : null,
            'valid_upto' => isset($request->vehicle_registeration['valid_upto']) ? $request->vehicle_registeration['valid_upto'] : null,
            
            'drivinglicence_images'=>isset($request->driving_licence['drivinglicence_no']) ? $request->driving_licence['drivinglicence_images'] : null,
            'drivinglicence_no' =>  isset($request->driving_licence['drivinglicence_no']) ? $request->driving_licence['drivinglicence_no'] : null,

            'drivinglicence_expiry_date' => isset($request->driving_licence['drivinglicence_expiry_date']) ? $request->driving_licence['drivinglicence_expiry_date'] : null,
            'licence_no_plate_images'=>isset($request->licence_no_plate['licence_no_plate_images']) ? $request->licence_no_plate['licence_no_plate_images'] : null,
            'no_plate' => isset($request->licence_no_plate['no_plate']) ? $request->licence_no_plate['no_plate'] : null,
            'vehicle_insurence_image'=>isset($request->vehicle_insurance['vehicle_insurence_image'])  ?  $request->vehicle_insurance['vehicle_insurence_image'] :null,
            'policy_number' => isset($request->vehicle_insurance['policy_number']) ? $request->vehicle_insurance['policy_number'] : null,
            'vehicle_insurence_valid_upto' => isset($request->vehicle_insurance['vehicle_insurence_valid_upto']) ? $request->vehicle_insurance['vehicle_insurence_valid_upto'] : null,
            'vehicle_insurence_valid_from' => isset($request->vehicle_insurance['vehicle_insurence_valid_from']) ? $request->vehicle_insurance['vehicle_insurence_valid_from'] : null,
        
        ];
    }
}
