<?php

namespace Intersoft\Auth\App\Models;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\UserAddress;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intersoft\Auth\App\Http\Resources\UserLoginResource;

class User extends Model 
{
    use Notifiable,HasApiTokens;
    
    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at',
        'created_at',
        'email_verified_at'
    ];

    protected $appends = ['resource_url'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getResourceUrlAttribute()
    {
        return url('/admin/users/'.$this->getKey());
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function AuthAccessToken()
    {
        return $this->hasMany('Intersoft\Auth\App\Models\OauthAccessToken');
    }

    public function setResource($user)
    {
        return new UserLoginResource($user);
    }

    public function userAddresses()
    {
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id');
    }
}
