<?php

namespace Intersoft\Auth\App\Repository;

use Intersoft\Auth\App\Repository\Interfaces\IGenericRepository;

abstract class Criteria
{

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public abstract function apply($model, IGenericRepository $repository);
}
