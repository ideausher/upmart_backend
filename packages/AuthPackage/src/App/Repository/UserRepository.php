<?php

namespace Intersoft\Auth\App\Repository;

use App\Models\User;
use Lcobucci\JWT\Parser;
use App\Models\Notification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\FCMNotification\AccountActivated;
use Intersoft\Auth\App\Traits\ApiUserTrait;
use Intersoft\Auth\App\Helpers\V1\UserHelper;
use Intersoft\Auth\App\Repository\GenericRepository;
use Intersoft\Auth\App\Repository\Interfaces\IUserRepository;

class UserRepository extends GenericRepository implements IUserRepository
{
    use ApiUserTrait;
    private $userHelper;

    public function model()
    {
        return 'App\Models\User';
    }
    function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
    
    function getGeneratedString($length)
    {
        $generatedString = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited
    
        for ($i=0; $i < $length; $i++) {
            $generatedString .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }
    
        return $generatedString;
    }
    public function get_user(array $where)
    {
        $data=$this->model->where($where)->first();
        return $data;
    }

    public function createUser(array $data)
    {
        $user = $this->model->create([
            'title' => $data['title'],
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'country_code' => $data['country_code'],
            'country_iso_code' => $data['country_iso_code'],
            'password' => $data['password'],
            'user_type'=>$data['user_type'],
            'referral_code' => $this->getGeneratedString(6)
            
        ]);
        $user->token = $user->createToken('Api access token')->accessToken;
        $this->insertDeviceDetails($user->token, $user->id);
        return $user;
    }
    public function createSocialUser($data)
    {
        $user = $this->model->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'country_code' => $data['country_code'],
            'country_iso_code' => $data['country_iso_code'],
            'password' => $data['password'],
            'verified' => $data['verified'],
            'profile_picture' => $data['profile_picture'],
            'social_type' => $data['social_type'],
            'social_id'=> $data['social_id'],
            'user_type' => $data['user_type'],
            'referral_code' => $this->getGeneratedString(6)
        ]);
        $user->token = $user->createToken('Api access token')->accessToken;
        $this->insertDeviceDetails($user->token, $user->id);
        return $user;
    }
   
        
    





    /**
     * Finds a user with id
     * @param string $id
     * @return User|null
     */
    public function findUserWithId($id)
    {
        return $this->where([
                'id' => $id
            ])->first();
    }

    /**
     * Get User By its referral code
     *
     * @return User
     */
    public function getUserByReferralCode($referralCode){
        return $this->where([
            'referral_code' => $referralCode
        ])->first();
    }

    /**
     * Find Delivery Boy with ID
     *
     * @param [type] $id
     * @return void
     */
    public function findDeliveryBoyWithId($id)
    {
        return $this->where([
                'id' => $id,
                'user_type' => 2
            ])->first();
    }

    /**
     * Finds a user with Social Id
     * @param string $sociaId
     * @return User|null
     */
    public function findUserWithSocialId($socialId)
    {
        return $this->where([
                'social_id' => $socialId
            ])->first();
    }

    /**
     * Finds a user with Phone Number
     * @param string $phoneNumber
     * @param string $countryCode
     * @return User|null
     */
    public function findUserWithPhoneNumber($phoneNumber, $countryCode)
    {
    
        return $this->model->where([
            'phone_number' => $phoneNumber,
            'country_code' => $countryCode,
        ])->first();
    }

    /**
     * Finds a user with Email ID
     * @param string $email
     * @return User|null
     */
    public function findUserWithEmailId($email)
    {
        return $this->where([
            'email' => $email,
        ])->first();
    }

    /**
     * Finds a user with Phone Number and Email ID
     * @param string $phoneNumber
     * @param string $countryCode
     * @return User|null
     */
    public function findUserWithPhoneNumberAndEmailId($phoneNumber, $countryCode, $email)
    {
        return $this->where([
            'phone_number' => $phoneNumber,
            'country_code' => $countryCode,
            'email' => $email,
        ])->first();
    }

    /**
     * Attempt to login with Phone No, Country Code and Password
     * @param string $phone_no
     * @param string $countryCode
     * @param string $password
     * @return User|false
     */
    public function attemptAuthPhoneNo($phone_no, $countryCode, $password)
    {
        
        $user = $this->model->where(['phone_number'=>$phone_no,'country_code' => $countryCode])->first();
        
        if (!$user) {
            return false;
		}
        if (!Hash::check($password, $user->password)) {
            return false;
		}
// echo "hello";die;
        return $user;
    }
    public function attemptAuthPhoneNoWithoutToken($phone_no, $countryCode, $password)
    {
        $user = Auth::attempt([
                'phone_number' => $phone_no,
                'password' => $password
            ]);
        if (!$user) {
            return false;
        }

        $user =	$this->findUserWithPhoneNumber($phone_no, $countryCode);
        if (!$user) {
            return false;
        }
        // $user->token = $user->createToken('Api access token')->accessToken;
    
        return $user;

    }


    public function attemptAuthOnlyPhoneNo($user)
    {
        $user = Auth::loginUsingId($user->id);
        if (!$user) {
            return false;
        }

        // $user =	$this->findUserWithPhoneNumber($phone_no,$countryCode);
        // if(!$user)
        // 	return false;
        // $user->token = $user->createToken('Api access token')->accessToken;

        // return $user;
    }
    /**
     * Attempt to login with Email Id and Password
     * @param email $email
     * @param string $password
     * @return User|false
     */
    public function attemptAuthEmailId($email, $password)
    {
        $user = Auth::attempt([
            'email' =>  $email,
            'password' => $password
            ]);

        if (!$user) {
            return false;
        }

        $user =	$this->findUserWithEmailId($email);
        $user->token = $user->createToken('Api access token')->accessToken;
        $this->insertDeviceDetails($user->token, $user->id);


        return $user;
    }
    public function attemptAuthEmailIdWithoutToken($email, $password)
    {
        $user = Auth::attempt([
            'email' =>  $email,
            'password' => $password
            ]);

        if (!$user) {
            return false;
        }

        $user =	$this->findUserWithEmailId($email);
        // $user->token = $user->createToken('Api access token')->accessToken;


        return $user;
    }


    /**
     * Attempt to login with Social Id
     * @param string $social_id
     * @return User|false
     */
    public function attemptAuthSocialId($social_id, $social_type)
    {
        // $user = $this->findUserWithSocialId($social_id);
        $user1 = Auth::attempt([
            'social_id' => $social_id,
            'social_type' => $social_type,
            'password'=>'',
        ]);
        $user=Auth::user();
        if (!$user) {
            return false;
        }

        $user->token = $user->createToken('Api access token')->accessToken;
        $this->insertDeviceDetails($user->token, $user->id);
        return $user;
    }

    /**
     * Delete Single Access token of a user
     * @param User $user
     * @return User
     */
    public function deleteAccessToken($user)
    {
        return $user->AuthAccessToken()->delete();
    }

    /**
     * Updates a user with data
     * @param User $user
     * @param array $data
     * @return User
     */
    public function updateUser($user, $data)
    {
        return $user->update($data);
    }

    public function getDeliveryBoysWithReviews($minRating , $maxRating, $rejectmin , $rejectmax, $rejectnull, $lat, $lng , $maxAcceptingLimit)
    {
        $avg = $this->model->join('users_based_location', 'users_based_location.user_id', '=', 'users.id')
        ->selectRaw(" 
            users.*,
            ( 6371 * acos( cos( radians('$lat') ) * cos( radians( lat ) ) * cos( radians( lng) - 
                radians('$lng') ) + sin( radians('$lat') ) * sin( radians( lat ) ) ) 
            ) AS distance"
        )->where([
            'user_type' => 2,
            'is_block' => 0,
            'availability' => 1
        ])
        ->havingRaw('distance <= ' . env("CUS_DISTANCE_VALUE", "20"))
        ->with(['avgReviews','currentBookings'])
        ->get();

        Log::error("Delivery Boy : " .json_encode($avg));
// dd($avg);
        $avg = $avg->reject(function ($value) use($maxAcceptingLimit){
                if(isset($value->currentBookings) && ($value->currentBookings->count() >= $maxAcceptingLimit )){
                    Log::error("Delivery Boy Current Bookings : " . $value->currentBookings->count());
                    return true;
                }
            })
            ->reject(function ($value) use($minRating,$maxRating,$rejectmin,$rejectmax,$rejectnull){ // reject those having rating between min rating and maxrating
            Log::error(json_encode($value));
            if(isset($value->avgReviews)){
                
                if( (int)($value->avgReviews->avgRating) >= $minRating && (int)($value->avgReviews->avgRating) <= $maxRating){
                    Log::error("Avg Rating : " . $value->avgReviews->avgRating . " >= $minRating " . "and " .$value->avgReviews->avgRating ." <= $maxRating");
                    return true;
                }

                if(isset($rejectmin) && isset($rejectmax)){
                    
                    if( (int)($value->avgReviews->avgRating) >= $rejectmin && (int)($value->avgReviews->avgRating) <= $rejectmax){
                        Log::error("Avg Rating : " . $value->avgReviews->avgRating . " >= $rejectmin " . "and " .$value->avgReviews->avgRating ." <= $rejectmax");
                        return true;
                    }
                }

                if( (int)($value->avgReviews->avgRating) >= $minRating ){
                    Log::error("Avg Rating : " . $value->avgReviews->avgRating . " >= $minRating ");
                    return true;
                }

                if( (int)($value->avgReviews->avgRating) < $rejectmin ){
                    // dd('new if');
                    Log::error("Avg Rating : " . $value->avgReviews->avgRating . " <= $maxRating ");
                    return true;
                }
            }
            else{
                if($rejectnull == 1){
                    Log::error("Reject NULL : First IF");
                    return true;
                }
            }
        });
        return $avg;
    }

    public function getDeliveryBoys(){
        return $this->model->where([
                'user_type' => 2,
                'is_block' => 0,
                'availability' => 1
            ])
            ->whereNotNull('stripe_customer_id')
            ->with(['deliveryBoyDocuments'])
            ->get()
            ->reject(function($value){
                if( ! isset($value->deliveryBoyDocuments)){
                    return true;
                }
            });
    }
    public function getDeliveryBoysWithLatLng($lat, $lng){
        return $this->model->selectRaw("
                *,
                ( 6371 * acos( cos( radians(' $lat ') ) * cos( radians( latitude ) ) * cos( radians( longitude) - 
                    radians('  $lng ') ) + sin( radians('  $lat ') ) * sin( radians( latitude ) ) ) 
                ) AS distance "
            )
            ->where([
                'user_type' => 2,
                'is_block' => 0,
                'availability' => 1
            ])
            ->whereNotNull('stripe_customer_id')
            ->with(['deliveryBoyDocuments'])
            ->get()
            ->reject(function($value){
                if( ! isset($value->deliveryBoyDocuments)){
                    return true;
                }
            });
    }
    
}
